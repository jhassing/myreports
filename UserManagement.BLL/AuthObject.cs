using System;
using System.Web;
using System.Web.SessionState;
using System.Threading;
using System.Globalization;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Security.Principal;
using System.Resources;
using System.Text;
				
using MyReports.Common;

using Compass.Security.ActiveDirectory;

namespace UserManagement.BLL
{
	public class AuthObject
	{
	
		#region Get Authentication Object

		public static UserManagement.BLL.Authentication GetAuthObject
		{
			get
			{
				//return new UserManagement.BLL.Authentication( AppValues.UserName, AppValues.Password, AppValues.FullyQualifiedDomain, AppValues.LdapPort );
				return new UserManagement.BLL.Authentication( AppValues.ServiceAccountUser , AppValues.ServiceAccountPassword, AppValues.FullyQualifiedDomain, AppValues.LdapPort );
			}
		}

		#endregion

	}
}
