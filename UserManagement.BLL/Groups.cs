using System;
using System.Text;
using System.Collections; 
using System.Data;
using System.Data.SqlClient;

using System.Web.UI.WebControls;

using Compass.Data.MSSQLDataAccess;
using Compass.Security.ActiveDirectory;
using Novell.Directory.Ldap;

namespace UserManagement.BLL
{
	/// <summary>
	/// Class will handle all business logic associated with groups, membership, access ect
	/// </summary>
	public class Groups : BaseClass
	{
		public enum enumGroupScope
		{
			Global = 2,
			DomainLocal = 4,
			Universal = 8
		}

		public enum enumGroupType : long 
		{
			SecurityGroup = 2147483648,
			DistributionGroup = 0
		}


		#region ~const

		public Groups( Authentication oAuthentication ) 
			: base( oAuthentication )
		{		

		}

		#endregion

		#region Get Group Listing
		
		/// <summary>
		/// Method will search LDAP for all groups meetng the business logic requierments, place them into an ArrayList and return
		/// </summary>
		/// <param name="searchFilter">Filter to search LDAP for</param>
		/// <returns>ArrayList of SamAccountNames</returns>
		public ArrayList GetGroupListing( string searchFilter, string OU, int searhScope, int maxResults )
		{
			
			ArrayList	aryParams		= null;
			ArrayList	ary				= new ArrayList();
			ADGroup		oADGroup		= null;	
			
			try
			{
				oADGroup  = new ADGroup( base.get_LDAPConnection() );

				aryParams			=	new ArrayList();
				
				aryParams.Add( "samAccountName" );
				aryParams.Add( "distinguishedName" );	

				ArrayList aryGroupListing = oADGroup.FindGroups( searchFilter, aryParams, OU, searhScope, maxResults );

				foreach( LdapAttributeSet ldapAttribSet in aryGroupListing )
				{
					if ( ldapAttribSet != null )
					{				

						ary.Add( new System.Web.UI.WebControls.ListItem( base.GetAttributeValue( ldapAttribSet, "samAccountName" ),
							base.GetAttributeValue( ldapAttribSet, "distinguishedName" ) ));
					}
				}

				return ary;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				oADGroup		= null;
				base.Disconnect();
			}
		}

		#endregion

		#region Get Group Description Listing
		/// <summary>
		/// Method will search LDAP for the matching groups
		/// </summary>
		/// <param name="searchFilter">Filter to search for</param>
		/// <returns>Array of structs containing the samAccountName and description</returns>
		public ArrayList GetGroupDescriptionListing( string filter, string OU, int searhScope, int maxResults )
		{

			ArrayList	aryParams		= new ArrayList();
			ADGroup		oADGroup		= null;

			try
			{		

				oADGroup  = new ADGroup( base.get_LDAPConnection() );

				aryParams.Add( "cn" );
				aryParams.Add( "description" );
				aryParams.Add( "distinguishedName" );
		
				return oADGroup.FindGroups( filter, aryParams, OU, searhScope, maxResults );

			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				base.Disconnect();

				oADGroup = null;
			}
		}
		#endregion

        #region Get Group Description

        /// <summary>
		/// Method will search LDAP for the matching group description
		/// </summary>
        /// <param name="groupDN">The distinguished name of the group.</param>
		/// <returns>Group discription for the DN</returns>
        public void GetGroupDescription(string groupDN, out string groupName, out string description)
        {

			ADGroup		oADGroup		= null;

			try
			{
                oADGroup = new ADGroup(base.get_LDAPConnection());

                ArrayList ary = new ArrayList();
                ary.Add("description");
                ary.Add("cn");

                LdapAttributeSet ldapAttribSet = oADGroup.GetGroupProperties(groupDN, ary);

                if (ldapAttribSet != null)
                {
                    description = base.GetAttributeValue(ldapAttribSet, "description");
                    groupName = base.GetAttributeValue(ldapAttribSet, "cn");
                }
                else
                {
                    description = "";
                    groupName = "";
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                base.Disconnect();

                oADGroup = null;
            }
        }

        #endregion


        #region Get Group Membership, return DistinguishedName and Display Name

        public ArrayList GetGroupMembership( string userDistinguishedName )
		{		
			ADGroup oADGroup = null;

            ArrayList aryReturn = new ArrayList();
			
			try
			{
				oADGroup  = new ADGroup( base.get_LDAPConnection() );

				ArrayList ary = new ArrayList();
				ary.Add( "samAccountName" );
				ary.Add( "sn" );
				ary.Add( "cn" );
				ary.Add( "givenName" );
				ary.Add( "distinguishedName" );

                ArrayList obList = oADGroup.GetGroupMembership(userDistinguishedName, ary);                                    

                foreach (LdapAttributeSet ldapAttribSet in obList)
                {

                    string sam = base.GetAttributeValue(ldapAttribSet, "samAccountName");
                    string firstName = base.GetAttributeValue(ldapAttribSet, "givenName");
                    string lastName = base.GetAttributeValue(ldapAttribSet, "sn");
                    string DN = base.GetAttributeValue(ldapAttribSet, "distinguishedName");

                    string tmp = sam + " - " + firstName + " " + lastName;

                    aryReturn.Add(new ListItem(tmp, DN));
                }

                return aryReturn;       

			}
			catch (Exception ex)
			{
				throw ex;
			}   			
			finally
			{
				// Disconnect 
				base.Disconnect();

				oADGroup = null;
			}
		}

		#endregion

		#region Modify Group

		public void ModifyGroup( string groupDN, string description  )
		{
			ADGroup		oADGroup		= null;

			try
			{
				oADGroup  = new ADGroup( base.get_LDAPConnection() );

				oADGroup.ModifyGroup(	groupDN, description.Trim() );
			}
			catch (Exception ex)
			{
				throw ex;
			}				
			finally
			{
				oADGroup		= null;
				base.Disconnect();
			}		
		}

		#endregion

		#region Save User Group Membership
 
		public void SaveGroupMembership( string groupDN, ArrayList objectsToAdd, ArrayList objectsToRemove )
		{

			ADGroup           oADGroup          = null;                  

			try
			{     

				if ( objectsToAdd != null )
				{
					if ( objectsToAdd.Count > 0 )
					{

						oADGroup  = new ADGroup( base.get_LDAPConnection() );                               
 
						oADGroup.AddUsersToGroup( groupDN, objectsToAdd );
					}
				}
 
				if ( objectsToRemove != null )
				{
					if ( objectsToRemove.Count > 0 )
					{
						if ( oADGroup == null )
							oADGroup = new ADGroup( base.get_LDAPConnection() ); 

						oADGroup.RemoveUserFromGroup( groupDN, objectsToRemove );
					}
				} 
			}

			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{				
				oADGroup = null;

				base.Disconnect();
			}
		}

		#endregion

		#region Delete Group

		public void DeleteGroup( string groupName )
		{
			ADGroup		oADGroup		= null;

			try
			{
				oADGroup  = new ADGroup( base.get_LDAPConnection() );

				oADGroup.DeleteGroup( groupName );
			}
			catch (Exception ex)
			{
				throw ex;
			}			
			finally
			{
				oADGroup		= null;
				base.Disconnect();
			}			
		}

		#endregion

	}
}
