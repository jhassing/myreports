using System;

using Compass.Security.ActiveDirectory;

using Novell.Directory.Ldap;

namespace UserManagement.BLL
{
	/// <summary>
	/// Summary description for BaseClass.
	/// </summary>
	public class BaseClass
	{

		protected Authentication _authentication;
		protected LDAPConnection LdapConn;

		public enum enumSearchScope
		{
			SCOPE_BASE = 0,
			SCOPE_ONE = 1,
			SCOPE_SUB = 2
		}
		
		public BaseClass( Authentication oAuthentication )
		{
			_authentication = oAuthentication;
		}

        public BaseClass() { }

		protected LDAPConnection get_LDAPConnection()
		{
			try
			{
				LdapConn = new LDAPConnection( _authentication.User, 
					_authentication.Password,
					_authentication.LdapHost,
					_authentication.LdapPort );

				return LdapConn;
			}
			catch( Exception ex )
			{
				throw ex;
			}

																	
		}

		protected void Disconnect()
		{
			if ( LdapConn != null )
			{
				if ( LdapConn.Connected )
					LdapConn.Disconnect();
			}
		}

		protected string GetAttributeValue( LdapAttributeSet ldapAttribSet, string property )
		{
			try
			{
				LdapAttribute ldapAttrib = ldapAttribSet.getAttribute( property );

				if ( ldapAttrib == null )
					return "";

				return ldapAttribSet.getAttribute( property ).StringValue;
			}
			catch( Exception ex )
			{
				throw ex;
			}
		}

		protected string[] GetAttributeValueArray( LdapAttributeSet ldapAttribSet, string property )
		{

			try
			{
				LdapAttribute ldapAttrib = ldapAttribSet.getAttribute( property );

				if ( ldapAttrib == null )
					return null;

				return ldapAttribSet.getAttribute( property ).StringValueArray;
			}
			catch( Exception ex )
			{
				throw ex;
			}
		}
	}
}
