using System;
using System.Text;
using System.Collections.Generic;
using System.Collections; 
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices.ActiveDirectory;
using System.DirectoryServices;
using System.Web.UI.WebControls;

using Compass.Data.MSSQLDataAccess;
using Compass.Security.ActiveDirectory;

using Novell.Directory.Ldap;

using MyReports.Common;

namespace UserManagement.BLL
{
    public struct tUser
    {
        private string _dn;
        private ArrayList _userMembership;
        private string _personnelNumber;
        private string _homeFolder;
        private bool _actuateUser;
        private bool _budgetDistributionUser;
        private bool _blueBookDistributionUser;
        private bool _myFactsUser;
        private bool _passportUser;
        private bool _mupFinAdmin;
        private bool _mupPayAdmin;

        public tUser(string dn, ArrayList userMembership, string personnelNumber, string homeFolder,
                        bool actuateUser, bool budgetDistributionUser,
                        bool blueBookDistributionUser, 
                        bool myFactsUser, bool passportUser,
                        bool mupFinAdmin, bool mupPayAdmin)
        {
            this._dn = dn;
            this._userMembership = userMembership;
            this._personnelNumber = personnelNumber;
            this._homeFolder = homeFolder;
            this._actuateUser = actuateUser;
            this._budgetDistributionUser = budgetDistributionUser;
            this._blueBookDistributionUser = blueBookDistributionUser;
            this._myFactsUser = myFactsUser;
            this._passportUser = passportUser;
            this._mupFinAdmin = mupFinAdmin;
            this._mupPayAdmin = mupPayAdmin;
        }

        public string DN
        { get { return this._dn; } }

        public ArrayList UserMembership
        { get { return this._userMembership; } }

        public string PersonnelNumber
        { get { return this._personnelNumber; } }

        public string HomeFolder
        { get { return this._homeFolder; } }
            
        public bool ActuateUser
        { get { return this._actuateUser; } }

        public bool BudgetDistributionUser
        { get { return this._budgetDistributionUser; } }

        public bool BlueBookDistributionUser
        { get { return this._blueBookDistributionUser; } }

        public bool MyFactsUser
        { get { return this._myFactsUser; } }

        public bool PassportUser
        { get { return this._passportUser; } }

        public bool MupFinAdmin
        { get { return this._mupFinAdmin; } }

        public bool MupPayAdmin
        { get { return this._mupPayAdmin; } }

    }

    public class tUserListing
    {
        private string _dn;
        private string _cn;
        private string _samAccountName;
        private string _displayName;
        private string _personnelNumber;
        private string _firstName;
        private string _lastName;

        public tUserListing(string dn, string cn, string samAccountName, string displayName, string personnelNumber,
                        string firstName, string lastName)
        {
            this._dn = dn;
            this._cn = cn;
            this._samAccountName = samAccountName;
            this._displayName = displayName;
            this._personnelNumber = personnelNumber;
            this._firstName = firstName;
            this._lastName = lastName;
        }

        public string DN
        { get { return this._dn; } }

        public string CN
        { get { return this._cn; } }

        public string SamAccountName
        { get { return this._samAccountName; } }

        public string DisplayName
        { get { return this._displayName; } }

        public string PersonnelNumber
        { get { return this._personnelNumber; } }

        public string FirstName
        { get { return this._firstName; } }

        public string LastName
        { get { return this._lastName; } }
    }

	/// <summary>
	/// Static methods relating to user memberships, searching for users, ect
	/// </summary>
	public class Users : BaseClass
	{

		#region ~const

		public Users( Authentication oAuthentication ) 
			: base( oAuthentication )
		{		

		}

        public Users() : base() { }

		#endregion

		#region Add New User

		public string AddNewUser( string samAccountName, 
									string password, 
									Hashtable ht, 
									string searchBase,
									string sOU_UserContainer, 
									bool enableAccount, 
									bool forceUserResetPWDnextLogin, 
									bool setPasswordNeverExpire, 
									bool overrideExistingUserInfo )
		{
			IUserObject	oADUser = null;

			try
			{
				oADUser = new ADUserObject( base.get_LDAPConnection() );

				return oADUser.AddUser( samAccountName, password, ht, searchBase, sOU_UserContainer, enableAccount, forceUserResetPWDnextLogin, setPasswordNeverExpire, overrideExistingUserInfo);	

			}
			catch ( Exception ex )
			{
				throw ex;
			}
			finally
			{
				oADUser = null;
				base.Disconnect();
			}
		}

		#endregion

		#region DoesUserExist

		public bool DoesUserExist( string personnelNumber, string searchBase )
		{
			personnelNumber = personnelNumber.Trim();

			IUserObject	oADUser = null;

			try
			{
				oADUser = new ADUserObject( base.get_LDAPConnection() );

				return oADUser.DoesUserExist( personnelNumber, searchBase );
			}			
			catch ( Exception ex )
			{
				throw ex;
			}
			finally
			{
				oADUser = null;
				base.Disconnect();
			}

		}
		
		#endregion

        #region Find Users Return ArrayList of Sam Account Names
        /// <summary>
        /// Method will search Users for Ambiguous Name Resolution 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns>Arraylist ready to bind to a Listbox</returns>
        public ArrayList GetUserListingForSearch(string filter, string OU, int searhScope, int maxResults)
        {
            ArrayList aryReturn = new ArrayList();

            try
            {
                ArrayList ary = new ArrayList();

                // Execute the search 
                List<tUserListing> retUserListing = GetUsersBasedOnANR(filter, OU);// , searhScope, maxResults);

                foreach (tUserListing user in retUserListing)
                {

                    string sam = user.SamAccountName;
                    string firstName = user.FirstName;
                    string lastName = user.LastName;
                    string DN = user.DN;

                    string tmp = sam + " - " + firstName + " " + lastName;

                    aryReturn.Add(new ListItem(tmp, DN));
                }

                return aryReturn;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

		#region Return the Users group Membership

		public ArrayList UsersGroupMembership(ArrayList userMembership )
		{
			ArrayList aryReturn = new ArrayList();
			
			try
			{

                foreach (LdapAttributeSet ldapAttribSet in userMembership)
				{

					string cn = base.GetAttributeValue( ldapAttribSet, "cn" );
					string dn = base.GetAttributeValue( ldapAttribSet, "distinguishedName" );

					// See if this user meets the business logic requierments of Report Distribution group membership
					if (	( cn.ToUpper().Trim().IndexOf(AppValues.FinancialGroups) > -1 ) || 
						( cn.ToUpper().Trim().IndexOf(AppValues.PayrollHourlyGroups) > -1 ) || 
						( cn.ToUpper().Trim().IndexOf(AppValues.PayrollSalaryGroups) > -1 ) )
					{
						ListItem at = new ListItem(	cn, dn );

						aryReturn.Add( at );
					}					
				}

				return  aryReturn;

			}
			catch(Exception ex)
			{
				throw ex;
			}   			
			finally
			{
				//oADUser = null;

				base.Disconnect();
			}
		}
		#endregion

		#region Edit User Group Membership


		/// <summary>
		/// Sets the new users group listing, and returns curring listing
		/// </summary>
		/// <param name="userDN"></param>
		/// <param name="groupsToAdd"></param>
		/// <param name="groupsToRemove"></param>
		/// <returns></returns>
		public ArrayList EditUserMembership( string userDN, ArrayList groupsToAdd, ArrayList groupsToRemove )
		{
			IUserObject	oADUser = null;

			try
			{	
				oADUser = new ADUserObject( base.get_LDAPConnection() );

				if ( groupsToAdd != null )
				{
					if ( groupsToAdd.Count > 0 )
					{
						oADUser = new ADUserObject( base.get_LDAPConnection() );

						oADUser.AddUserToGroup( userDN, groupsToAdd );
					}
				}

				if ( groupsToRemove != null )
				{
					if ( groupsToRemove.Count > 0 )
					{
						oADUser = new ADUserObject( base.get_LDAPConnection() );

						oADUser.RemoveUserFromGroup( userDN, groupsToRemove );
					}
				}
				
				ArrayList ary = new ArrayList();
				ary.Add( "distinguishedName" );
				ary.Add( "cn" );

				ArrayList aryReturn = new ArrayList();
				ArrayList aryCurrentGroups = oADUser.GetUsersMembership( userDN, ary );

				foreach( LdapAttributeSet ldapAttribSet in aryCurrentGroups )
				{

					string cn = base.GetAttributeValue( ldapAttribSet, "cn" );
					string dn = base.GetAttributeValue( ldapAttribSet, "distinguishedName" );

					// See if this user meets the business logic requierments of Report Distribution group membership
					if (	( cn.ToUpper().Trim().IndexOf(AppValues.FinancialGroups) > -1 ) || 
						( cn.ToUpper().Trim().IndexOf(AppValues.PayrollHourlyGroups) > -1 ) || 
						( cn.ToUpper().Trim().IndexOf(AppValues.PayrollSalaryGroups) > -1 ) )
					{
						ListItem at = new ListItem(	cn, dn );

						aryReturn.Add( at );
					}					
				}

				return  aryReturn;



			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				oADUser = null;
				base.Disconnect();
			}

		}


		#endregion
	
		#region Find Users based on ANR and Return tObjects
		/// <summary>
		/// Method will search Users for Ambiguous Name Resolution 
		/// </summary>
		/// <param name="filter"></param>
		/// <returns></returns>		
        public List<tUserListing> GetUsersBasedOnANR(string filter, string OU)
        {
 			Hashtable	ht				=	new Hashtable();
			ArrayList	aryParams		=	new ArrayList();
			
            List<tUserListing> retUserListing = new List<tUserListing>();

            DirectoryEntry deRoot = null;
            DirectorySearcher myDirectorySearcher = null;
            SearchResultCollection mySearchResult = null;

			try
			{

                deRoot = new DirectoryEntry("LDAP://" + AppValues.FullyQualifiedDomain,
                                        AppValues.ServiceAccountUser,
                                        AppValues.ServiceAccountPassword,
                                        AuthenticationTypes.Secure);

                #region Setup Search

                myDirectorySearcher = new DirectorySearcher(deRoot);
                myDirectorySearcher.PropertyNamesOnly = false;
                myDirectorySearcher.PropertiesToLoad.Add("samAccountName");
                myDirectorySearcher.PropertiesToLoad.Add("distinguishedName");
                myDirectorySearcher.PropertiesToLoad.Add("sn");
                myDirectorySearcher.PropertiesToLoad.Add("givenName");
                myDirectorySearcher.PropertiesToLoad.Add("cn");
                myDirectorySearcher.PropertiesToLoad.Add("displayName");
                myDirectorySearcher.PropertiesToLoad.Add("perno");
                myDirectorySearcher.SearchScope = SearchScope.Subtree;
                myDirectorySearcher.Filter = "(&(objectCategory=user)(|(anr=" + filter + "*)(perno=" + filter + "*)))";

                myDirectorySearcher.SizeLimit = 500;
                myDirectorySearcher.PageSize = 10;


                // Get the first entry of the search.
                mySearchResult = myDirectorySearcher.FindAll();

                #endregion

                if (mySearchResult != null)
                {
                    foreach (SearchResult sr in mySearchResult)
                    {
                        string samAccountName = getPropertyValue(sr, "samAccountName").Trim().Replace("'", " ");

                        // First, check to see if this is a phone number accout.. if so, do not display
                        if ((Compass.Common.Functions.IsNumeric(samAccountName) && samAccountName.Length == 10) || (samAccountName.Length == 0))
                        {
                        }
                        else
                        {
                            string distinguishedName = getPropertyValue(sr, "distinguishedName");
                            string displayName = getPropertyValue(sr, "displayName");
                            string givenName = getPropertyValue(sr, "givenName");
                            string cn = getPropertyValue(sr, "cn").Trim();
                            string sn = getPropertyValue(sr, "sn").Trim();
                            string perno = getPropertyValue(sr, "perno").Trim();

                            retUserListing.Add(new tUserListing(distinguishedName, cn, samAccountName, displayName, perno, givenName, sn));
                        }
                    }
                }

                return retUserListing;
											
			}
			catch (Exception ex)
			{
				throw ex;
			}
            finally
            {
                #region Clean Up

                if (deRoot != null)
                {
                    deRoot.Close();
                    deRoot.Dispose();
                    deRoot = null;                  
                }

                if (myDirectorySearcher != null)
                {
                    myDirectorySearcher.Dispose();
                    myDirectorySearcher = null;
                }

                if (mySearchResult != null)
                {
                    mySearchResult.Dispose();
                    mySearchResult = null;
                }

                #endregion

            }
		}

		#endregion

		#region Get User Actuate Properties

        public tUser GetUserProperties(string distinguishedName)
		{
			
			ArrayList htProperties = new ArrayList();
			IUserObject oADUser			=	null;

            bool budgetDistributionUser = false;
            bool blueBookDistributionUser = false;
            bool myFactsUser = false;
            bool actuateUser = false;
            bool passportUser = false;
            bool mypFinAdmin = false;
            bool mupPayAdmin = false;
			
			htProperties.Add( "actuateRDHomeFolder" );
			htProperties.Add( "actuateUser" );
			htProperties.Add( "samAccountName" );

            ArrayList ary = new ArrayList();
            ary.Add("distinguishedName");
            ary.Add("cn");
        
			try
			{

				oADUser = new ADUserObject( base.get_LDAPConnection() );

				LdapAttributeSet ldapAttribSet =oADUser.GetUserProperties( distinguishedName, htProperties );
                ArrayList userMembership = oADUser.GetUsersMembership(distinguishedName, ary);

                // Is this user a MyReports User?
				string tmp = base.GetAttributeValue( ldapAttribSet, "actuateUser" );
				actuateUser = tmp != "" ? System.Convert.ToBoolean( tmp ) : false;

				// Get the users group membership									
                foreach (LdapAttributeSet ldapAttribSetUM in userMembership)
                {

                    string cn = base.GetAttributeValue(ldapAttribSetUM, "cn").Trim().ToLower();

					// Is this user a Budget Distribution, passport or BlueBookDistribution user?
                    if (cn.Equals("budgetdistributionusers"))
                        budgetDistributionUser = true;

                    if (cn.Equals("passportusers"))
                        passportUser = true;

                    if (cn.Equals("bluebookdistributionusers"))
                        blueBookDistributionUser = true;
                    
                    if (cn.Equals("myfactsusers"))
                        myFactsUser = true;

                    if (cn.Equals("myunitportalfinancialusersadmin"))
                        mypFinAdmin = true;

                    if (cn.Equals("myunitportalpayrollusersadmin"))
						mupPayAdmin = true;


				}

                return new tUser(distinguishedName,
                                            userMembership,
                                            base.GetAttributeValue(ldapAttribSet, "samAccountName"),
                                            base.GetAttributeValue(ldapAttribSet, "actuateRDHomeFolder"),
                                            actuateUser,
                                            budgetDistributionUser,
                                            blueBookDistributionUser,
                                            myFactsUser,
                                            passportUser,
                                            mypFinAdmin,
                                            mupPayAdmin);


			}
			catch(Exception ex)
			{
				throw new Exception("Error getting user properties", ex);
			}
		}

		#endregion

		#region Save MyReports Access

		public void SetMyReportsAccess( string distinguishedName, bool allow )
		{
			IUserObject	oADUser = null;

			try
			{
				oADUser = new ADUserObject( base.get_LDAPConnection() );

				oADUser.SetUserProperties( distinguishedName, "actuateUser", allow.ToString().ToUpper() );
			}
			catch (Exception ex)
			{
				throw ex;
			}  			
			finally
			{
				oADUser = null;
				base.Disconnect();
			}

		}

		#endregion

		#region Set Budget Distrbution Access

		public void SetBudgetDistributionAccess( string distinguishedName, string searchBase, bool allow )
		{	 
			IUserObject	oADUser = null;

			try
			{
				oADUser = new ADUserObject( base.get_LDAPConnection() );

				string DN = oADUser.GetObjectsDistinguishedName( "BudgetDistributionUsers", searchBase ); 

				if (allow)
				{
					// add this user to the BudgetDistributionUsers group
					oADUser.AddUserToGroup( distinguishedName, DN );
				}
				else
				{
					// Remove this user from the BudgetDistributionUsers group
					oADUser.RemoveUserFromGroup( distinguishedName, DN );

				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				oADUser = null;
				base.Disconnect();
			}
		}  


		#endregion

		#region Set Passport Access

		public void SetPassportAccess( string distinguishedName, string searchBase, bool allow )
		{
			IUserObject	oADUser = null;

			try
			{

				oADUser = new ADUserObject( base.get_LDAPConnection() );
				
				string DN = oADUser.GetObjectsDistinguishedName( "PassportUsers", searchBase ); 

				if (allow)
				{
					// add this user to the BudgetDistributionUsers group
					oADUser.AddUserToGroup( distinguishedName, DN );
				}
				else
				{
					// Remove this user from the BudgetDistributionUsers group
					oADUser.RemoveUserFromGroup( distinguishedName, DN );

				}
			}
			catch(Exception ex)
			{
				throw ex;
			}  			
			finally
			{
				oADUser = null;
				base.Disconnect();
			}
		}  


		#endregion

		#region Set Blue Book Distribution Access

		public void SetBlueBookDistributionAccess( string distinguishedName, string searchBase, bool allow )
		{
			IUserObject	oADUser = null;

			try
			{

				oADUser = new ADUserObject( base.get_LDAPConnection() );

				string DN = oADUser.GetObjectsDistinguishedName( "BlueBookDistributionUsers", searchBase ); 

				if (allow)
				{
					// add this user to the BudgetDistributionUsers group
					oADUser.AddUserToGroup( distinguishedName, DN );
				}
				else
				{
					// Remove this user from the BudgetDistributionUsers group
					oADUser.RemoveUserFromGroup( distinguishedName, DN );

				}
			}
			catch(Exception ex)
			{
				throw ex;
			}
			finally
			{
				oADUser = null;
				base.Disconnect();
			}
		}  


		#endregion

        #region Set MyFacts Access

        public void SetMyFactsAccess(string distinguishedName, string searchBase, bool allow)
        {
            IUserObject oADUser = null;

            try
            {

                oADUser = new ADUserObject(base.get_LDAPConnection());

                string DN = oADUser.GetObjectsDistinguishedName("MyFactsUsers", searchBase);

                if (allow)
                {
                    // add this user to the BudgetDistributionUsers group
                    oADUser.AddUserToGroup(distinguishedName, DN);
                }
                else
                {
                    // Remove this user from the BudgetDistributionUsers group
                    oADUser.RemoveUserFromGroup(distinguishedName, DN);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oADUser = null;
                base.Disconnect();
            }
        }


        #endregion

        #region Set Mup Fin Access

        public void SetMupFinAdmin(string distinguishedName, string searchBase, bool allow)
        {
            IUserObject oADUser = null;

            try
            {

                oADUser = new ADUserObject(base.get_LDAPConnection());

                string DN = oADUser.GetObjectsDistinguishedName("MyUnitPortalFinancialUsersAdmin", searchBase);

                if (allow)
                {
                    // add this user to the BudgetDistributionUsers group
                    oADUser.AddUserToGroup(distinguishedName, DN);
                }
                else
                {
                    // Remove this user from the BudgetDistributionUsers group
                    oADUser.RemoveUserFromGroup(distinguishedName, DN);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oADUser = null;
                base.Disconnect();
            }
        }


        #endregion

        #region Set Mup Pay Access

        public void SetMupPayAdmin(string distinguishedName, string searchBase, bool allow)
        {
            IUserObject oADUser = null;

            try
            {

                oADUser = new ADUserObject(base.get_LDAPConnection());

                string DN = oADUser.GetObjectsDistinguishedName("MyUnitPortalPayrollUsersAdmin", searchBase);

                if (allow)
                {
                    // add this user to the BudgetDistributionUsers group
                    oADUser.AddUserToGroup(distinguishedName, DN);
                }
                else
                {
                    // Remove this user from the BudgetDistributionUsers group
                    oADUser.RemoveUserFromGroup(distinguishedName, DN);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oADUser = null;
                base.Disconnect();
            }
        }


        #endregion

		#region Set New Home Folder in AD

		/// <summary>
		/// Method will edit the users Home folder attribute in AD
		/// </summary>
		/// <param name="distinguishedName">Distinguished Name of the User</param>
		/// <param name="homeFolder">New home folder to set</param>
		public void SetNewHomeFolder( string distinguishedName, string homeFolder )
		{
			IUserObject	oADUser = null;

			try
			{		
				oADUser = new ADUserObject( base.get_LDAPConnection() );

				oADUser.SetUserProperties( distinguishedName, "actuateRDHomeFolder", homeFolder );
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				oADUser = null;
				base.Disconnect();
			}
		}

		#endregion	

		#region Save New Users Group Membership

		public void SaveNewUserGroupMembership( string samAccountName, ArrayList groups )
		{
			ArrayList aryGroups = new ArrayList();

			IUserObject	oADUser = null;

			try
			{
//				
//				string usersDN = this._oADUser.GetObjectsDistinguishedName( samAccountName );
//
//				if ( usersDN.Length > 0 )
//				{
//
//					foreach ( string group in groups )
//					{
//						string groupDN = this._oADUser.GetObjectsDistinguishedName( group );
//
//						if ( groupDN.Length > 0 )
//						{
//							aryGroups.Add( groupDN );	
//						}
//
//					}
//
//					this._oADUser.AddUserToGroup( usersDN, aryGroups );
//				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		#endregion

		#region Verify User return Groups and Application access

		public LoginResult VerifyUser( string userId, string password, ref string distinguishedName, ref ArrayList groupListing )
		{
						
			IUserObject	oADUser = null;

			try
			{			
				oADUser = new ADUserObject( base.get_LDAPConnection() );
			
				LoginResult lr = oADUser.VerifyUserLogin( userId, password, "dc=na,dc=compassGroup,dc=corp", ref distinguishedName );  

				if ( lr == LoginResult.LOGIN_OK )
					groupListing = oADUser.GetUsersMembership( distinguishedName, false );

				return lr;

			}
			catch ( Exception ex )
			{
				throw ex;
			}
			finally
			{
				oADUser = null;
				base.Disconnect();
			}
		}

		#endregion


        private string getPropertyValue(SearchResult sr, string propertyName)
        {

            if (sr.Properties[propertyName].Count != 0)
            {
                // Property has atleast one value.
                return sr.Properties[propertyName][0].ToString();
            }
            else
                return "";
        }

	}
}
