using System;
using System.Text;
using System.Collections; 
using System.Data;
using System.Data.SqlClient;

using Compass.Data.MSSQLDataAccess;
using Compass.Security.ActiveDirectory;

using MyUnitPortal.Common;

namespace UserManagement.BLL
{

	public class General
	{
		public static ArrayList GetUserPropertiesFromTheDomain( ArrayList arySamAccountNames )
		{

			ArrayList aryReturn = new ArrayList();

			// Loop through each user, locate in the domain, and get their first and last name
			for (int x=0;x<arySamAccountNames.Count;x++)
			{

				Hashtable	htParams = new Hashtable();

				htParams.Add( "samAccountName", "" );
				htParams.Add( "sn", "" );
				htParams.Add( "givenName", "" );

				if ( MyUnitPortal.Common.Functions.getADUser().GetUserProperties( arySamAccountNames[x].ToString(), ref htParams ) )
					aryReturn.Add( htParams["samAccountName"].ToString() + "  -  " + htParams["sn"].ToString() + ", " + htParams["givenName"].ToString() );

			}
	
			return aryReturn;
		}
	}
}
