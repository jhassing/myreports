using System;
using System.Text;
using System.Collections; 
using System.Data;
using System.Data.SqlClient;

using Compass.Data.MSSQLDataAccess;
using Compass.Security.ActiveDirectory;

using MyReports.Common;

namespace UserManagement.BLL
{

	public class GroupMembershipBLL
	{
		private		string			_connectionString = "";
		private		ArrayList		_userMembership = null;
		private		string			_personnelNumber = "";
		private		IUserObject		_activeDirectoryHelper = null;

		#region Properties

		public string PersonnelNumber
		{
			get
			{
				return _personnelNumber;
			}
		}



		public ArrayList UserMembership
		{
			get
			{
				return _userMembership;
			}
		}



		#endregion

		#region ~ctor

		public GroupMembershipBLL(string connectionString)
		{
			_connectionString = connectionString;
		}

		public GroupMembershipBLL( Authentication oAuthentication ) 
			//: base( oAuthentication )
		{

		}



		~GroupMembershipBLL()
		{
			_connectionString = null;
			_activeDirectoryHelper = null;
		}



		#endregion

		#region Return an ArrayList of available home folders this user could me a member of based on group membership
		
		/// <summary>
		/// Method will loop through the users current group membership and gather a list of available home folders
		/// </summary>
		/// <param name="aryUsersGroupMembership">Current listing of the users group membership</param>
		/// <returns>ArrayList of possible home folders</returns>
		public ArrayList getHomeFolderListing( ArrayList aryUsersGroupMembership )
		{
			ArrayList aryRet = new ArrayList();
			SqlConnection conn = null;
			SqlDataReader dr = null;
			bool foundOne = false;

			try
			{
				if (aryUsersGroupMembership.Count < 1)
					return(aryUsersGroupMembership);

				// Build the where statement
				StringBuilder sb = new StringBuilder();

				foreach(  System.Web.UI.WebControls.ListItem item in aryUsersGroupMembership )
				{
					string s = item.Text;

					if (s.ToUpper().IndexOf(AppValues.FinancialGroups) > 0)
					{
						string su = s.Substring(0,s.Length-4);

						sb.Append( su + "|" );

						foundOne = true;
					}
				}

				if (!foundOne)
					return aryRet;

				SqlParameter [] arParms = new SqlParameter[1];

				arParms[0] = new SqlParameter( "@as_UserGroupMembership", SqlDbType.VarChar, 1000 );
				arParms[0].Value = sb.ToString();

				dr = SqlHelper.ExecuteReader( _connectionString, CommandType.StoredProcedure, "usp_Select_Home_Folders", arParms );	
				
				while (dr.Read())
				{
					string sLevelFolder = "";

					string sFolderCode = dr["FolderCode"].ToString();
					string sFolderName = dr["FolderName"].ToString();			

					bool isAllNumbers = true;
					// Loop through the sFolderCode
					// if it is all numbers, then it is an operations code

					char[] tt = sFolderCode.ToCharArray();

					foreach(char t in tt)
					{
						if ((t < '0') || (t > '9'))
							isAllNumbers = false;
					}

					if (isAllNumbers)
					{
						//String equales int, must be a operations code
						sLevelFolder = "/(3) Operations/";
						sFolderCode = sFolderCode.PadLeft(6, '0');

					}
					else if(sFolderCode.Length == 3)	// Must be a Region						
						sLevelFolder = "/(1) Accounting/";

					else	// Must be a district
						sLevelFolder = "/(2) District/";

					string sActuateFolder = sLevelFolder + sFolderCode + " - " + sFolderName;

					aryRet.Add(sActuateFolder);
				}

				dr.Close();
				aryRet.Sort();

				return aryRet;
			}
			catch(Exception ex)
			{
				throw (ex);
			}
			finally
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}

				conn = null;			
				dr = null;
			}
		}
		#endregion

	}
}
