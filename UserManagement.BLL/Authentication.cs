using System;

namespace UserManagement.BLL
{
	public struct Authentication
	{

		// Port the domain is on
		private		    int				_ldapPort;

		// Domain user service account info
		private			string			_user;
		private			string			_password;
		
		// Domain and base OU info
		private			string			_ldapHost;

		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="DomainUser">Domain user with read-write domain access.</param>
		/// <param name="DomainPassword">Password for the domain user.</param>
		/// <param name="ldapHost">Name of the domain to connecto to.</param>
		/// <param name="ldapPort">Port the LDAP server is on.</param>
		public Authentication( string User, string Password, string ldapHost, int ldapPort  )
		{
			_ldapHost				= ldapHost;					
			_ldapPort				= ldapPort;

			_user					= User;
			_password				= Password; 
		}

		public string User
		{
			get
			{
				return this._user;
			}
		}

		public string Password
		{
			get
			{
				return this._password;
			}
		}

		public string LdapHost
		{
			get
			{
				return this._ldapHost;
			}
		}

		public int LdapPort
		{
			get
			{
				return this._ldapPort;
			}
		}	  

	}
}
