using System;
using System.Text;
using System.Collections; 
using System.Data;
using System.Data.SqlClient;

using Compass.Data.MSSQLDataAccess;
using Compass.Security.ActiveDirectory;

namespace UserManagement.BLL
{
	#region StructNewUsers
	[Serializable]
	public struct StructNewUsers
	{
		private Guid _MasterID;
		private DateTime _RequestedDate;
		private int _BeenImported;
		private DateTime _ImportedDate;
		private string _samAccountName;
		private string _RequestorsFirstName;
		private string _RequestorsLastName;
		private string _RequestorsEmail;
		private string _RequestorsPhone;
		private string _RequestorsDepartment;

		public StructNewUsers(	Guid MasterID, 
			DateTime RequestedDate,
			int BeenImported,
			DateTime ImportedDate,
			string samAccountName,
			string RequestorsFirstName,
			string RequestorsLastName,
			string RequestorsEmail,
			string RequestorsPhone,
			string RequestorsDepartment )
		{
			_MasterID = MasterID;
			_RequestedDate = RequestedDate;
			_BeenImported = BeenImported;
			_ImportedDate = ImportedDate;
			_samAccountName = samAccountName;
			_RequestorsFirstName = RequestorsFirstName;
			_RequestorsLastName = RequestorsLastName;
			_RequestorsEmail = RequestorsEmail;
			_RequestorsPhone = RequestorsPhone;
			_RequestorsDepartment = RequestorsDepartment;
		}

		public Guid MasterID
		{
			get
			{
				return _MasterID;
			}
		}

		public DateTime RequestedDate
		{
			get
			{
				return _RequestedDate;
			}
		}
		public int BeenImported
		{
			get
			{
				return _BeenImported;
			}
		}
		public DateTime ImportedDate
		{
			get
			{
				return _ImportedDate;
			}
		}


		public string samAccountName
		{
			get
			{
				return _samAccountName;
			}
		}

		public string RequestorsFirstName
		{
			get
			{
				return _RequestorsFirstName;
			}
		}

		public string RequestorsLastName
		{
			get
			{
				return _RequestorsLastName;
			}
		}


		public string RequestorsEmail
		{
			get
			{
				return _RequestorsEmail;
			}
		}
		public string RequestorsPhone
		{
			get
			{
				return _RequestorsPhone;
			}
		}
		public string RequestorsDepartment
		{
			get
			{
				return _RequestorsDepartment;
			}
		}
	}
	#endregion

	#region StructNewUserDetails
	public struct StructNewUserDetails
	{
		private string _attributeName;
		private string _attributeValue;

		public StructNewUserDetails(string attributeName, string attributeValue)
		{
			_attributeName = attributeName;
			_attributeValue = attributeValue;
		}

		public string AttributeName
		{
			get
			{
				return _attributeName;
			}
		}

		public string AttributeValue
		{
			get
			{
				return _attributeValue;
			}
		}
	}
	#endregion

	#region Sort Classes
	public class NewUser_SORT_BY_SamAccountName : IComparer  
	{						
		int IComparer.Compare( Object x, Object y )  
		{			
			return( (new CaseInsensitiveComparer()).Compare( ((StructNewUsers)y).samAccountName, ((StructNewUsers)x).samAccountName ) );
		}
	}

	public class NewUser_SORT_BY_ImportedDate : IComparer  
	{						
		int IComparer.Compare( Object x, Object y )  
		{			
			return( (new CaseInsensitiveComparer()).Compare( ((StructNewUsers)y).ImportedDate, ((StructNewUsers)x).ImportedDate ) );
		}
	}

	public class NewUser_SORT_BY_RequestedDate : IComparer  
	{						
		int IComparer.Compare( Object x, Object y )  
		{			
			return( (new CaseInsensitiveComparer()).Compare( ((StructNewUsers)y).RequestedDate, ((StructNewUsers)x).RequestedDate ) );
		}
	}

	public class NewUser_SORT_BY_RequestorsEmail : IComparer  
	{						
		int IComparer.Compare( Object x, Object y )  
		{			
			return( (new CaseInsensitiveComparer()).Compare( ((StructNewUsers)y).RequestorsEmail, ((StructNewUsers)x).RequestorsEmail ) );
		}
	}

	public class NewUser_SORT_BY_RequestorsPhone : IComparer  
	{						
		int IComparer.Compare( Object x, Object y )  
		{			
			return( (new CaseInsensitiveComparer()).Compare( ((StructNewUsers)y).RequestorsPhone, ((StructNewUsers)x).RequestorsPhone ) );
		}
	}


	#endregion

	public class NewUserBLL
	{
		private string _connectionString = "";
		private Authentication _authentication;


		#region ~ctor
		
		public NewUserBLL( string connectionString, Authentication oAuthentication )
		{
			_connectionString = connectionString;
			_authentication = oAuthentication;

		}

		#endregion

		#region InsertUserInfoInoSQL
		public void InsertUserInfoInoSQL(		string samAccountName, 
												string password, 
												string groups,
												string requestorsFirstName, 
												string requestorsLastName, 
												string requestorsEmail, 
												string requestorsPhoneNumber, 
												string requestorsDepartment,										
												Hashtable htValues )
		{
			// First, insert a master record, and get that GUID
			Guid ID = InsertMasterRecord(	samAccountName, 
											password,	
											MyReports.Common.AppValues.UserName,
											requestorsFirstName, 
											requestorsLastName, 
											requestorsEmail, 
											requestorsPhoneNumber, 
											requestorsDepartment);

			// Next insert the detail records
			InsertDetailRecords_IntoSQL(ID, htValues);

			//Insert the groups into the database
			InsertGroupRecords_IntoSQL(ID, groups);

		}
		#endregion
		
		#region Get Sam Account Name FromSQL
		public string GetSamAccountName(Guid PK_ID)
		{
			SqlConnection conn = null;
			SqlParameter[] sqlParms = null;

			try
			{
				conn = new SqlConnection(_connectionString);

				sqlParms= new SqlParameter[2];

				sqlParms[0] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
				sqlParms[0].Value = PK_ID;

				sqlParms[1] = new SqlParameter("@av_samAccountName", SqlDbType.VarChar, 30);
				sqlParms[1].Direction = ParameterDirection.Output;
		
				SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_Select_samAccountName_From_Master", sqlParms);
				
				return sqlParms[1].Value.ToString();
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}

				conn = null;
				sqlParms = null;		
			}
		}
		#endregion

		#region Get Requestors Information FromSQL
		public void GetRequestorsInformation_From_Master(Guid PK_ID, out string RequestorsID, out string RequestorsEmail,
															out string RequestorsFirstName,
															out string RequestorsLastName,
															out DateTime RequestedDate)
		{

			SqlConnection conn = null;
			SqlParameter[] sqlParms = null;

			try
			{
				conn = new SqlConnection(_connectionString);

				sqlParms = new SqlParameter[6];

				sqlParms[0] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
				sqlParms[0].Value = PK_ID;

				sqlParms[1] = new SqlParameter("@av_RequestorsID", SqlDbType.VarChar, 20);
				sqlParms[1].Direction = ParameterDirection.Output;

				sqlParms[2] = new SqlParameter("@av_RequestorsEmail", SqlDbType.VarChar, 255);
				sqlParms[2].Direction = ParameterDirection.Output;

				sqlParms[3] = new SqlParameter("@av_RequestorsFirstName", SqlDbType.VarChar, 150);
				sqlParms[3].Direction = ParameterDirection.Output;
			
				sqlParms[4] = new SqlParameter("@av_RequestorsLastName", SqlDbType.VarChar, 150);
				sqlParms[4].Direction = ParameterDirection.Output;

				sqlParms[5] = new SqlParameter("@ad_RequestedDate", SqlDbType.DateTime);
				sqlParms[5].Direction = ParameterDirection.Output;				

				SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_Select_RequestorsInfo_From_Master", sqlParms);
				
				RequestorsID = sqlParms[1].Value.ToString();
				RequestorsEmail = sqlParms[2].Value.ToString();
				RequestorsFirstName = sqlParms[3].Value.ToString();
				RequestorsLastName = sqlParms[4].Value.ToString();
				RequestedDate = (DateTime)sqlParms[5].Value;

			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}

				conn = null;
				sqlParms = null;		
			}
		}

		#endregion

		#region Get Last Modified By FromSQL
		public void GetLastModifiedBy(Guid PK_ID, out string userName, out DateTime dtImportedDate)
		{
		
			SqlConnection conn = null;
			SqlParameter[] sqlParms= null;

			try
			{
				conn = new SqlConnection(_connectionString);

				sqlParms = new SqlParameter[3];

				sqlParms[0] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
				sqlParms[0].Value = PK_ID;

				sqlParms[1] = new SqlParameter("@av_LastModifiedBy", SqlDbType.VarChar, 150);
				sqlParms[1].Direction = ParameterDirection.Output;		
			
				sqlParms[2] = new SqlParameter("@ad_ImportedDateTime", SqlDbType.DateTime);
				sqlParms[2].Direction = ParameterDirection.Output;
						
				SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_Select_LastModifiedBy_Master", sqlParms);
				
				userName = sqlParms[1].Value.ToString();
				dtImportedDate = System.Convert.ToDateTime(sqlParms[2].Value);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}

				conn = null;
				sqlParms = null;		
			}
		}
		#endregion

		#region Get Rejects Reason FromSQL
		public SqlDataReader GetRejectReasons(Guid PK_ID)
		{
			SqlParameter[] sqlParms= null;

			try
			{
				sqlParms = new SqlParameter[1];

				sqlParms[0] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
				sqlParms[0].Value = PK_ID;

				SqlDataReader dr = SqlHelper.ExecuteReader(this._connectionString, CommandType.StoredProcedure, "usp_Select_RejectReason", sqlParms);
						

				return dr;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				sqlParms = null;		
			}
		}
		#endregion

		#region GetMasterRecordListing

		public ArrayList GetMasterRecordListing(int beenImported, string requestorsID, DateTime startDateTime, DateTime endDateTime)
		{
			SqlConnection conn = null;
			SqlParameter[] sqlParms= null;

			ArrayList ary = new ArrayList();

			try
			{
				conn = new SqlConnection(_connectionString);

				sqlParms = new SqlParameter[4];

				sqlParms[0] = new SqlParameter("@ai_beenImported", SqlDbType.Int);
				sqlParms[0].Value = beenImported;

				sqlParms[1] = new SqlParameter("@av_RequestorID", SqlDbType.VarChar, 100);
				sqlParms[1].Value = requestorsID;

				sqlParms[2] = new SqlParameter("@ad_StartDateTime", SqlDbType.DateTime);
				sqlParms[2].Value = startDateTime;

				sqlParms[3] = new SqlParameter("@ad_EndDateTime", SqlDbType.DateTime);
				sqlParms[3].Value = endDateTime;
		
				SqlDataReader dr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "usp_Select_Master", sqlParms);

				if (dr.HasRows )
				{
					while (dr.Read())
					{
						ary.Add(new StructNewUsers ( (	Guid)dr["MasterID"],
							(DateTime)dr["RequestedDateTime"],
							(int)dr["BeenImported"],
							(DateTime)dr["ImportedDateTime"],
							dr["samAccountName"].ToString(),
							dr["RequestorsFirstName"].ToString(),
							dr["RequestorsLastName"].ToString(),
							dr["RequestorsEmail"].ToString(),
							dr["RequestorsPhone"].ToString(),
							dr["RequestorsDepartment"].ToString() ) );
														
					}
				}

				dr.Close();

				return ary;

			}			
			catch (Exception ex)
			{
				throw ex;
			}
			finally 
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}

				conn = null;
				sqlParms = null;
			}		
		}
		#endregion

		#region GetDetailRecords
		public ArrayList GetDetailRecords(Guid PK_ID)
		{

			ArrayList ary = new ArrayList();

			SqlConnection conn = null;
			SqlParameter[] sqlParms= null;

			try
			{
				conn = new SqlConnection(_connectionString);

				sqlParms = new SqlParameter[1];

				sqlParms[0] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
				sqlParms[0].Value = PK_ID;

				SqlDataReader dr = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "usp_Select_Details", sqlParms);

				if (dr.HasRows )
				{
					while (dr.Read())
						ary.Add(new StructNewUserDetails(MyReports.Common.Functions.ConvertFromBytes(dr["AttributeName"]), MyReports.Common.Functions.ConvertFromBytes(dr["AttributeValue"])));
				}

				dr.Close();

				return ary;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally 
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}

				conn = null;
				sqlParms = null;
			}				
		}
		#endregion	
		
		#region Get New Users Group Membership
		public DataSet GetNewUserGroupMembership(Guid PK_ID)
		{
			SqlParameter[] sqlGroupParms = new SqlParameter[1];	

			try
			{					
				sqlGroupParms[0] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
				sqlGroupParms[0].Value = PK_ID;

				return SqlHelper.ExecuteDataset(this._connectionString, CommandType.StoredProcedure, "usp_Select_Groups", sqlGroupParms);
				
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally 
			{
				sqlGroupParms = null;
			}
		}
		#endregion

		#region Insert Master Record_IntoSQL

		private Guid InsertMasterRecord(	string samAccountName, 
											string password, 
											string requestorsID,
											string requestorsFirstName, 
											string requestorsLastName, 
											string requestorsEmail, 
											string requestorsPhoneNumber, 
											string requestorsDepartment )
		{
			
			SqlConnection conn = null;
			SqlTransaction myTrans = null;

			SqlParameter[] sqlParms= null;

			try
			{
				conn = new SqlConnection(_connectionString);
				conn.Open();

				myTrans = conn.BeginTransaction("Data Import");
		
				sqlParms = new SqlParameter[9];

				// SamAccountName
				sqlParms[0] = new SqlParameter("@av_samAccountName", SqlDbType.VarChar, 30);
				sqlParms[0].Value = samAccountName;

				// Password
				sqlParms[1] = new SqlParameter("@ab_password", SqlDbType.VarBinary , 255);
				sqlParms[1].Value = MyReports.Common.Functions.ConvertToBytes(password);

				// Requestors ID
				sqlParms[2] = new SqlParameter("@av_RequestorsID", SqlDbType.VarChar , 20);
				sqlParms[2].Value = requestorsID;

				// Requestors First Name
				sqlParms[3] = new SqlParameter("@av_requestorsFirstName", SqlDbType.VarChar , 150);
				sqlParms[3].Value = requestorsFirstName;

				// Requestors Last Name
				sqlParms[4] = new SqlParameter("@av_requestorsLastName", SqlDbType.VarChar , 150);
				sqlParms[4].Value = requestorsLastName;

				// Requestors Email
				sqlParms[5] = new SqlParameter("@av_requestorsEmail", SqlDbType.VarChar , 255);
				sqlParms[5].Value = requestorsEmail;

				// Requestors Phone Number
				sqlParms[6] = new SqlParameter("@av_requestorsPhoneNumber", SqlDbType.VarChar , 30);
				sqlParms[6].Value = requestorsPhoneNumber;

				// Requestors Department
				sqlParms[7] = new SqlParameter("@av_requestorsDepartment", SqlDbType.VarChar , 150);
				sqlParms[7].Value = requestorsDepartment;

				// Return Value
				sqlParms[8] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
				sqlParms[8].Direction = ParameterDirection.Output;
		
				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Insert_Master", sqlParms);

				myTrans.Commit();

				return ((Guid)sqlParms[8].Value);
			}
			catch ( Exception ex )
			{
				try
				{
					myTrans.Rollback("Data Import");
				}
				catch {}

				throw ex;
			}
			finally
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}
			
				if (myTrans != null)
					myTrans.Dispose();

				conn = null;
				myTrans = null;
				sqlParms = null;
			}						
		} 

		#endregion

		#region InsertDetailRecords_IntoSQL

		private void InsertDetailRecords_IntoSQL(Guid ID, Hashtable htValues)
		{
			SqlConnection conn = null;
			SqlTransaction myTrans = null;

			SqlParameter[] sqlParms = new SqlParameter[3];

			try
			{
				conn = new SqlConnection(_connectionString);
				conn.Open();

				myTrans = conn.BeginTransaction("Data Import");

				// loop through each recrod
				IDictionaryEnumerator myEnumerator = htValues.GetEnumerator();

				while ( myEnumerator.MoveNext() )
				{
					string attributeName = myEnumerator.Key.ToString();
					string attributeValue = myEnumerator.Value.ToString();

					// Master GUID
					sqlParms[0] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
					sqlParms[0].Value = ID;

					// Attribute Name
					sqlParms[1] = new SqlParameter("@ab_AttributeName", SqlDbType.VarChar , 250);
					sqlParms[1].Value = MyReports.Common.Functions.ConvertToBytes(attributeName);

					// Attribute Value
					sqlParms[2] = new SqlParameter("@ab_AttributeValue", SqlDbType.VarChar , 250);
					sqlParms[2].Value = MyReports.Common.Functions.ConvertToBytes(attributeValue);

					SqlHelper.ExecuteNonQuery(myTrans, "usp_Insert_Detail_Record", sqlParms);

				}
				
				myTrans.Commit();

			}
			catch (Exception ex)
			{
				try
				{
					myTrans.Rollback("Data Import");
				}
				catch {}

				throw ex;

			}
			finally 
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}
			
				if (myTrans != null)
					myTrans.Dispose();

				conn = null;
				myTrans = null;
				sqlParms = null;
			}

		}

		#endregion

		#region InsertGroupRecords_IntoSQL

		private void InsertGroupRecords_IntoSQL(Guid ID, string groups)
		{
			SqlConnection conn = null;
			SqlTransaction myTrans = null;

			SqlParameter[] sqlParms = new SqlParameter[2];

			try
			{
				conn = new SqlConnection(_connectionString);
				conn.Open();

				myTrans = conn.BeginTransaction("Data Import");

				// Master GUID
				sqlParms[0] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
				sqlParms[0].Value = ID;

				string[] group = groups.Split('|');

				// loop through each recrod
				for (int x=0;x<group.Length;x++)
				{
					sqlParms[1] = new SqlParameter("@GroupName", SqlDbType.VarBinary, 255);
					sqlParms[1].Value = MyReports.Common.Functions.ConvertToBytes(group[x]);

					SqlHelper.ExecuteNonQuery(myTrans, "usp_Insert_Groups", sqlParms);
				}
				
				myTrans.Commit();

			}
			catch (Exception ex)
			{
				try
				{
					myTrans.Rollback("Data Import");
				}
				catch {}

				throw ex;

			}
			finally 
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}
			
				if (myTrans != null)
					myTrans.Dispose();

				conn = null;
				myTrans = null;
				sqlParms = null;
			}

		}

		#endregion

		#region Insert Reject Reason IntoSQL
		public void InsertRejectReason(Guid PK_ID, string rejecter, string rejectReason)
		{
			SqlConnection conn = null;
			SqlTransaction myTrans = null;

			SqlParameter[] sqlParms = new SqlParameter[3];

			try
			{
				conn = new SqlConnection(_connectionString);
				conn.Open();

				myTrans = conn.BeginTransaction("Data Import");

				// Master GUID
				sqlParms[0] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
				sqlParms[0].Value = PK_ID;

				// Attribute Name
				sqlParms[1] = new SqlParameter("@av_Rejecter", SqlDbType.VarChar, 100);
				sqlParms[1].Value = rejecter;

				// Attribute Value
				sqlParms[2] = new SqlParameter("@at_RejectReason", SqlDbType.Text);
				sqlParms[2].Value = rejectReason;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Insert_RejectReason", sqlParms);				
				
				myTrans.Commit();

			}
			catch (Exception ex)
			{
				try
				{
					myTrans.Rollback("Data Import");
				}
				catch {}

				throw ex;

			}
			finally 
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}
			
				if (myTrans != null)
					myTrans.Dispose();

				conn = null;
				myTrans = null;
				sqlParms = null;
			}
		}
		#endregion

		#region UpdateBeenImported date Master_Detail
		public void UpdateBeenImported(Guid PK_ID, byte beenImported)
		{
			SqlConnection conn = null;
			SqlParameter[] sqlParms= null;

			try
			{
				conn = new SqlConnection(_connectionString);
		
				sqlParms = new SqlParameter[3];

				sqlParms[0] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
				sqlParms[0].Value = PK_ID;
			
				sqlParms[1] = new SqlParameter("@av_LastModifiedBy", SqlDbType.VarChar, 150);
				sqlParms[1].Value = MyReports.Common.AppValues.UserName;

				sqlParms[2] = new SqlParameter("@ai_BeenImported", SqlDbType.Int);
				sqlParms[2].Value = beenImported;
			
				SqlHelper.ExecuteNonQuery(conn, "usp_Update_Master_BeenImported", sqlParms);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}

				conn = null;
				sqlParms = null;
			}
		}
		#endregion	

		#region Add User to Active Directory
		public void AddUserToActiveDirectory(Guid PK_ID, string searchBase, string OU, bool enableAccount, bool forcePasswordReset, bool setPasswordNeverExpire, bool overrideExistingUserInfo)
		{
	
			//IUserObject AdHelper = null;
			DataSet drGroups = null;

			SqlConnection conn = null;
			SqlParameter[] sqlParms= null;

			try
			{
				conn = new SqlConnection(_connectionString);
		
				sqlParms = new SqlParameter[9];		
					
				// SamAccountName
				sqlParms[0] = new SqlParameter("@au_MasterID", SqlDbType.UniqueIdentifier);
				sqlParms[0].Value = PK_ID;

				// SamAccountName
				sqlParms[1] = new SqlParameter("@av_samAccountName", SqlDbType.VarChar , 30);
				sqlParms[1].Direction = System.Data.ParameterDirection.Output;

				// Password
				sqlParms[2] = new SqlParameter("@ab_password", SqlDbType.VarBinary , 255);
				sqlParms[2].Direction = System.Data.ParameterDirection.Output;

				// Requestors ID
				sqlParms[3] = new SqlParameter("@av_RequestorsID", SqlDbType.VarChar , 20);
				sqlParms[3].Direction = System.Data.ParameterDirection.Output;

				// Requestors First Name
				sqlParms[4] = new SqlParameter("@av_requestorsFirstName", SqlDbType.VarChar , 150);
				sqlParms[4].Direction = System.Data.ParameterDirection.Output;

				// Requestors Last Name
				sqlParms[5] = new SqlParameter("@av_requestorsLastName", SqlDbType.VarChar , 150);
				sqlParms[5].Direction = System.Data.ParameterDirection.Output;

				// Requestors Email
				sqlParms[6] = new SqlParameter("@av_requestorsEmail", SqlDbType.VarChar , 255);
				sqlParms[6].Direction = System.Data.ParameterDirection.Output;

				// Requestors Phone Number
				sqlParms[7] = new SqlParameter("@av_requestorsPhoneNumber", SqlDbType.VarChar , 30);
				sqlParms[7].Direction = System.Data.ParameterDirection.Output;

				// Requestors Department
				sqlParms[8] = new SqlParameter("@av_requestorsDepartment", SqlDbType.VarChar , 150);
				sqlParms[8].Direction = System.Data.ParameterDirection.Output;

				SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_Select_Master_By_ID", sqlParms);

				string samAccountName = sqlParms[1].Value.ToString();
				string password = MyReports.Common.Functions.ConvertFromBytes(sqlParms[2].Value); 	
				string requestorsEmail = sqlParms[6].Value.ToString();

				// Get the details
				Hashtable ht = new Hashtable();
				ArrayList ary = GetDetailRecords(PK_ID);

				for (int x=0;x<ary.Count;x++)
				{
					StructNewUserDetails ud = (StructNewUserDetails)ary[x];
					if ( (ud.AttributeName.ToLower() == "group") || (ud.AttributeName.ToLower() == "groups") )
					{
						// do nothing
					}
					else
						ht.Add(ud.AttributeName, ud.AttributeValue);
				}

				Users oUsers = new Users( _authentication );

				oUsers.AddNewUser( samAccountName, password, ht, searchBase, OU, enableAccount, forcePasswordReset, setPasswordNeverExpire, overrideExistingUserInfo);	
		
				// Now, add the user to the groups, if any are selected.
				drGroups = GetNewUserGroupMembership(PK_ID);

				ArrayList groups = new ArrayList();

				if (drGroups.Tables[0].Rows.Count > 0)
				{
					for (int x=0;x<drGroups.Tables[0].Rows.Count;x++)
					{
						groups.Add( MyReports.Common.Functions.ConvertFromBytes(drGroups.Tables[0].Rows[x]["GroupName"]).ToString() );
					}

				}
		
				oUsers.SaveNewUserGroupMembership( samAccountName, groups );				
				
				// Flag the database, make the ImportedDate for today
				UpdateBeenImported(PK_ID, 1);

			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally 
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}

				conn = null;
				sqlParms = null;

				drGroups = null;
			}	
		}
		#endregion
		
	}
}
