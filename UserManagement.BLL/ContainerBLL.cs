using System;
using System.Collections;

using Compass.Security.ActiveDirectory;
using Compass.Security.ActiveDirectory.Exchange;
using Novell.Directory.Ldap;

namespace UserManagement.BLL
{
	/// <summary>
	/// Summary description for Container.
	/// </summary>
	public class ContainerBLL : BaseClass
	{

		#region ~const

		public ContainerBLL( Authentication oAuthentication ) 
			: base( oAuthentication )
		{		

		}

		#endregion

		#region Method will return a valid listing of containers

		public ArrayList FindContainers( string filter, string ldapSearchBase, int searhScope, int maxResults )
		{

			Container	oContainer = null;

			try
			{
				oContainer = new Container( base.get_LDAPConnection() );

				string[] objectClasses = new string[1];
				objectClasses[0] = "";
				
				ArrayList htServers = new ArrayList();

				ArrayList ary = new ArrayList();
				ary.Add( "distinguishedName" );
				ary.Add( "objectClass" );
			
				ArrayList oContainers = oContainer.FindContainerObjects( filter, new string[]{ "organizationalUnit", "container"}, ary, ldapSearchBase, searhScope, maxResults );

				return oContainers;
			}

			catch (Exception ex)
			{
				throw new Exception( "Error getting exchange server listing", ex );
			}				
			finally
			{
				oContainer = null;

				base.Disconnect();
			}

		}


		#endregion

		#region Find Containers Canonical Name

		public string CanonicalName( string distinguishedName )
		{
			Container	oContainer = null;


			try
			{
				oContainer = new Container( base.get_LDAPConnection() );

				LdapAttribute ldapAttrib = oContainer.GetContainerProperties( distinguishedName, "canonicalName" );
				
				return ldapAttrib == null ? "" : ldapAttrib.StringValue;
	
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				oContainer = null;

				base.Disconnect();
			}
		}

		#endregion
	}		 
}
