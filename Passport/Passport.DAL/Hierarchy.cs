using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

using Compass.Data.MSSQLDataAccess;

namespace Passport.DAL
{
    public class Hierarchy : baseSQL
    {

        #region ~ctor

        public Hierarchy(string connectionString)
            : base(connectionString)
		{
			
		}

		#endregion

        #region Get Sector Code Listing

        public SqlDataReader GetSectorListing()
        {

            #region code

            const string STORED_PROC_NAME = "usp_Get_SectorListing";

            try
            {
                return base.ExecuteDataReader(STORED_PROC_NAME);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            #endregion

        }

        #endregion  

        #region Get Division Code Listing

        public SqlDataReader GetDivisionListing()
        {

            #region code

            const string STORED_PROC_NAME = "usp_Get_DivisionListing";

            try
            {
                return base.ExecuteDataReader(STORED_PROC_NAME);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            #endregion

        }

        #endregion  
    }
}
