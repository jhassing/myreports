using System;
using System.Data;
using System.Data.SqlClient;

using Compass.Data.MSSQLDataAccess;

namespace Passport.DAL
{
	/// <summary>
	/// Class will handle all Request Access, update access funtionality
	/// </summary>
	public class Access
	{
		private string m_sConnectionString;
		
		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public Access(string sConnectionString)
		{
			m_sConnectionString = sConnectionString;
		}

		/// <summary>
		/// Function will add a new Operations managers request for access to the passport application
		/// </summary>
		/// <param name="opNetworkID">Network ID of the operations mamanger</param>
		/// <param name="secondLineManagersNetworkID">Network ID of the second line manager</param>
		/// <param name="sector">Sector</param>
		/// <param name="opNumber">Operations Number</param>
		/// <param name="opName">Operations Name</param>
		public int RequestAccess(string opNetworkID, string secondLineManagersNetworkID, string sector, string opNumber, string opName)
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[8];

				arParms[0] = new SqlParameter("@as_RequestAccessID", SqlDbType.VarChar, 10 ); 
				arParms[0].Direction = System.Data.ParameterDirection.Output;

				arParms[1] = new SqlParameter("@as_ManagerLogin", SqlDbType.VarChar, 15 ); 
				arParms[1].Value = opNetworkID;

				arParms[2] = new SqlParameter("@as_SectorName", SqlDbType.VarChar, 50 ); 
				arParms[2].Value = sector;

				arParms[3] = new SqlParameter("@as_OperationNumber", SqlDbType.VarChar, 50 ); 
				arParms[3].Value = opNumber;

				arParms[4] = new SqlParameter("@as_OperationName", SqlDbType.VarChar, 50 ); 
				arParms[4].Value = opName;

				arParms[5] = new SqlParameter("@as_SecondLineManagerLogin", SqlDbType.VarChar, 50 ); 
				arParms[5].Value = secondLineManagersNetworkID;

				arParms[6] = new SqlParameter("@as_RequestStatusID", SqlDbType.VarChar, 10 ); 
				arParms[6].Value = 1;

				arParms[7] = new SqlParameter("@as_CreatedBy", SqlDbType.VarChar, 50 ); 
				arParms[7].Value = opNetworkID;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Gen_Insert_Request_Access", arParms);	

				// Commit the transaction
				myTrans.Commit();	
				
				// get the primary key, and return it.
				return System.Convert.ToInt32( arParms[0].Value );
			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}

		
		/// <summary>
		/// Update a users reequest for Access
		/// </summary>
		/// <param name="RequestAccessID">Primary key of the account to update</param>
		/// <param name="RequestStatusID">Status code to update to (Accept, decline)</param>
		/// <returns></returns>
		public string UpdateAccess(int RequestAccessID, int RequestStatusID)
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(m_sConnectionString);
				myConnection.Open();
	
				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[3];

				arParms[0] = new SqlParameter("@as_RequestAccessID", SqlDbType.VarChar, 10 ); 
				arParms[0].Value = RequestAccessID.ToString();

				arParms[1] = new SqlParameter("@as_RequestStatusID", SqlDbType.VarChar, 10 ); 
				arParms[1].Value = RequestStatusID.ToString();

				arParms[2] = new SqlParameter("@as_ManagerLogin", SqlDbType.VarChar, 15 ); 
				arParms[2].Direction = System.Data.ParameterDirection.Output;
				

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Update_Request_Access", arParms);	

				// Commit the transaction
				myTrans.Commit();	
				
				// get the primary key, and return it.
				return arParms[2].Value.ToString();


			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}

		}
	}
}
