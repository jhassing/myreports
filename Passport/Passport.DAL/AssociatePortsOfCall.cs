using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

using Compass.Data.MSSQLDataAccess;

namespace Passport.DAL
{
	/// <summary>
	///  Handles all data access actions for Associates and their ports of call
	/// </summary>
	public class AssociatePortsOfCall
	{
		private string m_sConnectionString;
		
		#region ~ctor

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public AssociatePortsOfCall(string sConnectionString)
		{
			m_sConnectionString = sConnectionString;
		}

		#endregion
		
		#region Get Associates Ports of Call for a particular Expedition

		/// <summary>
		/// Get a listing of all the Ports of call for this user, for this Expedition
		/// </summary>
		/// <param name="assocID">Associates PK</param>
		/// <param name="expeditionID">Expedition Number</param>
		/// <returns></returns>									  personnelNumber
		public SqlDataReader Get_Associate_PortsOfCall( int assocID, int expeditionID )
		{
			try
			{
				
				SqlParameter [] arParms = new SqlParameter[2];

				arParms[0] = new SqlParameter( "@ai_AssocID", SqlDbType.Int );
				arParms[0].Value = assocID;

				arParms[1] = new SqlParameter( "@ai_ExpeditionID", SqlDbType.Int );
				arParms[1].Value = expeditionID;

				return SqlHelper.ExecuteReader( m_sConnectionString, CommandType.StoredProcedure, "usp_Get_Associate_PortsOfCall", arParms);	
			}
			catch(Exception ex)
			{	
				throw(ex);
			}			
		}

		#endregion

		#region Insert Associates Completed Ports Of Call 

		/// <summary>
		/// Method will either update an existing Port of call completion date, or, enter a new completion date
		/// </summary>		
		/// <param name="expeditionID">Expedition number we are currently working with</param>
		/// <param name="portNumber">Port of call number inside the expedition</param>
		/// <param name="assocID">Users personnel number</param>
		/// <param name="operationNumber">Operation the user is a member of at this time</param>
		/// <param name="completionDate">Date the port of call was completed</param>
		/// <param name="userNetworkID">Network ID of the manager makng the changes</param>				
		public void InsertAssociatesCompletedPortsOfCall(	int expeditionID, int portNumber, int assocID, 
															int operationNumber, DateTime completionDate, 
															string userNetworkID )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[7];

				// New Record Primary Key
				arParms[0] = new SqlParameter("@ai_ExpeditonPortAssocID", SqlDbType.Int );
				arParms[0].Direction = System.Data.ParameterDirection.Output;
				
				// Expedition Number
				arParms[1] = new SqlParameter("@ai_ExpeditionID", SqlDbType.Int );
				arParms[1].Value = expeditionID;

				// Port of call completed
				arParms[2] = new SqlParameter("@ai_PortNumber", SqlDbType.Int );
				arParms[2].Value = portNumber;

				// Users Primary Key
				arParms[3] = new SqlParameter("@ai_AssocID", SqlDbType.Int );
				arParms[3].Value = assocID;

				// Operations Number
				arParms[4] = new SqlParameter("@ai_OperationNumber", SqlDbType.Int );
				arParms[4].Value = operationNumber;

				// Port of call Completion date			
				arParms[5] = new SqlParameter("@ad_CompletionDate", SqlDbType.DateTime );
				arParms[5].Value = completionDate;

				// Network ID of the users modifing this record			
				arParms[6] = new SqlParameter("@as_CreatedBy", SqlDbType.VarChar, 75 );
				arParms[6].Value = userNetworkID;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Insert_Expedition_Port_Associate", arParms);	

				myTrans.Commit();	

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}


		#endregion

		#region Remove Associate Completed Ports Of Call

		/// <summary>
		/// Method will remove a user from a completed port of call
		/// </summary>
		/// <param name="assocID">Users personnel number</param>
		/// <param name="expeditionID">Expedition number we are currently working with</param>
		/// <param name="operationNumber">Operation the user is a member of at this time</param>
		/// <param name="portOfCallNumber">Port of call number inside the expedition</param>
		public void RemoveAssociateCompletedPortsOfCall( int assocID, int expeditionID, int operationNumber, int portOfCallNumber )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[4];

				// Users Primary Key
				arParms[0] = new SqlParameter("@ai_assocID", SqlDbType.Int );
				arParms[0].Value = assocID;

				// Expedition Number
				arParms[1] = new SqlParameter("@ai_ExpeditionID", SqlDbType.Int );
				arParms[1].Value = expeditionID;

				// Operation Number		
				arParms[2] = new SqlParameter("@ai_OperationNumber", SqlDbType.Int );
				arParms[2].Value = operationNumber;

				// Port of call number			 
				arParms[3] = new SqlParameter("@ai_PortNumber", SqlDbType.Int );
				arParms[3].Value = portOfCallNumber;		

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Delete_Expedition_Port_Associate", arParms);	

				myTrans.Commit();

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}

		}

		#endregion

	}
}
