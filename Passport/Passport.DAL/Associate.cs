using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

using Compass.Data.MSSQLDataAccess;

namespace Passport.DAL
{
	/// <summary>
	/// Handles all actions associates with Associates in Passport
	/// </summary>
	public class Associate
	{
		private string m_sConnectionString;
		
		#region ~ctor

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public Associate(string sConnectionString)
		{
			this.m_sConnectionString = sConnectionString;
		}

		#endregion

		#region Add New Associate

		/// <summary>
		/// Insert a new Associate into the database, and return the pk ID for the record
		/// </summary>
		/// <param name="operationNumber">Operation the Associate is a member of.</param>
		/// <param name="assocPerNum">Associates personnel number</param>
		/// <param name="firstName">Associates first name</param>
		/// <param name="lastName">Associates last name</param>
		/// <param name="isActive">Is the Associate active</param>
		/// <param name="isCompassAssoc">Is the associate a compass user</param>
		/// <returns>PK for the newly added associate</returns>
		public int InsertNewAssociate(	int operationNumber, string assocPerNum, string firstName, string lastName, 
										bool isActive, bool isCompassAssoc )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(this.m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[7];

				// New Record Primary Key
				arParms[0] = new SqlParameter("@ai_AssocID", SqlDbType.Int );
				arParms[0].Direction = System.Data.ParameterDirection.Output;

				// Operations Number
				arParms[1] = new SqlParameter("@ai_OperationNumber", SqlDbType.Int );
				arParms[1].Value = operationNumber;

				// Personnel Number
				arParms[2] = new SqlParameter("@ai_AssocPerNum", SqlDbType.VarChar, 8 );
				arParms[2].Value = assocPerNum;

				// Associates First Name
				arParms[3] = new SqlParameter("@as_AssocFName", SqlDbType.VarChar, 20 );
				arParms[3].Value = firstName;

				// Associates Last Name
				arParms[4] = new SqlParameter("@as_AssocLName", SqlDbType.VarChar, 20 );
				arParms[4].Value = lastName;

				// Active flag		
				arParms[5] = new SqlParameter("@ab_ActiveFlag", SqlDbType.Bit );
				arParms[5].Value = isActive;

				// Active flag		
				arParms[6] = new SqlParameter("@ab_CompassAssocFlag", SqlDbType.Bit );
				arParms[6].Value = isCompassAssoc;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Insert_Associate", arParms);	

				myTrans.Commit();
	
				return System.Convert.ToInt32( arParms[0].Value.ToString() );

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}

		
		#endregion

		#region Delete Associate

		/// <summary>
		/// Deletes an assoicate from the database
		/// </summary>
		/// <param name="assocId">Primary Key of the associate to delete</param>
		public void DeleteAssociate( int assocId )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(this.m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[1];

				// Associates Primary Key
				arParms[0] = new SqlParameter("@ai_AssocID", SqlDbType.Int );
				arParms[0].Value = assocId;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Delete_Associate", arParms);	

				myTrans.Commit();

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}


		#endregion

		#region Modify Associate

		/// <summary>
		/// Method will modify an Associates data
		/// </summary>
		/// <param name="assocId">Primary Key of the associate to modify</param>
		/// <param name="operationNumber">Operation the Associate is a member of.</param>
		/// <param name="firstName">Associates first name</param>
		/// <param name="lastName">Associates last name</param>
		public void ModifyAssociate( int assocId, int operationNumber, string firstName, string lastName )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(this.m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[4];

				// Associates Primary Key
				arParms[0] = new SqlParameter("@ai_AssocID", SqlDbType.Int );
				arParms[0].Value = assocId;

				// Operations Number
				arParms[1] = new SqlParameter("@ai_OperationNumber", SqlDbType.Int );
				arParms[1].Value = operationNumber;

				// Associates First Name
				arParms[2] = new SqlParameter("@as_AssocFName", SqlDbType.VarChar, 20 );
				arParms[2].Value = firstName;

				// Associates Last Name
				arParms[3] = new SqlParameter("@as_AssocLName", SqlDbType.VarChar, 20 );
				arParms[3].Value = lastName;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Update_Associate", arParms);	

				myTrans.Commit();

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}

		#endregion

		#region Set Associate active flag
		
		/// <summary>
		/// Sets an associate to active or not
		/// </summary>
		/// <param name="assocId">Primary Key of the associate to modify</param>
		/// <param name="active">is this user active?</param>
		public void SetAssociateActive( int assocId, bool active )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(this.m_sConnectionString);
				myConnection.Open();
				
				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[2];

				// Associates Primary Key
				arParms[0] = new SqlParameter("@ai_AssocID", SqlDbType.Int );
				arParms[0].Value = assocId;

				// Active flag		
				arParms[1] = new SqlParameter("@ab_Active", SqlDbType.Bit );
				arParms[1].Value = active;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Update_Associate_SetActiveFlag", arParms);	

				myTrans.Commit();

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}

		#endregion

		#region Get Associates By Operations Number and ExpeditionNumber 

		/// <summary>
		/// Method will get a listing of all associates based on unit numbers
		/// </summary>
		/// <param name="unitNumber">Unit of the Associate</param>
		/// <param name="passportActiveFlag">Has the user already been set to use the site?</param>
		/// <returns></returns>
		public SqlDataReader GetAssociates_ByOperationsNumber_ExpeditionNumber( int operationNumber, int expeditionID )
		{

			try
			{
				
				SqlParameter [] arParms = new SqlParameter[1];

				arParms[0] = new SqlParameter( "@ai_OperationNumber", SqlDbType.Int );
				arParms[0].Value = operationNumber;
										   
				//arParms[1] = new SqlParameter( "@ai_ExpeditionNumber", SqlDbType.Int );
				//arParms[1].Value = expeditionID;

				//return SqlHelper.ExecuteReader( this.m_sConnectionString, CommandType.StoredProcedure, "usp_Get_Associate_ByOperationNumber_ExpeditionNumber", arParms);	
				return SqlHelper.ExecuteReader( this.m_sConnectionString, CommandType.StoredProcedure, "usp_Get_Associate_ByOperationNumber", arParms);	
			}
			catch(Exception ex)
			{	
				throw(ex);
			}		

		}
									  

		#endregion

		#region Get Non Compass Associates

		/// <summary>
		/// Method will get a listing of all associates based on unit numbers
		/// </summary>
		/// <param name="operationNumber">Operations number</param>
		/// <returns></returns>
		public SqlDataReader Get_NonCompassAssociates( int operationNumber )
		{

			try
			{
				
				SqlParameter [] arParms = new SqlParameter[1];
				
				arParms[0] = new SqlParameter( "@ai_OperationNumber", SqlDbType.Int );
				arParms[0].Value = operationNumber;
				
				return SqlHelper.ExecuteReader( this.m_sConnectionString, CommandType.StoredProcedure, "usp_Get_NonCompassAssociates", arParms );	
			}
			catch(Exception ex)
			{	
				throw(ex);
			}		

		}

		#endregion

		#region Get Associate by Assoc ID

		/// <summary>
		/// Method will lookup a single associate based on AssocID (Primary Key)
		/// </summary>
		/// <param name="assocID">Primary Key of an associate to locate inside the database</param>
		/// <param name="assocPerNum">Output: Associates Personnel Number.</param>
		/// <param name="operationNumber">Output: Associates Operation Number they belong to.</param>
		/// <param name="assocFirstName">Output: Associates first name.</param>
		/// <param name="assocLastName">Output: Associates Last Name.</param>
		/// <param name="compassAssocFlag">Output: indicates if this associate is a compass employee or were they added from the app.</param>
		/// <param name="activeFlag">Output: Is the associate currently active?</param>
		public void GetAssociateByAssocID (	int assocID, 
											ref string assocPerNum, 
											ref int operationNumber, 
											ref string assocFirstName,
											ref string assocLastName, 
											ref bool compassAssocFlag, 
											ref bool activeFlag )
		{		  
			try
			{
				
				SqlParameter [] arParms = new SqlParameter[7];

				arParms[0] = new SqlParameter( "@ai_AssocID", SqlDbType.Int );
				arParms[0].Value = assocID;		
				
				arParms[1] = new SqlParameter( "@as_AssocPerNum", SqlDbType.VarChar, 8 );
				arParms[1].Direction = System.Data.ParameterDirection.Output;	  

				arParms[2] = new SqlParameter( "@ai_OperationNumber", SqlDbType.Int );
				arParms[2].Direction = System.Data.ParameterDirection.Output;	 
				
				arParms[3] = new SqlParameter( "@as_AssocFName", SqlDbType.VarChar, 50 );
				arParms[3].Direction = System.Data.ParameterDirection.Output;	
 
				arParms[4] = new SqlParameter( "@as_AssocLName", SqlDbType.VarChar, 50 );
				arParms[4].Direction = System.Data.ParameterDirection.Output;	
 
				arParms[5] = new SqlParameter( "@ab_CompassAssocFlag", SqlDbType.Bit );
				arParms[5].Direction = System.Data.ParameterDirection.Output;	

				arParms[6] = new SqlParameter( "@ab_ActiveFlag", SqlDbType.Bit );
				arParms[6].Direction = System.Data.ParameterDirection.Output;	
								
				SqlHelper.ExecuteNonQuery( this.m_sConnectionString, CommandType.StoredProcedure, "usp_Get_Associate_By_AssocID", arParms);	

				// Personnel Number
				assocPerNum = arParms[1].Value.ToString();

				// Operation Number
				operationNumber = System.Convert.ToInt32( arParms[2].Value );

				// First Name
				assocFirstName = arParms[3].Value.ToString();

				// Last Name
				assocLastName = arParms[4].Value.ToString();

				// Compass Assoc Flag
				compassAssocFlag = System.Convert.ToBoolean( arParms[5].Value );

				// Active Flag
				activeFlag = System.Convert.ToBoolean( arParms[6].Value );

			}
			catch(Exception ex)
			{	
				throw(ex);
			}
		}

		#endregion

		#region Get the last used non-Compass personnel number

		public string GetLastUsedNonCompassPersonnelNumber()
		{
			try
			{
				
				SqlParameter [] arParms = new SqlParameter[1];

				arParms[0] = new SqlParameter( "@as_AssocPerNum", SqlDbType.VarChar, 8 );
				arParms[0].Direction = System.Data.ParameterDirection.Output;	  
								
				SqlHelper.ExecuteNonQuery( this.m_sConnectionString, CommandType.StoredProcedure, "usp_Get_LastUsed_NonCompass_PersonnelNumber", arParms);	

				return arParms[0].Value.ToString();

			}
			catch(Exception ex)
			{	
				throw(ex);
			}
			
		}

		#endregion



	}
}
