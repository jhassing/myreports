using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

using Compass.Data.MSSQLDataAccess;

namespace Passport.DAL
{
	/// <summary>
	/// Handles all database transactions with Ports of call
	/// </summary>
	public class PortsOfCall
	{
		private string m_sConnectionString;

		#region ~ctor

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public PortsOfCall(string sConnectionString)
		{
			this.m_sConnectionString = sConnectionString;
		}

		#endregion

		#region Get Ports Of Call listing for an Expedition

		public SqlDataReader Get_PortsOfCall_By_Expedition( int expeditionID )
		{

			try
			{
				
				SqlParameter [] arParms = new SqlParameter[1];
										   
				arParms[0] = new SqlParameter( "@ai_ExpeditionID", SqlDbType.Int );
				arParms[0].Value = expeditionID;

				return SqlHelper.ExecuteReader( this.m_sConnectionString, CommandType.StoredProcedure, "usp_Get_Expedition_Port_ByExpeditionID", arParms);	
			}
			catch(Exception ex)
			{	
				throw(ex);
			}		

		}

		#endregion

		#region Insert New Port of Call for an Existing Expedition
		/// <summary>
		/// Insert a new Port of Call into the database for an Expedition, and return the pk ID for the record
		/// </summary>
		/// <param name="expeditionID">Expedition ID</param>
		/// <param name="pocName">Port of Call Name</param>
		/// <param name="pocNumber">Port of Call Number</param>
		/// <param name="secondLineManagersNetworkID">Manager's network id</param>
		/// <returns>PK for the newly added Port of Call</returns>
		public int InsertNewPortofCall( int expeditionID, string pocName, int pocNumber, string secondLineManagersNetworkID )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(this.m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[5];

				// New Record Primary Key
				arParms[0] = new SqlParameter("@ai_POCID", SqlDbType.Int );
				arParms[0].Direction = System.Data.ParameterDirection.Output;

				// Expedition ID for new Port of Call
				arParms[1] = new SqlParameter("@ai_ExpeditionID", SqlDbType.Int );
				arParms[1].Value = expeditionID;

				// Port of Call's Name
				arParms[2] = new SqlParameter("@as_PortOfCallName", SqlDbType.VarChar, 75 );
				arParms[2].Value = pocName;

				// Port of Call's Number
				arParms[3] = new SqlParameter("@ai_PortOfCallNumber", SqlDbType.Int );
				arParms[3].Value = pocNumber;

				// Port of Call's Creator 
				arParms[4] = new SqlParameter("@as_PortOfCallCreator", SqlDbType.VarChar, 50 );
				arParms[4].Value = secondLineManagersNetworkID;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Insert_PortOfCall", arParms);	

				myTrans.Commit();
	
				return System.Convert.ToInt32( arParms[0].Value.ToString() );

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}


		#endregion
		
		#region Modify a Port of Call

		/// <summary>
		/// Method will modify a Port of Call
		/// </summary>
		/// <param name="pocId">Primary Key of the Port of Call to modify</param>
		/// <param name="pocName">Port of Call name</param>
		/// <param name="pocNumber">Port of Call description</param>
		public void ModifyPortOfCall( int pocId, string pocName, int pocNumber )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(this.m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[3];

				// Port of Call Primary Key
				arParms[0] = new SqlParameter("@ai_POCID", SqlDbType.Int );
				arParms[0].Value = pocId;

				// Port of Call Name
				arParms[1] = new SqlParameter("@as_POCName", SqlDbType.VarChar, 75 );
				arParms[1].Value = pocName;

				// Port of Call Description
				arParms[2] = new SqlParameter("@ai_POCNumber", SqlDbType.Int );
				arParms[2].Value = pocNumber;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Update_PortOfCall", arParms);	

				myTrans.Commit();

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}

		#endregion

		#region Delete Port of Call

		/// <summary>
		/// Deletes a Port of Call from the database
		/// </summary>
		/// <param name="pocId">Primary Key of the Port of Call to delete</param>
		public void DeletePortOfCall( int pocId )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(this.m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[1];

				// Associates Primary Key
				arParms[0] = new SqlParameter("@ai_pocID", SqlDbType.Int );
				arParms[0].Value = pocId;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Delete_PortOfCall", arParms);	

				myTrans.Commit();

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}


		#endregion


	}
}
