using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

using Compass.Data.MSSQLDataAccess;

namespace Passport.DAL
{
	/// <summary>
	/// Class will handle all data access for Operations
	/// </summary>
	public class Operations
	{

		private string m_sConnectionString;

		#region ~ctor

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public Operations(string sConnectionString)
		{
			m_sConnectionString = sConnectionString;
		}

		#endregion

		#region Get Operations for this 2nd Line Manager

		public SqlDataReader GetOperationsFromDMCode( string DM )
		{
			try
			{
				SqlParameter [] arParms = new SqlParameter[1];

				arParms[0] = new SqlParameter( "@as_DM", SqlDbType.VarChar, 10 );
				arParms[0].Value = DM;

				return SqlHelper.ExecuteReader( m_sConnectionString, CommandType.StoredProcedure, "usp_Get_OperationCode_By_DM", arParms );	
			}
			catch(Exception ex)
			{	
				throw(ex);
			}	
		}


		#endregion



		#region Get Operations Code for this Region

		public SqlDataReader GetOperationsFromRegionCode( string region )
		{
			try
			{
				SqlParameter [] arParms = new SqlParameter[1];

				arParms[0] = new SqlParameter( "@as_Region", SqlDbType.VarChar, 10 );
				arParms[0].Value = region;

				return SqlHelper.ExecuteReader( m_sConnectionString, CommandType.StoredProcedure, "usp_Get_OperationCode_By_Region", arParms );	
			}
			catch(Exception ex)
			{	
				throw(ex);
			}	
		}


		#endregion
	}
}
