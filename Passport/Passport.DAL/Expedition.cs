using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

using Compass.Data.MSSQLDataAccess;

namespace Passport.DAL
{
	/// <summary>
	/// Class will handle all data access associated with Expeditions.
	/// </summary>
	public class Expedition
	{
		private string m_sConnectionString;

		#region ~ctor

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public Expedition(string sConnectionString)
		{
			m_sConnectionString = sConnectionString;
		}

		#endregion

		#region Get Expeditions	Listing

		public SqlDataReader GetExpeditionListing()
		{
			try
			{
				return SqlHelper.ExecuteReader( m_sConnectionString, CommandType.StoredProcedure, "usp_Get_Expedition" );	
			}
			catch(Exception ex)
			{	
				throw(ex);
			}	
		}

		#endregion

		#region Get Ports of call count for a particular expedition
		
		public SqlDataReader GetCount_Expedition_PortsOfCall( int expeditionId )
		{

			try
			{
				SqlParameter [] arParms = new SqlParameter[1];

				// Associates Primary Key
				arParms[0] = new SqlParameter("@ai_ExpeditionID", SqlDbType.Int );
				arParms[0].Value = expeditionId;

				return SqlHelper.ExecuteReader( m_sConnectionString, CommandType.StoredProcedure, "usp_Get_Expedition_Port_Count", arParms );	
			}
			catch(Exception ex)
			{	
				throw(ex);
			}	
		}
		
		#endregion

		#region Insert new Expedition
		/// <summary>
		/// Insert a new Expedition into the database, and return the pk ID for the record
		/// </summary>
		/// <param name="expeditionName">Expedition name</param>
		/// <param name="expeditionDesc">Expedition Description</param>
		/// <param name="secondLineManagersNetworkID">Manager's network id</param>
		/// <returns>PK for the newly added Expedition</returns>
		public int InsertNewExpedition( string expeditionName, string expeditionDesc, string secondLineManagersNetworkID )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(this.m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[4];

				// New Record Primary Key
				arParms[0] = new SqlParameter("@ai_ExpeditionID", SqlDbType.Int );
				arParms[0].Direction = System.Data.ParameterDirection.Output;

				// Expedition Name
				arParms[1] = new SqlParameter("@as_ExpeditionName", SqlDbType.VarChar, 50 );
				arParms[1].Value = expeditionName;

				// Expedition Description
				arParms[2] = new SqlParameter("@as_ExpeditionDesc", SqlDbType.VarChar, 150 );
				arParms[2].Value = expeditionDesc;

				// Passport Expedition Creator (
				arParms[3] = new SqlParameter("@as_ExpeditionCreator", SqlDbType.VarChar, 50 );
				arParms[3].Value = secondLineManagersNetworkID;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Insert_Expedition", arParms);	

				myTrans.Commit();
	
				return System.Convert.ToInt32( arParms[0].Value.ToString() );

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}


		#endregion
		
		#region Modify an Expedition

		/// <summary>
		/// Method will modify an Expedition data
		/// </summary>
		/// <param name="expedId">Primary Key of the Expedition to modify</param>
		/// <param name="expedName">Expedition name</param>
		/// <param name="expedDesc">Expedition description</param>
		public void ModifyExpedition( int expedId, string expedName, string expedDesc )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(this.m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[3];

				// Expedition Primary Key
				arParms[0] = new SqlParameter("@ai_ExpedID", SqlDbType.Int );
				arParms[0].Value = expedId;

				// Expedition Name
				arParms[1] = new SqlParameter("@as_ExpedName", SqlDbType.VarChar, 50 );
				arParms[1].Value = expedName;

				// Expedition Desc
				arParms[2] = new SqlParameter("@as_ExpedDesc", SqlDbType.VarChar, 150 );
				arParms[2].Value = expedDesc;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Update_Expedition", arParms);	

				myTrans.Commit();

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}

		#endregion

		#region Delete an Expedition

		/// <summary>
		/// Deletes an Expedition from the database
		/// </summary>
		/// <param name="expedId">Primary Key of the Expedition to delete</param>
		public void DeleteExpedition( int expedId )
		{
			SqlConnection myConnection = null;
			SqlTransaction myTrans = null;

			try
			{
				myConnection = new SqlConnection(this.m_sConnectionString);
				myConnection.Open();				

				// Start a local transaction
				myTrans = myConnection.BeginTransaction();

				SqlParameter [] arParms = new SqlParameter[1];

				// Associates Primary Key
				arParms[0] = new SqlParameter("@ai_ExpedID", SqlDbType.Int );
				arParms[0].Value = expedId;

				SqlHelper.ExecuteNonQuery(myTrans, CommandType.StoredProcedure, "usp_Delete_Expedition", arParms);	

				myTrans.Commit();

			}
			catch(Exception ex)
			{	
				myTrans.Rollback();
				throw(ex);
			}			
			finally
			{
				if (myTrans != null)
					myTrans.Dispose();
				
				myTrans = null;

				if (myConnection != null)
					myConnection.Dispose();
				
				myConnection = null;

			}
		}


		#endregion

	}
}
