using System;   
using System.Collections;

using MyReports.Common;

using Compass.Security.ActiveDirectory;

namespace Passport.BLL
{

	#region ManagerInfo

	public struct ManagerInfo
	{
		private string _displayName;
		private string _samAccountName;
		private string _sn;
		private string _givenName;
		private string _email;

		public ManagerInfo(string DisplayName, string SamAccountName, string SN, string GivenName, string Email)
		{
			this._displayName = DisplayName;
			this._samAccountName = SamAccountName;
			this._sn = SN;
			this._givenName = GivenName;
			this._email = Email;
			
		}

		public string DisplayName
		{
			get
			{
				return this._displayName;
			}
		}

		public string SamAccountName
		{
			get
			{
				return this._samAccountName;
			}
		}

		public string SN
		{
			get
			{
				return this._sn;
			}
		}

		public string GivenName
		{
			get
			{
				return this._givenName;
			}
		}

		public string Email
		{
			get
			{
				return this._email;
			}
		}
	}

	#endregion

	public class Access
	{
		private string m_sConnectionString;
		
		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public Access(string sConnectionString)
		{
			m_sConnectionString = sConnectionString;
		}

		/// <summary>
		/// Function will add a new Operations managers request for access to the passport application
		/// </summary>
		/// <param name="opNetworkID">Network ID of the operations mamanger</param>
		/// <param name="secondLineManagersNetworkID">Network ID of the second line manager</param>
		/// <param name="sector">Sector</param>
		/// <param name="opNumber">Operations Number</param>
		/// <param name="opName">Operations Name</param>
		/// <returns>Primary Ket of the new record</returns>
		public int RequestAccess(string opNetworkID, string secondLineManagersNetworkID, string sector, string opNumber, string opName)
		{
			return new Passport.DAL.Access (this.m_sConnectionString).RequestAccess(opNetworkID, secondLineManagersNetworkID, sector, opNumber, opName); 
		}

		
		/// <summary>
		/// Update a users reequest for Access
		/// </summary>
		/// <param name="RequestAccessID">Primary key of the account to update</param>
		/// <param name="RequestStatusID">Status code to update to (Accept, decline)</param>
		/// <returns></returns>
		public string UpdateAccess(int RequestAccessID, int RequestStatusID)
		{
			return new Passport.DAL.Access (this.m_sConnectionString).UpdateAccess(RequestAccessID, RequestStatusID );			
		}


		public static ArrayList GroupMembership(string group)
		{
			
			ArrayList retObjects = new ArrayList();

			try
			{
//				ADGroup oADGroup = new ADGroup( General.DomainUser, General.DomainPassword, General.DomainName, General.LdapPort );
//				IUserObject oADUser = new ADUserObject( General.DomainUser, General.DomainPassword, General.DomainName, General.LdapPort );
//
//				ArrayList ary12 = new ArrayList();
//				ary12.Add ( "givenName" );
//				ary12.Add ( "sn" );
//
//				// Get the groups DN
//				string sDN = ""; //oADGroup.GetObjectsDistinguishedName( group + "_GRP" );
//
//				// Get the members
//				tObjects[] aryUsers = oADGroup.GetGroupMembership( sDN, ary12 );
//
//				//oADUser.FindUsers 
//				for (int x=0;x<aryUsers.Length;x++)
//				{
//					Hashtable ht = new Hashtable();
//					ht.Add( "anr", aryUsers[x].Attributes[0].Value + " " + aryUsers[x].Attributes[1].Value );		
//
//					string[] aryParams = new string[5];
//					aryParams[0] = "displayName";
//					aryParams[1] = "samAccountName";
//					aryParams[2] = "sn";
//					aryParams[3] = "givenName";
//					aryParams[4] = "mail";
//
//					// Locate the user.
//					tObjects[] oUsers = oADUser.FindUsers( ht, aryParams, new string[] { "user" }, AppValues.LdapBaseOU, 2, 1000 );
//
//					for (int y=0;y<oUsers.Length;y++)
//					{
//						// loop through	each user.
//						string DisplayName		= oUsers[y].Attributes[0].Value;
//						string samAccountName	= oUsers[y].Attributes[1].Value;
//						string sn				= oUsers[y].Attributes[2].Value;
//						string givenName		= oUsers[y].Attributes[3].Value;
//						string email			= oUsers[y].Attributes[4].Value;
//
//						// Make sure the email address is populated
//						if ( email.ToLower().IndexOf("compass-usa.com") > 0 )
//						{
//							if ( samAccountName.Length > 0 )
//							{
//								if ( !((samAccountName.Length == 10) && (Compass.Common.Functions.IsNumeric(samAccountName)) ) )
//								{		
//									// Loop through, making sure the email address is not already in the list.
//									bool addToList = true;
//
//									for (int z=0;z<retObjects.Count;z++)
//									{
//										if ( ((ManagerInfo)retObjects[z]).Email.Equals( email ))
//										{
//											addToList = false;
//											break;
//										}												
//									}
//								
//									if (addToList)
//										retObjects.Add( new ManagerInfo(DisplayName, samAccountName, sn, givenName, email) );
//								}
//							}										
//						}
//					}
//				}

				return retObjects;
				
			}
			catch(Exception ex)
			{
				throw new Exception( "Error getting group membership", ex );
			}


		}

	}
}
