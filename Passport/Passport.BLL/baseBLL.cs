using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using Passport.DAL;

namespace Passport.BLL
{
    public class BaseBLL
    {
        protected string _connectionString = "";

        #region ~ctor

        public BaseBLL(string connectionString)
        {
            _connectionString = connectionString;
        }

        #endregion

      
    }
}
