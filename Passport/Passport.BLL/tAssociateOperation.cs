using System;
using System.Collections;

namespace Passport.BLL
{

	#region Struct of Associates

	[Serializable]
	public struct tAssociates
	{
		private int				_assocID;
		private string			_personnelNumber;
		private int				_operationNumber;
		private string			_firstName;
		private string			_lastName;
		private bool			_activeFlag;
		private bool			_compassAssociate;
		
		
		#region Properties

		public int		AssocID
		{
			get 
			{
				return this._assocID;
			}
		}

		public string	PersonnelNumber
		{
			get 
			{
				return this._personnelNumber;
			}
		}
		
		public int		OperationNumber
		{
			get
			{
				return this._operationNumber;
			}
		}

		public string	FirstName
		{
			get
			{
				return this._firstName;
			}
		}
		public string	LastName
		{
			get
			{
				return this._lastName;
			}
		}
		public bool		ActiveFlag
		{
			get
			{
				return this._activeFlag;
			}
		}
		
		public bool		CompassAssociate
		{
			get
			{
				return this._compassAssociate;
			}
		}

		
		#endregion

		public tAssociates( int assocID, string personnelNumber, int operationNumber,
							string firstName, string lastName, bool activeFlag,  bool compassAssociate )
		{
			this._assocID				= assocID;
			this._personnelNumber		= personnelNumber;
			this._operationNumber		= operationNumber;
			this._firstName				= firstName;
			this._lastName				= lastName;
			this._activeFlag			= activeFlag;
			this._compassAssociate		= compassAssociate;
			
		}
	}

	#endregion

	#region Struct Associates By Operation and the Ports of Call for the Associate

	[Serializable]
	public struct tAssociateOperation
	{
        private string _associateName;
        private string _assocPerNum;
        private int _assocID;
        private bool _activeFlag;
        private bool _compassEmployee;
        private ArrayList _portOfCall;

		#region Properties

        public int AssocID
        { get { return this._assocID; } }

        public string AssocPerNum
        { get { return this._assocPerNum; } }

        public string associateName
        { get { return this._associateName; } }

        public bool ActiveFlag
        { get { return this._activeFlag; } }

        public bool CompassEmployee
        { get { return this._compassEmployee; } }
		
		public ArrayList	PortsOfCall 
		{
            get
            { return this._portOfCall; }
            set
            { this._portOfCall = value; }
		}

		#endregion

        public tAssociateOperation(int assocID, string assocPerNum, string associateName, bool activeFlag, bool compassEmployee, ArrayList aryPortsOfCall)
		{
            this._assocID = assocID;
            this._assocPerNum = assocPerNum;
			this._associateName = associateName;			
			this._portOfCall = aryPortsOfCall;
			this._activeFlag = activeFlag;
			this._compassEmployee = compassEmployee;
		}
	}


	[Serializable]
	public struct tPortOfCall
	{
		private int _portOfCallNumber;
		private DateTime _dateCompleted;
 
		#region Properties

		public int PortOfCallNumber
		{
			get
			{
				return this._portOfCallNumber;
			}
		}

		public DateTime DateCompleted
		{
			get
			{
				return this._dateCompleted;
			}
		}

		#endregion

		public tPortOfCall( int portOfCallNumber, DateTime dateCompleted )
		{
			this._portOfCallNumber = portOfCallNumber;
			this._dateCompleted = dateCompleted;
		}
	}


	#endregion

	#region Ports of Call struct

	[Serializable]
	public struct tPortsOfCall
	{
		private int			_expeditionPortID;
		private int			_expeditionID;
		private int			_portNumber;
		private string		_portOfCallName;
				
		#region Properties

		public int		ExpeditionPortID
		{
			get
			{
				return this._expeditionPortID;
			}
		}
		public int		ExpeditionID
		{
			get
			{
				return this._expeditionID;
			}
		}
		public int		PortNumber
		{
			get
			{
				return this._portNumber;
			}
		}

		public string	PortOfCallName
		{
			get
			{
				return this._portOfCallName;
			}
		}


		#endregion

		public tPortsOfCall( int expeditionPortID, int expeditionID, int portNumber, string portOfCallName )
		{
			this._expeditionPortID	= expeditionPortID;
			this._expeditionID		= expeditionID;
			this._portNumber		= portNumber;
			this._portOfCallName	= portOfCallName;
		}
	}

	#endregion	
}
