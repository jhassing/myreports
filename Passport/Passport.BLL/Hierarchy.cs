using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

using Compass.Data.MSSQLDataAccess;

using Passport.DAL;

namespace Passport.BLL
{
    public class SectorList
    {
        private string _sector;
        private string _sectorName;

        public SectorList(string sector, string sectorName)
        {
            this._sector = sector;
            this._sectorName = sectorName;
        }

        public string Sector
        { get { return this._sector; } }

        public string SectorName
        { get { return this._sectorName; } }    

        public static int Compare(SectorList x, SectorList y)
        {
            return new CaseInsensitiveComparer().Compare(x.SectorName, y.SectorName);            
        }
    }

    public struct DivisionList
    {
        private string _division;
        private string _divisionName;

        public DivisionList(string division, string divisionName)
        {
            this._division = division;
            this._divisionName = divisionName;
        }

        public string Division
        { get { return this._division; } }

        public string DivisionName
        { get { return this._divisionName; } }

        public static int Compare(DivisionList x, DivisionList y)
        {
            return new CaseInsensitiveComparer().Compare(x.DivisionName, y.DivisionName);
        }

    }

    public class Hierarchy : BaseBLL
    {
        #region ~ctor

        public Hierarchy(string connectionString)
            : base(connectionString)
        {
        }

        #endregion

        #region Get Sector Code Listing

        public List<SectorList> GetSectorListing()
        {
            List<SectorList> ary = new List<SectorList>();

            try
            {
                Passport.DAL.Hierarchy dal = new Passport.DAL.Hierarchy(base._connectionString);

                SqlDataReader dr = dal.GetSectorListing();

                while (dr.Read())
                {
                    string sector = dr["Sector"].ToString();
                    string sectorName = dr["SectorName"].ToString();
                    
                    ary.Add(new SectorList(sector, sectorName));

                }

                dal.CloseDBConnection();

                dal = null;

                ary.Sort(SectorList.Compare);

                return ary;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        #endregion

        #region Get Division Code Listing

        public List<DivisionList> GetDivisionListing()
        {
            List<DivisionList> ary = new List<DivisionList>();

            try
            {
                Passport.DAL.Hierarchy dal = new Passport.DAL.Hierarchy(base._connectionString);

                SqlDataReader dr = dal.GetDivisionListing();

                while (dr.Read())
                {
                    string division = dr["Division"].ToString();
                    string divisionName = dr["DivisionName"].ToString();

                    ary.Add(new DivisionList(division, divisionName));
                }

                dal.CloseDBConnection();

                dal = null;

                ary.Sort(DivisionList.Compare);

                return ary;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        #endregion

    }
}
