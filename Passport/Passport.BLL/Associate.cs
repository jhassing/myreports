using System;   
using System.Data.SqlClient;
using System.Collections;

using Compass.Security.ActiveDirectory;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Common;

using Passport.DAL;

namespace Passport.BLL
{	
	/// <summary>
	/// Handles all business logic for associates
	/// </summary>
	public class Associate
	{
		private string m_sConnectionString;
		
		#region ~ctor

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public Associate(string sConnectionString)
		{
			this.m_sConnectionString = sConnectionString;
		}

		#endregion

		#region Add New Associate

		/// <summary>
		/// Insert a new Associate into the database, and return the pk ID for the record
		/// </summary>
		/// <param name="operationNumber">Operation the Associate is a member of.</param>
		/// <param name="assocPerNum">Associates personnel number</param>
		/// <param name="firstName">Associates first name</param>
		/// <param name="lastName">Associates last name</param>
		/// <param name="isActive">Is the Associate active</param>
		/// <param name="isCompassAssoc">Is the associate a compass user</param>
		/// <returns>PK for the newly added associate</returns>
		public int InsertNewAssociate(	int operationNumber, string assocPerNum, string firstName, string lastName, 
										bool isActive, bool isCompassAssoc )
		{
			// The personnel number must be 8 char long
			if ( assocPerNum.Length != 8 )
				throw new ArgumentException( "The Personnel number must be 8 numbers" );

			try
			{
				Passport.DAL.Associate oAssociate = new Passport.DAL.Associate( this.m_sConnectionString );

				return oAssociate.InsertNewAssociate( operationNumber, assocPerNum, firstName, lastName, isActive, isCompassAssoc );  
			}
			catch (Exception ex)
			{
				throw new Exception( "Error inserting new user with personnel number " + assocPerNum, ex);
			}		
		}

		#endregion

		#region Delete Associate
		
		/// <summary>
		/// Deletes an assoicate from the database
		/// </summary>
		/// <param name="assocId">Primary Key of the associate to delete</param>
		public void DeleteAssociate( int assocId )
		{
			try
			{
				Passport.DAL.Associate oAssociate = new Passport.DAL.Associate( this.m_sConnectionString );

				oAssociate.DeleteAssociate( assocId );
			}
			catch (Exception ex)
			{
				throw new Exception( "Error deleting associate from the database", ex );
			}
		}


		#endregion

		#region Modify Associate

		/// <summary>
		/// Method will modify an Associates data
		/// </summary>
		/// <param name="assocId">Primary Key of the associate to modify</param>
		/// <param name="operationNumber">Operation the Associate is a member of.</param>
		/// <param name="firstName">Associates first name</param>
		/// <param name="lastName">Associates last name</param>
		public void ModifyAssociate( int assocId, int operationNumber, string firstName, string lastName )
		{
			try
			{
				Passport.DAL.Associate oAssociate = new Passport.DAL.Associate( this.m_sConnectionString );

				oAssociate.ModifyAssociate( assocId, operationNumber, firstName, lastName );  
			}
			catch (Exception ex)
			{
				throw new Exception( "Error modifying user", ex );
			}	
		}

		#endregion

		#region Set Associate active flag
		
		/// <summary>
		/// Sets an associate to active or not
		/// </summary>
		/// <param name="assocId">Primary Key of the associate to modify</param>
		/// <param name="active">is this user active?</param>
		public void SetAssociateActive( int assocId, bool active )
		{

			try
			{
				Passport.DAL.Associate oAssociate = new Passport.DAL.Associate( this.m_sConnectionString );

				oAssociate.SetAssociateActive( assocId, active );
			}
			catch (Exception ex)
			{
				throw new Exception( "Error setting associates active flag", ex );
			}
	
		}

		#endregion

		#region Get Non Compass Associates

		/// <summary>
		/// Method will get a listing of all associates based on unit numbers
		/// </summary>
		/// <param name="operationNumber">Operations number</param>
		/// <returns></returns>
		public ArrayList GetAssociates_ByOperationsNumber( int operationNumber, int expeditionID )
		{
			ArrayList aryReturnListAssociates = new ArrayList();

			try
			{
				Passport.DAL.Associate oAssociate = new Passport.DAL.Associate( this.m_sConnectionString );
			
				// Get the Associates, based on operation number
				using ( SqlDataReader dataReader = oAssociate.Get_NonCompassAssociates( operationNumber ))
				{
					// Iterate through DataReader
					while (dataReader.Read())
					{	
						// First, get the ID, first name, last name and personnel number of this associate
						int		assocID					=	System.Convert.ToInt32( dataReader["AssocID"].ToString() );
						string	firstName				=	dataReader["AssocFName"].ToString().Trim();
						string	lastName				=	dataReader["AssocLName"].ToString().Trim();
						string	personnelNumber			=	dataReader["AssocPerNum"].ToString().Trim();
						bool	activeFlag				=   System.Convert.ToBoolean( dataReader["ActiveFlag"] );
						bool	compassAssoc			=	System.Convert.ToBoolean( dataReader["CompassAssocFlag"] );						

						aryReturnListAssociates.Add( new tAssociates( 
																		assocID,			// Primary Key 
																		personnelNumber,	// Personnel Number 
																		operationNumber,	// Operations Number					
																		firstName,			// First Name
																		lastName,			// Last Name
																		activeFlag,			// Is the associate active?
																		compassAssoc ));		// Is the associate a compass employee?
					}
				}				

				return aryReturnListAssociates;
				
			}
			catch(Exception ex)
			{	
				throw( new Exception( "Error getting Completed listing of Associates for operations number: " + operationNumber, ex) );
			}	


		}

		#endregion

		#region Get Associate by Assoc ID

		/// <summary>
		/// Method will lookup a single associate based on AssocID (Primary Key)
		/// </summary>
		/// <param name="assocID">Primary Key of an associate to locate inside the database</param>
		public tAssociates GetAssociateByAssocID (	int assocID )
		{	
			string assocPerNum		=	""; 
			int operationNumber		=	0;
			string assocFirstName	=	"";
			string assocLastName	=	"";
			bool compassAssocFlag	=	false;
			bool activeFlag			=	false;

			try
			{
				new Passport.DAL.Associate( this.m_sConnectionString ).GetAssociateByAssocID(	assocID,
																							ref assocPerNum,
																							ref operationNumber,
																							ref assocFirstName,
																							ref assocLastName,
																							ref compassAssocFlag,
																							ref activeFlag );

				return new tAssociates( assocID,
										assocPerNum,   
										operationNumber,	
										assocFirstName, 
										assocLastName,	
										activeFlag, 
										compassAssocFlag ); 

										

			}
			catch (Exception ex)
			{
				throw new Exception( "Error getting single Associate by AssocID ", ex );
			}
		}

		#endregion

		#region Generate a new PersonnelNumber

		public string GenerateNewPersonnelNumber()
		{
			try
			{
				string lastUsed = new Passport.DAL.Associate( this.m_sConnectionString ).GetLastUsedNonCompassPersonnelNumber();

				int temp = System.Convert.ToInt32( lastUsed.Substring(1, 7) ) + 1;

				string temp1 = temp.ToString().PadLeft(7,'0');

				return "9" + temp1;


			}
			catch (Exception ex)
			{
				throw new Exception( "Error generating a new Personnel Number ", ex );
			}
		}

		#endregion

		#region Get the listing of operation numbers from ZROPS based on the users group membership in AD

		/// <summary>
		/// Function will return all the Operation Code that this user can view based on group membership in AD
		/// </summary>
		/// <param name="samAccountName"></param>
		/// <returns></returns>
		public ArrayList GetOperationListing( string distinguishedName )
		{

			IUserObject	oADUser = null;
			ArrayList aryRet = new ArrayList();
			LDAPConnection ldapConn = null;


			try
			{
				ldapConn = new LDAPConnection(	AppValues.ServiceAccountUser, 
					AppValues.ServiceAccountPassword,
					AppValues.FullyQualifiedDomain,
					AppValues.LdapPort );

				oADUser  = new ADUserObject( ldapConn );

				ArrayList ary = new ArrayList();
				ary.Add( "samAccountName" );


				ArrayList aryMem = oADUser.GetUsersMembership ( distinguishedName, false );

				// Get a reference to the operations obejct
				Operations oOperations = new Operations( this.m_sConnectionString );

				foreach ( string groupName in aryMem )
				{
					int n = 0;

					n = groupName.ToLower().IndexOf( "_grp" );

					if ( n == -1 )
						n = groupName.ToLower().IndexOf( "_pays" );

					if ( n == -1 )
						n = groupName.ToLower().IndexOf( "_payh" );
                  
                    // if (n > 0)  // 01/13/09 changed to n>2 to bypass new div/sector codes.   
                    if (n > 2)  
					{
						string group = groupName.Substring( 0,  n );
		
						ArrayList aryOps = new ArrayList();

						// Need to find out if this is a Region Code, DM code or a Unit Code

						//if ( group.Length == 3)
                        if (group.Length ==  3)
						{
							// This is a region code
							aryOps = oOperations.GetOperationsFromRegionCode( group );	
                        }
                        //else if ( group.Length == 2 )
                        //    // This is a Divison code no action taken
                                                
                        else if ( group.Length == 5 )
						{		
							if ( Compass.Common.Functions.IsNumeric( group ))
							{
								// Must be a 5 digit Unit
								aryOps.Add( group );
							}
							else
							{
								// Now, get all the Operations Numbers this DM group can see						
								aryOps = oOperations.GetOperationsFromDMCode( group );	
							}
						}
						else 
						{
							// check to see if we can find this under the OPS code
							aryOps.Add( group );


						}

						// add the operations codes to the return arraylist
						foreach( string temp in aryOps )
						{
							// Check to see if it already exisst inside the arraylist
							if ( !aryRet.Contains( temp ) )
							{
								// Does not exist.. add it
								aryRet.Add( temp );
							}
						}
					}
				}
			
				return aryRet;
			}
			catch (Exception ex)
			{
				throw new Exception( "Error getting Second line managers information from the Domain", ex );
			}
			finally
			{
				if ( ldapConn != null ) 
					ldapConn.Disconnect();

				oADUser = null;
			}
		}

		#endregion



	}
}