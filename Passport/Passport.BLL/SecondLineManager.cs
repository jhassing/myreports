using System;
using System.Web;
using System.Web.SessionState;
using System.Threading;
using System.Globalization;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Security.Principal;
using System.Resources;
using System.Text;

using Compass.Security.ActiveDirectory;
using Compass.Configuration.AppSettings;

using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Common;

using Passport.DAL;

namespace Passport.BLL
{
	public class SecondLineManager : General
	{
		public static bool LocateSecondLineManager( string SecondLineManagersNetworkID, ref Hashtable htParams )
		{
			// get the user name, password and the domain name.

			IUserObject	oADUser = null;
			LDAPConnection ldapConn = null;

			try
			{
				ldapConn = new LDAPConnection(	AppValues.ServiceAccountUser, 
												AppValues.ServiceAccountPassword,
												AppValues.FullyQualifiedDomain,
												AppValues.LdapPort );

				oADUser  = new ADUserObject( ldapConn );
				
				//string sDN = oADUser.GetObjectsDistinguishedName( SecondLineManagersNetworkID );

				//if ( sDN.Length == 0 )
					return false;

				//return oADUser.GetUserProperties( sDN, ref htParams );												 

				return true;
			}
			catch (Exception ex)
			{
				throw new Exception( "Error getting Second line managers information from the Domain", ex );
			}
			finally
			{

				if ( ldapConn != null ) 
					ldapConn.Disconnect();

				oADUser = null;
			}
		}

	}
}
