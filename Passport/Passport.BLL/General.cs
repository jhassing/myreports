using System;
using System.Web;
using System.Web.SessionState;
using System.Threading;
using System.Globalization;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Security.Principal;
using System.Resources;
using System.Text;

using Compass.Security.ActiveDirectory;
using Compass.Configuration.AppSettings;

using Microsoft.ApplicationBlocks.ExceptionManagement;

using Passport.DAL;

namespace Passport.BLL
{

	public class General
	{													
		#region Domain Name

		public static string DomainName
		{
			get
			{
				try
				{
					return ConfigKey( "General/DomainSettings", "FullyQualifiedDomainName" );
				}
				catch (Exception ex)
				{
					throw new Exception( "Error getting DomainName from the config file", ex );
				}
			}
		}

		#endregion

		#region Domain Name

		public static int LdapPort
		{
			get
			{
				try
				{
					return System.Convert.ToInt32( ConfigKey( "General/DomainSettings", "LdapPort" ) );
				}
				catch (Exception ex)
				{
					throw new Exception( "Error getting DomainName from the config file", ex );
				}
			}
		}

		#endregion

		#region Domain User

		public static string DomainUser
		{
			
			get
			{
				try
				{
					return ConfigKey( "General/AccessControl", "ServiceAccountUser" );
				}
				catch (Exception ex)
				{
					throw new Exception( "Error getting Domain user service account name from the config file", ex );
				}
			}
		}

		#endregion

		#region Domain Password

		public static string DomainPassword
		{
			get
			{
				try
				{
					return ConfigKey( "General/AccessControl", "ServiceAccountPassword" );;
				}
				catch (Exception ex)
				{
					throw new Exception( "Error getting Domain password for the service account from the config file", ex );
				}
			}
		}


		#endregion


		#region Base DB Settings
		private static string getBaseDBSettings(string section)
		{
			
			
			string conn = "packet size=4096;persist security info=True;data source=" + ConfigKey("General/DatabaseServer", "Name") + ";";

			string userID = "user id=" + ConfigKey(section + "/Database", "UserID") + ";";
			string password = "password=" + ConfigKey(section + "/Database", "Password") + ";";
			string databaseName = "initial catalog=" + ConfigKey(section + "/Database", "DatabaseName") + ";";
				
			string sValue = conn + userID + password + databaseName;

			return sValue;
		}
		#endregion

		#region Passport database connection string

		public static string Passport_DB_ConnectionString
		{
			get
			{					
				string sValue = System.Convert.ToString( HttpContext.Current.Cache["PassportDBString"] );

				if (sValue == null || sValue == "")
				{
					sValue = getBaseDBSettings("Passport");
					HttpContext.Current.Cache["PassportDBString"] = sValue;
				}
				  	
				return sValue;
	
			}
		}

		#endregion

		#region Report Distribution database connection string
		public static string RD_DB_ConnectionString
		{
			get
			{					
				string sValue = System.Convert.ToString( HttpContext.Current.Cache["ReportDistributionDBString"] );

				if (sValue == null || sValue == "")
				{
					sValue = getBaseDBSettings("ReportDistribution");
					HttpContext.Current.Cache["ReportDistributionDBString"] = sValue;
				}
				  	
				return sValue;
	
			}
		}
		#endregion


		#region SMTP

		public static string SMTP
		{
			get
			{
				try
				{
					return "Relay.Compass-USA.com";
				}
				catch (Exception ex)
				{
					throw new Exception( "Error getting SMTP Server from the config file", ex );
				}
			}
		}


		#endregion

		#region FormatPathForWebConfigFile

		protected static string FormatPathForWebConfigFile
		{
			get
			{
				string returnPath = "";

				string[] path = HttpContext.Current.Request.PhysicalApplicationPath.Split('\\');

				if ( path.Length > 2 )
				{
					for (int x=0;x<path.Length-3;x++)
					{
						returnPath+=path[x] + @"\";
					}
				}
				
				return returnPath + "web.config";
			}
				
		}

		#endregion

		#region Config Key Settings

		public static string ConfigKey(string sSection, string sKey)
		{
			try
			{
				string sSecKey = sSection + "/" + sKey;
				string sValue = System.Convert.ToString(HttpContext.Current.Cache[sSecKey]);

				if (sValue == null || sValue == "")
				{
					sValue = Compass.Configuration.AppSettings.AppSettingsReader.ReadKeyValue( General.FormatPathForWebConfigFile, sSection, sKey );

					HttpContext.Current.Cache.Insert(sSecKey, sValue);
				}

				return(sValue);
			}
			catch (Exception ex)
			{
				NameValueCollection nv = new NameValueCollection();

				nv.Add("Section", sSection);
				nv.Add("Key", sKey);				

				ExceptionManager.Publish ( new Exception("Error getting config key values", ex), nv );

				throw (ex);
			}		
		}

		#endregion
	}
}
