using System;   
using System.Data.SqlClient;
using System.Collections;

using Compass.Security.ActiveDirectory;
using Microsoft.ApplicationBlocks.ExceptionManagement;

using Passport.DAL;

namespace Passport.BLL
{
	#region Struct Expeditions

	[Serializable]
	public struct tExpeditions
	{
		public	int				_expeditionID;
		private string			_expeditionName;
		private string			_description;

		#region Properties

		public int ExpeditionID
		{
			get
			{
				return this._expeditionID;
			}
		}


		public string ExpeditionName
		{
			get 
			{
				return this._expeditionName;
			}
		}


		public string Description
		{
			get 
			{
				return this._description;
			}
		}


		#endregion

		public tExpeditions( int expeditionID, string expeditionName, string description  )
		{
			this._expeditionID = expeditionID;
			this._expeditionName = expeditionName;
			this._description = description;
		}
	}

	#endregion

	/// <summary>
	/// Class will handle all business logic associated with Expeditions.
	/// </summary>
	public class Expedition
	{
		private string m_sConnectionString;

		#region ~ctor

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public Expedition(string sConnectionString)
		{
			m_sConnectionString = sConnectionString;
		}

		#endregion

		#region Get Expeditions	Listing

		public ArrayList GetExpeditionListing()
		{
			ArrayList aryReturn = new ArrayList();

			try
			{
				Passport.DAL.Expedition oExpedition = new Passport.DAL.Expedition( m_sConnectionString );

				using ( SqlDataReader dataReader = oExpedition.GetExpeditionListing() )
				{
					// Iterate through DataReader
					while (dataReader.Read())
					{
						// First, get the ID, first name, last name and personnel number of this associate
						int		expeditionId		=	System.Convert.ToInt32( dataReader["ExpeditionId"].ToString() );
						string	expeditionName		=	dataReader["ExpeditionName"].ToString();
						string	description			=	dataReader["Description"].ToString();

						aryReturn.Add( new tExpeditions( expeditionId, expeditionName, description ) );

					}
				}

				return aryReturn;
					
			}
			catch(Exception ex)
			{	
				throw( new Exception( "Error getting Expeditions listing", ex) );
			}	
		}

		#endregion

		#region Get Ports of call count for a particular expedition
		
		public int GetCount_Expedition_PortsOfCall( int expeditionId )
		{
			int portCount = 0;

			try
			{
				Passport.DAL.Expedition oExpedition = new Passport.DAL.Expedition( m_sConnectionString );

				using ( SqlDataReader dataReader = oExpedition.GetCount_Expedition_PortsOfCall( expeditionId ) )
				{
					// Iterate through DataReader
					while (dataReader.Read())
						portCount =	System.Convert.ToInt32( dataReader["PortCount"].ToString() );
				}

				return portCount;
					
			}
			catch(Exception ex)
			{	
				throw( new Exception( "Error getting Expeditions port count", ex) );
			}	
		}
		
		#endregion

		#region Insert new Expedition
		/// <summary>
		/// Insert a new Expedition into the database, and return the pk ID for the record
		/// </summary>
		/// <param name="expeditionName">Expedition name</param>
		/// <param name="expeditionDesc">Expedition Description</param>
		/// <param name="secondLineManagersNetworkID">Manager's network id</param>
		/// <returns>PK for the newly added Expedition</returns>
		public int InsertNewExpedition( string expeditionName, string expeditionDesc, string secondLineManagersNetworkID )
		{
			try
			{
				Passport.DAL.Expedition oExpedition = new Passport.DAL.Expedition( m_sConnectionString );
				
				return oExpedition.InsertNewExpedition( expeditionName, expeditionDesc, secondLineManagersNetworkID );
				
			}
			catch(Exception ex)
			{
				throw new Exception( "Error Inserting Expedition: " + expeditionName, ex );
			}
		}

		#endregion

		#region Modify existing Expedition
		/// <summary>
		/// Method will modify an Expedition data
		/// </summary>
		/// <param name="expedId">Primary Key of the Expedition to modify</param>
		/// <param name="expedName">Expedition name</param>
		/// <param name="expedDesc">Expedition description</param>
		public void ModifyExpedition( int expeditionId, string expeditionName, string expeditonDesc )
		{
			try
			{
				Passport.DAL.Expedition oExpedition = new Passport.DAL.Expedition( m_sConnectionString );
				oExpedition.ModifyExpedition(expeditionId, expeditionName, expeditonDesc);
				
			}
			catch(Exception ex)
			{
				throw new Exception( "Error Modifying existing Expedition: " + expeditionName, ex );
			}
		}
		#endregion

		#region Delete exsiting Expedition
		/// <summary>
		/// Deletes an expedition from the database
		/// </summary>
		/// <param name="expedId">Primary Key of the Expedition to delete</param>
		public void DeleteExpedition( int expeditionId )
		{
			try
			{
				Passport.DAL.Expedition oExpedition = new Passport.DAL.Expedition( m_sConnectionString );
				oExpedition.DeleteExpedition( expeditionId );
				
			}
			catch(Exception ex)
			{
				throw new Exception( "Error Deleting Expedition", ex );
			}
		}
		#endregion


	}
}
