using System;   
using System.Data.SqlClient;
using System.Collections;

using Compass.Security.ActiveDirectory;
using Microsoft.ApplicationBlocks.ExceptionManagement;

using Passport.DAL;

namespace Passport.BLL
{
	/// <summary>
	///  Handles all business logic actions for Associates and their ports of call
	/// </summary>
	public class AssociatePortsOfCall
	{
		private string m_sConnectionString;
		
		#region ~ctor

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public AssociatePortsOfCall(string sConnectionString)
		{
			m_sConnectionString = sConnectionString;
		}

		#endregion

		#region Get Associates Completed Ports Of Call

		/// <summary>
		/// Method will get all the Completed Ports of Call for this Associate
		/// </summary>
		/// <param name="unitNumber">Unit which this Associate was a member of when the port of call was completed.</param>
		/// <param name="activeFlag">Flag indicated if this User is active or not</param>
		/// <returns></returns>
		public ArrayList GetAssociateCompletedPortsOfCall( int expeditionID, int operationNumber )
		{
			ArrayList aryReturnPortsOfCall = new ArrayList();

			try
			{
				Passport.DAL.AssociatePortsOfCall oAssociatePortsOfCall = new Passport.DAL.AssociatePortsOfCall( m_sConnectionString );
				Passport.DAL.Associate oAssociate = new Passport.DAL.Associate( m_sConnectionString );

				// Get the Associates, based on operation number
				using ( SqlDataReader dataReader = oAssociate.GetAssociates_ByOperationsNumber_ExpeditionNumber( operationNumber, expeditionID ))
				{
					// Iterate through DataReader
					while (dataReader.Read())
					{	
						// First, get the ID, first name, last name and personnel number of this associate
                        int assocID = System.Convert.ToInt32(dataReader["AssocID"].ToString());
                        string assocPerNum = dataReader["AssocPerNum"].ToString();
                        string firstName = dataReader["AssocFName"].ToString();
                        string lastName = dataReader["AssocLName"].ToString();
                        bool activeFlag = System.Convert.ToBoolean(dataReader["ActiveFlag"]);
                        bool compassEmployee = System.Convert.ToBoolean(dataReader["CompassAssocFlag"]);

						// Now, based on the associates ID, get the Ports of Call that this Associate has already completed
						ArrayList aryPortsOfCall = new ArrayList();

						using ( SqlDataReader drPortsOfCall = oAssociatePortsOfCall.Get_Associate_PortsOfCall( assocID, expeditionID  ) )
						{							
							while (	drPortsOfCall.Read() )
							{						
								DateTime dateCompleted	=	(DateTime)drPortsOfCall["CompletionDate"];
								int portOfCallNumber	=	System.Convert.ToInt32( drPortsOfCall["PortNumber"] );

								aryPortsOfCall.Add ( new tPortOfCall( portOfCallNumber, dateCompleted ) );
							}
						}

						aryReturnPortsOfCall.Add( new tAssociateOperation( assocID, assocPerNum, firstName + " " + lastName, activeFlag, compassEmployee, aryPortsOfCall ) );
					}
				}				

				return aryReturnPortsOfCall;
				
			}
			catch(Exception ex)
			{	
				throw( new Exception( "Error getting Completed ports of call listing for unit " + operationNumber, ex) );
			}			
		}


		#endregion

		#region Insert Associates Completed Ports Of Call

		/// <summary>
		/// Method will insert the completed ports of call for the user
		/// </summary>
		/// <param name="assocID">Associate Primary Key</param>
		/// <param name="operationNumber">Operation number the user belongs to when they complete the port of call training</param>
		/// <param name="userNetworkID">Network ID of the user who is entering the data</param>
		/// <param name="portsOfCall">Arraylist of PortsOfCall that were completed and the date which they were completed</param>
		public void InsertAssociatesCompletedPortsOfCall( int assocID, int expeditionID, int operationNumber, string userNetworkID, ArrayList portsOfCall )
		{
			try
			{
				Passport.DAL.AssociatePortsOfCall oAssociatePortsOfCall = new Passport.DAL.AssociatePortsOfCall( m_sConnectionString );

				// Loop through each port of call
				for (int x=0;x<portsOfCall.Count;x++)
				{
					int portNumber = 0;
					DateTime dtCompleted = System.DateTime.Now;

					try
					{
						portNumber = ((tPortOfCall)portsOfCall[x]).PortOfCallNumber;
						dtCompleted = ((tPortOfCall)portsOfCall[x]).DateCompleted;

						oAssociatePortsOfCall.InsertAssociatesCompletedPortsOfCall(	
													expeditionID,			// Expedition Number 
													portNumber,				// Port Number						
													assocID,				// Associate Primary Key													
													operationNumber,		// Operations Number	
													dtCompleted,			// Date Completed
													userNetworkID );			// Users samAccountName
													
																
					}
					catch(Exception ex)
					{	

						System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();
						nv.Add( "Operation Number", operationNumber.ToString() );
						nv.Add( "User Network ID", userNetworkID );
						nv.Add( "Port Number", portNumber.ToString() );
						nv.Add( "Date Port Completed", dtCompleted.ToString( "d" ) );

						ExceptionManager.Publish( new Exception( "Error inserting Completed ports of call", ex), nv );
					}

				}	
			}
			catch(Exception ex)
			{	
				throw( new Exception( "Error inserting Completed ports of call listing for unit " + operationNumber, ex) );
			}
		}

		
		#endregion

		#region Remove Associate Completed Ports Of Call

		/// <summary>
		/// Removes a particular Associate from a completed port of call
		/// </summary>
		/// <param name="assocID">Associate ID</param>
		/// <param name="expeditionID">Expedition ID of the current Port of Call</param>
		/// <param name="operationNumber">Operation number the user belongs to when they complete the port of call training</param>		
		/// <param name="portsOfCallNumbers">Listing of the removed ports of call</param>
		public void RemoveAssociateCompletedPortsOfCall( int assocID, int expeditionID, int operationNumber , ArrayList portsOfCallNumbers )
		{
			try
			{
				Passport.DAL.AssociatePortsOfCall oAssociatePortsOfCall = new Passport.DAL.AssociatePortsOfCall( m_sConnectionString );

				// Loop through each port of call
				for (int x=0;x<portsOfCallNumbers.Count;x++)
				{
					int portNumber = 0;

					try
					{
						portNumber = System.Convert.ToInt32( portsOfCallNumbers[x].ToString() );

						oAssociatePortsOfCall.RemoveAssociateCompletedPortsOfCall(	
									assocID,				// Associates primary key
									expeditionID,			// Expedition ID
									operationNumber,		// Operations Number
									portNumber );			// Port Number
					}
					catch(Exception ex)
					{	

						System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();
						nv.Add( "Operation Number", operationNumber.ToString() );
						nv.Add( "Port Number", portNumber.ToString() );

						ExceptionManager.Publish( new Exception( "Error Removing Completed ports of call", ex), nv );
					}
				}
			}
			catch(Exception ex)
			{	
				throw( new Exception( "Error Removing Completed ports of call listing for unit " + operationNumber, ex) );
			}
		}

		#endregion

		

	}
}
