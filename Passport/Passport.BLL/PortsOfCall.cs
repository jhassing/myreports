using System;   
using System.Data.SqlClient;
using System.Collections;

using Compass.Security.ActiveDirectory;
using Microsoft.ApplicationBlocks.ExceptionManagement;

using Passport.DAL;

namespace Passport.BLL
{
	/// <summary>
	/// Handles all business logic for Ports Of Call
	/// </summary>
	public class PortsOfCall
	{
		private string m_sConnectionString;
		
		#region ~ctor

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public PortsOfCall(string sConnectionString)
		{
			this.m_sConnectionString = sConnectionString;
		}

		#endregion

		#region Get Ports Of Call listing for an Expedition

		public ArrayList Get_PortsOfCall_By_Expedition( int expeditionID )
		{

			ArrayList aryReturnListPortsOfCall = new ArrayList();

			try
			{
				Passport.DAL.PortsOfCall oPortsOfCall = new Passport.DAL.PortsOfCall( this.m_sConnectionString );

				// Get the Ports of Call, based on Expedition number
				using ( SqlDataReader dataReader = oPortsOfCall.Get_PortsOfCall_By_Expedition( expeditionID  ))
				{
					// Iterate through DataReader
					while (dataReader.Read())
					{	
						// First, get the ID, first name, last name and personnel number of this associate
						int		expeditionPortID		=	System.Convert.ToInt32( dataReader["ExpeditionPortID"].ToString() );
						int		portNumber				=	System.Convert.ToInt32( dataReader["PortNumber"].ToString() );
						string	portName				=	dataReader["PortName"].ToString();
						

						aryReturnListPortsOfCall.Add( new tPortsOfCall( expeditionPortID, 
																		expeditionID,
																		portNumber,
																		portName ) );
					}
				}
	
				return aryReturnListPortsOfCall;				

			}
			catch(Exception ex)
			{	
				throw new Exception( "Error getting Ports of call listing for expedition ID: " + expeditionID, ex );
			}
		
	

		}

		#endregion

		#region Insert New Port of Call for an Expedition
		/// <summary>
		/// Insert a new Port of Call into the database for an Expedition, and return the pk ID for the record
		/// </summary>
		/// <param name="expeditionID">Expedition ID</param>
		/// <param name="pocName">Port of Call Name</param>
		/// <param name="pocNumber">Port of Call Number</param>
		/// <param name="secondLineManagersNetworkID">Manager's network id</param>
		/// <returns>PK for the newly added Port of Call</returns>
		public int InsertNewPortofCall( int expeditionID, string pocName, int pocNumber, string secondLineManagersNetworkID )
		{
			try
			{
				Passport.DAL.PortsOfCall oAssocPOC = new Passport.DAL.PortsOfCall( m_sConnectionString );
				
				return oAssocPOC.InsertNewPortofCall(expeditionID, pocName, pocNumber, secondLineManagersNetworkID);
				
			}
			catch(Exception ex)
			{
				throw new Exception( "Error Inserting new Port of Call: " + pocName, ex );
			}
		}
		#endregion

		#region Update Port of Call for an Expedition
		/// <summary>
		/// Method will modify a Port of Call
		/// </summary>
		/// <param name="pocId">Primary Key of the Port of Call to modify</param>
		/// <param name="pocName">Port of Call name</param>
		/// <param name="pocNumber">Port of Call description</param>
		public void ModifyPortOfCall( int pocId, string pocName, int pocNumber )
		{
			try
			{
				Passport.DAL.PortsOfCall oAssocPOC = new Passport.DAL.PortsOfCall( m_sConnectionString );
				
				oAssocPOC.ModifyPortOfCall( pocId, pocName, pocNumber );
				
			}
			catch(Exception ex)
			{
				throw new Exception( "Error Updating Port of Call: " + pocName, ex );
			}
		}
		#endregion

		#region Delete Port of Call
		/// <summary>
		/// Deletes a Port of Call from the database
		/// </summary>
		/// <param name="pocId">Primary Key of the Port of Call to delete</param>
		public void DeletePortOfCall( int pocId )
		{
			try
			{
				Passport.DAL.PortsOfCall oAssocPOC = new Passport.DAL.PortsOfCall( m_sConnectionString );
				
				oAssocPOC.DeletePortOfCall( pocId );
				
			}
			catch(Exception ex)
			{
				throw new Exception( "Error Deleting Port of Call: ", ex );
			}
		}
		#endregion


	}
}
