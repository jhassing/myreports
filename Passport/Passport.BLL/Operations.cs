using System;   
using System.Data.SqlClient;
using System.Collections;

using Compass.Security.ActiveDirectory;
using Microsoft.ApplicationBlocks.ExceptionManagement;

using Passport.DAL;

namespace Passport.BLL
{
	/// <summary>
	/// Class will handle all business logic for Operations
	/// </summary>
	public class Operations
	{

		private string m_sConnectionString;

		#region ~ctor

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public Operations(string sConnectionString)
		{
			m_sConnectionString = sConnectionString;
		}

		#endregion

		#region Get Operations Code for this Region

		public ArrayList GetOperationsFromRegionCode( string region )
		{
			ArrayList aryReturn = new ArrayList();

			try
			{
				Passport.DAL.Operations oOperations = new Passport.DAL.Operations( m_sConnectionString );

				using ( SqlDataReader dataReader = oOperations.GetOperationsFromRegionCode( region ) )
				{
					// Iterate through DataReader
					while (dataReader.Read())
						aryReturn.Add( dataReader["OperationCode"].ToString() );

				}

				return aryReturn;
					
			}
			catch(Exception ex)
			{	
				throw( new Exception( "Error getting operations listing", ex) );
			}

		}

		#endregion

		#region Get Operations for this DM group

		public ArrayList GetOperationsFromDMCode( string DM )
		{
			ArrayList aryReturn = new ArrayList();

			try
			{
				Passport.DAL.Operations oOperations = new Passport.DAL.Operations( m_sConnectionString );

				using ( SqlDataReader dataReader = oOperations.GetOperationsFromDMCode( DM ) )
				{
					// Iterate through DataReader
					while (dataReader.Read())
						aryReturn.Add( dataReader["OperationCode"].ToString() );

				}

				return aryReturn;
					
			}
			catch(Exception ex)
			{	
				throw( new Exception( "Error getting operations listing", ex) );
			}	
		}


		#endregion
	}
}
