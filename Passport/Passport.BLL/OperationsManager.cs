using System;
using System.Web;
using System.Web.SessionState;
using System.Threading;
using System.Globalization;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Security.Principal;
using System.Resources;
using System.Text;

using Compass.Security.ActiveDirectory;
using Compass.Configuration.AppSettings;

using Microsoft.ApplicationBlocks.ExceptionManagement;

using MyReports.Common;

using Passport.DAL;


namespace Passport.BLL
{
	public class OperationsManager : General
	{


		private string m_sConnectionString;

		#region ~ctor

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="sConnectionString">Database connection string</param>
		public OperationsManager(string sConnectionString)
		{
			m_sConnectionString = sConnectionString;
		}

		#endregion

		public static bool LocateOperationsManager( string OpsManagerNetworkID, ref Hashtable htParams )
		{
			// get the user name, password and the domain name.

			IUserObject	oADUser = null;
			LDAPConnection ldapConn = null;

			try
			{
				 ldapConn = new LDAPConnection(	AppValues.ServiceAccountUser, 
					AppValues.ServiceAccountPassword,
					AppValues.FullyQualifiedDomain,
					AppValues.LdapPort );

				oADUser  = new ADUserObject( ldapConn );

				//string sDN = oADUser.GetObjectsDistinguishedName( OpsManagerNetworkID );

				//if ( sDN.Length == 0 )
					return false;

				//return oADUser.GetUserProperties( sDN, ref htParams );				
			
			}
			catch (Exception ex)
			{
				throw new Exception( "Error getting Operations manager from the Domain", ex );
			}
			finally
			{
				if ( ldapConn != null ) 
					ldapConn.Disconnect();

				oADUser = null;
			}
		}


	


	}
}
