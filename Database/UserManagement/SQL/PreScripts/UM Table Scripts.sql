USE ADImport
GO
ALTER TABLE [dbo].[tblADImport_Detail] DROP CONSTRAINT [PK_tblADImport_Detail] 
GO

ALTER TABLE [dbo].[tblADImport_Groups] DROP CONSTRAINT [PK_tblADImport_Groups] 
GO

ALTER TABLE [dbo].[tblADImport_RejectReasons] DROP CONSTRAINT [PK_tblADImport_Reject] 
GO

ALTER TABLE [dbo].[tblADImport_Groups]DROP CONSTRAINT [DF_tblADImport_Groups_PKADImport_GroupsID] 
GO

ALTER TABLE [dbo].[tblADImport_Master] DROP	CONSTRAINT [DF_tblADImport_Master_PKADImport_MasterID] 
GO
ALTER TABLE [dbo].[tblADImport_Master] DROP	CONSTRAINT [DF_tblADImport_Master_RequestedDate] 
GO
ALTER TABLE [dbo].[tblADImport_Master] DROP	CONSTRAINT [DF_tblADImport_Master_BeenImported] 
GO
ALTER TABLE [dbo].[tblADImport_Master] DROP	CONSTRAINT [DF_tblADImport_Master_ImportedDate] 
GO

ALTER TABLE [dbo].[tblADImport_Master] ALTER Column PKADImport_MasterID DROP ROWGUIDCOL
GO

ALTER TABLE [dbo].[tblADImport_Detail] DROP	CONSTRAINT [FK_tblADImport_Detail_tblADImport_Master] 
GO

ALTER TABLE [dbo].[tblADImport_Groups] DROP CONSTRAINT [FK_tblADImport_Groups_tblADImport_Master] 
GO

ALTER TABLE [dbo].[tblADImport_RejectReasons] DROP CONSTRAINT [FK_tblADImport_Reject_tblADImport_Master] 
GO
ALTER TABLE [dbo].[tblADImport_Master] DROP	CONSTRAINT [PK_tblADImport_Master] 
GO
sp_rename 'tblADImport_Detail.FKADImport_MasterID', 'MasterID'
GO
sp_rename 'tblADImport_Detail.PKADImport_DetailID', 'DetailID'
GO
sp_rename 'tblADImport_Groups.FKADImport_MasterID', 'MasterID'
GO
sp_rename 'tblADImport_Groups.PKADImport_GroupsID', 'GroupID'
GO
sp_rename 'tblADImport_Master.ImportedDate', 'ImportedDateTime'
GO
sp_rename 'tblADImport_Master.PKADImport_MasterID', 'MasterID'
GO
sp_rename 'tblADImport_Master.RequestedDate', 'RequestedDateTime'
GO
sp_rename 'tblADImport_RejectReasons.FKADImport_MasterID', 'MasterID'
GO
sp_rename 'tblADImport_RejectReasons.PKADImport_Reject', 'RejectReasonID'
GO
sp_rename 'tblADImport_RejectReasons.RejectDate', 'RejectDateTime'
GO
sp_rename 'tblAdImport_Detail', 'Detail'
GO
sp_rename 'tblAdImport_Groups', 'Group'
GO
sp_rename 'tblAdImport_Master', 'Master'
GO
sp_rename 'tblAdImport_RejectReasons', 'Reject_Reasons'
GO

ALTER TABLE [dbo].[Detail] WITH NOCHECK ADD 
	CONSTRAINT [PK_Detail] PRIMARY KEY  CLUSTERED 
	(
		[DetailID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Group] WITH NOCHECK ADD 
	CONSTRAINT [PK_Group] PRIMARY KEY  CLUSTERED 
	(
		[GroupID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Master] WITH NOCHECK ADD 
	CONSTRAINT [PK_Master] PRIMARY KEY  CLUSTERED 
	(
		[MasterID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Reject_Reasons] WITH NOCHECK ADD 
	CONSTRAINT [PK_RejectReasonID] PRIMARY KEY  CLUSTERED 
	(
		[RejectReasonID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Group] ADD 
	CONSTRAINT [DF_Group_GroupID] DEFAULT (newid()) FOR [GroupID]
GO

ALTER TABLE [dbo].[Master] ADD 
	CONSTRAINT [DF_Master_MasterID] DEFAULT (newid()) FOR [MasterID],
	CONSTRAINT [DF_Master_RequestedDateTime] DEFAULT (getdate()) FOR [RequestedDateTime],
	CONSTRAINT [DF_Master_BeenImported] DEFAULT (0) FOR [BeenImported],
	CONSTRAINT [DF_Master_ImportedDateTime] DEFAULT (getdate()) FOR [ImportedDateTime]
GO

ALTER TABLE [dbo].[Detail] ADD 
	CONSTRAINT [FK_Detail_Master] FOREIGN KEY 
	(
		[MasterID]
	) REFERENCES [dbo].[Master] (
		[MasterID]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[Group] ADD 
	CONSTRAINT [FK_Group_Master] FOREIGN KEY 
	(
		[MasterID]
	) REFERENCES [dbo].[Master] (
		[MasterID]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[Reject_Reasons] ADD 
	CONSTRAINT [FK_Reject_Master] FOREIGN KEY 
	(
		[MasterID]
	) REFERENCES [dbo].[Master] (
		[MasterID]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[Master] ALTER Column MasterID ADD ROWGUIDCOL
GO
sp_RenameDB 'ADImport', 'UserManagement'


