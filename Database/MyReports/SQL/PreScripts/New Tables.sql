if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblArchiveValues]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblArchiveValues]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblOverwriteValues]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblOverwriteValues]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblReportViewedDates]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblReportViewedDates]
GO

CREATE TABLE [dbo].[tblArchiveValues] (
	[ArchiveValuesPK] [int] IDENTITY (1, 1) NOT NULL ,
	[ArchiveValueCode] [int] NULL ,
	[ArchiveValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblOverwriteValues] (
	[OverwriteValuesPK] [int] IDENTITY (1, 1) NOT NULL ,
	[OverwriteCode] [int] NULL ,
	[OverwriteValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OverwriteDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblReportViewedDates] (
	[ReportViewedDatesPK] [int] IDENTITY (1, 1) NOT NULL ,
	[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ReportName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreationTime] [datetime] NULL ,
	[ViewedDate] [datetime] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblArchiveValues] WITH NOCHECK ADD 
	CONSTRAINT [PK_tblArchiveValues] PRIMARY KEY  CLUSTERED 
	(
		[ArchiveValuesPK]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[tblOverwriteValues] WITH NOCHECK ADD 
	CONSTRAINT [PK_tblOverrightValues] PRIMARY KEY  CLUSTERED 
	(
		[OverwriteValuesPK]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO


ALTER TABLE tblReports DROP CONSTRAINT PK_tblReports 
GO

Alter table tblReports  
            ALTER COLUMN ReportCode nvarchar (20) not null

GO
ALTER TABLE tblReports ADD CONSTRAINT PK_tblReports PRIMARY KEY CLUSTERED 
        ( [ReportCode] 
        ) WITH FILLFACTOR = 90 ON [PRIMARY] 
GO

insert into tblOverwriteValues (OverwriteCode, OverwriteValue, OverwriteDescription) values (0, 'No', 'Archives all received reports.')
insert into tblOverwriteValues (OverwriteCode, OverwriteValue, OverwriteDescription) values (1, 'Yes', 'Deletes reports run in the same archive period.')
insert into tblOverwriteValues (OverwriteCode, OverwriteValue, OverwriteDescription) values (2, 'Force', 'Always overwrite  reports.')


insert into tblArchiveValues (ArchiveValueCode, ArchiveValue) values (0, 'Months')
insert into tblArchiveValues (ArchiveValueCode, ArchiveValue) values (1, 'Weeks')
insert into tblArchiveValues (ArchiveValueCode, ArchiveValue) values (2, 'Daily')


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblReports]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblReports]
GO

CREATE TABLE [dbo].[tblReports] (
	[ReportCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ArchiveLength] [int] NULL ,
	[ReportTypeID] [int] NULL ,
	[ArchiveValue] [int] NULL ,
	[OverwriteValue] [int] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblReports] WITH NOCHECK ADD 
	CONSTRAINT [PK_tblReports] PRIMARY KEY  CLUSTERED 
	(
		[ReportCode]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('AQGL09', 	 0, 0,0,1)

Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCAP003D', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCAP004B', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCAP2222', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCAP2223', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCAR2140', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCAR2201', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCAR2280', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCCO0062', 	 3, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCFI2072', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCFIGL9060', 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCGL2370',   0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCGL2391',   0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZFLI',       13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPAC', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPAI', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPAN', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPAX', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPAZ', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPBA', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPBB', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPM1', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPMC', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPMD', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPMF', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPMG', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPNA', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPOA', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPOD', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPPA', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPPC', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPPF', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPQA', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPQC', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPQD', 	 13,0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPRA', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPTF', 	 3, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPTI', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPTJ', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPTK', 	 0, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZPTL', 	 3, 0,0,1)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZRPCEDTU0',  1, 1,0,0)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCHR0042B',  3, 2,0,0)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('ZCHR161MR',  4, 1,0,0)
Insert into tblReports (ReportCode, ArchiveLength, ReportTypeID, ArchiveValue, OverwriteValue)     Values ('RPCLJNU0', 	 1, 1,0,0)

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblReportTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblReportTypes]
GO

CREATE TABLE [dbo].[tblReportTypes] (
	[PKReportTypesID] [int] IDENTITY (1, 1) NOT NULL ,
	[ReportTypeID] [int] NULL ,
	[ReportType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

insert into tblReportTypes (ReportTypeID, ReportType) values (0, 'Financial')
insert into tblReportTypes (ReportTypeID, ReportType) values (1, 'Payroll')
insert into tblReportTypes (ReportTypeID, ReportType) values (2, 'Profile')
