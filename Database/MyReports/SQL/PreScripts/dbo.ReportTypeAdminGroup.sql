if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_ReportTypeAdminGroup_AdminGroup]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[ReportTypeAdminGroup] DROP CONSTRAINT FK_ReportTypeAdminGroup_AdminGroup
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ReportTypeAdminGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ReportTypeAdminGroup]
GO
CREATE TABLE [dbo].[ReportTypeAdminGroup] (
	[ReportTypeAdminID] [int] IDENTITY (1, 1) NOT NULL ,
	[AdminGroupID] [int] NULL ,
	[ReportTypeID] [int] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[ReportTypeAdminGroup] WITH NOCHECK ADD 
	CONSTRAINT [PK_ReportTypeAdminGroup] PRIMARY KEY  CLUSTERED 
	(
		[ReportTypeAdminID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[ReportTypeAdminGroup] ADD 
	CONSTRAINT [FK_ReportTypeAdminGroup_AdminGroup] FOREIGN KEY 
	(
		[AdminGroupID]
	) REFERENCES [dbo].[AdminGroup] (
		[AdminGroupID]
	) NOT FOR REPLICATION ,
	CONSTRAINT [FK_ReportTypeAdminGroup_ReportType] FOREIGN KEY 
	(
		[ReportTypeID]
	) REFERENCES [dbo].[ReportType] (
		[ReportTypeID]
	) NOT FOR REPLICATION 
GO

alter table [dbo].[ReportTypeAdminGroup] nocheck constraint [FK_ReportTypeAdminGroup_AdminGroup]
GO

alter table [dbo].[ReportTypeAdminGroup] nocheck constraint [FK_ReportTypeAdminGroup_ReportType]
GO