Use ReportDistribution
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblReports_tblArchiveValues]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblReports] DROP CONSTRAINT FK_tblReports_tblArchiveValues
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblBurstProcessStatus_tblBurstProcessErrorCodes]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblBurstProcessStatus] DROP CONSTRAINT FK_tblBurstProcessStatus_tblBurstProcessErrorCodes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblBurstProcessStatus_Details_tblBurstProcessStatus]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblBurstProcessStatus_Details] DROP CONSTRAINT FK_tblBurstProcessStatus_Details_tblBurstProcessStatus
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblBurstProcessStatus_tblBurstProcessStatusCodes_tblBurstProcessStatus]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblBurstProcessStatus_tblBurstProcessStatusCodes] DROP CONSTRAINT FK_tblBurstProcessStatus_tblBurstProcessStatusCodes_tblBurstProcessStatus
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblBurstProcessStatus_tblBurstProcessStatusCodes_tblBurstProcessStatusCodes]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblBurstProcessStatus_tblBurstProcessStatusCodes] DROP CONSTRAINT FK_tblBurstProcessStatus_tblBurstProcessStatusCodes_tblBurstProcessStatusCodes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblFolderNameChanges_Details_tblFolderNameChanges_Master]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblFolderNameChanges_Details] DROP CONSTRAINT FK_tblFolderNameChanges_Details_tblFolderNameChanges_Master
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblHomeFolderStatus_tblFolderNameChanges_Master]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblHomeFolderStatus] DROP CONSTRAINT FK_tblHomeFolderStatus_tblFolderNameChanges_Master
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblFolderNameChanges_Details_tblFolderStatusCodes]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblFolderNameChanges_Details] DROP CONSTRAINT FK_tblFolderNameChanges_Details_tblFolderStatusCodes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblReports_tblOverwriteValues]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblReports] DROP CONSTRAINT FK_tblReports_tblOverwriteValues
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tblReports_tblReportTypes]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tblReports] DROP CONSTRAINT FK_tblReports_tblReportTypes
GO


ALTER TABLE [dbo].[tblReportTypes] WITH NOCHECK ADD 
	CONSTRAINT [PK_tblReportTypes] PRIMARY KEY  CLUSTERED 
	(
		[PKReportTypesID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO


ALTER TABLE [dbo].[tblBurstProcessStatus] WITH NOCHECK ADD 
	CONSTRAINT [FK_tblBurstProcessStatus_tblBurstProcessErrorCodes] FOREIGN KEY 
	(
		[tblBurstProcessErrorCodesFK]
	) REFERENCES [dbo].[tblBurstProcessErrorCodes] (
		[tblBurstProcessErrorCodesPK]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[tblBurstProcessStatus_Details] WITH NOCHECK ADD 
	CONSTRAINT [FK_tblBurstProcessStatus_Details_tblBurstProcessStatus] FOREIGN KEY 
	(
		[tblBurstProcessStatusFK]
	) REFERENCES [dbo].[tblBurstProcessStatus] (
		[tblBurstProcessStatusPK]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[tblBurstProcessStatus_tblBurstProcessStatusCodes] WITH NOCHECK ADD 
	CONSTRAINT [FK_tblBurstProcessStatus_tblBurstProcessStatusCodes_tblBurstProcessStatus] FOREIGN KEY 
	(
		[tblBurstProcessStatusFK]
	) REFERENCES [dbo].[tblBurstProcessStatus] (
		[tblBurstProcessStatusPK]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION ,
	CONSTRAINT [FK_tblBurstProcessStatus_tblBurstProcessStatusCodes_tblBurstProcessStatusCodes] FOREIGN KEY 
	(
		[tblBurstProcessStatusCodesFK]
	) REFERENCES [dbo].[tblBurstProcessStatusCodes] (
		[tblBurstProcessStatusCodesPK]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[tblFolderNameChanges_Details] WITH NOCHECK ADD 
	CONSTRAINT [FK_tblFolderNameChanges_Details_tblFolderNameChanges_Master] FOREIGN KEY 
	(
		[tblFolderNameChanges_MasterFK]
	) REFERENCES [dbo].[tblFolderNameChanges_Master] (
		[tblFolderNameChanges_Details_MasterID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION ,
	CONSTRAINT [FK_tblFolderNameChanges_Details_tblFolderStatusCodes] FOREIGN KEY 
	(
		[FolderStatusCode]
	) REFERENCES [dbo].[tblFolderStatusCodes] (
		[tblFolderStatusCodesID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[tblHomeFolderStatus] WITH NOCHECK ADD 
	CONSTRAINT [FK_tblHomeFolderStatus_tblFolderNameChanges_Master] FOREIGN KEY 
	(
		[tblFolderNameChanges_MasterFK]
	) REFERENCES [dbo].[tblFolderNameChanges_Master] (
		[tblFolderNameChanges_Details_MasterID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[tblReports] WITH NOCHECK ADD 
	CONSTRAINT [FK_tblReports_tblArchiveValues] FOREIGN KEY 
	(
		[ArchiveValue]
	) REFERENCES [dbo].[tblArchiveValues] (
		[ArchiveValuesPK]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION ,
	CONSTRAINT [FK_tblReports_tblOverwriteValues] FOREIGN KEY 
	(
		[ArchiveValue]
	) REFERENCES [dbo].[tblOverwriteValues] (
		[OverwriteValuesPK]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION ,
	CONSTRAINT [FK_tblReports_tblReportTypes] FOREIGN KEY 
	(
		[ReportTypeID]
	) REFERENCES [dbo].[tblReportTypes] (
		[PKReportTypesID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO


sp_rename 'tblHomeFolderStatus.tblHomeFolderStatue', 'tblHomeFolderStatus', 'COLUMN'