if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[User_Audit]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[User_Audit]
GO

CREATE TABLE [dbo].[User_Audit] (
	[User_AuditID] [int] IDENTITY (1, 1) NOT NULL ,
	[DomainName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SessionID] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UserName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LoginDateTime] [datetime] NULL ,
	[LogoutDateTime] [datetime] NULL ,
	[Platform] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Browser] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BrowserVersion] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UserHostAddress] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WorkstationName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[User_Audit] WITH NOCHECK ADD 
	CONSTRAINT [PK_User_Audit] PRIMARY KEY  CLUSTERED 
	(
		[User_AuditID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

