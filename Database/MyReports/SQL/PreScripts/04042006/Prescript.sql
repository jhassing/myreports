USE MYReports

ALTER TABLE dbo.Folder ADD SapHierarchyNodesID int NULL
go

ALTER TABLE dbo.Folder_Backup ADD SapHierarchyNodesID char(10) NULL
go

ALTER TABLE dbo.zrops ADD Division varchar(10) NULL
go
ALTER TABLE dbo.zrops ADD Sector varchar(10) NULL
go


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ClientEmailAddress]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ClientEmailAddress]
GO

CREATE TABLE [dbo].[ClientEmailAddress] (
	[ClientEmailAddressID] [int] IDENTITY (1, 1) NOT NULL ,
	[ClientEmailAddress] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ClientName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[KeyWord] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ClientEmailSendFromID] [int] NULL ,
	[CC1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CC2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BCC1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AccountantFN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AccountantLN] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Region] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreatedBy] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreatedDateTime] [datetime] NULL ,
	[ModifiedBy] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModifiedDateTime] [datetime] NULL ,
	CONSTRAINT [PK_ClientEmailAddress] PRIMARY KEY  CLUSTERED 
	(
		[ClientEmailAddressID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
) ON [PRIMARY]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ClientEmailAddress_AuditTracking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ClientEmailAddress_AuditTracking]
GO

CREATE TABLE [dbo].[ClientEmailAddress_AuditTracking] (
	[ClientEmailAddress_AuditTrackingID] [int] IDENTITY (1, 1) NOT NULL ,
	[NetworkID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ClientEmailAddress] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ClientEmailSendFrom] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CC1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CC2] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BCC1] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_ClientEmailAddress_AuditTracking] PRIMARY KEY  CLUSTERED 
	(
		[ClientEmailAddress_AuditTrackingID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ClientEmailAddress_AuditTrackingDetails]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ClientEmailAddress_AuditTrackingDetails]
GO

CREATE TABLE [dbo].[ClientEmailAddress_AuditTrackingDetails] (
	[ClientEmailAddress_AuditTrackingDetailsID] [int] IDENTITY (1, 1) NOT NULL ,
	[ClientEmailAddress_AuditTrackingID] [int] NULL ,
	[FileName] [varchar] (350) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_ClientEmailAddress_AuditTrackingDetails] PRIMARY KEY  CLUSTERED 
	(
		[ClientEmailAddress_AuditTrackingDetailsID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ClientEmailSendFrom]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ClientEmailSendFrom]
GO

CREATE TABLE [dbo].[ClientEmailSendFrom] (
	[ClientEmailSendFromID] [int] IDENTITY (1, 1) NOT NULL ,
	[FromEmailAddress] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreatedBy] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreatedDateTime] [datetime] NULL ,
	[ModifiedBy] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModifiedDateTime] [datetime] NULL ,
	CONSTRAINT [PK_ClientEmailSendFrom] PRIMARY KEY  CLUSTERED 
	(
		[ClientEmailSendFromID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
) ON [PRIMARY]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SapHierarchyNodes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SapHierarchyNodes]
GO

CREATE TABLE [dbo].[SapHierarchyNodes] (
	[SapHierarchyNodesID] [int] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_SapHierarchyNodes] PRIMARY KEY  CLUSTERED 
	(
		[SapHierarchyNodesID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
) ON [PRIMARY]
GO


