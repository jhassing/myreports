
Update a set a.ArchiveValue = b.ArchiveValuesPK from tblReports a
	inner join tblArchiveValues b on b.ArchiveValueCode = a.ArchiveValue

GO

Update a set a.OverwriteValue = b.OverwriteValuesPK from tblReports a
	inner join tblOverwriteValues b on a.OverwriteValue = b.OverwriteCode
GO

Update a set a.FolderStatusCode = b.tblFolderStatusCodesID from tblFolderNameChanges_Details a
inner join tblFolderStatusCodes b on a.FolderStatusCode = b.StatusCode
GO

ALTER TABLE [dbo].[tblArchiveValues] DROP CONSTRAINT [PK_tblArchiveValues]
GO

ALTER TABLE [dbo].[tblBurstProcessErrorCodes] DROP 	CONSTRAINT [PK_tblBurstProcessErrorCodes] 
GO

ALTER TABLE [dbo].[tblBurstProcessStatus] DROP CONSTRAINT [PK_tblBurstProcessStatus] 
GO

ALTER TABLE [dbo].[tblBurstProcessStatusCodes] DROP CONSTRAINT [PK_tblBurstProcessStatusCodes] 
GO
ALTER TABLE [dbo].[tblBurstProcessStatus_Details] DROP CONSTRAINT [PK_tblBurstProcessStatus_Details] 
GO

ALTER TABLE [dbo].[tblBurstProcessStatus_tblBurstProcessStatusCodes] DROP CONSTRAINT [PK_tblBurstProcessStatus_tblBurstProcessStatusCodes] 
GO

ALTER TABLE [dbo].[tblFolderNameChanges_Details] DROP CONSTRAINT [PK_tblFolderNameChanges_Details] 
GO	
ALTER TABLE [dbo].[tblFolderNameChanges_Master] DROP CONSTRAINT [PK_tblFolderNameChanges_Master]
GO

ALTER TABLE [dbo].[tblFolderStatusCodes] DROP CONSTRAINT [PK_tblFolderStatusCodes] 
GO

ALTER TABLE [dbo].[tblHomeFolderStatus] DROP CONSTRAINT [PK_tblHomeFolderStatus] 
GO

ALTER TABLE [dbo].[tblOverwriteValues] DROP CONSTRAINT [PK_tblOverrightValues] 
GO

ALTER TABLE [dbo].[tblReports] DROP CONSTRAINT [PK_tblReports] 
GO

ALTER TABLE [dbo].[zrops] DROP CONSTRAINT [PK_zrops] 
GO
DROP INDEX dbo.tblfolders.IX_folders

GO


sp_rename 'tblArchiveValues.ArchiveValuesPK', 'ArchiveValueID', 'COLUMN'
GO
sp_rename 'tblOverwriteValues.OverwriteValuesPK', 'OverwriteValueID', 'COLUMN'
GO
sp_rename 'tblFolderStatusCodes.tblFolderStatusCodesID', 'FolderStatusCodeID', 'COLUMN'
GO
sp_rename 'tblFolderNameChanges_Details.tblFolderNameChangesID', 'FolderNameChangeDetailID', 'COLUMN'
GO
sp_rename 'tblFolderNameChanges_Details.tblFolderNameChanges_MasterFK', 'FolderNameChangeMasterID', 'COLUMN'
GO
sp_rename 'tblFolderNameChanges_Details.FolderStatusCode', 'FolderStatusCodeID', 'COLUMN'
GO
sp_rename 'tblReports.ArchiveValue ', 'ArchiveValueID', 'COLUMN'
GO
sp_rename 'tblReports.OverwriteValue ', 'OverwriteValueID', 'COLUMN'
GO
sp_rename 'tblHomeFolderStatus.tblHomeFolderStatue', 'HomeFolderStatusID', 'COLUMN'
GO
sp_rename 'tblHomeFolderStatus.tblFolderNameChanges_MasterFK', 'FolderNameChangeMasterID', 'COLUMN'
GO
sp_rename 'tblFolderNameChanges_Master.tblFolderNameChanges_Details_MasterID', 'FolderNameChangeMasterID', 'COLUMN'
GO
sp_rename 'tblReportViewedDates.ReportViewedDatesPK', 'ReportViewedDateID', 'COLUMN'
GO
sp_rename 'tblBurstProcessStatusCodes.tblBurstProcessStatusCodesPK', 'BurstProcessStatusCodeID', 'COLUMN'
GO
sp_rename 'tblBurstProcessStatus_Details.tblBurstProcessStatus_DetailsPK', 'BurstProcessStatusDetailID', 'COLUMN'
GO
sp_rename 'tblBurstProcessStatus_Details.tblBurstProcessStatusFK', 'BurstProcessStatusID', 'COLUMN'
GO
sp_rename 'tblBurstProcessStatus.tblBurstProcessStatusPK', 'BurstProcessStatusID', 'COLUMN'
GO
sp_rename 'tblBurstProcessErrorCodes.tblBurstProcessErrorCodesPK', 'BurstProcessErrorCodeID', 'COLUMN'
GO
sp_rename 'tblBurstProcessStatus.tblBurstProcessErrorCodesFK', 'BurstProcessErrorCodeID', 'COLUMN'
GO
sp_rename 'tblBurstProcessStatus_tblBurstProcessStatusCodes.tblBurstProcessStatus_tblBurstProcessStatusCodesPK', 'BurstProcessStatus_BurstProcessStatusCodeID', 'COLUMN'
GO
sp_rename 'tblBurstProcessStatus_tblBurstProcessStatusCodes.tblBurstProcessStatusFK', 'BurstProcessStatusID', 'COLUMN'
GO
sp_rename 'tblBurstProcessStatus_tblBurstProcessStatusCodes.tblBurstProcessStatusCodesFK', 'BurstProcessStatusCodeID', 'COLUMN'
GO 

sp_rename 'tblArchiveValues', 'ArchiveValue'
GO
sp_rename 'tblBurstProcessErrorCodes', 'BurstProcessErrorCode'
GO
sp_rename 'tblBurstProcessStatus  ', 'BurstProcessStatus'
GO
sp_rename 'tblBurstProcessStatus_Details', 'BurstProcessStatus_Detail'
GO
sp_rename 'tblBurstProcessStatus_tblBurstProcessStatusCodes', 'BurstProcessStatus_BurstProcessStatusCode'
GO
sp_rename 'tblFolderNameChanges_Details', 'FolderNameChange_Detail'
GO
sp_rename 'tblFolderNameChanges_Master', 'FolderNameChange_Master'
GO
sp_rename 'tblFolderStatusCodes', 'FolderStatusCode'
GO
sp_rename 'tblHomeFolderStatus', 'HomeFolderStatus'
GO
sp_rename 'tblOverwriteValues', 'OverwriteValue'
GO
sp_rename 'tblReports', 'Report'
GO
sp_rename 'tblReportViewedDates', 'ReportViewedDate'
Go
sp_rename 'tblReportTypes', 'ReportType'
GO
sp_rename 'tblBurstProcessStatusCodes', 'BurstProcessStatusCode'
GO
ALTER TABLE ReportViewedDate ADD ReportCode nvarchar(20)
GO

ALTER TABLE tblFolders ADD  
	FolderID integer Identity
GO
ALTER TABLE Report ADD  
	ReportID integer Identity
GO
--ALTER TABLE [dbo].[tblFolders] DROP CONSTRAINT pk_FolderCode
GO
ALTER TABLE [dbo].[tblFolders] WITH NOCHECK ADD 
	CONSTRAINT [pk_Folder]  PRIMARY KEY  CLUSTERED 
	(
		[FolderID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 

GO
sp_rename 'tblFolders', 'Folder'
GO
ALTER TABLE tblFolders_Backup ADD  
	FolderID integer Identity
GO
ALTER TABLE [dbo].[tblFolders_Backup] WITH NOCHECK ADD 
	CONSTRAINT [pk_Folder_Backup]  PRIMARY KEY  CLUSTERED 
	(
		[FolderID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 

GO


sp_rename 'tblFolders_Backup', 'Folder_Backup'

GO
ALTER TABLE ReportType DROP COLUMN PKReportTypesID
GO
ALTER TABLE ReportType ALTER COLUMN
	ReportTypeID integer Not NULL

GO
ALTER TABLE [dbo].[ReportType] WITH NOCHECK ADD 
	CONSTRAINT [pk_ReportType]  PRIMARY KEY  CLUSTERED 
	(
		[ReportTypeID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[ArchiveValue] WITH NOCHECK ADD 
	CONSTRAINT [PK_ArchiveValue] PRIMARY KEY  CLUSTERED 
	(
		[ArchiveValueID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[BurstProcessErrorCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_BurstProcessErrorCode] PRIMARY KEY  CLUSTERED 
	(
		[BurstProcessErrorCodeID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[BurstProcessStatus] WITH NOCHECK ADD 
	CONSTRAINT [PK_BurstProcessStatus] PRIMARY KEY  CLUSTERED 
	(
		[BurstProcessStatusID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[BurstProcessStatusCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_BurstProcessStatusCode] PRIMARY KEY  CLUSTERED 
	(
		[BurstProcessStatusCodeID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO


--Start here
ALTER TABLE [dbo].[BurstProcessStatus_Detail] WITH NOCHECK ADD 
	CONSTRAINT [PK_BurstProcessStatusDetail] PRIMARY KEY  CLUSTERED 
	(
		[BurstProcessStatusDetailID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[BurstProcessStatus_BurstProcessStatusCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_BurstProcessStatus_BurstProcessStatusCode] PRIMARY KEY  CLUSTERED 
	(
		[BurstProcessStatus_BurstProcessStatusCodeID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[FolderNameChange_Detail] WITH NOCHECK ADD 
	CONSTRAINT [PK_FolderNameChange_Detail] PRIMARY KEY  CLUSTERED 
	(
		[FolderNameChangeDetailID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[FolderNameChange_Master] WITH NOCHECK ADD 
	CONSTRAINT [PK_FolderNameChange_Master] PRIMARY KEY  CLUSTERED 
	(
		[FolderNameChangeMasterID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[FolderStatusCode] WITH NOCHECK ADD 
	CONSTRAINT [PK_FolderStatusCode] PRIMARY KEY  CLUSTERED 
	(
		[FolderStatusCodeID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[HomeFolderStatus] WITH NOCHECK ADD 
	CONSTRAINT [PK_HomeFolderStatus] PRIMARY KEY  CLUSTERED 
	(
		[HomeFolderStatusID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[OverwriteValue] WITH NOCHECK ADD 
	CONSTRAINT [PK_OverrightValue] PRIMARY KEY  CLUSTERED 
	(
		[OverwriteValueID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Report] WITH NOCHECK ADD 
	CONSTRAINT [PK_ReportID] PRIMARY KEY  CLUSTERED 
	(
		[ReportID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[ReportViewedDate] WITH NOCHECK ADD 
	CONSTRAINT [PK_ReportViewedDate] PRIMARY KEY  CLUSTERED 
	(
		[ReportViewedDateID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO



ALTER TABLE [dbo].[BurstProcessStatus] WITH NOCHECK ADD 
	CONSTRAINT [FK_BurstProcessStatus_BurstProcessErrorCode] FOREIGN KEY 
	(
		[BurstProcessErrorCodeID]
	) REFERENCES [dbo].[BurstProcessErrorCode] (
		[BurstProcessErrorCodeID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[BurstProcessStatus_BurstProcessStatusCode] WITH NOCHECK ADD 
	CONSTRAINT [FK_BurstProcessStatus_BurstProcessStatusCode_BurstProcessStatus] FOREIGN KEY 
	(
		[BurstProcessStatusID]
	) REFERENCES [dbo].[BurstProcessStatus] (
		[BurstProcessStatusID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION ,
	CONSTRAINT [FK_BurstProcessStatus_BurstProcessStatusCode_BurstProcessStatusCode] FOREIGN KEY 
	(
		[BurstProcessStatusCodeID]
	) REFERENCES [dbo].[BurstProcessStatusCode] (
		[BurstProcessStatusCodeID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[BurstProcessStatus_Detail] WITH NOCHECK ADD 
	CONSTRAINT [FK_BurstProcessStatus_Detail_BurstProcessStatus] FOREIGN KEY 
	(
		[BurstProcessStatusID]
	) REFERENCES [dbo].[BurstProcessStatus] (
		[BurstProcessStatusID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[FolderNameChange_Detail] WITH NOCHECK ADD 
	CONSTRAINT [FK_FolderNameChange_Detail_FolderNameChange_Master] FOREIGN KEY 
	(
		[FolderNameChangeMasterID]
	) REFERENCES [dbo].[FolderNameChange_Master] (
		[FolderNameChangeMasterID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION ,
	CONSTRAINT [FK_FolderNameChange_Detail_FolderStatusCode] FOREIGN KEY 
	(
		[FolderStatusCodeID]
	) REFERENCES [dbo].[FolderStatusCode] (
		[FolderStatusCodeID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[HomeFolderStatus] WITH NOCHECK ADD 
	CONSTRAINT [FK_HomeFolderStatus_FolderNameChange_Master] FOREIGN KEY 
	(
		[FolderNameChangeMasterID]
	) REFERENCES [dbo].[FolderNameChange_Master] (
		[FolderNameChangeMasterID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO

ALTER TABLE [dbo].[Report] WITH NOCHECK ADD 
	CONSTRAINT [FK_Report_ArchiveValue] FOREIGN KEY 
	(
		[ArchiveValueID]
	) REFERENCES [dbo].[ArchiveValue] (
		[ArchiveValueID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION ,
	CONSTRAINT [FK_Report_OverwriteValue] FOREIGN KEY 
	(
		[OverwriteValueID]
	) REFERENCES [dbo].[OverwriteValue] (
		[OverwriteValueID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION ,
	CONSTRAINT [FK_Report_ReportType] FOREIGN KEY 
	(
		[ReportTypeID]
	) REFERENCES [dbo].[ReportType] (
		[ReportTypeID]
	) ON DELETE CASCADE  ON UPDATE CASCADE  NOT FOR REPLICATION 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tblZrops]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tblZrops]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Folder_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Folder_Select]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Folders_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Folders_Select]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAllReports]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetAllReports]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[dbo.usp_FolderStatusCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[dbo.usp_FolderStatusCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_BackupFoldersTable]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_BackupFoldersTable]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_DeleteReportFromACL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_DeleteReportFromACL]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetBurstProcessStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetBurstProcessStatus]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetBurstProcessStatusDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetBurstProcessStatusDetails]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetBurstStatusByFinishedDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetBurstStatusByFinishedDate]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetBurstStatusByReportName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetBurstStatusByReportName]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetFolderDescription]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetFolderDescription]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetFolderUpdateStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetFolderUpdateStatus]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetFolders]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetFolders]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetFoldersBackup]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetFoldersBackup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetReportsViewedList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetReportsViewedList]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetValidReports]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetValidReports]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetZrops]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetZrops]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Region_Code_AND_DM_Code_From_Unit_Code]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Region_Code_AND_DM_Code_From_Unit_Code]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Region_Code_From_DM_Code]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Region_Code_From_DM_Code]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_ReportArchiveValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_ReportArchiveValues]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_ReportType]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_ReportType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[ReportDistribution_User].[usp_GetFolderNameChange_Master]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [ReportDistribution_User].[usp_GetFolderNameChange_Master]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetFolderNameChange_Master]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetFolderNameChange_Master]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertBurstProcessStatus_BurstProcessStatusCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertBurstProcessStatus_BurstProcessStatusCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertBurstProcessStatus_Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertBurstProcessStatus_Details]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertBurstProcessStatus_SAPFileName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertBurstProcessStatus_SAPFileName]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertFolderTableUpdateRecord_Detail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertFolderTableUpdateRecord_Detail]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertFolderTableUpdateRecord_Master]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertFolderTableUpdateRecord_Master]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertFolders]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertFolders]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertHomeFolderStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertHomeFolderStatus]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertReportIntoACL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertReportIntoACL]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertViewedReportDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertViewedReportDate]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_LengthToArchive]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_LengthToArchive]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_RestoreFolderBackupTable]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_RestoreFolderBackupTable]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_UpdateBurstProcessStatus_ErrorCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_UpdateBurstProcessStatus_ErrorCode]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_UpdateBurstProcessStatus_ReportName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_UpdateBurstProcessStatus_ReportName]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_UpdateFolders]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_UpdateFolders]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_UpdateReportInACL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_UpdateReportInACL]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_doesFolderCodeExist]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_doesFolderCodeExist]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_doesReportExist]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_doesReportExist]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[dbo.usp_FolderStatusCodes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[dbo.usp_FolderStatusCodes]
GO

ALTER TABLE [dbo].[ArchiveValue] DROP COLUMN [ArchiveValueCode] 

GO
ALTER TABLE [dbo].[OverwriteValue] DROP COLUMN [OverwriteValue] 
GO

INSERT INTO ReportType (ReportType) VALUES ('Off Cycle')
GO
INSERT INTO ReportType (ReportType) VALUES ('Subsidiary Invoice')
GO

