

If exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AdminGroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AdminGroup]
GO


CREATE TABLE [dbo].[AdminGroup] (
	[AdminGroupID] [int] IDENTITY (1, 1) NOT NULL ,
	[GroupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO


ALTER TABLE [dbo].[AdminGroup] WITH NOCHECK ADD 
	CONSTRAINT [PK_AdminGroup] PRIMARY KEY  CLUSTERED 
	(
		[AdminGroupID]
	)  ON [PRIMARY] 
GO



