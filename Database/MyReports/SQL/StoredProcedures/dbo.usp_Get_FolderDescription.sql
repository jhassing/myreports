SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_FolderDescription]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_FolderDescription]
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Get_FolderDescription

Description:	Returns a folder description based on a folder code.
  
Parameters:	@as_ReportCode 		varchar(10) 	- Report Code to locate
		@as_ReturnValue		varchar(10)	- Return value

Returns:	Nothing

Revisions:	10/19/2003 John Schiavarelli Created

Example:	
		declare @as_FolderCode varchar(10)
		declare @as_FolderName varchar(1000)
		
		set @as_FolderCode = 'cha'
		
		exec usp_Get_FolderDescription @as_FolderCode, @as_FolderName out
		
		print @as_FolderName
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE    PROCEDURE usp_Get_FolderDescription 

		@as_FolderCode varchar(10),
		@as_FolderName varchar(1000) output

AS

	SET @as_FolderName =(SELECT top 1FolderName 
	FROM         dbo.Folder  WITH (NOLOCK)
	WHERE   FolderCode = @as_FolderCode)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

