SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetFolderUpdateStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetFolderUpdateStatus]
GO


CREATE PROCEDURE [dbo].[usp_GetFolderUpdateStatus] 
	@FKFolderStatusMasterID	as int,
	@FolderStatusCode		as int
AS

if (@FolderStatusCode = "" or @FolderStatusCode = null or @FolderStatusCode = 0)

	SELECT     dbo.tblFolderNameChanges_Details.DateChanged, dbo.tblFolderNameChanges_Details.FolderCode, 
	                      dbo.tblFolderNameChanges_Details.ExistingName, dbo.tblFolderNameChanges_Details.NewName, dbo.tblFolderStatusCodes.StatusDescription, 
	                      dbo.tblFolderNameChanges_Master.tblFolderNameChanges_Details_MasterID, dbo.tblFolderNameChanges_Details.FolderStatusCode
	FROM         dbo.tblFolderNameChanges_Details INNER JOIN
	                      dbo.tblFolderStatusCodes ON dbo.tblFolderNameChanges_Details.FolderStatusCode = dbo.tblFolderStatusCodes.StatusCode INNER JOIN
	                      dbo.tblFolderNameChanges_Master ON 
	                      dbo.tblFolderNameChanges_Details.tblFolderNameChanges_MasterFK = dbo.tblFolderNameChanges_Master.tblFolderNameChanges_Details_MasterID
	WHERE     (dbo.tblFolderNameChanges_Master.tblFolderNameChanges_Details_MasterID = @FKFolderStatusMasterID)

else

	SELECT     dbo.tblFolderNameChanges_Details.DateChanged, dbo.tblFolderNameChanges_Details.FolderCode, 
	                      dbo.tblFolderNameChanges_Details.ExistingName, dbo.tblFolderNameChanges_Details.NewName, dbo.tblFolderStatusCodes.StatusDescription, 
	                      dbo.tblFolderNameChanges_Master.tblFolderNameChanges_Details_MasterID, dbo.tblFolderNameChanges_Details.FolderStatusCode
	FROM         dbo.tblFolderNameChanges_Details INNER JOIN
	                      dbo.tblFolderStatusCodes ON dbo.tblFolderNameChanges_Details.FolderStatusCode = dbo.tblFolderStatusCodes.StatusCode INNER JOIN
	                      dbo.tblFolderNameChanges_Master ON 
	                      dbo.tblFolderNameChanges_Details.tblFolderNameChanges_MasterFK = dbo.tblFolderNameChanges_Master.tblFolderNameChanges_Details_MasterID
	WHERE     (dbo.tblFolderNameChanges_Master.tblFolderNameChanges_Details_MasterID = @FKFolderStatusMasterID) AND (dbo.tblFolderNameChanges_Details.FolderStatusCode = @FolderStatusCode)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

