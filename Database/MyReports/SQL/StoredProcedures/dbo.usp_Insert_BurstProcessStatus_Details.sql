SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_BurstProcessStatus_Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_BurstProcessStatus_Details]
GO




/****** Object:  Stored Procedure dbo.usp_InsertBurstProcessStatus_Details Script Date: 4/13/2004 3:57:09 PM ******/
--JJS modified 4/21/2004
CREATE   PROCEDURE dbo.usp_Insert_BurstProcessStatus_Details
	@ai_BurstProcessStatus		as int,
	@as_ReportLocation			as varchar(255)
AS
	INSERT INTO  dbo.BurstProcessStatus_Details
		(
			BurstProcessStatus,
			ReportLocation
		)
	VALUES
		(
			@ai_BurstProcessStatus,
			@as_ReportLocation
		)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

