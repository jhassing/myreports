SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertBurstProcessStatus_SAPFileName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertBurstProcessStatus_SAPFileName]
GO




/****** Object:  Stored Procedure dbo.usp_InsertFolders    Script Date: 1/20/2004 8:57:09 PM ******/
--JJS modified 1/20/2004
CREATE   procedure dbo.usp_InsertBurstProcessStatus_SAPFileName
	@SAPfileName			as varchar(25),
	@ident INT OUTPUT
as
	insert  dbo.tblBurstProcessStatus
		(
			SAPfileName,
			ReportName,
			StartedTime,
			tblBurstProcessErrorCodesFK
		)
		values
		(
			@SAPfileName,
			" ",
			GetDate(),
			0
		)

	select @ident = @@Identity

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

