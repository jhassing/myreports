SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_ClientEmailSendFrom]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_ClientEmailSendFrom]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.ClientEmailSendFrom

Description:	Inserts a new From email address
  
Parameters:	@as_FromEmailAddress		varchar(200)	New From email address	
		@as_CreatedBy			varchar	(25)	Network ID of the person doing the inserting

Returns:	Nothing

Revisions:	03/14/2006 John Schiavarelli Created

Example:	
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Insert_ClientEmailSendFrom
(
	@as_FromEmailAddress		varchar(200),
	@as_CreatedBy			varchar	(25)	
)
AS
	INSERT INTO ClientEmailSendFrom
	(
		FromEmailAddress,
		CreatedBy,
		CreatedDateTime,
		ModifiedBy,
		ModifiedDateTime
	)	
	VALUES
	(
		@as_FromEmailAddress,
		@as_CreatedBy,		
		getDate(),
		@as_CreatedBy,
		getDate()
	)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

