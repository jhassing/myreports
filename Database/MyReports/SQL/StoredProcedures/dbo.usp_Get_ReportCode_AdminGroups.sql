SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO





/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Get_ReportCode_AdminGroups

Description:	Gets a list of the report admin groups
  
Parameters:	

Returns:	Nothing

Algorithm:	

Revisions:	02/15/2005 Monica Hancock Created

Modified:	

Example:	exec usp_Get_ReportCode_AdminGroups
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE PROCEDURE dbo.usp_Get_ReportCode_AdminGroups 

AS

SELECT     
	r.ReportCode, 
	ag.GroupName
FROM    
	ReportType rt 
INNER JOIN ReportTypeAdminGroup rtag 
	ON rt.ReportTypeID = rtag.ReportTypeID 
INNER JOIN AdminGroup ag 
	ON rtag.AdminGroupID = ag.AdminGroupID 
INNER JOIN Report r 
	ON rt.ReportTypeID = r.ReportTypeID
ORDER BY 
	r.ReportCode

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

