SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Delete_ReportFromACL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Delete_ReportFromACL]
GO





/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Delete_ReportFromACL

Description:	Deletes a Report in the ACL (Access control list) of reports allowed to burst for R.D.
  
Parameters:	@av_ReportCode 		varchar(15) 	- Report Code to be removed from the ACL

Returns:	Nothing
Algorithm:	Deletes the recrod

Revisions:	03/298/2005 John Schiavarelli Created

Example:	exec usp_DeleteReportFromACL 'AQL'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Delete_ReportFromACL
	@as_ReportCode		as varchar(15)
as
	DELETE FROM Report
	WHERE ReportCode = @as_ReportCode
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

