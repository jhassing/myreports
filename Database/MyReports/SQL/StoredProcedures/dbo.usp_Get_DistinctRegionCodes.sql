SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_DistinctRegionCodes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_DistinctRegionCodes]
GO


/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_DistinctRegionCodes

Description:	Returns a distinct recordset of region codes from ZROPS
  
Parameters:	

Returns:	Recordset	

Revisions:	03/16/2006 John Schiavarelli created

Example:	exec usp_Get_DistinctRegionCodes
------------------------------------------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_DistinctRegionCodes
AS

	SET NOCOUNT ON
	
	SELECT DISTINCT TOP 100 PERCENT Region

	FROM         dbo.zrops

	WHERE     (NOT (Region IS NULL))

	ORDER BY Region
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

