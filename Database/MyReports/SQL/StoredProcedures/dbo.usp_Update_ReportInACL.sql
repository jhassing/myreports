SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_ReportInACL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_ReportInACL]
GO




/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Update_ReportInACL

Description:	Updates a Report in the ACL (Access control list) of reports allowed to burst for R.D.
  
Parameters:	@as_ReportCode 		varchar(15) 	- Report Code to be modified
		@ai_ArchiveLength		int  		- Archive length of the new report.
		@ai_ReportTypeID		int  		- Report type (i.e. Financial)
		@ai_ArchiveValue		int  		- Archive value (i.e. Months)
		@ai_OverwriteValue		int  		- Overwrite Value (i.e. Force each report to overwrite)

Returns:	Nothing
Algorithm:	Update master recrod

Revisions:	03/08/2005 John Schiavarelli Created

Example:	exec usp_Update_ReportInACL 'AQL', 0, 0, 0, 0
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Update_ReportInACL
	@as_ReportCode		as varchar(15),
	@ai_ArchiveLength		as int,
	@ai_ReportTypeID		as int,
	@ai_ArchiveValue		as int,
	@ai_OverwriteValue		as int
AS
	UPDATE 
		Report
	SET	
		ArchiveLength = @ai_ArchiveLength,  
		ReportTypeID = @ai_ReportTypeID, 
		ArchiveValueID = @ai_ArchiveValue, 
		OverwriteValueID = @ai_OverwriteValue
	WHERE
		ReportCode = @as_ReportCode
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

