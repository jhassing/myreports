SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_BackupFoldersTable]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_BackupFoldersTable]
GO


CREATE PROCEDURE dbo.usp_BackupFoldersTable

as

delete from tblFolders_Backup

insert into tblFolders_Backup (FolderCode, FolderName)  Select FolderCode, FolderName from tblFolders

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

