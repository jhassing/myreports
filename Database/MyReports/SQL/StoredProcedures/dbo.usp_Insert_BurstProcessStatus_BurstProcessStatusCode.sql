SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_BurstProcessStatus_BurstProcessStatusCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_BurstProcessStatus_BurstProcessStatusCode]
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Insert_BurstProcessStatus_BurstProcessStatusCode

Description:	Inserts a burst process status code.
  
Parameters:	@ai_BurstProcessStatusID 		Burst process status ID
		@ai_BurstProcessStatusCodeID		Status code ID		

Returns:	nothing

Revisions:	01/21/2004 John Schiavarelli Created

Example:	
		declare @ai_BurstProcessStatusID		as int	
		declare @ai_BurstProcessStatusCodeID	as int
		
		set @ai_BurstProcessStatusID = 3896
		set @ai_BurstProcessStatusCodeID = 1
		
		exec dbo.usp_Insert_BurstProcessStatus_BurstProcessStatusCode @ai_BurstProcessStatusID, @ai_BurstProcessStatusCodeID
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Insert_BurstProcessStatus_BurstProcessStatusCode
	@ai_BurstProcessStatusID			as int,
	@ai_BurstProcessStatusCodeID			as int
AS
	INSERT INTO  dbo.BurstProcessStatus_BurstProcessStatusCode
		(
			BurstProcessStatusID,
			BurstProcessStatusCodeID,
			ProcessTime
		)
	VALUES
		(
			@ai_BurstProcessStatusID,
			@ai_BurstProcessStatusCodeID,
			GetDate()
		)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

