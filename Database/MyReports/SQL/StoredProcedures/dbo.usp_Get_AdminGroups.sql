



SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO






/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Get_AdminGroups

Description:	Gets a list of the admin groups
  
Parameters:	

Returns:	Nothing

Algorithm:	

Revisions:	02/15/2005 Monica Hancock Created

Modified:	

Example:	exec usp_Get_AdminGroups
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE  PROCEDURE dbo.usp_Get_AdminGroups 

AS

SELECT     
	AdminGroupID, 
	GroupName
FROM    
	AdminGroup 


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

