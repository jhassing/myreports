SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_RegionListing]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_RegionListing]
GO


/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_RegionListing

Description: 	Regions
  
Parameters:	

Returns:	A listing of Regions

Revisions:	03/16/2006 John Schiavarelli created

Example:	exec usp_Get_RegionListing
------------------------------------------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_RegionListing

AS

	SET NOCOUNT ON
	
	SELECT DISTINCT TOP 100 PERCENT Region
	FROM         dbo.zrops
	WHERE     (Region IS NOT NULL)
	ORDER BY Region
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

