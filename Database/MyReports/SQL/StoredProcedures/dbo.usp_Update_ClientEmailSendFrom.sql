SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_ClientEmailSendFrom]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_ClientEmailSendFrom]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Update_ClientEmailSendFrom

Description:	Updates a client from email address
  
Parameters:	@as_FromEmailAddress		varchar(200)	New From email address

Returns:	Nothing

Revisions:	03/14/2006 John Schiavarelli Created

Example:	
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Update_ClientEmailSendFrom
(
	@ai_ClientEmailSendFromID	int,
	@as_FromEmailAddress		varchar(200),
	@as_ModifiedBy		varchar(25)
)
AS

	UPDATE 
		ClientEmailSendFrom
	SET	
		FromEmailAddress = @as_FromEmailAddress,
		ModifiedBy = @as_ModifiedBy,
		ModifiedDateTime = getDate()
	WHERE
		ClientEmailSendFromID = @ai_ClientEmailSendFromID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

