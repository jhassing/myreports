SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_ClientEmailAddress]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_ClientEmailAddress]
GO


/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_ClientEmailAddress

Description:	Locates a client email addresss listing
  
Parameters:	

Returns:	Client email address listing		

Revisions:	03/14/2006 John Schiavarelli created

Example:	exec usp_Get_ClientEmailAddress '','','','','',''
------------------------------------------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_ClientEmailAddress
(
	@as_ClientEmailAddress 	as varchar(200),
	@as_ClientName		as varchar(200),
	@as_KeyWord			as varchar(200),
	@as_AccountantFN		as varchar(50),
	@as_AccountantLN		as varchar(100),
	@as_Region			as varchar(25)
)
AS

	SET NOCOUNT ON

	SELECT     dbo.ClientEmailSendFrom.FromEmailAddress AS FromEmailAddress, dbo.ClientEmailAddress.ClientEmailAddressID, 
	                      dbo.ClientEmailAddress.ClientEmailAddress, dbo.ClientEmailAddress.ClientName, dbo.ClientEmailAddress.KeyWord, 
	                      dbo.ClientEmailAddress.ClientEmailSendFromID, dbo.ClientEmailAddress.CC1, dbo.ClientEmailAddress.CC2, dbo.ClientEmailAddress.BCC1, 
	                      dbo.ClientEmailAddress.AccountantFN, dbo.ClientEmailAddress.AccountantLN, dbo.ClientEmailAddress.Region, dbo.ClientEmailAddress.CreatedBy, 
	                      dbo.ClientEmailAddress.CreatedDateTime, dbo.ClientEmailAddress.ModifiedBy, dbo.ClientEmailAddress.ModifiedDateTime

	FROM         dbo.ClientEmailSendFrom INNER JOIN
	                      dbo.ClientEmailAddress ON dbo.ClientEmailSendFrom.ClientEmailSendFromID = dbo.ClientEmailAddress.ClientEmailSendFromID
 
	WHERE 
		( 
			( ClientEmailAddress like '%' + @as_ClientEmailAddress + '%') AND
			( ClientName like '%' + @as_ClientName + '%' ) AND
			( KeyWord like '%' + @as_KeyWord + '%' ) AND
			( AccountantFN like '%' + @as_AccountantFN + '%' ) AND
			( AccountantLN like '%' + @as_AccountantLN + '%' ) AND
			( Region like '%' + @as_Region + '%' )
		)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

