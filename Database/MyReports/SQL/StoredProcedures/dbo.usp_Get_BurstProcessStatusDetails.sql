SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_BurstProcessStatusDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_BurstProcessStatusDetails]
GO



CREATE PROCEDURE dbo.usp_Get_BurstProcessStatusDetails

	@BurstProcessStatusID as int

as
	BEGIN

		SELECT ReportLocation FROM BurstProcessStatus_Detail

		WHERE BurstProcessStatusID = @BurstProcessStatusID

		ORDER BY ReportLocation
	END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

