SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_BurstStatusByReportName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_BurstStatusByReportName]
GO



CREATE PROCEDURE [dbo].[usp_Get_BurstStatusByReportName] 
	@ReportName varchar(400)

AS

SELECT    
	bps.ReportCode, 
	bps.BurstProcessStatusID, 
	bps.SAPfileName, 
	MAX(bps.ReportName) AS ReportName,
	            (SELECT     
			[Description] 
		FROM  
			BurstProcessStatusCode
                        WHERE      
			StatusCode = MAX(bpsc.BurstProcessStatusCodeID)) AS Description, 
	MIN(bps.StartedTime) AS StartedTime, 
	MAX(bpsc.ProcessTime) AS [Finished Time]
FROM         
	BurstProcessStatus_BurstProcessStatusCode bpsc 
RIGHT OUTER JOIN
	BurstProcessStatus bps ON bpsc.BurstProcessStatusID = bps.BurstProcessStatusID
GROUP BY 
	bps.ReportCode, bps.SAPfileName, bps.BurstProcessErrorCodeID, bps.BurstProcessStatusID

HAVING      
	(bps.BurstProcessErrorCodeID = 0) AND  (MAX(bps.ReportName) LIKE '%' + @ReportName + '%')

ORDER BY 
	bps.ReportCode, 
	StartedTime DESC
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

