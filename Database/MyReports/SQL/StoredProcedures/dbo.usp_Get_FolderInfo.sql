SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_FolderInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_FolderInfo]
GO



CREATE PROCEDURE dbo.usp_Get_FolderInfo
AS

SET NOCOUNT ON;


SELECT  
	FolderCode, 
	FolderName 
FROM 
	Folder


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

