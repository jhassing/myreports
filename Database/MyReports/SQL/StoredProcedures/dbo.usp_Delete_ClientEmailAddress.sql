SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Delete_ClientEmailAddress]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Delete_ClientEmailAddress]
GO

/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Delete_ClientEmailAddress

Description:	Deletes a client email address from the table
  
Parameters:	

Returns:	Nothing
Algorithm:	Deletes the recrod

Revisions:	03/14/2006 John Schiavarelli Created

Example:	exec usp_Delete_ClientEmailAddress 1
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Delete_ClientEmailAddress
	@ai_ClientEmailAddressID	as int
AS
	DELETE FROM ClientEmailAddress

	WHERE ClientEmailAddressID = @ai_ClientEmailAddressID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

