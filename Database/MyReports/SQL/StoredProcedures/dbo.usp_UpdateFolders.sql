if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_Folders]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_Folders]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Update_Folders

Description:	Updates a folder description based on folder code.
  
Parameters:	@as_FolderCode 		varchar(10) 	- Folder Code to locate
		@as_FolderName		varchar(10)	- Folder description to update

Returns:	Nothing

Revisions:	11/18/2003 John Schiavarelli Created
		01/04/2004 John Schiavarelli modified 

Example:	
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Update_Folders
	@as_FolderCode		as varchar(10),
	@as_FolderName		as varchar(8000)
AS
	UPDATE
		Folder
	SET
		FolderName = @as_FolderName
	WHERE
		FolderCode = @as_FolderCode
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

