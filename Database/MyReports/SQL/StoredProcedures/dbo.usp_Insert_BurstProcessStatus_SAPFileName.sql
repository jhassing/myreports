SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_BurstProcessStatus_SAPFileName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_BurstProcessStatus_SAPFileName]
GO





/****** Object:  Stored Procedure dbo.usp_InsertFolders    Script Date: 1/20/2004 8:57:09 PM ******/
--JJS modified 1/20/2004
CREATE   PROCEDURE dbo.usp_Insert_BurstProcessStatus_SAPFileName
	@as_SAPfileName			as varchar(25),
	@ai_ident 				INT OUTPUT
AS
	INSERT INTO  dbo.BurstProcessStatus
		(
			SAPfileName,
			ReportName,
			StartedTime,
			BurstProcessErrorCodeID
		)
		VALUES
		(
			@as_SAPfileName,
			" ",
			GetDate(),
			0
		)

	SELECT @ai_ident = @@Identity
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

