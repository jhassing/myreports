SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Folders_Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Folders_Select]
GO


CREATE PROCEDURE dbo.Folders_Select
AS
	SET NOCOUNT ON;
SELECT  FolderCode, FolderName FROM tblFolders

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

