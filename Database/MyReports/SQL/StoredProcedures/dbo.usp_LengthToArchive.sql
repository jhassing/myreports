SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_LengthToArchive]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_LengthToArchive]
GO




/****** Object:  Stored Procedure dbo.usp_UpdateFolders    Script Date: 10/10/2003 3:57:09 PM ******/
--JJS modified 10/19/2003
CREATE PROCEDURE usp_LengthToArchive

@ReportCode varchar(10),
@ReturnLengthToArchive int output

AS

SET @ReturnLengthToArchive = (SELECT ArchiveLength
FROM         dbo.tblReports  WITH (NOLOCK)
WHERE   ReportCode = @ReportCode)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

