SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_FolderTableUpdateRecord_Master]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_FolderTableUpdateRecord_Master]
GO




/****** Object:  Stored Procedure dbo.usp_InsertFolders    Script Date: 12/10/2003 3:57:09 PM ******/
--JJS modified 12/31/2003
CREATE   PROCEDURE dbo.usp_Insert_FolderTableUpdateRecord_Master

AS


	INSERT  INTO FolderNameChange_Master
		(
			DateRun

		)
		VALUES
		(
			GetDate()
		)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

