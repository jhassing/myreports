USE [MyReports]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_UserListing]    Script Date: 12/05/2008 15:44:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_UserListing]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_UserListing]    
GO


CREATE PROCEDURE [dbo].[usp_Get_UserListing]
	/*
	(
	@parameter1 int = 5,
	@parameter2 datatype OUTPUT
	)
	*/
AS
SELECT     UserConversionID, firstName, lastName, perNo, networkID, [Exists]
--SELECT     UserConversionID, firstName, lastName, perNo, networkID

FROM         dbo.UserConversion

