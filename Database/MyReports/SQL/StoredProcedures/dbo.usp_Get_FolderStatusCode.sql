SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_FolderStatusCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_FolderStatusCode]
GO





/****** Object:  Stored Procedure dbo.usp_FolderStatusCode   Script Date: 3/10/2004 3:57:09 PM ******/

CREATE    PROCEDURE dbo.usp_Get_FolderStatusCode

AS

	SELECT 
		* 
	FROM 
		FolderStatusCode


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

