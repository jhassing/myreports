if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetReportsViewedList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetReportsViewedList]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


CREATE   procedure dbo.usp_GetReportsViewedList
(
	@UserName		varchar(50),
	@ReportName		varchar(250),
	@CreationTime		DateTime,
	@ReturnValue 		bit output,
	@ViewedDate		DateTime output
)
as

	SET NOCOUNT ON
	select @ReturnValue = count(0) from tblReportViewedDates where username = @UserName and [ReportName] = @ReportName and CreationTime like @CreationTime
	
	if @ReturnValue <> 0
		begin
			select @ViewedDate =  ViewedDate from tblReportViewedDates where username = @UserName and [ReportName] = @ReportName and CreationTime like @CreationTime
		end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

