SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_ValidReports]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_ValidReports]
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Get_ValidReports

Description:	Gets a listing of valid reports from the database.
  
Parameters:	@ai_ReportTypeID 		int - Optional Report Type ID

Returns:	Listing of valid reports

Revisions:	12/19/2003 John Schiavarelli Created

Example:	exec usp_Get_ValidReports
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Get_ValidReports
	@ai_ReportTypeID int = -1
AS

	SET NOCOUNT ON

IF @ai_ReportTypeID = -1
	SELECT DISTINCT 
		rpt.ReportCode, 
		rpt.ArchiveLength, 
		rt.ReportType, 
		ov.OverwriteDescription, 
		av.ArchiveValue, 
		ov.OverwriteValueID, 
		rt.ReportTypeID, 
		av.ArchiveValueID
	FROM         
		Report rpt 
	LEFT OUTER JOIN
                      	ReportType rt ON rpt.ReportTypeID = rt.ReportTypeID 
	LEFT OUTER JOIN
                      	OverwriteValue ov ON rpt.OverwriteValueID = ov.OverwriteValueID 
	LEFT OUTER JOIN
                      	ArchiveValue av ON rpt.ArchiveValueID = av.ArchiveValueID
	ORDER BY 
		rpt.ReportCode

ELSE
	SELECT DISTINCT 
		rpt.ReportCode, 
		rpt.ArchiveLength, 
		rt.ReportType, 
		ov.OverwriteDescription, 
		av.ArchiveValue, 
		ov.OverwriteValueID, 
		rt.ReportTypeID, 
		av.ArchiveValueID
	FROM         
		Report rpt 
	LEFT OUTER JOIN
                      	ReportType rt ON rpt.ReportTypeID = rt.ReportTypeID 
	LEFT OUTER JOIN
                      	OverwriteValue ov ON rpt.OverwriteValueID = ov.OverwriteValueID 
	LEFT OUTER JOIN
                      	ArchiveValue av ON rpt.ArchiveValueID = av.ArchiveValueID
	WHERE 
		rpt.ReportTypeID =  @ai_ReportTypeID
	ORDER BY 
		rpt.ReportCode
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

