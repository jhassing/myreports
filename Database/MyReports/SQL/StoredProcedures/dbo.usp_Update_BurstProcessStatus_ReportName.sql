SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_BurstProcessStatus_ReportName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_BurstProcessStatus_ReportName]
GO




/****** Object:  Stored Procedure dbo.usp_UpdateFolders    Script Date: 1/20/2004 6:12:09 PM ******/
--JJS modified 1/21/2003
CREATE   PROCEDURE dbo.usp_Update_BurstProcessStatus_ReportName
	@as_ReportName		as varchar(400),
	@ai_Ident			as int,
	@as_ReportCode		as varchar(20)
AS
	UPDATE
		BurstProcessStatus
	SET	
		ReportName = @as_ReportName, 
		ReportCode = @as_ReportCode
	WHERE
		BurstProcessStatusID = @ai_Ident
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

