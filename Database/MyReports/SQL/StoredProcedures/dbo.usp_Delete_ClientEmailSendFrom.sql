SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Delete_ClientEmailSendFrom]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Delete_ClientEmailSendFrom]
GO

/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Delete_ClientEmailSendFrom

Description:	Deletes a client from email 
  
Parameters:	

Returns:	Nothing
Algorithm:	Deletes the recrod

Revisions:	03/14/2006 John Schiavarelli Created

Example:	exec usp_Delete_ClientEmailSendFrom 1
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Delete_ClientEmailSendFrom
	@ai_ClientEmailSendFromID	as int
AS
	DELETE FROM ClientEmailSendFrom

	WHERE ClientEmailSendFromID = @ai_ClientEmailSendFromID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

