SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_ClientEmailSendFromByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_ClientEmailSendFromByPrimaryKey]
GO


/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_ClientEmailSendFromListing

Description:	Locates a client email addresss listing
  
Parameters:	

Returns:	Client email address listing		

Revisions:	03/14/2006 John Schiavarelli created

Example:	exec usp_Get_ClientEmailSendFromByPrimaryKey 22
------------------------------------------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_ClientEmailSendFromByPrimaryKey
(
	@ai_ClientEmailSendFromID 	int
)
AS

	SET NOCOUNT ON
	
	SELECT     ClientEmailSendFromID, FromEmailAddress, CreatedBy, CreatedDateTime, ModifiedBy, ModifiedDateTime
	
	FROM         dbo.ClientEmailSendFrom
	
	WHERE 
		ClientEmailSendFromID = @ai_ClientEmailSendFromID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

