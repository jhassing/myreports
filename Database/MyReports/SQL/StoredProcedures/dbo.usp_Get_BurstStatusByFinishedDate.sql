SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_BurstStatusByFinishedDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_BurstStatusByFinishedDate]
GO




/*--------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_BurstStatusByFinishedDate

Description:	Used to return last step completed in burst process
Parameters:	

Returns:	
Algorithm:	
Revisions:	06/14/2005 Monica Hancock Modified - Rewrite of usp_GetBurstStatusByFinishedDate
		
Example:	
		usp_GetBurstStatusByFinishedDate '2005-06-16 13:00:00', '2005-06-17 13:00:00'
-------------------------------------------------------------------------------------------------------------------------------------------*/


CREATE   PROCEDURE [dbo].[usp_Get_BurstStatusByFinishedDate] 

	@ad_BeginningDate as DateTime,
	@ad_EndingDate as DateTime

AS



SELECT DISTINCT 	
	bps.ReportName, 
	bps.ReportCode, 
	bps.StartedTime, 	
	MAX(bpsc.StatusCode) AS StatusCode

FROM         
	BurstProcessStatus  bps WITH (NOLOCK) 

INNER JOIN
            BurstProcessStatus_BurstProcessStatusCode bpsbpsc WITH (NOLOCK) ON bps.BurstProcessStatusID = bpsbpsc.BurstProcessStatusID 

INNER JOIN
             BurstProcessStatusCode bpsc WITH (NOLOCK) ON bpsbpsc.BurstProcessStatusCodeID = bpsc.BurstProcessStatusCodeID

GROUP BY 
	bps.ReportName, bps.ReportCode, bps.StartedTime, bps.BurstProcessErrorCodeID

HAVING      
	(bps.StartedTime BETWEEN CONVERT(DATETIME, @ad_BeginningDate, 102) AND CONVERT(DATETIME, @ad_EndingDate, 102)) 
	AND (bps.BurstProcessErrorCodeID = 0)

ORDER BY bps.StartedTime





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

