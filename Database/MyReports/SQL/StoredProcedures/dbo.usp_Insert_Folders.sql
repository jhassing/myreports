SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_Folders]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_Folders]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Update_Folders

Description:	Inserts a new folder code and folder description into the database
  
Parameters:	@as_FolderCode 		varchar(10) 	- Folder Code to insert
		@as_FolderName		varchar(10)	- Folder description to insert

Returns:	Nothing

Revisions:	11/18/2003 John Schiavarelli Created
		01/04/2004 John Schiavarelli modified 

Example:	
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Insert_Folders
	@as_FolderCode		as varchar(10),
	@as_FolderName		as varchar(8000)
AS
	INSERT INTO  dbo.Folder
		(
			FolderCode,
			FolderName
		)
	VALUES
		(
			@as_FolderCode,
			@as_FolderName
		)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

