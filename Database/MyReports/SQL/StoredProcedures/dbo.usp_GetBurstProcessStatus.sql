SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetBurstProcessStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetBurstProcessStatus]
GO




/****** Object:  Stored Procedure dbo.usp_GetBurstProcessStatus   Script Date: 10/10/2003 3:57:09 PM ******/
--JJS modified 10/19/2003
CREATE    PROCEDURE [usp_GetBurstProcessStatus] 
	@Beginning_Date DateTime, 
	@Ending_Date DateTime 

AS

SELECT * FROM tblBurstProcessStatus
WHERE StartedTime Between @Beginning_Date And @Ending_Date

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

