SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_doesReportExist]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_doesReportExist]
GO




/****** Object:  Stored Procedure dbo.usp_UpdateFolders    Script Date: 10/10/2003 3:57:09 PM ******/
--JJS modified 1/5/2004
CREATE PROCEDURE usp_doesReportExist 

@ReportCode varchar(10),
@ReturnValue varchar(10) output

AS

if exists(select  ReportCode from dbo.tblReports  WITH (NOLOCK) where   ReportCode = @ReportCode)
	set @ReturnValue = 1
else
	set @ReturnValue = 0

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

