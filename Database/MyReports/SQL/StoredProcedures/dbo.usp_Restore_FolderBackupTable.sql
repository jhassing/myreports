SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Restore_FolderBackupTable]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Restore_FolderBackupTable]
GO



CREATE PROCEDURE dbo.usp_Restore_FolderBackupTable

AS

	DELETE FROM Folder

	INSERT INTO Folder(FolderCode, FolderName)  
	SELECT  FolderCode, FolderName from Folder_backup


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

