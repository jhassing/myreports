if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_UserAuditData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_UserAuditData]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO




/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Insert_UserAuditData

Description:	Inserts a new record for audit tracking
  
Parameters:	@as_DomainName 		varchar(15) 	- Domain used to log into
		@as_SessionID			varchar(250)	- Current web session ID
		@as_UserName			varchar(10)  	- Current user logging into the site
		@as_Platform 			varchar(15) 	- Client platform
		@as_Browser			varchar(10)  	- Client browser type
		@ad_BrowserVersion		varchar(20)  	- Client browser version
		@as_UserHostAddress		varchar(20)  	- Client IP Address
		@as_WorkstationName		varchar(250) 	- Client workstation name.		

Returns:	Nothing
Algorithm:	Update master recrod

Revisions:	03/28/2005 Monica Hancock Created
Modified:	03/28/2005 John Schiavarelli (Made it work the correct way !!!)

Example:	exec usp_Insert_UserAuditData 'na.compassGroup.corp', '12345', 'schiaj01', 'WinXP', 'IE', 6.0, '128.1.1.2', 'localhost'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE  PROCEDURE dbo.usp_Insert_UserAuditData  

	@as_DomainName 		varchar(20),
	@as_SessionID			varchar(250),
	@as_UserName 		varchar(10),
	@as_Platform			varchar(15),
	@as_Browser			varchar(10),
	@ad_BrowserVersion		varchar(20),
	@as_UserHostAddress		varchar(20),
	@as_WorkstationName		varchar(250)

AS

Insert into User_Audit (DomainName,SessionID, Username, LoginDateTime,Platform,Browser,BrowserVersion,UserHostAddress,WorkstationName)
Values (@as_DomainName,@as_SessionID, @as_UserName, getDate(),@as_Platform,@as_Browser,@ad_BrowserVersion,@as_UserHostAddress,@as_WorkstationName)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

