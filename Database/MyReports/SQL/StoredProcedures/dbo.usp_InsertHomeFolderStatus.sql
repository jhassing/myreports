SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertHomeFolderStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertHomeFolderStatus]
GO



/****** Object:  Stored Procedure dbo.usp_InsertHomeFolderStatus    Script Date: 3/22/2004 12:21:01 PM ******/
--JJS modified 3/22/2004
CREATE   procedure dbo.usp_InsertHomeFolderStatus
	@UserID				as varchar(50),
	@ExistingHomeFolderName		as varchar(400),
	@NewHomeFolderName			as varchar(400)
as

	DECLARE  @ID varchar(10)

	SET @ID = (select max (tblFolderNameChanges_Details_MasterID) from tblFolderNameChanges_Master)

	insert tblHomeFolderStatus
		(
			UserID,
			ExistingHomeFolderName,
			NewHomeFolderName,
			tblFolderNameChanges_MasterFK

		)
		values
		(
			@UserID,
			@ExistingHomeFolderName,
			@NewHomeFolderName,
			@ID
		)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

