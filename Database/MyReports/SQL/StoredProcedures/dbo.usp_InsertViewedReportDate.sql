if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertViewedReportDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertViewedReportDate]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


CREATE   procedure dbo.usp_InsertViewedReportDate
	@UserName		as varchar(50),
	@ReportName		as varchar(250),
	@CreationTime		as DateTime,
	@ViewedDate		as DateTime

as

	declare	@ReturnValue bit

	SET NOCOUNT ON
	select @ReturnValue = count(0) from tblReportViewedDates where username = @UserName and [ReportName] = @ReportName

	if @ReturnValue = 0
		begin

		insert  tblReportViewedDates
			(
				UserName,
				[ReportName],
				CreationTime,
				ViewedDate
			)
			values
			(
				@UserName,
				@ReportName,
				@CreationTime,
				@ViewedDate
			)
		end

	else
		begin
			update tblReportViewedDates
			Set ViewedDate = @ViewedDate,
			CreationTime = @CreationTime	
			WHERE username = @UserName and [ReportName] = @ReportName
		end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

