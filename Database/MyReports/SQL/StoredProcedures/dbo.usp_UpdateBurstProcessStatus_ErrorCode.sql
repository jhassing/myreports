SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_UpdateBurstProcessStatus_ErrorCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_UpdateBurstProcessStatus_ErrorCode]
GO



/****** Object:  Stored Procedure dbo.usp_UpdateFolders    Script Date: 1/20/2004 6:12:09 PM ******/
--JJS modified 1/21/2003
CREATE   procedure dbo.usp_UpdateBurstProcessStatus_ErrorCode
	@ErrorCode		as int,
	@Ident			as int
as
	update	dbo.tblBurstProcessStatus
	set	tblBurstProcessErrorCodesFK= @ErrorCode
	where	tblBurstProcessStatusPK = @Ident

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

