SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_ClientEmailAddressByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_ClientEmailAddressByPrimaryKey]
GO


/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_ClientEmailAddressByPrimaryKey

Description:	Locates a client email addresss by the primary key
  
Parameters:	

Returns:	One record 	

Revisions:	03/16/2006 John Schiavarelli created

Example:	exec usp_Get_ClientEmailAddressByPrimaryKey 5
------------------------------------------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_ClientEmailAddressByPrimaryKey
(
	@ai_ClientEmailAddressID 	as int
)
AS

	SET NOCOUNT ON
	
	SELECT     dbo.ClientEmailSendFrom.FromEmailAddress AS FromEmailAddress, dbo.ClientEmailAddress.ClientEmailAddressID, 
	                      dbo.ClientEmailAddress.ClientEmailAddress, dbo.ClientEmailAddress.ClientName, dbo.ClientEmailAddress.KeyWord, 
	                      dbo.ClientEmailAddress.ClientEmailSendFromID, dbo.ClientEmailAddress.CC1, dbo.ClientEmailAddress.BCC1, dbo.ClientEmailAddress.CC2, 
	                      dbo.ClientEmailAddress.AccountantFN, dbo.ClientEmailAddress.AccountantLN, dbo.ClientEmailAddress.Region, dbo.ClientEmailAddress.CreatedBy, 
	                      dbo.ClientEmailAddress.CreatedDateTime, dbo.ClientEmailAddress.ModifiedBy, dbo.ClientEmailAddress.ModifiedDateTime
	FROM         dbo.ClientEmailSendFrom INNER JOIN
	                      dbo.ClientEmailAddress ON dbo.ClientEmailSendFrom.ClientEmailSendFromID = dbo.ClientEmailAddress.ClientEmailSendFromID
	 
	WHERE 
		( ClientEmailAddressID = @ai_ClientEmailAddressID )
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

