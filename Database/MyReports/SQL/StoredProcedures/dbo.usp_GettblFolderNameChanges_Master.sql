SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GettblFolderNameChanges_Master]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GettblFolderNameChanges_Master]
GO




/****** Object:  Stored Procedure dbo.usp_GetBurstProcessStatus   Script Date: 3/10/2004 3:57:09 PM ******/

CREATE    PROCEDURE [usp_GettblFolderNameChanges_Master] 

AS

	SELECT * 
	FROM tblFolderNameChanges_Master

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

