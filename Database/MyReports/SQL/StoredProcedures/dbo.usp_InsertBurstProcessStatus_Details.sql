SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertBurstProcessStatus_Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertBurstProcessStatus_Details]
GO



/****** Object:  Stored Procedure dbo.usp_InsertBurstProcessStatus_Details Script Date: 4/13/2004 3:57:09 PM ******/
--JJS modified 4/21/2004
CREATE   procedure dbo.usp_InsertBurstProcessStatus_Details
	@tblBurstProcessStatusFK		as int,
	@ReportLocation			as varchar(255)
as
	insert  dbo.tblBurstProcessStatus_Details
		(
			tblBurstProcessStatusFK,
			ReportLocation
		)
		values
		(
			@tblBurstProcessStatusFK,
			@ReportLocation
		)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

