SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertFolderTableUpdateRecord_Master]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertFolderTableUpdateRecord_Master]
GO



/****** Object:  Stored Procedure dbo.usp_InsertFolders    Script Date: 12/10/2003 3:57:09 PM ******/
--JJS modified 12/31/2003
CREATE   procedure dbo.usp_InsertFolderTableUpdateRecord_Master

as


	insert  tblFolderNameChanges_Master
		(
			DateRun

		)
		values
		(
			GetDate()
		)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

