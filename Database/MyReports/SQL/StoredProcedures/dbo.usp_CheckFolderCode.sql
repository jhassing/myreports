SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_CheckFolderCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_CheckFolderCode]
GO




/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_CheckFolderCode

Description:	Checks to see if a folder code exists
  
Parameters:	@as_FolderCode 		varchar(10) 	- Folder Code to locate
		@as_ReturnValue		varchar(10)	- Return value

Returns:	Nothing

Revisions:	11/18/2003 John Schiavarelli Created
		01/04/2004 John Schiavarelli modified 

Example:	
		declare @as_FolderCode varchar(50)
		declare @ab_ReturnValue int
		
		set @as_FolderCode = 'cha'
		
		exec usp_CheckFolderCode @as_FolderCode, @ab_ReturnValue out
		
		print @ab_ReturnValue
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE  PROCEDURE dbo.usp_CheckFolderCode
	@as_FolderCode varchar(10),
	@ab_ReturnValue bit output
AS

IF EXISTS(SELECT FolderCode FROM Folder WITH (NOLOCK) WHERE FolderCode=@as_FolderCode)
	SET @ab_ReturnValue = 1
ELSE
	SET @ab_ReturnValue = 0
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

