SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[dbo.usp_FolderStatusCodes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[dbo.usp_FolderStatusCodes]
GO




/****** Object:  Stored Procedure dbo.usp_FolderStatusCodes   Script Date: 3/10/2004 3:57:09 PM ******/

CREATE    PROCEDURE [dbo.usp_FolderStatusCodes] 

AS

	SELECT * 
	FROM tblFolderStatusCodes

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

