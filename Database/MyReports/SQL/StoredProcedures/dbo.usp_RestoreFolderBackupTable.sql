SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_RestoreFolderBackupTable]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_RestoreFolderBackupTable]
GO


CREATE PROCEDURE dbo.usp_RestoreFolderBackupTable

as

delete from tblFolders

insert into tblFolders(FolderCode, FolderName)  Select FolderCode, FolderName from tblFolders_backup

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

