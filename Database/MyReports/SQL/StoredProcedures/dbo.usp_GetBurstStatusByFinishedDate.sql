SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetBurstStatusByFinishedDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetBurstStatusByFinishedDate]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_GetBurstStatusByFinishedDate

Description:	Used to return last step completed in burst process
Parameters:	

Returns:	
Algorithm:	
Revisions:	06/14/2005 Monica Hancock Modified - Rewrite of usp_GetBurstStatusByFinishedDate
		6/20/2005  MH Modified - Added StatusCode = 5 criteria to select statement
		6/21/2005  MH Modified - Added tblReports to select criteria to sort by report type ID.
		
Example:	
		usp_GetBurstStatusByFinishedDate '2004-06-22 08:00:01', '2004-06-23 08:00:00'
-------------------------------------------------------------------------------------------------------------------------------------------*/


CREATE   PROCEDURE [dbo].[usp_GetBurstStatusByFinishedDate] 

	@ad_BeginningDate as DateTime,
	@ad_EndingDate as DateTime

as


SELECT DISTINCT     
	bps.ReportName, 
	bps.ReportCode, 
	bps.StartedTime, 
	MAX(bpsc.StatusCode) AS StatusCode,
	rpt.ReportTypeID

FROM 
	tblBurstProcessStatus bps WITH (NOLOCK)  

INNER JOIN
                      tblBurstProcessStatus_tblBurstProcessStatusCodes bpsbpsc WITH (NOLOCK)  ON bps.tblBurstProcessStatusPK = bpsbpsc.tblBurstProcessStatusFK 
INNER JOIN
                      tblBurstProcessStatusCodes bpsc WITH (NOLOCK)  ON bpsbpsc.tblBurstProcessStatusCodesFK = bpsc.tblBurstProcessStatusCodesPK 
INNER JOIN
                      tblReports rpt WITH (NOLOCK) ON bps.ReportCode = rpt.ReportCode

GROUP BY bps.ReportName, bps.ReportCode, bps.StartedTime, bps.tblBurstProcessErrorCodesFK, rpt.ReportTypeID

HAVING      
	(bps.StartedTime BETWEEN CONVERT(DATETIME, @ad_BeginningDate, 102) AND CONVERT(DATETIME, @ad_EndingDate, 102)) 
	AND (bps.tblBurstProcessErrorCodesFK = 0) AND
	(MAX(bpsc.StatusCode) = 5)

ORDER BY rpt.ReportTypeID DESC, bps.StartedTime DESC



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

