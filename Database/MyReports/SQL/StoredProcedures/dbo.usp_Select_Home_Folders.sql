SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Select_Home_Folders]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Select_Home_Folders]
GO




/*----------------------------------------------------------------------------------------------------------------------------------------------------------	
Stored Proc:	usp_Select_Home_Folders

Parameters:	@as_UserGroupMembership varchar (1000)

Description:  	Selects home folder for particular user

Revisions:	01/13/2006 Created - Monica Hancock

EXAMPLE:	exec usp_Select_Home_Folders 'cha|cha07|cha02|2104'
		
----------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Select_Home_Folders 

	@as_UserGroupMembership varchar (1000)


AS


DECLARE	@ls_HomeFolder 		varchar (500)
DECLARE	@li_charpos 		integer
DECLARE	@li_StartPos 		integer
DECLARE	@ls_delimiter 		char(1)
DECLARE	@ll_ErrorCode		INTEGER		-- Captures the SQL error number
DECLARE	@ls_ErrorMessage	VARCHAR(200)		-- Used to store a custom error message
DECLARE	@ls_SystemErrorMsg	VARCHAR(255)		-- Captures the systems error message
DECLARE @ls_SQL 		VARCHAR(2000)


SET @ls_delimiter = '|'
SET @li_charpos = 1
SET @li_StartPos = 0
SET @ls_HomeFolder = ''''

If RIGHT(@as_UserGroupMembership, 1) <> '|'
	BEGIN
		SET @as_UserGroupMembership = @as_UserGroupMembership + @ls_delimiter
	END

WHILE @li_charpos <=Len(@as_UserGroupMembership)
BEGIN
	SET @li_charpos = @li_charpos + 1
		IF SUBSTRING(@as_UserGroupMembership, @li_charpos, 1) = @ls_delimiter
			BEGIN
				Select @ls_HomeFolder = @ls_HomeFolder + ''', ''' + substring(@as_UserGroupMembership, @li_StartPos, @li_charpos - @li_StartPos)
				Set @li_StartPos = @li_charpos + 1

			END
							
END
	
		SET @ls_HomeFolder = substring(@ls_HomeFolder, 6, Len(@ls_HomeFolder))
		
		SET @ls_SQL = 'SELECT FolderCode, FolderName, FolderCode + FolderName as FolderCode_Name FROM Folder Where FolderCode in(''' + @ls_HomeFolder + ''')'
		--PRINT @ls_SQL
		EXEC(@ls_SQL)
		


RETURN

--This is the error handler to rollback transactions and end the program.

Error_Handler:
--	Cause the client to see the error message
	RAISERROR (@ls_ErrorMessage, 16, 1)
	RETURN 1


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

