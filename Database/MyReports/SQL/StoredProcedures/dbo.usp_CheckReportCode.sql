SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_CheckReportCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_CheckReportCode]
GO




/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_CheckReportCode

Description:	Checks to see if a report code is valid inside the MyReport ACL
  
Parameters:	@as_ReportCode 		varchar(10) 	- Report Code to locate
		@as_ReturnValue		varchar(10)	- Return value

Returns:	Nothing

Revisions:	10/10/2003 John Schiavarelli Created
		01/05/2004 John Schiavarelli modified 

Example:	
		DECLARE @i int
		exec usp_CheckReportCode 'ZCGL2370', @i out
		print @i
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE PROCEDURE dbo.usp_CheckReportCode

	@as_ReportCode varchar(10),
	@as_ReturnValue varchar(10) output

AS

IF EXISTS(SELECT  ReportCode FROM Report  WITH (NOLOCK) WHERE   ReportCode = @as_ReportCode)
	SET @as_ReturnValue = 1
ELSE
	SET @as_ReturnValue = 0
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

