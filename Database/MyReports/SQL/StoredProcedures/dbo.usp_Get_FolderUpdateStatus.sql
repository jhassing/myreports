SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_FolderUpdateStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_FolderUpdateStatus]
GO



CREATE PROCEDURE [dbo].[usp_Get_FolderUpdateStatus] 
	@FolderStatusMasterID	as int,
	@FolderStatusCode		as int
AS

IF (@FolderStatusCode = "" or @FolderStatusCode = NULL or @FolderStatusCode = 0)

	SELECT    
		fd.DateChanged, 
		fd.FolderCode, 
		fd.ExistingName, 
		fd.NewName, 
		fsc.StatusDescription, 
		fm.FolderNameChangeMasterID, 
                      	fd.FolderStatusCodeID
	FROM         
		FolderNameChange_Detail fd 
	INNER JOIN
                      FolderStatusCode fsc ON fd.FolderStatusCodeID = fsc.FolderStatusCodeID
	INNER JOIN
                      FolderNameChange_Master fm ON fd.FolderNameChangeMasterID = fm.FolderNameChangeMasterID	
	WHERE     
		(fm.FolderNameChangeMasterID = @FolderStatusMasterID)

ELSE

	SELECT     
		fd.DateChanged, 
		fd.FolderCode, 
		fd.ExistingName, 
		fd.NewName, 
		fsc.StatusDescription, 
		fm.FolderNameChangeMasterID, 
	            fd.FolderStatusCodeID
	FROM        
		FolderNameChange_Detail fd 
	INNER JOIN
                      FolderStatusCode fsc ON fd.FolderStatusCodeID = fsc.FolderStatusCodeID
	INNER JOIN
                      FolderNameChange_Master fm ON fd.FolderNameChangeMasterID = fm.FolderNameChangeMasterID	
	WHERE     
		(fm.FolderNameChangeMasterID = @FolderStatusMasterID) 
	AND 
		(fd.FolderStatusCodeID = @FolderStatusCode)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

