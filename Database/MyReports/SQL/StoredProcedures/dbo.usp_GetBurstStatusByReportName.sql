SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetBurstStatusByReportName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetBurstStatusByReportName]
GO


CREATE PROCEDURE [dbo].[usp_GetBurstStatusByReportName] 
	@ReportName varchar(400)

as



SELECT     TOP 100 PERCENT dbo.tblBurstProcessStatus.ReportCode,  dbo.tblBurstProcessStatus.tblBurstProcessStatusPK, dbo.tblBurstProcessStatus.SAPfileName, 
		MAX(dbo.tblBurstProcessStatus.ReportName) AS ReportName, 
	(SELECT [Description] from tblBurstProcessStatusCodes where StatusCode=MAX(dbo.tblBurstProcessStatus_tblBurstProcessStatusCodes.tblBurstProcessStatusCodesFK)) as Description,
	MIN(dbo.tblBurstProcessStatus.StartedTime) as StartedTime,
	  MAX(dbo.tblBurstProcessStatus_tblBurstProcessStatusCodes.ProcessTime) as [Finished Time]
FROM         dbo.tblBurstProcessStatus_tblBurstProcessStatusCodes RIGHT OUTER JOIN
                      dbo.tblBurstProcessStatus ON 
                      dbo.tblBurstProcessStatus_tblBurstProcessStatusCodes.tblBurstProcessStatusFK = dbo.tblBurstProcessStatus.tblBurstProcessStatusPK

GROUP BY dbo.tblBurstProcessStatus.ReportCode, dbo.tblBurstProcessStatus.SAPfileName, dbo.tblBurstProcessStatus.tblBurstProcessErrorCodesFK,  dbo.tblBurstProcessStatus.tblBurstProcessStatusPK

HAVING      (dbo.tblBurstProcessStatus.tblBurstProcessErrorCodesFK = 0) AND  (MAX(dbo.tblBurstProcessStatus.ReportName) LIKE '%' + @ReportName + '%')


ORDER BY dbo.tblBurstProcessStatus.ReportCode, StartedTime desc

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

