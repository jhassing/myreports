SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_doesFolderCodeExist]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_doesFolderCodeExist]
GO




/****** Object:  Stored Procedure dbo.usp_UpdateFolders    Script Date: 11/18/2003 3:57:09 PM ******/
--JJS modified 1/4/2004
CREATE  PROCEDURE dbo.usp_doesFolderCodeExist


@FolderCode varchar(10),
@ReturnValue bit output

as

if exists(select FolderCode from dbo.tblFolders WITH (NOLOCK) where FolderCode=@FolderCode)
	set @ReturnValue = 1
else
	set @ReturnValue = 0

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

