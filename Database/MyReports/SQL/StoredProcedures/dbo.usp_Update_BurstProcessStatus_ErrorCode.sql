SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_BurstProcessStatus_ErrorCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_BurstProcessStatus_ErrorCode]
GO




/****** Object:  Stored Procedure dbo.usp_UpdateFolders    Script Date: 1/20/2004 6:12:09 PM ******/
--JJS modified 1/21/2003
CREATE   PROCEDURE dbo.usp_Update_BurstProcessStatus_ErrorCode
	@ai_ErrorCode			as int,
	@ai_Ident			as int
AS
	UPDATE
		BurstProcessStatus
	SET	
		BurstProcessErrorCodeID= @ai_ErrorCode
	WHERE	
		BurstProcessStatusID = @ai_Ident
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

