if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_ViewedReportDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_ViewedReportDate]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Get_ValidReports

Description:	Gets a listing of valid reports from the database.
  
Parameters:	@as_UserName		as varchar(50),		User who viewed the report.
		@as_ReportName	as varchar(250),		Report that the user has viewed.
		@ad_CreationTime	as DateTime,		Date/Time the report was created.
		@ad_ViewedDate	as DateTime		Date/Time the report was viewed.
		@as_ReportCode		as varchar(20)		ReportCode

Returns:	Nothing

Revisions:	04/29/2004 John Schiavarelli Created
		02/08/2006 MJH Modified - added ReportCode to parameters

Example:	exec usp_Get_ValidReports
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE    PROCEDURE dbo.usp_Insert_ViewedReportDate
	@as_UserName			as varchar(50),
	@as_ReportName		as varchar(250),
	@ad_CreationTime		as DateTime,
	@ad_ViewedDate		as DateTime,
	@as_ReportCode		as varchar(20)

AS

	DECLARE	@lb_ReturnValue bit

	SET NOCOUNT ON
	SELECT 
		@lb_ReturnValue = count(0) 
	FROM 
		ReportViewedDate 
	WHERE 
		Username = @as_UserName 
	AND 
		ReportName = @as_ReportName

	IF @lb_ReturnValue = 0
		BEGIN

			INSERT INTO  ReportViewedDate
			(
				UserName,
				[ReportName],
				CreationTime,
				ViewedDate,
				ReportCode
			)
			VALUES
			(
				@as_UserName,
				@as_ReportName,
				@ad_CreationTime,
				@ad_ViewedDate,
				@as_ReportCode
			)
		END

	ELSE
		BEGIN
			UPDATE 
				ReportViewedDate
			SET 
				ViewedDate = @ad_ViewedDate,
				CreationTime = @ad_CreationTime,
				ReportCode = @as_ReportCode
	
			WHERE 
				Username = @as_UserName 
			AND 
				[ReportName] = @as_ReportName
		END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

