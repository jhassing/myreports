SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_ClientEmailSendFromListing]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_ClientEmailSendFromListing]
GO


/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_ClientEmailSendFromListing

Description:	Locates a client email addresss listing
  
Parameters:	

Returns:	Client email address listing		

Revisions:	03/14/2006 John Schiavarelli created

Example:	exec usp_Get_ClientEmailSendFromListing 'f'
------------------------------------------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_ClientEmailSendFromListing
(
	@as_FromEmailAddress 		as varchar(200)
)
AS

	SET NOCOUNT ON
	
	SELECT     ClientEmailSendFromID, FromEmailAddress
	FROM         dbo.ClientEmailSendFrom
	WHERE   FromEmailAddress like '%' + @as_FromEmailAddress + '%'
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

