SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_ViewedReports]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_ViewedReports]
GO





/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Get_ViewedReports

Description:	Gets a listing of reports viewed between a given date range.
  
Parameters:	@ad_StartDate 	datetime
		@ad_EndDate	datetime

Returns:	Listing of viewed reports

Revisions:	1/20/2006 Monica Hancock	Created

Example:	exec usp_Get_ViewedReports '1/18/2006', '1/20/2006'
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE     PROCEDURE dbo.usp_Get_ViewedReports
	@ad_StartDate datetime,
	@ad_EndDate datetime
AS

SET NOCOUNT ON


	SELECT     
		UserName, 
		ReportName, 
		ViewedDate
	FROM    
		ReportViewedDate
	WHERE
		convert(datetime, convert(varchar(12), ViewedDate), 101) 
	between 
		@ad_StartDate and @ad_EndDate
	AND
		ReportName is Not Null



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

