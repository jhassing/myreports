SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_FolderTableUpdateRecord_Detail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_FolderTableUpdateRecord_Detail]
GO




/****** Object:  Stored Procedure dbo.usp_InsertFolders    Script Date: 1/12/2004 12:21:01 PM ******/
--JJS modified 1/14/2004
CREATE   PROCEDURE dbo.usp_Insert_FolderTableUpdateRecord_Detail
	@as_FolderCode				as varchar(10),
	@as_ExistingFolderName			as varchar(400),
	@as_NewFolderName			as varchar(400),
	@ai_FolderStatusCode			as int
AS

	DECLARE  @ID varchar(10)

	SET @ID = (SELECT MAX (FolderNameChangeMasterID) from FolderNameChange_Master)

	INSERT INTO  FolderNameChange_Detail
		(
			FolderCode,
			ExistingName,
			NewName,
			FolderStatusCodeID,
			FolderNameChangeMasterID

		)
	VALUES
		(
			@as_FolderCode,
			@as_ExistingFolderName,
			@as_NewFolderName,
			@ai_FolderStatusCode,
			@ID
		)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

