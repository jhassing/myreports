SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Region_Code_From_DM_Code]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Region_Code_From_DM_Code]
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Get_Region_Code_From_DM_Code

Description:	Locate the region that a particular DM reports into 
  
Parameters:	@as_DMCode 			varchar(10) 	- DM code to search.
		@as_RegionCode		varchar(10)	- Region the DM reports into

Returns:	Region

Revisions:	02/10/2004 John Schiavarelli Created

Example:	declare @as_DMCode 		varchar(10)
		declare @as_RegionCode 		varchar(10)
		
		set @as_DMCode = 'cha02'
		
		exec usp_Get_Region_Code_From_DM_Code @as_DMCode, @as_RegionCode out
		
		print @as_RegionCode
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE    PROCEDURE [dbo].[usp_Get_Region_Code_From_DM_Code] 

	@as_DMCode 		varchar(10),
	@as_RegionCode 	varchar(10) output

AS

	SET @as_RegionCode = 
		(SELECT     TOP 1 Region
	FROM        
		Zrops WITH (NOLOCK)
	WHERE    
		(DM = @as_DMCode))
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

