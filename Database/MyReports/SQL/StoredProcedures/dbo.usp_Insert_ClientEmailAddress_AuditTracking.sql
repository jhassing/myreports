SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_ClientEmailAddress_AuditTracking]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_ClientEmailAddress_AuditTracking]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Insert_ClientEmailAddress_AuditTracking

Description:	Inserts a new Client Email Listing
  
Parameters:	@as_NetworkID			as varchar(50),       Network ID of the user sending the email
		@as_ClientEmailAddress 	as varchar(200),	    Client Email Address
		@as_ClientEmailSendFrom	as varchar(200),	    From Email Address
		@as_CC1			as varchar(200),     CC 1 Email Address
		@as_CC2			as varchar(200),     CC 2 Email Address
		@as_BCC1			as varchar(200),     BCC Email Address

Returns:	Nothing

Revisions:	03/14/2006 John Schiavarelli Created

Example:	exec usp_Insert_ClientEmailAddress_AuditTracking 'schiaj01', 'Test@msn.com',  'From', 'CC1@msn.com', 'CC2@msn.com', 'BCC1@msn.com'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Insert_ClientEmailAddress_AuditTracking
	@as_NetworkID			as varchar(50),
	@as_ClientEmailAddress 	as varchar(200),
	@as_ClientEmailSendFrom	as varchar(250),
	@as_CC1			as varchar(200),
	@as_CC2			as varchar(200),
	@as_BCC1			as varchar(200),
	@ai_ident 			int output

AS
	INSERT INTO ClientEmailAddress_AuditTracking
	(
		NetworkID,
		ClientEmailAddress,
		ClientEmailSendFrom,
		CC1,
		CC2,
		BCC1
	)	
	VALUES
	(
		@as_NetworkID,
		@as_ClientEmailAddress,
		@as_ClientEmailSendFrom,
		@as_CC1,
		@as_CC2,
		@as_BCC1
	)

	SELECT @ai_ident = @@Identity
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

