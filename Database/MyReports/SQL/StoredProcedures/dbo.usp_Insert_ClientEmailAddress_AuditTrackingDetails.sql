SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_ClientEmailAddress_AuditTrackingDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_ClientEmailAddress_AuditTrackingDetails]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Insert_ClientEmailAddress_AuditTrackingDetails

Description:	Inserts a new Client Email Listing Audit tracking detail
  
Parameters:	

Returns:	Nothing

Revisions:	03/14/2006 John Schiavarelli Created

Example:	exec usp_Insert_ClientEmailAddress_AuditTrackingDetails 35, 'FileName.pdf'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Insert_ClientEmailAddress_AuditTrackingDetails
(
	@ai_ClientEmailAddress_AuditTrackingID		as int,
	@as_FileName					as varchar(350)
)
AS
	INSERT INTO ClientEmailAddress_AuditTrackingDetails
	(
		ClientEmailAddress_AuditTrackingID,
		[FileName]
	)	
	VALUES
	(
		@ai_ClientEmailAddress_AuditTrackingID,
		@as_FileName
		
	)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

