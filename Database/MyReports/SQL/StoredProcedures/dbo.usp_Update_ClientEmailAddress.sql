SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_ClientEmailAddress]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_ClientEmailAddress]
GO





/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Update_ClientEmailAddress

Description:	Updates a client email address 
  
Parameters:	@ai_ClientEmailAddressID	as int,			Primary key of the record to update
		@as_ClientEmailAddress 	as varchar(200),		New Client email Address
		@as_ClientName		as varchar(200),		New Client Name
		@as_ClientContact		as varchar(200),		New Client Contact
		@as_CC1			as varchar(200),    	CC 1 Email Address
		@as_CC2			as varchar(200),     	CC 2 Email Address
		@as_CC3			as varchar(200),     	C 3 Email Address
		@as_AccountantFN		as varchar(50),       	Accountants First Name
		@as_AccountantLN		as varchar(100),     	Accountants Lats Name
		@as_Region  			as varchar(10),		New Region
		@as_ModifiedBy 		as varchar(25)		SamAccountName (from AD) of the user making the modificate

Returns:	Nothing

Revisions:	03/14/2006 John Schiavarelli Created

Example:	exec usp_Update_ClientEmailAddress 1, 'JohnSchiavarelli@msn.com', 'Client Name', 'Client Contact',5, 'CC1@msn.com', 'CC2@msn.com', 'CC3@msn.com', 'John', 'LastName','cha', 'schiaj02'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Update_ClientEmailAddress
	@ai_ClientEmailAddressID	as int,
	@as_ClientEmailAddress 	as varchar(200),
	@as_ClientName		as varchar(200),
	@as_KeyWord			as varchar(200),	
	@ai_ClientEmailSendFromID	as int,
	@as_CC1			as varchar(200),
	@as_CC2			as varchar(200),
	@as_BCC1			as varchar(200),
	@as_AccountantFN		as varchar(50),
	@as_AccountantLN		as varchar(100),
	@as_Region  			as varchar(10),
	@as_ModifiedBy 		as varchar(25)

AS
	UPDATE 
		ClientEmailAddress
	SET	
		ClientEmailAddress = @as_ClientEmailAddress,  
		ClientName = @as_ClientName, 
		KeyWord = @as_KeyWord, 
		ClientEmailSendFromID = @ai_ClientEmailSendFromID,
		CC1 = @as_CC1,
		CC2 = @as_CC2,
		BCC1 = @as_BCC1,
		AccountantFN = @as_AccountantFN,
		AccountantLN = @as_AccountantLN,
		Region = @as_Region,
		ModifiedBy = @as_ModifiedBy,
		ModifiedDateTime = getDate()
	WHERE
		ClientEmailAddressID = @ai_ClientEmailAddressID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

