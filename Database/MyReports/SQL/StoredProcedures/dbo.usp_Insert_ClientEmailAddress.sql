SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_ClientEmailAddress]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_ClientEmailAddress]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Insert_ClientEmailAddress

Description:	Inserts a new Client Email Listing
  
Parameters:	@as_ClientEmailAddress 	as varchar(200),	    New Email Address for the client
		@as_ClientName		as varchar(200),     Client name
		@as_ClientContact		as varchar(200),	    Contact at the client
		@as_CC1			as varchar(200),     CC 1 Email Address
		@as_CC2			as varchar(200),     CC 2 Email Address
		@as_CC3			as varchar(200),     CC 3 Email Address
		@as_AccountantFN		as varchar(50),       Accountants First Name
		@as_AccountantLN		as varchar(100),     Accountants Lats Name
		@as_Region  			as varchar(10),        Region the client belongs to
		@as_CreatedBy 		as varchar(25)        Person who is adding this

Returns:	Nothing

Revisions:	03/14/2006 John Schiavarelli Created

Example:	exec usp_Insert_ClientEmailAddress 'JohnSchiavarelli@msn.com', 'Client Name', 'Client Contact', 1, 'CC1@msn.com', 'CC2@msn.com', 'CC3@Msn.com', 'John', 'LastName','cha', 'schiaj02'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Insert_ClientEmailAddress
	@as_ClientEmailAddress 	as varchar(200),
	@as_ClientName		as varchar(200),
	@as_KeyWord			as varchar(200),	
	@ai_ClientEmailSendFromID	as int,
	@as_CC1			as varchar(200),
	@as_CC2			as varchar(200),
	@as_BCC1			as varchar(200),
	@as_AccountantFN		as varchar(50),
	@as_AccountantLN		as varchar(100),
	@as_Region  			as varchar(10),
	@as_CreatedBy 		as varchar(25)

AS
	INSERT INTO ClientEmailAddress
	(
		ClientEmailAddress,
		ClientName,
		KeyWord,
		ClientEmailSendFromID,
		CC1,
		CC2,
		BCC1,
		AccountantFN,
		AccountantLN,
		Region,
		CreatedBy,
		CreatedDateTime,
		ModifiedBy,
		ModifiedDateTime
	)	
	VALUES
	(
		@as_ClientEmailAddress,
		@as_ClientName,
		@as_KeyWord,
		@ai_ClientEmailSendFromID,
		@as_CC1,
		@as_CC2,
		@as_BCC1,
		@as_AccountantFN,
		@as_AccountantLN,
		@as_Region,
		@as_CreatedBy,
		getDate(),
		@as_CreatedBy,
		getDate()
	)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

