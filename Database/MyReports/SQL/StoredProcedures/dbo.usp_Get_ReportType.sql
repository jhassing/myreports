SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_ReportType]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_ReportType]
GO







/****** Object:  Stored Procedure dbo.usp_Get_ReportType    Script Date: 11/29/2004 12:24:14 PM ******/
CREATE    PROCEDURE [dbo].[usp_Get_ReportType] 
	
	@ReportCode varchar(20),
	@ReportTypeID int output

	AS
		
		SET NOCOUNT ON

		SET @ReportTypeID = 
			(SELECT 
				ReportTypeID
			FROM         
				dbo.Report WITH (NOLOCK)
			WHERE     
				ReportCode = @ReportCode)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

