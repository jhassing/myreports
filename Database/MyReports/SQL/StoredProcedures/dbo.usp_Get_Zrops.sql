SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Zrops]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Zrops]
GO





/****** Object:  Stored Procedure dbo.usp_GetBurstProcessStatus   Script Date: 3/10/2004 3:57:09 PM ******/

CREATE    PROCEDURE [dbo].[usp_Get_Zrops] 

AS

	SELECT * 
	FROM Zrops


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

