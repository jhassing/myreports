SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetBurstProcessStatusDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetBurstProcessStatusDetails]
GO


CREATE PROCEDURE dbo.usp_GetBurstProcessStatusDetails

	@BurstProcessStatusID as int

as
	Begin

		Select ReportLocation from tblBurstProcessStatus_Details

		Where tblBurstProcessStatusFK = @BurstProcessStatusID

		order by ReportLocation
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

