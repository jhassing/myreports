SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetZrops]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetZrops]
GO




/****** Object:  Stored Procedure dbo.usp_GetBurstProcessStatus   Script Date: 3/10/2004 3:57:09 PM ******/

CREATE    PROCEDURE [usp_GetZrops] 

AS

	SELECT * 
	FROM tblZrops

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

