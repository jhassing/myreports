SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Backup_FoldersTable]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Backup_FoldersTable]
GO



CREATE PROCEDURE dbo.usp_Backup_FoldersTable

AS

DELETE FROM Folder_Backup

INSERT INTO Folder_Backup (FolderCode, FolderName)  
SELECT FolderCode, FolderName from Folder


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

