if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_ReportViewedList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_ReportViewedList]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_ReportViewedList

Description:	Check and see if a user has already viewed a report
  
Parameters:	@as_UserName		varchar(50),
		@as_ReportName		varchar(250),
		@as_CreationTime	varchar(20)
		@ab_ReturnValue 	bit output,
		@ad_ViewedDate		DateTime output
)
Returns:		

Revisions:	04/03/2004 John Schiavarelli created
		02/10/2006 MH changed @ad_CreattionTime from datetime to varchar(20)

Example:	
------------------------------------------------------------------------------------------------------------------------------*/


CREATE    PROCEDURE dbo.usp_Get_ReportViewedList
(
	@as_UserName		varchar(50),
	@as_ReportName		varchar(250),
	@as_CreationTime	varchar(20),
	@ab_ReturnValue 	bit output,
	@ad_ViewedDate		DateTime output
)
AS

	SET NOCOUNT ON
	SELECT 
		@ab_ReturnValue = count(0) 
	FROM 
		ReportViewedDate 
	WHERE 
		Username = @as_UserName 
	AND 
		ReportName = @as_ReportName 
	AND 
		CreationTime = @as_CreationTime
	
	IF @ab_ReturnValue <> 0
		BEGIN
			SELECT @ad_ViewedDate =  ViewedDate from ReportViewedDate where username = @as_UserName and [ReportName] = @as_ReportName and CreationTime = @as_CreationTime
		END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

