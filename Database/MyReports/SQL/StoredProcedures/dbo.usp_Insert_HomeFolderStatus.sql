SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_HomeFolderStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_HomeFolderStatus]
GO




/****** Object:  Stored Procedure dbo.usp_InsertHomeFolderStatus    Script Date: 3/22/2004 12:21:01 PM ******/
--JJS modified 3/22/2004
CREATE   PROCEDURE dbo.usp_Insert_HomeFolderStatus
	@as_UserID				as varchar(50),
	@as_ExistingHomeFolderName		as varchar(400),
	@as_NewHomeFolderName			as varchar(400)
AS

	DECLARE  @ID varchar(10)

	SET @ID = (SELECT MAX(FolderNameChangeMasterID) from FolderNameChange_Master)

	INSERT INTO HomeFolderStatus
		(
			UserID,
			ExistingHomeFolderName,
			NewHomeFolderName,
			FolderNameChangeMasterID

		)
		VALUES
		(
			@as_UserID,
			@as_ExistingHomeFolderName,
			@as_NewHomeFolderName,
			@ID
		)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

