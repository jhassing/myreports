SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertBurstProcessStatus_BurstProcessStatusCodes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertBurstProcessStatus_BurstProcessStatusCodes]
GO



/****** Object:  Stored Procedure dbo.usp_InsertBurstProcessStatus_tblBurstProcessStatusCodesScript Date: 1/21/2004 3:57:09 PM ******/
--JJS modified 1/21/2004
CREATE   procedure dbo.usp_InsertBurstProcessStatus_BurstProcessStatusCodes
	@tblBurstProcessStatusFK		as int,
	@tblBurstProcessStatusCodesFK		as int
as
	insert  dbo.tblBurstProcessStatus_tblBurstProcessStatusCodes
		(
			tblBurstProcessStatusFK,
			tblBurstProcessStatusCodesFK,
			ProcessTime
		)
		values
		(
			@tblBurstProcessStatusFK,
			@tblBurstProcessStatusCodesFK,
			GetDate()
		)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

