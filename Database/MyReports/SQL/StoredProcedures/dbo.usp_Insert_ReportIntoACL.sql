SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_ReportIntoACL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_ReportIntoACL]
GO




/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Insert_ReportIntoACL

Description:	Inserts a Report in the ACL (Access control list) of reports allowed to burst for R.D.
  
Parameters:	@as_ReportCode 		varchar(15) 	- Report Code to be modified
		@ai_ArchiveLength		int  		- Archive length of the new report.
		@ai_ReportTypeID		int  		- Report type (i.e. Financial)
		@ai_ArchiveValue		int  		- Archive value (i.e. Months)
		@ai_OverwriteValue		int  		- Overwrite Value (i.e. Force each report to overwrite)

Returns:	Nothing
Algorithm:	Update master recrod

Revisions:	03/08/2005 John Schiavarelli Created

Example:	exec usp_InsertReportIntoACL 'AQL', 0, 0, 0, 0
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Insert_ReportIntoACL
	@as_ReportCode		as varchar(15),
	@ai_ArchiveLength		as int,
	@ai_ReportTypeID		as int,
	@ai_ArchiveValue		as int,
	@ai_OverwriteValue		as int
AS
	INSERT INTO Report (ReportCode, ArchiveLength, ReportTypeID, ArchiveValueID, OverwriteValueID) 
	VALUES (@as_ReportCode, @ai_ArchiveLength, @ai_ReportTypeID, @ai_ArchiveValue, @ai_OverwriteValue  )
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

