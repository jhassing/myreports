SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Region_Code_AND_DM_Code_From_Unit_Code]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Region_Code_AND_DM_Code_From_Unit_Code]
GO





/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Get_Region_Code_AND_DM_Code_From_Unit_Code

Description:	Locate the region that a particular DM reports into 
  
Parameters:	@as_DMCode 			varchar(10) 	- DM code to search.
		@as_RegionCode		varchar(10)	- Region the DM reports into

Returns:	Region

Revisions:	02/10/2004 John Schiavarelli Created

Example:	
		declare @as_UnitCode		varchar(10)
		declare @as_DMCode 		varchar(10)
		declare @as_RegionCode 		varchar(10)
		
		set @as_UnitCode = '0000002395'
		
		exec usp_Get_Region_Code_AND_DM_Code_From_Unit_Code @as_UnitCode, @as_DMCode out, @as_RegionCode out
		
		print @as_DMCode
		print @as_RegionCode
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE    PROCEDURE [dbo].[usp_Get_Region_Code_AND_DM_Code_From_Unit_Code] 
	@as_UnitCode varchar(10),
	@as_RegionCode varchar(10) output,
	@as_DMCode varchar(10) output

AS

	(SELECT 
		TOP 1 @as_RegionCode = REGION, 
		@as_DMCode = DM
	FROM         
		Zrops WITH (NOLOCK)
	WHERE     
		(Unit = @as_UnitCode))
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

