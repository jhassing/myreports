SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_UpdateBurstProcessStatus_ReportName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_UpdateBurstProcessStatus_ReportName]
GO



/****** Object:  Stored Procedure dbo.usp_UpdateFolders    Script Date: 1/20/2004 6:12:09 PM ******/
--JJS modified 1/21/2003
CREATE   procedure dbo.usp_UpdateBurstProcessStatus_ReportName
	@ReportName		as varchar(400),
	@Ident			as int,
	@ReportCode		as varchar(20)
as
	update	dbo.tblBurstProcessStatus
	set	ReportName = @ReportName, ReportCode = @ReportCode
	where	tblBurstProcessStatusPK = @Ident

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

