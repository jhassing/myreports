SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertFolderTableUpdateRecord_Detail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertFolderTableUpdateRecord_Detail]
GO



/****** Object:  Stored Procedure dbo.usp_InsertFolders    Script Date: 1/12/2004 12:21:01 PM ******/
--JJS modified 1/14/2004
CREATE   procedure dbo.usp_InsertFolderTableUpdateRecord_Detail
	@FolderCode				as varchar(10),
	@ExistingFolderName			as varchar(400),
	@NewFolderName			as varchar(400),
	@FolderStatusCode			as int
as

	DECLARE  @ID varchar(10)

	SET @ID = (select max (tblFolderNameChanges_Details_MasterID) from tblFolderNameChanges_Master)

	insert  tblFolderNameChanges_Details
		(
			FolderCode,
			ExistingName,
			NewName,
			FolderStatusCode,
			tblFolderNameChanges_MasterFK

		)
		values
		(
			@FolderCode,
			@ExistingFolderName,
			@NewFolderName,
			@FolderStatusCode,
			@ID
		)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

