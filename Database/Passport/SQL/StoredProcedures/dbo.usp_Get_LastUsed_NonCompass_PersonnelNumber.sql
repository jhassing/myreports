SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_LastUsed_NonCompass_PersonnelNumber]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_LastUsed_NonCompass_PersonnelNumber]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Get_Associate_By_AssocID

Description:     Retrieves the latest non-compass personnel number

Parameters:  	@as_AssocPerNum 			Last non compass personnel number used returned to the client   			

Created By:		
		08/17/2005		John Schiavarelli

Returns:         (0 for Success), (1 for Failure)

Example:
	DECLARE 	@lastPerna 	VARCHAR(8)
	EXECUTE 	usp_Get_LastUsed_NonCompass_PersonnelNumber @lastPerna output
	PRINT		 @lastPerna

----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Get_LastUsed_NonCompass_PersonnelNumber
(
			@as_AssocPerNum       		VARCHAR(8) OUTPUT
) AS

	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)

	SELECT  @as_AssocPerNum = MAX(AssocPerNum) FROM dbo.Associate        

	RETURN 0
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

