SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_OperationCode_By_Region]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_OperationCode_By_Region]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Get_OperationCode_By_Region

Description:     Selects all the Operations Number that a Region manager can view

Parameters:     @as_DM              VARCHAR(10)   	DM which will query for OperationCodes

History:
	8/24/2005	John Schiavarelli

Returns:         (0 for Success), (1 for Failure)
----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Get_OperationCode_By_Region
(
			@as_Region              VARCHAR(10)   
		) AS

	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)

	SELECT
		OperationCode 
	FROM
		zrops	
	WHERE
		Region          = @as_Region        
           
	RETURN 0
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

