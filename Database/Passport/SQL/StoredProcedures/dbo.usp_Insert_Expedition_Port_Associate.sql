SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_Expedition_Port_Associate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_Expedition_Port_Associate]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Insert_Expedition_Port_Associate

Description:     Generated on 8/17/2005 12:01:04 PM by COMPASS-USA\SchiaJ01

Parameters:      @ai_ExpeditonPortAssocID int                  Not Nullable Primary Key Identity      Size:4    Prec:10  Scale:0  
                 @ai_ExpeditionPortID     int                  Not Nullable             Non-Identity  Size:4    Prec:10  Scale:0  
                 @ai_ExpeditionAssocID    int                  Not Nullable             Non-Identity  Size:4    Prec:10  Scale:0  
                 @ai_OperationNumber      int                  Not Nullable             Non-Identity  Size:4    Prec:10  Scale:0  
                 @ad_CompletionDate       datetime             Not Nullable             Non-Identity  Size:8    Prec:23  Scale:3  
                 @ad_CreatedDateTime      datetime             Not Nullable             Non-Identity  Size:8    Prec:23  Scale:3  
                 @as_CreatedBy            varchar              Not Nullable             Non-Identity  Size:75   Prec:0   Scale:0  
                 @ad_ModifiedDateTime     datetime             Not Nullable             Non-Identity  Size:8    Prec:23  Scale:3  
                 @as_ModifiedBy           varchar              Not Nullable             Non-Identity  Size:75   Prec:0   Scale:0  

Created By:
		8/17/2005		John Schiavarelli

Returns:         (0 for Success), (1 for Failure)

Example: usp_Insert_Expedition_Port_Associate '', 1, 2, 6201543, 4562, '10/10/2005', 'schiaj02'
----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Insert_Expedition_Port_Associate(
			@ai_ExpeditonPortAssocID	INT OUTPUT,
			@ai_ExpeditionID		INT,
			@ai_PortNumber		INT,
			@ai_AssocID			INT,
			@ai_OperationNumber      	INT                                 , -- int                (4 bytes)     (not nullable)
			@ad_CompletionDate         	DATETIME                            , -- datetime           (8 bytes)     (not nullable)
			@as_CreatedBy              		VARCHAR(75)                         -- varchar            (75 bytes)    (not nullable)
		) AS

	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)


	DECLARE @expeditionPortID INT

	/*  Get the Expedition Port ID */
	EXECUTE usp_Get_ExpeditionPortID @expeditionPortID output, @ai_ExpeditionID,@ai_PortNumber

	/* Check to see if the Record already exisst */
	DECLARE @ExpeditonPortAssocID INT
	IF (exists (SELECT ExpeditonPortAssocID from Expedition_Port_Associate WHERE (ExpeditionPortID = @expeditionPortID) AND (AssocID = @ai_AssocID) AND (OperationNumber = @ai_OperationNumber) ) )
		BEGIN 
			/* Modify the record, as it already exists */	
			EXECUTE usp_Update_Expedition_Port_Associate @expeditionPortID, @ai_AssocID, @ai_OperationNumber, @ad_CompletionDate, @as_CreatedBy
		END
	ELSE
		BEGIN
			/* Add a new record */
			INSERT INTO Expedition_Port_Associate
			(
				ExpeditionPortID,
				AssocID,
				OperationNumber,
				CompletionDate,
				CreatedDateTime,
				CreatedBy,
				ModifiedDateTime,
				ModifiedBy 
			)
			VALUES
			(
				@expeditionPortID,
				@ai_AssocID,
				@ai_OperationNumber,
				@ad_CompletionDate,
				GetDate(),
				@as_CreatedBy,
				GetDate(),
				@as_CreatedBy 
			)
		END


	IF @@ROWCOUNT = 0
	BEGIN
		SET @ls_ErrorDescription = 'Error Inserting to Table: Expedition_Port_Associate'
		GOTO ErrorHandler
	END

	-- Set the Scope Identity for the primary Key
	SET @ai_ExpeditonPortAssocID = SCOPE_IDENTITY()
	RETURN 0

ErrorHandler:
	RAISERROR(@ls_ErrorDescription, 16, 1)
	RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

