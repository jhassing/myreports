SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_Expedition]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_Expedition]
GO




/*--------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	

Description:	Inserts a record in the expedition table

Parameters:	@as_ExpeditionName
		@as_Description 
		@as_UserName
		
Returns:	
Algorithm:	
Revisions:	08/04/2005	MJH Created

Example:	
		EXECUTE usp_Insert_Expedition 'Test Expedition', 'Test Expedition', 'smithj01'
-------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE   PROCEDURE dbo.usp_Insert_Expedition
	(
		@as_ExpeditionName 	varchar(50),
		@as_Description	varchar(150),
		@as_UserName		varchar(50)
	)
AS

	SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	--Select record by id

	INSERT INTO Expedition (ExpeditionName, Description, CreatedDateTime, CreatedBy, ModifiedDateTime, ModifiedBy)	
	VALUES(@as_ExpeditionName, @as_Description, GETDATE(), @as_UserName, GETDATE(), @as_UserName)


	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error inserting data in to Expedition table.'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

