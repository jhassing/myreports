SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Report_Passport_UnitView]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Report_Passport_UnitView]
GO

/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Report_Passport_UnitView

Description:	Returns a percentage of associates who have completed port of call courses 
		1 through 10 per unit.

Parameters:	@as_CC varchar(5) 	Cost Center

Returns:	

Revisions:	05/31/2006 -  Jennifer L. Dominick
		
Example:	
		EXECUTE dbo.usp_Report_Passport_UnitView '3074'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE         PROCEDURE dbo.usp_Report_Passport_UnitView
	@as_CC varchar(1000)
AS


	SET NOCOUNT ON

	-- Temporary Table
	DECLARE @ltbl_UnitInfo	TABLE ( 
	OperationNumber			int,
	AssocID			varchar(6),
	Assoc_name		varchar(50),
	unitname		varchar(50),
	PC_1			varchar(10),
	PC_2			varchar(10),
	PC_3			varchar(10),
	PC_4			varchar(10),
	PC_5			varchar(10),
	PC_6			varchar(10),
	PC_7			varchar(10),
	PC_8			varchar(10),
	PC_9			varchar(10),
	PC_10			varchar(10),
	assoc_cnt			int,
	poc1_cnt			int,
	poc2_cnt			int,
	poc3_cnt			int,
	poc4_cnt			int,
	poc5_cnt			int,
	poc6_cnt			int,
	poc7_cnt			int,
	poc8_cnt			int,
	poc9_cnt			int,
	poc10_cnt			int)
		

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	DECLARE @li_charpos 			integer
	DECLARE	@li_StartPos	 		integer
	DECLARE	@ls_delimiter 			char(1)
	DECLARE @li_strpos			integer
	DECLARE @ls_CC 				varchar (100)

	DECLARE @ltbl_Unit TABLE (
		CC int
		)
	
	SET @ls_delimiter = '|'
	SET @li_charpos = 1
	SET @li_StartPos = 0
	SET @ls_CC = ''


	--Parse out CC from pipe delimited string and place into temp table

	If RIGHT(@as_CC, 1) <> '|'
		SET @as_CC = @as_CC + @ls_delimiter

		WHILE @li_charpos <=Len (@as_CC)
			BEGIN
				SET @li_charpos = @li_charpos + 1
					IF SUBSTRING(@as_CC, @li_charpos, 1) = @ls_delimiter
				BEGIN
					Set @ls_CC = SUBSTRING(@as_cc, @li_startPos, @li_charpos - @li_StartPos)
					INSERT INTO @ltbl_Unit (CC) VALUES (@ls_CC)
					Set @li_StartPos = @li_charpos + 1
					Set @li_charpos = @li_StartPos 
				END
			END
			
	-- Insert associates who have taken port of call courses
	insert into @ltbl_UnitInfo
	select 
		OperationNumber,
		AssocID,
		Assoc_name,
		unitname,
		(case when min(PC1) is not null then convert(char(10),min(PC1),101) end) as PC_1,
		(case when min(PC2) is not null then convert(char(10),min(PC2),101) end) as PC_2,
		(case when min(PC3) is not null then convert(char(10),min(PC3),101) end) as PC_3,
		(case when min(PC4) is not null then convert(char(10),min(PC4),101) end) as PC_4,
		(case when min(PC5) is not null then convert(char(10),min(PC5),101) end) as PC_5,
		(case when min(PC6) is not null then convert(char(10),min(PC6),101) end) as PC_6,
		(case when min(PC7) is not null then convert(char(10),min(PC7),101) end) as PC_7,
		(case when min(PC8) is not null then convert(char(10),min(PC8),101) end) as PC_8,
		(case when min(PC9) is not null then convert(char(10),min(PC9),101) end) as PC_9,
		(case when min(PC10) is not null then convert(char(10),min(PC10),101) end) as PC_10,
		assoc_cnt=count(distinct assoc_name),
		poc1_cnt=(case when min(pc1) is not null then 1 else 0 end),
		poc2_cnt=(case when min(pc2) is not null then 1 else 0 end),
		poc3_cnt=(case when min(pc3) is not null then 1 else 0 end),
		poc4_cnt=(case when min(pc4) is not null then 1 else 0 end),
		poc5_cnt=(case when min(pc5) is not null then 1 else 0 end),
		poc6_cnt=(case when min(pc6) is not null then 1 else 0 end),
		poc7_cnt=(case when min(pc7) is not null then 1 else 0 end),
		poc8_cnt=(case when min(pc8) is not null then 1 else 0 end),
		poc9_cnt=(case when min(pc9) is not null then 1 else 0 end),
		poc10_cnt=(case when min(pc10) is not null then 1 else 0 end)
		

		from(
		Select distinct
		a.OperationNumber,
		a.AssocId,
		a.AssocFName + ' ' + a.AssocLName as Assoc_Name,
		h.unitname,
		counter=convert(numeric(12,2),(select count(distinct associd) from associate asct where a.operationnumber=asct.operationnumber and asct.activeflag=1)),
		(case when ep.portNumber=1 then ex.Completion_Date end) as PC1,
		(case when ep.PortNumber=2 then ex.Completion_Date end) as PC2,
		(case when ep.PortNumber=3 then ex.Completion_Date end) as PC3,
		(case when ep.PortNumber=4 then ex.Completion_Date end) as PC4,
		(case when ep.PortNumber=5 then ex.Completion_Date end) as PC5,
		(case when ep.PortNumber=6 then ex.Completion_Date end) as PC6,
		(case when ep.PortNumber=7 then ex.Completion_Date end) as PC7,
		(case when ep.PortNumber=8 then ex.Completion_Date end) as PC8,
		(case when ep.PortNumber=9 then ex.Completion_Date end) as PC9,
		(case when ep.PortNumber=10 then ex.Completion_Date end) as PC10

		from dbo.Associate a WITH (NOLOCK) 

		inner join hierarchy h on a.operationnumber=convert(int,h.unit)


		LEFT JOIN
		(SELECT OperationNumber,associd,ExpeditionPortiD, min(CASE WHEN CompletionDate is not null  THEN CompletionDate END) AS Completion_Date 
		FROM dbo.Expedition_Port_Associate WITH (NOLOCK) group by  OperationNumber,associd,ExpeditionPortiD ) ex ON a.OperationNumber = ex.OperationNumber  
		and a.associd=ex.associd 

		left  join dbo.expedition_port ep WITH (NOLOCK) ON ex.ExpeditionPortiD = ep.ExpeditionPortiD 

		where a.ActiveFlag=1  
		and a.OperationNumber in (Select CC from @ltbl_Unit)
		AND isnumeric(h.unit)=1) c

		Group by OperationNumber,AssocID,Assoc_name,unitname,c.counter
		Order by OperationNumber,Assoc_name


		

		insert into @ltbl_UnitInfo
		select 
		OperationNumber,
		AssocID,
		a.AssocFName + ' ' + a.AssocLName as Assoc_Name,
		unitname,
		PC_1=null,
		PC_2=null,
		PC_3=null,
		PC_4=null,
		PC_5=null,
		PC_6=null,
		PC_7=null,
		PC_8=null,
		PC_9=null,
		PC_10=null,
		assoc_cnt=0,
		poc1_cnt=0,
		poc2_cnt=0,
		poc3_cnt=0,
		poc4_cnt=0,
		poc5_cnt=0,
		poc6_cnt=0,
		poc7_cnt=0,
		poc8_cnt=0,
		poc9_cnt=0,
		poc10_cnt=0
		from dbo.associate a with (nolock)

		inner join hierarchy h on a.operationnumber=convert(int,h.unit)


		where a.aCtiveflag =1
		and a.associd not in (select associd from @ltbl_UnitInfo)
		and operationNumber in (Select CC from @ltbl_Unit)
			
		select * from @ltbl_UnitInfo

	

--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Associate table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1












GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

