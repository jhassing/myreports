SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Report_Passport_CompassView]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Report_Passport_CompassView]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Report_Passport_CompassView

Description:	Returns a percentage of associates who have completed port of call courses 
		1 through 10 per unit.

Parameters:	@as_CC varchar(5) 	Cost Center

Returns:	

Revisions:	05/30/2006 -  Jennifer L. Dominick
		
Example:	
		EXECUTE dbo.usp_Report_Passport_CompassView 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE   PROCEDURE dbo.usp_Report_Passport_CompassView
	
AS



SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	DECLARE @li_charpos 			integer
	DECLARE	@li_StartPos	 		integer
	DECLARE	@ls_delimiter 			char(1)
	DECLARE @li_strpos			integer
	DECLARE @ls_CC 				varchar (100)

	DECLARE @ltbl_Unit TABLE (
		CC int
		)
	
	SET @ls_delimiter = '|'
	SET @li_charpos = 1
	SET @li_StartPos = 0
	SET @ls_CC = ''


	

	SELECT
	SECTOR,
	SECTORnAME,
	assoc_cnt=count (distinct associd),
	POC1=SUM(POC1),
	POC2=SUM(POC2),
	POC3=SUM(POC3),
	POC4=SUM(POC4),
	POC5=SUM(POC5),
	POC6=SUM(POC6),
	POC7=SUM(POC7),
	POC8=SUM(POC8),
	POC9=SUM(POC9),
	POC10=SUM(POC10),
	GRAND_TOTAL=SUM(PALL)

	FROM (

	select DISTINCT
	h.sector,
	h.sectorName,
	a.associd,
	--counter=convert(numeric(12,2),()),
	POC1=(Case when ep.PortNumber=1 and epa.CompletionDate is not null then 1 else 0 end),
	POC2=(CASE WHEN EP.PortNumber=2 and epa.CompletionDate is not null then 1 else 0 end),
	POC3=(CASE WHEN ep.PortNumber=3 and epa.CompletionDate is not null then 1 else 0 end),
	POC4=(CASE WHEN ep.PortNumber=4 and epa.CompletionDate is not null then 1 else 0 end),
	POC5=(CASE WHEN ep.PortNumber=5 and epa.CompletionDate is not null then 1 else 0 end),
	POC6=(CASE WHEN ep.PortNumber=6 and epa.CompletionDate is not null then 1 else 0 end), 
	POC7=(CASE WHEN ep.PortNumber=7 and epa.CompletionDate is not null then 1 else 0 end),
	POC8=(CASE WHEN ep.PortNumber=8 and epa.CompletionDate is not null then 1 else 0 end),
	POC9=(CASE WHEN ep.PortNumber=9 and epa.CompletionDate is not null then 1 else 0 end),
	POC10=(CASE WHEN ep.PortNumber=10 and epa.CompletionDate is not null then 1 else 0 end),
	PALL =( CASE WHEN epa.completiondate is not null  then 1 else 0 end)

	from 
	Associate a
	left  join expedition_Port_associate epa on a.associd=epa.associd and a.operationnumber =epa.operationnumber
	left join expedition_port ep on  ep.ExpeditionPortID=epa.ExpeditionPortID
	inner join hierarchy h on a.operationnumber=convert(int,h.unit)
	where a.activeflag=1
	AND isnumeric(h.unit)=1
	--and sector='C00000'
	--group by h.sector,sectorName
	) C
	GROUP BY SECTOR,SECTORnAME

	

--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Associate table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

