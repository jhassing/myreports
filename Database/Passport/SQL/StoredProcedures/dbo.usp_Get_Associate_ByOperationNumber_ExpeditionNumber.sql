SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Associate_ByOperationNumber_ExpeditionNumber]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Associate_ByOperationNumber_ExpeditionNumber]
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_Associate_ByOperationNumber_ExpeditionNumber

Description:	Returns listing of Associates

Parameters:	@ai_OperationNumber              		INT,		Operations Number
		@ai_ExpeditionNumber                          	INT		Expedition Number
)


Returns:	Associates that have been entered into Passport application
Algorithm:	
Revisions:	08/02/2005 	John Schiavarelli	 Created 

Example:	
		EXECUTE usp_Get_Associate_ByOperationNumber_ExpeditionNumber 1192, 1
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE   PROCEDURE dbo.usp_Get_Associate_ByOperationNumber_ExpeditionNumber (			@ai_OperationNumber              		INT,
			@ai_ExpeditionNumber                          	INT
)
AS

	SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	--Select record by id
	SELECT    
		a.AssocID,
		a.AssocPerNum,
		a.OperationNumber,
		a.AssocFName,
		a.AssocLName,
		a.ActiveFlag,
		a.CompassAssocFlag,
		IsNull ( ea.Active, 0 ) as Active
	
	FROM	
			(SELECT	a1.AssocID,
				a1.AssocPerNum,
				a1.OperationNumber,
				a1.AssocFName,
				a1.AssocLName,
				a1.ActiveFlag,
				a1.CompassAssocFlag
			FROM	dbo.Associate a1
			WHERE	a1.OperationNumber = @ai_OperationNumber
				) 
		a
	LEFT OUTER JOIN
			(SELECT	a2.AssocID,
				ea2.ExpeditionID,
				ea2.Active
			FROM	dbo.Expedition_Associate ea2
			RIGHT OUTER JOIN
				dbo.Associate a2 ON ea2.AssocID = a2.AssocID
			WHERE	a2.OperationNumber = @ai_OperationNumber 
				AND ea2.ExpeditionID = @ai_ExpeditionNumber
				)
		ea ON a.AssocID = ea.AssocID

	WHERE ea.Active = 1

	ORDER BY a.AssocLName, a.AssocFName



	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from table Request_Access'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

