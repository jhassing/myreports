SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_Associate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_Associate]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Insert_Associate

History:     Generated on 8/3/2005 8:02:45 PM by COMPASS-USA\SchiaJ01

Description:
	Inserts a new associate into the database

Parameters:      
		@ai_AssocID                             	 INT=NULL OUT              , -- int                  (4 bytes)     (not nullable)
		@ai_OperationNumber                  INT                                	 , -- int                  (4 bytes)     (not nullable)
		@ai_AssocPerNum                       VARCHAR(8)                   , -- varchar               	 (8 bytes)     (not nullable)
		@as_AssocFName                       VARCHAR(20)                	, -- varchar           (20 bytes)    (not nullable)
		@as_AssocLName                       VARCHAR(20)              	, -- varchar           (20 bytes)    (not nullable)
		@ab_ActiveFlag                           BIT                                 	, -- bit                	 (1 bytes)     (not nullable)
		
Returns:         (0 for Success), (1 for Failure)
----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Insert_Associate(
			@ai_AssocID                             	 INT=NULL OUT              , -- int                  (4 bytes)     (not nullable)
			@ai_OperationNumber                  INT                                	 , -- int                  (4 bytes)     (not nullable)
			@ai_AssocPerNum                       VARCHAR(8)                   , -- varchar               	 (8 bytes)     (not nullable)
			@as_AssocFName                       VARCHAR(20)                	, -- varchar           (20 bytes)    (not nullable)
			@as_AssocLName                       VARCHAR(20)              	, -- varchar           (20 bytes)    (not nullable)
			@ab_CompassAssocFlag	BIT,			-- bit                	 (1 bytes)     (not nullable)
			@ab_ActiveFlag                           BIT                                 	 -- bit                	 (1 bytes)     (not nullable)
		) AS

	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)


	INSERT INTO Associate
	(
		OperationNumber,
		AssocPerNum,
		AssocFName,
		AssocLName,
		CompassAssocFlag,
		ActiveFlag
		
	)
	VALUES
	(
		@ai_OperationNumber,
		@ai_AssocPerNum,
		@as_AssocFName,
		@as_AssocLName,
		@ab_CompassAssocFlag,
		@ab_ActiveFlag
		
	)

	IF @@ROWCOUNT = 0
	BEGIN
		SET @ls_ErrorDescription = 'Error Inserting to Table: Associate'
		GOTO ErrorHandler
	END

	-- Set the Scope Identity for the primary Key
	SET @ai_AssocID = SCOPE_IDENTITY()
	RETURN 0

ErrorHandler:
	RAISERROR(@ls_ErrorDescription, 16, 1)
	RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

