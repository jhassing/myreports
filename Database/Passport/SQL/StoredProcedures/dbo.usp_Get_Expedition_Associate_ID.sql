SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Expedition_Associate_ID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Expedition_Associate_ID]
GO

/*--------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	

Description:	Returns ExpeditionPortID primary key

Parameters:	@ai_ExpeditionAssociateID	int output,
		@ai_ExpeditionID 		int,		Expedition ID
		@ai_AssocID			int		Associates PK

Returns:	
Algorithm:	
Revisions:	08/16/2005	John Schiavarelli Created

Example:	
		EXECUTE usp_Get_Expedition_Associate_ID 1
-------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE    PROCEDURE dbo.usp_Get_Expedition_Associate_ID
	(
		@ai_ExpeditionAssociateID	int output,
		@ai_ExpeditionID 		int,
		@ai_AssocID			int
	)
AS

	SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	--Select record by id

	SELECT     @ai_ExpeditionAssociateID = ExpeditionAssocID

	FROM         dbo.Expedition_Associate

	WHERE     (ExpeditionID = @ai_ExpeditionID) AND (AssocID = @ai_AssocID)
	

	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Expedition_Port table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

