SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_OperationCode_By_DM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_OperationCode_By_DM]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Get_OperationCode_By_DM

Description:     Selects all the Operations Number that a DM can view

Parameters:     @as_DM              VARCHAR(10)   	DM which will query for OperationCodes

History:
	8/24/2005	John Schiavarelli

Returns:         (0 for Success), (1 for Failure)
----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Get_OperationCode_By_DM
(
			@as_DM              VARCHAR(10)   
		) AS

	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)

	SELECT
		OperationCode 
	FROM
		zrops	
	WHERE
		DM          = @as_DM        
           
	RETURN 0
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

