SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Associate_ByOperationNumber_ExpeditionNumber_All]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Associate_ByOperationNumber_ExpeditionNumber_All]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_Associate_ByOperationNumber_ExpeditionNumber_All

Description:	Returns listing of Associates

Parameters:	@ai_OperationNumber              		INT,		Operations Number
		@ai_ExpeditionNumber                          	INT,		Expedition Number
		@ab_CompassAssoc				BIT,		Compass Associate
		@ab_NonCompassAssoc				BIT,		Non-Compass Associate
		@ab_Active					BIT,		Active Associate
		@ab_NonActive					BIT		Inactive Associate
)

Returns:	Associates and value to indicate if' they are participating in an expedition
		Active = 1  -- Employee is participating
		Active = 0  -- Employee is not participating	
Revisions:	8/16/2005 TB Created
		9/27/2005 MH Added CompassAssoc, NonCompassAssoc, Active and NonActive parameters. 
			Needed the ability to return any variation of Active, Inactive, CompassAssoc and NonCompass Assoc.

Example:	
		EXECUTE usp_Get_Associate_ByOperationNumber_ExpeditionNumber_All 1002, 1, 1, 1, 1, 1
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE     PROCEDURE dbo.usp_Get_Associate_ByOperationNumber_ExpeditionNumber_All (			@ai_OperationNumber              		INT,
			@ai_ExpeditionNumber                          	INT,
			@ab_CompassAssoc				BIT,
			@ab_NonCompassAssoc				BIT,
			@ab_Active					BIT,
			@ab_NonActive					BIT
)
AS

	SET NOCOUNT ON

	DECLARE @ls_Active		VARCHAR(50)
	DECLARE	@ls_CompassAssoc	VARCHAR(50)

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message
	

	DECLARE @ltbl_Active TABLE (
		Active BIT
		)

	DECLARE @ltbl_CompassAssoc TABLE (
		CompassAssoc BIT
		)
	
	If @ab_Active = 1 and @ab_NonActive = 1 
		Insert into @ltbl_Active Values (1)
		Insert into @ltbl_Active Values (0)	
	
	If @ab_Active = 1 and @ab_NonActive = 0 
		Insert into @ltbl_Active Values (1)
	
	If @ab_Active = 0 and @ab_NonActive = 1 
		Insert into @ltbl_Active Values (0)
	
	IF @ab_CompassAssoc = 1 and @ab_NonCompassAssoc = 1 
		Insert into @ltbl_CompassAssoc Values (1)
		Insert into @ltbl_CompassAssoc Values (0)

	IF @ab_CompassAssoc = 1 and @ab_NonCompassAssoc = 0 
		Insert into @ltbl_CompassAssoc Values (1)
	
	IF @ab_CompassAssoc = 0 and @ab_NonCompassAssoc = 1 
		Insert into @ltbl_CompassAssoc Values (0)
		

	--Select record by id
	SELECT    
		a.AssocID,
		a.AssocPerNum,
		a.OperationNumber,
		a.AssocFName,
		a.AssocLName,
		a.ActiveFlag,
		a.CompassAssocFlag,
		IsNull ( ea.Active, 0 ) as ActiveForExpedition
	
	FROM	
			(SELECT	a1.AssocID,
				a1.AssocPerNum,
				a1.OperationNumber,
				a1.AssocFName,
				a1.AssocLName,
				a1.CompassAssocFlag,
				a1.ActiveFlag
			FROM	dbo.Associate a1
			WHERE	a1.OperationNumber = @ai_OperationNumber AND 	
				a1.ActiveFlag in(SELECT Active FROM @ltbl_Active)	AND
				a1.CompassAssocFlag in(SELECT CompassAssoc FROM @ltbl_CompassAssoc)
				)
		a
	LEFT OUTER JOIN
			(SELECT	a2.AssocID,
				ea2.ExpeditionID,
				ea2.Active
			FROM	dbo.Expedition_Associate ea2
			RIGHT OUTER JOIN
				dbo.Associate a2 ON ea2.AssocID = a2.AssocID
			WHERE	a2.OperationNumber = @ai_OperationNumber 
				AND ea2.ExpeditionID = @ai_ExpeditionNumber
				)
		ea ON a.AssocID = ea.AssocID

	ORDER BY 		a.AssocLName


	



	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from table Request_Access'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

