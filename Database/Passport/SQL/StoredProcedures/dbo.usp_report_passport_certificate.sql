SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Report_Passport_Certificate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Report_Passport_Certificate]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Report_Passport_Certificate

Description:	Returns a percentage of associates who have completed port of call courses 
		1 through 10 per unit.

Parameters:	@as_CC varchar(5) 	Cost Center

Returns:	

Revisions:	05/31/2006 -  Jennifer L. Dominick
		
Example:	
		EXECUTE dbo.usp_Report_Passport_Certificate '356793'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE         PROCEDURE dbo.usp_Report_Passport_Certificate
		@as_CC varchar(1000)
AS


SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	DECLARE @li_charpos 			integer
	DECLARE	@li_StartPos	 		integer
	DECLARE	@ls_delimiter 			char(1)
	DECLARE @li_strpos			integer
	DECLARE @ls_CC 				varchar (100)

	DECLARE @ltbl_Unit TABLE (
		CC int
		)
	
	SET @ls_delimiter = '|'
	SET @li_charpos = 1
	SET @li_StartPos = 0
	SET @ls_CC = ''


	--Parse out CC from pipe delimited string and place into temp table

/*	If RIGHT(@as_CC, 1) <> '|'
		SET @as_CC = @as_CC + @ls_delimiter

		WHILE @li_charpos <=Len (@as_CC)
			BEGIN
				SET @li_charpos = @li_charpos + 1
					IF SUBSTRING(@as_CC, @li_charpos, 1) = @ls_delimiter
				BEGIN
					Set @ls_CC = SUBSTRING(@as_cc, @li_startPos, @li_charpos - @li_StartPos)
					INSERT INTO @ltbl_Unit (CC) VALUES (@ls_CC)
					Set @li_StartPos = @li_charpos + 1
					Set @li_charpos = @li_StartPos 
				END
			END*/
			
	

	select 
		OperationNumber,
		AssocID,
		Assoc_name,
		completiondate=convert(char(10),pc_10,110)
--		completed=sum(poc1_cnt)+sum(poc2_cnt)+sum(poc3_cnt)+sum(poc4_cnt)+sum(poc5_cnt)+sum(poc6_cnt)+sum(poc7_cnt)+sum(poc8_cnt)+sum(poc9_cnt)+sum(poc10_cnt)
	from(

	select 
		OperationNumber,
		AssocID,
		Assoc_name,
		(case when Pc10 is not null then PC10 end) as PC_10
		

		from(
		Select distinct
		a.OperationNumber,
		a.AssocId,
		a.AssocFName + ' ' + a.AssocLName as Assoc_Name,
		h.unitname,
		--counter=convert(numeric(12,2),(select count(distinct associd) from associate asct where a.operationnumber=asct.operationnumber and asct.activeflag=1)),
		(case when ep.PortNumber=10 and ex.Completion_Date is not null then ex.Completion_Date end) as PC10

		from dbo.Associate a WITH (NOLOCK) 

		inner join hierarchy h on a.operationnumber=convert(int,h.unit)


		LEFT JOIN
		(SELECT OperationNumber,associd,ExpeditionPortiD, min(CASE WHEN CompletionDate is not null  THEN CompletionDate END) AS Completion_Date 
		FROM dbo.Expedition_Port_Associate WITH (NOLOCK) group by  OperationNumber,associd,ExpeditionPortiD ) ex ON a.OperationNumber = ex.OperationNumber  
		and a.associd=ex.associd 

		left  join dbo.expedition_port ep WITH (NOLOCK) ON ex.ExpeditionPortiD = ep.ExpeditionPortiD 

		where a.ActiveFlag=1  
		and a.associd=@as_CC 
		AND isnumeric(h.unit)=1 ) as c
		group by OperationNumber,AssocID,Assoc_name,pc10) as d
		where pc_10 is not null
		group by OperationNumber,AssocID,Assoc_Name,pc_10
		--having (sum(poc1_cnt)+sum(poc2_cnt)+sum(poc3_cnt)+sum(poc4_cnt)+sum(poc5_cnt)+sum(poc6_cnt)+sum(poc7_cnt)+sum(poc8_cnt)+sum(poc9_cnt)+sum(poc10_cnt))=7
		--Order by OperationNumber,Assoc_name




	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Associate table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
	--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

