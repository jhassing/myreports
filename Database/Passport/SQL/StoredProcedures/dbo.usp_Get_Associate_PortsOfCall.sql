SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Associate_PortsOfCall]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Associate_PortsOfCall]
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_Associate_PortsOfCall

Description:	Returns listing of Completed Ports of Call
Parameters:	
		@as_AssocPerNum       		Personnel number of the associate we need to find ports of call
		@ai_ExpeditionID		Which expedition are we looking at


Returns:	
Algorithm:	
Revisions:	08/02/2005 	John Schiavarelli	Created
		08/15/2005	Teresa Bradley		Modified to use new tables 

Example:	
		EXECUTE usp_Get_Associate_PortsOfCall 425411,1
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE    PROCEDURE dbo.usp_Get_Associate_PortsOfCall 
(			@ai_AssocID       		int,
			@ai_ExpeditionID		int
)
AS

	SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	--Select record by id
	SELECT	epa.CompletionDate, 
			ep.PortNumber

	FROM		dbo.Expedition_Port_Associate epa 


	INNER JOIN
			dbo.Expedition_Port ep ON epa.ExpeditionPortID = ep.ExpeditionPortID

	WHERE	epa.AssocID = @ai_AssocID
			AND ep.ExpeditionID = @ai_ExpeditionID





	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from table Request_Access'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

