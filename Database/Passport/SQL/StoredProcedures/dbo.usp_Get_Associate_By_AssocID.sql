SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Associate_By_AssocID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Associate_By_AssocID]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Get_Associate_By_AssocID

Description:     Delected an Associate based on primary key

Parameters:     			@ai_AssocID            		INT            , -- int                 (4 bytes)     (not nullable)
			@as_AssocPerNum       		VARCHAR(8) OUTPUT     , -- varchar             (8 bytes)     (not nullable)
			@ai_OperationNumber    	INT   OUTPUT         , -- int                 (4 bytes)     (not nullable)
			@as_AssocFName        		VARCHAR(50) OUTPUT    , -- varchar             (50 bytes)    (not nullable)
			@as_AssocLName         		VARCHAR(50)  OUTPUT  , -- varchar             (50 bytes)    (nullable)
			@ab_CompassAssocFlag 	BIT OUTPUT            , -- bit                 (1 bytes)     (not nullable)
			@ab_ActiveFlag         		BIT OUTPUT            -- bit                 (1 bytes)     (not nullable)

Returns:         (0 for Success), (1 for Failure)
----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Get_Associate_By_AssocID(
			@ai_AssocID            		INT            , -- int                 (4 bytes)     (not nullable)
			@as_AssocPerNum       		VARCHAR(8) OUTPUT     , -- varchar             (8 bytes)     (not nullable)
			@ai_OperationNumber    	INT   OUTPUT         , -- int                 (4 bytes)     (not nullable)
			@as_AssocFName        		VARCHAR(50) OUTPUT    , -- varchar             (50 bytes)    (not nullable)
			@as_AssocLName         		VARCHAR(50)  OUTPUT  , -- varchar             (50 bytes)    (nullable)
			@ab_CompassAssocFlag 	BIT OUTPUT            , -- bit                 (1 bytes)     (not nullable)
			@ab_ActiveFlag         		BIT OUTPUT            -- bit                 (1 bytes)     (not nullable)
		) AS

	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)


	SELECT
		@as_AssocPerNum = AssocPerNum,
		@ai_OperationNumber = OperationNumber,
		@as_AssocFName = AssocFName,
		@as_AssocLName = AssocLName,
		@ab_CompassAssocFlag = CompassAssocFlag,
		@ab_ActiveFlag = ActiveFlag
	FROM Associate

	WHERE
		AssocID             = @ai_AssocID                   

	RETURN 0
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

