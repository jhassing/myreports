SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Report_Passport_DistrictView]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Report_Passport_DistrictView]
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Report_Passport_DistrictView

Description:	Returns a percentage of associates who have completed port of call courses 
		1 through 10 per unit.

Parameters:	@as_CC varchar(5) 	-Region from Passport Hierarchy Table

Returns:	

Revisions:	05/30/2006 -  Jennifer L. Dominick
		
Example:	
		EXECUTE dbo.usp_Report_Passport_DistrictView 'LKN000' - For Actuate Hyper link functionality
		EXECUTE dbo.usp_Report_Passport_DistrictView 'CHA' - To run report from parameter screen.

		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE   PROCEDURE dbo.usp_Report_Passport_DistrictView
	 @as_CC varchar(8000)
AS

SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	DECLARE @li_charpos 			integer
	DECLARE	@li_StartPos	 		integer
	DECLARE	@ls_delimiter 			char(1)
	DECLARE @li_strpos			integer
	DECLARE @ls_CC 				varchar (100)
	DECLARE @valid				int

	DECLARE @ltbl_Unit TABLE (
		CC int
		)
	
	SET @ls_delimiter = '|'
	SET @li_charpos = 1
	SET @li_StartPos = 0
	SET @ls_CC = ''
	SET @valid=LEN(@as_CC)
	
	
	
	--Parse out CC from pipe delimited string and place into temp table
	/*if @valid =1 
	begin

	If RIGHT(@as_CC, 1) <> '|'
		SET @as_CC = @as_CC + @ls_delimiter

		WHILE @li_charpos <=Len (@as_CC)
			BEGIN
				SET @li_charpos = @li_charpos + 1
					IF SUBSTRING(@as_CC, @li_charpos, 1) = @ls_delimiter
				BEGIN
					Set @ls_CC = SUBSTRING(@as_cc, @li_startPos, @li_charpos - @li_StartPos)
					INSERT INTO @ltbl_Unit (CC) VALUES (@ls_CC)
					Set @li_StartPos = @li_charpos + 1
					Set @li_charpos = @li_StartPos 
				END
			END
	end	*/		
	
	if @valid = 3
		select 
		c.OperationNumber,
		UnitName=rtrim(c.unitname),
		SectorName=rTRIM(SectorName),
		RegionName=rtrim(RegionName),
		POC1_CNT=SUM(POC1),
		POC2_CNT=SUM(POC2),
		POC3_CNT=SUM(POC3),
		POC4_CNT=SUM(POC4),
		POC5_CNT=SUM(POC5),
		POC6_CNT=SUM(POC6),
		POC7_CNT=SUM(POC7),
		POC8_CNT=SUM(POC8),
		POC9_CNT=SUM(POC9),
		POC10_CNT=SUM(POC10),
		assoc_cnt=(SELECT COUNT(*) FROM ASSOCIATE ast where ACTIVEFLAG=1 and ast.operationnumber=c.operationnumber),
		GRAND_TOTAL=(sum(poc1)+sum(poc2)+sum(poc3)+sum(poc4)+sum(poc5)+sum(Poc6)+sum(poc7)+sum(poc8)+sum(poc9)+sum(poc10)),
		Region
	from (
		select 
		epa.OperationNumber,
		h.sectorName,
		Region,
		RegionName,
		h.unit,
		h.unitname,
		a.associd,
		POC1=(Case when ep.PortNumber=1 and epa.CompletionDate is not null then 1 else 0 end),
		POC2=(CASE WHEN EP.PortNumber=2 and epa.CompletionDate is not null then 1 else 0 end),
		POC3=(CASE WHEN ep.PortNumber=3 and epa.CompletionDate is not null then 1 else 0 end),
		POC4=(CASE WHEN ep.PortNumber=4 and epa.CompletionDate is not null then 1 else 0 end),
		POC5=(CASE WHEN ep.PortNumber=5 and epa.CompletionDate is not null then 1 else 0 end),
		POC6=(CASE WHEN ep.PortNumber=6 and epa.CompletionDate is not null then 1 else 0 end), 
		POC7=(CASE WHEN ep.PortNumber=7 and epa.CompletionDate is not null then 1 else 0 end),
		POC8=(CASE WHEN ep.PortNumber=8 and epa.CompletionDate is not null then 1 else 0 end),
		POC9=(CASE WHEN ep.PortNumber=9 and epa.CompletionDate is not null then 1 else 0 end),
		POC10=(CASE WHEN ep.PortNumber=10 and epa.CompletionDate is not null then 1 else 0 end)
	
		from 
		Associate a
		inner join expedition_Port_associate epa on a.operationnumber =epa.operationnumber
		inner join expedition_port ep on  ep.ExpeditionPortID=epa.ExpeditionPortID
		inner join hierarchy h on a.operationnumber=convert(int,h.unit)
		where a.activeflag=1
		and a.associd=epa.associd
		AND isnumeric(h.unit)=1
		and substring(h.region,1,3) =@as_CC  
		  ) c
		group by c.operationNumber,c.unitname,c.sectorname,c.regionname,c.region

	else
		select 
		c.OperationNumber,
		UnitName=rtrim(c.unitname),
		SectorName=rTRIM(SectorName),
		RegionName=rtrim(RegionName),
		POC1_CNT=SUM(POC1),
		POC2_CNT=SUM(POC2),
		POC3_CNT=SUM(POC3),
		POC4_CNT=SUM(POC4),
		POC5_CNT=SUM(POC5),
		POC6_CNT=SUM(POC6),
		POC7_CNT=SUM(POC7),
		POC8_CNT=SUM(POC8),
		POC9_CNT=SUM(POC9),
		POC10_CNT=SUM(POC10),
		assoc_cnt=(SELECT COUNT(*) FROM ASSOCIATE ast where ACTIVEFLAG=1 and ast.operationnumber=c.operationnumber),
		GRAND_TOTAL=(sum(poc1)+sum(poc2)+sum(poc3)+sum(poc4)+sum(poc5)+sum(Poc6)+sum(poc7)+sum(poc8)+sum(poc9)+sum(poc10)),
		Region
	
		from (
		
		select 
		epa.OperationNumber,
		h.sectorName,
		Region,
		RegionName,
		h.unit,
		h.unitname,
		a.associd,
		POC1=(Case when ep.PortNumber=1 and epa.CompletionDate is not null then 1 else 0 end),
		POC2=(CASE WHEN EP.PortNumber=2 and epa.CompletionDate is not null then 1 else 0 end),
		POC3=(CASE WHEN ep.PortNumber=3 and epa.CompletionDate is not null then 1 else 0 end),
		POC4=(CASE WHEN ep.PortNumber=4 and epa.CompletionDate is not null then 1 else 0 end),
		POC5=(CASE WHEN ep.PortNumber=5 and epa.CompletionDate is not null then 1 else 0 end),
		POC6=(CASE WHEN ep.PortNumber=6 and epa.CompletionDate is not null then 1 else 0 end), 
		POC7=(CASE WHEN ep.PortNumber=7 and epa.CompletionDate is not null then 1 else 0 end),
		POC8=(CASE WHEN ep.PortNumber=8 and epa.CompletionDate is not null then 1 else 0 end),
		POC9=(CASE WHEN ep.PortNumber=9 and epa.CompletionDate is not null then 1 else 0 end),
		POC10=(CASE WHEN ep.PortNumber=10 and epa.CompletionDate is not null then 1 else 0 end)

		from 
		Associate a
		inner join expedition_Port_associate epa on a.operationnumber =epa.operationnumber
		inner join expedition_port ep on  ep.ExpeditionPortID=epa.ExpeditionPortID
		inner join hierarchy h on a.operationnumber=convert(int,h.unit)
		where a.activeflag=1
		and a.associd=epa.associd
		AND isnumeric(h.unit)=1
		and h.region =@as_CC 
		  ) c
		group by c.operationNumber,c.unitname,c.sectorname,c.regionname,c.region

	

--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Associate table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1















GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

