SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Expedition]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Expedition]
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	

Description:	Returns all expeditions
Parameters:	

Returns:	
Algorithm:	
Revisions:	08/02/2005 TB Created 

Example:	
		EXECUTE usp_Get_Expedition 
-------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE  PROCEDURE dbo.usp_Get_Expedition
AS

	SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	--Select record by id
	SELECT
		e.ExpeditionID, e.ExpeditionName, e.Description
	FROM	dbo.Expedition e 



	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from table Expedition and Expedition_Port'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

