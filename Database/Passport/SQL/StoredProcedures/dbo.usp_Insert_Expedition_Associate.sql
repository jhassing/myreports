SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_Expedition_Associate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_Expedition_Associate]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Insert_Expedition_Associate

Description:     Generated on 8/17/2005 4:46:44 PM by COMPASS-USA\SchiaJ01

Parameters:      @ai_ExpeditionAssocID int                  Not Nullable Primary Key Identity      Size:4    Prec:10  Scale:0  
                 @ai_ExpeditionID      int                  Not Nullable             Non-Identity  Size:4    Prec:10  Scale:0  
                 @ai_AssocID           int                  Not Nullable             Non-Identity  Size:4    Prec:10  Scale:0  
                 @ab_Active            bit                  Not Nullable             Non-Identity  Size:1    Prec:1   Scale:0  

Returns:         (0 for Success), (1 for Failure)
----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Insert_Expedition_Associate(
			@ai_ExpeditionAssocID   INT=NULL OUT                        , -- int                (4 bytes)     (not nullable)
			@ai_AssocID             INT                                 , -- int                (4 bytes)     (not nullable)
			@ai_ExpeditionID        INT                                 , -- int                (4 bytes)     (not nullable)
			@ab_Active              BIT                                   -- bit                (1 bytes)     (not nullable)
		) AS

	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)



	INSERT INTO Expedition_Associate
	(
		ExpeditionID,
		AssocID,
		Active 
	)
	VALUES
	(
		@ai_ExpeditionID,
		@ai_AssocID,
		@ab_Active 
	)

	IF @@ROWCOUNT = 0
	BEGIN
		SET @ls_ErrorDescription = 'Error Inserting to Table: Expedition_Associate'
		GOTO ErrorHandler
	END

	-- Set the Scope Identity for the primary Key
	SET @ai_ExpeditionAssocID = SCOPE_IDENTITY()
	RETURN 0

ErrorHandler:
	RAISERROR(@ls_ErrorDescription, 16, 1)
	RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

