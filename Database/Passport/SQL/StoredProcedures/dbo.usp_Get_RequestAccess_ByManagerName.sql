SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_RequestAccess_ByManagerName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_RequestAccess_ByManagerName]
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	

Description:	Returns a request for access
Parameters:	@as_ManagerLogin VARCHAR(15)

Returns:	Operation Numbers where manager has approved access
Algorithm:	
Revisions:	08/02/2005 TB Created 

Example:	
		EXECUTE usp_Get_RequestAccess_ByManagerName 'WardC01'
-------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE   PROCEDURE dbo.usp_Get_RequestAccess_ByManagerName(			@as_ManagerLogin VARCHAR(15))
AS

	SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	--Select record by id
	SELECT
		ra.OperationNumber
	FROM	dbo.Request_Access ra

	WHERE	ra.ManagerLogin = @as_ManagerLogin
		AND ra.RequestStatusID = 2 --Approved


	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from table Request_Access'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

