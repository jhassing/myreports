SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Rpt_Completed_Assoc_Courses]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Rpt_Completed_Assoc_Courses]
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Rpt_Completed_Assoc_Courses

Description:	Returns list of associates and the dates they completed Port of Call 
		Courses 1 through 10

Parameters:	@as_CC varchar(5) 	Cost Center

Returns:	

Revisions:	12/23/2005 -   JLD
		
Example:	
		EXECUTE dbo.usp_Rpt_Completed_Assoc_Courses '11351|12827|11350'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
ALTER     PROCEDURE dbo.usp_Rpt_Completed_Assoc_Courses
	@as_CC varchar(1000)
AS

SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	DECLARE @li_charpos 			integer
	DECLARE	@li_StartPos	 		integer
	DECLARE	@ls_delimiter 			char(1)
	DECLARE @li_strpos			integer
	DECLARE @ls_CC 				varchar (100)

	DECLARE @ltbl_Unit TABLE (
		CC int
		)
	
	SET @ls_delimiter = '|'
	SET @li_charpos = 1
	SET @li_StartPos = 0
	SET @ls_CC = ''


	--Parse out CC from pipe delimited string and place into temp table

	If RIGHT(@as_CC, 1) <> '|'
		SET @as_CC = @as_CC + @ls_delimiter

		WHILE @li_charpos <=Len (@as_CC)
			BEGIN
				SET @li_charpos = @li_charpos + 1
					IF SUBSTRING(@as_CC, @li_charpos, 1) = @ls_delimiter
				BEGIN
					Set @ls_CC = SUBSTRING(@as_cc, @li_startPos, @li_charpos - @li_StartPos)
					INSERT INTO @ltbl_Unit (CC) VALUES (@ls_CC)
					Set @li_StartPos = @li_charpos + 1
					Set @li_charpos = @li_StartPos 
				END
			END
			
	select 
	OperationNumber,
	AssocID,
	Assoc_name,
	(case when min(PC1) is not null then min(PC1) end) as PC_1,
	(case when min(PC2) is not null then min(PC2) end) as PC_2,
	(case when min(PC3) is not null then min(PC3) end) as PC_3,
	(case when min(PC4) is not null then min(PC4) end) as PC_4,
	(case when min(PC5) is not null then min(PC5) end) as PC_5,
	(case when min(PC6) is not null then min(PC6) end) as PC_6,
	(case when min(PC7) is not null then min(PC7) end) as PC_7,
	(case when min(PC8) is not null then min(PC8) end) as PC_8,
	(case when min(PC9) is not null then min(PC9) end) as PC_9,
	(case when min(PC10) is not null then min(PC10) end) as PC_10,
	(case when min(PC1) is not null then count(distinct Associd) end) as pc1_cnt,
	(case when min(PC2) is not null then count(distinct Associd) end) as pc2_cnt,
	(case when min(PC3) is not null then count(distinct Associd) end) as pc3_cnt,
	(case when min(PC4) is not null then count(distinct Associd) end) as pc4_cnt,
	(case when min(PC5) is not null then count(distinct Associd) end) as pc5_cnt,
	(case when min(PC6) is not null then count(distinct Associd) end) as pc6_cnt,
	(case when min(PC7) is not null then count(distinct Associd) end) as pc7_cnt,
	(case when min(PC8) is not null then count(distinct Associd) end) as pc8_cnt,
	(case when min(PC9) is not null then count(distinct Associd) end) as pc9_cnt,
	(case when min(PC10) is not null then count(distinct Associd) end) as pc10_cnt,
	Assoc_Tot_Cnt

	from(
		Select distinct
		a.OperationNumber,
		a.AssocId,
		Assoc_Tot_Cnt =(select count(*) from dbo.Associate ac WITH (NOLOCK) where activeflag=1 and ac.OperationNumber=a.OperationNumber),
		a.AssocFName + ' ' + a.AssocLName as Assoc_Name,
		(case when ep.portNumber=1 then ex.Completion_Date end) as PC1,
		(case when ep.PortNumber=2 then ex.Completion_Date end) as PC2,
		(case when ep.PortNumber=3 then ex.Completion_Date end) as PC3,
		(case when ep.PortNumber=4 then ex.Completion_Date end) as PC4,
		(case when ep.PortNumber=5 then ex.Completion_Date end) as PC5,
		(case when ep.PortNumber=6 then ex.Completion_Date end) as PC6,
		(case when ep.PortNumber=7 then ex.Completion_Date end) as PC7,
		(case when ep.PortNumber=8 then ex.Completion_Date end) as PC8,
		(case when ep.PortNumber=9 then ex.Completion_Date end) as PC9,
		(case when ep.PortNumber=10 then ex.Completion_Date end) as PC10


		from dbo.Associate a WITH (NOLOCK) 

		LEFT JOIN
		(SELECT OperationNumber,associd,ExpeditionPortiD, min(CASE WHEN CompletionDate is not null  THEN CompletionDate END) AS Completion_Date 
		FROM dbo.Expedition_Port_Associate WITH (NOLOCK) group by  OperationNumber,associd,ExpeditionPortiD ) ex ON a.OperationNumber = ex.OperationNumber  
		and a.associd=ex.associd 

		left  join dbo.expedition_port ep WITH (NOLOCK) ON ex.ExpeditionPortiD = ep.ExpeditionPortiD 

		where a.ActiveFlag=1  
		and a.OperationNumber in (Select CC from @ltbl_Unit) ) as c
		Group by OperationNumber,AssocID,Assoc_name,Assoc_Tot_Cnt
		Order by OperationNumber,Assoc_name,Assoc_Tot_Cnt

	

--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Associate table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

