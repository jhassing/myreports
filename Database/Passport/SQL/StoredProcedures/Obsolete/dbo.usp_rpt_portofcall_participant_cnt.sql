SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Rpt_PortofCall_Participant_Cnt]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Rpt_PortofCall_Participant_Cnt]
GO





/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Rpt_PortofCall_Participant_Cnt

Description:	Returns a count of associates throughout compass who have completed Port of Call Course 1 - 10
		on a monthly basis

Parameters:	@Year  integer -    Year - (generate report for specific year

Returns:	

Revisions:	12/22/2005 -   JLD
		
Example:	
		EXECUTE usp_Rpt_PortofCall_Participant_Cnt 2005
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
create      PROCEDURE dbo.usp_Rpt_PortofCall_Participant_Cnt
	@as_year integer
AS

SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	DECLARE @ls_year		integer  	-- Capture the year to select data for.
	set @ls_year = 0



	-- Select year to generate data for 	
	SET @ls_year=@as_year

	
			
	Select 
	'POC '+ltrim(str(PortNumber)+ ' - ' + PortName) as Port_Name,
	sum(Jan_cnt) as jan_cnt,
	sum(Feb_cnt) as feb_cnt, 
	sum(Mar_cnt) as mar_cnt,
	sum(Apr_cnt) as apr_cnt,
	sum(May_cnt) as may_cnt,
	sum(Jun_cnt) as jun_cnt, 
	sum(July_cnt) as july_cnt,
	sum(Aug_cnt) as aug_cnt,
	sum(Sep_cnt) as sep_cnt,
	sum(Oct_cnt) as oct_cnt, 
	sum(Nov_cnt) as nov_cnt,
	sum(Dec_cnt) as dec_cnt 
	from (  
	Select 
		ep.PortName,
		ep.Portnumber,
		(case when month(ex.CompletionDate)=1 then 1 else 0 end) as jan_cnt, 
		(case when month(ex.CompletionDate)=2 then 1 else 0 end) as Feb_cnt,
		(case when month(ex.CompletionDate)=3 then 1 else 0 end) as Mar_cnt, 
		(case when month(ex.CompletionDate)=4 then 1 else 0 end) as Apr_cnt,
		(case when month(ex.CompletionDate)=5 then 1 else 0 end) as May_cnt, 
		(case when month(ex.CompletionDate)=6 then 1 else 0 end) as Jun_cnt,
		(case when month(ex.CompletionDate)=7 then 1 else 0 end) as July_cnt, 
		(case when month(ex.CompletionDate)=8 then 1 else 0 end) as Aug_cnt,
		(case when month(ex.CompletionDate)=9 then 1 else 0 end) as Sep_cnt, 
		(case when month(ex.CompletionDate)=10 then 1 else 0 end) as Oct_cnt,
		(case when month(ex.CompletionDate)=11 then 1 else 0 end) as Nov_cnt, 
		(case when month(ex.Completiondate)=12 then 1 else 0 end) as Dec_cnt  
		from dbo.Associate a WITH (NOLOCK)  RIGHT OUTER JOIN  dbo.Expedition_Port_Associate ex  
		WITH (NOLOCK) ON a.OperationNumber = ex.OperationNumber   
		and a.associd=ex.associd  RIGHT OUTER join dbo.expedition_port ep WITH (NOLOCK) ON ex.ExpeditionPortiD = ep.ExpeditionPortiD  
		and a.ActiveFlag=1  
		and year(ex.CompletionDate)=@ls_year ) as c  
		group by PortNumber,PortName
		Order by PortNumber
	

	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Associate table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
	--Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

