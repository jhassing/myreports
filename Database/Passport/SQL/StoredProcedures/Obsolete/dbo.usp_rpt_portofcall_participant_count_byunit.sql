SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Rpt_PortofCall_Participant_Count_byUnit]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Rpt_PortofCall_Participant_Count_byUnit]
GO







/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Rpt_PortofCall_Participant_Count_byUnit

Description:	Returns of count of Port of Call course taken for active associates from period
		January through December

Parameters:	@as_CC varchar(5) 	Cost Center

Returns:	

Revisions:	12/27/2005 -   JLD
		
Example:	
		EXECUTE dbo.usp_Rpt_PortofCall_Participant_Count_byUnit '12827|11351'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
create  PROCEDURE dbo.usp_Rpt_PortofCall_Participant_Count_byUnit
	@as_CC varchar(1000)
AS

SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	DECLARE @li_charpos 			integer
	DECLARE	@li_StartPos	 		integer
	DECLARE	@ls_delimiter 			char(1)
	DECLARE @li_strpos			integer
	DECLARE @ls_CC 				varchar (100)

	DECLARE @ltbl_Unit TABLE (
		CC int
		)
	
	SET @ls_delimiter = '|'
	SET @li_charpos = 1
	SET @li_StartPos = 0
	SET @ls_CC = ''


	--Parse out CC from pipe delimited string and place into temp table

	If RIGHT(@as_CC, 1) <> '|'
		SET @as_CC = @as_CC + @ls_delimiter

		WHILE @li_charpos <=Len (@as_CC)
			BEGIN
				SET @li_charpos = @li_charpos + 1
					IF SUBSTRING(@as_CC, @li_charpos, 1) = @ls_delimiter
				BEGIN
					Set @ls_CC = SUBSTRING(@as_cc, @li_startPos, @li_charpos - @li_StartPos)
					INSERT INTO @ltbl_Unit (CC) VALUES (@ls_CC)
					Set @li_StartPos = @li_charpos + 1
					Set @li_charpos = @li_StartPos 
				END
			END
			
	

	select 	OperationNumber,
	       	PortNumber,
	       	PortName,
		sum(jan) as jan_cnt,
		sum(feb) as feb_cnt,
		sum(mar) as mar_cnt,
		sum(apr) as apr_cnt,
		sum(may) as may_cnt,
		sum(jun) as jun_cnt,
		sum(jul) as jul_cnt,
		sum(aug) as aug_cnt,
		sum(sep) as sep_cnt,
		sum(oct) as oct_nct,
		sum(nov) as nov_cnt,
		sum(dec) as dec_cnt

	from (
		Select 
		epa.OperationNumber,
		ep.PortNumber,
		ep.PortName,
		(case when  month(epa.CompletionDate)=1 then 1 else 0 end) as Jan,
		(case when  month(epa.CompletionDate)=2 then 1 else 0 end) as Feb,
		(case when month(epa.CompletionDate)=3 then 1 else 0 end) as Mar,
		(case when month(epa.CompletionDate)=4 then 1 else 0 end) as Apr,
		(case when  month(epa.CompletionDate)=5 then 1 else 0 end) as May,
		(case when  month(epa.CompletionDate)=6 then 1 else 0 end) as Jun,
		(case when month(epa.CompletionDate)=7 then 1 else 0 end) as Jul,
		(case when month(epa.CompletionDate)=8 then 1 else 0 end) as Aug,
		(case when month(epa.CompletionDate)=9 then 1 else 0 end) as Sep,
		(case when month(epa.CompletionDate)=10 then 1 else 0 end) as Oct,
		(case when month(epa.CompletionDate)=11 then 1 else 0 end) as Nov,
		(case when month(epa.CompletionDate)=12 then 1 else 0 end) as dec

		from dbo.Expedition_Port  ep WITH (NOLOCK) 

		inner join dbo.Expedition_Port_Associate epa WITH (NOLOCK) ON ep.ExpeditionPortID=epa.ExpeditionPortID
		
		inner join dbo.Associate a with (NOLOCK) ON epa.AssocId =a.AssocID
		where epa.OperationNumber in (Select CC from @ltbl_Unit) 
		and a.ActiveFlag=1
	union all
		select 
		eps.OperationNumber,
		e.PortNumber,
		e.PortName,
		0 as Jan,
		0 as Feb,
		0 as Mar,
		0 as Apr,
		0 as May,
		0 as Jun,
		0 as Jul,
		0 as Aug,
		0 as Sep,
		0 as Oct,
		0 as Nov,
		0 as dec

		from dbo.Expedition_Port E WITH (NOLOCK) 
	 	CROSS join dbo.Expedition_Port_Associate eps WITH (NOLOCK) 
		where e.portNumber not in (Select 
			ep.PortNumber
			from dbo.Expedition_Port  ep WITH (NOLOCK) 
			inner join dbo.Expedition_Port_Associate epa WITH (NOLOCK) ON ep.ExpeditionPortID=epa.ExpeditionPortID where eps.OperationNumber=epa.OperationNumber )
			) as c
		where c.OperationNumber in (Select CC from @ltbl_Unit) 
		group by OperationNumber,PortNumber,PortName
		Order by OperationNumber,PortNumber


	

--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Associate table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

