SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Rpt_PortofCall_Compl_Percent_CompassGrp]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Rpt_PortofCall_Compl_Percent_CompassGrp]
GO







/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Rpt_PortofCall_Compl_Percent_CompassGrp

Description:	Returns a count for Port of Call couse 1 -10 by Year and percentage of all
		associates for a particular year. Divided by Total count per cource/all associates.

Parameters:	@Year  integer -    Year - (generate report for specific year

Returns:	

Revisions:	12/28/2005 -   JLD
		
Example:	
		EXECUTE usp_Rpt_PortofCall_Compl_Percent_CompassGrp 2005
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
create       PROCEDURE dbo.usp_Rpt_PortofCall_Compl_Percent_CompassGrp
	@as_year integer
AS

SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	DECLARE @ls_year		integer  	-- Capture the year to select data for.
	set @ls_year = 0



	-- Select year to generate data for 	
	SET @ls_year=@as_year

	
			
	select
	'POC '+ltrim(str(PortNumber)+ ' - ' + PortName) as Port_Name,
	sum(tot_count) as tot_count,
	max(Assoc_tot_cnt) as Assoc_Total_Count

		from (
			Select distinct
			epa.OperationNumber,
			ep.PortNumber,
			ep.PortName,
			count(PortNumber) as tot_count,
			Assoc_Tot_Cnt =(select count(*) from dbo.Associate a WITH (NOLOCK) where activeflag=1)
		from dbo.Expedition_Port  ep WITH (NOLOCK) 

		inner join dbo.Expedition_Port_Associate epa WITH (NOLOCK) ON ep.ExpeditionPortID=epa.ExpeditionPortID

		inner join dbo.Associate a with (NOLOCK) ON epa.AssocId =a.AssocID

		where year(epa.CompletionDate)=@ls_year
		and a.ActiveFlag=1
		group by epa.OperationNumber,ep.PortNumber,ep.PortName

	union all
		select distinct
		eps.OperationNumber,
		e.PortNumber,
		e.PortName,
		0 as tot_count,
		Assoc_Tot_Cnt=0
		from dbo.Expedition_Port E WITH (NOLOCK) 
 		CROSS join dbo.Expedition_Port_Associate eps WITH (NOLOCK) 
		where e.portNumber not in (Select ep.PortNumber
					from dbo.Expedition_Port  ep WITH (NOLOCK) 
					inner join dbo.Expedition_Port_Associate epa WITH (NOLOCK) 
					ON ep.ExpeditionPortID=epa.ExpeditionPortID 
					where year(completionDate)=@ls_year)

		and year(eps.Completiondate)=@ls_year) as c
	group by PortNumber,PortName
	Order by PortNumber,PortName
	

	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Associate table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
	--Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

