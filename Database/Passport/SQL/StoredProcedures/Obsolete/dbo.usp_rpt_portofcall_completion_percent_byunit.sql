SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Rpt_PortofCall_Completion_Percent_byUnit]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Rpt_PortofCall_Completion_Percent_byUnit]
GO








/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Rpt_PortofCall_Completion_Percent_byUnit

Description:	Returns of  percentage of Port of Call course taken for active associates by Unit.

Parameters:	@as_CC varchar(5) 	Cost Center

Returns:	

Revisions:	12/28/2005 -   JLD
		
Example:	
		EXECUTE dbo.usp_Rpt_PortofCall_Completion_Percent_byUnit '12827|11351'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
create   PROCEDURE dbo.usp_Rpt_PortofCall_Completion_Percent_byUnit
	@as_CC varchar(1000)
AS

	SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	DECLARE @li_charpos 			integer
	DECLARE	@li_StartPos	 		integer
	DECLARE	@ls_delimiter 			char(1)
	DECLARE @li_strpos			integer
	DECLARE @ls_CC 				varchar (100)

	DECLARE @ltbl_Unit TABLE (
		CC int
		)
	
	SET @ls_delimiter = '|'
	SET @li_charpos = 1
	SET @li_StartPos = 0
	SET @ls_CC = ''


	--Parse out CC from pipe delimited string and place into temp table

	If RIGHT(@as_CC, 1) <> '|'
		SET @as_CC = @as_CC + @ls_delimiter

		WHILE @li_charpos <=Len (@as_CC)
			BEGIN
				SET @li_charpos = @li_charpos + 1
					IF SUBSTRING(@as_CC, @li_charpos, 1) = @ls_delimiter
				BEGIN
					Set @ls_CC = SUBSTRING(@as_cc, @li_startPos, @li_charpos - @li_StartPos)
					INSERT INTO @ltbl_Unit (CC) VALUES (@ls_CC)
					Set @li_StartPos = @li_charpos + 1
					Set @li_charpos = @li_StartPos 
				END
			END
			
	

	select
	OperationNumber,
	PortNumber,
	PortName,
	max(tot_count) as total_cnt,
	max(Assoc_Tot_cnt) as Assoc_Total_Cnt

	from (
		Select distinct
		epa.OperationNumber,
		ep.PortNumber,
		ep.PortName,
		count(PortNumber) as tot_count,
		Assoc_Tot_Cnt =(select count(*) from dbo.Associate a WITH (NOLOCK) where activeflag=1 and a.OperationNumber=epa.OperationNumber)

		from dbo.Expedition_Port  ep WITH (NOLOCK) 

			inner join dbo.Expedition_Port_Associate epa WITH (NOLOCK) ON ep.ExpeditionPortID=epa.ExpeditionPortID

			inner join dbo.Associate a with (NOLOCK) ON epa.AssocId =a.AssocID

		where epa.OperationNumber in (Select CC from @ltbl_Unit) 
		and a.ActiveFlag=1
		group by epa.OperationNumber,ep.PortNumber,ep.PortName
	union all
		select 	eps.OperationNumber,
		e.PortNumber,
		e.PortName,
		0 as tot_count,
		Assoc_Tot_Cnt=0
		from dbo.Expedition_Port E WITH (NOLOCK) 
 		CROSS join dbo.Expedition_Port_Associate eps WITH (NOLOCK) 
		where e.portNumber not in (Select 
		ep.PortNumber
			from dbo.Expedition_Port  ep WITH (NOLOCK) 

				inner join dbo.Expedition_Port_Associate epa WITH (NOLOCK) 

				ON ep.ExpeditionPortID=epa.ExpeditionPortID 
			where eps.OperationNumber=epa.OperationNumber)) as c
			where OperationNumber in (Select CC from @ltbl_Unit) 
		group by OperationNumber,PortNumber,PortName
		Order by OperationNumber,PortNumber,PortName
				

	

--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Associate table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1






GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

