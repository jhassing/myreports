SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Delete_Expedition_Port_Associate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Delete_Expedition_Port_Associate]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Delete_Expedition_Port_Associate

Description:     Generated on 8/17/2005 12:09:30 PM by COMPASS-USA\SchiaJ01

Parameters:      @ai_ExpeditonPortAssocID int                  Not Nullable Primary Key Identity      Size:4    Prec:10  Scale:0  
                 @ai_ExpeditionPortID     int                  Not Nullable             Non-Identity  Size:4    Prec:10  Scale:0  
                 @ai_ExpeditionAssocID    int                  Not Nullable             Non-Identity  Size:4    Prec:10  Scale:0  
                 @ai_OperationNumber      int                  Not Nullable             Non-Identity  Size:4    Prec:10  Scale:0  
                 @ad_CompletionDate       datetime             Not Nullable             Non-Identity  Size:8    Prec:23  Scale:3  
                 @ad_CreatedDateTime      datetime             Not Nullable             Non-Identity  Size:8    Prec:23  Scale:3  
                 @as_CreatedBy            varchar              Not Nullable             Non-Identity  Size:75   Prec:0   Scale:0  
                 @ad_ModifiedDateTime     datetime             Not Nullable             Non-Identity  Size:8    Prec:23  Scale:3  
                 @as_ModifiedBy           varchar              Not Nullable             Non-Identity  Size:75   Prec:0   Scale:0  

Created By:
		8/17/2005	John Schiavarelli

Returns:         (0 for Success), (1 for Failure)
----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Delete_Expedition_Port_Associate(
			@ai_AssocID			INT,
			@ai_ExpeditionID	 	INT,
			@ai_OperationNumber		INT,
			@ai_PortNumber		INT

			


)
AS 
	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)


	DECLARE @expeditionPortID INT
	DECLARE @expeditionAssocID INT

	/*  Get the Expedition Port ID */
	EXECUTE usp_Get_ExpeditionPortID @expeditionPortID output, @ai_ExpeditionID,@ai_PortNumber

	DELETE Expedition_Port_Associate WHERE (ExpeditionPortID = @expeditionPortID) AND (AssocID = @ai_AssocID) AND (OperationNumber = @ai_OperationNumber)

	IF @@ROWCOUNT = 0
	BEGIN
		SET @ls_ErrorDescription = 'Error Deleting from Table Expedition_Port_Associate with ' + 
			'@expeditionPortID = ''' + CONVERT(VARCHAR(10),@expeditionPortID) + '''' 
		GOTO ErrorHandler
	END
	RETURN 0

ErrorHandler:
	RAISERROR(@ls_ErrorDescription, 16, 1)
	RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

