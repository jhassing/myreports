SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_Associate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_Associate]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Update_Associate

Description:     Generated on 8/3/2005 8:02:45 PM by COMPASS-USA\SchiaJ01

Parameters:      @ai_AssocID                   int                 Not Nullable  Primary Key   Identity      
                 @ai_OperationNumber           int                 Not Nullable                Non-Identity  
                 @ai_AssocPerNum               int                 Not Nullable                Non-Identity  
                 @as_AssocFName                varchar             Not Nullable                Non-Identity  
                 @as_AssocLName                varchar             Not Nullable                Non-Identity  
                 @ab_ActiveFlag                bit                 Not Nullable                Non-Identity  
                 @ad_CreatedDateTime           datetime            Not Nullable                Non-Identity  
                 @as_CreatedBy                 varchar             Not Nullable                Non-Identity  
                 @ad_ModifiedDateTime          datetime            Not Nullable                Non-Identity  
           
Returns:         (0 for Success), (1 for Failure)
----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Update_Associate(
			@ai_AssocID                             INT            , -- int                 (4 bytes)     (not nullable)
			@ai_OperationNumber                     INT            , -- int                 (4 bytes)     (not nullable)
			@as_AssocFName                          VARCHAR(20)    , -- varchar             (20 bytes)    (not nullable)
			@as_AssocLName                          VARCHAR(20)     -- varchar             (20 bytes)    (not nullable)
		) AS

	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)


	UPDATE Associate
	SET
		OperationNumber                    = @ai_OperationNumber,
		AssocFName                         = @as_AssocFName,
		AssocLName                         = @as_AssocLName

	WHERE
		AssocID                            = @ai_AssocID                   
	IF @@ROWCOUNT = 0
	BEGIN
		SET @ls_ErrorDescription = 'Error Deleting from Table Associate with ' + 
			'@ai_AssocID = ''' + CONVERT(VARCHAR(10),@ai_AssocID) + '''' 
		GOTO ErrorHandler
	END
	RETURN 0

ErrorHandler:
	RAISERROR(@ls_ErrorDescription, 16, 1)
	RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

