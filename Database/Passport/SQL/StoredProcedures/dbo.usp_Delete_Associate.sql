SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Delete_Associate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Delete_Associate]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Delete_Associate

History:     Generated on 8/3/2005 8:02:45 PM by COMPASS-USA\SchiaJ01

Description: Deletes an associate from the database

Parameters:  	@ai_AssocID	Primary key for the associate    

Returns:         (0 for Success), (1 for Failure)
----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Delete_Associate(
			@ai_AssocID                         INT )
AS 
	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)

	DELETE FROM Associate 
	WHERE 
		AssocID                       = @ai_AssocID                   
	IF @@ROWCOUNT = 0
	BEGIN
		SET @ls_ErrorDescription = 'Error Deleting from Table Associate with ' + 
			'@ai_AssocID = ''' + CONVERT(VARCHAR(10),@ai_AssocID) + '''' 
		GOTO ErrorHandler
	END
	RETURN 0

ErrorHandler:
	RAISERROR(@ls_ErrorDescription, 16, 1)
	RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

