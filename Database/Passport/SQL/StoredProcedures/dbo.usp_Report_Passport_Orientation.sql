SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

USE [Passport]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Report_Passport_Orientation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Report_Passport_Orientation]
GO

/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Report_Passport_Orientation

Description:	Returns of count of Port of Call course taken by Sector from period
		January through December

Returns:	

Revisions:	06/01/2006 -   JLD
		
Example:	
		EXECUTE dbo.usp_Report_Passport_Orientation
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE       PROCEDURE dbo.usp_Report_Passport_Orientation
	
AS
SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	DECLARE @li_charpos 			integer
	DECLARE	@li_StartPos	 		integer
	DECLARE	@ls_delimiter 			char(1)
	DECLARE @li_strpos			integer
	DECLARE @ls_CC 				varchar (100)
	DECLARE @CURRENT_YEAR			INT
	DECLARE @CURRENT_MONTH			INT
	DECLARE @PREVIOUS_YEAR			INT
	DECLARE @CUR_DATE			varchar(12)
	DECLARE @PREV_DATE			varchar(12)
	DECLARE @PREV_YEAR			INT
	DECLARE @CRITERIA_DATE 			varchar(12)
	DECLARE @JAN_YEAR			INT

	DECLARE @ltbl_Unit TABLE (
		CC int
		)
	
	SET @ls_delimiter = '|'
	SET @li_charpos = 1
	SET @li_StartPos = 0
	SET @ls_CC = ''
	SET @CURRENT_YEAR =YEAR(GETDATE())
	SET @CURRENT_MONTH =MONTH(GETDATE())
	SET @PREVIOUS_YEAR=YEAR(GETDATE())-1
	--SET @TODAYS_DATE =DATE(GETDATE())
	SET @PREV_DATE = CONVERT(VARCHAR(12),(select dateadd(month,-11,getdate())),101)
	SET @CUR_DATE=	 CONVERT(VARCHAR(12),getdate(),101)
	SET @CRITERIA_DATE =CONVERT(VARCHAR(12),CONVERT(VARCHAR(2),MONTH(@PREV_DATE)) + "/01/" + CONVERT(VARCHAR(4),YEAR(@PREV_DATE)),101)

	-- Insert the curent year into a temporary table
	DECLARE @ltbl_years TABLE (
	jan_year		int,
	feb_year		int,
	mar_year		int,
	apr_year		int,
	may_year		int,
	jun_year		int,
	jul_year		int,
	aug_year		int,
	sep_year		int,
	Oct_year		int,
	Nov_year		int,
	Dec_year		int)	
	

	-- Get current year for each period.
	insert into @ltbl_years
	select distinct Jan_year,	
	feb_year,
	mar_year,
	apr_year,
	may_year,
	jun_year,
	jul_year,
	aug_year,
	sep_year,
	Oct_year,
	Nov_year,
	Dec_year		


	from (
	Select 
	(case when month(epa.CompletionDate)=1 then year(epa.CompletionDate) end) as Jan_year,
	(case when month(epa.CompletionDate)=2 then year(epa.CompletionDate) end) as feb_year,
	(case when month(epa.CompletionDate)=3 then year(epa.CompletionDate) end) as mar_year,
	(case when month(epa.CompletionDate)=4 then year(epa.CompletionDate) end) as apr_year,
	(case when month(epa.CompletionDate)=5 then year(epa.CompletionDate) end) as may_year,
	(case when month(epa.CompletionDate)=6 then year(epa.CompletionDate) end) as Jun_year,
	(case when month(epa.CompletionDate)=7 then year(epa.CompletionDate) end) as Jul_year,
	(case when month(epa.CompletionDate)=8 then year(epa.CompletionDate) end) as Aug_year,
	(case when month(epa.CompletionDate)=9 then year(epa.CompletionDate) end) as Sep_year,
	(case when month(epa.CompletionDate)=10 then year(epa.CompletionDate) end) as Oct_year,
	(case when month(epa.CompletionDate)=11 then year(epa.CompletionDate) end) as Nov_year,	
	(case when month(epa.CompletionDate)=12 then year(epa.CompletionDate) end) as Dec_year
	from dbo.Expedition_Port  ep WITH (NOLOCK) 
	inner join  dbo.Expedition_Port_Associate epa WITH (NOLOCK) ON ep.ExpeditionPortID=epa.ExpeditionPortID
	inner join dbo.Associate a with (NOLOCK) ON epa.AssocId =a.AssocID
	inner join hierarchy h on a.operationnumber=convert(int,h.unit)
	where a.activeflag=1
	AND isnumeric(h.unit)=1 and (completiondate) between @CRITERIA_DATE and @CUR_DATE) as c

	

	select 
		SectorName,
		Divisionname,
		PortNumber,
		PortName,
		sum(Jan) as Jan_cnt,
		sum(Feb) as Feb_cnt,
		sum(Mar) as Mar_cnt,
		sum(Apr) as Apr_cnt,
		sum(May) as May_cnt,
		sum(Jun) as Jun_cnt,
		sum(Jul) as Jul_cnt,
		sum(Aug) as Aug_cnt,
		sum(Sep) as Sep_cnt,
		sum(Oct) as Oct_cnt,
		sum(Nov) as Nov_cnt,
		sum(Dec) as Dec_cnt,
		jan_year=(select distinct jan_year from @ltbl_years where jan_year is not null),
		feb_year=(select distinct feb_year from @ltbl_years where feb_year is not null),
		mar_year=(select distinct mar_year from @ltbl_years where mar_year is not null),
		apr_year=(select distinct apr_year from @ltbl_years where apr_year is not null),
		may_year=(select distinct may_year from @ltbl_years where may_year is not null),
		jun_year=(select distinct jun_year from @ltbl_years where jun_year is not null),
		jul_year=(select distinct jul_year from @ltbl_years where jul_year is not null),
		aug_year=(select distinct aug_year from @ltbl_years where aug_year is not null),
		sep_year=(select distinct sep_year from @ltbl_years where sep_year is not null),
	        oct_year=(select distinct oct_year from @ltbl_years where oct_year is not null),
		nov_year=(select distinct nov_year from @ltbl_years where nov_year is not null),
		dec_year=(select distinct dec_year from @ltbl_years where dec_year is not null)
		
		from (select h.SectorName,
	h.DivisionName,
	ep.PortNumber,
	ep.PortName,
	month(epa.CompletionDate) as month,
	year(epa.CompletionDate) as current_year,
	(case when  month(epa.CompletionDate)=1 then 1 else 0 end) as Jan,
	(case when  month(epa.CompletionDate)=2 then 1 else 0 end) as Feb,
	(case when month(epa.CompletionDate)=3 then 1 else 0 end) as Mar,
	(case when month(epa.CompletionDate)=4 then 1 else 0 end) as Apr,
	(case when  month(epa.CompletionDate)=5 then 1 else 0 end) as May,
	(case when  month(epa.CompletionDate)=6 then 1 else 0 end) as Jun,
	(case when month(epa.CompletionDate)=7 then 1 else 0 end) as Jul,
	(case when month(epa.CompletionDate)=8 then 1 else 0 end) as Aug,
	(case when month(epa.CompletionDate)=9 then 1 else 0 end) as Sep,
	(case when month(epa.CompletionDate)=10 then 1 else 0 end) as Oct,
	(case when month(epa.CompletionDate)=11 then 1 else 0 end) as Nov,
	(case when month(epa.CompletionDate)=12 then 1 else 0 end) as dec


		from dbo.Expedition_Port  ep WITH (NOLOCK) 
		inner join  dbo.Expedition_Port_Associate epa WITH (NOLOCK) ON ep.ExpeditionPortID=epa.ExpeditionPortID
		inner join dbo.Associate a with (NOLOCK) ON epa.AssocId =a.AssocID
		inner join hierarchy h on a.operationnumber=convert(int,h.unit)
		where a.activeflag=1
		AND isnumeric(h.unit)=1 and completiondate between @CRITERIA_DATE and @CUR_DATE) c
		group by PortNumber,PortName,SectorName,Divisionname
		order by PortNumber,SectorName,Divisionname



	

--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Associate table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1











GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

