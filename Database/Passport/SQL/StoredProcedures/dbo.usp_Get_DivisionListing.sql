SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

USE [Passport]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_DivisionListing]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_DivisionListing]
GO




/*--------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_DivisionListing

Description:	Returns Division listing from the Hierarchy table

Parameters:	NONE

Returns:	
Algorithm:	
Revisions:	08/23/2006	JJS Created
		11/19/2008	Jason Hassing

Example:	
		EXECUTE usp_Get_DivisionListing
-------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE   PROCEDURE dbo.usp_Get_DivisionListing

AS

	SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message


	SELECT DISTINCT 
		Division, 
		SectorName + ' - ' + DivisionName as DivisionName
	FROM         
		dbo.Hierarchy
		order by division


	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from Request_Access table'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

