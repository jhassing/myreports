SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_Expedition_Associate_ActiveFlag]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_Expedition_Associate_ActiveFlag]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Update_Expedition_Associate_ActiveFlag

History:     Generated on 8/3/2005 8:02:45 PM by COMPASS-USA\SchiaJ01

Description: Updates an Associates active flag to true/false

Parameters:   
		@ai_AssocID                             INT            , -- int                 (4 bytes)     (not nullable)
		@ab_ActiveFlag                           BIT           -- bit                	 (1 bytes)     (not nullable)   


Returns:         (0 for Success), (1 for Failure)
----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Update_Expedition_Associate_ActiveFlag(
			@ai_AssocID                             	INT            , -- int                 (4 bytes)     (not nullable)
			@ai_Expedition			INT	,
			@ab_Active	                          BIT           -- bit                	 (1 bytes)     (not nullable)

		) AS

	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)


	/* First, does the record already exists */
	if ( EXISTS (SELECT ExpeditionAssocID FROM  Expedition_Associate WHERE (ExpeditionID = @ai_Expedition ) AND ( AssocID = @ai_AssocID) ) )
		BEGIN
			UPDATE Expedition_Associate
			SET
				Active = @ab_Active

			WHERE
				( AssocID                         = @ai_AssocID ) AND
				( ExpeditionID		= @ai_Expedition )
		END
	ELSE
		BEGIN
			/* The record does not exist, add  a new one */
			EXECUTE usp_Insert_Expedition_Associate null, @ai_AssocID, @ai_Expedition, @ab_Active
		END


	IF @@ROWCOUNT = 0
	BEGIN
		SET @ls_ErrorDescription = 'Error Updating table Expedition_Associate ' + 
			'@ai_AssocID = ''' + CONVERT(VARCHAR(10),@ai_AssocID) + '''' 
		GOTO ErrorHandler
	END
	RETURN 0

ErrorHandler:
	RAISERROR(@ls_ErrorDescription, 16, 1)
	RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

