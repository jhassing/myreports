SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_Expedition_Port_Associate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_Expedition_Port_Associate]
GO

/*----------------------------------------------------------------------------------------------------------------------
Stored Proc:     dbo.usp_Update_Expedition_Port_Associate

Description:     Generated on 8/17/2005 12:09:30 PM by COMPASS-USA\SchiaJ01

Parameters:      @ai_ExpeditonPortAssocID int                  Not Nullable Primary Key Identity      Size:4    Prec:10  Scale:0  
                 @ai_ExpeditionPortID     int                  Not Nullable             Non-Identity  Size:4    Prec:10  Scale:0  
                 @ai_ExpeditionAssocID    int                  Not Nullable             Non-Identity  Size:4    Prec:10  Scale:0  
                 @ai_OperationNumber      int                  Not Nullable             Non-Identity  Size:4    Prec:10  Scale:0  
                 @ad_CompletionDate       datetime             Not Nullable             Non-Identity  Size:8    Prec:23  Scale:3  
                 @ad_CreatedDateTime      datetime             Not Nullable             Non-Identity  Size:8    Prec:23  Scale:3  
                 @as_CreatedBy            varchar              Not Nullable             Non-Identity  Size:75   Prec:0   Scale:0  
                 @ad_ModifiedDateTime     datetime             Not Nullable             Non-Identity  Size:8    Prec:23  Scale:3  
                 @as_ModifiedBy           varchar              Not Nullable             Non-Identity  Size:75   Prec:0   Scale:0  

Created By:
		8/17/2005 	John Schiavarelli

Returns:         (0 for Success), (1 for Failure)
----------------------------------------------------------------------------------------------------------------------*/
CREATE PROCEDURE dbo.usp_Update_Expedition_Port_Associate(
			@ai_ExpeditionPortID       	INT            , -- int                 (4 bytes)     (not nullable)
			@ai_AssocID      	INT            , -- int                 (4 bytes)     (not nullable)
			@ai_OperationNumber        	INT            , -- int                 (4 bytes)     (not nullable)
			@ad_CompletionDate         	DATETIME       , -- datetime            (8 bytes)     (not nullable)
			@as_ModifiedBy             		VARCHAR(75)      -- varchar             (75 bytes)    (not nullable)
		) AS

	SET NOCOUNT ON
	DECLARE @ls_ErrorDescription VARCHAR(256)


	UPDATE Expedition_Port_Associate
	SET						
		CompletionDate          	= @ad_CompletionDate,
		ModifiedDateTime        	= GetDate(),
		ModifiedBy              	= @as_ModifiedBy 

	WHERE
 		(ExpeditionPortID        	= @ai_ExpeditionPortID) AND
		(AssocID       		= @ai_AssocID) AND 
		(OperationNumber         	= @ai_OperationNumber)  
	IF @@ROWCOUNT = 0
	BEGIN
		SET @ls_ErrorDescription = 'Error Deleting from Table Expedition_Port_Associate with ' + 
			'@ai_ExpeditionPortID = ''' + CONVERT(VARCHAR(10),@ai_ExpeditionPortID) + '''' 
		GOTO ErrorHandler
	END
	RETURN 0

ErrorHandler:
	RAISERROR(@ls_ErrorDescription, 16, 1)
	RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

