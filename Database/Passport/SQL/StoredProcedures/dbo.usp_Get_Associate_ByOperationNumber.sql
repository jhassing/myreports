SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_Associate_ByOperationNumber]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_Associate_ByOperationNumber]
GO




/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_Associate_ByOperationNumber

Description:	Returns listing of Associates

Parameters:	@ai_OperationNumber              		INT,		Operations Number
)


Returns:	Associates that have been entered into Passport application
Algorithm:	
Revisions:	08/02/2005 	John Schiavarelli	 Created 

Example:	
		EXECUTE usp_Get_Associate_ByOperationNumber 1002
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE    PROCEDURE dbo.usp_Get_Associate_ByOperationNumber (			@ai_OperationNumber              		INT
)
AS

	SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	--Select record by id
	SELECT     dbo.Associate.*

	FROM         dbo.Associate 
	
	WHERE     ( dbo.Associate.OperationNumber = @ai_OperationNumber ) AND ( ActiveFlag = 1 ) AND ( HourlySalary = 'H' )

	ORDER BY AssocLName, AssocFName


	--Get the Error Codes and Row Count for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR, @ll_RowCount = @@ROWCOUNT

	--Check for Errors
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error selecting data from table Request_Access'
		GOTO Error_Handler
	END

	RETURN 

	--This is the error handler to rollback transactions and end the program.
	Error_Handler:
--		Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

