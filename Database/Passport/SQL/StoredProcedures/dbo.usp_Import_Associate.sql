SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Import_Associate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Import_Associate]
GO






/*--------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	

Description:	
Parameters:	

Returns:	
Algorithm:	
Revisions:	

Example:	
		EXECUTE dbo.usp_Import_Associate
-------------------------------------------------------------------------------------------------------------------------------------------*/
CREATE     PROCEDURE dbo.usp_Import_Associate
AS
	SET NOCOUNT ON

	--Required Variables
	DECLARE	@ll_RowCount		INT 		--Captures the number of rows affected by a statement
	DECLARE	@ll_ErrorCode		INT		--Captures the SQL error number
	DECLARE	@ls_ErrorMessage	VARCHAR(200)	--Used to store a custom error message
	DECLARE	@ls_SystemErrorMsg	NVARCHAR(255)	--Captures the systems error message

	--------------------------------------------------------------
	--Update active records
	--------------------------------------------------------------
	UPDATE dbo.Associate
	SET
		OperationNumber=ia.OperationNumber,
		AssocFName=ia.AssocFName,
		AssocLName=ia.AssocLName,
		CompassAssocFlag=1,
		HourlySalary=ia.HourlySalary,
		ActiveFlag = Case ia.ActiveFlag  
			When 1 Then 1
			Else 0 End,
		ModifiedDateTime = GETDATE(),
		ModifiedBy = 'AssocImport'
	FROM	dbo.Associate a, dbo.Import_Associate ia
	WHERE	a.AssocPerNum=ia.AssocPerNum
	
	--Get the Error Codes for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error updating active associates in table Associate'
		GOTO Error_Handler
	END	
	
	--------------------------------------------------------------
	--Update inactive records
	--------------------------------------------------------------
	UPDATE dbo.Associate
	SET
		ActiveFlag = 0
	FROM	dbo.Associate a 
	LEFT OUTER JOIN dbo.Import_Associate ia ON a.AssocPerNum = ia.AssocPerNum
	WHERE	ia.AssocPerNum IS NULL
	
	--Get the Error Codes for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error updating inactive associates in table Associate'
		GOTO Error_Handler
	END

	--------------------------------------------------------------
	--Insert new records
	--------------------------------------------------------------
	INSERT INTO dbo.Associate (AssocPerNum,OperationNumber,AssocFName,AssocLName,CompassAssocFlag,
					ActiveFlag,CreatedDateTime,CreatedBy,ModifiedDateTime,ModifiedBy, HourlySalary)
	SELECT
		ia.AssocPerNum,
		ia.OperationNumber,
		ia.AssocFName,
		ia.AssocLName,
		1		AS CompassAssocFlag,
		Case ia.ActiveFlag When 1 then 1 else 0 end AS ActiveFlag,
		GETDATE()	AS CreatedDateTime,
		'AssocImport'	AS CreatedBy,
		GETDATE()	AS ModifiedDateTime,
		'AssocImport'	AS ModifiedBy,
		ia.HourlySalary 	AS HourlySalary
	FROM
		dbo.Import_Associate ia
	LEFT OUTER JOIN
		dbo.Associate a ON ia.AssocPerNum= a.AssocPerNum
	WHERE	a.AssocPerNum IS NULL
	
	--Get the Error Codes for the Previous Statement
	SELECT @ll_ErrorCode = @@ERROR
	IF @ll_ErrorCode != 0
	BEGIN
		SELECT @ls_ErrorMessage = 'Error inserting data into table Associate'
		GOTO Error_Handler
	END

	

 	RETURN 

	--------------------------------------------------------------
	--This is the error handler to rollback transactions and end the program.
	--------------------------------------------------------------
	Error_Handler:
	--Cause the client to see the error message
		RAISERROR (@ls_ErrorMessage, 16, 1)
		RETURN 1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

