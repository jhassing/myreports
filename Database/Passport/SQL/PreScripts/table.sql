if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Expedition_Associate_Associate]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Expedition_Associate] DROP CONSTRAINT FK_Expedition_Associate_Associate
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Expedition_Associate_Expedition]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Expedition_Associate] DROP CONSTRAINT FK_Expedition_Associate_Expedition
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Expedition_Expedition_Port]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Expedition_Port] DROP CONSTRAINT FK_Expedition_Expedition_Port
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Operation_Expedition_Expedition]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Operation_Expedition] DROP CONSTRAINT FK_Operation_Expedition_Expedition
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Expedition_Port_Associate_Expedition_Associate]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Expedition_Port_Associate] DROP CONSTRAINT FK_Expedition_Port_Associate_Expedition_Associate
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Expedition_Port_Associate_Expedition_Port]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Expedition_Port_Associate] DROP CONSTRAINT FK_Expedition_Port_Associate_Expedition_Port
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Associate_Port_Expedition_Port]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[zzAssociate_Port] DROP CONSTRAINT FK_Associate_Port_Expedition_Port
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Request_Access_Request_Status]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Request_Access] DROP CONSTRAINT FK_Request_Access_Request_Status
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Associate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Associate]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Expedition]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Expedition]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Expedition_Associate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Expedition_Associate]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Expedition_Port]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Expedition_Port]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Expedition_Port_Associate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Expedition_Port_Associate]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Import_Associate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Import_Associate]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Operation_Expedition]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Operation_Expedition]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Request_Access]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Request_Access]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Request_Status]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Request_Status]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Sector]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Sector]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[zrops]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[zrops]
GO

CREATE TABLE [dbo].[Associate] (
	[AssocID] [int] IDENTITY (1, 1) NOT NULL ,
	[AssocPerNum] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OperationNumber] [int] NOT NULL ,
	[AssocFName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[AssocLName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CompassAssocFlag] [bit] NOT NULL ,
	[ActiveFlag] [bit] NOT NULL ,
	[CreatedDateTime] [datetime] NULL ,
	[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModifiedDateTime] [datetime] NULL ,
	[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Expedition] (
	[ExpeditionID] [int] IDENTITY (1, 1) NOT NULL ,
	[ExpeditionName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreatedDateTime] [datetime] NOT NULL ,
	[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModifiedDateTime] [datetime] NOT NULL ,
	[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Expedition_Associate] (
	[ExpeditionAssocID] [int] IDENTITY (1, 1) NOT NULL ,
	[ExpeditionID] [int] NOT NULL ,
	[AssocID] [int] NOT NULL ,
	[Active] [bit] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Expedition_Port] (
	[ExpeditionPortID] [int] IDENTITY (1, 1) NOT NULL ,
	[ExpeditionID] [int] NOT NULL ,
	[PortNumber] [int] NOT NULL ,
	[PortName] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CreatedDateTime] [datetime] NOT NULL ,
	[CreatedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModifiedDateTime] [datetime] NOT NULL ,
	[ModifiedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Expedition_Port_Associate] (
	[ExpeditonPortAssocID] [int] IDENTITY (1, 1) NOT NULL ,
	[ExpeditionPortID] [int] NOT NULL ,
	[ExpeditionAssocID] [int] NOT NULL ,
	[OperationNumber] [int] NOT NULL ,
	[CompletionDate] [datetime] NOT NULL ,
	[CreatedDateTime] [datetime] NOT NULL ,
	[CreatedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModifiedDateTime] [datetime] NOT NULL ,
	[ModifiedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Import_Associate] (
	[ImportAssociateID] [int] IDENTITY (1, 1) NOT NULL ,
	[AssocLName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AssocFName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AssocPerNum] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OperationNumber] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Operation_Expedition] (
	[OperationExpeditionID] [int] IDENTITY (1, 1) NOT NULL ,
	[OperationNumber] [int] NOT NULL ,
	[ExpeditionID] [int] NOT NULL ,
	[CreatedDateTime] [datetime] NOT NULL ,
	[CreatedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModifiedDateTime] [datetime] NOT NULL ,
	[ModifiedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Request_Access] (
	[RequestAccessID] [int] IDENTITY (1, 1) NOT NULL ,
	[ManagerLogin] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SectorName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OperationNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OperationName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SecondLineManagerLogin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[RequestStatusID] [int] NOT NULL ,
	[CreatedDateTime] [datetime] NOT NULL ,
	[ModifiedDateTime] [datetime] NOT NULL ,
	[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Request_Status] (
	[RequestStatusID] [int] IDENTITY (1, 1) NOT NULL ,
	[RequestStatus] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Sector] (
	[SectorID] [int] IDENTITY (1, 1) NOT NULL ,
	[SectorCode] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SectorName] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CreatedDateTime] [datetime] NOT NULL ,
	[CreatedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModifiedDateTime] [datetime] NOT NULL ,
	[ModifiedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[zrops] (
	[ZropsID] [int] IDENTITY (1, 1) NOT NULL ,
	[OperationCode] [int] NULL ,
	[DM] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Region] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Associate] WITH NOCHECK ADD 
	CONSTRAINT [cuidx_Associate_AssocPerNum] UNIQUE  CLUSTERED 
	(
		[AssocPerNum]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Expedition] WITH NOCHECK ADD 
	CONSTRAINT [PK_Expedition] PRIMARY KEY  CLUSTERED 
	(
		[ExpeditionID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Expedition_Associate] WITH NOCHECK ADD 
	CONSTRAINT [cuidx_Expedition_Associate_ExpeditionID_AssocID] UNIQUE  CLUSTERED 
	(
		[ExpeditionID],
		[AssocID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Expedition_Port] WITH NOCHECK ADD 
	CONSTRAINT [cuidx_Expedition_Port_ExpeditionID_PortNumber] UNIQUE  CLUSTERED 
	(
		[ExpeditionID],
		[PortNumber]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Expedition_Port_Associate] WITH NOCHECK ADD 
	CONSTRAINT [cuidx_Expedition_Port_Associate_ExpeditionPortID_ExpeditionAssocID_OperationNumber] UNIQUE  CLUSTERED 
	(
		[ExpeditionPortID],
		[ExpeditionAssocID],
		[OperationNumber]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Operation_Expedition] WITH NOCHECK ADD 
	CONSTRAINT [PK_Operation_Expedition] PRIMARY KEY  CLUSTERED 
	(
		[OperationExpeditionID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Request_Status] WITH NOCHECK ADD 
	CONSTRAINT [cuidx_Request_Status_RequestStatus] UNIQUE  CLUSTERED 
	(
		[RequestStatus]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Sector] WITH NOCHECK ADD 
	CONSTRAINT [PK_Sector] PRIMARY KEY  CLUSTERED 
	(
		[SectorID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[zrops] WITH NOCHECK ADD 
	CONSTRAINT [PK_zrops] PRIMARY KEY  CLUSTERED 
	(
		[ZropsID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

 CREATE  CLUSTERED  INDEX [cuidx_Import_Associate_AssocPerNumber] ON [dbo].[Import_Associate]([AssocPerNum]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  CLUSTERED  INDEX [idx_Request_Access_ManagerLogin_RequestStatusID] ON [dbo].[Request_Access]([ManagerLogin], [RequestStatusID]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

ALTER TABLE [dbo].[Associate] WITH NOCHECK ADD 
	CONSTRAINT [DF_Associate_ActiveFlag] DEFAULT (1) FOR [ActiveFlag],
	CONSTRAINT [DF_Associate_CreatedDateTime] DEFAULT (getdate()) FOR [CreatedDateTime],
	CONSTRAINT [DF_Associate_ModifiedDateTime] DEFAULT (getdate()) FOR [ModifiedDateTime],
	CONSTRAINT [uidx_Associate_AssocID] PRIMARY KEY  NONCLUSTERED 
	(
		[AssocID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [uidx_Associate_AssocPerNum] UNIQUE  NONCLUSTERED 
	(
		[AssocPerNum]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Expedition] WITH NOCHECK ADD 
	CONSTRAINT [DF_Expedition_CreatedDateTime] DEFAULT (getdate()) FOR [CreatedDateTime],
	CONSTRAINT [DF_Expedition_ModifiedDateTime] DEFAULT (getdate()) FOR [ModifiedDateTime],
	CONSTRAINT [IX_ExpeditionName] UNIQUE  NONCLUSTERED 
	(
		[ExpeditionName]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Expedition_Associate] WITH NOCHECK ADD 
	CONSTRAINT [uidx_Expedition_Associate_ExpeditionAssocID] PRIMARY KEY  NONCLUSTERED 
	(
		[ExpeditionAssocID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [IX_Expedition_Associate] UNIQUE  NONCLUSTERED 
	(
		[ExpeditionID],
		[AssocID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Expedition_Port] WITH NOCHECK ADD 
	CONSTRAINT [DF_Expedition_Port_CreatedDateTime] DEFAULT (getdate()) FOR [CreatedDateTime],
	CONSTRAINT [DF_Expedition_Port_ModifiedDateTime] DEFAULT (getdate()) FOR [ModifiedDateTime],
	CONSTRAINT [uidx_Expedition_Port_ExpeditionPortID] PRIMARY KEY  NONCLUSTERED 
	(
		[ExpeditionPortID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Expedition_Port_Associate] WITH NOCHECK ADD 
	CONSTRAINT [uidx_Expedition_Port_Associate_ExpeditionPortAssocID] PRIMARY KEY  NONCLUSTERED 
	(
		[ExpeditonPortAssocID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Import_Associate] WITH NOCHECK ADD 
	CONSTRAINT [uidx_Import_Associates] PRIMARY KEY  NONCLUSTERED 
	(
		[ImportAssociateID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Operation_Expedition] WITH NOCHECK ADD 
	CONSTRAINT [DF_Unit_Expedition_CreatedDateTime] DEFAULT (getdate()) FOR [CreatedDateTime],
	CONSTRAINT [DF_Unit_Expedition_ModifiedDateTime] DEFAULT (getdate()) FOR [ModifiedDateTime]
GO

ALTER TABLE [dbo].[Request_Access] WITH NOCHECK ADD 
	CONSTRAINT [DF_Request_Access_RequestStatusID] DEFAULT (1) FOR [RequestStatusID],
	CONSTRAINT [DF_AccessRequest_CreatedDateTime] DEFAULT (getdate()) FOR [CreatedDateTime],
	CONSTRAINT [DF_AccessRequest_ModifiedDateTime] DEFAULT (getdate()) FOR [ModifiedDateTime],
	CONSTRAINT [PK_Manager] PRIMARY KEY  NONCLUSTERED 
	(
		[RequestAccessID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Request_Status] WITH NOCHECK ADD 
	CONSTRAINT [uidx_RequestStatus_RequestStatusID] PRIMARY KEY  NONCLUSTERED 
	(
		[RequestStatusID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Sector] WITH NOCHECK ADD 
	CONSTRAINT [DF_Sector_CreatedDateTime] DEFAULT (getdate()) FOR [CreatedDateTime],
	CONSTRAINT [DF_Sector_ModifiedDateTime] DEFAULT (getdate()) FOR [ModifiedDateTime],
	CONSTRAINT [IX_Sector_SectorCode] UNIQUE  NONCLUSTERED 
	(
		[SectorCode]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Expedition_Associate] ADD 
	CONSTRAINT [FK_Expedition_Associate_Associate] FOREIGN KEY 
	(
		[AssocID]
	) REFERENCES [dbo].[Associate] (
		[AssocID]
	),
	CONSTRAINT [FK_Expedition_Associate_Expedition] FOREIGN KEY 
	(
		[ExpeditionID]
	) REFERENCES [dbo].[Expedition] (
		[ExpeditionID]
	)
GO

ALTER TABLE [dbo].[Expedition_Port] ADD 
	CONSTRAINT [FK_Expedition_Expedition_Port] FOREIGN KEY 
	(
		[ExpeditionID]
	) REFERENCES [dbo].[Expedition] (
		[ExpeditionID]
	)
GO

ALTER TABLE [dbo].[Expedition_Port_Associate] ADD 
	CONSTRAINT [FK_Expedition_Port_Associate_Expedition_Associate] FOREIGN KEY 
	(
		[ExpeditionAssocID]
	) REFERENCES [dbo].[Expedition_Associate] (
		[ExpeditionAssocID]
	),
	CONSTRAINT [FK_Expedition_Port_Associate_Expedition_Port] FOREIGN KEY 
	(
		[ExpeditionPortID]
	) REFERENCES [dbo].[Expedition_Port] (
		[ExpeditionPortID]
	)
GO

ALTER TABLE [dbo].[Operation_Expedition] ADD 
	CONSTRAINT [FK_Operation_Expedition_Expedition] FOREIGN KEY 
	(
		[ExpeditionID]
	) REFERENCES [dbo].[Expedition] (
		[ExpeditionID]
	)
GO

ALTER TABLE [dbo].[Request_Access] ADD 
	CONSTRAINT [FK_Request_Access_Request_Status] FOREIGN KEY 
	(
		[RequestStatusID]
	) REFERENCES [dbo].[Request_Status] (
		[RequestStatusID]
	)
GO

