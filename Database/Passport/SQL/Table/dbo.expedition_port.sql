if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Expedition_Port]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Expedition_Port]
GO

CREATE TABLE [dbo].[Expedition_Port] (
	[ExpeditionPortID] [int] IDENTITY (1, 1) NOT NULL ,
	[ExpeditionID] [int] NOT NULL ,
	[PortNumber] [int] NOT NULL ,
	[PortName] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Expedition_Port_CreatedDateTime] DEFAULT (getdate()),
	[CreatedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModifiedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Expedition_Port_ModifiedDateTime] DEFAULT (getdate()),
	[ModifiedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [uidx_Expedition_Port_ExpeditionPortID] PRIMARY KEY  NONCLUSTERED 
	(
		[ExpeditionPortID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [cuidx_Expedition_Port_ExpeditionID_PortNumber] UNIQUE  CLUSTERED 
	(
		[ExpeditionID],
		[PortNumber]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [FK_Expedition_Expedition_Port] FOREIGN KEY 
	(
		[ExpeditionID]
	) REFERENCES [dbo].[Expedition] (
		[ExpeditionID]
	)
) ON [PRIMARY]
GO


