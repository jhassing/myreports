if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Import_Associate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Import_Associate]
GO

CREATE TABLE [dbo].[Import_Associate] (
	[ImportAssociateID] [int] IDENTITY (1, 1) NOT NULL ,
	[AssocLName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AssocFName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AssocPerNum] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OperationNumber] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [uidx_Import_Associates] PRIMARY KEY  NONCLUSTERED 
	(
		[ImportAssociateID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
) ON [PRIMARY]
GO

 CREATE  CLUSTERED  INDEX [cuidx_Import_Associate_AssocPerNumber] ON [dbo].[Import_Associate]([AssocPerNum]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO


