if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Request_Status]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Request_Status]
GO

CREATE TABLE [dbo].[Request_Status] (
	[RequestStatusID] [int] IDENTITY (1, 1) NOT NULL ,
	[RequestStatus] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [uidx_RequestStatus_RequestStatusID] PRIMARY KEY  NONCLUSTERED 
	(
		[RequestStatusID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [cuidx_Request_Status_RequestStatus] UNIQUE  CLUSTERED 
	(
		[RequestStatus]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
) ON [PRIMARY]
GO


