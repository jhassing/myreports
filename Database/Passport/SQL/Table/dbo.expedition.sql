if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Expedition]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Expedition]
GO

CREATE TABLE [dbo].[Expedition] (
	[ExpeditionID] [int] IDENTITY (1, 1) NOT NULL ,
	[ExpeditionName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Expedition_CreatedDateTime] DEFAULT (getdate()),
	[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModifiedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Expedition_ModifiedDateTime] DEFAULT (getdate()),
	[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_Expedition] PRIMARY KEY  CLUSTERED 
	(
		[ExpeditionID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [IX_ExpeditionName] UNIQUE  NONCLUSTERED 
	(
		[ExpeditionName]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
) ON [PRIMARY]
GO


