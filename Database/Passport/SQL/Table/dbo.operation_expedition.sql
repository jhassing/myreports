if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Operation_Expedition]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Operation_Expedition]
GO

CREATE TABLE [dbo].[Operation_Expedition] (
	[OperationExpeditionID] [int] IDENTITY (1, 1) NOT NULL ,
	[OperationNumber] [int] NOT NULL ,
	[ExpeditionID] [int] NOT NULL ,
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Unit_Expedition_CreatedDateTime] DEFAULT (getdate()),
	[CreatedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModifiedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Unit_Expedition_ModifiedDateTime] DEFAULT (getdate()),
	[ModifiedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_Operation_Expedition] PRIMARY KEY  CLUSTERED 
	(
		[OperationExpeditionID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [FK_Operation_Expedition_Expedition] FOREIGN KEY 
	(
		[ExpeditionID]
	) REFERENCES [dbo].[Expedition] (
		[ExpeditionID]
	)
) ON [PRIMARY]
GO


