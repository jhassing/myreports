if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Request_Access]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Request_Access]
GO

CREATE TABLE [dbo].[Request_Access] (
	[RequestAccessID] [int] IDENTITY (1, 1) NOT NULL ,
	[ManagerLogin] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SectorName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OperationNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OperationName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SecondLineManagerLogin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[RequestStatusID] [int] NOT NULL CONSTRAINT [DF_Request_Access_RequestStatusID] DEFAULT (1),
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_AccessRequest_CreatedDateTime] DEFAULT (getdate()),
	[ModifiedDateTime] [datetime] NOT NULL CONSTRAINT [DF_AccessRequest_ModifiedDateTime] DEFAULT (getdate()),
	[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_Manager] PRIMARY KEY  NONCLUSTERED 
	(
		[RequestAccessID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [FK_Request_Access_Request_Status] FOREIGN KEY 
	(
		[RequestStatusID]
	) REFERENCES [dbo].[Request_Status] (
		[RequestStatusID]
	)
) ON [PRIMARY]
GO

 CREATE  CLUSTERED  INDEX [idx_Request_Access_ManagerLogin_RequestStatusID] ON [dbo].[Request_Access]([ManagerLogin], [RequestStatusID]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO


