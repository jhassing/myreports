if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Hierarchy]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Hierarchy]
GO

CREATE TABLE [dbo].[Hierarchy] (
	[Sector] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SectorName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Division] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DivisionName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Region] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RegionName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Unit] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[UnitName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Hierarchy] WITH NOCHECK ADD 
	CONSTRAINT [PK_Import_Hierarchy] PRIMARY KEY  CLUSTERED 
	(
		[Unit]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

