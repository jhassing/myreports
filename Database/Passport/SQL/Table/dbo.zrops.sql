if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[zrops]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[zrops]
GO

CREATE TABLE [dbo].[zrops] (
	[ZropsID] [int] IDENTITY (1, 1) NOT NULL ,
	[OperationCode] [int] NULL ,
	[DM] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Region] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_zrops] PRIMARY KEY  CLUSTERED 
	(
		[ZropsID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
) ON [PRIMARY]
GO


