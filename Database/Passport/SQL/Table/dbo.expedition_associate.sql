if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Expedition_Associate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Expedition_Associate]
GO

CREATE TABLE [dbo].[Expedition_Associate] (
	[ExpeditionAssocID] [int] IDENTITY (1, 1) NOT NULL ,
	[ExpeditionID] [int] NOT NULL ,
	[AssocID] [int] NOT NULL ,
	[Active] [bit] NOT NULL ,
	CONSTRAINT [uidx_Expedition_Associate_ExpeditionAssocID] PRIMARY KEY  NONCLUSTERED 
	(
		[ExpeditionAssocID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [cuidx_Expedition_Associate_ExpeditionID_AssocID] UNIQUE  CLUSTERED 
	(
		[ExpeditionID],
		[AssocID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [IX_Expedition_Associate] UNIQUE  NONCLUSTERED 
	(
		[ExpeditionID],
		[AssocID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [FK_Expedition_Associate_Associate] FOREIGN KEY 
	(
		[AssocID]
	) REFERENCES [dbo].[Associate] (
		[AssocID]
	),
	CONSTRAINT [FK_Expedition_Associate_Expedition] FOREIGN KEY 
	(
		[ExpeditionID]
	) REFERENCES [dbo].[Expedition] (
		[ExpeditionID]
	)
) ON [PRIMARY]
GO


