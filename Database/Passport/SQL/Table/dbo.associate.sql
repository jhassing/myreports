if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Associate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Associate]
GO

CREATE TABLE [dbo].[Associate] (
	[AssocID] [int] IDENTITY (1, 1) NOT NULL ,
	[AssocPerNum] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OperationNumber] [int] NOT NULL ,
	[AssocFName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[AssocLName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CompassAssocFlag] [bit] NOT NULL ,
	[ActiveFlag] [bit] NOT NULL CONSTRAINT [DF_Associate_ActiveFlag] DEFAULT (1),
	[CreatedDateTime] [datetime] NULL CONSTRAINT [DF_Associate_CreatedDateTime] DEFAULT (getdate()),
	[CreatedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModifiedDateTime] [datetime] NULL CONSTRAINT [DF_Associate_ModifiedDateTime] DEFAULT (getdate()),
	[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [uidx_Associate_AssocID] PRIMARY KEY  NONCLUSTERED 
	(
		[AssocID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [cuidx_Associate_AssocPerNum] UNIQUE  CLUSTERED 
	(
		[AssocPerNum]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [uidx_Associate_AssocPerNum] UNIQUE  NONCLUSTERED 
	(
		[AssocPerNum]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
) ON [PRIMARY]
GO


