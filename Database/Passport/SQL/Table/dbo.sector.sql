if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Sector]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Sector]
GO

CREATE TABLE [dbo].[Sector] (
	[SectorID] [int] IDENTITY (1, 1) NOT NULL ,
	[SectorCode] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SectorName] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CreatedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Sector_CreatedDateTime] DEFAULT (getdate()),
	[CreatedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModifiedDateTime] [datetime] NOT NULL CONSTRAINT [DF_Sector_ModifiedDateTime] DEFAULT (getdate()),
	[ModifiedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_Sector] PRIMARY KEY  CLUSTERED 
	(
		[SectorID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [IX_Sector_SectorCode] UNIQUE  NONCLUSTERED 
	(
		[SectorCode]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
) ON [PRIMARY]
GO


