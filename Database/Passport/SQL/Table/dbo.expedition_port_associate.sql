if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Expedition_Port_Associate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Expedition_Port_Associate]
GO

CREATE TABLE [dbo].[Expedition_Port_Associate] (
	[ExpeditonPortAssocID] [int] IDENTITY (1, 1) NOT NULL ,
	[ExpeditionPortID] [int] NOT NULL ,
	[ExpeditionAssocID] [int] NOT NULL ,
	[OperationNumber] [int] NOT NULL ,
	[CompletionDate] [datetime] NOT NULL ,
	[CreatedDateTime] [datetime] NOT NULL ,
	[CreatedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModifiedDateTime] [datetime] NOT NULL ,
	[ModifiedBy] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [uidx_Expedition_Port_Associate_ExpeditionPortAssocID] PRIMARY KEY  NONCLUSTERED 
	(
		[ExpeditonPortAssocID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [cuidx_Expedition_Port_Associate_ExpeditionPortID_ExpeditionAssocID_OperationNumber] UNIQUE  CLUSTERED 
	(
		[ExpeditionPortID],
		[ExpeditionAssocID],
		[OperationNumber]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [FK_Expedition_Port_Associate_Expedition_Associate] FOREIGN KEY 
	(
		[ExpeditionAssocID]
	) REFERENCES [dbo].[Expedition_Associate] (
		[ExpeditionAssocID]
	),
	CONSTRAINT [FK_Expedition_Port_Associate_Expedition_Port] FOREIGN KEY 
	(
		[ExpeditionPortID]
	) REFERENCES [dbo].[Expedition_Port] (
		[ExpeditionPortID]
	)
) ON [PRIMARY]
GO


