using System.Collections.Generic;

namespace MyReports.DAL
{
    public class ViewedReportDateCreatedAndCostCenterComparer : IComparer<ViewedReportObject>
    {
        #region IComparer<struct_ViewedReport> CostCenters

        public int Compare(ViewedReportObject viewedReport1, ViewedReportObject viewedReport2)
        {
            int retInt = 1;
            if (viewedReport1 == null && viewedReport2 != null)
            {
                return 0;
            }
            if (viewedReport1 != null && viewedReport2 == null)
            {
                return 0;
            }
            if (viewedReport1 != null)
            {
                return
                    retInt =
                    viewedReport1.BurstDate == viewedReport2.BurstDate
                        ? viewedReport1.CostCenterID.CompareTo(viewedReport2.CostCenterID)
                        : viewedReport2.BurstDate.CompareTo(viewedReport1.BurstDate);
            }
            return retInt;
        }

        #endregion
    }
}