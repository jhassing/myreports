using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using ReportDistribution.SQLServerDAL.Base;

namespace MyReports.DAL
{
    /// <summary>
    /// Summary description for AuditLogging.
    /// </summary>
    public class AuditLogging : DalBase
    {
        public AuditLogging(Database db) : base(db)
        {
        }

        public bool InsertAudit(string domainName, string sessionID, string userName,
                                string platform, string browser, string browserVersion,
                                string userHostAddress, string workstationName)
        {
            const string sqlCommand = "usp_Insert_UserAuditData";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_DomainName", DbType.String, domainName);
            _db.AddInParameter(dbCommand, "as_SessionID", DbType.String, sessionID);
            _db.AddInParameter(dbCommand, "as_UserName", DbType.DateTime, userName);
            _db.AddInParameter(dbCommand, "as_Platform", DbType.String, platform);
            _db.AddInParameter(dbCommand, "as_Browser", DbType.String, browser);
            _db.AddInParameter(dbCommand, "ad_BrowserVersion", DbType.String, browserVersion);
            _db.AddInParameter(dbCommand, "as_UserHostAddress", DbType.String, userHostAddress);
            _db.AddInParameter(dbCommand, "as_WorkstationName", DbType.String, workstationName);

            return DoTransaction(dbCommand);
        }
    }
}