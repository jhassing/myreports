using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Common;
using ReportDistribution.SQLServerDAL.Base;
using UserManagement.BLL;

namespace MyReports.DAL
{
    public class ViewedReport : DalBase
    {
        public ViewedReport(Database db) : base(db)
        {
        }

        public List<ViewedReportObject> GetViewedReports(DateTime startDate, DateTime endDate, string reportType,
                                                         string costCenterList, bool viewed, bool notviewed)
        {
            List<ViewedReportObject> aryRet = new List<ViewedReportObject>();

            Authentication oAuth = new Authentication(AppValues.UserName, AppValues.Password,
                                                      AppValues.FullyQualifiedDomain, AppValues.LdapPort);
            Users oUser = new Users(oAuth);

            const string sqlCommand = "usp_Get_ViewedReports";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            //todo Add parameters for report (Type[Sub,AR], OrganizationalUnit[Rgn,Dist,CC[]])	

            _db.AddInParameter(dbCommand, "ad_StartDate", DbType.Int32, startDate);
            _db.AddInParameter(dbCommand, "ad_EndDate", DbType.Int32, endDate);
            _db.AddInParameter(dbCommand, "as_ReportType", DbType.Int32, reportType);
            _db.AddInParameter(dbCommand, "as_CostCenters", DbType.Int32, costCenterList);
            _db.AddInParameter(dbCommand, "ab_Viewed", DbType.Int32, viewed);
            _db.AddInParameter(dbCommand, "ab_NotViewed", DbType.Int32, notviewed);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    string reportname = dataReader["ReportName"].ToString().Trim();
                    string[] reportElemements = reportname.Split('/');

                    string reporttype = reportElemements[3].Contains("ZCAR2201") ? "ZCAR2201" : "ZCAR2821";

                    string username = dataReader["UserName"].ToString();
                    if (username != "")
                    {
                        List<tUserListing> userlist = oUser.GetUsersBasedOnANR(username, AppValues.CurrentOU);

                        // if username is not in Active Directory check for a closed account starting with zz
                        if (userlist.Count == 0)
                        {
                            userlist = oUser.GetUsersBasedOnANR("zz" + username, AppValues.CurrentOU);
                        }
                        // if an account is found set the username to firstname lastname, otherwise return the account ID
                        if (userlist.Count != 0)
                        {
                            string realname = userlist[0].FirstName + " " + userlist[0].LastName;
                            username = realname;
                        }
                    }

                    const string rgn = "";
                    string[] costcenter = reportElemements[2].Split('-');
                    string costcenterid = costcenter[0].Trim();
                    string costcentername = "";
                    if (costcenter.Length > 1)
                    {
                        for (int i = 1; i < costcenter.Length; i++)
                        {
                            if (i > 1)
                                costcentername += "-";
                            costcentername += costcenter[i];
                        }
                    }
                    costcentername.Trim();
                    string delay = "";
                    DateTime bursttime = Convert.ToDateTime(dataReader["BurstEntryTime"]);
                    string burstdate = bursttime.ToShortDateString();
                    DateTime viewedtime;
                    string vieweddate = "";
                    if (dataReader["ViewedDate"] != DBNull.Value)
                    {
                        viewedtime = Convert.ToDateTime(dataReader["ViewedDate"]);
                        vieweddate = viewedtime.ToShortDateString();
                        TimeSpan epoch = viewedtime - bursttime;
                        delay = epoch.Days.ToString();
                    }

                    aryRet.Add(
                        new ViewedReportObject
                            {
                                UserName = username,
                                ReportType = reporttype,
                                ReportName = reportname,
                                Region = rgn,
                                CostCenterID = costcenterid,
                                CostCenterName = costcentername,
                                Delay = delay,
                                BurstDate = burstdate,
                                ViewedDate = vieweddate
                            });
                }
            }

            aryRet.Sort(new ViewedReportDateCreatedAndCostCenterComparer());

            return aryRet;
        }
    }
}