using System;

namespace MyReports.DAL
{
    [Serializable]
    public class ViewedReportObject
    {
        public string ReportName { get; set; }
        public string ReportType { get; set; }
        public string UserName { get; set; }
        public string Region { get; set; }
        public string CostCenterID { get; set; }
        public string CostCenterName { get; set; }
        public string ViewedDate { get; set; }
        public string BurstDate { get; set; }
        public string Delay { get; set; }
    }
}