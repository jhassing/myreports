'----------------------------------------------------------------------------
' File: C:\Actuate Reports\BenefitsAccts\Beni\Beni.bas
'
' Generated From: C:\Actuate Reports\BenefitsAccts\Beni\Beni.rod
' Generated On:   12/07/05 13:32:00
'----------------------------------------------------------------------------
'
' Modules generated into this Basic file include:

'       Module C:\Actuate Reports\BenefitsAccts\Beni\Library\NADatabaseConnection.rol
'
'       Module C:\Actuate Reports\BenefitsAccts\Beni\Beni.rod
'           Includes Library/NADatabaseConnection.rol
'
'----------------------------------------------------------------------------
'

'----------------------------------------
' Global type and variable declarations
'----------------------------------------

Declare
    Option Internal
    
    '----------------------------------------
    ' Global variables
    '----------------------------------------
    
    Global as_NationalAccountNumber      As String
    Global odbc                          As String

End Declare


' Main program for the Factory.  Creates a Report class that performs
' the report generation process.

Sub Factory( )
	Dim Report As AcReport

	StartFactory( )
	CreateRelationshipMap( )
	Set Report = New NewReportApp
	Report.SetProperties( )
	Report.BuildReport( )
	FinishFactory( ) 
End Sub


Sub CreateRelationshipMap( )
    AddComponent( "NewReportApp", "Content", 1, "NewReportApp::Report" )
    AddComponent( "NewReportApp", "PageList", 1, "NewReportApp::SimplePageList" )
    AddComponent( "NewReportApp::Benefits", "Content", 1, "NewReportApp::Frame" )
    AddComponent( "NewReportApp::Benefits", "Before", 1, "NewReportApp::Frame2" )
    AddComponent( "NewReportApp::DataSource", "DataRow", 1, "NewReportApp::DataRow" )
    AddComponent( "NewReportApp::Frame", "Content", 1, "NewReportApp::Frame::RectangleControl" )
    AddComponent( "NewReportApp::Frame", "Content", 2, "NewReportApp::Frame::FullNameControl" )
    AddComponent( "NewReportApp::Frame", "Content", 3, "NewReportApp::Frame::PernrControl" )
    AddComponent( "NewReportApp::Frame", "Content", 4, "NewReportApp::Frame::DescriptionControl" )
    AddComponent( "NewReportApp::Frame", "Content", 5, "NewReportApp::Frame::EEGrpDescControl" )
    AddComponent( "NewReportApp::Frame1", "Content", 1, "NewReportApp::Frame1::RectangleControl" )
    AddComponent( "NewReportApp::Frame1", "Content", 2, "NewReportApp::Frame1::LabelControl2" )
    AddComponent( "NewReportApp::Frame1", "Content", 3, "NewReportApp::Frame1::LabelControl4" )
    AddComponent( "NewReportApp::Frame1", "Content", 4, "NewReportApp::Frame1::LabelControl3" )
    AddComponent( "NewReportApp::Frame1", "Content", 5, "NewReportApp::Frame1::LineControl4" )
    AddComponent( "NewReportApp::Frame1", "Content", 6, "NewReportApp::Frame1::FullNameControl1" )
    AddComponent( "NewReportApp::Frame1", "Content", 7, "NewReportApp::Frame1::FullNameControl" )
    AddComponent( "NewReportApp::Frame1", "Content", 8, "NewReportApp::Frame1::LineControl5" )
    AddComponent( "NewReportApp::Frame1", "Content", 9, "NewReportApp::Frame1::LabelControl11" )
    AddComponent( "NewReportApp::Frame1", "Content", 10, "NewReportApp::Frame1::LabelControl10" )
    AddComponent( "NewReportApp::Frame2", "Content", 1, "NewReportApp::Frame2::RectangleControl" )
    AddComponent( "NewReportApp::Frame2", "Content", 2, "NewReportApp::Frame2::LineControl2" )
    AddComponent( "NewReportApp::Frame2", "Content", 3, "NewReportApp::Frame2::LineControl3" )
    AddComponent( "NewReportApp::Frame2", "Content", 4, "NewReportApp::Frame2::LabelControl12" )
    AddComponent( "NewReportApp::Frame2", "Content", 5, "NewReportApp::Frame2::UnitMailADDR1Control6" )
    AddComponent( "NewReportApp::Frame2", "Content", 6, "NewReportApp::Frame2::UnitMailADDR1Control5" )
    AddComponent( "NewReportApp::Frame2", "Content", 7, "NewReportApp::Frame2::LabelControl9" )
    AddComponent( "NewReportApp::Frame2", "Content", 8, "NewReportApp::Frame2::LabelControl8" )
    AddComponent( "NewReportApp::Frame2", "Content", 9, "NewReportApp::Frame2::LabelControl7" )
    AddComponent( "NewReportApp::Frame2", "Content", 10, "NewReportApp::Frame2::LabelControl6" )
    AddComponent( "NewReportApp::Frame2", "Content", 11, "NewReportApp::Frame2::LabelControl5" )
    AddComponent( "NewReportApp::Frame2", "Content", 12, "NewReportApp::Frame2::LabelControl4" )
    AddComponent( "NewReportApp::Frame2", "Content", 13, "NewReportApp::Frame2::LabelControl3" )
    AddComponent( "NewReportApp::Frame2", "Content", 14, "NewReportApp::Frame2::LabelControl2" )
    AddComponent( "NewReportApp::Frame2", "Content", 15, "NewReportApp::Frame2::LabelControl1" )
    AddComponent( "NewReportApp::Frame2", "Content", 16, "NewReportApp::Frame2::LabelControl" )
    AddComponent( "NewReportApp::Frame2", "Content", 17, "NewReportApp::Frame2::UnitMailADDR1Control4" )
    AddComponent( "NewReportApp::Frame2", "Content", 18, "NewReportApp::Frame2::UnitMailADDR1Control1" )
    AddComponent( "NewReportApp::Frame2", "Content", 19, "NewReportApp::Frame2::UnitMailADDR1Control" )
    AddComponent( "NewReportApp::Frame2", "Content", 20, "NewReportApp::Frame2::LineControl" )
    AddComponent( "NewReportApp::Page", "Content", 1, "NewReportApp::Flow" )
    AddComponent( "NewReportApp::Page", "Content", 2, "NewReportApp::Page::DateTimeControl" )
    AddComponent( "NewReportApp::Report", "DataStream", 1, "NewReportApp::DataSource" )
    AddComponent( "NewReportApp::Report", "Content", 1, "NewReportApp::Benefits" )
    AddComponent( "NewReportApp::Report", "PageHeader", 1, "NewReportApp::Frame1" )
    AddComponent( "NewReportApp::Report", "Connection", 1, "NewReportApp::ODBCConnection" )
    AddComponent( "NewReportApp::SimplePageList", "PageStyle", 1, "NewReportApp::Page" )
End Sub

Class DBConnection Subclass Of AcODBCConnection

    Sub SetProperties( )
        Super::SetProperties( )
        ConfigKey = ""
        DataSource = "benefits"
        Password = "Benefits"
        UserName = "Benefits"
    End Sub
    
    '----------------------------------------
    ' User-defined methods
    '----------------------------------------
    
#Location "DBConnection%Connect"
    Function Connect( ) As Boolean
    'ConfigKey = NewReportApp::odbc
        Connect = Super::Connect( )
        ' Insert your code here
    End Function
#Pop

End Class


Class NewReportApp Subclass Of AcReport

    Class After_WithOperName Subclass Of AcFrame
    
        Sub SetProperties( )
            Super::SetProperties( )
            Size.Height = 540
            Size.Width = 12960
        End Sub
        
    End Class
    
    
    Class Benefits Subclass Of AcGroupSection
    
        Sub SetProperties( )
            Super::SetProperties( )
            ShowHeaderOnFirst = AsPageHeader
            KeyColumnName = "CC"
        End Sub
        
        '----------------------------------------
        ' Property methods
        '----------------------------------------
        
        Function PageBreakAfter( ) As Boolean
            PageBreakAfter = True
        End Function
        
        Function PageBreakBefore( ) As Boolean
            PageBreakBefore = False
        End Function
        
        Sub SetTocEntry( row As AcDataRow )
#Location "NewReportApp::Benefits!TocValueExp"
            TocEntry = "Account: " + GetKeyString( )
#Pop
        End Sub
        
    End Class
    
    
    Class Cost_Center Subclass Of AcGroupSection
    
        Sub SetProperties( )
            Super::SetProperties( )
            KeyColumnName = "OperNumber"
        End Sub
        
        Sub SetTocEntry( row As AcDataRow )
#Location "NewReportApp::Cost_Center!TocValueExp"
            TocEntry = "Cost Center: " + GetKeyString( )
#Pop
        End Sub
        
    End Class
    
    
    Class DataRow Subclass Of AcDataRow
    
        Dim CC As Integer
        Dim CCName As String
        Dim Description As String
        Dim EEGrpDesc As String
        Dim EMail As String
        Dim Fax As String
        Dim FullName As String
        Dim NC As String
        Dim Pernr As Integer
        Dim Phone As String
        Dim UnitMail_ADDR1 As String
        Dim UnitMail_City As String
        Dim UnitMail_Zip As String

        Sub BuildBinding( )
        End Sub
        
    End Class
    
    
    Class DataSource Subclass Of AcDatabaseSource
    
        '----------------------------------------
        ' User-defined methods
        '----------------------------------------
        
#Location "NewReportApp::DataSource%BindDataRow%AcDBCursor"
        Sub BindDataRow( cursor As AcDBCursor )
        
        	Cursor.BindColumn(1, "NewReportApp::DataRow", "CC")
        	Cursor.BindColumn(2, "NewReportApp::DataRow", "CCName")
        	Cursor.BindColumn(3, "NewReportApp::DataRow", "UnitMail_ADDR1")
        	Cursor.BindColumn(4, "NewReportApp::DataRow", "UnitMail_City")
        	Cursor.BindColumn(5, "NewReportApp::DataRow", "NC")
        	Cursor.BindColumn(6, "NewReportApp::DataRow", "UnitMail_Zip")
        	Cursor.BindColumn(7, "NewReportApp::DataRow", "Fax")
        	Cursor.BindColumn(8, "NewReportApp::DataRow", "Phone")
        	Cursor.BindColumn(9, "NewReportApp::DataRow", "Email")
        	Cursor.BindColumn(10, "NewReportApp::DataRow", "FullName")
        	Cursor.BindColumn(11, "NewReportApp::DataRow", "Pernr")
        	Cursor.BindColumn(12, "NewReportApp::DataRow", "EEGrpDesc")
        	Cursor.BindColumn(13, "NewReportApp::DataRow", "Description")
          
        End Sub
        
#Pop

#Location "NewReportApp::DataSource%Finish"
        Sub Finish( )
            ' Super::Finish( )
            ' Insert your code here
        
            set cursor = nothing
        
            AcDataSource::Finish()
        
        End Sub
#Pop

#Location "NewReportApp::DataSource%Start"
        Function Start( ) As Boolean
            Start = Super::Start( )
        
        	Dim oCon as AcODBCConnection
        	Dim oStmt as AcDBStatement
        	Dim sCmd as string
        
        	'Get the connection
        
        	set oCon = getConnection()
        	
        	'Frame the sql string to execute the storedprocedure
        
        	sCmd = "call usp_Get_Benefits_Roster;1 (:as_CC)"
        
        	set oStmt = new AcDbStatement(oCon)
        	oStmt.Prepare(sCmd)
        	set Cursor = oStmt.AllocateCursor()
        
        	Cursor.DefineProcedureInputParameter("as_CC", as_NationalAccountNumber ) 
        
        	'Open the cursor, in case of error show the error message
        	if not Cursor.OpenCursor() then
        		msgbox oCon.getGeneralErrorText()
        	else
        
        	end if
        
        	'Call BindDataRow to bind the variables of the datarow to the fields of the resultset from the storedprocedure
        	BindDataRow(Cursor)
        	
        End Function
#Pop

    End Class
    
    
    Class DBConnection1 Subclass Of AcODBCConnection
    
        Sub SetProperties( )
            Super::SetProperties( )
            ConnectionString = "asd"
            DataSource = "DEV_NAR"
            Password = "report4na"
            UserName = "NA_ReportUser"
        End Sub
        
    End Class
    
    
    Class Flow Subclass Of AcTopDownFlow
    
        Sub SetProperties( )
            Super::SetProperties( )
            Border.Pen = SingleLine
            Position.X = 540
            Position.Y = 540
            Size.Height = 14400
            Size.Width = 10980
        End Sub
        
    End Class
    
    
    Class Frame Subclass Of AcFrame
    
        Class DescriptionControl Subclass Of AcTextControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Position.X = 5760
                Position.Y = 8
                Size.Height = 216
                Size.Width = 2232
                TextPlacement.Horizontal = TextAlignCenter
                TextPlacement.Vertical = TextAlignMiddle
            End Sub
            
            ' ValueExp: [Description]
            
            Sub SetValue( row As AcDataRow )
                Dim theRow As NewReportApp::DataRow
                Set theRow = row
#Location "NewReportApp::Frame::DescriptionControl!ValueExp"
                DataValue = theRow.Description
#Pop
            End Sub
            
        End Class
        
        
        Class EEGrpDescControl Subclass Of AcTextControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Position.X = 8820
                Position.Y = 8
                Size.Height = 216
                Size.Width = 1800
                TextPlacement.Horizontal = TextAlignCenter
                TextPlacement.Vertical = TextAlignMiddle
            End Sub
            
            ' ValueExp: [EEGrpDesc]
            
            Sub SetValue( row As AcDataRow )
                Dim theRow As NewReportApp::DataRow
                Set theRow = row
#Location "NewReportApp::Frame::EEGrpDescControl!ValueExp"
                DataValue = theRow.EEGrpDesc
#Pop
            End Sub
            
        End Class
        
        
        Class FullNameControl Subclass Of AcTextControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Position.X = 360
                Position.Y = 8
                Size.Height = 216
                Size.Width = 3268
                TextPlacement.Vertical = TextAlignMiddle
            End Sub
            
            ' ValueExp: [FullName]
            
            Sub SetValue( row As AcDataRow )
                Dim theRow As NewReportApp::DataRow
                Set theRow = row
#Location "NewReportApp::Frame::FullNameControl!ValueExp"
                DataValue = theRow.FullName
#Pop
            End Sub
            
        End Class
        
        
        Class PernrControl Subclass Of AcIntegerControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Position.X = 4320
                Position.Y = 8
                Size.Height = 216
                Size.Width = 720
                TextPlacement.Vertical = TextAlignMiddle
            End Sub
            
            ' ValueExp: [Pernr]
            
            Sub SetValue( row As AcDataRow )
                Dim theRow As NewReportApp::DataRow
                Set theRow = row
#Location "NewReportApp::Frame::PernrControl!ValueExp"
                DataValue = theRow.Pernr
#Pop
            End Sub
            
        End Class
        
        
        Class RectangleControl Subclass Of AcRectangleControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Position.X = 0
                Position.Y = 0
                Size.Height = 274
                Size.Width = 10980
            End Sub
            
        End Class
        
        
        Sub SetProperties( )
            Super::SetProperties( )
            Border.Pen = NullLine
            Position.X = 0
            Position.Y = 360
            Size.Height = 274
            Size.Width = 10980
        End Sub
        
    End Class
    
    
    Class Frame1 Subclass Of AcFrame
    
        Class FullNameControl Subclass Of AcTextControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Font.Underline = False
                Position.X = 292
                Position.Y = 2366
                Size.Height = 258
                Size.Width = 860
            End Sub
            
            ' ValueExp: [CC]
            
            Sub SetValue( row As AcDataRow )
                Dim theRow As NewReportApp::DataRow
                Set theRow = row
#Location "NewReportApp::Frame1::FullNameControl!ValueExp"
                DataValue = theRow.CC
#Pop
            End Sub
            
        End Class
        
        
        Class FullNameControl1 Subclass Of AcTextControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Font.Underline = False
                Position.X = 1684
                Position.Y = 2350
                Size.Height = 258
                Size.Width = 5160
            End Sub
            
            ' ValueExp: [CCName]
            
            Sub SetValue( row As AcDataRow )
                Dim theRow As NewReportApp::DataRow
                Set theRow = row
#Location "NewReportApp::Frame1::FullNameControl1!ValueExp"
                DataValue = theRow.CCName
#Pop
            End Sub
            
        End Class
        
        
        Class LabelControl10 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 278
                Position.Y = 2084
                Size.Height = 245
                Size.Width = 900
                Text = "Unit"
            End Sub
            
        End Class
        
        
        Class LabelControl11 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 1670
                Position.Y = 2097
                Size.Height = 245
                Size.Width = 2748
                Text = "Unit Name"
            End Sub
            
        End Class
        
        
        Class LabelControl2 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 12
                Position.X = 180
                Position.Y = 180
                Size.Height = 430
                Size.Width = 10620
                Text = "2006 Anual Enrollment Full Time Hourly and Full Time Salaried Benefits Roster"
            End Sub
            
        End Class
        
        
        Class LabelControl3 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 180
                Position.Y = 707
                Size.Height = 258
                Size.Width = 2236
                Text = "TO: Unit Manager"
            End Sub
            
        End Class
        
        
        Class LabelControl4 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Position.X = 180
                Position.Y = 965
                Size.Height = 948
                Size.Width = 10620
                Text = "The following active full time Associates are eligible for 2006 benefits. If an Associate is not listed below and they are full time or if any Unit or Associate information below is incorrect or blank, please contact the Payroll department to update our records. If you have benefits questions, please conact the Benefits Answerline at 800-341-7763 or email us at benefits.department@compass-usa.com."
                TextPlacement.MultiLine = True
            End Sub
            
        End Class
        
        
        Class LineControl4 Subclass Of AcLineControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                EndPosition.X = 10980
                EndPosition.Y = 1980
                LineStyle.Width = 43
                Position.X = 0
                Position.Y = 1980
            End Sub
            
        End Class
        
        
        Class LineControl5 Subclass Of AcLineControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                EndPosition.X = 10800
                EndPosition.Y = 2327
                Position.X = 180
                Position.Y = 2327
            End Sub
            
        End Class
        
        
        Class RectangleControl Subclass Of AcRectangleControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                LineStyle.Width = 14
                Position.X = 0
                Position.Y = 1980
                Size.Height = 634
                Size.Width = 10980
            End Sub
            
        End Class
        
        
        Sub SetProperties( )
            Super::SetProperties( )
            Position.X = 0
            Position.Y = 720
            Size.Height = 2624
            Size.Width = 10980
        End Sub
        
    End Class
    
    
    Class Frame2 Subclass Of AcFrame
    
        Class LabelControl Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 3240
                Position.Y = 113
                Size.Height = 216
                Size.Width = 1206
                Text = "Address:"
                TextPlacement.Horizontal = TextAlignRight
            End Sub
            
        End Class
        
        
        Class LabelControl1 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 3070
                Position.Y = 329
                Size.Height = 216
                Size.Width = 1376
                Text = "City/State/Zip:"
                TextPlacement.Horizontal = TextAlignRight
            End Sub
            
        End Class
        
        
        Class LabelControl12 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 306
                Position.Y = 104
                Size.Height = 216
                Size.Width = 2574
                Text = "Unit Mailing Address /"
                TextPlacement.Horizontal = TextAlignLeft
            End Sub
            
        End Class
        
        
        Class LabelControl2 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 3682
                Position.Y = 545
                Size.Height = 216
                Size.Width = 774
                Text = "Email:"
                TextPlacement.Horizontal = TextAlignRight
            End Sub
            
        End Class
        
        
        Class LabelControl3 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 8340
                Position.Y = 333
                Size.Height = 216
                Size.Width = 602
                Text = "Fax:"
                TextPlacement.Horizontal = TextAlignRight
            End Sub
            
        End Class
        
        
        Class LabelControl4 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 306
                Position.Y = 339
                Size.Height = 216
                Size.Width = 2064
                Text = "Contact Information"
                TextPlacement.Horizontal = TextAlignLeft
            End Sub
            
        End Class
        
        
        Class LabelControl5 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 322
                Position.Y = 896
                Size.Height = 230
                Size.Width = 2378
                Text = "Associate"
                TextPlacement.Horizontal = TextAlignLeft
            End Sub
            
        End Class
        
        
        Class LabelControl6 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 3960
                Position.Y = 903
                Size.Height = 216
                Size.Width = 1356
                Text = "Personnel#"
                TextPlacement.Horizontal = TextAlignCenter
            End Sub
            
        End Class
        
        
        Class LabelControl7 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 5760
                Position.Y = 903
                Size.Height = 216
                Size.Width = 2232
                Text = "Employee Subgroup"
                TextPlacement.Horizontal = TextAlignCenter
            End Sub
            
        End Class
        
        
        Class LabelControl8 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 8820
                Position.Y = 903
                Size.Height = 216
                Size.Width = 1800
                Text = "Employee Status"
            End Sub
            
        End Class
        
        
        Class LabelControl9 Subclass Of AcLabelControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Bold = True
                Font.Size = 10
                Position.X = 8226
                Position.Y = 549
                Size.Height = 216
                Size.Width = 720
                Text = "Phone:"
                TextPlacement.Horizontal = TextAlignRight
            End Sub
            
        End Class
        
        
        Class LineControl Subclass Of AcLineControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                EndPosition.X = 10980
                EndPosition.Y = 900
                LineStyle.Width = 43
                Position.X = 0
                Position.Y = 900
            End Sub
            
        End Class
        
        
        Class LineControl2 Subclass Of AcLineControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                EndPosition.X = 12814
                EndPosition.Y = 20812
                LineStyle.Width = 43
                Position.X = -720
                Position.Y = 20812
            End Sub
            
        End Class
        
        
        Class LineControl3 Subclass Of AcLineControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                EndPosition.X = 12814
                EndPosition.Y = 21227
                LineStyle.Width = 43
                Position.X = -720
                Position.Y = 21227
            End Sub
            
        End Class
        
        
        Class RectangleControl Subclass Of AcRectangleControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                LineStyle.Width = 14
                Position.X = 0
                Position.Y = 0
                Size.Height = 1138
                Size.Width = 10980
            End Sub
            
        End Class
        
        
        Class UnitMailADDR1Control Subclass Of AcTextControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Font.Underline = True
                Position.X = 4524
                Position.Y = 131
                Size.Height = 216
                Size.Width = 2216
            End Sub
            
            ' ValueExp: [UnitMail_ADDR1]
            
            Sub SetValue( row As AcDataRow )
                Dim theRow As NewReportApp::DataRow
                Set theRow = row
#Location "NewReportApp::Frame2::UnitMailADDR1Control!ValueExp"
                DataValue = theRow.UnitMail_ADDR1
#Pop
            End Sub
            
        End Class
        
        
        Class UnitMailADDR1Control1 Subclass Of AcTextControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Font.Underline = True
                Position.X = 4528
                Position.Y = 347
                Size.Height = 216
                Size.Width = 3082
            End Sub
            
            ' ValueExp: [UnitMail_City] & " , " & [NC] & " "  &  [UnitMail_Zip]
            
            Sub SetValue( row As AcDataRow )
                Dim theRow As NewReportApp::DataRow
                Set theRow = row
#Location "NewReportApp::Frame2::UnitMailADDR1Control1!ValueExp"
                DataValue = theRow.UnitMail_City & " , " & theRow.NC & " " & theRow.UnitMail_Zip
#Pop
            End Sub
            
        End Class
        
        
        Class UnitMailADDR1Control4 Subclass Of AcTextControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Font.Underline = True
                Position.X = 4528
                Position.Y = 563
                Size.Height = 216
                Size.Width = 3698
            End Sub
            
            ' ValueExp: [EMail]
            
            Sub SetValue( row As AcDataRow )
                Dim theRow As NewReportApp::DataRow
                Set theRow = row
#Location "NewReportApp::Frame2::UnitMailADDR1Control4!ValueExp"
                DataValue = theRow.EMail
#Pop
            End Sub
            
        End Class
        
        
        Class UnitMailADDR1Control5 Subclass Of AcTextControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Font.Underline = True
                Position.X = 9042
                Position.Y = 324
                Size.Height = 216
                Size.Width = 1634
            End Sub
            
            ' ValueExp: [Fax]
            
            Sub SetValue( row As AcDataRow )
                Dim theRow As NewReportApp::DataRow
                Set theRow = row
#Location "NewReportApp::Frame2::UnitMailADDR1Control5!ValueExp"
                DataValue = theRow.Fax
#Pop
            End Sub
            
        End Class
        
        
        Class UnitMailADDR1Control6 Subclass Of AcTextControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.Size = 10
                Font.Underline = True
                Position.X = 9042
                Position.Y = 540
                Size.Height = 216
                Size.Width = 1634
            End Sub
            
            ' ValueExp: [Phone]
            
            Sub SetValue( row As AcDataRow )
                Dim theRow As NewReportApp::DataRow
                Set theRow = row
#Location "NewReportApp::Frame2::UnitMailADDR1Control6!ValueExp"
                DataValue = theRow.Phone
#Pop
            End Sub
            
        End Class
        
        
        Sub SetProperties( )
            Super::SetProperties( )
            Position.X = 0
            Position.Y = 180
            Size.Height = 1152
            Size.Width = 10958
        End Sub
        
        '----------------------------------------
        ' Property methods
        '----------------------------------------
        
        Function PageBreakAfter( ) As Boolean
            PageBreakAfter = False
        End Function
        
    End Class
    
    
    Class ODBCConnection Subclass Of AcODBCConnection
    
        Sub SetProperties( )
            Super::SetProperties( )
            DataSource = "benefits"
            Password = "benefits"
            UserName = "benefits"
        End Sub
        
        '----------------------------------------
        ' User-defined methods
        '----------------------------------------
        
#Location "NewReportApp::ODBCConnection%Connect"
        Function Connect( ) As Boolean
        'ConfigKey = NewReportApp::odbc
            Connect = Super::Connect( )
            ' Insert your code here
        End Function
#Pop

    End Class
    
    
    Class Page Subclass Of AcPage
    
        Class DateTimeControl Subclass Of AcDateTimeControl
        
            Sub SetProperties( )
                Super::SetProperties( )
                Font.FaceName = "Arial"
                Font.Size = 8
                Position.X = 540
                Position.Y = 15120
                Size.Height = 286
                Size.Width = 3600
            End Sub
            
            '----------------------------------------
            ' Property methods
            '----------------------------------------
            
            Function Format( ) As String
                Format = "Long Date"
            End Function
            
            ' ValueExp: Now()
            
            Sub SetValue( row As AcDataRow )
#Location "NewReportApp::Page::DateTimeControl!ValueExp"
                DataValue = Now( )
#Pop
            End Sub
            
        End Class
        
        
        Sub SetProperties( )
            Super::SetProperties( )
            Size.Height = 15840
            Size.Width = 12420
        End Sub
        
        '----------------------------------------
        ' Property methods
        '----------------------------------------
        
        Function CanIncreaseHeight( ) As Boolean
            CanIncreaseHeight = False
        End Function
        
        Function CanIncreaseWidth( ) As Boolean
            CanIncreaseWidth = False
        End Function
        
        Function CanReduceHeight( ) As Boolean
            CanReduceHeight = False
        End Function
        
        Function CanReduceWidth( ) As Boolean
            CanReduceWidth = False
        End Function
        
    End Class
    
    
    Class Report Subclass Of AcReportSection
    
        Sub SetProperties( )
            Super::SetProperties( )
            ShowHeaderOnFirst = AsPageHeader
            Sorting = PreSorted
        End Sub
        
        '----------------------------------------
        ' Property methods
        '----------------------------------------
        
        Function TocAddComponent( ) As AcTOCNodeType
            TocAddComponent = TOCIfAllVisible
        End Function
        
        Sub SetSortKey( adapter As AcDataAdapter )
            adapter.AddSortKey( "CC", SortAscending )
        End Sub
        
        '----------------------------------------
        ' User-defined methods
        '----------------------------------------
        
#Location "NewReportApp::Report%NewPageHeader"
        Function NewPageHeader( ) As AcFrame
        
        	if UCASE(rtrim(ltrim(as_NationalAccountNumber))) = "NATL020000" then
        		Set NewPageHeader = New Persistent NewReportApp::Report_PageHeaderWithOperName
        	else
            	Set NewPageHeader = Super::NewPageHeader( )
        	end if
        
            ' Insert your code here
        End Function
#Pop

    End Class
    
    
    Class Report_PageHeaderWithOperName Subclass Of AcFrame
    
        Sub SetProperties( )
            Super::SetProperties( )
            Size.Height = 2340
            Size.Width = 12960
        End Sub
        
    End Class
    
    
    Class SimplePageList Subclass Of AcSimplePageList
    
        Sub SetProperties( )
            Super::SetProperties( )
            SplitOversizePagesWhenPrinting = False
        End Sub
        
    End Class
    
    
    Class WithOperName Subclass Of AcFrame
    
        Sub SetProperties( )
            Super::SetProperties( )
            Size.Height = 216
            Size.Width = 12960
        End Sub
        
    End Class
    
    
    '----------------------------------------
    ' Property methods
    '----------------------------------------
    
    Function TocAddContents( ) As Boolean
        TocAddContents = True
    End Function
    
End Class


' End of file.
