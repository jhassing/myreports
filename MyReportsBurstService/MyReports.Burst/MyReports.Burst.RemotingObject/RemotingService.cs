#region Namespace Imports

using System;
using System.Threading;
using MyReports.Burst.BurstProcess;
using MyReports.Burst.Import.FolderNames;
using MyReports.Burst.Import.Zrops;

#endregion

namespace MyReports.Burst.RemotingObject
{
    public class RemotingService : MarshalByRefObject
    {
        #region Private Variables

        private readonly Mutex _mutexActuateRunningCount = new Mutex();
        private readonly Mutex _mutexNonBurstActivePending = new Mutex();
        private readonly Mutex _mutexThreadCount = new Mutex();
        private readonly BurstServiceEntryclass _oEntry;
        private readonly FolderNamesImport _oFolderNamesImport;
        private readonly ZropsFileImport _oZropsFileImport;

        // Thread monitoring variables
        private bool _nonBurstActivityPending;

        // SAP File pickup variables
        private bool _wasFilePickupTurnedOn;

        // Folder Import pickup variabes

        private bool _wasFolderPickupTurnedOn;

        // Zrops ZropsFileImport pickup variables
        private bool _wasZropsFilePickupTurnedOn;

        #endregion

        public RemotingService()
        {
            RunningThreads = 0;
            RunningActuateReports = 0;

            // Get a handle to the burst process class
            _oEntry = new BurstServiceEntryclass();
            _oEntry.ThreadStarted += BurstClassThreadStarted;
            _oEntry.ThreadFinished += BurstClassThreadFinished;

            // Folder ZropsFileImport Class
            _oFolderNamesImport = new FolderNamesImport();
            _oFolderNamesImport.FileFound += FolderNamesImportFileFound;
            _oFolderNamesImport.FolderImportFinished += FolderNamesImportFolderImportFinished;

            // Zrops ZropsFileImport Class
            _oZropsFileImport = new ZropsFileImport();
            _oZropsFileImport.FileFound += ZropsFileImportFileFound;
            _oZropsFileImport.FileImportFinished += ZropsFileImportFileImportFinished;


        }

        #region Properties

        public bool IsSapFilePickupMonitorRunning { get; private set; }

        public bool IsFolderImportMonitorRunning { get; private set; }

        public bool IsZropsFilePickupTurnedOn { get; private set; }

        public int RunningThreads { get; set; }

        public int RunningActuateReports { get; set; }

        public bool Debug { get; set; }

        #endregion

        #region threading and actuate running counts

        private void ThreadRunningCountAdd()
        {
            _mutexThreadCount.WaitOne();

            RunningThreads++;

            if (Debug)
                Console.WriteLine("Thread Count is: " + RunningThreads);

            _mutexThreadCount.ReleaseMutex();
        }


        private void ThreadRunningCountSubtract()
        {
            _mutexThreadCount.WaitOne();

            RunningThreads--;

            if (Debug)
                Console.WriteLine("Thread Count is: " + RunningThreads);
            if (RunningThreads < 0)
            {
                Console.WriteLine("WARNING: negative value reached");
                RunningThreads = 0;
            }

            IsThreadsAndActuateZero();

            _mutexThreadCount.ReleaseMutex();
        }


        private void ActuateRunningCountAdd()
        {
            _mutexActuateRunningCount.WaitOne();      
      
            RunningActuateReports++;
            
            if (Debug)
                Console.WriteLine("Actuate Reports Count is: " + RunningActuateReports);
            
            _mutexActuateRunningCount.ReleaseMutex();
        }


        public void ActuateRunningCountSubtract()
        {
            _mutexActuateRunningCount.WaitOne();

            RunningActuateReports--;
            
            if (Debug)
                Console.WriteLine("Actuate Reports Count is: " + RunningActuateReports);

            if (RunningActuateReports < 0)
            {
                Console.WriteLine("WARNING: negative value reached");
                //m_nNumberOfActuateReports = 0;
            }

            IsThreadsAndActuateZero();

            //CheckFolderWaitingForImport();
            _mutexActuateRunningCount.ReleaseMutex();
        }

        public void ResetThreadAndActuateCountsToZero()
        {
            RunningActuateReports = 0;
            RunningThreads = 0;
            IsThreadsAndActuateZero();
        }

        /// <summary>
        /// Check to see if Threads and Actuate Running counts are zero.
        /// If so fire the event to import Folders and/or Zrops
        /// </summary>
        public void IsThreadsAndActuateZero()
        {
            if (RunningThreads == 0 && RunningActuateReports == 0)
            {
                // it is now safe to fire the event so long as the event is actually pointing to a method
                if (NonBurstEntryImportFileFoundEvent != null)
                {
                    NonBurstEntryImportFileFoundEvent();
                }
            }
        }

        #endregion threads and actuate running

        private event NonBurstEntryImportFileFound NonBurstEntryImportFileFoundEvent;

        public void StartSapFilePickup()
        {
            _oEntry.StartDirectoryMonitoring();
            IsSapFilePickupMonitorRunning = true;
        }


        public void StopSapFilePickup()
        {
            IsSapFilePickupMonitorRunning = false;
            _oEntry.StopDirectoryMonitoring();
        }


        private void BurstClassThreadStarted()
        {
            ThreadRunningCountAdd();
        }


        private void BurstClassThreadFinished(bool bReportSentToActuate)
        {
            if (bReportSentToActuate)
                ActuateRunningCountAdd();
            
            ThreadRunningCountSubtract();
        }


        public void StartFolderImportFilePickup()
        {
          
            _oFolderNamesImport.Start_FolderNamesImportMonitor();
            IsFolderImportMonitorRunning = true;
        }


        public void StopFolderImportFilePickup()
        {

            IsFolderImportMonitorRunning = false;
            _oFolderNamesImport.Stop_FolderNamesImportMonitor();
        }


        private void FolderNamesImportFileFound()
        {

            // Gather current state of objects
            _mutexNonBurstActivePending.WaitOne();
            GatherCurrentState();
            _nonBurstActivityPending = true;
            _mutexNonBurstActivePending.ReleaseMutex();

            // Stop all file import process
            StopSapFilePickup();
            StopFolderImportFilePickup();
            StopZropsImportFilePickup();

            NonBurstEntryImportFileFoundEvent += _oFolderNamesImport.ImportFolders;

            // remove this call
            //CheckFolderWaitingForImport();
            IsThreadsAndActuateZero();
        }

        private void FolderNamesImportFolderImportFinished()
        {
            _mutexNonBurstActivePending.WaitOne();
            _nonBurstActivityPending = false;
            _mutexNonBurstActivePending.ReleaseMutex();

            NonBurstEntryImportFileFoundEvent -= _oFolderNamesImport.ImportFolders;

            if (_wasFilePickupTurnedOn)
                StartSapFilePickup();

            if (_wasFolderPickupTurnedOn)
                StartFolderImportFilePickup();

            if (_wasZropsFilePickupTurnedOn)
                StartZropsImportFilePickup();
        }


        private void GatherCurrentState()
        {
            if (_nonBurstActivityPending) return;

            _wasFilePickupTurnedOn = IsSapFilePickupMonitorRunning;
            _wasFolderPickupTurnedOn = IsFolderImportMonitorRunning;
            _wasZropsFilePickupTurnedOn = IsZropsFilePickupTurnedOn;
        }


        public override object InitializeLifetimeService()
        {
            return (null);
        }

        #region ZropsImport event methods

        private void ZropsFileImportFileFound()
        {
            _mutexNonBurstActivePending.WaitOne();
            GatherCurrentState();
            _nonBurstActivityPending = true;
            _mutexNonBurstActivePending.ReleaseMutex();

            //pause the file finders
            StopSapFilePickup();
            StopFolderImportFilePickup();
            StopZropsImportFilePickup();

            //add import the zrops file to the event delegate
            NonBurstEntryImportFileFoundEvent += _oZropsFileImport.ImportZropsFile;

            //check to see if threads and actuate running is zero, if so wait until they are zero
            IsThreadsAndActuateZero();
        }

        private void ZropsFileImportFileImportFinished()
        {
            _mutexNonBurstActivePending.WaitOne();
            _nonBurstActivityPending = false;
            _mutexNonBurstActivePending.ReleaseMutex();

            NonBurstEntryImportFileFoundEvent -= _oZropsFileImport.ImportZropsFile;

            if (_wasZropsFilePickupTurnedOn)
                StartZropsImportFilePickup();

            if (_wasFolderPickupTurnedOn)
                StartFolderImportFilePickup();

            if (_wasFilePickupTurnedOn)
                StartSapFilePickup();
        }

        public void StartZropsImportFilePickup()
        {
            IsZropsFilePickupTurnedOn = true;
            _oZropsFileImport.Start_ZropsFileImportMonitor();
        }

        public void StopZropsImportFilePickup()
        {
            IsZropsFilePickupTurnedOn = false;
            _oZropsFileImport.Stop_ZropsFileImportMonitor();
        }

        #endregion

        #region remote administration methods

        public void ScrubArchivedSapFiles()
        {
            SAP_Files files = new SAP_Files();
            files.DeleteOldFiles();
        }

        public void ScrubArchivedSapFiles(int age)
        {
            SAP_Files files = new SAP_Files();
            files.DeleteOldFiles(age);
        }

        #region Get Config Values from Application config file

        //public string GetConfigValue(string node, string key)
        //{
        //    return AppSettingsReader.ReadKeyValue(ReadRegistry.InstallDirectory, node, key);
        //}


    

        #endregion

        #endregion

        #region Nested type: NonBurstEntryImportFileFound

        /// <summary>
        /// This delegate is used to import non-SAP reports, i.e. Folder and Zrops data imports.
        /// Since these imports may adversely affect BurstEntries at runtime the import may be run 
        /// only when the BurstServiceEntryclass imports are paused and the thread count is zero.
        /// In such a case an event will occur which will fire all methods in the delegate.
        /// </summary>
        private delegate void NonBurstEntryImportFileFound();

        #endregion



    } // Class
} // Namespace