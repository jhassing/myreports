using System;
using System.IO;
using Microsoft.Win32;

namespace MyReports.Burst.Registry
{
    public class ReadRegistry
    {
        public static string InstallDirectory
        {
            get
            {
                string path = "";

                try
                {

                    String REG_KEY;

                    if (Environment.Is64BitOperatingSystem)
                        REG_KEY = @"SOFTWARE\Wow6432Node\Compass-USA\MyReportsBurst";
                    else
                        REG_KEY = @"SOFTWARE\Compass-USA\MyReportsBurst";

                    RegistryKey rk1 = null;

                    // Setup the registry values we need to get
                    RegistryKey rk = Microsoft.Win32.Registry.LocalMachine;
                    rk1 = rk.OpenSubKey(REG_KEY, false);

                    // Get the install path of the Burst Service
                    path = rk1.GetValue("InstallLocation", "").ToString();

                    if (!path.EndsWith(@"\"))
                        path = path + @"\";

                    // Find the 1st (and should be only) config file
                    if (!Directory.Exists(path))
                        throw new ApplicationException("The directory, " + path + "does not exist");

                    string[] files = Directory.GetFiles(path, "*.config");

                    if (files.Length > 0)
                    {
                        path = files[0];

                        if (!File.Exists(path))
                            throw new ApplicationException("Config figuration file cannot be found. Path: " + path);
                    }
                    else
                        throw new ApplicationException("Config figuration file cannot be found. Path: " + path);

                    return (path);
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
            }
        }
    }
}


