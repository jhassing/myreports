
using System.Collections;

namespace MyReports.Burst.ReportPermissions
{
    interface IPermissions
    {
        ArrayList GetFolderPermissionGroups(string sTildyLine);
    }
}
