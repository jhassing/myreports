using System;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;

namespace MyReports.Burst.ReportPermissions
{
    /// <summary>
    /// Summary description for PayrollPermissions.
    /// This class will get the various permissions based on business logic for both Payroll and Stub reports for myReports phase 2 distribution.
    /// </summary>
    public class PayrollPermissions : BasePermissions, IPermissions
    {
        protected override void GetGroupNames(string sLine)
        {
            string salaryGroupExt = ConfigurationReader.RdSettings.GroupPermissionProperties.PayrollSalary;
            string hourlyGroupExt = ConfigurationReader.RdSettings.GroupPermissionProperties.PayrollHourly;
            
            try
            {

                // First, get the Dm group setings if available
                if (m_sDMCode.Length > 0)
                    m_sDMCodeGroup = m_sDMCode + salaryGroupExt;

                string[] pipeSplit = sLine.Split('|');

                Enum_PayTypes nPayType = (Enum_PayTypes)System.Convert.ToInt32(pipeSplit[3]);

                if (nPayType == Enum_PayTypes.Hourly)
                {
                    // if this is an Hourly employee, they cannot see ANY salary report.. 
                    if (m_sUnitCode.Length > 0)
                    {
                        m_sUnitCodeSalaryGroup = m_sUnitCode + salaryGroupExt;
                        m_sUnitCodeHourlyGroup = m_sUnitCode + hourlyGroupExt;
                    }
                }
                else if (nPayType == Enum_PayTypes.Salary)
                {
                    if (m_sUnitCode.Length > 0)
                    {
                        m_sUnitCodeSalaryGroup = m_sUnitCode + salaryGroupExt;
                        m_sUnitCodeHourlyGroup = "";
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }
    }
}
