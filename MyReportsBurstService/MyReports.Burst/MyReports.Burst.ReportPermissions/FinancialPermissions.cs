using System.Collections;
using MyReports.Burst.Common.Configuration.Helper;

namespace MyReports.Burst.ReportPermissions
{
    public class FinancialPermissions : BasePermissions, IPermissions
    {
        public override ArrayList GetFolderPermissionGroups(string sTildyLine)
        {

            ArrayList aryGroupArray = new ArrayList();

            ParseTildyInformation(sTildyLine);

            if (m_sUnitCodeGroup.Length > 0)
                aryGroupArray.Add(m_sUnitCodeGroup);

            if (m_sDMCodeGroup.Length > 0)
                aryGroupArray.Add(m_sDMCodeGroup);

            if (m_sRegionCodeGroup.Length > 0)
                aryGroupArray.Add(m_sRegionCodeGroup);

            return (aryGroupArray);
        }

        protected override void GetGroupNames(string sLine)
        {
            string csGroupExt = ConfigurationReader.RdSettings.GroupPermissionProperties.FinancialReports;

            m_sUnitCodeGroup = string.IsNullOrEmpty(m_sUnitCode) ? "" : m_sUnitCode + csGroupExt;
            m_sRegionCodeGroup = string.IsNullOrEmpty(m_sRegionCode) ? "" : m_sRegionCode + csGroupExt;
            m_sDMCodeGroup = string.IsNullOrEmpty(m_sDMCode) ? "" : m_sDMCode + csGroupExt;
        }
    }
}
