#region Namespace Imports

using System;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;

#endregion

namespace MyReports.Burst.ReportPermissions
{
    /// <summary>
    /// Summary description for PayrollPermissions.
    /// This class will get the various permissions based on business logic for both Payroll and Stub reports for myReports phase 2 distribution.
    /// </summary>
    public class ProfilePermissions : BasePermissions, IPermissions
    {
        protected override void GetGroupNames(string sLine)
        {
            string salaryGroupExt = ConfigurationReader.RdSettings.GroupPermissionProperties.PayrollSalary;
            string hourlyGroupExt = ConfigurationReader.RdSettings.GroupPermissionProperties.PayrollHourly;
            
            try
            {               
                m_sDMCodeGroup = m_sDMCode == string.Empty ? "" : m_sDMCode + salaryGroupExt;

                string[] pipeSplit = sLine.Split('|');

                Enum_PayTypes nPayType = (Enum_PayTypes) Convert.ToInt32(pipeSplit[3]);

                if (nPayType == Enum_PayTypes.Hourly)
                {
                    // Hourly report can be seen by hourly and salary.
                    if (m_sUnitCode.Length > 0)
                    {
                        m_sUnitCodeSalaryGroup = m_sUnitCode + salaryGroupExt;
                        m_sUnitCodeHourlyGroup = m_sUnitCode + hourlyGroupExt;
                    }
                }
                else if (nPayType == Enum_PayTypes.Salary)
                {
                    // Salary report cannot be seen by hourly or salery.. Just the Dm
                    // SO, do nothing here
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}