using System.Collections;

namespace MyReports.Burst.ReportPermissions
{
    public class BasePermissions
    {
        #region	Protected Class Variables

        // Values inside the tildy header
        protected string m_sUnitCode = "";
        protected string m_sDMCode = "";
        protected string m_sRegionCode = "";

        // Group assigments bases from AD
        protected string m_sDMCodeGroup = "";
        protected string m_sUnitCodeGroup = "";
        protected string m_sRegionCodeGroup = "";
        protected string m_sUnitCodeSalaryGroup = "";
        protected string m_sUnitCodeHourlyGroup = "";

        #endregion

        protected void ParseTildyInformation(string sTildyLine)
        {
            string[] split = sTildyLine.Split('~');

            m_sUnitCode = split[2];
            m_sRegionCode = split[3];
            m_sDMCode = split[4];

            GetGroupNames(sTildyLine);
        }


        public virtual ArrayList GetFolderPermissionGroups(string sTildyLine)
        {
            ParseTildyInformation(sTildyLine);

            ArrayList aryGroupArray = new ArrayList();

            if (m_sUnitCodeSalaryGroup.Length > 0)
                aryGroupArray.Add(m_sUnitCodeSalaryGroup);

            if (m_sUnitCodeHourlyGroup.Length > 0)
                aryGroupArray.Add(m_sUnitCodeHourlyGroup);

            if (m_sDMCodeGroup.Length > 0)
                aryGroupArray.Add(m_sDMCodeGroup);

            aryGroupArray.Add("MyPayAdmin");

            return (aryGroupArray);
        }

        protected virtual void GetGroupNames(string sLine)
        {
            // leave empty. must be implimented by caller
        }
    }
}
