using System.Collections;
using System.IO;
using MyReports.Burst.Common;

namespace MyReports.Burst.RunReport
{

    public class RunActuateReport : ActuateFileFolderFunctions
    {

        public RunActuateReport(string sMasterFileListLocation)
        {
            // Step 1:  Log Into Actuate
            LoginActuate();

            ExecuteReport(sMasterFileListLocation);
        }

        private void ExecuteReport(string sMasterFileListLocation)
        {
            const string reportName = "/SAP.rox";
            const string headLine = "This is a SAP Burst Report";

            string jobName = GetReportCodeFromFile(sMasterFileListLocation);

            Hashtable htParms = new Hashtable { { "pMasterFileList", sMasterFileListLocation } };

            ExecuteReport(reportName, htParms, jobName, headLine);

            Dispose();

        }


        private static string GetReportCodeFromFile(string sMasterFileListLocation)
        {

            using (StreamReader sr = new StreamReader(sMasterFileListLocation))
            {
                string sLine = sr.ReadLine();

                if (sLine != null)
                    if (sLine.Length > 0)
                    {
                        string[] sTemp = sLine.Split('\\');

                        // The 2nd line is always the Actual Report Name					
                        string sReportCode = Functions.GetReportCodeFromReportName(sTemp[sTemp.Length - 1]);
                        string[] sTemp1 = sReportCode.Split('|');

                        return sTemp1[0];
                    }

                sr.ReadToEnd();
                sr.Close();
            }

            return "";
        }


    }
}
