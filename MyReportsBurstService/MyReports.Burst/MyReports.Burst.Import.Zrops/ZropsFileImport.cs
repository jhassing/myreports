using System;
using System.Collections.Generic;
using Compass.FileSystem.ExtendedFileMonitor;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Common.Structs;

namespace MyReports.Burst.Import.Zrops
{
    // these delegates are the for file found and import finished events
    public delegate void ZropsFileFound();
    public delegate void ZropsFileImportFinish();

    public class ZropsFileImport
    {
        /// <summary>
        /// This class will be used to monitor a specific directory for any new Zrops import files.
        /// Once a new file is found, an event is raised, and the threading process for reports will 
        /// be paused.  When the thread count is down to 0 for BurstService Entries and Folder Imports 
        /// the file is then imported into the ZROPS table in SQL Server.  Once this process is 
        /// completed the threading process will be resumed.
        /// </summary>

        #region Private member variables

        // Directory monitor settings
        public static string m_sFTPDirectory = "";
        public static string m_sFileExtToLookFor = "";
        private string m_sZropsTempImportDir = "";
        private string m_sZropsDataArchiveDir = "";

        // Directory Monitor
        private ExtendedFileMonitorClass oFileMonitor;

        // Class Events
        public event ZropsFileFound FileFound;
        public event ZropsFileImportFinish FileImportFinished;

        private string m_sPath = "";

        #endregion

        #region ~ctor
        public ZropsFileImport()
        {
            // Get Directory Monitoring values
            m_sFTPDirectory = ConfigurationReader.RdSettings.BurstProperties.FtpDirectory;
            m_sFileExtToLookFor = ConfigurationReader.RdSettings.ZropsProperties.ZropsFileExtToLookFor;
            m_sZropsTempImportDir = ConfigurationReader.RdSettings.ZropsProperties.ZropsTempImportDir;
            m_sZropsDataArchiveDir = ConfigurationReader.RdSettings.ZropsProperties.ZropsDataArchiveDir;
            
            //configure the file monitor to look for Zrops files in the ftp directory
            oFileMonitor = new ExtendedFileMonitorClass(m_sFTPDirectory);
            oFileMonitor.FileFound += new FileMonitorEventHandler(oFileMonitor_FileFound);
        }
        #endregion

        #region Start and stop the File Monitor

        public void Start_ZropsFileImportMonitor()
        {
            m_sPath = "";
            oFileMonitor.StartDiretoryMonitoring();
        }

        public void Stop_ZropsFileImportMonitor()
        {
            oFileMonitor.StopDirectoryMonitoring();
        }

        #endregion

        //This method is run via the ZropsFileFound event when the file monitor finds a .zrops handshake file
        private void oFileMonitor_FileFound(string sPath)
        {
            if (sPath.ToLower().EndsWith(m_sFileExtToLookFor.ToLower()))
            {
                m_sPath = sPath;

                // Raise the event back to the Remoting Object. He will tell us when it is clear to start the import.
                if (FileFound != null)
                    FileFound();
            }
        }

        #region Import Zrops entries from file

        public void ImportZropsFile()
        {
            string sZropsTempPName = "";
            string sZropsArchPName = "";
            string sZropsTempImportDir = "";

            try
            {
                // Move the file to a temp location for importing, this also deletes the .zrops handshake file

                Console.WriteLine("Import started for: " + m_sPath);
                Functions.MoveFile(m_sPath, m_sZropsTempImportDir, out sZropsTempPName, out sZropsTempImportDir);

                try
                {
                    // Backup ZROPS table
                    new SqlServerDal.Zrops(ConfigurationReader.Db).BackupToTable();
                }
                catch (Exception ex)
                {
                    // if the backup fails, do not continue.
                    // WHAT TO DO??
                    throw ex;
                }

                try
                {
                    // Import the data table
                    ImportZropsData(sZropsTempPName);
                    Console.WriteLine("Import complete for: " + m_sPath);
                }
                catch (Exception ex)
                {
                    // failed to do import, restore data before throwing error
                    this.RestoreFromBackupTable();
                    throw ex;
                }

                // when import is complete fire the FileImportFinished event
                if (FileImportFinished != null)
                {
                    FileImportFinished();
                }
            }
            catch (Exception ex)
            {
                // ???????
                throw (ex);
            }
        }

        #endregion

        #region Import Zrops from SAP data file

        private void ImportZropsData(string zropsDataFilePname)
        {
            // Read ZROPS from SAP data file and place it in an array of strings
            string[] zropsData = System.IO.File.ReadAllLines(zropsDataFilePname);

            // create ArrayList to hold NewZropsRecord objects
            List<Hierarchy> m_aryZrops = new List<Hierarchy>();

            // split the row on ~ character and add the Unit, Dm and Region into the ArrayList
            foreach (string row in zropsData)
            {
                string[] currentRow = row.Split('~');

                // add each line as a NewZropsRecord object to the arraylist
                m_aryZrops.Add(new Hierarchy
                                   {
                                       Region = currentRow[1],
                                       Unit = currentRow[3],
                                       Dm = currentRow[2],
                                       Division = "",
                                       Sector = ""
                                   });
            }

            // insert the records held the ArrayList into the database
            new SqlServerDal.Zrops(ConfigurationReader.Db).Insert(m_aryZrops);
        }

        #endregion

        #region Backup and clear Zrops table

        public void BackupToTable()
        {
            #region code

            try
            {
                //new ReportDistribution.SQLServerDAL.Zrops(base._connectionString).BackupToTable();
                new SqlServerDal.Zrops(ConfigurationReader.Db).BackupToTable();
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            #endregion
        }
        #endregion

        #region Restore Zrops Table From Backup
        public void RestoreFromBackupTable()
        {
            #region code

            try
            {
                new SqlServerDal.Zrops(ConfigurationReader.Db).RestoreFromBackupTable();
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            #endregion
        }
        #endregion
    }
}
