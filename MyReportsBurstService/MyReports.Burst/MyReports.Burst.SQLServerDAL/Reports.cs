#region Namespace Imports

using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Burst.Common;
using MyReports.Burst.SqlServerDal.Base;

#endregion

namespace MyReports.Burst.SqlServerDal
{
    public class Reports : DalBase
    {
        private ArrayList _aryAdminGroups;
        private bool _hasLoadedAdminGroups;

        #region ~ctor

        public Reports(Database db)
            : base(db)
        {
        }

        #endregion

        /// <summary>
        /// Function will check SQL Server to find if this report code is in there.
        /// Return: True of False, depending on the existance of a report code
        /// </summary>
        /// <param name="reportName">Report name, which we will parse for the report code</param>
        /// <returns>True False depending if the report code exists in the database</returns>
        public bool DoesReportExist(string reportName)
        {
            const string sqlCommand = "usp_CheckReportCode";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            string reportCode = Functions.GetReportCodeFromReportName(reportName);

            _db.AddInParameter(dbCommand, "as_ReportCode", DbType.String, reportCode);
            _db.AddOutParameter(dbCommand, "as_ReturnValue", DbType.Int32, 1);

            _db.ExecuteNonQuery(dbCommand);

            return Convert.ToBoolean(_db.GetParameterValue(dbCommand, "as_ReturnValue"));
        }

        /// <summary>
        /// Get which type, Financial, Payroll, ect report this is.
        /// </summary>
        /// <param name="reportName">Report name to search for.</param>
        /// <returns></returns>
        public Enum_ReportTypes GetReportType(string reportName)
        {
            const string sqlCommand = "usp_Get_ReportType";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            string reportCode = Functions.GetReportCodeFromReportName(reportName);

            _db.AddInParameter(dbCommand, "ReportCode", DbType.String, reportCode);
            _db.AddOutParameter(dbCommand, "ReportTypeID", DbType.Int32, 5);

            _db.ExecuteNonQuery(dbCommand);          

            return (_db.GetParameterValue(dbCommand, "ReportTypeID") == DBNull.Value ? 
                Enum_ReportTypes.None  : 
                (Enum_ReportTypes)_db.GetParameterValue(dbCommand, "ReportTypeID"))
            ;
        }
        
        public DataSet GetReportArchiveValues()
        {
            const string sqlCommand = "		SELECT  	*	FROM  dbo.Report WITH (NOLOCK) " +
	                                    "LEFT OUTER JOIN " +
			                                    "dbo.ReportType ON dbo.Report.ReportTypeID = dbo.ReportType.ReportTypeID " +
	                                    "LEFT OUTER JOIN " +
	                                            "dbo.OverwriteValue ON dbo.Report.OverwriteValueID = dbo.OverwriteValue.OverwriteValueID " +
	                                    "LEFT OUTER JOIN " +
	                                            "dbo.ArchiveValue ON dbo.Report.ArchiveValueID = dbo.ArchiveValue.ArchiveValueID ";

            return _db.ExecuteDataSet(CommandType.Text, sqlCommand);
        }

        public void GetReportArchiveValues(string reportCode, out enum_OverwriteValues overwriteCode,
                                           out int archiveLength, out int reportTypeId,
                                           out enum_ArchiveTimeUnits archiveTimeUnits)
        {
            const string sqlCommand = "usp_Get_ReportArchiveValues";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_ReportCode", DbType.String, reportCode);
            _db.AddOutParameter(dbCommand, "ai_OverwriteCode", DbType.Int32, 5);
            _db.AddOutParameter(dbCommand, "ai_ArchiveLength", DbType.Int32, 5);
            _db.AddOutParameter(dbCommand, "ai_ReportTypeID", DbType.Int32, 5);
            _db.AddOutParameter(dbCommand, "ai_ArchiveValueCode", DbType.Int32, 5);

            _db.ExecuteNonQuery(dbCommand);

            overwriteCode = (enum_OverwriteValues) _db.GetParameterValue(dbCommand, "ai_OverwriteCode");
            archiveLength = (int) _db.GetParameterValue(dbCommand, "ai_ArchiveLength");
            reportTypeId = (int) _db.GetParameterValue(dbCommand, "ai_ReportTypeID");
            archiveTimeUnits = (enum_ArchiveTimeUnits) _db.GetParameterValue(dbCommand, "ai_ArchiveValueCode");
        }

        public DataSet GetValidReports()
        {
            return GetValidReports(-1);
        }

        public DataSet GetValidReports(int reportType)
        {
            const string sqlCommand = "usp_Get_ValidReports";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_ReportTypeID", DbType.Int32, reportType);

            return _db.ExecuteDataSet(dbCommand);
        }

        public DataSet GetViewedReports(DateTime startDate, DateTime endDate, string reportType, string costCenterList)
        {
            //todo Add parameters for report (Type[Sub,AR], OrganizationalUnit[Rgn,Dist,CC[]])	

            const string sqlCommand = "usp_Get_ViewedReports";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ad_StartDate", DbType.DateTime, startDate);
            _db.AddInParameter(dbCommand, "ad_EndDate", DbType.DateTime, endDate);
            _db.AddInParameter(dbCommand, "as_ReportType", DbType.String, reportType);
            _db.AddInParameter(dbCommand, "as_CostCenters", DbType.String, costCenterList);

            return _db.ExecuteDataSet(dbCommand);
        }

        public bool InsertReportHasBeenViewed(string userName, string reportName, DateTime creationTime)
        {
            const string sqlCommand = "usp_Insert_ViewedReportDate";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            string reportCode = Functions.GetReportCodeFromReportName(reportName);

            _db.AddInParameter(dbCommand, "as_UserName", DbType.String, userName);
            _db.AddInParameter(dbCommand, "as_ReportName", DbType.String, reportName);
            _db.AddInParameter(dbCommand, "ad_CreationTime", DbType.DateTime, creationTime);
            _db.AddInParameter(dbCommand, "ad_ViewedDate", DbType.DateTime, DateTime.Now);
            _db.AddInParameter(dbCommand, "as_ReportCode", DbType.String, reportCode);

            return DoTransaction(dbCommand);
        }

        public bool HasReportEverBeenViewed(string userName, string reportName, string creationTime,
                                             out DateTime viewedDate)
        {
            const string sqlCommand = "usp_Get_ReportViewedList";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_UserName", DbType.String, userName);
            _db.AddInParameter(dbCommand, "as_ReportName", DbType.String, reportName);
            _db.AddInParameter(dbCommand, "as_CreationTime", DbType.String, creationTime);
            _db.AddInParameter(dbCommand, "ab_ReturnValue", DbType.Int32);
            _db.AddInParameter(dbCommand, "ad_ViewedDate", DbType.DateTime);

            _db.ExecuteNonQuery(dbCommand);

            viewedDate = _db.GetParameterValue(dbCommand, "ad_ViewedDate") != DBNull.Value
                             ?
                                 (DateTime) _db.GetParameterValue(dbCommand, "ad_ViewedDate")
                             : DateTime.Now;

            return _db.GetParameterValue(dbCommand, "ab_ReturnValue") != DBNull.Value
                       ? (bool) _db.GetParameterValue(dbCommand, "ab_ReturnValue")
                       : false;
        }

        public bool DeleteReportFromAcl(string reportCode)
        {
            const string sqlCommand = "usp_Delete_ReportFromACL";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_ReportCode", DbType.String, reportCode);

            return DoTransaction(dbCommand);
        }

        public ArrayList GetReportAdminGroups(string reportCode)
        {
            const string sqlCommand = "usp_Get_ReportCode_AdminGroups";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            IDataReader dr = _db.ExecuteReader(dbCommand);

            ArrayList aryRet = new ArrayList();

            if (!_hasLoadedAdminGroups)
            {
                _hasLoadedAdminGroups = true;

                _aryAdminGroups = new ArrayList();

                while (dr.Read())
                    _aryAdminGroups.Add(new AdminGroups(dr["ReportCode"].ToString(), dr["GroupName"].ToString()));
            }

            // Ok.. now loop through the struct getting all the admin groups for this report
            foreach (AdminGroups oAdminGroups in _aryAdminGroups)
                if (oAdminGroups.ReportCode.ToLower().Equals(reportCode.ToLower()))
                    aryRet.Add(oAdminGroups.GroupName);

            return aryRet;
        }

        public ArrayList GetAdminGroups()
        {
            ArrayList aryRet = new ArrayList();

            const string sqlCommand = "usp_Get_AdminGroups";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    aryRet.Add(dataReader["GroupName"].ToString());
                }
            }

            return aryRet;
        }

        public bool InsertOrUpdateReportInAcl(string updateInsert, string reportCode, int archiveLength,
                                              int reportTypeId, int archiveValue,
                                              int overwriteValue)
        {
            DbCommand dbCommand = updateInsert == "I"
                                      ? _db.GetStoredProcCommand("usp_Insert_ReportIntoACL")
                                      : _db.GetStoredProcCommand("usp_Update_ReportInACL");

            _db.AddInParameter(dbCommand, "as_ReportCode", DbType.String, reportCode);
            _db.AddInParameter(dbCommand, "ai_ArchiveLength", DbType.Int32, archiveLength);
            _db.AddInParameter(dbCommand, "ai_ReportTypeID", DbType.Int32, reportTypeId);
            _db.AddInParameter(dbCommand, "ai_ArchiveValue", DbType.Int32, archiveValue);
            _db.AddInParameter(dbCommand, "ai_OverwriteValue", DbType.Int32, overwriteValue);

            return DoTransaction(dbCommand);
        }
    } // Class
} // Namespace