#region Namespace Imports

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Burst.SqlServerDal.Base;
using MyReports.Common.Structs;

#endregion

namespace MyReports.Burst.SqlServerDal
{
    public class Zrops : DalBase
    {
        public Zrops(Database db) : base(db) {}

        public List<string> GetDmAndFolderNameByRegion(string regionCode)
        {
            List<string> retList = new List<string>();

            const string sqlCommand = "usp_Get_DMAndFolderNameByRegion";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_Region", DbType.String, regionCode);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    retList.Add(dataReader["Dm"].ToString().Trim() + " - " + dataReader["FolderName"].ToString().Trim());
                }
            }

            return retList;
        }

        public List<string> GetUnitByDm(string dmCode)
        {
            List<string> retList = new List<string>();

            const string sqlCommand = "usp_Get_UnitsByDM";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_DM", DbType.String, dmCode);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    retList.Add(dataReader["Unit"].ToString().Trim());
                }
            }

            return retList;
        }

        public List<string> GetUnitByRegion(string regionCode)
        {
            List<string> retList = new List<string>();

            const string sqlCommand = "usp_Get_UnitsByRegion";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_Region", DbType.String, regionCode);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    retList.Add(dataReader["Unit"].ToString().Trim());
                }
            }

            return retList;
        }

        public DataSet GetZropsTable()
        {
            const string sqlCommand = "usp_Get_Zrops";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            return _db.ExecuteDataSet(dbCommand);
        }

        public List<string> GetDistinctRegionCodeListing()
        {
            List<string> retList = new List<string>();

            const string sqlCommand = "usp_Get_DistinctRegionCodes";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    retList.Add(dataReader["Region"].ToString().Trim());
                }
            }

            return retList;
        }

        public List<string> GetDistinctRegionCodeAndNameListing()
        {
            List<string> retList = new List<string>();

            const string sqlCommand = "usp_Get_DistinctRegionCodesAndNames";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    retList.Add(dataReader["Region"].ToString().Trim() + " - " +
                                dataReader["FolderName"].ToString().Trim());
                }
            }

            return retList;
        }

        public Hierarchy GetRegionCodeFromDmCode(string sDmCode)
        {
            const string sqlCommand = "usp_Get_Region_Code_From_DM_Code";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_DMCode", DbType.String, sDmCode);
            _db.AddOutParameter(dbCommand, "as_RegionCode", DbType.String, 10);

            _db.ExecuteNonQuery(dbCommand);

            Hierarchy hierarchy = new Hierarchy
                                      {
                                          Region =
                                              _db.GetParameterValue(dbCommand, "as_RegionCode") != DBNull.Value
                                                  ? (string) _db.GetParameterValue(dbCommand, "as_RegionCode")
                                                  : "",
                                          Dm = sDmCode,
                                          Unit = ""
                                      };

            return hierarchy;
        }

        public Hierarchy GetRegionCodeAnddmCodeFromUnitCode(string sUnitCode)
        {
            const string sqlCommand = "usp_Get_Region_Code_AND_DM_Code_From_Unit_Code";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_UnitCode", DbType.String, sUnitCode);
            _db.AddOutParameter(dbCommand, "as_RegionCode", DbType.String, 10);
            _db.AddOutParameter(dbCommand, "as_DMCode", DbType.String, 10);

            _db.ExecuteNonQuery(dbCommand);

            Hierarchy hierarchy = new Hierarchy
                                      {
                                          Region =
                                              _db.GetParameterValue(dbCommand, "as_RegionCode") != DBNull.Value
                                                  ? (string) _db.GetParameterValue(dbCommand, "as_RegionCode")
                                                  : "",
                                          Dm =
                                              _db.GetParameterValue(dbCommand, "as_DMCode") != DBNull.Value
                                                  ? (string) _db.GetParameterValue(dbCommand, "as_DMCode")
                                                  : "",
                                          Unit = sUnitCode
                                      };

            return hierarchy;
        }

        public void BackupToTable()
        {
            const string sqlCommand = "usp_BackupZrops";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.ExecuteNonQuery(dbCommand);
        }

        public void Insert(List<Hierarchy> hierarchy)
        {
            const string sqlCommand = "usp_Insert_Zrops";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            using (DbConnection connection = _db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();

                try
                {
                    foreach (Hierarchy zropsRecord in hierarchy)
                    {
                        _db.AddInParameter(dbCommand, "as_Unit", DbType.String, zropsRecord.Unit);
                        _db.AddInParameter(dbCommand, "as_DM", DbType.String, zropsRecord.Dm);
                        _db.AddInParameter(dbCommand, "as_Region", DbType.String, zropsRecord.Region);
                        _db.AddInParameter(dbCommand, "as_Division", DbType.String, zropsRecord.Division);
                        _db.AddInParameter(dbCommand, "as_Sector", DbType.String, zropsRecord.Sector);

                        _db.ExecuteNonQuery(dbCommand, transaction);
                    }
                    // Commit the transaction
                    transaction.Commit();
                }
                catch
                {
                    // Rollback transaction 
                    transaction.Rollback();
                }

                connection.Close();
            }
        }

        public void RestoreFromBackupTable()
        {
            const string sqlCommand = "usp_RestoreZropsBackupTable";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.ExecuteNonQuery(dbCommand);
        }
    }
}