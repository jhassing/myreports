namespace MyReports.Burst.SqlServerDal.ClientEmail
{
    public struct ClientEmailSendFromObject
    {
        public int ClientEmailSendFromID { get; set; }
        public string FromEmailAddress { get; set; }
        public int EmailMessageID { get; set; }
    }
}