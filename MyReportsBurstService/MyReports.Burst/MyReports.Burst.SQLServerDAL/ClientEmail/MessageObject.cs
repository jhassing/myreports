namespace MyReports.Burst.SqlServerDal.ClientEmail
{
    public struct MessageObject
    {
        public int EmailMessageID { get; set; }
        public string EmailTitle { get; set; }
        public string EmailSubject { get; set; }
        public string EmailMessage { get; set; }
    }
}