using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Burst.SqlServerDal.Base;

namespace MyReports.Burst.SqlServerDal.ClientEmail
{
    public class EmailMessage : DalBase
    {
        #region ~ctor

        public EmailMessage(Database db)
            : base(db)
        {
        }

        #endregion

        public List<MessageObject> Get(int emailMessageID)
        {
            List<MessageObject> listRet = new List<MessageObject>();

            const string sqlCommand = "usp_Get_EmailMessageByPrimaryKey";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_EmailMessageID", DbType.Int32, emailMessageID);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    listRet.Add(new MessageObject
                                    {
                                        EmailMessage = (string) dataReader["Message"],
                                        EmailMessageID = (int) dataReader["EmailMessageID"],
                                        EmailSubject = (string) dataReader["Subject"],
                                        EmailTitle = (string) dataReader["Title"]
                                    });
                }
            }

            return listRet;
        }

        public List<MessageObject> Get(string emailTitle)
        {
            List<MessageObject> listRet = new List<MessageObject>();

            //input sting cannot be null or the this method will return nothing, but I want it to return all records if nothing is provided
            if (emailTitle == null)
                emailTitle += "";

            const string sqlCommand = "usp_Get_EmailMessageListing";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_Title", DbType.Int32, emailTitle);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    listRet.Add(new MessageObject
                                    {
                                        EmailMessage = (string) dataReader["Message"],
                                        EmailMessageID = (int) dataReader["EmailMessageID"],
                                        EmailSubject = (string) dataReader["Subject"],
                                        EmailTitle = (string) dataReader["Title"]
                                    });
                }
            }

            return listRet;
        }

        public bool Insert(string emailTitle, string emailSubject, string emailMessage, string createdBy)
        {
            const string sqlCommand = "usp_Insert_EmailMessage";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_Title", DbType.String, emailTitle);
            _db.AddInParameter(dbCommand, "as_Subject", DbType.String, emailSubject);
            _db.AddInParameter(dbCommand, "as_Message", DbType.String, emailMessage);
            _db.AddInParameter(dbCommand, "as_CreatedBy", DbType.String, createdBy);

            return DoTransaction(dbCommand);
        }

        public bool Update(int emailMessageID, string emailTitle, string emailSubject, string emailMessage,
                           string modyfiedBy)
        {
            const string sqlCommand = "usp_Delete_EmailMessage";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_EmailMessageID", DbType.Int32, emailMessageID);
            _db.AddInParameter(dbCommand, "as_Title", DbType.String, emailTitle);
            _db.AddInParameter(dbCommand, "as_Subject", DbType.String, emailSubject);
            _db.AddInParameter(dbCommand, "as_Message", DbType.String, emailMessage);
            _db.AddInParameter(dbCommand, "as_ModifiedBy", DbType.String, modyfiedBy);

            return DoTransaction(dbCommand);
        }

        public bool Delete(int emailMessageID)
        {
            const string sqlCommand = "usp_Delete_EmailMessage";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_EmailMessageID", DbType.Int32, emailMessageID);

            return DoTransaction(dbCommand);
        }
    }
}