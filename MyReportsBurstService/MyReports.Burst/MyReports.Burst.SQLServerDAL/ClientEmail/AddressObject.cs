using System;

namespace MyReports.Burst.SqlServerDal.ClientEmail
{
    public struct AddressObject
    {
        public int ClientEmailAddressID { get; set; }
        public string ClientEmailAddress { get; set; }
        public string ClientName { get;  set; }
        public string KeyWord { get;  set; }
        public string FromEmailAddress { get;  set; }
        public int ClientEmailSendFromID { get;  set; }
        public string Cc1 { get;  set; }
        public string Cc2 { get;  set; }
        public string Bcc1 { get;  set; }
        public string AccountantFn { get;  set; }
        public string AccountantLn { get;  set; }
        public string Region { get;  set; }
        public string CreatedBy { get;  set; }
        public DateTime CreatedTimeDate { get;  set; }
        public string ModifiedBy { get;  set; }
        public DateTime ModifiedDateTime { get;  set; }
    }
}