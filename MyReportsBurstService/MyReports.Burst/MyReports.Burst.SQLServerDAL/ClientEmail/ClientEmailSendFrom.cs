using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Burst.SqlServerDal.Base;

namespace MyReports.Burst.SqlServerDal.ClientEmail
{
    public class ClientEmailSendFrom : DalBase
    {
        public ClientEmailSendFrom(Database db) : base(db) {}

        public List<ClientEmailSendFromObject> Get(int clientEmailSendFromID)
        {
            List<ClientEmailSendFromObject> aryRet = new List<ClientEmailSendFromObject>();

            const string sqlCommand = "usp_Get_ClientEmailSendFromByPrimaryKey";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_ClientEmailSendFromID", DbType.Int32, clientEmailSendFromID);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    aryRet.Add(
                        new ClientEmailSendFromObject
                            {
                                ClientEmailSendFromID = System.Convert.ToInt32(dataReader["ClientEmailSendFromID"].ToString()),
                                FromEmailAddress = dataReader["FromEmailAddress"].ToString(),
                                EmailMessageID = System.Convert.ToInt32(dataReader["EmailMessageID"].ToString())
                            });
                }
            }

            return aryRet;

        }

        public List<ClientEmailSendFromObject> Get(string fromEmailAddress)
        {
            List<ClientEmailSendFromObject> aryRet = new List<ClientEmailSendFromObject>();
            fromEmailAddress += "";

            const string sqlCommand = "usp_Get_ClientEmailSendFromListing";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_ClientEmailSendFromID", DbType.String, fromEmailAddress);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    aryRet.Add(
                        new ClientEmailSendFromObject
                            {
                                ClientEmailSendFromID = System.Convert.ToInt32(dataReader["ClientEmailSendFromID"].ToString()),
                                FromEmailAddress = dataReader["FromEmailAddress"].ToString(),
                                EmailMessageID = System.Convert.ToInt32(dataReader["EmailMessageID"].ToString())
                            });
                }
            }

            return aryRet;
        }        

        public bool Update(int clientEmailSendFromID, string fromEmailAddress, int emailMessageID, string modifiedBy)
        {

            const string sqlCommand = "usp_Update_ClientEmailSendFrom";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_ClientEmailSendFromID", DbType.Int32, clientEmailSendFromID);
            _db.AddInParameter(dbCommand, "as_FromEmailAddress", DbType.String, fromEmailAddress);
            _db.AddInParameter(dbCommand, "as_EmailMessageID", DbType.Int32, emailMessageID);
            _db.AddInParameter(dbCommand, "as_ModifiedBy", DbType.String, modifiedBy);

            return DoTransaction(dbCommand);
        }

        public bool Insert(string fromEmailAddress, int emailMessageID, string createdBy)
        {

            const string sqlCommand = "usp_Insert_ClientEmailSendFrom";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_FromEmailAddress", DbType.String, fromEmailAddress);
            _db.AddInParameter(dbCommand, "as_EmailMessageID", DbType.Int32, emailMessageID);
            _db.AddInParameter(dbCommand, "as_CreatedBy", DbType.String, createdBy);

            return DoTransaction(dbCommand);
        }

        public bool Delete(int clientEmailSendFromID)
        {
            const string sqlCommand = "usp_Delete_ClientEmailSendFrom";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_ClientEmailSendFromID", DbType.Int32, clientEmailSendFromID);

            return DoTransaction(dbCommand);
        }
    }
}