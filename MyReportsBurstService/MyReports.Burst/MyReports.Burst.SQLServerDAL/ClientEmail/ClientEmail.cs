using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Burst.SqlServerDal.Base;

namespace MyReports.Burst.SqlServerDal.ClientEmail
{
    public class Address : DalBase
    {
        public Address(Database db) : base(db) {}

        public List<AddressObject> Get(string clientEmailAddress,
                                       string clientName,
                                       string clientContact,
                                       string accountantFn,
                                       string accountantLn,
                                       string region)
        {
            clientEmailAddress += "";
            clientName += "";
            accountantFn += "";
            accountantLn += "";
            region += "";

            List<AddressObject> aryRet = new List<AddressObject>();

            const string sqlCommand = "usp_Get_ClientEmailAddress";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_ClientEmailAddress", DbType.String, clientEmailAddress);
            _db.AddInParameter(dbCommand, "as_ClientName", DbType.String, clientName);
            _db.AddInParameter(dbCommand, "as_ClientContact", DbType.String, clientContact);
            _db.AddInParameter(dbCommand, "as_AccountantFN", DbType.String, accountantFn);
            _db.AddInParameter(dbCommand, "as_AccountantLN", DbType.String, accountantLn);
            _db.AddInParameter(dbCommand, "as_Region", DbType.String, region);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    aryRet.Add(
                        new AddressObject
                            {
                                ClientEmailAddressID = (int) dataReader["ClientEmailAddressID"],
                                ClientEmailAddress = (string) dataReader["ClientEmailAddress"],
                                ClientName = (string) dataReader["ClientName"],
                                KeyWord = (string) dataReader["KeyWord"],
                                FromEmailAddress = (string) dataReader["FromEmailAddress"],
                                ClientEmailSendFromID = (int) dataReader["ClientEmailSendFromID"],
                                Cc1 = (string) dataReader["CC1"],
                                Cc2 = (string) dataReader["CC2"],
                                Bcc1 = (string) dataReader["BCC1"],
                                AccountantFn = (string) dataReader["AccountantFN"],
                                AccountantLn = (string) dataReader["AccountantLN"],
                                Region = (string) dataReader["Region"],
                                CreatedBy = (string) dataReader["CreatedBy"],
                                CreatedTimeDate = (DateTime) dataReader["CreatedDateTime"],
                                ModifiedBy = (string) dataReader["ModifiedBy"],
                                ModifiedDateTime = (DateTime) dataReader["ModifiedDateTime"],
                            });
                }
            }

            return aryRet;
        }


        public List<AddressObject> Get(int clientEmailAddressID)
        {
            List<AddressObject> aryRet = new List<AddressObject>();

            const string sqlCommand = "usp_Get_ClientEmailAddressByPrimaryKey";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_ClientEmailAddressID", DbType.Int32, clientEmailAddressID);

            using (IDataReader dataReader = _db.ExecuteReader(dbCommand))
            {
                while (dataReader.Read())
                {
                    aryRet.Add(
                        new AddressObject
                            {
                                ClientEmailAddressID = (int) dataReader["ClientEmailAddressID"],
                                ClientEmailAddress = (string) dataReader["ClientEmailAddress"],
                                ClientName = (string) dataReader["ClientName"],
                                KeyWord = (string) dataReader["KeyWord"],
                                FromEmailAddress = (string) dataReader["FromEmailAddress"],
                                ClientEmailSendFromID = (int) dataReader["ClientEmailSendFromID"],
                                Cc1 = (string) dataReader["CC1"],
                                Cc2 = (string) dataReader["CC2"],
                                Bcc1 = (string) dataReader["BCC1"],
                                AccountantFn = (string) dataReader["AccountantFN"],
                                AccountantLn = (string) dataReader["AccountantLN"],
                                Region = (string) dataReader["Region"],
                                CreatedBy = (string) dataReader["CreatedBy"],
                                CreatedTimeDate = (DateTime) dataReader["CreatedDateTime"],
                                ModifiedBy = (string) dataReader["ModifiedBy"],
                                ModifiedDateTime = (DateTime) dataReader["ModifiedDateTime"],
                            });
                }
            }

            return aryRet;
        }


        public bool Update(int clientEmailAddressID,
                           string clientEmailAddress,
                           string clientName,
                           string keyWord,
                           int clientEmailSendFromID,
                           string cc1,
                           string cc2,
                           string bcc1,
                           string accountantFn,
                           string accountantLn,
                           string region,
                           string modifiedBy)
        {
            const string sqlCommand = "usp_Update_ClientEmailAddress";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_ClientEmailAddressID", DbType.String, clientEmailAddressID);
            _db.AddInParameter(dbCommand, "as_ClientEmailAddress", DbType.String, clientEmailAddress);
            _db.AddInParameter(dbCommand, "as_ClientName", DbType.String, clientName);
            _db.AddInParameter(dbCommand, "as_KeyWord", DbType.String, keyWord);
            _db.AddInParameter(dbCommand, "ai_ClientEmailSendFromID", DbType.Int32, clientEmailSendFromID);
            _db.AddInParameter(dbCommand, "as_CC1", DbType.String, cc1);
            _db.AddInParameter(dbCommand, "as_CC2", DbType.String, cc2);
            _db.AddInParameter(dbCommand, "as_BCC1", DbType.String, bcc1);
            _db.AddInParameter(dbCommand, "as_AccountantFN", DbType.String, accountantFn);
            _db.AddInParameter(dbCommand, "as_AccountantLN", DbType.String, accountantLn);
            _db.AddInParameter(dbCommand, "as_Region", DbType.String, region);
            _db.AddInParameter(dbCommand, "as_ModifiedBy", DbType.String, modifiedBy);

            return DoTransaction(dbCommand);
        }


        public bool Insert(string clientEmailAddress,
                           string clientName,
                           string keyWord,
                           int clientEmailSendFromID,
                           string cc1,
                           string cc2,
                           string bcc1,
                           string accountantFn,
                           string accountantLn,
                           string region,
                           string createdBy)
        {
            const string sqlCommand = "usp_Insert_ClientEmailAddress";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_ClientEmailAddress", DbType.String, clientEmailAddress);
            _db.AddInParameter(dbCommand, "as_ClientName", DbType.String, clientName);
            _db.AddInParameter(dbCommand, "as_KeyWord", DbType.String, keyWord);
            _db.AddInParameter(dbCommand, "ai_ClientEmailSendFromID", DbType.Int32, clientEmailSendFromID);
            _db.AddInParameter(dbCommand, "as_CC1", DbType.String, cc1);
            _db.AddInParameter(dbCommand, "as_CC2", DbType.String, cc2);
            _db.AddInParameter(dbCommand, "as_BCC1", DbType.String, bcc1);
            _db.AddInParameter(dbCommand, "as_AccountantFN", DbType.String, accountantFn);
            _db.AddInParameter(dbCommand, "as_AccountantLN", DbType.String, accountantLn);
            _db.AddInParameter(dbCommand, "as_Region", DbType.String, region);
            _db.AddInParameter(dbCommand, "as_CreatedBy", DbType.String, createdBy);

            return DoTransaction(dbCommand);
        }


        public bool Delete(int clientEmailAddressID)
        {
            const string sqlCommand = "usp_Delete_ClientEmailAddress";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_ClientEmailAddressID", DbType.Int32, clientEmailAddressID);

            return DoTransaction(dbCommand);
        }
    }

    public class AuditTracking : DalBase
    {
        public AuditTracking(Database db) : base(db)
        {
        }

        public int Insert(string networkID,
                          string clientEmailAddress,
                          string clientEmailSendFrom,
                          string cc1,
                          string cc2,
                          string bcc1)
        {
            const string sqlCommand = "usp_Insert_ClientEmailAddress_AuditTracking";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_NetworkID", DbType.String, networkID);
            _db.AddInParameter(dbCommand, "as_ClientEmailAddress", DbType.String, clientEmailAddress);
            _db.AddInParameter(dbCommand, "as_ClientEmailSendFrom", DbType.String, clientEmailSendFrom);

            _db.AddInParameter(dbCommand, "as_CC1", DbType.String, cc1);
            _db.AddInParameter(dbCommand, "as_CC2", DbType.String, cc2);
            _db.AddInParameter(dbCommand, "as_BCC1", DbType.String, bcc1);

            _db.AddOutParameter(dbCommand, "ai_ident", DbType.Int64, 10);

            DoTransaction(dbCommand);

            return _db.GetParameterValue(dbCommand, "ai_ident") != DBNull.Value
                       ? (int) _db.GetParameterValue(dbCommand, "ai_ident")
                       : 0;
        }

        public void InsertDetail(int id, ArrayList fileNames)
        {
            const string sqlCommand = "usp_Insert_ClientEmailAddress_AuditTrackingDetails";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            using (DbConnection connection = _db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();

                try
                {
                    foreach (string fileName in fileNames)
                    {
                        _db.AddInParameter(dbCommand, "ai_ClientEmailAddress_AuditTrackingID", DbType.Int32, id);
                        _db.AddInParameter(dbCommand, "as_FileName", DbType.String, Path.GetFileName(fileName));

                        _db.ExecuteNonQuery(dbCommand, transaction);
                    }
                    // Commit the transaction
                    transaction.Commit();
                }
                catch
                {
                    // Rollback transaction 
                    transaction.Rollback();
                }

                connection.Close();
            }
        }
    }
}