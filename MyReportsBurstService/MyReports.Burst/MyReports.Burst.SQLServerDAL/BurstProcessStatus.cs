using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Burst.SqlServerDal.Base;

namespace MyReports.Burst.SqlServerDal
{
    public class BurstProcessStatus : DalBase
    {
        public BurstProcessStatus(Database db) : base(db)
        {
        }

        public bool InsertBurstProcessStatusBurstProcessStatusCodes(int nBurstProcessStatusPK,
                                                                    int nBurstProcessStatusCodesPK)
        {
            const string sqlCommand = "usp_Insert_BurstProcessStatus_BurstProcessStatusCode";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_BurstProcessStatusID", DbType.Int32, nBurstProcessStatusPK);
            _db.AddInParameter(dbCommand, "ai_BurstProcessStatusCodeID", DbType.Int32, nBurstProcessStatusCodesPK);

            return DoTransaction(dbCommand);
        }

        public int InsertBurstProcessStatusSapFileName(string sSapFileName)
        {
            const string sqlCommand = "usp_Insert_BurstProcessStatus_SAPFileName";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_SAPfileName", DbType.String, sSapFileName);
            _db.AddOutParameter(dbCommand, "ai_ident", DbType.Int32, 10);

            DoTransaction(dbCommand);

            return _db.GetParameterValue(dbCommand, "ai_ident") != DBNull.Value
                       ? (int) _db.GetParameterValue(dbCommand, "ai_ident")
                       : 0;
        }

        public bool InsertBurstProcessStatusReportLocations(int key, string reportLocation)
        {
            const string sqlCommand = "usp_Insert_BurstProcessStatus_Details";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_BurstProcessStatus", DbType.Int32, key);
            _db.AddInParameter(dbCommand, "as_ReportLocation", DbType.String, reportLocation);

            return DoTransaction(dbCommand);
        }

        public bool UpdateBurstProcessStatusReportName(int key, string reportName, string reportCode)
        {
            const string sqlCommand = "usp_Update_BurstProcessStatus_ReportName";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_ReportName", DbType.String, reportName);
            _db.AddInParameter(dbCommand, "ai_Ident", DbType.Int32, key);
            _db.AddInParameter(dbCommand, "as_ReportCode", DbType.String, reportCode);

            return DoTransaction(dbCommand);
        }

        public bool UpdateBurstProcessStatusErrorCode(int key, int errorCode)
        {
            const string sqlCommand = "usp_Update_BurstProcessStatus_ErrorCode";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ai_ErrorCode", DbType.Int32, errorCode);
            _db.AddInParameter(dbCommand, "ai_Ident", DbType.Int32, key);

            return DoTransaction(dbCommand);
        }

        public DataSet GetBurstProcessStatus(DateTime startDate, DateTime endDate)
        {
            const string sqlCommand = "usp_Get_BurstStatusByFinishedDate";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "ad_BeginningDate", DbType.DateTime, startDate);
            _db.AddInParameter(dbCommand, "ad_EndingDate", DbType.DateTime, endDate);

            return _db.ExecuteDataSet(dbCommand);
        }
    }
}