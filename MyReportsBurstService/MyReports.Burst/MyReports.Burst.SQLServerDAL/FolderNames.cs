#region Namespace Imports

using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Burst.SqlServerDal.Base;
using MyReports.Common.Structs;

#endregion

namespace MyReports.Burst.SqlServerDal
{
    public class FolderNames : DalBase
    {
        #region ~ctor

        public FolderNames(Database db)
            : base(db)
        {
        }

        #endregion

        public DataSet GetFolders()
        {
            const string sqlCommand = "usp_Get_Folders";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            return _db.ExecuteDataSet(dbCommand);
        }

        /// <summary>
        /// Function will compare folder names from the import file against the folder name in the database
        /// based on the requested folder code.
        /// </summary>
        /// <param name="dsFolders">Data Set containing the folders</param>
        /// <param name="sFolderCode">Folder code to search for in the database.</param>
        /// <param name="sNewFolderName">Folder name from the import file.</param>
        /// <param name="sDatabaseFolderName">Folder name currently contained in the database, that gets returned to the calling function.</param>
        /// <returns>True or False based on the folder names being the same.</returns>
        /// <example>IsFolderCodeSameAsDatabase("cho", "Middle Atlantic Region", string sOldValue)</example>
        public bool IsFolderCodeSameAsDatabase(DataSet dsFolders, string sFolderCode, string sNewFolderName, out string sDatabaseFolderName)
        {
            bool bRetValue = false;

            sDatabaseFolderName = "";

            try
            {

                DataRow[] foundRows = dsFolders.Tables[0].Select("FolderCode='" + sFolderCode + "'");

                if (foundRows.Length > 0)
                {
                    if (foundRows[0].ItemArray.Length > 1)
                    {
                        sDatabaseFolderName = foundRows[0].ItemArray[1].ToString();
                        bRetValue = (sNewFolderName == sDatabaseFolderName);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return (bRetValue);

        }

        /// <summary>
        /// A method to return a folder name form SQL Server.
        /// The method will return a string containing the folder name
        /// </summary>	
        public static string GetFolderName(string sFolderCode, DataSet dsFolders, bool bPadWithLeadingZeros)
        {
            try
            {
                string tmp = "";

                // First, string the leading zeros off the folder code, just in case
                string tmpFolderCode = sFolderCode.TrimStart('0');

                // Next, find the folder name
                DataRow[] foundRows = dsFolders.Tables[0].Select("FolderCode='" + tmpFolderCode + "'");

                tmp = bPadWithLeadingZeros ? Common.Functions.FormatOperationsCode(sFolderCode) : sFolderCode;

                if (foundRows != null)
                {
                    if (foundRows.Length > 0)
                    {
                        if (foundRows[0].ItemArray.Length > 1)
                            tmp += " - " +
                                   Common.Functions.RemoveIllegalChars(
                                       foundRows[0].ItemArray[1].ToString());
                    }
                }

                return tmp;

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public static string GetFolderName(string sFolderCode, DataSet dsFolders)
        {
            try
            {
                return GetFolderName(sFolderCode, dsFolders, false);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public bool DoesFolderCodeExist(string sFolderCode)
        {
            const string sqlCommand = "usp_CheckFolderCode";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_FolderCode", DbType.String, sFolderCode.Trim());
            _db.AddOutParameter(dbCommand, "ab_ReturnValue", DbType.Byte, 1);

            _db.ExecuteNonQuery(dbCommand);
            
            return _db.GetParameterValue(dbCommand, "ab_ReturnValue") != DBNull.Value
                       ? Convert.ToBoolean(_db.GetParameterValue(dbCommand, "ab_ReturnValue"))
                       : false;
        }

        public void UpdateInsertFolderName(ArrayList aryFolders)
        {
            foreach (NewFolderRecord folder in aryFolders)
            {
                if (folder.NewRecord)
                {
                    const string sqlCommand = "usp_Insert_Folders";
                    DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

                    _db.AddInParameter(dbCommand, "as_FolderCode", DbType.String, folder.FolderCode);
                    _db.AddInParameter(dbCommand, "as_FolderName", DbType.String, folder.NewFolderName);

                    DoTransaction(dbCommand);
                }
                else
                {
                    const string sqlCommand = "usp_Update_Folders";
                    DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

                    _db.AddInParameter(dbCommand, "as_FolderCode", DbType.String, folder.FolderCode);
                    _db.AddInParameter(dbCommand, "as_FolderName", DbType.String, folder.NewFolderName);

                    DoTransaction(dbCommand);
                }
            }
        }

        public bool BackupFoldersTable()
        {
            const string sqlCommand = "usp_Backup_FoldersTable";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            return DoTransaction(dbCommand);
        }

        #region Get Folders Backup Table

        public void GetFoldersBackupTable()
        {
        }

        #endregion
    } // Class
} // Namespace