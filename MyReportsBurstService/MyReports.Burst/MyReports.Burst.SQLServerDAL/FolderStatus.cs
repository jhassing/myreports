using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Burst.SqlServerDal.Base;
using MyReports.Burst;
using MyReports.Common.Structs;

namespace MyReports.Burst.SqlServerDal
{
    public class FolderStatus : DalBase
    {
        public FolderStatus(Database db) : base(db)
        {
        }

        public bool InsertHomeFolderStatusUpdate(string sUserID, string sExistingHomeFolderName,
                                                 string sNewHomeFolderName)
        {
            const string sqlCommand = "usp_Insert_HomeFolderStatus";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            _db.AddInParameter(dbCommand, "as_UserID", DbType.String, sUserID);
            _db.AddInParameter(dbCommand, "as_ExistingHomeFolderName", DbType.String, sExistingHomeFolderName);
            _db.AddInParameter(dbCommand, "as_NewHomeFolderName", DbType.String, sNewHomeFolderName);

            return DoTransaction(dbCommand);
        }


        public void InsertFolderStatusDetail(List<NewFolderRecord> aryFolders)
        {
            const string sqlCommand = "usp_Insert_FolderTableUpdateRecord_Detail";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            using (DbConnection connection = _db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();

                try
                {
                    foreach (NewFolderRecord folder in aryFolders)
                    {
                        _db.AddInParameter(dbCommand, "as_FolderCode", DbType.String, folder.FolderCode);
                        _db.AddInParameter(dbCommand, "as_ExistingFolderName", DbType.String,
                                           folder.CurrentFolderName);
                        _db.AddInParameter(dbCommand, "as_NewFolderName", DbType.String, folder.NewFolderName);
                        _db.AddInParameter(dbCommand, "ai_FolderStatusCode", DbType.Int32, folder.LogCode);

                        _db.ExecuteNonQuery(dbCommand, transaction);
                    }
                    // Commit the transaction
                    transaction.Commit();
                }
                catch
                {
                    // Rollback transaction 
                    transaction.Rollback();
                }

                connection.Close();
            }
        }

        public bool InsertFolderStatusMaster()
        {
            const string sqlCommand = "usp_Insert_FolderTableUpdateRecord_Master";
            DbCommand dbCommand = _db.GetStoredProcCommand(sqlCommand);

            return DoTransaction(dbCommand);
        }

        public SqlDataReader GetFoldersBackupTable()
        {
            const SqlDataReader rd = null;

            try
            {
                //SQLServerDAL.FolderStatus dal = new SQLServerDAL.FolderStatus(base._connectionString);
                // rd = dal.GetFoldersBackupTable();
            }

            catch (Exception ex)
            {
                throw (ex);
            }

            return (rd);
        }

        public void BackupFoldersTable()
        {
            try
            {
                //SQLServerDAL.FolderStatus dal = new SQLServerDAL.FolderStatus(base._connectionString);
            }

            catch (Exception ex)
            {
                throw (ex);
            }
        }
    } // Class
} // Namespace