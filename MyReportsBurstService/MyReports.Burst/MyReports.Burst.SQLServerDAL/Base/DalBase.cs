﻿#region Namespace Imports

using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

#endregion

namespace MyReports.Burst.SqlServerDal.Base
{
    public abstract class DalBase
    {
        protected readonly Database _db;

        protected DalBase(Database db)
        {
            _db = db;
        }

        protected bool DoTransaction(DbCommand dbCommand)
        {
            bool result = false;

            using (DbConnection connection = _db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();

                try
                {
                    _db.ExecuteNonQuery(dbCommand, transaction);

                    // Commit the transaction
                    transaction.Commit();

                    result = true;
                }
                catch
                {
                    // Rollback transaction 
                    transaction.Rollback();
                }

                connection.Close();

                return result;
            }
        }
    }
}