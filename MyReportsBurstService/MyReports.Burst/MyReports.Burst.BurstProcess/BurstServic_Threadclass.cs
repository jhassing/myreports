using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.BurstProcess.Hierarchy;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.ReportArchive;
using MyReports.Burst.RunReport;
using MyReports.Burst.SqlServerDal;
using MyReports.Burst.SubsidyInvoice;
using MyReports.Common;

namespace MyReports.Burst.BurstProcess
{
    internal class BurstServiceThreadclass
    {
        #region Public Events

        public event ThreadEventHandlerStart ThreadStarted;
        public event ThreadEventHandlerFinished ThreadFinished;

        #endregion

        #region Private Class Variables

        private readonly string _mSBurstDir = "";
        private readonly string _mSFileExtention = "";
        private readonly string _mSFilePath = "";
        private List<BurstInformation> _aBurstFiles;
        private bool _bDoSoapCall;
        private int _mNSqlServerPKvalue;
        private string _mNewReportName = "";


        // Arraylist contains the files that were bursted

        private CompassFinancialHierarchy _oCompassFinancialHierarchy;
        private CompassPayrollHierarchy _oCompassPayrollHierarchy;
        private StreamWriter _swDm;
        private StreamWriter _swRegion;
        private StreamWriter _swUnit;

        #endregion

        #region ~ctor

        /// <summary>
        /// This class starts the directory monitoring for RD
        /// </summary>
        /// <param name="sFilePath">Path to the file that will be burst</param>
        /// <param name="sBurstDir">Directory of where to burst the files</param>
        /// <param name="fileExtention">UNIX Handshake file name.</param>
        public BurstServiceThreadclass(string sFilePath, string sBurstDir, string fileExtention)
        {
            // First, clear the GC
            GC.Collect();

            _mSFilePath = sFilePath;
            _mSBurstDir = sBurstDir;
            _mSFileExtention = fileExtention;
        }


        ~BurstServiceThreadclass()
        {
            ThreadStarted = null;
            ThreadFinished = null;
            _swUnit = null;
            _swDm = null;
            _swRegion = null;
        }

        #endregion

        #region Start Process

        public void StartProcess()
        {
            // Raise the Start event
            OnThreadStarted();

            if (_mSFileExtention.ToLower().Equals(".sub"))
            {
                // Subsidy Invoice file, process differently
                LogReportToSql(Enum_ReportTypes.Subsidy_Invoice);
                new ProcessSubsidyInvoice().Import(BurstServiceEntryclass.UnitFolderName, _mSFilePath);
                OnThreadFinished();
            }
            else
            {
                LogReportToSql(Enum_ReportTypes.Financial);
                StartBurst();
            }
        }

        #endregion

        #region Start Burst Process

        private void StartBurst()
        {
            string sLine;
            string sLine1;
            //string untouchedLine;

            bool bInvalidReportName = false;
            bool bReportNameVerified = false;

            Enum_ReportTypes nReportType = Enum_ReportTypes.None;
            Enum_PayTypes nPayType = Enum_PayTypes.None;

            _aBurstFiles = new List<BurstInformation>();

            // Load the folder codes/name once per thread
            var oFolderName = new FolderNames(ConfigurationReader.Db);
            DataSet dsFolders = oFolderName.GetFolders();

            try
            {
                using (var sr = new StreamReader(_mSFilePath))
                {
                    while (sr.Peek() >= 0)
                    {
                        try
                        {
                            // Read the line
                            sLine = sr.ReadLine();
                            sLine1 = sLine.Replace("|", "").Trim().TrimStart(' ').Replace(" ", "");

                            if (sLine.Length > 0)
                            {
                                if (sLine[0] == 12)
                                {
                                    // Page break char.                                    
                                    WriteLine(sLine[0].ToString(), nReportType);
                                    // Read the report code line in and destroy it
                                    if ((sLine1.Length > 0) && (sLine1[1] == 126))
                                        sr.ReadLine();
                                }
                                else
                                {
                                    if ((sLine1.Length > 0) && (sLine1[0] == 126)) // Look for a "~"
                                    {
                                        #region Process Tildy char

                                        // Reset all the reads and their pointers
                                        ResetWriters();

                                        // Read the 1st line after the tildy. This 'should' be the report name and the report code line
                                        string reportNameCodeLine =
                                            sr.ReadLine().Trim().Replace("|", "").TrimStart(' ');

                                        // Report any illegal chars from the line
                                        _mNewReportName = Functions.RemoveIllegalChars(reportNameCodeLine, "-",
                                                                                       "Illegal Report Name");

                                        // Make sure this is a report that we are supposed to burst
                                        // We only need to do this one time. Same report name used throughout file.
                                        if (!bReportNameVerified)
                                        {
                                            bReportNameVerified = true;

                                            // Get the report type
                                            nReportType =
                                                new Reports(ConfigurationReader.Db).GetReportType(_mNewReportName);

                                            #region if this report is invalid, flag here, take care of it later

                                            if (nReportType == Enum_ReportTypes.None)
                                            {
                                                // Report is invalid.. Do not burst
                                                bInvalidReportName = true;
                                                break;
                                            }

                                            // Now check to see if we have data in the Unit, Region OR Dm tildys. If all 3 are empty, then exit
                                            string[] tmp2 = sLine1.Split('~');
                                            if (tmp2.Length > 3)
                                            {
                                                // Proper length
                                                if ((tmp2[2].Trim().Length > 0) || (tmp2[3].Trim().Length > 0) ||
                                                    (tmp2[4].Trim().Length > 0))
                                                {
                                                    // Good..
                                                }
                                                else
                                                {
                                                    bInvalidReportName = true;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                bInvalidReportName = true;
                                                break;
                                            }

                                            #endregion
                                        }

                                        switch (nReportType)
                                        {
                                            #region Process Financial Reports

                                            case Enum_ReportTypes.Financial:
                                                {
                                                    _oCompassFinancialHierarchy =
                                                        new CompassFinancialHierarchy(sLine1,
                                                                                      _mNewReportName,
                                                                                      false,
                                                                                      dsFolders);

                                                    #region Unit

                                                    // Payroll will ALWAYS go to the Unit
                                                    if (_oCompassFinancialHierarchy.UnitIncluded)
                                                    {
                                                        _bDoSoapCall = true;
                                                        Directory.CreateDirectory(_mSBurstDir + @"\" +
                                                                                  BurstServiceEntryclass.
                                                                                      UnitFolderName);
                                                        _swUnit =
                                                            new StreamWriter(
                                                                _mSBurstDir + @"\" +
                                                                _oCompassFinancialHierarchy.UnitFileName, true,
                                                                Encoding.ASCII) { NewLine = "\r\n" };

                                                        AddBurstInformationToArray(
                                                            _mSBurstDir + "\\" +
                                                            _oCompassFinancialHierarchy.UnitFileName,
                                                            _oCompassFinancialHierarchy.UnitFolderName,
                                                            nReportType,
                                                            nPayType);
                                                    }

                                                    #endregion

                                                    #region Region

                                                    if (_oCompassFinancialHierarchy.RegionIncluded)
                                                    {
                                                        _bDoSoapCall = true;
                                                        Directory.CreateDirectory(_mSBurstDir + @"\" +
                                                                                  BurstServiceEntryclass.
                                                                                      RegionFolderName);
                                                        _swRegion =
                                                            new StreamWriter(
                                                                _mSBurstDir + @"\" +
                                                                _oCompassFinancialHierarchy.RegionFileName, true,
                                                                Encoding.ASCII) { NewLine = "\r\n" };
                                                        AddBurstInformationToArray(
                                                            _mSBurstDir + "\\" +
                                                            _oCompassFinancialHierarchy.RegionFileName,
                                                            _oCompassFinancialHierarchy.RegionFolderName,
                                                            nReportType,
                                                            nPayType);
                                                    }

                                                    #endregion

                                                    #region Dm

                                                    if (_oCompassFinancialHierarchy.DmIncluded)
                                                    {
                                                        _bDoSoapCall = true;
                                                        Directory.CreateDirectory(_mSBurstDir + @"\" +
                                                                                  BurstServiceEntryclass.
                                                                                      DmFolderName);
                                                        _swDm =
                                                            new StreamWriter(
                                                                _mSBurstDir + @"\" +
                                                                _oCompassFinancialHierarchy.DmFileName,
                                                                true, Encoding.ASCII) { NewLine = "\r\n" };
                                                        AddBurstInformationToArray(
                                                            _mSBurstDir + "\\" +
                                                            _oCompassFinancialHierarchy.DmFileName,
                                                            _oCompassFinancialHierarchy.DmFolderName,
                                                            nReportType,
                                                            nPayType);
                                                    }

                                                    #endregion

                                                    break;
                                                }

                                            #endregion

                                            #region Process Payroll Reports

                                            case Enum_ReportTypes.Payroll:
                                            case Enum_ReportTypes.Profiles:
                                                {
                                                    if (_mSFileExtention.ToLower().Equals(".off"))
                                                    {
                                                        _oCompassPayrollHierarchy =
                                                            new CompassPayrollHierarchy(sLine1,
                                                                                        _mNewReportName,
                                                                                        true,
                                                                                        dsFolders);
                                                    }
                                                    else
                                                    {
                                                        _oCompassPayrollHierarchy =
                                                            new CompassPayrollHierarchy(sLine1,
                                                                                        _mNewReportName,
                                                                                        false,
                                                                                        dsFolders);
                                                    }

                                                    #region If this is a Payroll or Stubs report, find out if the report is hourly or salary

                                                    if ((nReportType == Enum_ReportTypes.Payroll) ||
                                                        (nReportType == Enum_ReportTypes.Profiles))
                                                    {
                                                        string reportName = new TildyHeader(sLine1).ReportName;

                                                        if ((reportName.EndsWith("h")) || (reportName.EndsWith("H")))
                                                        {
                                                            nPayType = Enum_PayTypes.Hourly;
                                                        }
                                                        else if ((reportName.EndsWith("s")) ||
                                                                 (reportName.EndsWith("S")))
                                                        {
                                                            nPayType = Enum_PayTypes.Salary;
                                                        }
                                                    }

                                                    #endregion

                                                    // Check and see if the unit is included.. It MUST be here before we can proceed
                                                    if (_oCompassPayrollHierarchy.UnitIncluded)
                                                    {
                                                        _bDoSoapCall = true;
                                                        Directory.CreateDirectory(_mSBurstDir + @"\" +
                                                                                  BurstServiceEntryclass.
                                                                                      PayrollFolderName);
                                                        _swUnit =
                                                            new StreamWriter(
                                                                _mSBurstDir + @"\" +
                                                                _oCompassPayrollHierarchy.PayrollFileName, true,
                                                                Encoding.ASCII) { NewLine = "\r\n" };

                                                        AddBurstInformationToArray(
                                                            _mSBurstDir + "\\" +
                                                            _oCompassPayrollHierarchy.PayrollFileName,
                                                            _oCompassPayrollHierarchy.PayrollFolderName,
                                                            _mSFileExtention.ToLower().Equals(".off")
                                                                ? Enum_ReportTypes.Off_Cycle
                                                                : nReportType,
                                                            nPayType);
                                                    }

                                                    break;
                                                }

                                            #endregion
                                        }

                                        #region Log to SQL Server. This report is ok to burst

                                        string pipeLineCode = sLine1.Split('~').Length >= 1
                                                                  ? sLine1.Split('~')[1]
                                                                  : "";

                                        var oBurstProcessStatus =
                                            new BurstProcessStatus(ConfigurationReader.Db);

                                        oBurstProcessStatus.UpdateBurstProcessStatusReportName(_mNSqlServerPKvalue,
                                                                                               pipeLineCode + " - " +
                                                                                               _mNewReportName,
                                                                                               Functions.
                                                                                                   GetReportCodeFromReportName
                                                                                                   (_mNewReportName));
                                        oBurstProcessStatus.InsertBurstProcessStatusBurstProcessStatusCodes(
                                            _mNSqlServerPKvalue, (int)Enum_BurstStatusCodes.ReportNameValid);

                                        #endregion

                                        #endregion
                                    }
                                    else
                                        WriteLine(sLine, nReportType);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            var nv = new NameValueCollection {{"Burst Directory", _mSBurstDir}};
                            ExceptionManager.Publish(ex, nv);

                            continue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var nv = new NameValueCollection {{"Burst Directory", _mSBurstDir}};
                ExceptionManager.Publish(ex, nv);
            }

            // We are finished looping through this File
            ResetWriters();
            OnThreadFinished();

            try
            {
                if (_bDoSoapCall)
                {
                    #region Do SOAP Call to Actuate

                    // Write the master list to the File Log, Get back the file name and path
                    string sMasterFileList = WriteFileListToLog();

                    // Archive the existing reports

                    var oArchive = new ProcessMasterListFile();
                    oArchive.ArchiveReports(sMasterFileList);

                    // This report is ready for actuate, send to SQL Server first
                    var oBurstProcessStatus = new BurstProcessStatus(ConfigurationReader.Db);
                    oBurstProcessStatus.InsertBurstProcessStatusBurstProcessStatusCodes(_mNSqlServerPKvalue,
                                                                                        (int)
                                                                                        Enum_BurstStatusCodes.
                                                                                            FinishedBursting);


                    // Import the bursted files into Actuate
                    new RunActuateReport(sMasterFileList);

                    #endregion
                }
                else
                {
                    #region  Nothing ran, just delete any files, and move the file to the archive folder

                    if (bInvalidReportName)
                    {
                        // Invalid report name

                        // Inform SQL Server this was an invalid report name
                        var oBurstProcessStatus =
                            new BurstProcessStatus(ConfigurationReader.Db);
                        oBurstProcessStatus.UpdateBurstProcessStatusErrorCode(_mNSqlServerPKvalue, 1);

                        // Write entry to the event log
                        clsEventLog.WriteEventLogData(
                            "Report Name: " + _mNewReportName + " is not qualified to burst.\r Name does not exist",
                            EventLogEntryType.Warning, clsEventLog.Enum_ErrorCodes.enum_InvalidReportName);
                    }
                    else
                    {
                        // SAP File had not tildy information

                        // Inform SQL Server this report had no tildy information
                        var oBurstProcessStatus =
                            new BurstProcessStatus(ConfigurationReader.Db);
                        oBurstProcessStatus.UpdateBurstProcessStatusErrorCode(_mNSqlServerPKvalue, 2);

                        // Write entry to the event log
                        clsEventLog.WriteEventLogData("File did not contain any tildy info \n " + _mSFilePath,
                                                      EventLogEntryType.Warning,
                                                      clsEventLog.Enum_ErrorCodes.enum_NoTildyInformation);

                        try
                        {
                            // Clean up left over SAP File(s)
                            Functions.MoveSapReport(
                                ConfigurationReader.RdSettings.ArchiveProperties.DirectoryToArchiveTo,
                                _mSFilePath + "\\");
                            Functions.DeleteBurstFolder(_mSBurstDir + "\\");
                        }
                        catch (Exception ex)
                        {
                            ExceptionManager.Publish(ex);
                        }
                    }

                    #endregion
                }
            }

            catch (Exception ex)
            {
                //				System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();
                //				nv.Add("Path", sPath);
                _bDoSoapCall = false;
                ExceptionManager.Publish(ex);
            }
            finally
            {
                // Clean Up
                //OnThreadFinished(); // move this to fire prior to the actuate SOAP call for thread count accuracy
                _swUnit = null;
                _swDm = null;
                _swRegion = null;
                _aBurstFiles = null;
                _oCompassFinancialHierarchy = null;
                _oCompassPayrollHierarchy = null;
                _bDoSoapCall = false;
            }
        }

        #endregion

        #region Add Burst Information To Array

        private void AddBurstInformationToArray(string physicalLocation, string actuateLocation,
                                                Enum_ReportTypes reportType, Enum_PayTypes payType)
        {
            var o = new BurstInformation(physicalLocation, actuateLocation, reportType, payType);

            bool here =
                _aBurstFiles.Any(
                    oBurstInformation => oBurstInformation.ActuateLocation.ToLower() == o.ActuateLocation.ToLower());

            if (!here)
                _aBurstFiles.Add(o);
        }

        #endregion

        #region Write File List To Log

        private string WriteFileListToLog()
        {
            string sMasterFileName = _mSBurstDir + @"\MasterFileList";

            try
            {
                bool bValue;

                do
                {
                    if (File.Exists(sMasterFileName))
                    {
                        //Rename The file
                        bValue = true;
                        sMasterFileName = sMasterFileName + "1";
                    }
                    else
                        bValue = false;
                } while (bValue);

                sMasterFileName = sMasterFileName + ".txt";

                using (var swMaster = new StreamWriter(sMasterFileName, false))
                    foreach (BurstInformation oBurstInformation in _aBurstFiles)
                        swMaster.WriteLine(oBurstInformation.PhysicalLocation + "|" +
                                           oBurstInformation.ActuateLocation + "|" +
                                           (int) oBurstInformation.ReportType + "|" +
                                           (int) oBurstInformation.PayType);
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
            }

            return sMasterFileName;
        }

        #endregion

        #region Reset Writers

        private void ResetWriters()
        {
            // Reset the writters
            if (_swUnit != null)
                _swUnit.Close();

            if (_swDm != null)
                _swDm.Close();

            if (_swRegion != null)
                _swRegion.Close();
        }

        #endregion

        #region Write Line

        private void WriteLine(string sLine, Enum_ReportTypes nReportType)
        {
            try
            {
                switch (nReportType)
                {
                    case Enum_ReportTypes.Financial:
                        {
                            if (_oCompassFinancialHierarchy != null)
                            {
                                if (_oCompassFinancialHierarchy.UnitIncluded)
                                    _swUnit.WriteLine(sLine);

                                if (_oCompassFinancialHierarchy.DmIncluded)
                                    _swDm.WriteLine(sLine);

                                if (_oCompassFinancialHierarchy.RegionIncluded)
                                    _swRegion.WriteLine(sLine);
                            }

                            break;
                        }

                    case Enum_ReportTypes.Profiles:
                    case Enum_ReportTypes.Payroll:
                        {
                            if (_oCompassPayrollHierarchy != null)
                            {
                                if (_oCompassPayrollHierarchy.UnitIncluded)
                                    _swUnit.WriteLine(sLine);
                            }

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                var nv = new NameValueCollection {{"Line", sLine}};
                ExceptionManager.Publish(ex, nv);
            }
        }

        #endregion

        #region Events

        protected virtual void OnThreadStarted()
        {
            if (ThreadStarted != null)
            {
                // Invokes the delegates. (Raise the event)
                ThreadStarted();
            }
        }

        protected virtual void OnThreadFinished()
        {
            if (ThreadFinished != null)
            {
                // Invokes the delegates. (Raise the event)
                ThreadFinished(_bDoSoapCall);
            }
        }

        #endregion

        private void LogReportToSql(Enum_ReportTypes nReportType)
        {
            BurstProcessStatus oBurstProcessStatus;

            try
            {
                oBurstProcessStatus = new BurstProcessStatus(ConfigurationReader.Db);
                string[] sTemp = _mSFilePath.Split('\\');

                // Log to SQL Server, we found a file.
                // We need to keep the primary key for this file, we will use it later
                switch (nReportType)
                {
                    case Enum_ReportTypes.Financial:
                        _mNSqlServerPKvalue =
                            oBurstProcessStatus.InsertBurstProcessStatusSapFileName(sTemp[sTemp.Length - 1]);
                        oBurstProcessStatus.InsertBurstProcessStatusBurstProcessStatusCodes(_mNSqlServerPKvalue,
                                                                                            (int)
                                                                                            Enum_BurstStatusCodes.
                                                                                                FileFound);

                        break;

                    case Enum_ReportTypes.Subsidy_Invoice:
                        string[] tmp2 = sTemp[2].Split('~');

                        _mNSqlServerPKvalue =
                            oBurstProcessStatus.InsertBurstProcessStatusSapFileName(sTemp[sTemp.Length - 1]);
                        oBurstProcessStatus.InsertBurstProcessStatusBurstProcessStatusCodes(_mNSqlServerPKvalue,
                                                                                            (int)
                                                                                            Enum_BurstStatusCodes.
                                                                                                FileFound);

                        oBurstProcessStatus.UpdateBurstProcessStatusReportName(_mNSqlServerPKvalue, tmp2[2], tmp2[1]);
                        break;
                }

                // Write the ID to a file in the directory. Need to get this once the permissions are set.
                string sIdFileName = Path.GetDirectoryName(_mSFilePath) + @"\dbid";

                if (File.Exists(sIdFileName))
                    File.Delete(sIdFileName);

                using (var swMaster = new StreamWriter(sIdFileName, false))
                    swMaster.WriteLine(_mNSqlServerPKvalue);
            }
            catch (Exception ex)
            {
                var nv = new NameValueCollection
                             {
                                 {"File Path", _mSFilePath},
                                 {"Burst Directory", _mSBurstDir}
                             };
                ExceptionManager.Publish(ex, nv);
            }
        }
    }

    // Class
}

// Namespace