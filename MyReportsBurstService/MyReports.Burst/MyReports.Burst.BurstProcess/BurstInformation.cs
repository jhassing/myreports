﻿using MyReports.Burst.Common;

namespace MyReports.Burst.BurstProcess
{
    public class BurstInformation
    {
        public BurstInformation(string physicalLocation, string actuateLocation, Enum_ReportTypes reportType,
                                Enum_PayTypes payType)
        {
            PhysicalLocation = physicalLocation;
            ActuateLocation = actuateLocation;
            ReportType = reportType;
            PayType = payType;
        }

        public string PhysicalLocation { get; private set; }

        public string ActuateLocation { get; private set; }

        public Enum_ReportTypes ReportType { get; private set; }

        public Enum_PayTypes PayType { get; private set; }
    }
}