using System.Data;
using MyReports.Burst.Common;
using MyReports.Burst.SqlServerDal;

namespace MyReports.Burst.BurstProcess.Hierarchy
{
    public sealed class CompassFinancialHierarchy : CompassHierarchy
    {
        public CompassFinancialHierarchy(string sHeader, string sReportName, bool offCycle, DataSet dsFolderCode)
            : base(sHeader, sReportName, offCycle, dsFolderCode)
        {
            if (UnitCode.Length > 0)
            {
                UnitIncluded = true;
                GetUnitFolderName();
            }
            else
                UnitIncluded = false;


            // Populate the tildy header info, incase we are missing a Region or a Dm
            // Do not include this in the burst, this is for permission setting purposes only.
            FillTildyHeaderInfo();

            // Get the folder names to save information to
            SetSaveFileName();
        }

        private void GetUnitFolderName()
        {
            UnitFolderName = "\\" + BurstServiceEntryclass.UnitFolderName + "\\" +
                             Functions.RemoveIllegalChars(FolderNames.GetFolderName(UnitCode, DsFolders, true)) +
                             "\\" + NewReportName;
        }

        protected override void GetDmFolderName()
        {
            DmFolderName = "\\" + BurstServiceEntryclass.DmFolderName + "\\" +
                           Functions.RemoveIllegalChars(FolderNames.GetFolderName(DmCode, DsFolders)) + "\\" +
                           NewReportName;
        }

        protected override void GetRegionFolderName()
        {
            RegionFolderName = "\\" + BurstServiceEntryclass.RegionFolderName + "\\" +
                               Functions.RemoveIllegalChars(FolderNames.GetFolderName(RegionCode, DsFolders)) +
                               "\\" + NewReportName;
        }


        /// <summary>
        /// This function will populate the tildy header information incase we are missing something.
        /// For example, lets say we get a tildy that looks like this. ~RM~5112~~CHS02~
        /// In this case, we are missing the region level info, so, we will not burst to the (1) Accounting folder.
        /// However, the accountant must still have permissions set on the lower level folders, 5112 and CHS02.
        /// So, we will use the ZROPS table to find the information.
        /// </summary>		
        protected override void FillTildyHeaderInfo()
        {
            DmIncluded = (DmCode.Length > 0 ? true : false);
            RegionIncluded = (RegionCode.Length > 0 ? true : false);

            if(DmIncluded)
                GetDmFolderName();
            
            if (RegionIncluded)
                GetRegionFolderName();
        
            // First Check
            if ((UnitCode.Length > 0) &&
                (RegionCode.Length == 0) &&
                (DmCode.Length == 0))
            {
                GetRegionCodeAnddmCodeFromUnitCode();
            }
            else if ((DmIncluded) && (RegionIncluded))
                GetRegionCodeFromDmCode();            
        }


        protected override void SetSaveFileName()
        {
            // Set file to save names here									
            UnitFileName = BurstServiceEntryclass.UnitFolderName + @"\" + "~" + ReportName + "~" +
                           UnitCode + "~" + RegionCode + "~" + DmCode + "~";
            DmFileName = BurstServiceEntryclass.DmFolderName + @"\" + "~" + ReportName + "~~" +
                         RegionCode + "~" + DmCode + "~";
            RegionFileName = BurstServiceEntryclass.RegionFolderName + @"\" + "~" + ReportName + "~~" +
                             RegionCode + "~~";
        }

        #region Properties

        public string RegionFileName { get; private set; }
        public string DmFileName { get; private set; }
        public string UnitFileName { get; private set; }
        public string RegionFolderName { get; private set; }
        public string DmFolderName { get; private set; }
        public string UnitFolderName { get; private set; }
        public bool RegionIncluded { get; private set; }
        public bool DmIncluded { get; private set; }

        #endregion
    } // Class
}

// Namespace