using System;
using System.Collections.Specialized;
using System.Data;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.SqlServerDal;
using MyReports.Common;

namespace MyReports.Burst.BurstProcess.Hierarchy
{
    /// <summary>
    /// Base class for the Compass Heirarchy setup.
    /// For use with the Tildy header in each report.
    /// </summary>
    public abstract class CompassHierarchy
    {
        // Report Name

        //protected FolderNames oFolderName = null;
        protected DataSet DsFolders;

        // Real report name


        // Include variables
        protected string NewReportName = "";
        protected string DmCode = "";
        protected string RegionCode = "";
        protected string ReportName = "";
        protected string UnitCode = "";

        protected CompassHierarchy(string sHeader, string sReportName, bool offCycle, DataSet dsFolderCode)
        {
            try
            {
                // Parse Header information here.				
                ParseTildyHeader(sHeader);

                // Generate Exact report name here
                NewReportName = ReportName.Length > 0 ? ReportName + " - " + sReportName : sReportName;

                if (offCycle)
                {
                    DateTime dt = DateTime.Now;

                    int n = NewReportName.LastIndexOf(" ");
                    string t1 = NewReportName.Substring(0, n) + " " + dt.ToString("MM-dd-yyyy HH:mm");
                    string t2 = NewReportName.Substring(n + 1, NewReportName.Length - n - 1);

                    NewReportName = t1.Trim() + " " + t2.Trim();
                }

                // using folder.. look in per for exact
                //oFolderName = new FolderNames(ReportDistribution.Common.Configuration.Helper.ConfigurationReader.DatabaseConnection);
                DsFolders = dsFolderCode;

            }
            catch (Exception ex)
            {
                NameValueCollection nv = new NameValueCollection
                                             {
                                                 {"Header", sHeader},
                                                 {"Report Name", sReportName},
                                                 {"Unit Code", UnitCode},
                                                 {"Dm Code", DmCode},
                                                 {"Region Code", RegionCode}
                                             };
                ExceptionManager.Publish(ex, nv);
                UnitIncluded = false;
            }
        }

        protected virtual void FillTildyHeaderInfo()
        {
            // do nothing here.. Inherited class will handle this
        }

        protected virtual void SetSaveFileName()
        {
            // do nothing here.. Inherited class will handle this
        }

        protected virtual void GetUnitFolderName(bool offCycle)
        {
            // do nothing here.. Inherited class will handle this
        }

        protected virtual void GetDmFolderName()
        {
            // do nothing here.. Inherited class will handle this
        }

        protected virtual void GetRegionFolderName()
        {
            // do nothing here.. Inherited class will handle this
        }


        protected void ParseTildyHeader(string sHeader)
        {
            TildyHeader oTildyHeader = new TildyHeader(sHeader);
            ReportName = oTildyHeader.ReportName;
            RegionCode = oTildyHeader.RegionCode;
            DmCode = oTildyHeader.DmCode;
            UnitCode = oTildyHeader.UnitCode;
        }

        protected void GetRegionCodeAnddmCodeFromUnitCode()
        {
            // Need to find the region code and the Dm Code

            Zrops oZrops = new Zrops(ConfigurationReader.Db);

            MyReports.Common.Structs.Hierarchy hierarchy = oZrops.GetRegionCodeAnddmCodeFromUnitCode(UnitCode);

            RegionCode = hierarchy.Region;
            DmCode = hierarchy.Dm;
        }

        protected void GetRegionCodeFromDmCode()
        {
            // Need to find the Region code
            Zrops oZrops = new Zrops(ConfigurationReader.Db);

            RegionCode = oZrops.GetRegionCodeFromDmCode(DmCode).Region;
        }

        public bool UnitIncluded { get; set; }
    }
}