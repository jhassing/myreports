using System.Data;
using MyReports.Burst.Common;
using MyReports.Burst.SqlServerDal;

namespace MyReports.Burst.BurstProcess.Hierarchy
{
    public sealed class CompassPayrollHierarchy : CompassHierarchy
    {
        public CompassPayrollHierarchy(string sHeader, string sReportName, bool offCycle, DataSet dsFolderCode)
            : base(sHeader, sReportName, offCycle, dsFolderCode)
        {
            if (UnitCode.Length > 0)
            {
                UnitIncluded = true;
                GetUnitFolderName(offCycle);
            }
            else
                UnitIncluded = false;


            // Populate the tildy header info, incase we are missing a Region or a Dm
            // Do not include this in the burst, this is for permission setting purposes only.
            FillTildyHeaderInfo();

            // Get the folder names to save information to
            SetSaveFileName();
        }

        public string PayrollFileName { get; set; }
        public string PayrollFolderName { get; set; }

        protected override void FillTildyHeaderInfo()
        {
            // First Check
            if ((UnitCode.Length <= 0) || (DmCode.Length != 0)) return;
            GetRegionCodeAnddmCodeFromUnitCode();

            // Blank out the region code, we will never use it for the Payroll reports
            RegionCode = "";
        }

        protected override void SetSaveFileName()
        {
            // Set file to save names here									
            PayrollFileName = BurstServiceEntryclass.PayrollFolderName + @"\" + "~" + ReportName + "~" +
                              UnitCode + "~" + RegionCode + "~" + DmCode + "~";
        }

        protected override void GetUnitFolderName(bool offCycle)
        {
            PayrollFolderName = offCycle
                                    ? "\\" + BurstServiceEntryclass.PayrollFolderName + "\\" +
                                      Functions.RemoveIllegalChars(FolderNames.GetFolderName(UnitCode, DsFolders,
                                                                                             true)) + "\\" +
                                      BurstServiceEntryclass.OffCycleFolderName + "\\" + NewReportName
                                    : "\\" + BurstServiceEntryclass.PayrollFolderName + "\\" +
                                      Functions.RemoveIllegalChars(FolderNames.GetFolderName(UnitCode, DsFolders,
                                                                                             true)) + "\\" +
                                      NewReportName;
        }
    }

    // class
}

// Namespace