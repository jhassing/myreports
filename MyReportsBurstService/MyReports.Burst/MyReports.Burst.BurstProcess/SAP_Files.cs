#region Namespace Imports

using System;
using System.IO;
using System.Timers;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;

#endregion

namespace MyReports.Burst.BurstProcess
{
    /// <summary>
    /// Summary description for SAP_Files.
    /// </summary>
    public class SAP_Files
    {
        private readonly Timer _aTimer;

        public SAP_Files()
        {
            _aTimer = new Timer
                          {
                              Interval = Functions.GetTimerInterval()
                          };
            _aTimer.Elapsed += aTimer_Elapsed;
        }


        public void StartTimer()
        {
            // Start the timer
            _aTimer.Start();
            _aTimer.Enabled = true;
        }

        public void StopTimer()
        {
            // Stop the timer
            _aTimer.Stop();
            _aTimer.Enabled = false;
        }

        private void aTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                // Stop the timer
                StopTimer();
                DeleteOldFiles();
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
            }

            // Start the timer up again
            _aTimer.Interval = Functions.GetTimerInterval();
            StartTimer();
        }

        public void DeleteOldFiles()
        {
            // Delete all files here that are older then 30 days
            // Get Directory Monitoring values			
            string sBurstDirectoryArchive = ConfigurationReader.RdSettings.ArchiveProperties.DirectoryToArchiveTo;

            double nLengthToArchiveFiles =
                Convert.ToDouble(ConfigurationReader.RdSettings.ArchiveProperties.HowLongToArchiveFor);
            if (nLengthToArchiveFiles < 30)
                nLengthToArchiveFiles = 30;

            string[] dirs = Directory.GetFiles(@sBurstDirectoryArchive, "*.*");

            foreach (string dir in dirs)
            {
                DateTime dt = File.GetCreationTime(dir);

                DateTime dtAdd = dt.AddDays(nLengthToArchiveFiles);

                if (dtAdd < DateTime.Now)
                    File.Delete(dir);
            }
        }

        public void DeleteOldFiles(int age)
        {
            // Delete all files here that are older then 'age' days, but no less than 30
            // Get Directory Monitoring values			
            string sBurstDirectoryArchive = ConfigurationReader.RdSettings.ArchiveProperties.DirectoryToArchiveTo;
            double nLengthToArchiveFiles = Convert.ToDouble(age);
            if (nLengthToArchiveFiles < 30)
                nLengthToArchiveFiles = 30;

            string[] dirs = Directory.GetFiles(@sBurstDirectoryArchive, "*.*");

            foreach (string dir in dirs)
            {
                DateTime dt = File.GetCreationTime(dir);

                DateTime dtAdd = dt.AddDays(nLengthToArchiveFiles);

                if (dtAdd < DateTime.Now)
                    File.Delete(dir);
            }
        }
    } // Class
} // Namespace