using System;
using System.Collections.Specialized;
using System.IO;
using System.Threading;
using Compass.FileSystem.ExtendedFileMonitor;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;

namespace MyReports.Burst.BurstProcess
{
    /// <summary>
    /// The Actuate Burst Windows Service, when started, creates an instance of this class.
    /// This is where all directory monitoring will take place. 
    /// </summary>
    public delegate void ThreadEventHandlerStart();

    public delegate void ThreadEventHandlerFinished(bool bReportSentToActuate);

    public delegate void AsyncDelegate();

    public class BurstServiceEntryclass
    {
        #region Member Variables

        // Static member variables
        public static string DmFolderName = "";
        public static string FileExtToLookFor = "";
        public static string FtpDirectory = "";
        public static string OffcycleFileExt = "";
        public static string OffCycleFolderName = "";
        public static string PayrollFolderName = "";
        public static string RegionFolderName = "";

        // Remoting variables
        public static string RemoteMachinePort;
        public static string SubsidyInvoice = "";
        public static string UnitFolderName = "";

        private readonly string _burstDirectory = "";

        // Directory Monitor
        private readonly ExtendedFileMonitorClass _fileMonitor;

        // Class Events
        public event ThreadEventHandlerStart ThreadStarted;
        public event ThreadEventHandlerFinished ThreadFinished;

        #endregion

        #region ~ctor

        public BurstServiceEntryclass()
        {
            try
            {
                // Get Directory Monitoring values
                _burstDirectory = ConfigurationReader.RdSettings.BurstProperties.Directory;
                                                
                FtpDirectory = ConfigurationReader.RdSettings.BurstProperties.FtpDirectory;
                FileExtToLookFor = ConfigurationReader.RdSettings.BurstProperties.FileExtToLookFor;
                OffcycleFileExt = ConfigurationReader.RdSettings.OffCycleProperties.FileExtToLookFor;
                OffCycleFolderName = ConfigurationReader.RdSettings.OffCycleProperties.FolderName;

                SubsidyInvoice = ConfigurationReader.RdSettings.SubsidyInvoiceProperties.FileExtToLookFor;

                RemoteMachinePort = ConfigurationReader.RemoteSettings.RemoteMachinePort;
                
                string[] sTempFolders = ConfigurationReader.RdSettings.FolderProperties.FolderNames.Split(',');

                // Store the base folder names
                RegionFolderName = sTempFolders[1].Trim();
                DmFolderName = sTempFolders[2].Trim();
                UnitFolderName = sTempFolders[3].Trim();
                PayrollFolderName = sTempFolders[4].Trim();

                // Register the call back event
                _fileMonitor = new ExtendedFileMonitorClass(FtpDirectory);

                _fileMonitor.FileFound += FileMonitorFileFound;
            }
            catch (Exception ex)
            {
                NameValueCollection nv = new NameValueCollection
                                             {
                                                 {"Burst Directory", _burstDirectory},
                                                 {"FTP Directory", FtpDirectory},
                                                 {"File Extention to look for", FileExtToLookFor},
                                                 {"Remote Machine-Port", RemoteMachinePort},
                                                 {"Region Folder Name", RegionFolderName},
                                                 {"Dm Folder Name", DmFolderName},
                                                 {"Unit Folder Name", UnitFolderName}
                                             };

                ExceptionManager.Publish(ex, nv);
            }
        }

        #endregion

        public void StartDirectoryMonitoring()
        {
            _fileMonitor.StartDiretoryMonitoring();
        }


        public void StopDirectoryMonitoring()
        {
            _fileMonitor.StopDirectoryMonitoring();
        }

        /// <summary>
        /// Event that gets fired once a new file is found.
        /// Once a new files is located, we create a new thread that will move the file, burst the file and
        /// then do a SOAP call to actuate to import the file.
        /// Once that process is finished, we will then move the file and delete the directory.
        /// </summary>
        /// <param name="sPath">Path of the file</param>
        private void FileMonitorFileFound(string sPath)
        {
            try
            {

                // First, we need to know what this file is? 
                if ((sPath.ToLower().EndsWith(FileExtToLookFor.ToLower())) ||
                    (sPath.ToLower().EndsWith(OffcycleFileExt.ToLower())) ||
                    (sPath.ToLower().EndsWith(SubsidyInvoice.ToLower())))
                {
                    // Sleep for 1 seconds
                    Thread.Sleep(1500);

                    string sCopyName;
                    string sBurstDir;

                    // Regular Financial or Payroll Report
                    Functions.MoveFile(sPath, _burstDirectory, out sCopyName, out sBurstDir);

                    BurstServiceThreadclass oBurst = new BurstServiceThreadclass(sCopyName, sBurstDir,
                                                                                 Path.GetExtension(sPath));

                    oBurst.ThreadStarted += BurstThreadStarted;
                    oBurst.ThreadFinished += BurstThreadFinished;

                    AsyncDelegate dlgt = oBurst.StartProcess;

                    dlgt.BeginInvoke(
                        CallbackMethod,
                        dlgt);
                }
            }
            catch (Exception ex)
            {
                NameValueCollection nv = new NameValueCollection { { "Path", sPath } };
                ExceptionManager.Publish(ex, nv);
            }
        }

        private static void CallbackMethod(IAsyncResult ar)
        {
            // Retrieve the delegate.
            AsyncDelegate dlgt = (AsyncDelegate)ar.AsyncState;

            // Call EndInvoke to retrieve the results.
            dlgt.EndInvoke(ar);
        }


        /// <summary>
        /// Event that is fired once the burst process is kicked off.
        /// We need to update the counter in the remote process
        /// </summary>
        private void BurstThreadStarted()
        {
            // Raise an event to the remoting object.
            // Tell remoting object we have started another thread.
            if (ThreadStarted != null)
                ThreadStarted();
        }

        /// <summary>
        /// Event that is fired once the burst process is finished.
        /// We need to update the counter in the remote process
        /// </summary>
        private void BurstThreadFinished(bool bReportSentToActuate)
        {
            try
            {
                if (ThreadFinished != null)
                    ThreadFinished(bReportSentToActuate);
            }
            catch (Exception ex)
            {
                NameValueCollection nv = new NameValueCollection { { "Report Sent to Actuate", bReportSentToActuate.ToString() } };
                ExceptionManager.Publish(ex, nv);
            }
        }
    } // Class
} // Namespace