using System;
using System.Collections;
using System.Collections.Specialized;
// Compass References
using Compass.Reporting.Actuate;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using Server_Proxy.localhost;



namespace ReportDistribution.SubsidyInvoice
{

    public class ActuateInterface : ActuateHelper
    {
        public ActuateInterface(string actuateUser, string actuatePassword, string actuateServer)
        {
            try
            {
                base.LoginActuate(actuateUser, actuatePassword, actuateServer);
            }
            catch (Exception ex)
            {
                NameValueCollection nv = new NameValueCollection();
                nv.Add("User Name", actuateUser);
                nv.Add("User Password", actuatePassword);
                nv.Add("Server", actuateServer);
                ExceptionManager.Publish(new Exception("Error loging into Actuate.", ex));

                throw ex;
            }
        }

        public void UploadFileToActuate(string path, string fullFilePath)
        {
            base.UploadFile(path, fullFilePath);
        }

        public void SetFilePermissions(string path, ArrayList permissions)
        {
            base.SetFolderPermissions(permissions, path, "RV", true);
        }

        public new void Dispose()
        {
            base.Dispose();
        }



        public new void RenameFileFolder(string sOldFolderName, string sNewFolderName)
        {
            base.RenameFileFolder(sOldFolderName, sNewFolderName);
        }

        public new SelectFilesResponse DoesFileExist(string sFolderPath, string sFileFolderToFind, bool latestVersionOnly, bool searchRecursive)
        {
            return base.DoesFileExist(sFolderPath, sFileFolderToFind, latestVersionOnly, searchRecursive);

        }

        public new void MoveFileFolder(string sSourceFolder, string sSourceFileFolderToMove, string sTarget, bool ReplaceExistingReport)
        {
            base.MoveFileFolder(sSourceFolder, sSourceFileFolderToMove, sTarget, ReplaceExistingReport);
        }



        public new void DeleteFileFolder(string sFolderPath, string sFileFolderToDelete)
        {
            base.DeleteFileFolder(sFolderPath, sFileFolderToDelete);
        }

        

    }
}
