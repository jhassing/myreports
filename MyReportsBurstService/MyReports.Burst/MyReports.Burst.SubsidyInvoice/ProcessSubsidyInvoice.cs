#region Namespace Imports

using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Threading;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.ReportArchive;
using MyReports.Burst.SqlServerDal;
using MyReports.Common.Structs;
using ReportDistribution.SubsidyInvoice;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.Database;
using Microsoft.Practices.EnterpriseLibrary.Logging.Filters;
using Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation;
using Server_Proxy.localhost;





#endregion

namespace MyReports.Burst.SubsidyInvoice
{
    /// <summary>
    /// Summary description for ProcessSubsidyInvoice.
    /// </summary>
    public class ProcessSubsidyInvoice : ActuateFileFolderFunctions
    {
        // Actuate interface class
        private readonly Mutex _mutexImportThread = new Mutex();
        private ActuateInterface _oActuateInterface;
        static log4net.ILog logger = log4net.LogManager.GetLogger("ProcessSubsidyInvoice");
        
        

        public void Import(string unitFolder, string filePath)
        {
            try

            {
               
                
                //Must have '~'
                if (filePath.IndexOf('~') > 0)
                {
                    string[] tmp = filePath.Split('~');

                    if (tmp.Length > 2)
                    {
                        
                        string programName = tmp[1].ToUpper();
                        string reportName = tmp[2].Replace('_', ' ') + " " + programName + ".pdf";
                        reportName = reportName.Replace("subsidy", "Subsidy");
                        reportName = reportName.Replace("invoice", "Invoice");
                        string physicalReportName = Path.GetDirectoryName(filePath) + "\\" + reportName;
                        string unitCode = tmp[3];
                        
                        Enum_ReportTypes nReportType =
                            new Reports(ConfigurationReader.Db).GetReportType(programName);

                        if (nReportType == Enum_ReportTypes.Subsidy_Invoice)
                        {
                            // Mutually exclusive code block prevent cross contamination of Subsidy Invoices importing into actuate at the same time
                            _mutexImportThread.WaitOne();
                            logger.Info(Thread.CurrentThread.ManagedThreadId + " " + tmp[3] + " : Starting Subsidy Processing");
                            // Rename file							
                            System.IO.File.Copy(filePath, physicalReportName, true);
                            logger.Info(Thread.CurrentThread.ManagedThreadId + " " + tmp[3] + " : Copying File");
                            FolderNames oFolderName = new FolderNames(ConfigurationReader.Db);
                            DataSet dsFolders = oFolderName.GetFolders();
                            
                            Thread.Sleep(3000);

                            string unitFolderName = FolderNames.GetFolderName(unitCode, dsFolders, true);

                            string destinationFolder = "/" + unitFolder + "/" + unitFolderName + "/";
                            
                            //log4net.Config.XmlConfigurator.Configure();
                            //logger.Info("TEST MESSAGE");

                            logger.Info(Thread.CurrentThread.ManagedThreadId + " " + tmp[3] + " : Getting Folder Name: " + unitFolderName + "  Destination Folder" + destinationFolder);

                            // Login to the actuate server
                            LoginToActuate();
                            
                            logger.Info(Thread.CurrentThread.ManagedThreadId + " " + tmp[3] + " : Logged Into Actuate");

                            // checks to see if the folder already exists in Actuate so it can be created if its not
                            try
                            {
                            SelectFilesResponse res = _oActuateInterface.DoesFileExist(destinationFolder,"", false, false);
                            if (res != null)
                                {
                                    //folder exists so do nothing
                                }
                            }
                                
                            catch
                            {
                                //an error is set off when the folder does not exist so this catch statement is utilized to create the folder
                                base.LoginActuate();
                                base.CreateFolder("\\" + unitFolder, "\\" + unitFolderName);
                                
                            }
                           

                            
                            
                            // Archive the previous version
                            ArchiveReports oArchiveReports = new ArchiveReports();
                            oArchiveReports.ArchiveReport(destinationFolder, reportName, programName,
                                                          GetPermissions(unitCode), nReportType);
                            logger.Info(Thread.CurrentThread.ManagedThreadId + " " + tmp[3] + " : Archive Report Complete");
                            Thread.Sleep(1000); 

                            // Upload the report
                            UploadFile(destinationFolder, physicalReportName);
                            logger.Info(Thread.CurrentThread.ManagedThreadId + " " + tmp[3] + " : Upload File Complete");
                            Thread.Sleep(1000);

                            // Set the permissions on the folder.
                            SetFilePermissions(destinationFolder, unitCode);
                            logger.Info(Thread.CurrentThread.ManagedThreadId + " " + tmp[3] + " : Set Folder Permissions Complete");
                            Thread.Sleep(500);

                            // Set the permissions on the Report.
                            SetFilePermissions(destinationFolder + "/" + reportName, unitCode);
                            logger.Info(Thread.CurrentThread.ManagedThreadId + " " + tmp[3] + " : Set Report Permissions Complete");

                            // Log to SQL Server
                            LogFinishToSqlServer(physicalReportName, destinationFolder + reportName);
                            logger.Info(Thread.CurrentThread.ManagedThreadId + " " + tmp[3] + " : Log to SQL Complete");
                            Thread.Sleep(500);

                            // Clean Up
                            CleanUp(filePath);
                            logger.Info(Thread.CurrentThread.ManagedThreadId + " " + tmp[3] + " : Clean Up Complete");

                            // end mutially exclusive code block 
                            logger.Info(Thread.CurrentThread.ManagedThreadId + " " + tmp[3] + " : Subsidy Processing Complete");
                            _mutexImportThread.ReleaseMutex();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Do nothing.. Assume the error has already been trapped.
                ExceptionManager.Publish(ex);
                logger.Info(Thread.CurrentThread.ManagedThreadId + " " +"Error Found "+ ex.Message);
            }
            finally
            {
                if (_oActuateInterface != null)
                    _oActuateInterface.Dispose();

                _oActuateInterface = null;
            }
        }
        #region Create instance of Actuate Interface class and Login to actuate

        private void LoginToActuate()
        {
            try
            {
                _oActuateInterface = new ActuateInterface(ConfigurationReader.ActuateProperties.AdminUserName,
                                                          ConfigurationReader.ActuateProperties.AdminPassword,
                                                          ConfigurationReader.ActuateProperties.Url);
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
                throw;
            }
        }

        #endregion

        #region Upload File

        private void UploadFile(string destinationFolder, string physicalFilePath)
        {
            try
            {
                _oActuateInterface.UploadFileToActuate(destinationFolder, physicalFilePath);
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
                throw;
            }
        }

        #endregion

        #region Set the report permissions

        private void SetFilePermissions(string fileName, string unit)
        {
            try
            {
                _oActuateInterface.SetFilePermissions(fileName, GetPermissions(unit));
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region Get Permissions

        private static ArrayList GetPermissions(string unit)
        {
            string csGroupExt = ConfigurationReader.RdSettings.GroupPermissionProperties.FinancialReports;

            // get the permissions for this report from the database
            Zrops oZrops = new Zrops(ConfigurationReader.Db);

            Hierarchy hierarchy = oZrops.GetRegionCodeAnddmCodeFromUnitCode(unit);

            ArrayList roles = new ArrayList();

            if (hierarchy.Region.Length > 0) roles.Add(hierarchy.Region + csGroupExt);
            if (hierarchy.Dm.Length > 0) roles.Add(hierarchy.Dm + csGroupExt);

            roles.Add(unit + csGroupExt);

            return roles;
        }

        #endregion

        #region Log Subsidy Invoice to SQL Server

        private static void LogFinishToSqlServer(string physicalReportName, string destinationFolder)
        {
            try
            {
                // Open the ID text file, to find the primary lek field for the database.
                string sIDFileName = Path.GetDirectoryName(physicalReportName) + @"\dbid";
                int nKey = 0;
                using (StreamReader sr = new StreamReader(sIDFileName))
                {
                    while (sr.Peek() >= 0)
                    {
                        string sTemp = sr.ReadLine();
                        nKey = Convert.ToInt32(sTemp);
                    }
                }

                //InsertBurstProcessStatusReportLocations
                BurstProcessStatus oBurstProcessStatus = new BurstProcessStatus(ConfigurationReader.Db);
                //Do not change the / characters in the report pathname to \ anymore
                //oBurstProcessStatus.InsertBurstProcessStatusReportLocations(nKey, destinationFolder.Replace('/', '\\'));
                oBurstProcessStatus.InsertBurstProcessStatusReportLocations(nKey, destinationFolder);
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(new Exception("Error logging Subsidy Invoice to SQL Server", ex));
            }
        }

        #endregion
        

        #region Clean Up

        private static void CleanUp(string filePath)
        {
            try
            {
                // Move the file to the archive folder once finished
                string sArchiveDir = ConfigurationReader.RdSettings.ArchiveProperties.DirectoryToArchiveTo;

                if (System.IO.File.Exists(sArchiveDir + Path.GetFileName(filePath)))
                    System.IO.File.Delete(sArchiveDir + Path.GetFileName(filePath));

                System.IO.File.Move(filePath, sArchiveDir + Path.GetFileName(filePath));

                // Delete the temporary folder
                Directory.Delete(Path.GetDirectoryName(filePath), true);
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(new Exception("Error cleaning up after Subsidy Invoice", ex));
            }
        }

        #endregion

    }
}