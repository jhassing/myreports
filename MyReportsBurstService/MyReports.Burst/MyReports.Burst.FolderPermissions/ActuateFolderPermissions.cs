#region Namespace Imports

using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using Compass.Utilities.Email;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.RemotingObject;
using MyReports.Burst.ReportPermissions;
using MyReports.Burst.SqlServerDal;

#endregion

namespace MyReports.Burst.FolderPermissions
{
    /// <summary>
    /// Public Interface, must be included so this project can be called form a non-managed (i.e. VB6.0) application, 
    /// just like any other COM object.
    /// </summary>
    [Guid("80D8E15F-4C11-4f35-A034-4FFD1E3A91E9")]
    internal interface IActuateFolderPermissions
    {
        void SetActuateFolderPermissions(string sMasterFileLocation);
    }

    [Guid("ADEEE4AA-24C0-4621-8B59-1C3DF13F2C79")]
    public class clsSetActuateFolderPermissions : IActuateFolderPermissions
    {
        #region IActuateFolderPermissions Members

        public void SetActuateFolderPermissions(string sMasterFileLocation)
        {
            FolderPermissions fpSet = new FolderPermissions();
            fpSet.SetActuateFolderPermissions(sMasterFileLocation);
        }

        #endregion
    }

    internal class FolderPermissions : ActuateLoginBaseClass
    {
        /// <summary>
        /// This public function is called from within Actuate, once the burst import process is complete.
        /// It will loop back around the bursted files, and set the permissions on each file and folder that was imported.
        /// The groups that are being set, must reside in Active Directory.
        /// </summary>
        /// <param name="sMasterFileLocation">Location of the Master Text file listing, which hold the physical location of each bursted file.</param>
        public void SetActuateFolderPermissions(string sMasterFileLocation)
        {
            
            StreamReader srFromFile = null;
            ArrayList aryGroupArray = null;
            int nId = 0;

            // Verify we can fin the config file
            if (!File.Exists(ConfigFileLocation.APP_CONFIG_LOCATION))
                ExceptionManager.Publish(
                    new FileNotFoundException("Could not find config file: " + ConfigFileLocation.APP_CONFIG_LOCATION));

            // Log to SQL Server, we are ready to start the Set Permissions process
            BurstProcessStatus oBurstProcessStatus = new BurstProcessStatus(ConfigurationReader.Db);

            ArrayList aryAdminGroupsFromDb = new ArrayList();

            Reports oReports =
                new Reports(ConfigurationReader.Db);

            try
            {
                // Get the Admin groups	                
                aryAdminGroupsFromDb = oReports.GetAdminGroups();
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
            }

            try
            {
                string sLine;

                srFromFile = new StreamReader(@sMasterFileLocation);

                while (srFromFile.Peek() > -1)
                {
                    try
                    {
                        // Reset the admin groups here
                        ArrayList aryAdminGroups = aryAdminGroupsFromDb;

                        sLine = srFromFile.ReadLine();

                        SplitMasterTextFileLine oSplit = new SplitMasterTextFileLine(sLine);

                        // First, we need to know what kind of report this is in order to find out which "????" we need for group assignments
                        string[] split = sLine.Split('|');

                        string[] sFolders = oSplit.Folders;
                        string reportName = oSplit.ReportName;

                        string sFolderName = "";
                        string sReportNamePath = "";

                        Enum_ReportTypes reportType = (Enum_ReportTypes) Convert.ToInt32(split[2]);

                        if (reportType == Enum_ReportTypes.Off_Cycle)
                            sFolderName = "\\" + sFolders[1] + "\\" + sFolders[2] + "\\" + sFolders[3];

                        else
                            sFolderName = "\\" + sFolders[1] + "\\" + sFolders[2];

                        sReportNamePath = sFolderName + "\\" + reportName;

                        #region Get the Admin groups for this report code

                        int nLoc1 = split[1].LastIndexOf(' ');

                        string reportCode = split[1].Substring(nLoc1, split[1].Length - nLoc1).Trim();

                        ArrayList aryReportAdminGroups = oReports.GetReportAdminGroups(reportCode);

                        #endregion

                        #region Get the permissions, based on wether this is a Financial or Payroll report

                        switch (reportType)
                        {
                            case Enum_ReportTypes.Financial:
                                aryGroupArray = new FinancialPermissions().GetFolderPermissionGroups(sLine);
                                break;

                            case Enum_ReportTypes.Payroll:
                                aryGroupArray = new PayrollPermissions().GetFolderPermissionGroups(sLine);
                                break;

                            case Enum_ReportTypes.Off_Cycle:
                                aryGroupArray = new PayrollPermissions().GetFolderPermissionGroups(sLine);
                                break;
                            case Enum_ReportTypes.Profiles:
                                aryGroupArray = new ProfilePermissions().GetFolderPermissionGroups(sLine);
                                break;
                        }

                        #endregion

                        // Add all the admin groups to the folder permissions
                        SetFolderPermissions(aryAdminGroups, sFolderName, "VSR", false);
                        SetFolderPermissions(aryGroupArray, sFolderName, "VSR", false);

                        if (reportType == Enum_ReportTypes.Off_Cycle)
                        {
                            string basePayrollFolder = "\\" + sFolders[1] + "\\" + sFolders[2];

                            SetFolderPermissions(aryAdminGroups, basePayrollFolder, "VSR", false);
                            SetFolderPermissions(aryGroupArray, basePayrollFolder, "VSR", false);
                        }

                        // Add just the proper admin group to this report
                        SetFolderPermissions(aryReportAdminGroups, sReportNamePath, "VSR", false);
                        SetFolderPermissions(aryGroupArray, sReportNamePath, "VSR", false);
                    }
                    catch (Exception ex)
                    {
                        NameValueCollection nv = new NameValueCollection {{"Master File Location", sMasterFileLocation}};
                        ExceptionManager.Publish(ex, nv);

                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                NameValueCollection nv = new NameValueCollection {{"Master File Location", sMasterFileLocation}};
                ExceptionManager.Publish(ex, nv);
            }
            finally
            {
                try
                {
                    srFromFile.Close();
                    base.Dispose();
                }
                catch
                {
                }
            }

            #region Decrement the actuate counter

            try
            {
                // Decement the counter by 1
                RemotingService service = (RemotingService) Activator.GetObject(typeof (RemotingService),
                                                                                ConfigurationReader.RemoteSettings.
                                                                                    RemoteMachinePort);

                service.ActuateRunningCountSubtract();
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(new Exception("Could not decrement the counter", ex));
            }

            #endregion

            #region Send email notification the report is complete

            // Send of the email to whomever needs it, letting them know the reports have indeed burst
            try
            {
                // Open the ID text file, to find the primary lek field for the database.
                string sIdFileName = Path.GetDirectoryName(sMasterFileLocation) + @"\dbid";
                using (StreamReader sr = new StreamReader(sIdFileName))
                {
                    while (sr.Peek() >= 0)
                    {
                        string sTemp = sr.ReadLine();
                        nId = Convert.ToInt32(sTemp);
                    }
                }
                // Loop through the MasterFileList
                // Add each line to the report.
                string[] sLine;
                string sPath = "";
                StringBuilder sb = new StringBuilder();

                ArrayList ar = new ArrayList();

                using (StreamReader sr = new StreamReader(sMasterFileLocation))
                {
                    while (sr.Peek() >= 0)
                    {
                        sLine = sr.ReadLine().Split('|');
                        //sPath = sLine[1];
                        sPath = sLine[1].Replace('\\', '/') + ".roi";

                        string[] sParms = new string[1];
                        sParms[0] = "TimeStamp";
                        //AcGetFileDetails ddd = new AcGetFileDetails(sPath, null, sParms);

                        ar.Add(sPath);
                        // Save to database here								
                        oBurstProcessStatus.InsertBurstProcessStatusReportLocations(nId, sPath);
                    }
                }

                ar.Sort();

                // Get the report name
                string[] ss = sPath.Split('/');
                string sReportName = "";

                if (ss.Length > 2)
                    sReportName = ss[3];

                foreach (string sString in (IEnumerable) ar)
                {
                    sb.Append(sString + "<br>");
                }

                MailAddressCollection macTo = new MailAddressCollection();
                string[] sToListing = ConfigurationReader.RdSettings.EmailProperties.ReportFinsihedEmailTo.Split(';');

                foreach (string stmp22 in sToListing)
                {
                    macTo.Add(new MailAddress(stmp22));
                }

                try
                {
                    EmailHelper.SendMail(
                        ConfigurationReader.RdSettings.EmailProperties.SmtpServer, // SMTP Server
                        "Report " + sReportName + " finished", //Subject
                        sb.ToString(), //Message Body
                        macTo,
                        new MailAddress(ConfigurationReader.RdSettings.EmailProperties.EmailFromAccount), // From
                        null,
                        null,
                        null,
                        MailPriority.Normal,
                        true); // Mail Priority
                }
                catch (Exception ex)
                {
                    // Do something here
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
            }

            #endregion

            // Delete the files we were using
            try
            {
                Functions.MoveSapReport(ConfigurationReader.RdSettings.ArchiveProperties.DirectoryToArchiveTo,
                                        sMasterFileLocation);

                Functions.DeleteBurstFolder(sMasterFileLocation);
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
            }

            // Log to SQL Server, we are finished.. Wrap it up baby	
            oBurstProcessStatus.InsertBurstProcessStatusBurstProcessStatusCodes(nId,
                                                                                (int)
                                                                                Enum_BurstStatusCodes.
                                                                                    FinishedSettingFolderPermissions);

            // Clean up
            //oBurstProcessStatus.Dispose();
        }
    }
}