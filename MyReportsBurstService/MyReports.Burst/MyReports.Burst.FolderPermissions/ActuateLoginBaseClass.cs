using System;
using MyReports.Burst.Common.Configuration.Helper;
using Compass.Reporting.Actuate;
using Microsoft.ApplicationBlocks.ExceptionManagement;

namespace MyReports.Burst.FolderPermissions
{
    public class ActuateLoginBaseClass : ActuateHelper
    {
        public ActuateLoginBaseClass()
        {
            try
            {
                string sUserName = ConfigurationReader.ActuateProperties.AdminUserName;
                string sPassword = ConfigurationReader.ActuateProperties.AdminPassword;
                string url = ConfigurationReader.ActuateProperties.Url;

                // Login to actuate
                LoginActuate(sUserName, sPassword, url); //, out l_proxy);
            }
            catch (Exception e)
            {
                ExceptionManager.Publish(e);
            }

        }
    }


}
