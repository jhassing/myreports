#region

using System;
using MyReports.Common;

#endregion

namespace MyReports.Burst.Common
{
    /// <summary>
    /// This class will inherit from clsBase. Inorder to use this class, another class must inherite from this one.
    /// This class will convert a Master Text File List line, C:\ActuateBurst\tfd9wMa\(2) District\~~fbm~fbm01~|/(2) District/fbm01 - Daley,  B/op7497 - Operating Ledger
    /// into several pieces, which will be used throughout the many applications.
    /// </summary>
    public class SplitMasterTextFileLine
    {
        /// <summary>
        /// Funtion will split a single line from the Master Text File List into the
        /// Actuate Folders and Sub Folders along with the report name
        /// </summary>
        /// <param name="sLine">ex: C:\ActuateBurst\tfd9wMa\(2) District\~~fbm~fbm01~|/(2) District/fbm01 - Daley,  B/op7497 - Operating Ledger.roi</param>
        public SplitMasterTextFileLine(string sLine)
        {
            // Get the Actuate Folder names
            // ex. /(1) Accounting/cho - Mid Atlantic/ReportName.roi

            string[] pipeSplit = sLine.Split('|');
            string[] folders = pipeSplit[1].Split('\\');

            if (folders.Length <= 0) return;
            string[] temp = new string[folders.Length - 1];

            for (int x = 0; x < folders.Length - 1; x++)
                temp[x] = folders[x];

            Folders = temp;

            ReportName = folders[folders.Length - 1] + ".roi";

            int last = ReportName.LastIndexOf(" ");
            ReportCode = ReportName.Substring(last, ReportName.Length - last - 4).Trim();

            ReportType = (Enum_ReportTypes) Convert.ToInt32(pipeSplit[2]);
        }

        public string[] Folders { get; private set; }
        public string ReportName { get; private set; }
        public string ReportCode { get; private set; }
        public Enum_ReportTypes ReportType { get; private set; }
    }
}