﻿using System;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class ActiveDirectorySettings : SerializableConfigurationSection
    {
        private const string _serviceAccountUserProperty = "ServiceAccountUser";
        private const string _serviceAccountPasswordProperty = "ServiceAccountPassword";
        private const string _ldapHostProperty = "LdapHost";
        private const string _baseDnProperty = "BaseDn";
        private const string _ldapPortProperty = "LdapPort";
        private const string _maxResultesProperty = "MaxResultes";

        public const string SectionName = "ActiveDirectoryProperties";

        public static ActiveDirectorySettings GetActiveDirectoryProperties(IConfigurationSource configurationSource)
        {
            return (ActiveDirectorySettings)configurationSource.GetSection(SectionName);
        }

        public ActiveDirectorySettings() { }

        // Create the element.
        public ActiveDirectorySettings(string serviceAccountUser, string serviceAccountPassword, string ldapHost,
                                       string baseDn, int ldapPort, int maxResultes)
        {
            ServiceAccountUser = serviceAccountUser;
            ServiceAccountPassword = serviceAccountPassword;
            MaxResultes = maxResultes;
            LdapHost = ldapHost;
            BaseDn = baseDn;
            LdapPort = ldapPort;
        }

        [ConfigurationProperty(_serviceAccountUserProperty)]
        public string ServiceAccountUser
        {
            get { return (String)this[_serviceAccountUserProperty]; }
            set { this[_serviceAccountUserProperty] = value; }
        }

        [ConfigurationProperty(_serviceAccountPasswordProperty)]
        public string ServiceAccountPassword
        {
            get { return (String)this[_serviceAccountPasswordProperty]; }
            set { this[_serviceAccountPasswordProperty] = value; }
        }

        [ConfigurationProperty(_ldapHostProperty)]
        public string LdapHost
        {
            get { return (String)this[_ldapHostProperty]; }
            set { this[_ldapHostProperty] = value; }
        }

        [ConfigurationProperty(_baseDnProperty)]
        public string BaseDn
        {
            get { return (String)this[_baseDnProperty]; }
            set { this[_baseDnProperty] = value; }
        }

        [ConfigurationProperty(_ldapPortProperty)]
        public int LdapPort
        {
            get { return (int)this[_ldapPortProperty]; }
            set { this[_ldapPortProperty] = value; }
        }

        [ConfigurationProperty(_maxResultesProperty)]
        public int MaxResultes
        {
            get { return (int)this[_maxResultesProperty]; }
            set { this[_maxResultesProperty] = value; }
        }

    }
}