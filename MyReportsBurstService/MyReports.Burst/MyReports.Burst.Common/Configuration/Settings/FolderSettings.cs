﻿using System;
using System.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class FolderSettings : ConfigurationElement
    {
        private const string folderNamesProperty = "FolderNames";

        public FolderSettings(){}

        // Create the element.
        public FolderSettings(string folderNames)
        {
            FolderNames = folderNames;
        }

        [ConfigurationProperty(folderNamesProperty)]
        public string FolderNames
        {
            get { return (String)this[folderNamesProperty]; }
            set { this[folderNamesProperty] = value; }
        }
    }
}