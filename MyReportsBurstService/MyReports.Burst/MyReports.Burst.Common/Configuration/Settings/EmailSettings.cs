﻿using System;
using System.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class EmailSettings : ConfigurationElement
    {
        private const string smtpServerProperties = "SmtpServer";
        private const string reportFinsihedEmailToProperties = "ReportFinsihedEmailTo";
        private const string emailFromAccountProperties = "EmailFromAccount";
        private const string exceptionEmailListingProperties = "ExceptionEmailListing";
        private const string dailyEmailListingProperties = "DailyEmailListing";

        public EmailSettings()
        {
        }

        // Create the element.
        public EmailSettings(string sMtpServer, string reportFinsihedEmailTo, string emailFromAccount, string exceptionEmailListing,
                             string dailyEmailListing)
        {
            SmtpServer = sMtpServer;
            ReportFinsihedEmailTo = reportFinsihedEmailTo;
            EmailFromAccount = emailFromAccount;
            ExceptionEmailListing = exceptionEmailListing;
            DailyEmailListing = dailyEmailListing;
        }

        [ConfigurationProperty(smtpServerProperties)]
        public string SmtpServer
        {
            get { return (String)this[smtpServerProperties]; }
            set { this[smtpServerProperties] = value; }
        }

        [ConfigurationProperty(reportFinsihedEmailToProperties)]
        public string ReportFinsihedEmailTo
        {
            get { return (String)this[reportFinsihedEmailToProperties]; }
            set { this[reportFinsihedEmailToProperties] = value; }
        }

        [ConfigurationProperty(emailFromAccountProperties)]
        public string EmailFromAccount
        {
            get { return (String)this[emailFromAccountProperties]; }
            set { this[emailFromAccountProperties] = value; }
        }

        [ConfigurationProperty(exceptionEmailListingProperties)]
        public string ExceptionEmailListing
        {
            get { return this[exceptionEmailListingProperties] as string; }
            set { this[exceptionEmailListingProperties] = value; }
        }

        [ConfigurationProperty(dailyEmailListingProperties)]
        public string DailyEmailListing
        {
            get { return (String)this[dailyEmailListingProperties]; }
            set { this[dailyEmailListingProperties] = value; }
        }
    }
}