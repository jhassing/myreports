﻿using System;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class RemotingSettings : SerializableConfigurationSection
    {
        private const string remoteMachineProperty = "RemoteMachine";
        private const string remotePortProperty = "RemotingPort";

        public const string SectionName = "RemotingProperties";

        public static RemotingSettings GetRemotingSettings(IConfigurationSource configurationSource)
        {
            return (RemotingSettings)configurationSource.GetSection(RemotingSettings.SectionName);
        }

        // Create the element.
        public RemotingSettings(string remoteMachine, int remotingPort)
        {
            RemoteMachine = remoteMachine;
            RemotingPort = remotingPort;
        }
        public RemotingSettings() { }

        [ConfigurationProperty(remoteMachineProperty)]
        public string RemoteMachine
        {
            get { return (String)this[remoteMachineProperty]; }
            set { this[remoteMachineProperty] = value; }
        }

        [ConfigurationProperty(remotePortProperty)]
        public int RemotingPort
        {
            get { return (int)this[remotePortProperty]; }
            set { this[remotePortProperty] = value; }
        }

        public string RemoteMachinePort
        {
            get { return "tcp://" + this.RemoteMachine + ":" + this.RemotingPort + "/RemotingService"; }
        }
    }
}