﻿using System;
using System.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class BurstSettings : ConfigurationElement
    {
        private const string directoryProperty = "Directory";
        private const string adminDirectoryProperty = "AdminDirectory";
        private const string ftpDirectoryProperty = "FtpDirectory";
        private const string fileExtToLookForProperty = "FileExtToLookFor";
        private const string folderRenameFileExtToLookForProperty = "FolderRenameFileExtToLookFor";

        public BurstSettings(){}

        // Create the element.
        public BurstSettings(string directory, string adminDirectory, string ftpDirectory,
                             string fileExtToLookFor, string folderRenameFileExtToLookFor)
        {
            Directory = directory;
            AdminDirectory = adminDirectory;
            FtpDirectory = ftpDirectory;
            FileExtToLookFor = fileExtToLookFor;
            FolderRenameFileExtToLookFor = folderRenameFileExtToLookFor;
        }

        [ConfigurationProperty(directoryProperty)]
        public string Directory
        {
            get { return (String)this[directoryProperty]; }
            set { this[directoryProperty] = value; }
        }

        [ConfigurationProperty(adminDirectoryProperty)]
        public string AdminDirectory
        {
            get { return (String)this[adminDirectoryProperty]; }
            set { this[adminDirectoryProperty] = value; }
        }

        [ConfigurationProperty(ftpDirectoryProperty)]
        public string FtpDirectory
        {
            get { return (String)this[ftpDirectoryProperty]; }
            set { this[ftpDirectoryProperty] = value; }
        }

        [ConfigurationProperty(fileExtToLookForProperty)]
        public string FileExtToLookFor
        {
            get { return (String)this[fileExtToLookForProperty]; }
            set { this[fileExtToLookForProperty] = value; }
        }

        [ConfigurationProperty(folderRenameFileExtToLookForProperty)]
        public string FolderRenameFileExtToLookFor
        {
            get { return (String)this[folderRenameFileExtToLookForProperty]; }
            set { this[folderRenameFileExtToLookForProperty] = value; }
        }
    }
}