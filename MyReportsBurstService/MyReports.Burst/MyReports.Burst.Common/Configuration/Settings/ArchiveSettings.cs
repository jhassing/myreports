﻿using System;
using System.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class ArchiveSettings : ConfigurationElement
    {
        private const string directoryToArchiveToProperty = "DirectoryToArchiveTo";
        private const string howLongToArchiveForProperty = "HowLongToArchiveFor";
        private const string daysToHoldReportsProperty = "DaysToHoldReports";
        private const string archiveFolderMonthsProperty = "ArchiveFolderMonths";

        public ArchiveSettings(){}

        // Create the element.
        public ArchiveSettings(string directory, string adminDirectory, string ftpDirectory, string fileExtToLookFor)
        {
            DirectoryToArchiveTo = directory;
            HowLongToArchiveFor = adminDirectory;
            DaysToHoldReports = ftpDirectory;
            ArchiveFolderMonths = fileExtToLookFor;
        }

        [ConfigurationProperty(directoryToArchiveToProperty)]
        public string DirectoryToArchiveTo
        {
            get { return (String)this[directoryToArchiveToProperty]; }
            set { this[directoryToArchiveToProperty] = value; }
        }

        [ConfigurationProperty(howLongToArchiveForProperty)]
        public string HowLongToArchiveFor
        {
            get { return (String)this[howLongToArchiveForProperty]; }
            set { this[howLongToArchiveForProperty] = value; }
        }

        [ConfigurationProperty(daysToHoldReportsProperty)]
        public string DaysToHoldReports
        {
            get { return (String)this[daysToHoldReportsProperty]; }
            set { this[daysToHoldReportsProperty] = value; }
        }

        [ConfigurationProperty(archiveFolderMonthsProperty)]
        public string ArchiveFolderMonths
        {
            get { return (String)this[archiveFolderMonthsProperty]; }
            set { this[archiveFolderMonthsProperty] = value; }
        }
    }
}