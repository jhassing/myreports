﻿using System;
using System.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class CharactersToReplaceSettings : ConfigurationElement
    {
        private const string charactersToReplaceProperty = "CharactersToReplace";

        public CharactersToReplaceSettings(){}

        // Create the element.
        public CharactersToReplaceSettings(string charactersToReplace)
        {
            CharactersToReplace = charactersToReplace;
        }

        [ConfigurationProperty(charactersToReplaceProperty)]
        public string CharactersToReplace
        {
            get { return (String)this[charactersToReplaceProperty]; }
            set { this[charactersToReplaceProperty] = value; }
        }
    }
}