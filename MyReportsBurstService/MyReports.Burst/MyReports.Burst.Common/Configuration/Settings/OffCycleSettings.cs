﻿using System;
using System.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class OffCycleSettings : ConfigurationElement
    {
        private const string folderNameProperty = "FolderName";
        private const string fileExtToLookForProperty = "FileExtToLookFor";        

        public OffCycleSettings(){}

        // Create the element.
        public OffCycleSettings(string folderName, string fileExtToLookFor)
        {
            FolderName = folderName;
            FileExtToLookFor = fileExtToLookFor;
        }

        [ConfigurationProperty(folderNameProperty)]
        public string FolderName
        {
            get { return (String)this[folderNameProperty]; }
            set { this[folderNameProperty] = value; }
        }

        [ConfigurationProperty(fileExtToLookForProperty)]
        public string FileExtToLookFor
        {
            get { return (String)this[fileExtToLookForProperty]; }
            set { this[fileExtToLookForProperty] = value; }
        }
    }
}