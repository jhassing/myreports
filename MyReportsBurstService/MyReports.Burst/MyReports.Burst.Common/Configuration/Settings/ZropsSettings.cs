﻿using System;
using System.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{

    public class ZropsSettings : ConfigurationElement
    {
        private const string zropsFileExtToLookForProperty = "ZropsFileExtToLookFor";
        private const string zropsTempImportDirProperties = "ZropsTempImportDir";
        private const string zropsDataArchiveDirProperties = "ZropsDataArchiveDir";

        public ZropsSettings() { }

        public ZropsSettings(string zropsFileExtToLookFor, string zropsTempImportDir, string zropsDataArchiveDir)
        {
            ZropsDataArchiveDir = zropsDataArchiveDir;
            ZropsTempImportDir = zropsTempImportDir;
            ZropsFileExtToLookFor = zropsFileExtToLookFor;
        }


        [ConfigurationProperty(zropsFileExtToLookForProperty)]
        public string ZropsFileExtToLookFor
        {
            get { return (String)this[zropsFileExtToLookForProperty]; }
            set { this[zropsFileExtToLookForProperty] = value; }
        }


        [ConfigurationProperty(zropsTempImportDirProperties)]
        public string ZropsTempImportDir
        {
            get { return (String)this[zropsTempImportDirProperties]; }
            set { this[zropsTempImportDirProperties] = value; }
        }


        [ConfigurationProperty(zropsDataArchiveDirProperties)]
        public string ZropsDataArchiveDir
        {
            get { return (String)this[zropsDataArchiveDirProperties]; }
            set { this[zropsDataArchiveDirProperties] = value; }
        }
    }
}
