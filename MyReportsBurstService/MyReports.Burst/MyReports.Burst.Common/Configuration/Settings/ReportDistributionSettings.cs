﻿using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class ReportDistributionSettings : ConfigurationSection
    {
        private const string burstProperties = "BurstProcessProperties";
        private const string zropsProperties = "ZropsProperties";
        private const string emailProperties = "EmailProperties";
        private const string charactersToReplaceProperties = "CharactersToReplaceProperties";
        private const string subsidyInvoiceProperties = "SubsidyInvoiceProperties";
        private const string offCycleProperties = "OffCycleProperties";
        private const string groupPermissionProperties = "GroupPermissionProperties";
        private const string folderProperties = "FolderProperties";
        private const string archiveProperties = "ArchiveProperties";

        public const string SectionName = "ReportDistributionProperties";

        public static ReportDistributionSettings GetReportDistributionSettings(IConfigurationSource configurationSource)
        {
            return (ReportDistributionSettings)configurationSource.GetSection(ReportDistributionSettings.SectionName);
        }

        public ReportDistributionSettings() { }

        // Create the element.
        public ReportDistributionSettings(BurstSettings burstSettings, ZropsSettings zropsSettings, EmailSettings emailSettings,
                                          CharactersToReplaceSettings charactersToReplaceSettings, SubsidyInvoiceSettings subsidyInvoiceSettings,
                                          OffCycleSettings offCycleSettings, GroupPermissionSettings groupPermissionSettings, FolderSettings folderSettings,
                                          ArchiveSettings archiveSettings)
        {
            BurstProperties = burstSettings;
            ZropsProperties = zropsSettings;
            EmailProperties = emailSettings;
            CharactersToReplaceProperties = charactersToReplaceSettings;
            SubsidyInvoiceProperties = subsidyInvoiceSettings;
            OffCycleProperties = offCycleSettings;
            GroupPermissionProperties = groupPermissionSettings;
            FolderProperties = folderSettings;
            FolderProperties = folderSettings;
            ArchiveProperties = archiveSettings;
        }

        [ConfigurationProperty(burstProperties)]
        public BurstSettings BurstProperties
        {
            get { return (BurstSettings) base[burstProperties]; }
            set { base[burstProperties] = value; }
        }

        [ConfigurationProperty(zropsProperties)]
        public ZropsSettings ZropsProperties
        {
            get { return (ZropsSettings)base[zropsProperties]; }
            set { base[zropsProperties] = value; }
        }

        [ConfigurationProperty(emailProperties)]
        public EmailSettings EmailProperties
        {
            get { return (EmailSettings)base[emailProperties]; }
            set { base[emailProperties] = value; }
        }

        [ConfigurationProperty(charactersToReplaceProperties)]
        public CharactersToReplaceSettings CharactersToReplaceProperties
        {
            get { return (CharactersToReplaceSettings)base[charactersToReplaceProperties]; }
            set { base[charactersToReplaceProperties] = value; }
        }

        [ConfigurationProperty(subsidyInvoiceProperties)]
        public SubsidyInvoiceSettings SubsidyInvoiceProperties
        {
            get { return (SubsidyInvoiceSettings)base[subsidyInvoiceProperties]; }
            set { base[subsidyInvoiceProperties] = value; }
        }

        [ConfigurationProperty(offCycleProperties)]
        public OffCycleSettings OffCycleProperties
        {
            get { return (OffCycleSettings)base[offCycleProperties]; }
            set { base[offCycleProperties] = value; }
        }

        [ConfigurationProperty(groupPermissionProperties)]
        public GroupPermissionSettings GroupPermissionProperties
        {
            get { return (GroupPermissionSettings)base[groupPermissionProperties]; }
            set { base[groupPermissionProperties] = value; }
        }

        [ConfigurationProperty(folderProperties)]
        public FolderSettings FolderProperties
        {
            get { return (FolderSettings)base[folderProperties]; }
            set { base[folderProperties] = value; }
        }

        [ConfigurationProperty(archiveProperties)]
        public ArchiveSettings ArchiveProperties
        {
            get { return (ArchiveSettings)base[archiveProperties]; }
            set { base[archiveProperties] = value; }
        }

    }
}