﻿using System;
using System.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class GroupPermissionSettings : ConfigurationElement
    {
        private const string financialReportsProperty = "FinancialReports";
        private const string payrollSalaryProperty = "PayrollSalary";
        private const string payrollHourlyProperty = "PayrollHourly";

        public GroupPermissionSettings(){}

        // Create the element.
        public GroupPermissionSettings(string financialReports, string payrollSalary, string payrollHourly)
        {
            FinancialReports = financialReports;
            PayrollSalary = payrollSalary;
            PayrollHourly = payrollHourly;
        }

        [ConfigurationProperty(financialReportsProperty)]
        public string FinancialReports
        {
            get { return (String)this[financialReportsProperty]; }
            set { this[financialReportsProperty] = value; }
        }

        [ConfigurationProperty(payrollSalaryProperty)]
        public string PayrollSalary
        {
            get { return (String)this[payrollSalaryProperty]; }
            set { this[payrollSalaryProperty] = value; }
        }

        [ConfigurationProperty(payrollHourlyProperty)]
        public string PayrollHourly
        {
            get { return (String)this[payrollHourlyProperty]; }
            set { this[payrollHourlyProperty] = value; }
        }
    }
}