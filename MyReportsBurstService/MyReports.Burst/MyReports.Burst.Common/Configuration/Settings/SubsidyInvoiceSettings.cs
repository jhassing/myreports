﻿using System;
using System.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class SubsidyInvoiceSettings : ConfigurationElement
    {
        private const string fileExtToLookForProperty = "FileExtToLookFor";

        public SubsidyInvoiceSettings(){}

        // Create the element.
        public SubsidyInvoiceSettings(string fileExtToLookFor)
        {
            FileExtToLookFor = fileExtToLookFor;
        }

        [ConfigurationProperty(fileExtToLookForProperty)]
        public string FileExtToLookFor
        {
            get { return (String)this[fileExtToLookForProperty]; }
            set { this[fileExtToLookForProperty] = value; }
        }
    }
}