﻿using System;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace MyReports.Burst.Common.Configuration.Settings
{
    public class ActuateSettings : SerializableConfigurationSection
    {
        private const string adminUserNameProperty = "AdminUserName";
        private const string adminPasswordProperty = "AdminPassword";
        private const string urlProperty = "Url";

        public const string SectionName = "ActuateProperties";

        public static ActuateSettings GetActuateProperties(IConfigurationSource configurationSource)
        {
            return (ActuateSettings)configurationSource.GetSection(ActuateSettings.SectionName);
        }

        public ActuateSettings() { }

        // Create the element.
        public ActuateSettings(string adminUserName, string adminPassword, string url)
        {
            AdminUserName = adminUserName;
            AdminPassword = adminPassword;
            Url = url;
        }

        [ConfigurationProperty(adminUserNameProperty)]
        public string AdminUserName
        {
            get { return (String)this[adminUserNameProperty]; }
            set { this[adminUserNameProperty] = value; }
        }

        [ConfigurationProperty(adminPasswordProperty)]
        public string AdminPassword
        {
            get { return (String)this[adminPasswordProperty]; }
            set { this[adminPasswordProperty] = value; }
        }

        [ConfigurationProperty(urlProperty)]
        public string Url
        {
            get { return (String)this[urlProperty]; }
            set { this[urlProperty] = value; }
        }
    }
}
