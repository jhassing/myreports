﻿using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Burst.Common.Configuration.Settings;

namespace MyReports.Burst.Common.Configuration.Helper
{
    public class ConfigurationReader
    {
        public static ReportDistributionSettings RdSettings;
        public static RemotingSettings RemoteSettings;
        public static ActuateSettings ActuateProperties;
        public static ActiveDirectorySettings ActiveDirectoryProperties;

        public static Database Db;

        public static string DatabaseConnection;

        static ConfigurationReader()
        {
            string fileName;

//#if DEBUG
  //          fileName = "MyReports.config";
//#else
            fileName = MyReports.Burst.Registry.ReadRegistry.InstallDirectory;
//#endif





            if (!File.Exists(fileName))
                throw new FileNotFoundException("Could not find the configuration file", fileName);

            IConfigurationSource source = new FileConfigurationSource(fileName);

            DatabaseProviderFactory factory = new DatabaseProviderFactory(source);

           
            RdSettings = ReportDistributionSettings.GetReportDistributionSettings(source);
            RemoteSettings = RemotingSettings.GetRemotingSettings(source); 
            ActuateProperties = ActuateSettings.GetActuateProperties(source);
            ActiveDirectoryProperties = ActiveDirectorySettings.GetActiveDirectoryProperties(source);

            DatabaseProviderFactory dbFactory = new DatabaseProviderFactory(source);
            Db = dbFactory.Create("MyReports");

        }
    }
}