using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.Registry;

namespace MyReports.Burst.Common
{

    #region AdminGroups struct

    public struct AdminGroups
    {
        public AdminGroups(string reportCode, string groupName) : this()
        {
            ReportCode = reportCode;
            GroupName = groupName;
        }

        public string ReportCode { get; private set; }

        public string GroupName { get; private set; }
    }

    #endregion

    /// <summary>
    /// clsBase is the base class used for most classes in the MyReports burst process. This class should hold general 
    /// funtionality that would be used in more then one place. If the functionality is Actuate related
    /// then use the clsActuateBase class, which also is derived from this class, for that code.
    /// </summary>
    /// 
    public class ConfigFileLocation
    {
        public static readonly string APP_CONFIG_LOCATION = ReadRegistry.InstallDirectory;
    }

    public class Functions
    {
        /// <summary>
        /// Function moves the SAP report from the FTP directory to a holding place.
        /// This file will be kept for Archive, at the physical file location, for a 
        /// certain length of time specified in the XML config file.
        /// </summary>
        /// <param name="archiveFolder">Folder to move archive files into</param>
        /// <param name="sMasterFileLocation">Physical location of the SAP Report in the FTP directory</param>
        public static void MoveSapReport(string archiveFolder, string sMasterFileLocation)
        {
            string sFolderName = GetFolderName(sMasterFileLocation);

            string[] split = sFolderName.Split('\\');
            string sFileName = split[split.Length - 2];

            string sFileToMove = sFolderName + sFileName;

            if (!Directory.Exists(@archiveFolder))
                Directory.CreateDirectory(@archiveFolder);

            string sDestFile = @archiveFolder + @sFileName;

            if (File.Exists(@sFileToMove))
            {
                if (File.Exists(@sDestFile))
                    File.Delete(@sDestFile);

                File.Move(@sFileToMove, @sDestFile);
            }
        }

        /// <summary>
        /// Function will return a file path without the file name included.
        /// </summary>
        /// <param name="sMasterFileLocation"></param>
        /// <returns>ex. GetFolderName("C:\\Windows\\Test\\notepad.exe"
        ///	Function will return "C:\\Windows\\Test\\</returns>
        private static string GetFolderName(string sMasterFileLocation)
        {
            string[] split = null;
            string sRetValue = "";

            try
            {
                split = sMasterFileLocation.Split('\\');

                int x = (split.Length - 1);

                for (int y = 0; y < x; y++)
                    sRetValue = sRetValue + split[y] + "\\";
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return sRetValue;
        }

        /// <summary>
        /// Function will delete the burst directory once the process is finished
        /// </summary>
        /// <param name="sMasterFileLocation">Folder to delete</param>
        public static void DeleteBurstFolder(string sMasterFileLocation)
        {
            Directory.Delete(GetFolderName(sMasterFileLocation), true);
        }

        public static string FormatFolder(string sFolder)
        {
            if (sFolder.Length > 0)
            {
                sFolder = sFolder.Replace("/", @"\");

                if (sFolder.Substring(0, 1) != @"\")
                    sFolder = @"\" + sFolder;
            }

            return (sFolder);
        }

        public static void MoveFile(string sFullPath, string sBurstDirectory, out string sCopyName, out string sBurstDir)
        {
            sCopyName = "";
            sBurstDir = "";

            string handShakeFile = Path.GetFileName(sFullPath);
            string fileName = Path.GetFileNameWithoutExtension(sFullPath);
            string currentDirectory = Path.GetDirectoryName(sFullPath);

            sBurstDir = sBurstDirectory + fileName;

            Directory.CreateDirectory(sBurstDir);

            sCopyName = sBurstDir + @"\" + fileName;

            if (File.Exists(sCopyName))
                File.Delete(sCopyName);

            if (sFullPath.ToLower().EndsWith(".sub"))
            {
                File.Move(currentDirectory + "\\" + handShakeFile, sCopyName);
            }
            else
            {
                // Delete handshake file
                File.Delete(sFullPath);

                // Move the regular file
                File.Move(currentDirectory + "\\" + fileName, sCopyName);
            }
        }

        private static string RemoveEndingIllegalCharacters(string sFolderName)
        {
            return sFolderName.TrimEnd('*', '-', '_').Trim();
        }

        public static string RemoveIllegalChars(string sIn)
        {
            return (RemoveIllegalChars(sIn, "", ""));
        }

        /// <summary>
        /// For all folder and file names, we need to remove specific characters 
        /// which windows or Actuate will not allow a folder or file to be named.
        /// </summary>
        /// <param name="sIn">Folder/File name</param>
        /// <param name="sIn">Character to use to replace illegial with</param>
        /// <param name="sIn">String to return if string is blank</param>
        /// <param name="sCharToReplaceWith"></param>
        /// <param name="sReturnStringIfBlank"></param>
        /// <returns>Converted Folder/File Name</returns>
        public static string RemoveIllegalChars(string sIn, string sCharToReplaceWith, string sReturnStringIfBlank)
        {
            sIn = RemoveEndingIllegalCharacters(sIn);

            // Strip out known folder or file illegal characters
            const string ill = @"\,/,:,;,*,?, "",<,>,|,~";

            string[] aryIll = ill.Split(',');

            sIn = aryIll.Aggregate(sIn, (current, t) => current.Replace(t, sCharToReplaceWith));

            if (sIn.Length == 0)
                sIn = sReturnStringIfBlank;

            // Remove known escape characters
            char[] cIn = sIn.ToCharArray();
            sIn = "";

            foreach (char c in cIn)
            {
                switch (c)
                {
                    case (char) 0x0022:
                        break;
                    case (char) 0x0027:
                        break;
                    case (char) 0x005C:
                        break;
                    case (char) 0x0000:
                        break;
                    case (char) 0x0007:
                        break;
                    case (char) 0x0008:
                        break;
                    case (char) 0x000C:
                        break;
                    case (char) 0x000A:
                        break;
                    case (char) 0x000D:
                        break;
                    case (char) 0x0009:
                        break;
                    case (char) 0x000B:
                        break;
                    default:
                        sIn += c;
                        break;
                }
            }

            // convert other language characters
            string illegalChars = ConfigurationReader.RdSettings.CharactersToReplaceProperties.CharactersToReplace;
            

            // Get Directory Monitoring values						
            string[] sTempFolders = illegalChars.Split(';');

            string[] sAryIllegalChars;

            // loop through the other language characters, replacing them with the appripriate english character
            for (int x = 0; x < sTempFolders.Length; x++)
            {
                sAryIllegalChars = sTempFolders[x].Split(',');

                if (sAryIllegalChars.Length <= 1) continue;

                sIn = sIn.Replace(sAryIllegalChars[0], sAryIllegalChars[1]);
            }

            return RemoveEndingIllegalCharacters(sIn).Trim();
        }

        /// <summary>
        /// Format the folder code to include 6 digits
        /// </summary>
        /// <param name="sOperation">Folder Code input. (1234)</param>
        /// <returns>001234</returns>
        /// <example>FormatOperationsCode("1234")</example>
        public static string FormatOperationsCode(string sOperation)
        {
            return sOperation.Length > 0 ? (sOperation.PadLeft(6, '0')) : ("");
        }

        public static string GetFolderCodeFromFolderName(string sFolderName)
        {
            return sFolderName.IndexOf("-") > 0
                       ? sFolderName.Substring(0, sFolderName.IndexOf("-") - 1).Trim()
                       : sFolderName.Trim();
        }

        /// <summary>
        /// Function calculates the number of Milliseconds remain in the day.
        /// </summary>
        /// <returns>Returns the number of milliseconds left in the day, plus 5 additional seconds.</returns>
        public static double GetTimerInterval()
        {
            // Number of milliseconds in a day
            const double milDays = 86400000;
            DateTime dtNow = DateTime.Now;

            // Add 5 seconds to the end of the day.
            return (milDays - dtNow.TimeOfDay.TotalMilliseconds + 5000);
        }

        /// <summary>
        /// Function will retrieve the report code from the report name.
        /// </summary>
        /// <param name="sReportName">/(3) Operations/001111 - Folder Name/Report Name ZPTA.roi</param>
        /// <returns>ZPTA</returns>
        public static string GetReportCodeFromReportName(string sReportName)
        {
            // First, remove all the spaces
            string sTemp =
                sReportName.Substring(sReportName.LastIndexOf(" ") + 1,
                                      sReportName.Length - sReportName.LastIndexOf(" ") - 1).Trim();

            // Now, find if there is a ".roi -  If so, remove it
            if (sTemp.IndexOf(".roi") > 0)
                sTemp = sTemp.Substring(0, sTemp.Length - 4);

            // Checking for report name is not necessary, in fact if a financial report has the string subsidy in it, 
            // this code will prevent it from bursting correctly
            //if (sReportName.ToLower().Contains("subsidy"))
            //    sTemp = "ZCAR2821";

            return (sTemp);
        }

        public static string GetOperationsCodeFromFolderCode(IEnumerable<char> sOppCode)
        {
            string sTemp = "";

            bool bFoundNumber = false;

            foreach (char c in sOppCode)
            {
                if (!bFoundNumber)
                {
                    if (c != 48)   
                        bFoundNumber = true;
                }
                if (bFoundNumber)
                    sTemp += c.ToString();
            }

            return (sTemp.Trim());
        }
    } // Class
} // Namespace