namespace MyReports.Common.Structs
{
    public struct NewFolderRecord
    {
        public NewFolderRecord(string sFolderCode, string sCurrentFolderName, string sNewFolderName, int nLogCode,
                               bool bNewRecord) : this()
        {
            FolderCode = sFolderCode;
            CurrentFolderName = sCurrentFolderName;
            NewFolderName = sNewFolderName;
            LogCode = nLogCode;
            NewRecord = bNewRecord;
        }

        public string FolderCode { get; private set; }
        public string CurrentFolderName { get; private set; }
        public string NewFolderName { get; private set; }
        public int LogCode { get; private set; }
        public bool NewRecord { get; private set; }
    }
}