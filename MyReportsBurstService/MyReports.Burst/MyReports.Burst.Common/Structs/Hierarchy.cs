namespace MyReports.Common.Structs
{
    public struct Hierarchy
    {
        public string Sector { get; set; }
        public string Division { get; set; }        
        public string Region { get; set; }
        public string Dm { get; set; }
        public string Unit { get; set; }
    }
}