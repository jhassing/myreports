using System;
using System.Collections.Specialized;
using System.Text;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common.Configuration.Helper;

namespace MyReports.Burst.Common
{
    public class ExceptionPublisher : IExceptionPublisher
    {

        public ExceptionPublisher()
        {
        }
        // Provide implementation of the IPublishException interface
        // This contains the single Publish method.
        void IExceptionPublisher.Publish(Exception exception, NameValueCollection AdditionalInfo, NameValueCollection ConfigSettings)
        {
            string m_OpMail = ConfigurationReader.RdSettings.EmailProperties.ExceptionEmailListing;

            // Create StringBuilder to maintain publishing information.
            StringBuilder strInfo = new StringBuilder();

            // Record the contents of the AdditionalInfo collection.
            if (AdditionalInfo != null)
            {
                // Record General information.
                strInfo.AppendFormat("{0}General Information{0}", Environment.NewLine);
                strInfo.AppendFormat("{0}Additonal Info:", Environment.NewLine);
                foreach (string i in AdditionalInfo)
                {
                    strInfo.AppendFormat("{0}{1}: {2}", Environment.NewLine, i, AdditionalInfo.Get(i));
                }
            }
            // Append the exception text
            strInfo.AppendFormat("{0}{0}Exception Information{0}{1}", Environment.NewLine, exception.ToString());

            // send notification email if operatorMail attribute was provided
            if (m_OpMail.Length > 0)
            {
                try
                {
                    string subject = "Exception Notification";
                    string body = strInfo.ToString();

                    System.Net.Mail.MailAddressCollection macTO = new System.Net.Mail.MailAddressCollection();
                    macTO.Add(m_OpMail);

                    Compass.Utilities.Email.EmailHelper.SendMail(
                        ConfigurationReader.RdSettings.EmailProperties.SmtpServer,	// SMTP
                        subject, // Subject
                        body,	// Body
                        macTO,	// To Email address
                        new System.Net.Mail.MailAddress("ReportDistributionExceptionManager@Compass-USA.com"),  // From Email
                        null,   // CC
                        null,   // BCC
                        null,
                        System.Net.Mail.MailPriority.High,
                        true);
                }
                catch
                {
                    // Do not throw en error.
                    
                }

            }
        }
    }
}
