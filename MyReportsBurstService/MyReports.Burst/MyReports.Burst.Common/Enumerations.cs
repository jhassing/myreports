namespace MyReports.Burst.Common
{
    public enum Enum_BurstStatusCodes { FileFound = 1, ReportNameValid = 2, FinishedBursting = 3, ActuateFinishedImport = 4, FinishedSettingFolderPermissions = 5 };
    public enum Enum_ReportTypes { Financial = 0, Payroll = 1, Profiles = 2, Off_Cycle = 3, Subsidy_Invoice = 4, Benefit = 5, Folder_Rename = 97, Zrops = 98, None = 99 };
    public enum Enum_PayTypes { None = 0, Salary = 1, Hourly = 2 };
    public enum enum_OverwriteValues { No = 1, Yes = 2, Force = 3 };
    public enum enum_ArchiveTimeUnits { Monthly = 1, Weekly = 2, Daily = 3 };
}
