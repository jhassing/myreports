using MyReports.Burst.Common;

namespace MyReports.Common
{
    /// <summary>
    /// Summary description for TildyHeader.
    /// </summary>
    public class TildyHeader
    {
        public TildyHeader(string sHeader)
        {
            string[] split = null;
            split = sHeader.Split('~');

            ReportName = split.Length > 1 ? split[1] : "Missing_ReportName";
            UnitCode = split.Length > 2 ? Functions.RemoveIllegalChars(split[2], "", "") : "missing_UNIT";
            RegionCode = split.Length > 3 ? Functions.RemoveIllegalChars(split[3], "", "") : "missing_REGION";
            DmCode = split.Length > 4 ? Functions.RemoveIllegalChars(split[4], "", "") : "missing_DM";
        }

        public string ReportName { get; private set; }
        public string UnitCode { get; private set; }
        public string RegionCode { get; private set; }
        public string DmCode { get; private set; }
    }
}