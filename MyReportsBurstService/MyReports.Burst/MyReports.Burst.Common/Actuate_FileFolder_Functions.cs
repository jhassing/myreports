#region Namespace Imports

using System;
using Compass.Reporting.Actuate;
using MyReports.Burst.Common.Configuration.Helper;

#endregion

namespace MyReports.Burst.Common
{
    /// <summary>
    /// Class for Actuate directory related access.
    /// </summary>	
    public abstract class ActuateFileFolderFunctions : ActuateHelper
    {
        protected void LoginActuate()
        {
            Console.WriteLine("Attempting to loging to Actuate.");

            Console.WriteLine("Url: {0}  Username: {1}", 
                ConfigurationReader.ActuateProperties.Url, ConfigurationReader.ActuateProperties.AdminUserName);

            LoginActuate(ConfigurationReader.ActuateProperties.AdminUserName,
                         ConfigurationReader.ActuateProperties.AdminPassword,
                         ConfigurationReader.ActuateProperties.Url);
            
        }
    }
}