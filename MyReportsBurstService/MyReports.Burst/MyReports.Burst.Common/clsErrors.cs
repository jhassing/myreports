using System.Diagnostics;

public class clsEventLog
{
    public enum Enum_ErrorCodes { enum_NoTildyInformation = 100, enum_InvalidReportName = 200, enum_SQLServerError = 400, enum_ApplicationError = 500, enum_ActuateError = 800 };

    public static void WriteEventLogData(string sMessage, System.Diagnostics.EventLogEntryType nEventLogType, Enum_ErrorCodes nErrorCode)
    {
        string EVENT_VIEWER_LOG = "Report_Distribution";

        if (!EventLog.SourceExists(EVENT_VIEWER_LOG))
            EventLog.CreateEventSource(EVENT_VIEWER_LOG, EVENT_VIEWER_LOG);

        EventLog log = new EventLog();
        log.Source = EVENT_VIEWER_LOG;

        sMessage = sMessage + "\n";

        log.WriteEntry(sMessage, nEventLogType, (int)nErrorCode);

    }


}

