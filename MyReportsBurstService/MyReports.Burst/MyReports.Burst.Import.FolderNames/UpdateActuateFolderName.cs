using System;
using System.Collections;
using System.Collections.Specialized;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.SqlServerDal;
using Server_Proxy.localhost;

namespace MyReports.Burst.Import.FolderNames
{
    /// <summary>
    /// This class will recusivly loop through each folder in Actuate, and see if any of
    /// the folder names have changed. If one has indeed changed, we will rename it.
    /// </summary>
    public class UpdateActuateFolderName : FolderRenameBase, IFolderRename
    {
        #region Private Class Variables

        // Hold the array list of the old and new folders that need to be renamed		
        private string[] aryFolders;
        private FolderStatus m_FolderStatus = new FolderStatus(ConfigurationReader.Db);

        private string m_sDir = "";

        #endregion

        /// <summary>
        /// Contructor for renaming class.
        /// </summary>
        /// <param name="alFoldersList">Array list of folder changes. 
        ///	format: ~folder code~old name~new name~</param>
        public UpdateActuateFolderName(ArrayList alFoldersList)
        {
            m_alFoldersList = alFoldersList;
        }

        #region IFolderRename Members

        public override void ProcessFolderCode()
        {
            // See if the folder codes are the same.
            // First, get the current folder code.					
            try
            {
                int n = aryFolders.Length;
                string sLastFolder = aryFolders[n - 2];

                base.sFolderCode = ProcessOperationsFolder(aryFolders[n - 3], base.sFolderCode);
                base.sFolderCode = ProcessOperationsFolder(aryFolders[n - 2], base.sFolderCode);

                if (sOldFolderName == "")
                    sExistingFolderName = sFolderCode;
                else
                    sExistingFolderName = sFolderCode + " - " + sOldFolderName;

                string sActuateFolderCode = Functions.GetFolderCodeFromFolderName(sLastFolder);
                if (sFolderCode.ToLower() == sActuateFolderCode.ToLower())
                {
                    string sUpdatedFolderCode_Name = sFolderCode.Trim();

                    if (sUpdatedFolderName.Length > 0)
                        sUpdatedFolderCode_Name += " - " + sUpdatedFolderName.Trim();

                    // now see if the folder description in Actuate is the same as the renamed description
                    if (sUpdatedFolderCode_Name.ToLower() != sLastFolder.ToLower())
                    {
                        // First, need need to know if this folder exists

                        // if the folder does exist already, we need to move the contents of the 
                        // old folder into the new folder and delete the old folder

                        if (base.DoesFileExist(m_sDir, sUpdatedFolderCode_Name, false, false).TotalCount != 0)
                        {
                            Console.WriteLine("Moving Directory: " + m_sDir + sLastFolder);

                            //archive files first before moving them
                            
                            base.MoveFileFolder(m_sDir + sLastFolder, "*", m_sDir + sUpdatedFolderCode_Name, false);
                            base.DeleteFileFolder(m_sDir, sLastFolder);
                        }
                        else
                        {
                            // if the folder does not exist, we just need to rename it
                            Console.WriteLine("Renaming Directory: " + m_sDir + sLastFolder);
                            base.RenameFileFolder(m_sDir + sLastFolder, m_sDir + sUpdatedFolderCode_Name);
                        }

                        // Place the info into SQL Server for later tracking. We might need to know about it.
                        //m_FolderStatus.InsertFolderStatusDetail (sFolderCode, sExistingFolderName, sUpdatedFolderCode_Name, 3);
                        // FIX THIS FOLDER STATUS TRACKING
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
            }
        }

        #endregion

        protected override void ProcessFileDirectory(string sDirectory, File actuateFile)
        {
            string sSearch = "";

            try
            {
                sSearch = actuateFile.Name + "/";
                string sFullFolder = sDirectory + sSearch;
                m_sDir = sDirectory;

                aryFolders = sFullFolder.Split('/');

                base.LoopThroughArrayList();

                Console.WriteLine("Searching: " + sDirectory + sSearch);

                DirSearch(sDirectory + sSearch, ActuateFileType.Directory);
            }
            catch (Exception ex)
            {
                NameValueCollection nv = new NameValueCollection();
                nv.Add("Folder", sDirectory + sSearch);

                ExceptionManager.Publish(ex, nv);
            }
        }
    } // Class
} // Namespace