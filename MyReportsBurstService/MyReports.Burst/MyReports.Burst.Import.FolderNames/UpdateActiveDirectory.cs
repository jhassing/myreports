using System;
using System.Collections;
using Microsoft.ApplicationBlocks.ExceptionManagement;

namespace MyReports.Burst.Import.FolderNames
{
	/// <summary>
	/// Summary description for UpdateActiveDirectory.
	/// </summary>
	public class UpdateActiveDirectory : FolderRenameBase, IFolderRename
	{
		public UpdateActiveDirectory(ArrayList alFoldersList)
		{		
			try
			{
				base.m_alFoldersList = alFoldersList;
			}
			catch (Exception ex) 
			{
				ExceptionManager.Publish( ex );
			}
		}


		public void Update_actuateRDHomeFolder()
		{
			try
			{
				base.LoopThroughArrayList();	
			}
			catch (Exception ex) 
			{
				ExceptionManager.Publish( ex );
			}
		}


		public override void ProcessFolderCode()
		{
			try
			{
				ActiveDirectory oActiveDirectory = new ActiveDirectory();
				oActiveDirectory.EditActuateRDHomeFolder(base.sFolderCode, base.sUpdatedFolderName);
			}
			catch (Exception ex) 
			{				
				ExceptionManager.Publish( ex );
			}
		}

	}
}
