using System;
using System.Data.SqlClient;
using System.IO;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.SqlServerDal;
using Microsoft.ApplicationBlocks.ExceptionManagement;

namespace MyReports.Burst.Import.FolderNames
{
    /// <summary>
    /// Summary description for RestoreFolderName Class.
    /// </summary>
    public class RestoreFolderName
    {

        private readonly FolderStatus _folderStatus = new FolderStatus(ConfigurationReader.Db);

        public RestoreFolderName() { }

        /// <summary>
        /// By calling this class, we will do the following.
        /// (1) Create a tildy("~") delimited file based on the folders back SQL Table
        /// (2) Move this, and the .Flag file to the FTP Directory.
        /// This will, in effect reset the SQL Server folders table back to its pervious state.
        /// Also, as a result, it will then rename the Actuate folders back to their pervious state.
        /// By dropping the text file we just created back into the FTP directory, we have simulated the 
        /// folder downlaod process.
        /// Damm, I am good..
        /// </summary>
        public void RestoreFolderNames()
        {
            try
            {

                string sFileName = "txtFolders.txt";

                // First, prompt the user if they are sure they want to restore.
                // Not implimented yet

                // Now, prepare the text file
                if (File.Exists(sFileName))
                    File.Delete(sFileName);

                StreamWriter swFolders = new StreamWriter(sFileName, true);
                swFolders.NewLine = "\r\n";

                // Next, open the folders table backup copy
                SqlDataReader drFolders = _folderStatus.GetFoldersBackupTable();

                // Now, loop through the tblFolders_Backup table
                // Create a ~ dilimeted file, like the one we would recive from SAP

                // Always call Read before accessing data.
                while (drFolders.Read())
                {
                    string sFolderCode = drFolders.GetString(0);
                    string sFolderName = drFolders.GetString(1);

                    string sLine = "~" + sFolderCode + "~" + sFolderName + "~";

                    // Write the string to a file.. next..
                    swFolders.WriteLine(sLine);
                }

                // always call Close when done reading.
                drFolders.Close();
                swFolders.Close();

                // Prepare the .Folders file. This file is for the 'flag' to pickup and burst
                // Get Directory Monitoring values							
                string sFoldersFileExtentionToLookFor = ConfigurationReader.RdSettings.BurstProperties.FileExtToLookFor;
                
                sFoldersFileExtentionToLookFor = sFoldersFileExtentionToLookFor.Substring(1, sFoldersFileExtentionToLookFor.Length - 1);

                string sExtentionFileName = sFileName + sFoldersFileExtentionToLookFor;

                // Now, prepare the text file
                if (File.Exists(sExtentionFileName))
                    File.Delete(sExtentionFileName);

                FileStream fs = File.Create(sExtentionFileName);
                fs.Close();


                // Next, place this file into the FTP Directory where we would recieve this file				
                string sFTPDirectory = ConfigurationReader.RdSettings.BurstProperties.FileExtToLookFor;
                string sFileDest = sFTPDirectory + sFileName;
                string sFileExtentionDest = sFTPDirectory + sExtentionFileName;

                if (File.Exists(sFileDest))
                    File.Delete(sFileDest);

                File.Move(sFileName, sFileDest);

                if (File.Exists(sFileExtentionDest))
                    File.Delete(sFileExtentionDest);

                File.Move(sExtentionFileName, sFileExtentionDest);


                // The process will take care of the rest.

            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
            }

        }
    }
}
