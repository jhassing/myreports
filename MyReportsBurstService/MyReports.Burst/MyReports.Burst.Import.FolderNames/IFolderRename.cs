
namespace MyReports.Burst.Import.FolderNames
{
    public interface IFolderRename
    {
        void ProcessFolderCode();
    }
}
