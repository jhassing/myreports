using System;
using System.Collections;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common;
using MyReports.Common.Structs;

namespace MyReports.Burst.Import.FolderNames
{
    /// <summary>
    /// Summary description for FolderRenameBase.
    /// </summary>
    public abstract class FolderRenameBase : ActuateFileFolderFunctions, IFolderRename
    {
        protected string[] sValues = null;
        protected string sFolderCode = "";
        protected string sOldFolderName = "";
        protected string sUpdatedFolderName = "";
        protected string sExistingFolderName = "";

        protected ArrayList m_alFoldersList = new ArrayList();

        public FolderRenameBase()
        {
            base.LoginActuate();
        }

        public void LoopThroughArrayList()
        {
            try
            {
                for (int x = 0; x < m_alFoldersList.Count; x++)
                {
                    NewFolderRecord folder = (NewFolderRecord)m_alFoldersList[x];

                    sFolderCode = folder.FolderCode;
                    sOldFolderName = folder.CurrentFolderName;
                    sUpdatedFolderName = folder.NewFolderName;

                    ProcessFolderCode();

                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
            }
        }

        public string ProcessOperationsFolder(string sBaseFolderName, string sFolderCode)
        {
            string sCode = "";

            try
            {
                if ((sBaseFolderName == "(3) Operations") || (sBaseFolderName == "(4) Payroll"))
                    sCode = Common.Functions.FormatOperationsCode(sFolderCode);
                else
                    sCode = sFolderCode;
            }
            catch (Exception ex)
            {
                System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();
                nv.Add("Base Folder Name", sBaseFolderName);
                nv.Add("Folder Code", sFolderCode);
                ExceptionManager.Publish(ex, nv);
            }

            return (sCode);


        }

        public virtual void ProcessFolderCode()
        {
            // Empty Implimentation. Override this in the inheriting class
        }
    }
}
