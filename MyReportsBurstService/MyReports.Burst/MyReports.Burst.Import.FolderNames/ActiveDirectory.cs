using System;
using System.Collections;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.SqlServerDal;
using Compass.Security.ActiveDirectory;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using Novell.Directory.Ldap;

namespace MyReports.Burst.Import.FolderNames
{
    /// <summary>
    /// Summary description for ActuateHomeFolder.
    /// </summary>
    public class ActiveDirectory
    {
        private FolderStatus m_FolderStatus = new FolderStatus(ConfigurationReader.Db);

        public void EditActuateRDHomeFolder(string sFolderCode, string sNewFolderName)
        {
            try
            {

                string serviceAccountUser = ConfigurationReader.ActiveDirectoryProperties.ServiceAccountUser;
                string serviceAccountPassword = ConfigurationReader.ActiveDirectoryProperties.ServiceAccountPassword;
                string ldapHost = ConfigurationReader.ActiveDirectoryProperties.LdapHost;
                int ldapPort = ConfigurationReader.ActiveDirectoryProperties.LdapPort;
                string ldapSearchBase = ConfigurationReader.ActiveDirectoryProperties.BaseDn;
                int maxResults = ConfigurationReader.ActiveDirectoryProperties.MaxResultes;
                
                

                int searchScope = 2;

                Hashtable htUserSearch = new Hashtable();
                Hashtable ht = null;
                ArrayList myAL1 = new ArrayList();


                LDAPConnection ldapConn = new LDAPConnection(serviceAccountUser, serviceAccountPassword, ldapHost, ldapPort);

                IUserObject oADUtilities = new ADUserObject(ldapConn);

                // Try to "find" the user first. Search the entire domain for the user.
                // If we find the user, connect to that container inside the user specific OU

                ArrayList retProperties = new ArrayList();
                retProperties.Add("distinguishedName");
                retProperties.Add("actuateRDHomeFolder");

                htUserSearch.Add("actuateRDHomeFolder=", "*/" + sFolderCode + "*");

                ArrayList coUsers1 = oADUtilities.FindUsers(htUserSearch, retProperties, "objectCategory", new string[] { "user" }, ldapSearchBase, searchScope, maxResults);

                htUserSearch.Clear();
                htUserSearch.Add("actuateRDHomeFolder=", "*/" + Common.Functions.FormatOperationsCode(sFolderCode) + "*");
                ArrayList coUsers2 = oADUtilities.FindUsers(htUserSearch, retProperties, "objectCategory", new string[] { "user" }, ldapSearchBase, searchScope, maxResults);

                coUsers1.AddRange(coUsers2);

                foreach (LdapAttributeSet ldapAttribSet in coUsers1)
                {
                    try
                    {
                        LdapAttribute ldapAttribHomeFolder = ldapAttribSet.getAttribute("actuateRDHomeFolder");

                        string sOriginalHomeFolder = "";

                        if (ldapAttribHomeFolder != null)
                        {
                            sOriginalHomeFolder = ldapAttribHomeFolder.StringValue;
                        }

                        string[] sAryOriginalHomeFolder = sOriginalHomeFolder.Split('/');

                        if (sAryOriginalHomeFolder.Length == 3)	//Only 3 is the acceptable value here
                        {
                            string sExistingFolderCode = GetFolderCode(sAryOriginalHomeFolder[2]);

                            string sBaseFolderName = sAryOriginalHomeFolder[1].ToString();

                            if ((sBaseFolderName == "(3) Operations") || (sBaseFolderName == "(4) Payroll"))
                                sFolderCode = Common.Functions.FormatOperationsCode(sFolderCode);

                            if (sExistingFolderCode.ToLower() == sFolderCode.ToLower())
                            {
                                string sNewHomeFolder = "/" + sBaseFolderName + "/" + sFolderCode.Trim();

                                if (sNewFolderName.Length > 0)
                                    sNewHomeFolder += " - " + sNewFolderName.Trim();

                                string dn = ldapAttribSet.getAttribute("distinguishedName").StringValue;

                                oADUtilities.SetUserProperties(dn, "actuateRDHomeFolder", sNewHomeFolder);

                                //m_FolderStatus.InsertHomeFolderStatusUpdate( dn, sOriginalHomeFolder, sNewHomeFolder);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();
                        //nv.Add("Name", sUser.Name);
                        //nv.Add("Sam Account Name", sUser.SamAccountName );
                        nv.Add("Folder Code", sFolderCode);
                        nv.Add("New Folder Name", sNewFolderName);
                        ExceptionManager.Publish(ex, nv);

                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();
                nv.Add("Folder Code", sFolderCode);
                nv.Add("New Folder Name", sNewFolderName);
                ExceptionManager.Publish(ex, nv);
            }


        }

        private string GetFolderCode(string sIN)
        {
            string sTemp = "";

            if (sIN.IndexOf(" - ") > 0)
            {
                sTemp = sIN.Substring(0, sIN.IndexOf(" - "));

            }
            else
            {
                sTemp = sIN.Trim();
            }

            return (sTemp);

        }


    }	// Class

}	// Namespace
