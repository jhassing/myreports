using System;
using System.Collections;
using System.Data;
using System.IO;
using Compass.FileSystem.ExtendedFileMonitor;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.SqlServerDal;
using MyReports.Common.Structs;

namespace MyReports.Burst.Import.FolderNames
{
    /// <summary>
    /// Class will be used to monitor a specific directory for any new folder import files.
    /// Once a new file is found, an event is raised, and the file is moved to a new location.
    /// The file is then looped through, and imported into the folders table in SQL Server.
    /// Once this process is completed, we then loop through all the folders in actuate, changing the
    /// appropriate folder names to be in accordance with the foler names in SQL Server.
    /// </summary>
    /// 
    public delegate void FolderImportFileFound();
    public delegate void FolderImportFinish();

    public class FolderNamesImport
    {
        // Driectory Settings
        private string m_sAdminBurstDirectory = "";

        // Directory monitor settings
        public static string m_sFTPDirectory = "";
        public static string m_sFileExtToLookFor = "";

        // Directory Monitor
        private ExtendedFileMonitorClass oFileMonitor;

        // Class Events
        public event FolderImportFileFound FileFound;
        public event FolderImportFinish FolderImportFinished;

        private string m_sPath = "";


        public FolderNamesImport()
        {
            // Get Directory Monitoring values
            m_sAdminBurstDirectory = ConfigurationReader.RdSettings.BurstProperties.AdminDirectory;
            m_sFTPDirectory = ConfigurationReader.RdSettings.BurstProperties.FtpDirectory;
            m_sFileExtToLookFor = ConfigurationReader.RdSettings.BurstProperties.FolderRenameFileExtToLookFor;                     
            
            oFileMonitor = new ExtendedFileMonitorClass(m_sFTPDirectory);
            oFileMonitor.FileFound += new FileMonitorEventHandler(oFileMonitor_FileFound);
        }

        public void Start_FolderNamesImportMonitor()
        {
            m_sPath = "";
            oFileMonitor.StartDiretoryMonitoring();
        }


        public void Stop_FolderNamesImportMonitor()
        {
            oFileMonitor.StopDirectoryMonitoring();
        }


        /// <summary>
        /// Event that gets fired once the directory monitor discovers that a new Folder Names file has come down from SAP.
        /// Once the file has been discovered, we will move it and loop through it. Funtion will go through each entry in the SAP
        /// text file download, and compare the folder codes to the code inside SQL Server database.
        /// If a code is not found in the database, we will add it.
        /// If a code is found, and the folder name has not changed, we will discard this record.
        /// If the code is found in the database, and the folder name has changed, we will update the database and then 
        /// rename the folder name inside Actuate.
        /// </summary>
        /// <param name="sPath">Path to the SAP folder downlaod file.</param>
        private void oFileMonitor_FileFound(string sPath)
        {
            if (sPath.ToLower().EndsWith(m_sFileExtToLookFor.ToLower()))
            {
                m_sPath = sPath;

                // Raise the event back to the Remoting Object. He will tell us when it is clear to start the import.
                if (FileFound != null)
                    FileFound();
            }
        }


        public void ImportFolders()
        {
            ArrayList myAL = new ArrayList();

            string sCopyName;
            string sBurstDir;
            string sLine;
            string sFolderCode;
            string sNewFolderName;
            string sDatabaseFolderName;

            // Move the file to a better, more useable location
            Common.Functions.MoveFile(m_sPath, m_sAdminBurstDirectory, out sCopyName, out sBurstDir);

            // find this folder code in the "Folders" table
            SqlServerDal.FolderNames oFolderNames = new SqlServerDal.FolderNames(ConfigurationReader.Db);
            FolderStatus oFolderStatus = new FolderStatus(ConfigurationReader.Db);

            // Add this entry to the master table
            oFolderStatus.InsertFolderStatusMaster();

            // Make a backup of the folders table
            oFolderStatus.BackupFoldersTable();

            // Get the folder names list from SQL Server
            SqlServerDal.FolderNames oFolderName = new SqlServerDal.FolderNames(ConfigurationReader.Db);
            DataSet dsFolders = oFolderName.GetFolders();


            // Now, loop through the file
            using (StreamReader sr = new StreamReader(sCopyName, System.Text.Encoding.UTF7))
            {
                try
                {
                    while (sr.Peek() >= 0)
                    {
                        sLine = sr.ReadLine();

                        if (sLine.Length <= 0) continue;

                        string[] sTemp = sLine.Split('~');

                        if (sTemp.Length != 4) continue;

                        sFolderCode = sTemp[1].Trim();
                        sNewFolderName = Common.Functions.RemoveIllegalChars(sTemp[2].Trim().Trim(), "-", "");

                        if (oFolderNames.DoesFolderCodeExist(sFolderCode))
                        {
                            bool isFolderCodeSameAsDatabase = oFolderNames.IsFolderCodeSameAsDatabase(dsFolders, sFolderCode, sNewFolderName, out sDatabaseFolderName);

                            Console.WriteLine("Folder Code: {0}  --  Database Name: {1}    --  New Name: {2}", sFolderCode, sDatabaseFolderName, sNewFolderName);

                            if (!isFolderCodeSameAsDatabase)
                                myAL.Add(new NewFolderRecord(sFolderCode, sDatabaseFolderName, sNewFolderName, 1, false));

                        }
                        else
                            myAL.Add(new NewFolderRecord(sFolderCode, "", sNewFolderName, 2, true));
                    }
                }
                catch (Exception ex)
                {
                    string sdf = "";
                }
            }

            if (myAL.Count > 0)
            {
                // First, send the new updated codes to SQL Server
                oFolderNames.UpdateInsertFolderName(myAL);

                // Now Rename these folders inside actuate.
                UpdateActuateFolderName oUpdateActuateFolderName = new UpdateActuateFolderName(myAL);
                oUpdateActuateFolderName.DirSearch("/", Compass.Reporting.Actuate.ActuateHelper.ActuateFileType.Directory);

                // Next, rename the Actuate Home Folders
                UpdateActiveDirectory oUpdateActiveDirectory = new UpdateActiveDirectory(myAL);
                //oUpdateActiveDirectory.Update_actuateRDHomeFolder();
            }

            if (FolderImportFinished != null)
                FolderImportFinished();

        }	// private voide ImportFolders()


    }	// Class

}	// Namespace
