using System;
using System.Collections;
using System.Diagnostics;
using System.Reflection;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;

namespace MyReports.Burst.ReportArchive
{
    /// <summary>
    /// Summary description for ArchiveBase.
    /// </summary>
    public abstract class ArchiveBase : ActuateFileFolderFunctions
    {
        // Actuate FilesFolder class		
        protected string sArchiveBaseFolderName;

        public ArchiveBase()
        {
            try
            {
                // Step 1:  Log Into Actuate
                base.LoginActuate();

                // Get the archive folder name (Base archive folder name)			
                string[] sTemp1 = ConfigurationReader.RdSettings.FolderProperties.FolderNames.Split(',');
                sArchiveBaseFolderName = "\\" + sTemp1[0].ToString();

                
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        /// <summary>
        /// Method will Create a new folder, move the report to the new folder and archive the report.
        /// </summary>
        /// <param name="sFolder">Folder where the report currently exists.</param>
        /// <param name="sFileName">Report name in Actuate</param>
        /// <param name="sCreatedFileMonth">Month the report was created in.</param>
        /// <param name="ReplaceExistingReport">Replace existing or version the reports.</param>
        /// <param name="LengthOfTimeToArchive">length of time (Months, weeks, ect) to archive the report.</param>
        /// <param name="ArchiveTimeUnits">Months weeks days</param>
        protected void _archiveFile(string sFolder,
                                        string sFileName,
                                        string sCreatedFileMonth,
                                        ArrayList permissions,
                                        bool ReplaceExistingReport,
                                        int LengthOfTimeToArchive,
                                        enum_ArchiveTimeUnits ArchiveTimeUnits)
        {
            try
            {
                #region Private method variables

                sArchiveBaseFolderName = Functions.FormatFolder(sArchiveBaseFolderName);
                sCreatedFileMonth = Functions.FormatFolder(sCreatedFileMonth);
                sFolder = Functions.FormatFolder(sFolder);
                sFileName = Functions.FormatFolder(sFileName);
                string sFileToMove = Functions.FormatFolder(sFolder + sFileName);
                string newFolderName = Functions.FormatFolder(sArchiveBaseFolderName + sCreatedFileMonth + sFolder);

                #endregion

                #region Create the Sub Folder
                try
                {
                    base.CreateFolder(sArchiveBaseFolderName + sCreatedFileMonth, sFolder);
                }
                catch (Exception ex)
                {
                    System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();

                    nv.Add("Folder", sFolder);
                    nv.Add("Archivebase folder", sArchiveBaseFolderName);
                    nv.Add("Created Month", sCreatedFileMonth);

                    ExceptionManager.Publish(new Exception("Error creating archive folder", ex), nv);
                }

                #endregion

                #region Set the folder Permissions

                try
                {
                    StackFrame stackFrame = new StackFrame();
                    MethodBase methodBase = stackFrame.GetMethod();
                    Console.WriteLine("Method called " + methodBase.Name);

                    if (permissions != null)
                    {
                        if (permissions.Count > 0)
                        {
                            base.SetFolderPermissions(permissions, newFolderName, "VRS", false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();

                    nv.Add("Folder Name", newFolderName);
                    ExceptionManager.Publish(new Exception("Error setting base folder permissions in archive", ex), nv);
                }

                #endregion

                #region Move the file to the archive directory

                try
                {
                    base.MoveFileFolder(sFolder, sFileName, sArchiveBaseFolderName + sCreatedFileMonth + sFolder, ReplaceExistingReport);
                }
                catch (Exception ex)
                {
                    System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();

                    nv.Add("Folder Name", sFolder);
                    nv.Add("File Name", sFileName);
                    nv.Add("Archive base folder", sArchiveBaseFolderName);
                    nv.Add("Created Month", sCreatedFileMonth);
                    nv.Add("Replace existing report", ReplaceExistingReport.ToString());

                    ExceptionManager.Publish(new Exception("Error moving report to archive folder.", ex), nv);
                }

                #endregion

                #region Set the File Permissions

                try
                {
                    if (permissions != null)
                    {
                        if (permissions.Count > 0)
                        {
                            base.SetFolderPermissions(permissions, newFolderName + sFileName, "VRS", false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();

                    nv.Add("Folder Name", newFolderName);
                    ExceptionManager.Publish(new Exception("Error setting base folder permissions in archive", ex), nv);
                }

                #endregion

                #region Get the archive length and Time units

                System.DateTime dt = System.DateTime.Now;
                System.DateTime dt2 = System.DateTime.Now;

                if (ArchiveTimeUnits == enum_ArchiveTimeUnits.Daily)
                {
                    dt2 = dt.AddDays(LengthOfTimeToArchive);
                }
                else if (ArchiveTimeUnits == enum_ArchiveTimeUnits.Weekly)
                {
                    dt2 = dt.AddDays(LengthOfTimeToArchive * 7);
                }
                else if (ArchiveTimeUnits == enum_ArchiveTimeUnits.Monthly)
                {
                    dt2 = dt.AddMonths(LengthOfTimeToArchive);
                }

                #endregion

                #region Set the archive length on the report

                try
                {
                    base.ArchiveFileFolder(LengthOfTimeToArchive, sArchiveBaseFolderName + sCreatedFileMonth + sFolder, sFileName, dt2);
                }
                catch (Exception ex)
                {
                    System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();

                    nv.Add("Length to archive", LengthOfTimeToArchive.ToString());
                    nv.Add("File Name", sFileName);
                    nv.Add("Folder Name", sFolder);
                    nv.Add("Archive base folder", sArchiveBaseFolderName);
                    nv.Add("Created Month", sCreatedFileMonth);

                    ExceptionManager.Publish(new Exception("Error moving report to archive folder.", ex), nv);
                }

                #endregion

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        protected string GetFileCreatedMonthFormated(DateTime dt)
        {

            try
            {
                return ("\\(" + dt.ToString("MM") + ") " + dt.ToString("MMMM"));
            }
            catch
            {
                throw;
            }
        }


    }	// Class

}	// NameSpace
