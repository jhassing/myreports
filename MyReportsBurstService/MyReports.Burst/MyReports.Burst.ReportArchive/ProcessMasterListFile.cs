using System;
using System.Collections;
using System.IO;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common;
using MyReports.Burst.ReportPermissions;

namespace MyReports.Burst.ReportArchive
{
    public class ProcessMasterListFile
    {
        public void ArchiveReports(string sMasterFileListLocation)
        {
            #region Private method level variables

            SplitMasterTextFileLine oSplit = null;
            StreamReader srFromFile = null;
            string sLine = "";
            string sFolder = "";
            string reportName = "";
            string[] sFolders = null;

            #endregion

            ArchiveReports oArchiveReports = new ArchiveReports();

            try
            {
                srFromFile = new StreamReader(sMasterFileListLocation);

                // Loop through each line in the report
                while (srFromFile.Peek() > -1)
                {
                    sLine = srFromFile.ReadLine();

                    // Break Folder Structure apart
                    oSplit = new SplitMasterTextFileLine(sLine);

                    sFolders = oSplit.Folders;

                    sFolder = Functions.FormatFolder(sFolders[1]) + Functions.FormatFolder(sFolders[2]);
                    reportName = oSplit.ReportName;

                    try
                    {
                        ArrayList permissions = null;

                        if (oSplit.ReportType == Enum_ReportTypes.Financial)
                            permissions = new FinancialPermissions().GetFolderPermissionGroups(sLine);
                        else if (oSplit.ReportType == Enum_ReportTypes.Payroll)
                            permissions = new PayrollPermissions().GetFolderPermissionGroups(sLine);
                        else if (oSplit.ReportType == Enum_ReportTypes.Profiles)
                            permissions = new ProfilePermissions().GetFolderPermissionGroups(sLine);

                        oArchiveReports.ArchiveReport(sFolder, reportName, oSplit.ReportCode, permissions, oSplit.ReportType);
                    }
                    catch (Exception ex)
                    {
                        // Publish the error, but, contine processing

                        System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();

                        nv.Add("Folder", sFolder);
                        nv.Add("Report Name", reportName);
                        nv.Add("Line", sLine);

                        ExceptionManager.Publish(ex, nv);

                    }
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                try
                {
                    srFromFile.Close();
                    oArchiveReports.Dispose();
                }
                catch /* (Exception ex) */
                {
                    // Throw no errors???
                }
            }

        }

    }
}
