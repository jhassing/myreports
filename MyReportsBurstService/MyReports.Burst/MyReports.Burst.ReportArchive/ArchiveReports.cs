using System;
using System.Collections;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common;
using MyReports.Burst.SqlServerDal;
using Server_Proxy.localhost;
using MyReports.Burst.Common.Configuration.Helper;

namespace MyReports.Burst.ReportArchive
{
    public class ArchiveReports : ArchiveBase
    {
        public void ArchiveReport(string sFolder, string reportName, string reportCode, ArrayList permissions, Enum_ReportTypes nReportType)
        {

            enum_OverwriteValues OverwriteCode = enum_OverwriteValues.Force;
            enum_ArchiveTimeUnits ArchiveTimeUnits = enum_ArchiveTimeUnits.Daily;
            int ArchiveLength = 0;
            int ReportTypeID;

            // get the report in actuate
            SelectFilesResponse res = base.DoesFileExist(sFolder, reportName, false, false);

            if (res != null)
            {
                Server_Proxy.localhost.File[] l_fileList = (Server_Proxy.localhost.File[])res.Item;

                if (l_fileList.Length > 0)
                {

                    #region Get the archive values for this report from the database.

                    try
                    {
                        Reports oReports = new Reports(ConfigurationReader.Db);
                        oReports.GetReportArchiveValues(reportCode, out OverwriteCode, out ArchiveLength, out ReportTypeID, out ArchiveTimeUnits);
                    }
                    catch (Exception ex)
                    {
                        System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();

                        nv.Add("Report Code", reportCode);
                        ExceptionManager.Publish(new Exception("Error getting the archive values from the database.", ex), nv);
                    }

                    #endregion

                    // get the report date time stamp
                    DateTime dt = l_fileList[0].TimeStamp;

                    // If archive length = 0 OR Overwrite code is force, then always delete this report.
                    if (ArchiveLength == 0 || OverwriteCode == enum_OverwriteValues.Force)
                    {
                        DeleteFile(sFolder, reportName);
                    }
                    else if (OverwriteCode == enum_OverwriteValues.No)
                    {
                        // Always archive the report
                        base._archiveFile(sFolder, reportName, GetFileCreatedMonthFormated(dt), permissions, false, ArchiveLength, ArchiveTimeUnits);

                    }
                    else
                    {
                        if (isMonthEndReport(dt.Month))
                        {
                            base._archiveFile(sFolder, reportName, GetFileCreatedMonthFormated(dt), permissions, true, ArchiveLength, ArchiveTimeUnits);
                        }
                        else
                            DeleteFile(sFolder, reportName);
                    }
                }
            }
        }


        private void DeleteFile(string folder, string reportName)
        {
            try
            {
                base.DeleteFileFolder(folder, reportName);
            }
            catch (Exception ex)
            {
                System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();

                nv.Add("Folder", folder);
                nv.Add("Report Name", reportName);

                ExceptionManager.Publish(new Exception("Error deleting report", ex), nv);
            }
        }

        /// <summary>
        /// Determine if this report has been run in the same month as today.
        /// </summary>
        /// <param name="nReportMonth">Report month</param>
        /// <returns>true/false</returns>
        private bool isMonthEndReport(int nReportMonth)
        {
            int nCurrentMonth = System.DateTime.Now.Month;

            if (nCurrentMonth != nReportMonth)
                return (true);
            else
                return (false);

        }

    }
}
