using System;
using System.Data.SqlClient;
using System.IO;

namespace RDconfig
{
    class AddFileSizesToDB
    {
        public void Start()
        {
            string folder = @"\\cguschp250\e$\ActuateBurstArchive";
            string connectionString = MyReports.Burst.Common.Configuration.Helper.ConfigurationReader.DatabaseConnection;


            DirectoryInfo di = new DirectoryInfo(folder);

            FileInfo[] fiArr = di.GetFiles();

            Console.WriteLine("The directory {0} contains the following files:", di.Name);

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                connection.Open();

                foreach (FileInfo f in fiArr)
                {
                    if (!f.Name.Contains("~"))
                    {
                        string name = f.Name.ToLower();
                        long lengthInMB = f.Length;

                        Console.WriteLine("The size of {0} is {1} bytes.", name, lengthInMB);

                        // Now, find the record in the DB.
                        string queryString = "SELECT  * FROM dbo.BurstProcessStatus WHERE (SAPfileName = '" + name + "')";

                        SqlCommand command = new SqlCommand(queryString, connection);

                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Console.WriteLine(String.Format("{0}, {1}", reader[0], reader[1]));

                            // Get the Primary Key of this record
                            int priKey = System.Convert.ToInt32(reader[0]);

                            // SQL Statement for the update
                            string updateSQL = "update BurstProcessStatus Set FileSize = " + lengthInMB + " WHERE (SAPfileName = '" + name + "')";

                            // Execute the Statement.
                            using (SqlConnection updateConn = new SqlConnection(connectionString))
                            {
                                updateConn.Open();
                                SqlCommand updateCommand = new SqlCommand(updateSQL, updateConn);
                                int numReturned = updateCommand.ExecuteNonQuery();
                            }

                        }

                        reader.Close();
                    }
                }
            }
        }
    }
}