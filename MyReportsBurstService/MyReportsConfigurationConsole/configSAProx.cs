using System;
using System.Collections;
using MyReports.Burst.Common;

namespace MyReports.Burst.ConfigurationConsole
{
    /// <summary>
    /// Summary description for configSAProx.
    /// </summary>
    public class configSAProx : ActuateFileFolderFunctions
    {
        public void SetTheSAProxPermissions()
        {
            try
            {
                // Step 1:  Log Into Actuate
                base.LoginActuate();

                Console.WriteLine("Setting the sap.rox permissions");
                setPermissions();

                Console.WriteLine("Finished\n");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private void setPermissions()
        {

            ArrayList sRoles = new ArrayList();
            sRoles.Add("actAll");

            base.SetFolderPermissions(sRoles, "/sap.rox", "SR", true);
        }
    }
}
