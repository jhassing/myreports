#region Namespace Imports

using Compass.Reporting.Actuate;
using MyReports.Burst.Common.Configuration.Helper;

#endregion

namespace MyReports.Burst.ConfigurationConsole
{
    internal class ActuateAccess : ActuateHelper
    {
        public void LoginActuate()
        {
            string sUserName = ConfigurationReader.ActuateProperties.AdminUserName;
            string sPassword = ConfigurationReader.ActuateProperties.AdminPassword;
            string url = ConfigurationReader.ActuateProperties.Url;

            base.LoginActuate(sUserName, sPassword, url);
        }
    }
}