#region Namespace Imports

using System;
using System.Collections;
using System.Threading;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;

#endregion

namespace MyReports.Burst.ConfigurationConsole
{
    /// <summary>
    /// Summary description for config.
    /// </summary>
    public class ConfigBaseFolders : ActuateFileFolderFunctions
    {
        private string m_sBaseFolders;
        private string m_sMonthsFolders = "";

        public void ConfigTheBaseFolders()
        {
            try
            {
                // Step 1:  Log Into Actuate
                base.LoginActuate();

                Console.WriteLine("Getting config variables\r");
                GetConfigVariables();

                Console.WriteLine("Creating the base folders\r");
                CreateTheBaseFolders();

                Console.WriteLine("Finished\n");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        private void GetConfigVariables()
        {
            try
            {
                // Get the config variables								
                m_sBaseFolders = ConfigurationReader.RdSettings.FolderProperties.FolderNames;


                m_sMonthsFolders =
                    ConfigurationReader.RdSettings.ArchiveProperties.ArchiveFolderMonths;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        private void CreateTheBaseFolders()
        {
            string[] sBaseFolder = m_sBaseFolders.Split(',');
            string[] sMonths = m_sMonthsFolders.Split(',');

            ArrayList sRoles = new ArrayList {"actAll"};

            string sArchiveFolder = "";

            int nArraySize = (Convert.ToInt32(sMonths.Length) + Convert.ToInt32(sBaseFolder.Length));

            for (int x = 0; x < sBaseFolder.Length; x++)
            {
                string sBase = sBaseFolder[x];

                if (x == 0)
                    sArchiveFolder = sBase;

                base.CreateFolder("/", sBase);
                base.SetFolderPermissions(sRoles, "/" + sBase, "VSR", false);
            }

            // Create the Months folders under the Archive Folder Section
            // Loop through the months variable in the config file				
            for (int x = sBaseFolder.Length; x < nArraySize; x++)
            {
                string sMonthFolder = sMonths[x - sBaseFolder.Length];

                base.CreateFolder("/" + sArchiveFolder + "/", sMonthFolder);
                base.SetFolderPermissions(sRoles, "/" + sArchiveFolder + "/" + sMonthFolder, "VSR", false);

                // Now, create the base folders under each month
                for (int xx = 1; xx < sBaseFolder.Length; xx++)
                {
                    string sBase = sBaseFolder[xx];

                    base.CreateFolder("/" + sArchiveFolder + "/" + sMonthFolder + "/", sBase);
                    base.SetFolderPermissions(sRoles, "/" + sArchiveFolder + "/" + sMonthFolder + "/" + sBase, "VSR",
                                              false);
                }
            }

            #region Create the Benefits Folder

            sRoles = new ArrayList {"actAll"};

            base.CreateFolder("/", "(5) Benefits");
            base.SetFolderPermissions(sRoles, "/(5) Benefits", "VSR", false);

            #endregion

            #region Create the Blue Book Distribution Base folders

            sRoles = new ArrayList {"BlueBookDistributionUsers", "BlueBookDistributionAdmins"};

            base.CreateFolder("/", "Blue Book Distribution");
            base.SetFolderPermissions(sRoles, "/Blue Book Distribution", "VSR", false);

            base.CreateFolder("/Blue Book Distribution/", "Archives");
            base.SetFolderPermissions(sRoles, "/Blue Book Distribution/Archives", "VSR", false);

            for (int x = 1; x < 13; x++)
            {
                string folderName = "Period " + x.ToString().PadLeft(2, '0');

                base.CreateFolder("/Blue Book Distribution/Archives/", folderName);
                base.SetFolderPermissions(sRoles, "/Blue Book Distribution/Archives/" + folderName, "VSR", false);
            }

            #endregion

            #region Create the National Accounts Folders

            sRoles = new ArrayList {"NA_AdminGroup"};

            base.CreateFolder("/", "National_Accounts");
            base.SetFolderPermissions(sRoles, "/National_Accounts", "VSR", false);

            sRoles = new ArrayList {"NA_AdminGroup", "actAll"};

            base.CreateFolder("/National_Accounts/", "Commission");
            base.SetFolderPermissions(sRoles, "/National_Accounts/Commission", "VSR", false);

            base.CreateFolder("/National_Accounts/", "Management");
            base.SetFolderPermissions(sRoles, "/National_Accounts/Management", "VSR", false);

            base.CreateFolder("/National_Accounts/", "Profitability");
            base.SetFolderPermissions(sRoles, "/National_Accounts/Profitability", "VSR", false);

            #endregion

            #region Create the Passport Folder

            sRoles = new ArrayList {"passportAdmins"};

            base.CreateFolder("/", "Passport");
            base.SetFolderPermissions(sRoles, "/Passport", "VSR", false);

            base.CreateFolder("/Passport/", "Reports");
            base.SetFolderPermissions(sRoles, "/Passport/Reports", "VSR", false);

            #endregion
        }


        public void UploadNar()
        {
            // Step 1:  Log Into Actuate
            base.LoginActuate();

            Console.WriteLine("Uploading the reports\r\r");

            ArrayList sRoles = new ArrayList {"actAll"};

            string[] commission = new[]
                                      {
                                          "Florida Tax Summary.rox",
                                          "Sales and Commission Summary by Disbursement Type - G N.rox",
                                          "Sales and Commission Summary by Disbursement Type - SI.rox",
                                          "Sales and Commissions by Location - G N.rox",
                                          "Sales and Commissions by Location - SI (Option 1 - Location, CCC3).rox",
                                          "Sales and Commissions by Location - SI (Option 2 - CCC3, Location).rox",
                                          "Sales and Commissions by Location - SI and OCS.rox",
                                          "Sales and Commissions by Location by Product - G N.rox",
                                          "Sales and Commissions by Location by Product - SI.rox",
                                          "Sales and Commissions by Location by Product Group - G N.rox",
                                          "Sales and Commissions by Machine by Product - G N BD T.rox",
                                          "Sales and Commissions by Machine by Product - GN.rox",
                                          "Sales and Commissions by Machine by Product by Sell Price - G N.rox",
                                          "Sales and Commissions by Machine Type - GN.rox"
                                      };


            for (int x = 0; x < commission.Length; x++)
            {
                string rptName = commission[x];

                Console.WriteLine("Uploading: " + rptName + "\r");
                base.UploadFile("/National_Accounts/Commission/", @"C:\Reports\Commission\" + rptName);

                Thread.Sleep(20);

                Console.WriteLine("Setting Permissions: " + rptName + "\r");
                base.SetFolderPermissions(sRoles, "/National_Accounts/Commission/" + rptName, "VSR", false);

                Thread.Sleep(20);
            }

            string[] management = new[]
                                      {
                                          "Account P & L Summary By National Account - Selected Period.rox",
                                          "Account P & L Summary By National Account - YTD.rox",
                                          "Sales and Profit Summary By National Account Entity(NonTraditional).rox",
                                          "Sales and Profit Summary By National Account Entity.rox",
                                          "Sales and Profit Summary by National Acct by Branch - Current Period.rox",
                                          "Sales and Profit Summary by National Acct by Branch - YTD.rox",
                                          "Sales and Profit Summary by National Acct by Region - Current Period.rox",
                                          "Sales and Profit Summary by National Acct by Service Providers.rox",
                                          "Sales and Profit Summary by Service Provider by National Account.rox",
                                          "Sales and Profit Summary Total By National Account Entity.rox",
                                          "Sales by Product Category (YTD).rox",
                                          "Weighted Average Selling Price by Product Category.rox"
                                      };


            for (int x = 0; x < management.Length; x++)
            {
                string rptName = management[x];

                Console.WriteLine("Uploading: " + rptName + "\r");
                base.UploadFile("/National_Accounts/Management/", @"C:\Reports\Management\" + rptName);

                Thread.Sleep(20);

                Console.WriteLine("Setting Permissions: " + rptName + "\r");
                base.SetFolderPermissions(sRoles, "/National_Accounts/Management/" + rptName, "VSR", false);

                Thread.Sleep(20);
            }

            Console.WriteLine("Finished\n");
        }
    }
}