#region Namespace Imports

using System;
using System.Collections;
using Compass.Reporting.Actuate;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.SqlServerDal;
using MyReports.Common.Structs;
using Server_Proxy.localhost;

#endregion

namespace MyReports.Burst.ConfigurationConsole
{
    internal class ResetFolderPermissions : ActuateFileFolderFunctions
    {
        public void Start()
        {
            try
            {
                // Step 1:  Log Into Actuate
                Console.WriteLine("Signing into Actuate.\r\n");
                base.LoginActuate();

                // Get the archive folder name (Base archive folder name)
                string[] sTemp1 = ConfigurationReader.RdSettings.FolderProperties.FolderNames.Split(',');

                for (int x = 0; x < sTemp1.Length; x++)
                {
                    string sFolderToSearch = "/" + sTemp1[x] + "/";

                    Console.WriteLine("Getting base folder " + sFolderToSearch + " contents.\r\n");

                    GetFolderItemsResponse oActuateFileAttributes = base.GetDirectoryContents(sFolderToSearch);

                    long nFolderCount = oActuateFileAttributes.TotalCount;

                    for (int y = 0; y < nFolderCount; y++)
                    {
                        if (oActuateFileAttributes.ItemList[y].FileType.ToLower() == "directory")
                        {
                            // string sFolderName = sFolderToSearch + oActuateFileAttributes.ItemList[y].Name;
                            string sFolderName = sFolderToSearch + oActuateFileAttributes.ItemList[y].Name + "/";

                            // Continue the search
                            Console.WriteLine("Processing: " + sFolderName);
                            // System.IO.File.AppendAllText("C:\\data\\rdcPermissions.txt", "Processing: " + sFolderName + "\n");

                            // Get the premissions that SHOULD be on this folder
                            ArrayList folderPermissions = GetFolderPermissions(sFolderName);

                            // Set this folders permissions
                            //base.SetFolderPermissions(folderPermissions, sFolderName, "VSR", true);

                            base.DirSearch(sFolderName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);

                string SDF = "";
            }
        }

        public void Start(string sFolderToSearch)
        {
            try
            {
                // Step 1:  Log Into Actuate
                Console.WriteLine("Signing into Actuate.\r\n");
                base.LoginActuate();

                Console.WriteLine("Getting base folder " + sFolderToSearch + " contents.\r\n");

                GetFolderItemsResponse oActuateFileAttributes = base.GetDirectoryContents(sFolderToSearch);
                long nFolderCount = oActuateFileAttributes.TotalCount;

                for (int y = 0; y < nFolderCount; y++)
                {
                    if (oActuateFileAttributes.ItemList[y].FileType.ToLower() == "directory")
                    {
                        string sFolderName = sFolderToSearch + oActuateFileAttributes.ItemList[y].Name;
                        //string sFolderName = sFolderToSearch + oActuateFileAttributes.ItemList[y].Name + "/";
                        // Continue the search


                        DirSearch(sFolderName); // added RS


                        Console.WriteLine("Processing: " + sFolderName);
                        // System.IO.File.AppendAllText("C:\\data\\rdcPermissions.txt", "Processing:  " + sFolderName + "\n");

                        // First, see if the folder is empty. If so, just delete it instead of adding permissions.
                        if (base.GetDirectoryContents(sFolderName).TotalCount == 0)
                        {
                            // Delete the folder
                            base.DeleteFileFolder(sFolderToSearch, oActuateFileAttributes.ItemList[y].Name);
                        }
                        else
                        {
                            // Get the premissions that SHOULD be on this folder
                            ArrayList folderPermissions = GetFolderPermissions(sFolderName);

                            // Set this folders permissions
                            base.SetFolderPermissions(folderPermissions, sFolderName, "VSR", true);
                            // Console.WriteLine("Permissions set for: " + sFolderName);

                            folderSearchC(sFolderName + "/");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //ExceptionManager.Publish(ex);

                string SDF = "";
            }
        }


        public void folderSearchC(string sDir)
        {
            folderSearchC(sDir, ActuateHelper.ActuateFileType.All);
        }

        public void folderSearchC(string sDir, ActuateHelper.ActuateFileType actuateFileType)
        {
            if (sDir.Length > 1)
            {
                if (sDir.Substring(0, 2) == "//")
                    sDir = sDir.Substring(1, sDir.Length - 1);
            }

            try
            {
                GetFolderItemsResponse countAttribs = GetDirectoryContents(sDir, actuateFileType, true);

                if (countAttribs != null)
                {
                    for (int x = 0; x < countAttribs.TotalCount; x++)
                    {
                        if (countAttribs.ItemList[x].FileType.ToLower() == "directory")
                        {
                            string sFolderName = sDir + countAttribs.ItemList[x].Name; // + "/";

                            //>>>>>>>>>>>
                            // Get the permissions that SHOULD be on this folder
                            ArrayList folderPermissions = GetFolderPermissions(sFolderName);

                            // Set this folders permissions

                            //  Console.WriteLine("Setting Permissions for: " + sFolderName);
                            // System.IO.File.AppendAllText("C:\\data\\rdcPermissions.txt", "Setting Permissions:  " + sFolderName + "\n");         
                            base.SetFolderPermissions(folderPermissions, sFolderName, "VSR", true);

                            //<<<<<<<<<

                            // Continue the search
                            //    Console.WriteLine(sFolderName + ": " + countAttribs.ItemList[x].FileType);
                            folderSearchC(sFolderName + "/"); // added 
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        protected override void ProcessFileDirectory(string sDirectory, File actuateFile)
        {
            if (!sDirectory.EndsWith("/"))
                sDirectory = sDirectory + "/";

            string sSearch = actuateFile.Name + "/";

            string name = actuateFile.Name;

            //  Console.WriteLine("Processing: " + sDirectory + name);

            // is this a folder or a file.
            if (actuateFile.FileType.ToLower() == "directory")
            {
                // Get the permissions for this folder
                ArrayList folderPermissions = GetFolderPermissions(sDirectory);

                // Set the folders permissions

                // Search through this folder, find any other folders or files
                DirSearch(sDirectory + sSearch);
            }
            else
            {
                // We found a file, so set the permissions on it.

                // Set the permissions here????
                ArrayList filePermissions = GetFilePermissions(sDirectory + name);
                base.SetFolderPermissions(filePermissions, sDirectory + name, "VSR", true);
            }
        }

        #region Get File Permissions

        public ArrayList GetFilePermissions(string path)
        {
            string[] ff = path.Split('/');

            string code = Functions.GetFolderCodeFromFolderName(ff[ff.Length - 2]);

            return new ArrayList();
        }

        #endregion

        #region Get Folder Permissions

        public static ArrayList GetFolderPermissions(string path)
        {
            ArrayList ary = new ArrayList();

            try
            {
                string financialExtention = ConfigurationReader.RdSettings.GroupPermissionProperties.FinancialReports;
                string payrollSalary = ConfigurationReader.RdSettings.GroupPermissionProperties.PayrollSalary;
                string payrollHourly = ConfigurationReader.RdSettings.GroupPermissionProperties.PayrollHourly;

                string[] ff = path.Split('/');

                string code = Functions.GetFolderCodeFromFolderName(ff[ff.Length - 1]);

                #region (1) Accounting

                if (path.IndexOf("(1) Accounting") > 0)
                {
                    ary.Add(code + financialExtention);
                }

                #endregion

                #region (2) District

                if (path.IndexOf("(2) District") > 0)
                {
                    // Get all the REGION codes from the database for this Dm
                    ary.Add(code + financialExtention);

                    if (code.Length >= 3)
                    {
                        code = code.Substring(0, 3);

                        ary.Add(code + financialExtention);
                    }
                }

                #endregion

                #region (3) Opperations

                // This is an operations Folder
                if (path.IndexOf("(3) Operations") > 0)
                {
                    code = code.TrimStart('0');

                    Hierarchy hierarchy = new Zrops(ConfigurationReader.Db).GetRegionCodeAnddmCodeFromUnitCode(code);

                    if (hierarchy.Region.Length > 0) ary.Add(hierarchy.Region + financialExtention);
                    if (hierarchy.Dm.Length > 0) ary.Add(hierarchy.Dm + financialExtention);

                    ary.Add(code + financialExtention);
                }

                #endregion

                #region (4) Payroll

                // This is a Payroll folder.. p.s. do not forget, FOLDERS get Salary AND hourly permissions
                if (path.IndexOf("(4) Payroll") > 0)
                {
                    // Get all the Dm codes and Region codes from the DB for this operation
                    code = code.TrimStart('0');

                    Hierarchy hierarchy = new Zrops(ConfigurationReader.Db).GetRegionCodeAnddmCodeFromUnitCode(code);

                    if (hierarchy.Region.Length > 0) ary.Add(hierarchy.Region + payrollSalary);
                    if (hierarchy.Dm.Length > 0) ary.Add(hierarchy.Dm + payrollSalary);

                    ary.Add(code + payrollSalary);
                    ary.Add(code + payrollHourly);
                }

                #endregion

                // ary.Add("MyFinAdmin");
                // ary.Add("MyPayAdmin");
                ary.Add("MyBenefitsAdmin");

                return ary;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}