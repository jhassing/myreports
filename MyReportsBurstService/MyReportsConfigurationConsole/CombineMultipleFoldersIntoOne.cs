#region Namespace Imports

using System;
using System.Collections.Generic;
using System.Data;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.SqlServerDal;
using RDconfig;
using Server_Proxy.localhost;

#endregion

namespace MyReports.Burst.ConfigurationConsole
{
    internal struct GroupNames
    {
        private string _newName;
        private string _oldName;

        public GroupNames(string OldName, string NewName)
        {
            _oldName = OldName;
            _newName = NewName;
        }

        public string OldName
        {
            get { return _oldName; }
        }

        public string NewName
        {
            get { return _newName; }
        }
    }

    internal class CombineMultipleFoldersIntoOne : ActuateFileFolderFunctions
    {
        private static string _folderToSearchFor = "";
        private static string folderCode = "";

        private string baseFolder = "/(1) Accounting/";
        private string folderName = "";

        private List<GroupNames> lGroupNames = null;

        private static bool DoesFolderCodeExists(File file)
        {
            string[] path = file.Name.Split('/');

            string tmpFolderName = path[path.Length - 1];

            string tmpFolderCode = Functions.GetFolderCodeFromFolderName(tmpFolderName);

            if (tmpFolderCode.ToLower() == folderCode.ToLower())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool DoesFolderExist(File file)
        {
            string[] path = file.Name.Split('/');

            string tmpFolderName = path[path.Length - 1];

            if (tmpFolderName.ToLower() == _folderToSearchFor.ToLower())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Start()
        {
            try
            {
                // Step 1:  Log Into Actuate
                base.LoginActuate();

                // Step 2: Get the Folder Code listing from SQL and Load into Memory
                FolderNames oFolderName = new FolderNames(ConfigurationReader.Db);
                DataSet dsFolders = oFolderName.GetFolders();

                // Step 3: Get the folder listing under the base folder we want to Combine.
                GetFolderItemsResponse files = GetDirectoryContents(baseFolder);

                List<File> lFiles = new List<File>();

                // Put the files list inside a collection so we can enumerate and search if needed.
                for (int x = 0; x < files.TotalCount; x++)
                {
                    lFiles.Add(files.ItemList[x]);
                }

                // Step 4: Loop through each folder, find out if more then one exists.
                for (int x = 0; x < files.TotalCount; x++)
                {
                    bool hereAgain = false;

                    // Get the Folder Name
                    string[] path = files.ItemList[x].Name.Split('/');

                    folderName = path[path.Length - 1];

                    // Get Code
                    folderCode = Functions.GetFolderCodeFromFolderName(folderName);

                    Console.WriteLine("Searching for folder: " + baseFolder + folderName);

                    List<File> lMatchingFolders = lFiles.FindAll(DoesFolderCodeExists);

                    if (lMatchingFolders != null)
                    {
                        if (lMatchingFolders.Count > 1)
                            hereAgain = true;
                    }

                    if (hereAgain)
                    {
                        string SQLfolderName = "";

                        // This DOES exist multiple times. Combine them all
                        if (baseFolder.Contains("(3) Operations") || baseFolder.Contains("(4) Payroll"))
                            FolderNames.GetFolderName(folderCode, dsFolders, true);
                        else
                            FolderNames.GetFolderName(folderCode, dsFolders, false);

                        // Does the SQL Folder even exist?
                        _folderToSearchFor = SQLfolderName;

                        if (lFiles.FindAll(DoesFolderExist).Count == 0)
                        {
                            // Create the new folder.
                            base.CreateFolder(baseFolder, SQLfolderName);

                            // Assign the permissions
                            base.SetFolderPermissions(
                                ResetFolderPermissions.GetFolderPermissions(baseFolder + SQLfolderName),
                                baseFolder + SQLfolderName, "VSR", true);
                        }

                        // Loop through all the folders..
                        for (int y = 0; y < lMatchingFolders.Count; y++)
                        {
                            try
                            {
                                // First, is it the "REAL" folder?
                                string tmpName = lMatchingFolders[y].Name;

                                if (tmpName.ToLower() != SQLfolderName.ToLower())
                                {
                                    // Get the content listing of the Source Folder
                                    GetFolderItemsResponse sourceFiles = base.GetDirectoryContents(baseFolder + tmpName);

                                    if (sourceFiles != null)
                                    {
                                        // Get a listing of the Destination folders contents
                                        GetFolderItemsResponse destinationFiles =
                                            base.GetDirectoryContents(baseFolder + SQLfolderName);

                                        if (destinationFiles != null)
                                        {
                                            // Loop through all the files inside the source folder, compare them to the destination folder
                                            for (int x1 = 0; x1 < sourceFiles.TotalCount; x1++)
                                            {
                                                for (int x2 = 0; x2 < destinationFiles.TotalCount; x2++)
                                                {
                                                    if (sourceFiles.ItemList[x1].Name.ToLower() ==
                                                        destinationFiles.ItemList[x2].Name.ToLower())
                                                    {
                                                        // If the file exist inside the destination folder, Archive it.. 
                                                        if (sourceFiles.ItemList[x1].TimeStamp >
                                                            destinationFiles.ItemList[x2].TimeStamp)
                                                        {
                                                            // if the source file was created after the destination file, then delete the destination file
                                                            base.DeleteFileFolder(baseFolder + SQLfolderName + "/",
                                                                                  sourceFiles.ItemList[x2].Name);
                                                        }
                                                        else
                                                        {
                                                            // Seems like the destination file was created after the source file, so, delete the source file
                                                            base.DeleteFileFolder(baseFolder + tmpName + "/",
                                                                                  destinationFiles.ItemList[x2].Name);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        // Remove the folder from the Cache List
                                        lFiles.Remove(lMatchingFolders[y]);
                                    }

                                    // Move the payroll files over
                                    base.MoveFileFolder(baseFolder + tmpName, "*", baseFolder + SQLfolderName, false);

                                    // Wack the bad folder
                                    base.DeleteFileFolder(baseFolder, tmpName);
                                }
                            }
                            catch (Exception ex)
                            {
                                string ERROR = "";
                            }
                        }
                    }

                    string NOTHING = "";
                }
            }
            catch (Exception ex)
            {
                string ERROR = "";
            }

            base.Logout();

            string afsdf = "";
        }
    }
}