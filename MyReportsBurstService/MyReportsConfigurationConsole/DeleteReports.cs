using System;
using System.IO;
using MyReports.Burst.Common;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Burst.Common.Configuration.Helper;

namespace MyReports.Burst.ConfigurationConsole
{
    internal class DeleteReports : ActuateFileFolderFunctions
    {
        public void Start()
        {
            Console.WindowWidth = 170;
            Console.WindowHeight = 60;
            Console.SetBufferSize(170, 60);

            // Step 1:  Log Into Actuate
            Console.WriteLine("Signing into Actuate.\r\n");
            LoginActuate();

            // Get the list of files to delete


            //                const string sqlCommand = "SELECT     TOP 100 PERCENT dbo.BurstProcessStatus.BurstProcessStatusID, dbo.BurstProcessStatus.SAPfileName, dbo.BurstProcessStatus.ReportName,  " +
            //                      "dbo.BurstProcessStatus.ReportCode, dbo.BurstProcessStatus.StartedTime, dbo.BurstProcessStatus.BurstProcessErrorCodeID,  " +
            //                      "dbo.BurstProcessStatus_Detail.ReportLocation, dbo.ReportType.ReportType, dbo.ReportType.ReportTypeID " +
            //"FROM         dbo.BurstProcessStatus INNER JOIN " +
            //                      "dbo.BurstProcessStatus_Detail ON dbo.BurstProcessStatus.BurstProcessStatusID = dbo.BurstProcessStatus_Detail.BurstProcessStatusID INNER JOIN " +
            //                      "dbo.Report ON dbo.BurstProcessStatus.ReportCode = dbo.Report.ReportCode INNER JOIN " +
            //                      "dbo.ReportType ON dbo.Report.ReportTypeID = dbo.ReportType.ReportTypeID " +
            //"WHERE     (dbo.BurstProcessStatus.StartedTime BETWEEN CONVERT(DATETIME, '2009-12-02 18:00:00', 102) AND CONVERT(DATETIME, '2009-12-04 10:00:00', " + 
            //                      "102)) AND (dbo.BurstProcessStatus.ReportCode LIKE 'ZPT%') AND (dbo.BurstProcessStatus_Detail.ReportLocation LIKE '/(1)%' OR  " +
            //                      "dbo.BurstProcessStatus_Detail.ReportLocation LIKE '/(2)%') " +
            //"ORDER BY dbo.BurstProcessStatus.StartedTime, dbo.BurstProcessStatus.ReportCode";

            //               const string sqlCommand = " SELECT     TOP 100 PERCENT dbo.BurstProcessStatus.BurstProcessStatusID, dbo.BurstProcessStatus.SAPfileName, dbo.BurstProcessStatus.ReportName, " +
            //                      "dbo.BurstProcessStatus.ReportCode, dbo.BurstProcessStatus.StartedTime, dbo.BurstProcessStatus.BurstProcessErrorCodeID, " +
            //                      "dbo.BurstProcessStatus_Detail.ReportLocation, dbo.ReportType.ReportType, dbo.ReportType.ReportTypeID " +
            //"FROM         dbo.BurstProcessStatus INNER JOIN " +
            //                      "dbo.BurstProcessStatus_Detail ON dbo.BurstProcessStatus.BurstProcessStatusID = dbo.BurstProcessStatus_Detail.BurstProcessStatusID INNER JOIN " +
            //                      "dbo.Report ON dbo.BurstProcessStatus.ReportCode = dbo.Report.ReportCode INNER JOIN " +
            //                      "dbo.ReportType ON dbo.Report.ReportTypeID = dbo.ReportType.ReportTypeID " +
            //"WHERE     (dbo.BurstProcessStatus.StartedTime BETWEEN CONVERT(DATETIME, '2010-6-01 19:00:00', 102) AND CONVERT(DATETIME, '2010-6-03 05:00:00', " +
            //                      "102)) AND (dbo.BurstProcessStatus.ReportCode LIKE 'ZPPC' OR dbo.BurstProcessStatus.ReportCode LIKE 'ZPPM') AND (dbo.BurstProcessStatus_Detail.ReportLocation LIKE '/(1)%' OR  " +
            //                      "dbo.BurstProcessStatus_Detail.ReportLocation LIKE '/(2)%' OR dbo.BurstProcessStatus_Detail.ReportLocation LIKE '/(3)%') " +
            //                "ORDER BY dbo.BurstProcessStatus.StartedTime, dbo.BurstProcessStatus.ReportCode";


            //               const string sqlCommand = " SELECT     TOP 100 PERCENT dbo.BurstProcessStatus.BurstProcessStatusID, dbo.BurstProcessStatus.SAPfileName, dbo.BurstProcessStatus.ReportName, " +
            //                      "dbo.BurstProcessStatus.ReportCode, dbo.BurstProcessStatus.StartedTime, dbo.BurstProcessStatus.BurstProcessErrorCodeID, " +
            //                      "dbo.BurstProcessStatus_Detail.ReportLocation, dbo.ReportType.ReportType, dbo.ReportType.ReportTypeID " +
            //"FROM         dbo.BurstProcessStatus INNER JOIN " +
            //                     "dbo.BurstProcessStatus_Detail ON dbo.BurstProcessStatus.BurstProcessStatusID = dbo.BurstProcessStatus_Detail.BurstProcessStatusID INNER JOIN " +
            //                      "dbo.Report ON dbo.BurstProcessStatus.ReportCode = dbo.Report.ReportCode INNER JOIN " +
            //                      "dbo.ReportType ON dbo.Report.ReportTypeID = dbo.ReportType.ReportTypeID " +
            //"WHERE     (dbo.BurstProcessStatus.StartedTime BETWEEN CONVERT(DATETIME, '2010-10-08 08:00:00', 102) AND CONVERT(DATETIME, '2010-10-08 11:00:00', 102)) AND  " +
            //                "(dbo.BurstProcessStatus.ReportCode LIKE 'ZCHR0030') " +
            //                "ORDER BY dbo.BurstProcessStatus.StartedTime, dbo.BurstProcessStatus.ReportCode"; 

            //const string sqlCommand = "SELECT TOP 100 PERCENT dbo.BurstProcessStatus.BurstProcessStatusID, dbo.BurstProcessStatus.SAPfileName, dbo.BurstProcessStatus.ReportName, " +
            //       "dbo.BurstProcessStatus.ReportCode, dbo.BurstProcessStatus.StartedTime, dbo.BurstProcessStatus.BurstProcessErrorCodeID, " +
            //       "dbo.BurstProcessStatus_Detail.ReportLocation, dbo.ReportType.ReportType, dbo.ReportType.ReportTypeID " +
            // "FROM         dbo.BurstProcessStatus INNER JOIN " +
            //       "dbo.BurstProcessStatus_Detail ON dbo.BurstProcessStatus.BurstProcessStatusID = dbo.BurstProcessStatus_Detail.BurstProcessStatusID INNER JOIN " +
            //       "dbo.Report ON dbo.BurstProcessStatus.ReportCode = dbo.Report.ReportCode INNER JOIN " +
            //       "dbo.ReportType ON dbo.Report.ReportTypeID = dbo.ReportType.ReportTypeID " +
            // "WHERE     (dbo.BurstProcessStatus.StartedTime BETWEEN CONVERT(DATETIME, '2010-12-06 09:30:00', 102) AND CONVERT(DATETIME, '2010-12-06 10:00:00', " +
            //       "102)) AND (dbo.BurstProcessStatus.ReportCode LIKE 'ZCHR161MR') AND (dbo.BurstProcessStatus_Detail.ReportLocation LIKE '/(4)%') " +
            // "ORDER BY dbo.BurstProcessStatus.StartedTime, dbo.BurstProcessStatus.ReportCode";


            const string sqlCommand =
                "SELECT TOP 100 PERCENT dbo.BurstProcessStatus.BurstProcessStatusID, dbo.BurstProcessStatus.SAPfileName, dbo.BurstProcessStatus.ReportName, " +
                "dbo.BurstProcessStatus.ReportCode, dbo.BurstProcessStatus.StartedTime, dbo.BurstProcessStatus.BurstProcessErrorCodeID, " +
                "dbo.BurstProcessStatus_Detail.ReportLocation, dbo.ReportType.ReportType, dbo.ReportType.ReportTypeID " +
                "FROM         dbo.BurstProcessStatus INNER JOIN " +
                "dbo.BurstProcessStatus_Detail ON dbo.BurstProcessStatus.BurstProcessStatusID = dbo.BurstProcessStatus_Detail.BurstProcessStatusID INNER JOIN " +
                "dbo.Report ON dbo.BurstProcessStatus.ReportCode = dbo.Report.ReportCode INNER JOIN " +
                "dbo.ReportType ON dbo.Report.ReportTypeID = dbo.ReportType.ReportTypeID " +
                "WHERE     (dbo.BurstProcessStatus.StartedTime BETWEEN CONVERT(DATETIME, '2018-01-10 15:40:00', 102) AND CONVERT(DATETIME, '2018-10-08 17:00:00', " +
                "102)) AND (dbo.BurstProcessStatus.ReportCode LIKE 'ZCHR0030') AND (dbo.BurstProcessStatus_Detail.ReportLocation LIKE '/(1)%') " +
                "ORDER BY dbo.BurstProcessStatus.StartedTime, dbo.BurstProcessStatus.ReportCode";

            Database db = ConfigurationReader.Db;

            int nCount = 0;

            Console.WriteLine("Executing SQL Statement: {0}\r\n", sqlCommand);

            DataSet ds = db.ExecuteDataSet(CommandType.Text, sqlCommand);

            Console.WriteLine("Completed SQL Statement: {0}\r\n", sqlCommand);
            Console.WriteLine("");
            Console.WriteLine("Total record count: {0}\r\n", ds.Tables[0].Rows.Count);

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Do you wish to run in demo mode?");

            bool runInDemoMode = (Console.ReadKey().Key == ConsoleKey.Y);

            string tmpFolder = DateTime.Now.ToString("yyyy-MM-dd : hh:mm:ss");

            if (!runInDemoMode)
            {
                Console.WriteLine("Are you sure you wish to run this SQL and remove these files?");

                if (Console.ReadKey().Key != ConsoleKey.Y) return;
                
                CreateFolder("/(0) Archives/Deleted_Files/", tmpFolder, "");

                string tmpSqlFileName = Path.GetTempPath() + "Sql.txt";

                if (File.Exists(tmpSqlFileName)) File.Delete(tmpSqlFileName);

                using (StreamWriter sw = new StreamWriter(tmpSqlFileName))
                {
                    // Add some text to the file.
                    sw.WriteLine("This is the SQL");
                    sw.WriteLine("");
                    sw.WriteLine(sqlCommand);
                }

                UploadFile("/(0) Archives/Deleted_Files/" + tmpFolder + "/", tmpSqlFileName);

                if (File.Exists(tmpSqlFileName)) File.Delete(tmpSqlFileName);
            }

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                // Find the report in Actuate
                string reportLocation = dr["ReportLocation"] as string;

                if (reportLocation.Length == 0) continue;

                string[] tmp = reportLocation.Split('/');

                string reportName = tmp[tmp.Length - 1];

                string reportPath = "";
                for (int x = 0; x < tmp.Length - 1; x++)
                    reportPath = reportPath + tmp[x] + "/";

                nCount++;

                Console.WriteLine("Item Number: {0}  Folder: {1}  Report: {2}", nCount, reportPath, reportName);
                // Delete the report.
                try
                {
                    // Flag the database

                    // Move the file to a temp folder    
                    if (!runInDemoMode)           
                        MoveFileFolder(reportPath, reportName, "/(0) Archives/Deleted_Files/" + tmpFolder + reportPath, true);

                }
                catch (Exception)
                {
                    continue;
                }
            }
        }
    }
}