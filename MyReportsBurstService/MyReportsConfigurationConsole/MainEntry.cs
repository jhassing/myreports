#region Namespace Imports

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Xml;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using MyReports.Burst.BurstProcess;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.Common.Configuration.Settings;
using MyReports.Burst.FolderPermissions;
using MyReports.Burst.Import.FolderNames;
using MyReports.Burst.RemotingObject;

#endregion

namespace MyReports.Burst.ConfigurationConsole
{
    public class MainEntry
    {
        private static RemotingService remotingService;
        
        [STAThread]
        private static void Main(string[] args)
        {
            Console.BufferHeight = 9999;
            Console.WindowHeight = 40;
            Console.CursorVisible = true;
            MainScreen();
        }

        private static void WriteNewValueToConfigFile(string section, string key, string value)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(ConfigFileLocation.APP_CONFIG_LOCATION);


            if (section.StartsWith("/"))
                section = section.Substring(1, section.Length - 1);

            if (!section.StartsWith("/Configuration/"))
                section = "/configuration/" + section;

            XmlNodeList nodesSource = doc.SelectNodes(section);

            for (int x = 0; x < nodesSource.Count; x++)
            {
                for (int y = 0; y < nodesSource.Item(x).ChildNodes.Count; y++)
                {
                    string configFileKey = nodesSource.Item(x).ChildNodes.Item(y).Attributes["key"].Value;

                    if (key == configFileKey)
                    {
                        nodesSource.Item(x).ChildNodes.Item(y).Attributes["value"].Value = value;
                    }
                }
            }

            doc.Save(ConfigFileLocation.APP_CONFIG_LOCATION);
        }

        private static void WriteEnvironmentVars()
        {
            Console.WriteLine("Remoting Object Machine Name: {0}", ConfigurationReader.RemoteSettings.RemoteMachine);

            Console.WriteLine("Actuate Report Server: {0}", ConfigurationReader.ActuateProperties.Url);

            Console.WriteLine("Current Database: {0}", ConfigurationReader.Db.ConnectionString);

            Console.Write("\r\n");
        }

        #region Main Screen

        private static void WriteCommands()
        {
            Console.Clear();

            WriteEnvironmentVars();

            Console.WriteLine("\rWelcome to the MyReports Burst Command Console.\n");
            Console.WriteLine("(0)  Exit this Application");
            Console.WriteLine("(1)  Switch Database and Server");
            Console.WriteLine("(2)  Setup the Base Folder and apply the permissions\r");
            Console.WriteLine("(3)  Apply the permissions to the sap.rox file\r");
            Console.WriteLine("(4)  Directory Monitoring service");
            Console.WriteLine("(5)  Run Folder contents Report");
            Console.WriteLine("(6)  Test server report printing");
            Console.WriteLine("(7)  Rename Folder test (Not Implimented)");
            Console.WriteLine("(8)  Start folder name import process");
            Console.WriteLine("(9)  Restore folder names");
            Console.WriteLine("(10) Clear old ROI files from Actuate");
            Console.WriteLine("(11) Delete all SAP file archived over 30 days old");
            Console.WriteLine("(12) Testing of Premission Reset");
            Console.WriteLine("(13) Combine multiple folders into one ( base folders only)");
            Console.WriteLine("(14) Reset folder permissions based off ZROPS");
            Console.WriteLine("(15) Delete Reports from Volume based on SQL Source");
            Console.WriteLine("(16) Analyse folder contents");
            Console.WriteLine("(21) Upload the National Account Reports");
        }

        private static void MainScreen()
        {
            try
            {
                string sVal = "";

                WriteCommands();

                while (sVal != "0")
                {
                    sVal = Console.ReadLine();

                    switch (sVal)
                    {
                        case "cls":
                            Console.Clear();
                            WriteCommands();
                            break;

                        case "0": // Exit                            
                            break;

                        case "1": // Switch Database and Server	
                            SwitchDatabaseAndServer_MainMenu();
                            break;

                        case "2": // create BASE folders		
                            Console.WriteLine("Signing into Actuate\r");
                            ConfigBaseFolders rd = new ConfigBaseFolders();
                            rd.ConfigTheBaseFolders();
                            WriteCommands();
                            break;

                        case "3": // Set sap.rox permissions
                            Console.WriteLine("Signing into Actuate\r");
                            configSAProx rd1 = new configSAProx();
                            rd1.SetTheSAProxPermissions();
                            WriteCommands();
                            break;

                        case "4": // Start directory monitoring
                            FileMonitor();
                            break;

                        case "5": // Reset Thread Counts
                            remotingService.RunningThreads = 0;
                            remotingService.RunningActuateReports = 0;
                            break;

                        case "6": // Test Server report printing
                            //TestArchiveRule gg1 = new TestArchiveRule();
                            //gg1.PrintReport();
                            break;

                        case "7": // Rename the My Test folder to My Renamed Test
                            //TestArchiveRule ww1 = new TestArchiveRule();
                            //ww1.RenameFolder();
                            break;

                        case "8": // Start the Folder name change Import process
                            try
                            {
                                Console.WriteLine("Use Menu Option 4...");
                                /*//RemotingService service1 = (RemotingService)Activator.GetObject(typeof(ReportDistribution.RemotingObject.RemotingService), sRemotingMachine_Port);
                                Thread.Sleep(2);
                                //service1.StartFolderImportFilePickup();
                           
                                remotingService.StartFolderImportFilePickup();
                                Console.WriteLine("Folder import monitor has started...");
                                */
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                                Console.WriteLine("Please start the monitoring service using option 4");
                                continue;
                            }
                            break;

                        case "9": // Restore the folder names
                            RestoreFolderName rst = new RestoreFolderName();
                            rst.RestoreFolderNames();
                            break;

                        case "10": // Clean actuate base folders
                            //CleanActuateFolders cln = new CleanActuateFolders();
                            //cln.ArchiveOldFiles();
                            break;

                        case "11":
                            SAP_Files oooo = new SAP_Files();
                            oooo.DeleteOldFiles();
                            break;

                        case "12":
                            //ActuateFolderPermissions oclsActuateFolderPermissions = new ActuateFolderPermissions();
                            //oclsActuateFolderPermissions.ActuateFolderPermissions("C:\\MasterFileList.txt");

                            //ResetFolderPermissions oResetFolderPermissions = new ResetFolderPermissions();
                            //oResetFolderPermissions.BeginFolderPermissionsReset();
                            clsSetActuateFolderPermissions oclsActuateFolderPermissions =
                                new clsSetActuateFolderPermissions();
                            oclsActuateFolderPermissions.SetActuateFolderPermissions("C:\\masterfilelist.txt");
                            break;

                        case "13":
                            CombineMultipleFoldersIntoOne oCombineMultipleFoldersIntoOne =
                                new CombineMultipleFoldersIntoOne();
                            oCombineMultipleFoldersIntoOne.Start();
                            break;

                        case "14":
                            ResetFolderPermissions();
                            break;

                        case "15":
                            DeleteReports dd = new DeleteReports();
                            dd.Start();
                            break;

                        case "16":
                            FolderAnalysisScreen();
                            break;

                        case "21": // Upload National Accounts Reports
                            Console.WriteLine("Signing into Actuate\r");
                            ConfigBaseFolders rd2 = new ConfigBaseFolders();
                            rd2.UploadNar();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
                Console.WriteLine("Error: Source  | " + ex.Source);
                Console.WriteLine("Error: Message | " + ex.Message);
                Console.WriteLine("Error: Target  | " + ex.TargetSite);
                
                Console.WriteLine(ex.StackTrace + "\r\nPress Enter...");
                Console.ReadLine();
                //throw ex;
            }
        }

        #endregion

        #region Switch Database and iServer

        private static void Write_SwitchMain_Menu()
        {
            // Write out the screen
            Console.Clear();
            WriteEnvironmentVars();
            Console.WriteLine("Option 1: Switch Database and Actuate iServer\r\n");
            Console.WriteLine("(0) Return to main menu.\r\n");
            Console.WriteLine("(1) Switch Database");
            Console.WriteLine("(2) Switch iServer");
        }

        private static void SwitchDatabaseAndServer_MainMenu()
        {
            Write_SwitchMain_Menu();

            string sVal = "";

            while (sVal != "0")
            {
                sVal = Console.ReadLine();

                switch (sVal)
                {
                    case "cls": // Clear Screen
                        Console.Clear();
                        WriteCommands();
                        break;

                    case "0": // Return to main menu.                        
                        MainScreen();
                        break;

                    case "1": // Switch Database
                        SwitchDatabase();
                        break;

                    case "2": // Switch iServer
                        SwitchiServer();
                        break;
                }
            }
        }

        private static void SwitchDatabase()
        {
            // Vars
            string section = "ReportDistribution/DatabaseProperties";
            string key = "SQLServer";

            // Write out the screen
            Console.Clear();

            WriteEnvironmentVars();
            Console.WriteLine("Option 1: Switch Database\r\n");
            Console.WriteLine("(0) Return to previous menu.\r\n");
            Console.WriteLine(@"(1) Development (cguschd310\DEV)");
            Console.WriteLine(@"(2) Staging     (cguschd310)");
            Console.WriteLine(@"(3) Production  (cguschp309)");

            string sVal = "";

            while (sVal != "0")
            {
                sVal = Console.ReadLine();

                switch (sVal)
                {
                    case "cls":

                        #region Clear Screen

                        Console.Clear();
                        WriteCommands();
                        break;

                        #endregion

                    case "0": // Return to main menu.                        
                        SwitchDatabaseAndServer_MainMenu();
                        break;

                    case "1": // DEV
                        WriteNewValueToConfigFile(section, key, @"cguschd310\dev");
                        MainScreen();
                        break;

                    case "2": // Staging
                        WriteNewValueToConfigFile(section, key, @"cguschd310");
                        MainScreen();
                        break;

                    case "3": // Production
                        WriteNewValueToConfigFile(section, key, @"cguschp309");
                        MainScreen();
                        break;
                }
            }
        }

        private static void SwitchiServer()
        {
            // vars
            string section = "ReportDistribution/RemotingProperties";
            string key = "remoteMachine";

            // Write out the screen
            Console.Clear();

            WriteEnvironmentVars();
            Console.WriteLine("Option 1: Switch iServer\r\n");
            Console.WriteLine("(0) Return to previous menu.\r\n");
            Console.WriteLine(@"(1) Laptop      (localhost)");
            Console.WriteLine(@"(2) Development (reportserver)");
            Console.WriteLine(@"(3) Staging     (cguschd297vm)");
            Console.WriteLine(@"(4) Production  (cguschp250)");

            string sVal = "";

            while (sVal != "0")
            {
                sVal = Console.ReadLine();

                switch (sVal)
                {
                    case "cls":

                        #region Clear Screen

                        Console.Clear();
                        WriteCommands();
                        break;

                        #endregion

                    case "0": // Return to main menu.                        
                        SwitchDatabaseAndServer_MainMenu();
                        break;

                    case "1": // Production
                        WriteNewValueToConfigFile(section, key, @"localhost");
                        MainScreen();
                        break;

                    case "2": // DEV
                        WriteNewValueToConfigFile(section, key, "reportserver");
                        MainScreen();
                        break;

                    case "3": // Staging
                        WriteNewValueToConfigFile(section, key, @"cguschd297vm");
                        MainScreen();
                        break;

                    case "4": // Production
                        WriteNewValueToConfigFile(section, key, @"cguschp250");
                        MainScreen();
                        break;
                }
            }
        }

        #endregion

        #region Reset Folder Permnissions

        public static void ResetFolderPermissions()
        {
            ResetFolderPermissions oResetFolderPermissions = new ResetFolderPermissions();

            WriteResetFolderPermssionsCommands();

            //  string logPath = ConfigurationManager.AppSettings["PERMISSIONS_LOG_PATH"] + "\\setPermissionsC_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            //  TextWriter tw = new StreamWriter(logPath);

            string sVal = "";

            while (sVal != "0")
            {
                sVal = Console.ReadLine();

                switch (sVal)
                {
                    case "cls":

                        #region Clear Screen

                        Console.Clear();
                        WriteCommands();
                        break;

                        #endregion

                    case "0": // Return to main menu.                        
                        WriteCommands();
                        break;

                    case "9":

                        #region (0) Archives

                        Console.WriteLine("Starting folder permissions reset on (0) Archives.");

                        //  TextWriter tw = new StreamWriter("ConsolePermissions_Start.txt");
                        //tw.WriteLine("Starting Reset Archive Permissions @: " + DateTime.Now);
                        // tw.Close();

                        oResetFolderPermissions.Start("/(0) Archives/");

                        WriteResetFolderPermssionsCommands();
                        Console.WriteLine("Finished Setting folder permissions reset on ((0) Archives.\r\n");

                        //  System.IO.File.AppendAllText(logPath, "Reset Archive Permissions Completed @: " + DateTime.Now + "\n");

                        break;

                        #endregion

                    case "1":

                        #region (1) Accounting

                        //>>>>>>>>>>>>>

                        // tw.WriteLine("Reset Permissions Starting @: " + DateTime.Now + "\n");
                        // tw.Close();            
                        //<<<<<<<<<<

                        Console.WriteLine("Starting folder permissions reset on (1) Accounting.");

                        oResetFolderPermissions.Start("/(1) Accounting/");
                        // oResetFolderPermissions.Start();

                        WriteResetFolderPermssionsCommands();
                        Console.WriteLine("Finished Setting folder permissions reset on (1) Accounting.\r\n");
                        //          System.IO.File.AppendAllText(logPath,"Reset Permissions Completed @: " + DateTime.Now + "\n");
                        //tw.Close();


                        break;

                        #endregion

                    case "2":

                        #region (2) District

                        Console.WriteLine("Starting folder permissions reset on (2) District.");

                        oResetFolderPermissions.Start("/(2) District/");

                        WriteResetFolderPermssionsCommands();
                        Console.WriteLine("Finished Setting folder permissions reset on (2) District.\r\n");

                        break;

                        #endregion

                    case "3":

                        #region (3) Operations

                        Console.WriteLine("Starting folder permissions reset on (3) Operations.");

                        oResetFolderPermissions.Start("/(3) Operations/");

                        WriteResetFolderPermssionsCommands();
                        Console.WriteLine("Finished Setting folder permissions reset on (3) Operations.\r\n");

                        break;

                        #endregion

                    case "4":

                        #region (4) Payroll

                        Console.WriteLine("Starting folder permissions reset on (4) Payroll.\r\n");

                        oResetFolderPermissions.Start("/(4) Payroll/");

                        WriteResetFolderPermssionsCommands();
                        Console.WriteLine("Finished Setting folder permissions reset on (4) Payroll.\r\n");

                        break;

                        #endregion

                    case "5":
                        Console.WriteLine("Starting folder permissions reset on All folders.");
                        break;
                }
            }
        }

        private static void WriteResetFolderPermssionsCommands()
        {
            Console.Clear();
            WriteEnvironmentVars();
            Console.WriteLine("Option 16: Reset folder permissions based off ZROPS\r\r");
            Console.WriteLine("(0) Return to main menu.");
            Console.WriteLine("(9) Archives");
            Console.WriteLine("(1) Accounting");
            Console.WriteLine("(2) District");
            Console.WriteLine("(3) Operations");
            Console.WriteLine("(4) Payroll");
            Console.WriteLine("(5) All");
        }

        #endregion

        #region File Monitor

        private static void WriteFileMonitorMenu()
        {
            Console.Clear();

            bool m_bDebug = false;
            if (remotingService != null)
                m_bDebug = remotingService.Debug;

            WriteEnvironmentVars();
            
            Console.WriteLine("(0)  Return to Main Menu");
            Console.WriteLine("(1)  Start the Remoting Service");
            Console.WriteLine("(2)  Start SAP File Monitor");
            Console.WriteLine("(3)  Start Folder Import File Monitor");
            Console.WriteLine("(4)  Start ZROPS Import File Monitor");
            Console.WriteLine("(5)  Stop the Remoting Service");
            Console.WriteLine("(6)  Stop SAP File Monitor");
            Console.WriteLine("(7)  Stop Folder Import File Monitor");
            Console.WriteLine("(8)  Stop ZROPS Import File Monitor");
            Console.WriteLine("(9)  Show File Monitors Status");
            Console.WriteLine("(10) Current Running Threads");
            Console.WriteLine("(11) Current Running Actuate Jobs");
            Console.WriteLine("(12) Reset Thread and Actuate Job counts to zero");
            Console.WriteLine("(13) Toggle debug threading output, currently set to: " + m_bDebug);
        }


        private static void FileMonitor()
        {
            WriteFileMonitorMenu();
            string sVal = "";
            while (sVal != "0")
            { 
                sVal = Console.ReadLine();

                #region switch menu options

                switch (sVal)
                {
                    case "0":
                        MainScreen();
                        break;
                    case "1":
                        try
                        {
                            Console.WriteLine("Starting Remoting Service...");

                            ChannelServices.RegisterChannel(new TcpChannel(ConfigurationReader.RemoteSettings.RemotingPort), false);
                            
                            //// Expose an object for remote calls.
                            RemotingConfiguration.RegisterWellKnownServiceType(
                                typeof(RemotingService), "RemotingService",
                                WellKnownObjectMode.Singleton);

                            remotingService =
                                (RemotingService)Activator.GetObject(typeof(RemotingService),
                                    ConfigurationReader.RemoteSettings.RemoteMachinePort);

                            int y = remotingService.RunningThreads;                            
                            
                            Console.WriteLine("Remoting Service is running...");
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        break;
                    case "2":
                        if (remotingService != null)
                        {
                            try
                            {
                                Console.WriteLine("Starting SAP File Monitor\r");
                                remotingService.StartSapFilePickup();
                                Console.WriteLine("SAP File Monitor Set\r\nReady...\n");
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                        else
                        {
                            Console.WriteLine(
                                "Error:  Remoting service needs to be started prior to the SAP file monitor");
                        }
                        break;
                    case "3":
                        if (remotingService != null)
                        {
                            try
                            {
                                Console.WriteLine("Starting Folder Import File Monitor\r");
                                remotingService.StartFolderImportFilePickup();
                                Console.WriteLine("Folder Import File Monitor Set\r\nReady...\n");
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                        else
                        {
                            Console.WriteLine(
                                "Error:  Remoting service needs to be started prior to the Folder Import file monitor");
                        }
                        break;
                    case "4":
                        if (remotingService != null)
                        {
                            Console.WriteLine("Starting ZROPS Import File Monitor\r");
                            remotingService.StartZropsImportFilePickup();
                            Console.WriteLine("ZROPS Import File Monitor Set\r\nReady...\n");
                        }
                        else
                            Console.WriteLine(
                                "Error:  Remoting service needs to be started prior to the ZROPS Import File monitor");
                        break;
                    case "5":
                        try
                        {
                            Console.WriteLine("Not yet implemented...");
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                        break;
                    case "6":
                        try
                        {
                            remotingService.StopSapFilePickup();
                            Console.WriteLine("Stopped SAP File Monitor\r\n");
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }

                        break;
                    case "7":
                        try
                        {
                            remotingService.StopFolderImportFilePickup();
                            Console.WriteLine("Stopped Folder Import File Monitor\r\n");
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        break;
                    case "8":
                        try
                        {
                            remotingService.StartZropsImportFilePickup();
                            Console.WriteLine("Stopped Zrops Import File Monitor\r\n");
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        break;
                    case "9":
                        bool m_bRemotingServiceIsUp = false;
                        bool m_bSAPFileMonIsUp = false;
                        bool m_bFolderImportMonIsUp = false;
                        bool m_bZROPSImportMonIsUp = false;

                        if (remotingService != null)
                        {
                            m_bRemotingServiceIsUp = true;
                            m_bSAPFileMonIsUp = remotingService.IsSapFilePickupMonitorRunning;
                            m_bFolderImportMonIsUp = remotingService.IsFolderImportMonitorRunning;
                            m_bZROPSImportMonIsUp = remotingService.IsZropsFilePickupTurnedOn;
                        }
                        Console.WriteLine("Remoting Object up           : " + m_bRemotingServiceIsUp);
                        Console.WriteLine("SAP File Montor up           : " + m_bSAPFileMonIsUp);
                        Console.WriteLine("Folder Import File Montor up : " + m_bFolderImportMonIsUp);
                        Console.WriteLine("ZROPS Import File Montor up  : " + m_bZROPSImportMonIsUp);
                        break;

                    case "10":
                        Console.WriteLine("Number of running threads: {0}.\r\n", remotingService.RunningThreads);
                        break;

                    case "11":
                        Console.WriteLine("Number of running Actuate Jobs: {0}.\r\n",
                                          remotingService.RunningActuateReports);
                        break;

                    case "12":
                        remotingService.ResetThreadAndActuateCountsToZero();
                        Console.WriteLine("Number of running threads: {0}.\r", remotingService.RunningThreads);
                        Console.WriteLine("Number of running Actuate Jobs: {0}.\r",
                                          remotingService.RunningActuateReports);
                        break;

                    case "13":
                        if (remotingService.Debug)
                        {
                            remotingService.Debug = false;
                        }
                        else
                        {
                            remotingService.Debug = true;
                        }
                        Console.WriteLine("Threading count debug output is set to: " + remotingService.Debug);
                        break;

                    default:
                        WriteFileMonitorMenu();
                        break;
                }

                #endregion
            }
        }

        #endregion

        #region Folder Analysis
        private static void FolderAnalysisScreen()
        {
            String currentFolder = "/";
            WriteFolderAnalysisMenu(currentFolder);
            ActuateFolder af = new ActuateFolder();
            string sVal = "";
            while (sVal != "0")
            {
                sVal = Console.ReadLine();

                #region switch menu options
                switch (sVal)
                {
                    case "0":
                        MainScreen();
                        break;
                    case "1":
                        
                        Console.WriteLine("Threading count debug output is set to: " + remotingService.Debug);
                        break;
                    case "2":
                        //Set Current Folder
                        Console.WriteLine("Please enter a valid Actuate folder path:");
                        string folderPath = Console.ReadLine();
                        if (af.doesFolderExist(folderPath))
                        {
                            currentFolder = folderPath;
                            WriteFolderAnalysisMenu(currentFolder);
                        }
                        else
                        {
                            Console.WriteLine("Folder " + folderPath + " does not exist.  Please press a key to try again");
                            
                            Console.Read();
                            WriteFolderAnalysisMenu(currentFolder);
                        }
                        break;
                    case "3":
                        //list folder contents
                        Console.WriteLine("What is the max number of reports in each folder do you want to search for?");
                        int minReportCount = 0;
                        try
                        {
                            minReportCount = Convert.ToInt32(Console.ReadLine());
                        }
                        catch
                        {
                            Console.WriteLine("hey, you need to enter an integer!  Try again...");
                        }
                        List<Server_Proxy.localhost.File> files = af.listFolderContents(currentFolder, true, minReportCount);
                        foreach (Server_Proxy.localhost.File f in files)
                        {
                            Console.WriteLine("{0,-40} {1,10} {2,10}", f.Name, f.Size, f.TimeStamp);
                            //Console.WriteLine("The value is {0,10} � continue from here",9999);
                        }
                        break;
                    case "4":
                        // list folders with n or more reports
                        Console.WriteLine("What is the max number of reports in each folder do you want to search for?");
                        int n = 1;
                        try
                        {
                            n = Convert.ToInt32(Console.ReadLine());
                        }
                        catch
                        {
                            Console.WriteLine("hey, you need to enter an integer!  Try again...");
                            break;
                        }
                        List<ActuateFolderDetails> dirs = af.listFoldersWithReports(currentFolder, n);

                        foreach (ActuateFolderDetails afd in dirs)
                        {
                            Console.WriteLine("{0,40} {1} {2} ", afd.Name, afd.NumberOfFiles, afd.SizeBytes);
                        }

                        break;
                    case "5":
                        Console.WriteLine("List all reports date, pages and sibling count details");
                        System.IO.StreamWriter sw = new System.IO.StreamWriter("c:\\temp\\ActuateDetailsReport.txt");
                        sw.WriteLine("\"Pathname\",\"CreatedTime\",\"Size\",\"PageCount\",\"SiblingCount\"");
                        List<ActuateFileDetails> aFileDetails = af.GetFileDetailsByFolder(currentFolder, true);
                        foreach (ActuateFileDetails f in aFileDetails)
                        {
                            Console.WriteLine("\"{0}{1}\",\"{2}\",\"{3}\"", f.Path, f.Name, f.CreatedTime, f.SizeBytes);
                            sw.WriteLine("\"{0}{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\"", f.Path, f.Name, f.CreatedTime, f.SizeBytes, f.PageCount, f.NumberOfSiblings);
                        }
                        sw.Close();
                        break;
                    case "6":
                        Console.WriteLine("Downloading Reports in the Current Folder");
                        af.DownloadReports(currentFolder);
                        break;
                    case "99":
                        Console.WriteLine("Testing for more Actuate file and directory features");
                        af.TestDetails(currentFolder);
                        break;
                    default:
                        WriteFolderAnalysisMenu(currentFolder);
                        break;
                 }
                 #endregion switch menu options
             }
        }

        private static void WriteFolderAnalysisMenu(String currentFolder)
        {
            Console.Clear();

            bool m_bDebug = false;
            if (remotingService != null)
                m_bDebug = remotingService.Debug;

            WriteEnvironmentVars();

            Console.WriteLine("(0)  Return to Main Menu");
            Console.WriteLine("(1)  Toggle debug threading output, currently set to: " + m_bDebug);
            Console.WriteLine("(2)  Set Current Folder, currently set to: " + currentFolder);
            Console.WriteLine("(3)  List Folder Contents");
            Console.WriteLine("(4)  Get details of folders with n or more reports");
            Console.WriteLine("(5)  List reports older than 13 months.");
            Console.WriteLine("(6)  Download All Reports by Folder.");
            Console.WriteLine("(99) Test...");
        }

 
        #endregion Folder Analysis

        # region Folder Download



        #endregion

        #region Import Zrops

        private static void ImportZrops()
        {
            ImportZrops oImportZrops = new ImportZrops();

            WriteImportZropsCommands();

            string sVal = "";

            while (sVal != "0")
            {
                sVal = Console.ReadLine();

                switch (sVal)
                {
                    case "cls":

                        #region Clear Screen

                        Console.Clear();
                        WriteImportZropsCommands();
                        break;

                        #endregion

                    case "0": // Return to main menu.                        
                        MainScreen();
                        break;

                    case "1":

                        #region (1) Backup

                        Console.WriteLine("Starting backup of Zrops table");
                        oImportZrops.StartBackup();
                        WriteImportZropsCommands();
                        Console.WriteLine("Backup of Zrops table complete.\r\n");
                        break;

                        #endregion

                    case "2":

                        #region (2) Import

                        Console.WriteLine("Starting import of ZROPS table.");
                        oImportZrops.StartImport();
                        WriteImportZropsCommands();
                        Console.WriteLine("Import of ZROPS table complete.\r\n");
                        break;

                        #endregion

                    case "3":

                        #region (3) Restore

                        Console.WriteLine("Starting restoration of zrops table from zrops_backup table.");
                        oImportZrops.StartRestore();
                        WriteImportZropsCommands();
                        Console.WriteLine("Restoration complete.\r\n");
                        break;

                        #endregion

                    case "4":

                        #region (4) Backup and Import

                        Console.WriteLine("Starting backup of Zrops table");
                        oImportZrops.StartBackup();
                        Console.WriteLine("Backup of Zrops table complete.\r\n");
                        Console.WriteLine("Starting import of ZROPS table.");
                        oImportZrops.StartImport();
                        WriteImportZropsCommands();
                        Console.WriteLine("Import of ZROPS table complete.\r\n");
                        break;

                        #endregion

                    case "5":

                        #region (5) Automated Import via the Remoting Service

                        remotingService.StartZropsImportFilePickup();
                        Console.WriteLine("Zrops Import Listener Started...");
                        break;

                        #endregion
                }
            }
        }


        private static void WriteImportZropsCommands()
        {
            Console.Clear();
            WriteEnvironmentVars();
            Console.WriteLine("Option 18: Import ZROPS update from SAP data file\r\r");
            Console.WriteLine("(0) Return to main menu.");
            Console.WriteLine("(1) Backup");
            Console.WriteLine("(2) Import");
            Console.WriteLine("(3) Restore");
            Console.WriteLine("(4) Backup and Import");
            Console.WriteLine("(5) Automate Import with the Remoting Object");
        }

        #endregion
    }
}