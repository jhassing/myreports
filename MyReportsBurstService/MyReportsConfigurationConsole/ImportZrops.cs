using System;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.SqlServerDal;

namespace MyReports.Burst.ConfigurationConsole
{
    class ImportZrops
    {
        public void StartBackup()
        {
            try
            {
                new Zrops(ConfigurationReader.Db).BackupToTable();
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public void StartRestore()
        {
            try
            {
                new Zrops(ConfigurationReader.Db).RestoreFromBackupTable();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public void StartImport()
        {
            try
            {
                //new MyReports.Burst.Import.Zrops.ZropsImport.ZropsFileImport();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
