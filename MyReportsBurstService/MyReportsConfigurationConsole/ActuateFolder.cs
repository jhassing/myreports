﻿using System;
using System.IO;
using MyReports.Burst.Common;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using MyReports.Burst.Common.Configuration.Helper;
using System.Collections.Generic;
using Server_Proxy.localhost;

namespace MyReports.Burst.ConfigurationConsole
{
    internal class ActuateFolder : ActuateFileFolderFunctions
    {
        public Boolean doesFolderExist(string path)
        {
            Boolean exists = false;

            try
            {
                // Step 1:  Log Into Actuate
                Console.WriteLine("Signing into Actuate.\r\n");
                base.LoginActuate();
                Console.WriteLine("Signed into Actuate... continuing actions\r\n");

                if (base.GetDirectoryContents(path).TotalCount > 0) exists = true;

                base.Logout();
            }
            catch (System.Web.Services.Protocols.SoapException ex) {
                Console.WriteLine("the path " + path + " is invalid...");
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                exists = false;
            }

            return exists;
        }

        public List<Server_Proxy.localhost.File> listFolderContents(string path, Boolean recursive)
        {
            return listFolderContents(path, recursive, 1);
        }

        public List<Server_Proxy.localhost.File> listFolderContents(string path)
        {
            // Step 1:  Log Into Actuate
            Console.WriteLine("Signing into Actuate.\r\n");
            base.LoginActuate();
            Console.WriteLine("Signed into Actuate... continuing actions\r\n");

            List<Server_Proxy.localhost.File> files = new List<Server_Proxy.localhost.File>();

            Server_Proxy.localhost.GetFolderItemsResponse gfir = base.GetDirectoryContents(path);
            foreach (Server_Proxy.localhost.File f in gfir.ItemList)
            {
                files.Add(f);
            }

            base.Logout();

            return files;
        }

        public List<Server_Proxy.localhost.File> listFolderContents(string path, Boolean recursive, int minimumFileCount)
        {
            // Step 1:  Log Into Actuate
            Console.WriteLine("Signing into Actuate.\r\n");
            base.LoginActuate();
            Console.WriteLine("Signed into Actuate... continuing actions\r\n");
            long ticks = new DateTime().Ticks;
            List<Server_Proxy.localhost.File> files = new List<Server_Proxy.localhost.File>();

            int i = 0;

            Server_Proxy.localhost.GetFolderItemsResponse gfir = base.GetDirectoryContents(path);
            foreach (Server_Proxy.localhost.File f in gfir.ItemList)
            {
                files.Add(f);
                i++;
                Console.WriteLine(i + " " + (new DateTime().Ticks - ticks));
                ticks = new DateTime().Ticks;

                if (f.FileType == "Directory")
                {
                    Server_Proxy.localhost.GetFolderItemsResponse gfir2 = base.GetDirectoryContents(path + "/" + f.Name);
                    if (gfir2.TotalCount > minimumFileCount)
                    {
                        foreach (Server_Proxy.localhost.File f2 in gfir2.ItemList)
                        {
                            Console.WriteLine("Adding file to list: " + f.Name + "/" + f2.Name);
                            files.Add(f2);
                        }
                    }
                }
            }

            base.Logout();

            return files;
        }
        public List<ActuateFolderDetails> listFoldersWithReports(string path, int minimumFileCount)
        {
            // Step 1:  Log Into Actuate
            Console.WriteLine("Signing into Actuate.\r\n");
            base.LoginActuate();
            Console.WriteLine("Signed into Actuate... continuing actions\r\n");
            List<ActuateFolderDetails> dirList = new List<ActuateFolderDetails>();

            Server_Proxy.localhost.GetFolderItemsResponse gfir = base.GetDirectoryContents(path);
            foreach (Server_Proxy.localhost.File f in gfir.ItemList)
            {
                if (f.FileType == "Directory")
                {
                    Server_Proxy.localhost.GetFolderItemsResponse gfir2 = base.GetDirectoryContents(path + "/" + f.Name);
                    if (gfir2.TotalCount > minimumFileCount)
                    {
                        ActuateFolderDetails actuateFolderDetails = new ActuateFolderDetails();
                        actuateFolderDetails.Name = f.Name;
                        actuateFolderDetails.NumberOfFiles = gfir2.TotalCount;
                        long b = 0;
                        foreach (Server_Proxy.localhost.File f2 in gfir2.ItemList)
                        {
                            b += f2.Size;
                        }
                        dirList.Add(actuateFolderDetails);
                    }
                }
            }

            base.Logout();

            return dirList;
        }

        internal List<ActuateFileDetails> GetFileDetailsByFolder(string pathname, Boolean recurse)
        {
            List<ActuateFileDetails> afdList = new List<ActuateFileDetails>();
            // Step 1:  Log Into Actuate
            Console.WriteLine("Signing into Actuate.\r\n");
            base.LoginActuate();
            Console.WriteLine("Signed into Actuate... continuing actions\r\n");
            long ticks = new DateTime().Ticks;

            Server_Proxy.localhost.GetFolderItemsResponse gfir = base.GetDirectoryContents(pathname);
            foreach (Server_Proxy.localhost.File f in gfir.ItemList)
            {
                if (f.FileType == "Directory")
                {
                    Server_Proxy.localhost.GetFolderItemsResponse gfir2 = base.GetDirectoryContents(pathname + "/" + f.Name);
                    foreach (Server_Proxy.localhost.File f2 in gfir2.ItemList)
                    {
                        Console.WriteLine(f.Name + "/" + f2.Name);
                        ActuateFileDetails curFile = new ActuateFileDetails();
                        curFile.CreatedTime = f2.TimeStamp;
                        curFile.Name = f2.Name;
                        curFile.SizeBytes = f2.Size;
                        curFile.Path = pathname + "/" + f.Name + "/";
                        curFile.NumberOfSiblings = gfir2.TotalCount;
                        curFile.PageCount = f2.PageCount;
                        afdList.Add(curFile);
                    }
                }
                else
                {
                    ActuateFileDetails afd = new ActuateFileDetails();
                    afd.Name = f.Name;
                    afd.Path = pathname + "/";
                    afd.CreatedTime = f.TimeStamp;
                    afd.NumberOfSiblings = gfir.TotalCount;
                    afd.PageCount = f.PageCount;
                    afdList.Add(afd);
                }
            }

            base.Logout();
            return afdList;
        }

        internal List<ActuateFileDetails> GetFileDetailsByFolder(string pathname)
        {
            // Step 1:  Log Into Actuate
            Console.WriteLine("Signing into Actuate.\r\n");
            base.LoginActuate();
            Console.WriteLine("Signed into Actuate... continuing actions\r\n");

            Server_Proxy.localhost.GetFolderItemsResponse gfir = base.GetDirectoryContents(pathname);
            List<ActuateFileDetails> afd = new List<ActuateFileDetails>();

            foreach (Server_Proxy.localhost.File f in gfir.ItemList)
            {
                //Console.WriteLine(f.Name);
                ActuateFileDetails curFile = new ActuateFileDetails();
                curFile.CreatedTime = f.TimeStamp;
                curFile.Name = f.Name;
                curFile.SizeBytes = f.Size;
                curFile.Path = pathname + "/";
                curFile.NumberOfSiblings = gfir.TotalCount;
                curFile.PageCount = f.PageCount;
                afd.Add(curFile);
            }
            base.Logout();
            return afd;
        }

        internal void DownloadReports(string pathName)
        {
            // Step 1:  Log Into Actuate
            Console.WriteLine("Signing into Actuate.\r\n");
            base.LoginActuate();

            Server_Proxy.localhost.GetFolderItemsResponse gfir = base.GetDirectoryContents(pathName);
            String nPath = "c:\\temp\\" + pathName;
            System.IO.Directory.CreateDirectory(nPath);
            String sdName = new DirectoryInfo(nPath).FullName;
            foreach (Server_Proxy.localhost.File f in gfir.ItemList)
            {
                if (f.FileType.ToLower() == "roi")
                {
                    base.DownloadFile(pathName + "/" + f.Name, sdName + "\\" + f.Name);
                }
            }



            base.Logout();
            Console.WriteLine("Logged off of Actuate.\r\n");
        }

        internal void TestDetails(string fileName)
        {
            base.LoginActuate();
            DownloadFile downloadFile = new DownloadFile
            {
                Item = "/(3) Operations/000100 - C&S Vending/bsf- - Operation Balance Sheet ZPRA.roi",
                ItemElementName = ItemChoiceType34.FileName,
                DownloadEmbedded = true
            };

            DownloadFileResponse lRes = LProxy.downloadFile(downloadFile);
            Server_Proxy.localhost.DownloadFileResponse dfr = new DownloadFileResponse();
            
            //dfr.File.FileType = "pdf";
            //Server_Proxy.localhost.File f = new Server_Proxy.localhost.File();
            //f.FileType = "pdf";
                       
            //	Save downloaded file
            MemoryStream memoryStream = new MemoryStream(((Attachment)lRes.Item).ContentData);
            FileStream fileStream = new FileStream("C:\\Temp\\(3) Operations\\000100 - C&S Vending\\bsf- - Operation Balance Sheet ZPRA.roi", FileMode.Create);
            memoryStream.WriteTo(fileStream);
            fileStream.Close();
            base.Logout();
        }
    }


    public struct ActuateFolderDetails
    {
        private String _name;
        private long _numberOfFiles;
        private int _sizeBytes;
        private DateTime _createdTime;

        public String Name
        {
            get{ return _name;}
            set{ _name = value;}
        }
        public long NumberOfFiles
        {
            get { return _numberOfFiles; }
            set { _numberOfFiles = value; }
        }
        public int SizeBytes
        {
            get { return _sizeBytes; }
            set { _sizeBytes = value; }
        }
        public DateTime CreatedTime
        {
            get { return _createdTime; }
            set { _createdTime = value; }
        }
    }

    public struct ActuateFileDetails
    {
        private String _name;
        private String _path;
        private long _sizeBytes;
        private DateTime _createdTime;
        private long _numberOfSiblings;
        private long _pageCount;

        public String Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public String Path
        {
            get { return _path; }
            set { _path = value; }
        }
        public long SizeBytes
        {
            get { return _sizeBytes; }
            set { _sizeBytes = value; }
        }
        public DateTime CreatedTime
        {
            get { return _createdTime; }
            set { _createdTime = value; }
        }
        public long NumberOfSiblings
        {
            get { return _numberOfSiblings; }
            set { _numberOfSiblings = value; }
        }
        public long PageCount
        {
            get { return _pageCount; }
            set { _pageCount = value; }
        }
    }
}
