using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;


namespace RDconfig
{
    class tFile
    {
        private string _name;
        private DateTime _timeStamp;

        public tFile(string name, DateTime timeStamp)
        {
            this._name = name;
            this._timeStamp = timeStamp;
        }

        public string Name
        {
            get { return this._name; }
        }

        public DateTime TimeStamp
        {
            get { return this._timeStamp; }
        }

        public static int Compare(tFile x, tFile y)
        {
            return new CaseInsensitiveComparer().Compare(x.TimeStamp, y.TimeStamp);
        }
    }


    class RestoreArchive
    {
        public void Foo()
        {
            //if (args.Length != 2)
            //{
            //    System.Console.WriteLine(" Usage:    RestoreMyReportsArchive from_dir destination_dir");
            //    System.Console.WriteLine(" Example:  RestoreMyReportsArchive \\\\reportserver\\f$\\ActuateBurstArchive \\\\reportserver\\f$\\inetpub\\ftproot\\");
            //}
            //else
            {

                List<tFile> aryFiles = new List<tFile>();

                DirectoryInfo directory = new DirectoryInfo(@"C:\ActuateBurstArchive");
                //DirectoryInfo directory = new DirectoryInfo(args[0]);

                string ftproot = @"c:\inetpub\ftproot\";
                //string ftproot = args[1];

                FileInfo[] files = directory.GetFiles();

                for (int i = 0; i < files.Length; i++)
                {
                    aryFiles.Add(new tFile(files[i].Name, files[i].CreationTime));
                }

                aryFiles.Sort(tFile.Compare);

                foreach (tFile file in aryFiles)
                {
                    //copy the file to \\cguschp250\e$\inetpub\ftproot
                    string oldPathName = directory + "\\" + file.Name;
                    string newPathName = ftproot + "\\" + file.Name;
                    bool overwrite = true;

                    System.IO.File.Copy(oldPathName, newPathName, overwrite);
                    Console.WriteLine(file + " Moved to: \n" + newPathName);

                    System.Threading.Thread.Sleep(1000);
                    System.IO.File.Create(newPathName + ".end");
                    Console.WriteLine("End file created for: " + newPathName);

                    // then, rename the file, adding .sub to the end if necessary
                    //System.IO.File.Move(newPathName, newPathName);

                    // Sleep for 1 second
                    System.Threading.Thread.Sleep(3000);


                }
            }
        }
    }
}

