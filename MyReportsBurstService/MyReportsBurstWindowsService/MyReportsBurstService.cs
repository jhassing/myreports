using System;
using System.ComponentModel;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.ServiceProcess;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.RemotingObject;

namespace MyReports.BurstWindowsService
{
    public class MyReportsBurstService : ServiceBase
    {
        private RemotingService _remotingService;

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        public MyReportsBurstService()
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // MyReportsBurstService
            // 
            ServiceName = "MyReports Burst Service";
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Set things in motion so your service can do its work.
        /// </summary>
        protected override void OnStart(string[] args)
        {
            #region Start the Remoting Service

            ChannelServices.RegisterChannel(new TcpChannel(ConfigurationReader.RemoteSettings.RemotingPort), false);

            // Expose an object for remote calls.
            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof (RemotingService), "RemotingService",
                WellKnownObjectMode.Singleton);

            _remotingService =
                (RemotingService)
                Activator.GetObject(typeof (RemotingService), ConfigurationReader.RemoteSettings.RemoteMachinePort);

            _remotingService.StartSapFilePickup();

            #endregion

            #region Start the File Monitor

            _remotingService.StartSapFilePickup();

            #endregion
        }

        /// <summary>
        /// Stop this service.
        /// </summary>
        protected override void OnStop()
        {
            // Stop the singleton object            
            _remotingService.StopSapFilePickup();

            _remotingService = null;
        }
    }
}