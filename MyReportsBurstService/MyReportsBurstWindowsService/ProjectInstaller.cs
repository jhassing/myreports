using System.ComponentModel;

namespace ReportDistribution.BurstWindowsService
{
    /// <summary>
    /// Summary description for ProjectInstaller.
    /// </summary>
    [RunInstaller(true)]
    public class ProjectInstaller : System.Configuration.Install.Installer
    {
        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller MyReportsBurstServiceInstaller;
        private System.ServiceProcess.ServiceInstaller MyReportsFolderImportService;
        private System.ServiceProcess.ServiceInstaller MyReportsSAPfileremovalInstaller;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public ProjectInstaller()
        {
            // This call is required by the Designer.
            InitializeComponent();

            // TODO: Add any initialization after the InitializeComponent call
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }


        #region Component Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.MyReportsBurstServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.MyReportsFolderImportService = new System.ServiceProcess.ServiceInstaller();
            this.MyReportsSAPfileremovalInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstaller1
            // 
            this.serviceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceProcessInstaller1.Password = null;
            this.serviceProcessInstaller1.Username = null;
            // 
            // MyReportsBurstServiceInstaller
            // 
            this.MyReportsBurstServiceInstaller.ServiceName = "MyReports Burst Service";
            // 
            // MyReportsFolderImportService
            // 
            this.MyReportsFolderImportService.ServiceName = "MyReports Folder Import Service";
            // 
            // MyReportsSAPfileremovalInstaller
            // 
            this.MyReportsSAPfileremovalInstaller.ServiceName = "MyReports SAP file removal";
            this.MyReportsSAPfileremovalInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.MyReportsSAPfileremovalInstaller_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
																					  this.serviceProcessInstaller1,
																					  this.MyReportsBurstServiceInstaller,
																					  this.MyReportsFolderImportService,
																					  this.MyReportsSAPfileremovalInstaller});

        }
        #endregion

        private void MyReportsSAPfileremovalInstaller_AfterInstall(object sender, System.Configuration.Install.InstallEventArgs e)
        {

        }
    }
}
