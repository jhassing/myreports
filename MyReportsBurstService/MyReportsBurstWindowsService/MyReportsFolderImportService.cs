using System;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.RemotingObject;

namespace MyReports.BurstWindowsService
{
    public class MyReportsFolderImportService : System.ServiceProcess.ServiceBase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public MyReportsFolderImportService()
        {
            // This call is required by the Windows.Forms Component Designer.
            InitializeComponent();

            // TODO: Add any initialization after the InitComponent call
        }

        #region Component Designer generated code
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // MyReportsFolderImportService
            // 
            this.ServiceName = "MyReports Folder Import Service";

        }
        #endregion

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Set things in motion so your service can do its work.
        /// </summary>
        protected override void OnStart(string[] args)
        {
            // Remoting values						
            RemotingService service1 =
                (RemotingService) Activator.GetObject(typeof (RemotingService),
                                                      ConfigurationReader.RemoteSettings.RemoteMachinePort);

            System.Threading.Thread.Sleep(2);
            service1.StartFolderImportFilePickup();
        }

        /// <summary>
        /// Stop this service.
        /// </summary>
        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            // Remoting values						
            RemotingService service1 =
                (RemotingService)Activator.GetObject(typeof(RemotingService),
                                                      ConfigurationReader.RemoteSettings.RemoteMachinePort);

            System.Threading.Thread.Sleep(2);
            service1.StopFolderImportFilePickup();
        }
    }
}
