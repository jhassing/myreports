using System;
using System.ServiceProcess;

using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.BurstWindowsService;

namespace ReportDistribution.BurstWindowsService
{
    /// <summary>
    /// Summary description for Entry.
    /// </summary>
    public class Entry
    {
        public Entry()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        static void Main()
        {

            try
            {

                ServiceBase[] ServicesToRun = new System.ServiceProcess.ServiceBase[] { new MyReportsBurstService(), new MyReportsFolderImportService(), new MyReportsSaPfileRemoval() };

                System.ServiceProcess.ServiceBase.Run(ServicesToRun);

            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
            }
        }

    }
}
