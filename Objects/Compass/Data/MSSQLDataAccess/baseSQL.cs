using System;
using System.Data;
using System.Data.SqlClient;


namespace Compass.Data.MSSQLDataAccess
{

    public class baseSQL
    {

        protected string _connectionString = "";
        protected SqlConnection _sqlConn = null;
        protected SqlTransaction _sqlTran = null;
        protected SqlParameter[] _arParms = null;

        #region ~ctor

        public baseSQL(string connectionString)
        {
            _connectionString = connectionString;
        }

        #endregion

        #region Private Methods

        #region Open DB Connection

        protected void OpenDBConnection()
        {
            if (this._sqlConn == null)
                this._sqlConn = new SqlConnection(this._connectionString);

            if (this._sqlConn.State == System.Data.ConnectionState.Closed)
                this._sqlConn.Open();
        }

        #endregion

        #region Get Transaction

        private void StartDBTransaction()
        {
            OpenDBConnection();
            this._sqlTran = this._sqlConn.BeginTransaction();
        }

        #endregion

        #region Close DB Connection

        public void CloseDBConnection()
        {
            if (this._sqlConn != null)
            {
                this._sqlConn.Close();
                this._sqlConn.Dispose();
            }

            if (this._sqlTran != null)
            {
                this._sqlTran.Dispose();
            }

            this._sqlTran = null;
            this._sqlConn = null;

        }

        #endregion

        #endregion

        #region Execute Non Query

        protected void ExecuteNonQuery(string storedProcName)
        {
            #region code

            try
            {
                this.OpenDBConnection();

                SqlHelper.ExecuteNonQuery(this._sqlConn, CommandType.StoredProcedure, storedProcName, this._arParms);

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                this.CloseDBConnection();
            }

            #endregion
        }

        #endregion

        #region Execute DataSet

        protected DataSet ExecuteDataset(string storedProcName)
        {
            #region code

            try
            {
                this.OpenDBConnection();

                return SqlHelper.ExecuteDataset(this._sqlConn, storedProcName, this._arParms);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                this.CloseDBConnection();
            }

            #endregion
        }

        #endregion

        #region Execute DataReader

        protected SqlDataReader ExecuteDataReader(string storedProcName)
        {
            #region code

            try
            {
                this.OpenDBConnection();

                return SqlHelper.ExecuteReader(this._sqlConn, storedProcName, this._arParms);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {

            }

            #endregion
        }

        #endregion

        #region Execute Non Query With Transaction

        protected void ExecuteNonQuery_WithTransaction(string storedProcName)
        {
            #region code

            try
            {
                StartDBTransaction();

                SqlHelper.ExecuteNonQuery(this._sqlTran, CommandType.StoredProcedure, storedProcName, this._arParms);

                this._sqlTran.Commit();
            }
            catch (Exception ex)
            {
                this._sqlTran.Rollback();
                throw (ex);
            }
            finally
            {
                this.CloseDBConnection();
            }

            #endregion
        }

        #endregion

    }
}
