using System;
using System.Collections;
using java.util;
using java.util.zip;

namespace Compass.FileSystem.Compression
{
	#region delegates

	public delegate Enumeration EnumerationMethod();
	public delegate bool FilterEntryMethod(ZipEntry e);

	#endregion

	#region class EnumerationAdapter
	/// <summary>
	/// Wraps java enumerators 
	/// </summary>
	internal class EnumerationAdapter : IEnumerable
	{
		private class EnumerationWrapper : IEnumerator
		{
			private EnumerationMethod m_Method;
			private Enumeration m_Wrapped;
			private object m_Current;

			public EnumerationWrapper(EnumerationMethod method)
			{
				m_Method = method;
			}

			// IEnumerator
			public object Current
			{
				get { return m_Current; }
			}

			public void Reset()
			{
				m_Wrapped = m_Method();
				if (m_Wrapped == null)
					throw new InvalidOperationException();
			}

			public bool MoveNext()
			{
				if (m_Wrapped == null)
					Reset();
				bool Result = m_Wrapped.hasMoreElements();
				if (Result)
					m_Current = m_Wrapped.nextElement();
				return Result;
			}
		}

		private EnumerationMethod m_Method;

		public EnumerationAdapter(EnumerationMethod method)
		{
			if (method == null)
				throw new ArgumentException();
			m_Method = method;
		}

		// IEnumerable
		public IEnumerator GetEnumerator()
		{
			return new EnumerationWrapper(m_Method);
		}
	}

	#endregion

	#region class ZipUtils

	/// <summary>
	/// Zip stream utils
	/// </summary>
	public class ZipUtils
	{
		#region Public Methods

		/// <summary>
		/// Used to extract ZIP files
		/// </summary>
		/// <param name="file">zip file to extract.</param>
		/// <param name="path">Path to extract ZIP file into</param>
		/// <param name="filter">Null</param>
		public static void ExtractZipFile(ZipFile file, string path, FilterEntryMethod filter)
		{
			foreach(ZipEntry entry in new EnumerationAdapter(new EnumerationMethod(file.entries)))
			{
				if (!entry.isDirectory())
				{
					if ((filter == null || filter(entry)))
					{
						java.io.InputStream s = file.getInputStream(entry);
						try
						{
							string fname = System.IO.Path.GetFileName(entry.getName());
							string newpath = System.IO.Path.Combine(path, System.IO.Path.GetDirectoryName(entry.getName()));

							System.IO.Directory.CreateDirectory(newpath);

							java.io.FileOutputStream dest = new java.io.FileOutputStream(System.IO.Path.Combine(newpath, fname));
							try
							{
								CopyStream(s, dest);
							}
							finally
							{
								dest.close();
							}
						}
						finally
						{
							s.close();
						}
					}
				}
			}
		}


		/// <summary>
		/// Used to create an empty zip file.
		/// </summary>
		/// <param name="fileName">Empty ZIP file to create. </param>
		/// <returns>Pointer to newly created zip file.</returns>
		public static ZipFile CreateEmptyZipFile(string fileName)
		{
			new ZipOutputStream(new java.io.FileOutputStream(fileName)).close();
			return new ZipFile(fileName);
		}


		/// <summary>
		/// Create a new zip file.
		/// </summary>
		/// <param name="file">Pointer to an already created zip file.</param>
		/// <param name="newFiles">Files that will be zipped.</param>
		/// <returns></returns>
		public static ZipFile UpdateZipFile(ZipFile file, string[] newFiles)
		{
			return UpdateZipFile(file, null, newFiles);			
		}


		/// <summary>
		/// Create a new zip file.
		/// </summary>
		/// <param name="fileName">Full path to the new zip file to create.</param>
		/// <param name="newFiles">Files that will be zipped.</param>
		/// <returns></returns>
		public static ZipFile UpdateZipFile(string fileName, string[] newFiles)
		{
			return UpdateZipFile(CreateEmptyZipFile(fileName), null, newFiles);
		}


		/// <summary>
		/// Create a new zip file.
		/// </summary>
		/// <param name="fileName">Full path to the new zip file to create.</param>
		/// <param name="filter">null</param>
		/// <param name="newFiles">Files that will be zipped.</param>
		/// <returns></returns>
		public static ZipFile UpdateZipFile(string fileName, FilterEntryMethod filter, string[] newFiles)
		{
			return UpdateZipFile(CreateEmptyZipFile(fileName), filter, newFiles);
		}

		
		/// <summary>
		/// Add files to an already existing zip file
		/// </summary>
		/// <param name="file">Pointer to existing zip file</param>
		/// <param name="filter">null</param>
		/// <param name="newFiles">Files that will be zipped.</param>
		/// <returns></returns>
		public static ZipFile UpdateZipFile(ZipFile file, FilterEntryMethod filter, string[] newFiles)
		{
			
			string prev = file.getName();
			
			string[] sss = prev.Split('\\');
			string tmp = "";

			for (int x=0;x<sss.Length-1;x++)
			{
				tmp = tmp + sss[x].ToString() + @"\";
			}

			tmp += "temp.file";

			ZipOutputStream to = new ZipOutputStream(new java.io.FileOutputStream(tmp));

			try
			{
				CopyEntries(file, to, filter);
				// add entries here
				if (newFiles != null)
				{
					foreach(string f in newFiles)
					{
						f.Replace(@"\\", @"\");

						ZipEntry qq = new ZipEntry("CraigWard.txt");

						int u = f.LastIndexOf(@"\");
						string name = f.Substring(u+1, f.Length-u-1);

						//ZipEntry z = new ZipEntry(f.Remove(0, System.IO.Path.GetPathRoot(f).Length));
						ZipEntry z = new ZipEntry(name);
						
						z.setMethod(ZipEntry.DEFLATED);
						to.putNextEntry(z);
						try
						{
							java.io.FileInputStream s = new java.io.FileInputStream(f);
							try
							{
								CopyStream(s, to);
							}
							finally
							{
								s.close();
							}
						}
						finally
						{
							to.closeEntry();
						}
					}
				}
			}
			finally
			{
				to.close();
			}
			file.close();

			// now replace the old file with the new one
			System.IO.File.Copy(tmp, prev, true);
			System.IO.File.Delete(tmp);

			/*
			try
			{
				return new ZipFile(prev);
			}
			catch(java.util.zip.ZipException ex)

			{
			 string jj = "";
			}

			*/

			return null;
		}


		#endregion

		#region Private Methods

		private static void CopyStream(java.io.InputStream from, java.io.OutputStream to)
		{
			sbyte[] buffer = new sbyte[8192];
			int got; 
			while ((got = from.read(buffer, 0, buffer.Length)) > 0)
				to.write(buffer, 0, got);
		}

		private static void CopyEntries(ZipFile from, ZipOutputStream to)
		{
			CopyEntries(from, to, null);
		}

		private static void CopyEntries(ZipFile from, ZipOutputStream to, FilterEntryMethod filter)
		{
			foreach(ZipEntry entry in new EnumerationAdapter(new EnumerationMethod(from.entries)))
			{
				if (filter == null || filter(entry))
				{
					java.io.InputStream s = from.getInputStream(entry);
					try
					{
						to.putNextEntry(entry);
						try
						{
							CopyStream(s, to);
						}
						finally
						{
							to.closeEntry();
						}
					}
					finally
					{
						s.close();
					}
				}
			}
		}

		
		#endregion
	}
	
	#endregion
}
