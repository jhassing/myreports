using System;
using System.Collections;
using System.IO;
using System.Security.Principal;
using System.Runtime.InteropServices;

namespace Compass.FileSystem.Permissions
{	
	public class FilePermissions
	{	
		#region Const and Enums
		public enum SE_OBJECT_TYPE : uint
		{
			SE_UNKNOWN_OBJECT_TYPE = 0,
			SE_FILE_OBJECT, 
			SE_SERVICE, 
			SE_PRINTER, 
			SE_REGISTRY_KEY, 
			SE_LMSHARE, 
			SE_KERNEL_OBJECT, 
			SE_WINDOW_OBJECT, 
			SE_DS_OBJECT, 
			SE_DS_OBJECT_ALL, 
			SE_PROVIDER_DEFINED_OBJECT, 
			SE_WMIGUID_OBJECT, 
			SE_REGISTRY_WOW64_32KEY
		}

		public enum SECURITY_INFORMATION : uint
		{
			OWNER_SECURITY_INFORMATION       = 0x00000001,
			GROUP_SECURITY_INFORMATION       = 0x00000002,
			DACL_SECURITY_INFORMATION        = 0x00000004,
			SACL_SECURITY_INFORMATION        = 0x00000008,

			// Win2k only
			PROTECTED_DACL_SECURITY_INFORMATION     = 0x80000000,
			// Win2k only
			PROTECTED_SACL_SECURITY_INFORMATION     = 0x40000000,
			// Win2k only
			UNPROTECTED_DACL_SECURITY_INFORMATION   = 0x20000000,
			// Win2k only
			UNPROTECTED_SACL_SECURITY_INFORMATION   = 0x10000000,
		}

		public const string SE_RESTORE_NAME = "SeRestorePrivilege";

		[StructLayout(LayoutKind.Sequential)]
			public struct LUID
		{
			public uint lowpart;
			public uint highpart;
		}
        
		[StructLayout(LayoutKind.Sequential)]
			public struct LUID_AND_ATTRIBUTES
		{
			public LUID pLuid;
			public uint Attributes;
		}

		[StructLayout(LayoutKind.Sequential)]
			public struct TOKEN_PRIVILEGES
		{
			public int PrivilegeCount;
			public LUID_AND_ATTRIBUTES privileges;
		}

		[StructLayout(LayoutKind.Sequential)]
			public struct TRUSTEE
		{
			public uint pMultipleTrustee;
			public uint MultipleTrusteeOperation;
			public uint TrusteeForm;
			public uint TrusteeType;
			public uint ptstrName;
		}

		[StructLayout(LayoutKind.Sequential)]
			public struct EXPLICIT_ACCESS
		{
			public uint grfAccessPermissions;
			public uint grfAccessMode;
			public uint grfInheritance;
			public TRUSTEE pTRUSTEE;
		}

		// Generic access rights extracted from WinNT.h
		public const uint GENERIC_ALL = 0x10000000;
		public const uint GENERIC_EXECUTE = 0x20000000;
		public const uint GENERIC_READ = 0x80000000;
		public const uint GENERIC_WRITE = 0x40000000;

		public const uint DELETE = 0x00010000;
        
		public const uint SET_ACCESS = 2;

		// Inheritance Flags
		public const uint CONTAINER_INHERIT_ACE = 2;
		public const uint OBJECT_INHERIT_ACE = 1;

		// Error codes
		public const int ERROR_SUCCESS = 0;
		public const int ERROR_INSUFFICIENT_BUFFER = 122;
		public const int ERROR_NONE_MAPPED = 1332;

		private const uint MAXIMUM_ALLOWED = 0x02000000;
		private const uint TOKEN_QUERY = 0x0008;
		private const uint TOKEN_DUPLICATE = 0X0002;
		private const uint TOKEN_ADJUST_PRIVILEGES = 0x0020;
		private const uint SE_PRIVILEGE_ENABLED = 2;

		[DllImport("advapi32.dll", SetLastError=true)]
		private static extern bool OpenThreadToken(
			uint ThreadHandle, 
			uint DesiredAccess,
			bool OpenAsSelf,
			ref uint TokenHandle);

		
		[DllImport("advapi32.dll", SetLastError=true)]
		private static extern bool OpenThreadToken(
			uint ThreadHandle, 
			uint DesiredAccess,
			bool OpenAsSelf,
			ref IntPtr TokenHandle);

		[DllImport("advapi32.dll", SetLastError=true)]
		private static extern bool OpenProcessToken(
			uint ThreadHandle, 
			uint DesiredAccess,
			ref uint TokenHandle);

		[DllImport("advapi32.dll", SetLastError=true)]
		private static extern bool OpenProcessToken(
			uint ThreadHandle, 
			uint DesiredAccess,
			ref IntPtr TokenHandle);
        
		[DllImport("kernel32.dll", SetLastError=true)]
		public static extern bool CloseHandle(uint handle);

		[DllImport("kernel32.dll", SetLastError=true)]
		public static extern bool CloseHandle(IntPtr handle);

		[DllImport("kernel32.dll", SetLastError=true)]
		private static extern uint GetCurrentThread();

		[DllImport("kernel32.dll", SetLastError=true)]
		private static extern uint GetCurrentProcess();
        
		public static uint GetCurrentToken(uint accessMask)
		{
			uint hToken = 0;

			if (!OpenThreadToken(GetCurrentThread(), accessMask, true, ref hToken))
			{
				if (!OpenProcessToken(GetCurrentProcess(), accessMask, ref hToken))
				{
					throw new ApplicationException("OpenProcessToken failed with error code : " + Marshal.GetLastWin32Error().ToString());
				}
			}
			return hToken;
		}
        
		public const uint SYNCHRONIZE = 0x100000;
		public const uint READ_CONTROL = 0x20000;
		public const uint WRITE_DAC = 0x40000;
		public const uint WRITE_OWNER = 0x80000;

		public const uint STANDARD_RIGHTS_ALL = 0x1F0000;
		public const uint STANDARD_RIGHTS_EXECUTE = READ_CONTROL;
		public const uint STANDARD_RIGHTS_READ = READ_CONTROL;
		public const uint STANDARD_RIGHTS_REQUIRED = 0xF0000;
		public const uint STANDARD_RIGHTS_WRITE = READ_CONTROL;

		public const uint FILE_ADD_FILE = (0x2);                      
		public const uint FILE_ADD_SUBDIRECTORY = (0x4);              
		public const uint FILE_APPEND_DATA = (0x4);                   
		public const uint FILE_DELETE_CHILD = (0x40);                 
		public const uint FILE_LIST_DIRECTORY = (0x1);                
		public const uint FILE_READ_ATTRIBUTES = (0x80);              
		public const uint FILE_READ_DATA = (0x1);                     
		public const uint FILE_READ_EA = (0x8);                       
		public const uint FILE_READ_PROPERTIES = FILE_READ_EA;
		public const uint FILE_EXECUTE = (0x20);
		public const uint FILE_TRAVERSE = (0x20);
		public const uint FILE_WRITE_ATTRIBUTES = (0x100);
		public const uint FILE_WRITE_DATA = (0x2);
		public const uint FILE_WRITE_EA = (0x10);
		public const uint FILE_WRITE_PROPERTIES = FILE_WRITE_EA;

		public const uint FILE_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED | SYNCHRONIZE | 0x1FF);
		public const uint FILE_GENERIC_EXECUTE = (STANDARD_RIGHTS_EXECUTE | FILE_READ_ATTRIBUTES | FILE_EXECUTE | SYNCHRONIZE);
		public const uint FILE_GENERIC_READ = (STANDARD_RIGHTS_READ | FILE_READ_DATA | FILE_READ_ATTRIBUTES | FILE_READ_EA | SYNCHRONIZE);
		public const uint FILE_GENERIC_WRITE = (STANDARD_RIGHTS_WRITE | FILE_WRITE_DATA | FILE_WRITE_ATTRIBUTES | FILE_WRITE_EA | FILE_APPEND_DATA | SYNCHRONIZE);

		public const uint SecurityImpersonation = 2;

		[StructLayout(LayoutKind.Sequential)]
			public struct GENERIC_MAPPING
		{
			public uint GenericRead;
			public uint GenericWrite;
			public uint GenericExecute;
			public uint GenericAll;
		}
        
		[DllImport("advapi32.dll", SetLastError=true)]
		private static extern bool AccessCheck(
			uint pSecurityDescriptor,
			IntPtr ClientToken,
			uint DesiredAccess,
			ref GENERIC_MAPPING GenericMapping,
			IntPtr privilegeSet,
			ref uint privilegeSetLength, 
			ref uint GrantedAcces,
			ref uint accessStatus);
        
		[DllImport("advapi32.dll", SetLastError=true)]
		private static extern int DuplicateToken(
			IntPtr ExistingTokenHandle,
			uint ImpersonationLevel,
			ref IntPtr DuplicateTokenHandle);
        
		public static IntPtr GetCurrentImpersonationToken(uint accessMask)
		{
			IntPtr hToken = IntPtr.Zero;
			IntPtr hDupToken = IntPtr.Zero ;

			if (!OpenThreadToken(GetCurrentThread(), accessMask, true, ref hToken))
			{
				if (!OpenProcessToken(GetCurrentProcess(), accessMask, ref hToken))
				{
					throw new ApplicationException("OpenProcessToken failed with error code : " + Marshal.GetLastWin32Error().ToString());
				}
			}

			//if (DuplicateToken(hToken, (int)SecurityImpersonation, ref hDupToken)!= 0)
			if (DuplicateToken(hToken, 2, ref hDupToken)== 0)
			{
				throw new ApplicationException("DuplicateToken failed with error code : " + Marshal.GetLastWin32Error().ToString());
			}

			CloseHandle(hToken);
			return hDupToken;
		}
        
		[DllImport("Kernel32.dll",
			 CallingConvention=CallingConvention.Winapi,
			 SetLastError=true)]
		public static extern uint LocalFree(uint hMem);

		[DllImport("Advapi32.dll",
			 EntryPoint="BuildExplicitAccessWithNameA",
			 CallingConvention=CallingConvention.Winapi,
			 SetLastError=true, CharSet=CharSet.Ansi)]
		public static extern void BuildExplicitAccessWithName(
			ref EXPLICIT_ACCESS ea,
			IntPtr TrusteeName, 
			uint AccessPermissions,
			uint AccessMode,
			uint Inheritance);

		[DllImport("Advapi32.dll",
			 EntryPoint="SetEntriesInAclA",
			 CallingConvention=CallingConvention.Winapi,
			 SetLastError=true, CharSet=CharSet.Ansi)]
		public static extern uint SetEntriesInAcl(
			int CountofExplicitEntries,
			ref EXPLICIT_ACCESS ea,
			uint OldAcl,
			ref uint NewAcl);
        
		[DllImport("Advapi32.dll", CallingConvention=CallingConvention.Winapi, SetLastError=true, CharSet=CharSet.Ansi)]
		public static extern uint GetNamedSecurityInfo(
			[MarshalAs(UnmanagedType.LPStr)] string pObjectName,        
			SE_OBJECT_TYPE ObjectType,
			SECURITY_INFORMATION SecurityInfo,
			uint ppsidOwner, // We don't need owner
			uint ppsidGroup, // We don't need group
			ref uint ppDacl,
			uint ppSacl, // We don't need SACL
			ref uint ppSecurityDescriptor);
        
		[DllImport("Advapi32.dll", CallingConvention=CallingConvention.Winapi, SetLastError=true, CharSet=CharSet.Ansi)]
		public static extern uint SetNamedSecurityInfo(
			[MarshalAs(UnmanagedType.LPStr)] string pObjectName,        
			SE_OBJECT_TYPE ObjectType,
			SECURITY_INFORMATION SecurityInfo,
			IntPtr psidOwner,
			uint psidGroup,
			uint pDacl,
			uint pSacl);

		[DllImport("Advapi32.dll", CallingConvention=CallingConvention.Winapi, SetLastError=true, CharSet=CharSet.Ansi)]
		public static extern bool LookupAccountName(
			[MarshalAs(UnmanagedType.LPStr)] string lpSystemName, 
			[MarshalAs(UnmanagedType.LPStr)] string lpAccountName, 
			IntPtr Sid, 
			ref int cbSid, 
			IntPtr DomainName, 
			ref int cbDomainName, 
			ref uint peUse
			);

		[DllImport("Advapi32.dll", CallingConvention=CallingConvention.Winapi, SetLastError=true, CharSet=CharSet.Ansi)]
		public static extern bool LookupPrivilegeValue(
			[MarshalAs(UnmanagedType.LPStr)] string lpSystemName,
			[MarshalAs(UnmanagedType.LPStr)] string lpName,
			ref LUID Luid);
		[DllImport("Advapi32.dll", CallingConvention=CallingConvention.Winapi, SetLastError=true, CharSet=CharSet.Ansi)]
		public static extern bool AdjustTokenPrivileges(
			uint TokenHandle, 
			bool DisableAllPrivileges, 
			ref TOKEN_PRIVILEGES NewState, 
			int BufferLength, 
			uint PreviousState, 
			uint ReturnLength);
        
		public const uint FILE_SHARE_READ  = 0x00000001;  
		public const uint FILE_SHARE_WRITE = 0x00000002;  
		public const uint FILE_SHARE_DELETE = 0x00000004;
  
		public const int INVALID_HANDLE_VALUE = -1;

		public const uint CREATE_NEW = 1;
		public const uint CREATE_ALWAYS = 2;
		public const uint OPEN_EXISTING = 3;
		public const uint OPEN_ALWAYS = 4;
		public const uint TRUNCATE_EXISTING = 5;

		[DllImport("kernel32.dll", SetLastError=true)]
		public static extern int CreateFile(
			string lpFileName,
			uint dwDesiredAccess,
			uint dwShareMode,
			ref SECURITY_ATTRIBUTES lpSecurityAttributes,
			uint dwCreationDisposition,
			uint dwFlagsAndAttributes,
			uint hTemplateFile);
        
		[StructLayout(LayoutKind.Sequential)]
			public struct SECURITY_ATTRIBUTES
		{
			public static readonly int SizeOf = Marshal.SizeOf(typeof(SECURITY_ATTRIBUTES));
			public uint nLength;  
			public IntPtr lpSecurityDescriptor;  
			public bool bInheritHandle;
		}
        
		public const uint SECURITY_DESCRIPTOR_REVISION = 1;
		public const int SECURITY_DESCRIPTOR_MIN_LENGTH = 20;

		[DllImport("advapi32.dll", CallingConvention=CallingConvention.Winapi, SetLastError=true)]
		public static extern bool InitializeSecurityDescriptor(IntPtr pSecurityDescriptor, uint dwRevision);

		[DllImport("advapi32.dll", CallingConvention=CallingConvention.Winapi, SetLastError=true, CharSet=CharSet.Auto)]
		public static extern bool SetSecurityDescriptorDacl(
			IntPtr pSecurityDescriptor, 
			bool bDaclPresent, 
			uint pDacl, 
			bool bDaclDefaulted
			);
        
		#endregion

		#region Private class variables
		
		//private bool _bimpersonate;
		//private string _sImpersonateUser;
		//private string _sImpersonateDomain;
		//private string _sImpersonatePassword;

		#endregion
					
		#region Get File Property Methods

		/// <summary>
		/// Function will return a bitwise accessMask relating to the security permissions of the specific file
		/// </summary>
		/// <param name="FileOrFolderName">File or folder to get permissions.</param>
		/// <returns>uint</returns>
		/// 
		public uint GetGrantedAccess(string FileOrFolderName)
		{
			return GetGrantedAccess(FileOrFolderName, IntPtr.Zero);
		}


		public uint GetGrantedAccess(string FileOrFolderName, IntPtr hToken)
		{
			uint accessMask;

			accessMask = 0;

			uint pOldDacl = 0;
			uint pOldSD = 0;

			bool bReturn = false;
			uint accessStatus = 0;
			uint nBytes = 4096;
			IntPtr pBuffer = IntPtr.Zero;

			try
			{
				uint status;

				if (hToken == IntPtr.Zero)
					hToken = GetCurrentImpersonationToken(TOKEN_QUERY | TOKEN_DUPLICATE);

				status = GetNamedSecurityInfo(FileOrFolderName, SE_OBJECT_TYPE.SE_FILE_OBJECT,
					SECURITY_INFORMATION.DACL_SECURITY_INFORMATION|
					SECURITY_INFORMATION.OWNER_SECURITY_INFORMATION|
					SECURITY_INFORMATION.GROUP_SECURITY_INFORMATION,
					0,
					0,
					ref pOldDacl,
					0,
					ref pOldSD);
				if (status != ERROR_SUCCESS)
				{
					if (status == 5)
						return 0;
					else
						throw new ApplicationException("GetNamedSecurityInfo failed with error code : " + status.ToString());
				}
			
				GENERIC_MAPPING gMapping;

				gMapping.GenericRead = FILE_GENERIC_READ;
				gMapping.GenericWrite = FILE_GENERIC_WRITE;
				gMapping.GenericExecute = FILE_GENERIC_EXECUTE;
				gMapping.GenericAll = FILE_ALL_ACCESS;

				pBuffer = Marshal.AllocHGlobal((int)nBytes);
				bReturn = AccessCheck(pOldSD, hToken, MAXIMUM_ALLOWED, ref gMapping, pBuffer, ref nBytes, ref accessMask, ref accessStatus);
				if (bReturn == false)
				{
					throw new ApplicationException("AccessCheck failed with error code : " + Marshal.GetLastWin32Error().ToString());
				}
			}
			finally
			{
				if (pBuffer != IntPtr.Zero)
					Marshal.FreeHGlobal(pBuffer);
				if (pOldSD != 0)
					LocalFree(pOldSD);	
			}

			return accessMask;
		}
        
		
		/// <summary>
		/// Function will impersonate a specific user, and locate all the files he/she has access to in a particular directory.
		/// </summary>
		/// <param name="userName">User name to impersonate.</param>
		/// <param name="password">Password of the user to impersonate.</param>
		/// <param name="domainName">Domain the user we are impersonating is located in.</param>
		/// <param name="directoryToSearch">Directory to search files.</param>
		/// <param name="fileToSearchFor">File mask to search for.</param>
		/// <param name="Read">Look for read access.</param>
		/// <param name="Write">Look for write access.</param>
		/// <param name="Execute">Look for execute access.</param>
		/// <param name="All">Look for All access. Note: Setting this to true superceeds all other bool markers.</param>
		/// <returns>Arraylist containing each file matching the specified settings.</returns>
		/// <example>ArrayList ary = GetFileAccessList("passtest", "test,1234", "Compass-USA", "C:\\", "*.txt", true,false,false,false);</example>
		public ArrayList GetFilesUserCanRead(IntPtr hToken, string directoryToSearch, string fileToSearchFor)
		{
			ArrayList ary = new ArrayList();

			string[] files = System.IO.Directory.GetFiles(directoryToSearch, fileToSearchFor);

			for (int x=0;x<files.Length;x++)
			{
				uint accessMask = GetGrantedAccess(files[x], hToken);
				
				//bool bHasReadAccess = (( Compass.FileSystem.Permissions.FilePermissions.FILE_GENERIC_READ & accessMask) == Compass.FileSystem.Permissions.FilePermissions.FILE_GENERIC_READ);
				//bool bHasWriteAccess = (( Compass.FileSystem.Permissions.FilePermissions.FILE_GENERIC_WRITE & accessMask) == Compass.FileSystem.Permissions.FilePermissions.FILE_GENERIC_WRITE);			
				//bool bHasExecuteAccess = ((Compass.FileSystem.Permissions.FilePermissions.FILE_GENERIC_EXECUTE & accessMask) == Compass.FileSystem.Permissions.FilePermissions.FILE_GENERIC_EXECUTE);
				//bool bHasAllAccess = ((Compass.FileSystem.Permissions.FilePermissions.FILE_ALL_ACCESS & accessMask) == Compass.FileSystem.Permissions.FilePermissions.FILE_ALL_ACCESS);

				/*
				if ( (All) & (bHasAllAccess) )	// if all is true, and all is true on the file, nothing else to check
					ary.Add(files[x]);	// All Only
				else if ( (Read & !Write & !Execute) & (bHasReadAccess) )
					ary.Add(files[x]);	// Read only				
				else if ( (!Read & Write & !Execute) & (bHasWriteAccess) ) 
					ary.Add(files[x]);	// Write only				
				else if ( (!Read & !Write & Execute) & (bHasExecuteAccess) )
					ary.Add(files[x]);	// Execute Only						
				else if ( (Read & Write & !Execute) & (bHasReadAccess & bHasWriteAccess) )
					ary.Add(files[x]);	// Read and Write
				else if ( (Read & !Write & Execute) & (bHasReadAccess & bHasExecuteAccess) )
					ary.Add(files[x]);	// Read and Execute
				else if ( (!Read & Write & Execute) & (bHasWriteAccess & bHasExecuteAccess) )
					ary.Add(files[x]);	// Write and Execute
				else if ( (Read & Write & Execute) & (bHasReadAccess & bHasWriteAccess & bHasExecuteAccess) )
					ary.Add(files[x]);	// Read and Write and Execute
				*/

				if (( Compass.FileSystem.Permissions.FilePermissions.FILE_GENERIC_READ & accessMask) == Compass.FileSystem.Permissions.FilePermissions.FILE_GENERIC_READ)
					ary.Add(files[x]);
			}	

			return(ary);
		}

		#endregion

		#region Set File Permissions Methods

		public bool OverwritePermissionsOfFileOrFolder(string FileOrFolderName, string[] accountNames, uint[] accessMasks, string ownerName)
		{
			int x, i, sidSize, domainNameSize, nCount = 0;
			bool bReturn;

			bReturn = false;

			x = accountNames.Length;
			EXPLICIT_ACCESS[] ea = new EXPLICIT_ACCESS[x];
			IntPtr[] accountNamePtrs = new IntPtr[x];
			IntPtr sidPtr = IntPtr.Zero;
			IntPtr domainNamePtr = IntPtr.Zero;
			uint pDacl = 0;
			uint hToken = 0;

			try
			{
				bool bResult;
				uint status;
				uint eUse;
				LUID luid = new LUID();
				TOKEN_PRIVILEGES tp = new TOKEN_PRIVILEGES();

				sidSize = 0;
				domainNameSize = 0;
				eUse = 0;

				bResult = LookupAccountName(null, ownerName,
					sidPtr, ref sidSize,
					domainNamePtr, ref domainNameSize, ref eUse);
				if (bResult == false &&
					Marshal.GetLastWin32Error() != ERROR_INSUFFICIENT_BUFFER)
					throw new ApplicationException("LookupAccountName failed with error code : " + Marshal.GetLastWin32Error().ToString());

				sidPtr = Marshal.AllocHGlobal(sidSize);
				if (sidPtr == IntPtr.Zero)
					throw new ApplicationException("AllocHGlobal failed");
				domainNamePtr = Marshal.AllocHGlobal(domainNameSize);
				if (domainNamePtr == IntPtr.Zero)
					throw new ApplicationException("AllocHGlobal failed");

				bResult = LookupAccountName(null, ownerName,
					sidPtr, ref sidSize,
					domainNamePtr, ref domainNameSize, ref eUse);
				if (bResult == false)
					throw new ApplicationException("LookupAccountName failed with error code : " + Marshal.GetLastWin32Error().ToString());

				for (i = 0; i < x; i++)
				{
					if (accountNames[i] != "")
					{
						sidSize = 0;
						domainNameSize = 0;
						eUse = 0;

						bResult = LookupAccountName(null, accountNames[i].ToString(),
							sidPtr, ref sidSize,
							domainNamePtr, ref domainNameSize, ref eUse);

						if (bResult == false && Marshal.GetLastWin32Error() != ERROR_INSUFFICIENT_BUFFER)
						{
							// Do nothing, user not found
						}
						else
						{

							accountNamePtrs[i] = Marshal.StringToHGlobalAnsi(accountNames[i]);
							if (accountNamePtrs[i] == IntPtr.Zero)
								throw new ApplicationException("StringToHGlobalAnsi failed");							

							BuildExplicitAccessWithName(ref ea[i],
								accountNamePtrs[i],
								accessMasks[i],
								SET_ACCESS,
								CONTAINER_INHERIT_ACE | OBJECT_INHERIT_ACE);

							nCount++;
						}
					}
				}
                
				// Transfer the Access to another listing
				EXPLICIT_ACCESS[] ea1 = new EXPLICIT_ACCESS[nCount];
				int eaCount = 0;
				for (int tt=0;tt<accountNames.Length;tt++)
				{
					if ( !(ea[tt].grfAccessPermissions == 0) && !(ea[tt].pTRUSTEE.ptstrName == 0) )
					{
						ea1[eaCount] = ea[tt];
						eaCount++;
					}					
				}

				if (ea1.Length == 0)
					return true;

				status = SetEntriesInAcl(nCount, ref ea1[0], 0, ref pDacl);
				if (status != ERROR_SUCCESS)
					throw new ApplicationException("SetEntriesInAcl failed with error code : " + status.ToString());

				hToken = GetCurrentToken(TOKEN_QUERY | TOKEN_ADJUST_PRIVILEGES);

				bResult = LookupPrivilegeValue(null, SE_RESTORE_NAME, ref luid);
				if (bResult == false)
					throw new ApplicationException("LookupPrivilegeValue failed with error code : " + Marshal.GetLastWin32Error().ToString());

				tp.PrivilegeCount = 1;
				tp.privileges.pLuid = luid;
				tp.privileges.Attributes = SE_PRIVILEGE_ENABLED;

				bResult = AdjustTokenPrivileges(hToken, false, ref tp, Marshal.SizeOf(tp), 0, 0);
				if (bResult == false)
					throw new ApplicationException("AdjustTokenPrivileges failed with error code : " + Marshal.GetLastWin32Error().ToString());
                
				status = SetNamedSecurityInfo(FileOrFolderName, SE_OBJECT_TYPE.SE_FILE_OBJECT,
					SECURITY_INFORMATION.DACL_SECURITY_INFORMATION |
					SECURITY_INFORMATION.OWNER_SECURITY_INFORMATION |
					SECURITY_INFORMATION.PROTECTED_DACL_SECURITY_INFORMATION,
					sidPtr,
					0,
					pDacl,
					0);
				if (status != ERROR_SUCCESS)
					throw new ApplicationException("SetNamedSecurityInfo failed with error code : " + status.ToString());

				bReturn = true;
			}
			finally
			{
				for (i = 0; i < x; i++)
				{
					if (accountNamePtrs[i] != IntPtr.Zero)
					{
						Marshal.FreeHGlobal(accountNamePtrs[i]);
					}
				}
				if (sidPtr != IntPtr.Zero)
					Marshal.FreeHGlobal(sidPtr);
				if (domainNamePtr != IntPtr.Zero)
					Marshal.FreeHGlobal(domainNamePtr);
				if (pDacl != 0)
					LocalFree(pDacl);
				if (hToken != 0)
					CloseHandle(hToken);
			}

			return bReturn;
		}

		public bool MergePermissionsToFileOrFolder(string FileOrFolderName, string[] accountNames, uint[] accessMasks)
		{
			int x, i, sidSize, domainNameSize, nCount = 0;
			bool bReturn;

			bool bResult;
			uint eUse;
			IntPtr sidPtr = IntPtr.Zero;
			IntPtr domainNamePtr = IntPtr.Zero;

			bReturn = false;

			x = accountNames.Length;
			EXPLICIT_ACCESS[] ea = new EXPLICIT_ACCESS[x];
			IntPtr[] accountNamePtrs = new IntPtr[x];
			uint pDacl = 0;
			uint pOldDacl = 0;
			uint pOldSD = 0;

			try
			{
				uint status;

				status = GetNamedSecurityInfo(FileOrFolderName, SE_OBJECT_TYPE.SE_FILE_OBJECT,
					SECURITY_INFORMATION.DACL_SECURITY_INFORMATION,
					0,
					0,
					ref pOldDacl,
					0,
					ref pOldSD);
				if (status != ERROR_SUCCESS)
					throw new ApplicationException("GetNamedSecurityInfo failed with error code : " + status.ToString());

				for (i = 0; i < x; i++)
				{
					if (accountNames[i] != "")
					{
						sidSize = 0;
						domainNameSize = 0;
						eUse = 0;

						bResult = LookupAccountName(null, accountNames[i].ToString(),
							sidPtr, ref sidSize,
							domainNamePtr, ref domainNameSize, ref eUse);

						if (bResult == false && Marshal.GetLastWin32Error() != ERROR_INSUFFICIENT_BUFFER)
						{
							// Do nothing, user not found
						}
						else
						{

							accountNamePtrs[i] = Marshal.StringToHGlobalAnsi(accountNames[i]);
							if (accountNamePtrs[i] == IntPtr.Zero)
								throw new ApplicationException("StringToHGlobalAnsi failed");							

							BuildExplicitAccessWithName(ref ea[i],
								accountNamePtrs[i],
								accessMasks[i],
								SET_ACCESS,
								CONTAINER_INHERIT_ACE | OBJECT_INHERIT_ACE);

							nCount++;
						}
					}
				}
                
				// Transfer the Access to another listing
				EXPLICIT_ACCESS[] ea1 = new EXPLICIT_ACCESS[nCount];
				int eaCount = 0;
				for (int tt=0;tt<accountNames.Length;tt++)
				{
					if ( !(ea[tt].grfAccessPermissions == 0) && !(ea[tt].pTRUSTEE.ptstrName == 0) )
					{
						ea1[eaCount] = ea[tt];
						eaCount++;
					}					
				}

				if (ea1.Length == 0)
					return true;

				status = SetEntriesInAcl(nCount, ref ea1[0], pOldDacl, ref pDacl);
				if (status != ERROR_SUCCESS)
					throw new ApplicationException("SetEntriesInAcl failed with error code : " + status.ToString());

				status = SetNamedSecurityInfo(FileOrFolderName, SE_OBJECT_TYPE.SE_FILE_OBJECT,
					SECURITY_INFORMATION.DACL_SECURITY_INFORMATION |
					SECURITY_INFORMATION.PROTECTED_DACL_SECURITY_INFORMATION,
					IntPtr.Zero,
					0,
					pDacl,
					0);
				if (status != ERROR_SUCCESS)
					throw new ApplicationException("SetNamedSecurityInfo failed with error code : " + status.ToString());

				bReturn = true;
			}
			finally
			{
				if (pDacl != 0)
					LocalFree(pDacl);
				for (i = 0; i < x; i++)
				{
					if (accountNamePtrs[i] != IntPtr.Zero)
					{
						Marshal.FreeHGlobal(accountNamePtrs[i]);
					}
				}
				if (pOldSD != 0)
					LocalFree(pOldSD);
			}

			return bReturn;
		}
        
		public bool CreateFileWithPerm(string FileName, string[] accountNames, uint[] accessMasks)
		{
			int x;
			int i;
			bool bReturn;
			int hFileHandle = INVALID_HANDLE_VALUE;
			IntPtr pNewSD;

			bReturn = false;

			x = accountNames.Length;
			EXPLICIT_ACCESS[] ea = new EXPLICIT_ACCESS[x];
			IntPtr[] accountNamePtrs = new IntPtr[x];
			uint pDacl = 0;

			pNewSD = IntPtr.Zero;

			try
			{
				uint status;

				for (i = 0; i < x; i++)
				{
					accountNamePtrs[i] = Marshal.StringToHGlobalAnsi(accountNames[i]);
					if (accountNamePtrs[i] == IntPtr.Zero)
						throw new ApplicationException("StringToHGlobalAnsi failed");
                    
					BuildExplicitAccessWithName(ref ea[i],
						accountNamePtrs[i],
						accessMasks[i],
						SET_ACCESS,
						CONTAINER_INHERIT_ACE | OBJECT_INHERIT_ACE);
				}
                
				status = SetEntriesInAcl(x, ref ea[0], 0, ref pDacl);
				if (status != ERROR_SUCCESS)
					throw new ApplicationException("SetEntriesInAcl failed with error code : " + status.ToString());

				pNewSD = Marshal.AllocHGlobal(SECURITY_DESCRIPTOR_MIN_LENGTH);
				if (pNewSD == IntPtr.Zero)
					throw new ApplicationException("AllocHGlobal failed");

				if (InitializeSecurityDescriptor(pNewSD, SECURITY_DESCRIPTOR_REVISION) == false)
					throw new ApplicationException("InitializeSecurityDescriptor failed with error code : " + Marshal.GetLastWin32Error().ToString());

				if (SetSecurityDescriptorDacl(pNewSD, true, pDacl, false) == false)
					throw new ApplicationException("SetSecurityDescriptorDacl failed with error code : " + Marshal.GetLastWin32Error().ToString());
                
				SECURITY_ATTRIBUTES secAttr;

				secAttr.nLength = (uint)Marshal.SizeOf(typeof(SECURITY_ATTRIBUTES));
				secAttr.lpSecurityDescriptor = pNewSD;
				secAttr.bInheritHandle = false;

				hFileHandle = CreateFile(FileName,
					GENERIC_READ|GENERIC_WRITE,
					0, ref secAttr, OPEN_ALWAYS, 0, 0);

				if (hFileHandle == INVALID_HANDLE_VALUE)
					throw new ApplicationException("CreateFile failed with error code : " + Marshal.GetLastWin32Error().ToString());

				bReturn = true;
			}
			finally
			{
				if (hFileHandle != INVALID_HANDLE_VALUE)
					CloseHandle((uint)hFileHandle);

				if (pNewSD != IntPtr.Zero)
					Marshal.FreeHGlobal(pNewSD);

				if (pDacl != 0)
					LocalFree(pDacl);
            
				for (i = 0; i < x; i++)
				{
					if (accountNamePtrs[i] != IntPtr.Zero)
					{
						Marshal.FreeHGlobal(accountNamePtrs[i]);
					}
				}
			}

			return bReturn;
		}


		#endregion
	}
}
