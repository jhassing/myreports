using System;
using System.IO;

// Report Distribution Specific References
//using ReportDistribution.Common;

namespace Compass.FileSystem.ExtendedFileMonitor
{
	/// <summary>
	/// Summary description for ExtendedFileMonitor.
	/// </summary>
	/// 
	public delegate void FileMonitorEventHandler(string sPath);

	public class ExtendedFileMonitorClass : BaseFileMonitor
	{

		public event FileMonitorEventHandler FileFound;	

		private string m_sFTPDirectory = "";


		public ExtendedFileMonitorClass( string sFTPDirectory )
		{

			m_sFTPDirectory = sFTPDirectory;

            base.fswFileMonitor.InternalBufferSize = 131072;

			base.fswFileMonitor.Changed +=new System.IO.FileSystemEventHandler(fswFileMonitor_Changed);
			base.fswFileMonitor.Created += new System.IO.FileSystemEventHandler(fswFileMonitor_Created);
			base.fswFileMonitor.Renamed+=new RenamedEventHandler(fswFileMonitor_Renamed);

			base.fswFileMonitor.Filter = "*";
			base.fswFileMonitor.Path = m_sFTPDirectory;

		}

		public void StartDiretoryMonitoring()
		{

			// Look for any files right off the start
			// This was a limitation of the original File System Watcher
			// I extened this class for this funtinality

			string[] dirs = Directory.GetFiles( m_sFTPDirectory );
			
			foreach (string dir in dirs) 
			{
				// Ahh, found the file I was looking for
				// Raise the event to the caller.
				OnFileFound(dir);				
				
				// Pause for a second
				Pause();
			}			
			
			base.StartDirectoryMonitoring();
		}

		public new void StopDirectoryMonitoring()
		{
			base.StopDirectoryMonitoring();
		}

		private void fswFileMonitor_Changed(object sender, System.IO.FileSystemEventArgs e)
		{
			// Throw an event, the file is here.
			OnFileFound(e.FullPath);

			// Pause for a second
			Pause();

		}

		private void fswFileMonitor_Created(object sender, System.IO.FileSystemEventArgs e)
		{
			// Throw an event, the file is here.
			OnFileFound( e.FullPath );

			// Pause for a second
			Pause();			
		}

		protected virtual void OnFileFound(string sPath)
		{
			if (FileFound != null) 
			{
				Pause();
				// Invokes the delegates. 
                Console.WriteLine("File Found: " + sPath);
				FileFound(sPath);
			}
		}

		private void fswFileMonitor_Renamed(object sender, RenamedEventArgs e)
		{
			// Throw an event, the file is here.
			OnFileFound( e.FullPath );

			// Pause for a second
			Pause();	
		}

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.fswFileMonitor)).BeginInit();
            // 
            // fswFileMonitor
            // 
            this.fswFileMonitor.Created += new System.IO.FileSystemEventHandler(this.fswFileMonitor_Created_1);
            this.fswFileMonitor.Changed += new System.IO.FileSystemEventHandler(this.fswFileMonitor_Changed_1);
            ((System.ComponentModel.ISupportInitialize)(this.fswFileMonitor)).EndInit();

        }

        private void fswFileMonitor_Changed_1(object sender, FileSystemEventArgs e)
        {

        }

        private void fswFileMonitor_Created_1(object sender, FileSystemEventArgs e)
        {

        }
	}	// Class
}	// Namespace
