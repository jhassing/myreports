using System;

/// <summary>
/// Structure holds the specified Users password settings on the domain
/// </summary>
public struct PasswordSettings
{
	private bool		m_bDoesPasswordExpire;
	private bool		m_bHasPasswordBeenSet;
	private bool		m_bPasswordExpired;
	private DateTime	m_datDatePasswordLastSet;
	private int			m_nDaysTillPasswordExpires;
	private int			m_nTimeSincePasswordLastSet;	

	/// <summary>
	/// Password settings constructor.
	/// </summary>
	/// <param name="bDoesPasswordExpire"></param>
	/// <param name="bHasPasswordBeenSet"></param>
	/// <param name="bPasswordExpired"></param>
	/// <param name="datDatePasswordLastSet"></param>
	/// <param name="nTimeSincePasswordLastSet"></param>
	/// <param name="nDaysTillPasswordExpires"></param>
	public PasswordSettings(bool bDoesPasswordExpire, bool bHasPasswordBeenSet, 
		bool bPasswordExpired, DateTime datDatePasswordLastSet, 
		int nTimeSincePasswordLastSet, int nDaysTillPasswordExpires)
	{
		this.m_bDoesPasswordExpire = bDoesPasswordExpire;
		this.m_bHasPasswordBeenSet = bHasPasswordBeenSet;
		this.m_bPasswordExpired = bPasswordExpired;
		this.m_datDatePasswordLastSet = datDatePasswordLastSet;
		this.m_nDaysTillPasswordExpires = nTimeSincePasswordLastSet;
		this.m_nTimeSincePasswordLastSet = nDaysTillPasswordExpires;
	}

	/// <summary>
	/// Does the specified users password ever expire?
	/// </summary>
	public bool DoesPasswordExpire
	{
		get {return(this.m_bDoesPasswordExpire);}
	}


	/// <summary>
	/// Has the specified users password ever been set?
	/// </summary>
	public bool HasPasswordBeenSet
	{
		get {return(this.m_bHasPasswordBeenSet);}
	}


	/// <summary>
	/// Has the specified users password ever expired?
	/// </summary>
	public bool PasswordExpired
	{
		get {return(this.m_bPasswordExpired);}
	}


	/// <summary>
	/// The date the specified user last set his/her passsword
	/// </summary>
	public DateTime PasswordLastSet
	{
		get {return(this.m_datDatePasswordLastSet);}
	}

	/// <summary>
	/// The date the specified users password will expire.
	/// </summary>
	public DateTime DatePasswordExpires
	{
		get 
		{
			return(DateTime.Now.AddDays(this.m_nDaysTillPasswordExpires));
		}
	}


	/// <summary>
	/// Number of days till the specified users password expires
	/// </summary>
	public int DaysTillPasswordExpires
	{
		get {return(this.m_nDaysTillPasswordExpires);}
	}


	/// <summary>
	/// Number of days since the specified user last set his/her password
	/// </summary>
	public int TimeSincePasswordLastSet
	{
		get {return(this.m_nTimeSincePasswordLastSet);}
	}
	

}