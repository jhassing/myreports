using System;
using System.DirectoryServices.ActiveDirectory;


using Novell.Directory.Ldap;

namespace Compass.Security.ActiveDirectory
{
    /// <summary>
    /// Summary description for Connection.
    /// </summary>
    public class LDAPConnection : LdapConnection
    {

        #region Private member variables

        // Port the domain is on
        private int _ldapPort = 389;

        // Domain user service account info
        private string _domainUser = "";
        private string _domainPassword = "";

        // Domain and base OU info
        private string _ldapHost = "";
        private string _domainController = "";

        #endregion

        #region Properties


        /// <summary>
        /// Returns the IP address of the domain controller are are connected into.5
        /// </summary>
        public string DomainController
        {
            get
            {
                return this._domainController;
            }
        }

        /// <summary>
        /// Returns the Ldap server we are connecting to
        /// </summary>
        public string ldapHost
        {
            get
            {
                return this._ldapHost;
            }
        }

        /// <summary>
        /// Returns the Ldap port we are connecting to
        /// </summary>
        public int ldapPort
        {
            get
            {
                return this._ldapPort;
            }
        }

        /// <summary>
        /// Returns the Service account domain user
        /// </summary>
        public string DomainUser
        {
            get
            {
                return this._domainUser;
            }
        }

        /// <summary>
        /// Password of service account
        /// </summary>
        public string DomainPassword
        {
            get
            {
                return this._domainPassword;
            }
        }

        #endregion

        #region ~ctor

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="DomainUser">Domain user with read-write domain access.</param>
        /// <param name="DomainPassword">Password for the domain user.</param>
        /// <param name="ldapHost">Name of the domain to connecto to.</param>
        /// <param name="ldapPort">Port the LDAP server is on.</param>
        public LDAPConnection(string DomainUser, string DomainPassword, string ldapHost, int ldapPort)
        {
            ConnectToDomain(DomainUser, DomainPassword, ldapHost, ldapPort);
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="DomainUser">Domain user with read-write domain access.</param>
        /// <param name="DomainPassword">Password for the domain user.</param>
        /// <param name="ldapHost">Name of the domain to connecto to.</param>
        public LDAPConnection(string DomainUser, string DomainPassword, string ldapHost)
        {
            ConnectToDomain(DomainUser, DomainPassword, ldapHost, 389);
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="DomainUser">Domain user with read-write domain access.</param>
        /// <param name="DomainPassword">Password for the domain user.</param>
        /// <param name="ldapServer">The Domain controller to connect to.</param>
        /// <param name="ldapHost">Name of the domain to connecto to.</param>
        public LDAPConnection(string DomainUser, string DomainPassword, string ldapServer, string ldapHost)
        {
            ConnectToDomain(DomainUser, DomainPassword, ldapServer, ldapHost, 389);
        }

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="DomainUser">Domain user with read-write domain access.</param>
        /// <param name="DomainPassword">Password for the domain user.</param>
        /// <param name="ldapServer">The Domain controller to connect to.</param>
        /// <param name="ldapHost">Name of the domain to connecto to.</param>
        /// <param name="ldapPort">Port the LDAP server is on.</param>
        public LDAPConnection(string DomainUser, string DomainPassword, string ldapServer, string ldapHost, int ldapPort)
        {
            ConnectToDomain(DomainUser, DomainPassword, ldapServer, ldapHost, ldapPort);
        }

        #endregion

        #region Connect To Domain

        private void ConnectToDomain(string DomainUser, string DomainPassword, string ldapHost, int ldapPort)
        {
            try
            {
                this._ldapHost = ldapHost;
                this._ldapPort = ldapPort;
                this._domainUser = DomainUser;
                this._domainPassword = DomainPassword;

                ConnectToDirectoryEntry();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ConnectToDomain(string DomainUser, string DomainPassword, string ldapServer, string ldapHost, int ldapPort)
        {
            try
            {
                this._ldapHost = ldapHost;
                this._ldapPort = ldapPort;
                this._domainUser = DomainUser;
                this._domainPassword = DomainPassword;
                this._domainController = ldapServer;

                int ldapVersion = LdapConnection.Ldap_V3;

                base.Connect(this._domainController + "." + this._ldapHost, this._ldapPort);

                base.Bind(ldapVersion, this._domainUser, this._domainPassword);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Connect To DirectoryEntry

        /// <summary>
        /// Gets an LDAP conection object and return.
        /// </summary>
        /// <returns>Returns an LDAP connection to an object.</returns>
        protected void ConnectToDirectoryEntry()
        {

            try
            {
                getDomainControllerToConnectTo();

                int ldapVersion = LdapConnection.Ldap_V3;

                base.Connect(this._domainController, this._ldapPort);

                base.Bind(ldapVersion, this._domainUser, this._domainPassword);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        private void getDomainControllerToConnectTo()
        {
            DirectoryContext dc = null;
            Domain d = null;
            //DomainControllerCollection dcc = null;

            try
            {
                dc = new DirectoryContext(DirectoryContextType.Domain, ldapHost, DomainUser, DomainPassword);

                d = Domain.GetDomain(dc);
                //dcc = d.FindAllDomainControllers("NA-NC-CH");

                //this._domainController = dcc[0].Name;

                this._domainController = d.FindDomainController("NA-NC-CH", LocatorOptions.ForceRediscovery | LocatorOptions.AvoidSelf).Name;
            }
            catch (Exception ex)
            {
                throw new Exception("Could not find a domain controller to connect to.", ex);
            }
            finally
            {
                dc = null;
                //dcc = null;

                if (d != null)
                    d.Dispose();

                d = null;
            }
        }

        public static string GetDomainControllerToConnectTo
        {
            get
            {
                DirectoryContext dc = null;
                Domain d = null;
                //DomainControllerCollection dcc = null;

                try
                {
                    dc = new DirectoryContext(DirectoryContextType.Domain, "na.compassGroup.corp", "acadmin", "gt987er4");

                    d = Domain.GetDomain(dc);
                    //dcc = d.FindAllDomainControllers("NA-NC-CH");

                    //return dcc[0].Name;
                    return d.FindDomainController("NA-NC-CH", LocatorOptions.ForceRediscovery | LocatorOptions.AvoidSelf).Name;
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not find a domain controller to connect to.", ex);
                }
                finally
                {
                    dc = null;
                    //dcc = null;

                    if (d != null)
                        d.Dispose();

                    d = null;
                }
            }
        }

        #region Disconnect

        public override void Disconnect()
        {
            base.Disconnect();
        }

        #endregion
    }
}
