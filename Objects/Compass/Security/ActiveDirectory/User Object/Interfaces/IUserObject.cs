using System.Collections;
using Novell.Directory.Ldap;



namespace Compass.Security.ActiveDirectory
{
    /// <summary>
    /// Summary description for IUserObject.
    /// </summary>
    public interface IUserObject : IUserPassword
    {
        bool DoesUserExist(string distinguishedName, string searchBase);

        bool DoesUserPasswordNeverExpires(string distinguishedName);
        bool DoesUserPasswordNeverExpires(int userAccountControl);
        void SetUserPwdNeverExpire(string distinguishedName, bool setPwdNeverExpire);

        bool UserPasswordExpired(string distinguishedName);
        void SetUserPasswordExpired(string distinguishedName, bool setPwdNeverExpire);

        bool CanUserChangePwd(string distinguishedName);
        void SetUserCanChangePassword(string distinguishedName, bool setUserCanChPwd);

        bool GetUserCannotChangePassword(string distinguishedName);
        bool SetUserCannotChangePassword(string distinguishedName);

        bool IsAccountActive(string distinguishedName);
        bool IsAccountActive(int userAccountControl);
        void SetUserAccountDisabled(string distinguishedName, bool bEnable);

        int FindObjectsUserAccountControl(string distinguishedName);

        string AddUser(string samAccountName,
                                    string password,
                                    Hashtable ht,
                                    string searchBase,
                                    string sOU_UserContainer,
                                    bool enableUser,
                                    bool forceUserResetPWDnextLogin,
                                    bool setPasswordNeverExpire,
                                    bool editUserIfExists);

        void DeleteUser(string samAccountName);

        LdapAttributeSet GetUserProperties(string distinguishedName, ArrayList aryProps);
        LdapAttribute GetUserProperties(string distinguishedName, string propertyName);

        ArrayList GetUsersMembership(string userDN, ArrayList parms);
        ArrayList GetUsersMembership(string userDN, bool goDown2Levels);

        void AddUserToGroup(string userDistinguishedName, ArrayList groups);
        void AddUserToGroup(string userDistinguishedName, string groupDN);

        void RemoveUserFromGroup(string userDistinguishedName, ArrayList groups);
        void RemoveUserFromGroup(string userDistinguishedName, string groupDN);

        bool SetUserProperties(string distinguishedName, Hashtable htProperties);
        bool SetUserProperties(string distinguishedName, string property, string newValue);

        ArrayList FindUsers(Hashtable PropertiesToSearch, ArrayList parms, string objectClassAttributeName, string[] objectClasses, string ldapSearchBase, int searhScope, int maxResults);

        LoginResult VerifyUserLogin(string username, string pwd, string searchBase, ref string distinguishedName);

        void SetNewPassword(string distinguishedName, string sNewPassword);
        string GetObjectsDistinguishedName(string samAccountName, string searchBase);

    }
}
