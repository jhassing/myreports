using System;
using System.Collections;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using ActiveDs;
using Novell.Directory.Ldap;





namespace Compass.Security.ActiveDirectory
{
    /// <summary>
    /// Summary description for ADUserObject.
    /// </summary>
    public sealed class ADUserObject : BaseUserObject
    {
        #region ~const

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="DomainUser">Domain user with read-write domain access.</param>
        /// <param name="DomainPassword">Password for the domain user.</param>
        /// <param name="ldapHost">Name of the domain to connecto to.</param>
        /// <param name="ldapPort">Port the LDAP server is on.</param>
        public ADUserObject(LDAPConnection ldpConnection)
            : base(ldpConnection)
        {

        }


        #endregion

        #region Verify User	Login
        /// <summary>
        /// Verify the user is a member of the domain, and the credentials are valid.
        /// </summary>
        /// <param name="username">"acadmin"</param>
        /// <param name="pwd">"test,1234"</param>
        /// <returns>LoginResult.</returns>
        /// <example>VerifyUser("acadmin", "test,1234");</example>
        public override LoginResult VerifyUserLogin(string username, string pwd, string searchBase, ref string distinguishedName)
        {
            // Please leave this method alone.. the logic MUST be followed.. TRUST ME.


            LdapEntry ldapEntry = null;
            DirectoryEntry deUser = null;

            try
            {
                // First, bind to user, even if the user does not exist.
                ldapEntry = GetLdapEntry_UsingSamAccountName(username, searchBase);
                distinguishedName = ldapEntry.DN;

            }
            catch (Exception ex)
            {
                return LoginResult.LOGIN_USER_DOESNT_EXIST;
            }

            // Check the cradentials
            LdapConnection ldapConn = null;

            try
            {
                ldapConn = new LdapConnection();

                ldapConn.Connect(GetDomainControllerToConnectTo, this._conn.ldapPort);

                ldapConn.Bind(LdapConnection.Ldap_V3, username + "@" + this._conn.ldapHost, pwd);

            }
            catch
            {
                return (LoginResult.LOGIN_USER_PASSSWORD_INCORRECT);
            }
            finally
            {
                if (ldapConn != null)
                    ldapConn.Disconnect();

                ldapConn = null;
            }

            try
            {

                if (ldapEntry == null)		// User was not even found
                    return LoginResult.LOGIN_USER_DOESNT_EXIST;

                if (!IsAccountActive(System.Convert.ToInt32(ldapEntry.getAttribute("userAccountControl").StringValue)))	// User exists, but the account is inactive.
                    return LoginResult.LOGIN_USER_ACCOUNT_INACTIVE;

                // Get the users password settings
                deUser = ConnectToDirectoryEntry(distinguishedName);
                PasswordSettings ww = GetUserPasswordSettings(deUser);

                if ((ww.PasswordExpired) && (ww.DoesPasswordExpire))
                    return LoginResult.LOGIN_USER_PASSWORD_EXPIRED;

                return LoginResult.LOGIN_OK;

            }
            catch (Exception ex)
            {
                return (LoginResult.LOGIN_USER_DOESNT_EXIST);
            }
            finally
            {
                ldapEntry = null;

                if (deUser != null)
                    deUser.Dispose();

                deUser = null;

            }

        }
        #endregion

        #region Set New User Password
        /// <summary>
        /// Allow a forced password reset to the user, regardless of the existing password.
        /// </summary>
        /// <param name="sContainerName">Domain user.</param>
        /// <param name="sNewPassword">New pasword</param>
        /// <returns>True if successful, false otherwise</returns>
        /// <example>SetNewPassword("acadmin", "test,1234");</example>
        public override void SetNewPassword(string distinguishedName, string sNewPassword)
        {
            DirectoryEntry deCont = null;

            deCont = ConnectToDirectoryEntry(distinguishedName);

            try
            {
                SetNewPassword(deCont, sNewPassword);
            }
            catch
            {
                // try again
                try
                {
                    SetNewPassword(deCont, sNewPassword);
                }
                catch (Exception ex1)
                {
                    throw (ex1);
                }
            }
            finally
            {

                if (deCont != null)
                    deCont.Dispose();

                deCont = null;
            }
        }


        public override void SetNewPassword(string domainController, string distinguishedName, string sNewPassword)
        {
            DirectoryEntry deCont = null;

            try
            {
                string ldapPath = "LDAP://" + domainController + "/" + distinguishedName;

                deCont = new DirectoryEntry(ldapPath, this._conn.DomainUser, this._conn.DomainPassword, System.DirectoryServices.AuthenticationTypes.Secure);

                SetNewPassword(deCont, sNewPassword);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

                if (deCont != null)
                    deCont.Dispose();

                deCont = null;
            }
        }


        private void SetNewPassword(DirectoryEntry usr, string sNewPassword)
        {
            try
            {
                usr.Invoke("SetPassword", new object[] { sNewPassword });
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                // DO not clean up objects passed in.. the calling method MUST clean them up for us.
            }
        }


        #endregion

        #region Change Users Password
        /// <summary>
        /// Allows a user to change their domain password.
        /// </summary>
        /// <param name="sContainerName">Domain User.</param>
        /// <param name="sOldPassword">Existing user</param>
        /// <param name="sNewPassword">New password</param>
        /// <param name="sConfirmPassword">New password to verify against</param>
        /// <returns>True if successful, false otherwise</returns>
        /// <example>ChangePassword("cn=smith\, john,ou=users,dc=company,dc=corp", "test,1234", "newp,1234", "newp,1234");</example>
        public bool ChangePassword(string userDN, string oldPassword, string newPassword, string confirmPassword)
        {
            // Make sure the passwords are a match. This SHOULD be done on the client side,
            // however, we will do a double check just to make sure.
            // If they fail, just reutrn "false"
            if (newPassword != confirmPassword)
                throw (new Exception("The passwords do not match"));

            DirectoryEntry deChild = null;

            try
            {
                // Try and connect to the directory entry
                deChild = ConnectToDirectoryEntry(userDN);

                if (deChild != null)
                {
                    deChild.Invoke("ChangePassword", new object[] { oldPassword, newPassword });

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                if (deChild != null)
                    deChild.Dispose();

                deChild = null;
            }

        }

        #endregion

        #region Can User Change His/Her password?

        /// <summary>
        /// Returns "can the user change his/her password"
        /// </summary>
        /// <param name="userNetworkID">Account of the user</param>
        /// <returns></returns>
        public override bool CanUserChangePwd(string userDN)
        {
            DirectoryEntry de = null;
            int cantChange = 0;

            try
            {

                de = ConnectToDirectoryEntry(userDN);

                SecurityDescriptor mySecurityDescriptor = (SecurityDescriptor)de.Properties["ntSecurityDescriptor"].Value;

                AccessControlList myACL = (AccessControlList)mySecurityDescriptor.DiscretionaryAcl;

                foreach (AccessControlEntry ACE in (IEnumerable)myACL)
                {
                    if (ACE.ObjectType == CHANGE_PASSWORD_GUID)
                    {
                        if ((ACE.Trustee == @"NT AUTHORITY\SELF") || (ACE.Trustee == "EVERYONE"))
                        {
                            if (ACE.AceType == ADS_ACETYPE_ACCESS_DENIED_OBJECT)
                                cantChange++;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                de.Dispose();
                de = null;
            }

            return cantChange >= 1;
        }


        #endregion

        #region Set User can change Password

        /// <summary>
        /// Sets the user can change his/her password
        /// </summary>
        /// <param name="userNetworkID">User Network ID</param>
        /// <param name="setUserCanChPwd">True/False use can change  his/her password</param>
        public override void SetUserCanChangePassword(string userDN, bool setUserCanChPwd)
        {


            DirectoryEntry de = null;

            bool found = false;

            try
            {
                de = ConnectToDirectoryEntry(userDN);
                AccessControlEntry Ace1 = new AccessControlEntry();

                SecurityDescriptor mySecurityDescriptor = (SecurityDescriptor)de.Properties["ntSecurityDescriptor"].Value;

                AccessControlList Dacl = (AccessControlList)mySecurityDescriptor.DiscretionaryAcl;

                foreach (AccessControlEntry ACE in (IEnumerable)Dacl)
                {
                    if (ACE.ObjectType == CHANGE_PASSWORD_GUID)
                    {
                        if ((ACE.Trustee == @"NT AUTHORITY\SELF") || (ACE.Trustee == "EVERYONE"))
                        {
                            if (ACE.AceType == ADS_ACETYPE_ACCESS_DENIED_OBJECT)
                            {
                                // Remore the ACL
                                Dacl.RemoveAce(ACE);

                                found = true;

                            }
                        }
                    }
                }


                if (found)
                {
                    mySecurityDescriptor.DiscretionaryAcl = Dacl;
                    //de.Invoke( "ntSecurityDescriptor", new object[] {mySecurityDescriptor} );
                    de.Properties["ntSecurityDescriptor"].Value = mySecurityDescriptor;

                    de.CommitChanges();

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                de.Dispose();
            }
        }

        #endregion

        #region Force User to Change Password At Next Login
        /// <summary>
        /// Funciton will force a user to have to change his/her password at the next login
        /// </summary>
        /// <param name="sContainerName">User samAccountName inside the domain</param>
        /// <param name="forceUserResetPWDnextLogin">Yes or no to force reset?</param>
        public void UserMustChangePasswordAtNextLogin(string userDN, bool forceUserResetPWDnextLogin)
        {
            DirectoryEntry de = null;

            try
            {
                de = ConnectToDirectoryEntry(userDN);

                UserMustChangePasswordAtNextLogin(de, forceUserResetPWDnextLogin);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                de.Dispose();
                de = null;
            }
        }

        private void UserMustChangePasswordAtNextLogin(DirectoryEntry de, bool forceUserResetPWDnextLogin)
        {

            int val = (int)de.Properties["userAccountControl"].Value;

            try
            {
                if (forceUserResetPWDnextLogin)
                {
                    de.Properties["userAccountControl"].Value = val | ADS_UF_PASSWORD_EXPIRED;
                }
                else
                {
                    de.Properties["userAccountControl"].Value = val & ~ADS_UF_PASSWORD_EXPIRED;
                }

                de.CommitChanges();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region PasswordTimeTillExpired
        /// <summary>
        /// Function will evaluate a domain user, set password properties. Time to password expire, ect.
        /// </summary>
        /// <param name="sContainerName">Domain User.</param>
        /// <returns>Password object.</returns>
        public PasswordSettings GetUserPasswordSettings(string userDN)
        {
            return GetUserPasswordSettings(ConnectToDirectoryEntry(userDN));
        }

        /// <summary>
        /// Function will evaluate a domain user, set password properties. Time to password expire, ect.
        /// </summary>
        /// <param name="deUser">Domain User.</param>
        /// <returns>Password object.</returns>
        public PasswordSettings GetUserPasswordSettings(DirectoryEntry deUser)
        {
            bool bDoesPasswordExpire;
            bool bHasPasswordBeenSet;
            bool bPasswordExpired;
            DateTime datPasswordLastSet;
            int nTimeSincePasswordLastSet;
            int nDaysTillPasswordExpires;

            DirectoryEntry deDomain = null;

            try
            {
                if (deUser != null)
                {
                    deDomain = new DirectoryEntry("LDAP://" + this._conn.ldapHost, this._conn.DomainUser, this._conn.DomainPassword, System.DirectoryServices.AuthenticationTypes.Secure);

                    IADsLargeInteger liMaxPwd = (IADsLargeInteger)deDomain.Properties["maxPwdAge"][0];
                    TimeSpan maxPwd = new TimeSpan(GetLongFromLargeInteger(liMaxPwd));
                    int nMAXpwd = 0 - (int)maxPwd.TotalDays;

                    string sMaxPasswordAge = maxPwd.TotalDays.ToString();

                    int nUserContrl = (int)deUser.Properties["userAccountControl"][0];
                    string sUserAccountControl = nUserContrl.ToString();

                    const int UF_DONT_EXPIRE_PASSWD = 0x10000;
                    int pwditexp = UF_DONT_EXPIRE_PASSWD & nUserContrl;

                    bDoesPasswordExpire = pwditexp == 0x10000 ? false : true;

                    // Now find out when the pwd was last set			   
                    IADsLargeInteger liPwLastSet = (IADsLargeInteger)deUser.Properties["pwdLastSet"][0];

                    if ((liPwLastSet.HighPart == 0x0) & (liPwLastSet.HighPart == 0x0))
                        bHasPasswordBeenSet = false;
                    else
                        bHasPasswordBeenSet = true;

                    long lNum = GetLongFromLargeInteger(liPwLastSet);
                    System.DateTime sdt = DateTime.FromFileTime(lNum);

                    datPasswordLastSet = sdt;

                    // Get the number of days since password last set and today
                    System.DateTime dtNow = DateTime.Now;
                    TimeSpan tsLastSet;
                    tsLastSet = dtNow.Subtract(sdt.Date);

                    //  cast to int to round off hours, seconds and ticks
                    int nLastSet = (int)(tsLastSet.TotalDays);

                    nDaysTillPasswordExpires = nLastSet;

                    int pwdLife = nMAXpwd - nLastSet;

                    bPasswordExpired = nMAXpwd < nLastSet ? true : false;

                    nTimeSincePasswordLastSet = pwdLife;

                    return (new PasswordSettings(bDoesPasswordExpire, bHasPasswordBeenSet, bPasswordExpired,
                        datPasswordLastSet, nTimeSincePasswordLastSet, nDaysTillPasswordExpires));
                }
                else
                {
                    throw new Exception("User not found");
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                if (deDomain != null)
                    deDomain.Dispose();

                deDomain = null;
            }


        }

        #endregion

        private string GetDomainControllerToConnectTo
        {
            get
            {
                DirectoryContext dc = null;
                Domain d = null;
                DomainControllerCollection dcc = null;

                try
                {
                    dc = new DirectoryContext(DirectoryContextType.Domain, this._conn.ldapHost, this._conn.DomainUser, this._conn.DomainPassword);

                    d = Domain.GetDomain(dc);

                    //dcc = d.FindAllDomainControllers("NA-NC-CH");
                    //return dcc[0].Name;

                    string domainController = d.FindDomainController("NA-NC-CH", LocatorOptions.ForceRediscovery | LocatorOptions.AvoidSelf).Name;
                    return domainController;
                }
                catch (Exception ex)
                {
                    throw new Exception("Could not find a domain controller to connect to.", ex);
                }
                finally
                {
                    dc = null;
                    dcc = null;

                    if (d != null)
                        d.Dispose();

                    d = null;
                }
            }
        }

    }
}
