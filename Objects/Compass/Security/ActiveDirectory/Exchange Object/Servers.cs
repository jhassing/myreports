using System.Collections;

namespace Compass.Security.ActiveDirectory.Exchange
{
    /// <summary>
    /// Summary description for Servers.
    /// </summary>
    public class ExchangeServers : ADBaseObject
    {
        #region ~const

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="DomainUser">Domain user with read-write domain access.</param>
        public ExchangeServers(LDAPConnection ldpConnection)
            : base(ldpConnection)
        {

        }

        #endregion


        #region Find Mail Servers

        /// <summary>
        /// Returns an array of objects containing the valid Exchange Servers
        /// </summary>
        /// <param name="parms">Arraylist of parameters</param>
        /// <param name="ldapSearchBase">Search base for LDAP. ex. "dc=na,dc=company,dc=corp"</param>/// 
        /// <returns>Array of object struct types (User Defined)</returns>
        public ArrayList FindMailServers(ArrayList parms, string ldapSearchBase, int searhScope)
        {
            return base.FindObjects(new Hashtable(), parms, "objectCategory", new string[] { "msExchExchangeServer" }, ldapSearchBase, searhScope, 200);
        }

        #endregion

        #region Find Mail Store

        /// <summary>
        /// Returns an array of objects containing the valid Exchange Servers
        /// </summary>
        /// <param name="parms">Arraylist of parameters</param>
        /// <param name="ldapSearchBase">Search base for LDAP. ex. "dc=na,dc=company,dc=corp"</param>/// 
        /// <returns>Array of object struct types (User Defined)</returns>
        public ArrayList FindMailStore(ArrayList parms, string ldapSearchBase, int searhScope)
        {
            return base.FindObjects(new Hashtable(), parms, "objectCategory", new string[] { "msExchPrivateMDB" }, ldapSearchBase, searhScope, 200);
        }

        #endregion
    }
}
