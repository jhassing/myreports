using System;
using System.Text;

namespace Compass.Security.Cryptography {
	/// <summary>
	/// Helper class to encrypt and decrypt connection string info
	/// </summary>
	public class EncryptUtility {

		public static string Decrypt(object bytInput, bool bSimpleEncyrption)
		{ 
			// If the variable is blank, return the input
			if(bytInput.Equals(string.Empty) || bytInput.Equals(System.DBNull.Value))
			{
				return("");
			}

			if (bSimpleEncyrption)
				return(SimpleEncryption.Decrypt(Encoding.ASCII.GetString((byte[])bytInput)));
			else
				return(Decrypt(Encoding.ASCII.GetString((byte[])bytInput)));

		}

		public static string Decrypt(string sInputString)
		{

			// If the variable is blank, return the input
			if(sInputString.Equals(string.Empty) || sInputString.Equals(System.DBNull.Value))
			{
				return sInputString;
			}

			// Create an instance of the encryption API
			// We assume the key has been encrypted on this machine and not by a user
			DataProtector dp = new DataProtector(Store.Machine );

			// Use the API to decrypt the connection string
			// API works with bytes so we need to convert to and from byte arrays
			byte[] decryptedData = dp.Decrypt( Convert.FromBase64String( sInputString ), null );
			
			// Return the decyrpted data to the string
			return Encoding.ASCII.GetString( decryptedData );
		}

		public static string Decrypt(string sInputString, bool bSimpleEncyrption)
		{
			if (bSimpleEncyrption)
				return(SimpleEncryption.Decrypt(sInputString));
			else
				return(Decrypt(sInputString));

		}


		private static byte[] Encrypt(object encryptedString, bool bSimpleEncyrption){
			if (bSimpleEncyrption)
				return(Encoding.ASCII.GetBytes(SimpleEncryption.Encrypt((string)encryptedString)));
			else
				return(Encoding.ASCII.GetBytes((string)Encrypt(encryptedString.ToString())));
		}

		public static object Encrypt(string encryptedString)
		{
			// Create an instance of the encryption API
			// We assume the key has been encrypted on this machine and not by a user
			DataProtector dp = new DataProtector(Store.Machine);

			// Use the API to encrypt the connection string
			// API works with bytes so we need to convert to and from byte arrays
			byte[] dataBytes = Encoding.ASCII.GetBytes( encryptedString );
			byte[] encryptedBytes = dp.Encrypt( dataBytes, null );

			// Return the encyrpted data to the string
			return Convert.ToBase64String( encryptedBytes );

		}

		public static object Encrypt(string encryptedString, bool bSimpleEncyrption)
		{
			if (bSimpleEncyrption)
				return(SimpleEncryption.Encrypt(encryptedString));
			else
				return(Encrypt(encryptedString));
		}

		public static object Encrypt(string encryptedString, bool bSimpleEncyrption, bool ReturnByteArray)
		{
			if ((!bSimpleEncyrption) && (!ReturnByteArray))
				return(Encrypt(encryptedString));
			else if ((bSimpleEncyrption) && (!ReturnByteArray))
				return(Encrypt(encryptedString, true));
			else if (ReturnByteArray)
				return(Encrypt((object)encryptedString, bSimpleEncyrption));
			else
				return(null);
		}


	}	// Class

}	// Namespace
