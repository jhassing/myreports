using System;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;

namespace Compass.Security.Cryptography
{
    /// <summary>
    /// Summary description for SimpleEncyrption.
    /// </summary>
    internal sealed class SimpleEncryption
    {

        // Fields
        private static TripleDESCryptoServiceProvider des;

        // Methods
        public SimpleEncryption()
        {

        }


        private static string GetKey()
        {
            return ConfigurationSettings.AppSettings["Key"].ToString();
        }


        public static string Encrypt(string message)
        {
            byte[] array1;
            string text1;
            des = new TripleDESCryptoServiceProvider();
            des.Key = Encoding.ASCII.GetBytes(GetKey());
            des.Mode = System.Security.Cryptography.CipherMode.ECB; //2
            array1 = Encoding.ASCII.GetBytes(message);
            text1 = Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(array1, 0, array1.Length));
            return text1;
        }


        public static string Decrypt(string message)
        {
            byte[] array1;
            string text1;
            des = new TripleDESCryptoServiceProvider();
            des.Key = Encoding.ASCII.GetBytes(GetKey());
            des.Mode = System.Security.Cryptography.CipherMode.ECB; //2;
            array1 = Convert.FromBase64String(message);
            text1 = Encoding.ASCII.GetString(des.CreateDecryptor().TransformFinalBlock(array1, 0, array1.Length));
            return text1;
        }

    }
}
