using System;
using System.DirectoryServices;
using Compass.Security.ActiveDirectory;
using System.Collections;
using System.Collections.Specialized;
using System.Text;

using Compass.Reporting.Actuate;

namespace TestApp
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class Class1
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		/// 
		public static ADUser d = null;
		public static ADGroup g = null;

		[STAThread]
		static void Main(string[] args)
		{
			//ActuateProxy oo = new ActuateProxy();
			g = new ADGroup("schiaj02", "Test,3456", "na.compassDev.corp")   ;

			#region Test Delete Group(s)

//
//			ArrayList ary = new ArrayList();
//
//			ary.Add("asdasdasd_GRP");
//			ary.Add("aaaaaaaaaa_GRP");
//			ary.Add("asdfasdasd_GRP");
//			ary.Add("asd_GRP");
//
//			g.DeleteGroup( ary );
			
			#endregion

			#region Test Find Users and Properties
			
//			d = new ADUser("schiaj01", "Test,4567", "na.compassGroup.corp");
//
//			string searchName = "ward*";
//
//			#region Build a hashtable of properties to search for.
//			
//			Hashtable htParams = new Hashtable();
//			htParams.Add( "samAccountName", searchName);
//			htParams.Add( "sn", searchName);
//			htParams.Add( "givenName", searchName);
//
//			#endregion
//
//			#region Get all the parameters for the search.
//
//			ArrayList aryParams = new ArrayList();
//			aryParams.Add( "samAccountName" );
//			aryParams.Add( "sn" );
//			aryParams.Add( "givenName" );
//
//			#endregion
//
//			tObjects[] users = d.FindUsers( htParams, aryParams );
//
//			for (int x=0;x<users.Length;x++)
//			{
//				string name = users[x].Attributes[0].Name;
//				string Value = users[x].Attributes[0].Value;
//			
//				Console.WriteLine(name + ":\t" + Value);
//
//				for (int y=1;y<users[x].Attributes.Length;y++)
//				{
//					string name1 = users[x].Attributes[y].Name;
//					string Value1 = users[x].Attributes[y].Value;
//			
//					Console.WriteLine("\t" + name1.PadLeft(10,' ') + ": " + Value1);
//				}
//
//				Console.WriteLine("");				
//			}

			#endregion

			#region Test Find Groups

//			ArrayList aryGroupParams = new ArrayList();
//			aryGroupParams.Add( "samAccountName" );
//			aryGroupParams.Add( "description" );
//
//			tObjects[] groupListing = g.FindGroups("cha*_grp", aryGroupParams); 
//			
//			for (int x=0;x<groupListing.Length;x++)
//			{
//				string name = groupListing[x].Attributes[0].Name;
//				string Value = groupListing[x].Attributes[0].Value;
//			
//				Console.WriteLine(name + ":\t" + Value);
//
//				for (int y=1;y<groupListing[x].Attributes.Length;y++)
//				{
//					string name1 = groupListing[x].Attributes[y].Name;
//					string Value1 = groupListing[x].Attributes[y].Value;
//			
//					Console.WriteLine("\t" + name1.PadLeft(10,' ') + ": " + Value1);
//				}
//
//				Console.WriteLine("");				
//			}

			#endregion

			#region Find all members of a group

			string[] members = g.GetGroupMembership( "cha_grp" );

			foreach (string member in members)
			{
				Console.WriteLine( member );
			}

			#endregion


		}

	}
}
