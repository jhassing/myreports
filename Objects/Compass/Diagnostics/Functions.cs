using System;

namespace Compass.Diagnostics
{
	public class EventLog
	{

		public static void WriteToEventLog( string AppName, string Entry, System.Diagnostics.EventLogEntryType EventType)
		{
			System.Diagnostics.EventLog eventLog = new System.Diagnostics.EventLog();

			try
			{
				if (!System.Diagnostics.EventLog.Exists(AppName))
					System.Diagnostics.EventLog.CreateEventSource(AppName, AppName);

				System.Diagnostics.EventLog.WriteEntry(AppName, Entry, EventType);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		
	}



}
