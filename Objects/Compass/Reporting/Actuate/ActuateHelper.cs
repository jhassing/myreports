using System;
using System.Collections;
using System.IO;
using System.Threading;
using Server_Proxy;
using Server_Proxy.localhost;

namespace Compass.Reporting.Actuate
{
    public class ActuateHelper : IDisposable
    {
        protected ActuateAPI l_proxy;

        #region  enum ActuateFileType

        public enum ActuateFileType
        {
            All = 0,
            Directory = 1,
            ROI = 2,
            ROX = 3,
            PDF = 4
        }

        #endregion

        #region Login Actuate

        /// <summary>
        /// Logs into the actuate server
        /// </summary>
        /// <param name="sUserName">User name to log in with.</param>
        /// <param name="sPassword">Password to login with.</param>
        /// <param name="sURL">Server to log into.</param>
        /// <example>base.LoginActuate("acadmin", "password", "Http://Reportserver:8000");</example>
        protected virtual void LoginActuate(string sUserName, string sPassword, string sURL)//, out ActuateAPI proxy)
        {

            //ActuateAPI proxy;
            //	Create an instance of server proxy. Construct an empty header.
            l_proxy = new ActuateAPI();
            l_proxy.Url = sURL;
            l_proxy.HeaderValue = new Header();

            //	Prepare Login reqest parameters
            Server_Proxy.localhost.Login l_LoginReq = new Server_Proxy.localhost.Login();
            l_LoginReq.User = sUserName;
            l_LoginReq.Password = sPassword;

            //	Send Login reqest, handle SOAP exception if any
            LoginResponse l_loginRes;
            try
            {
                l_loginRes = l_proxy.login(l_LoginReq);
            }
            catch (Exception ex)
            {
                throw (ex);
                //throw (new ApplicationException("Login to actuate failed", ex));
            }

            //	Store authentication id
            l_proxy.HeaderValue.AuthId = l_loginRes.AuthId;

        }

        public void Logout()
        {
            l_proxy.Dispose();
        }

        #endregion

        #region ActuateFileAttributes[] getDirectoryContents

        protected GetFolderItemsResponse getDirectoryContents(string sDir, ActuateFileType enumFileType, string sFilter)
        {
            return (_getDirectoryContents(sDir, enumFileType, sFilter, false));
        }

        protected GetFolderItemsResponse getDirectoryContents(string sDir, ActuateFileType enumFileType, string sFilter, bool bLatestVersion)
        {
            return (_getDirectoryContents(sDir, enumFileType, sFilter, bLatestVersion));
        }

        protected GetFolderItemsResponse getDirectoryContents(string sDir, ActuateFileType enumFileType)
        {
            return (_getDirectoryContents(sDir, enumFileType, "*", false));
        }

        protected GetFolderItemsResponse getDirectoryContents(string sDir, ActuateFileType enumFileType, bool bLatestVersion)
        {
            return (_getDirectoryContents(sDir, enumFileType, "*", bLatestVersion));
        }

        protected GetFolderItemsResponse getDirectoryContents(string sDir, string sFilter)
        {
            return (_getDirectoryContents(sDir, ActuateFileType.All, sFilter, false));
        }

        protected GetFolderItemsResponse getDirectoryContents(string sDir, string sFilter, bool bLatestVersion)
        {
            return (_getDirectoryContents(sDir, ActuateFileType.All, sFilter, bLatestVersion));
        }

        protected GetFolderItemsResponse getDirectoryContents(string sDir, bool bLatestVersion)
        {
            return (_getDirectoryContents(sDir, ActuateFileType.All, "*", bLatestVersion));
        }

        protected GetFolderItemsResponse getDirectoryContents(string sDir)
        {
            return (_getDirectoryContents(sDir, ActuateFileType.All, "*", false));
        }

        private GetFolderItemsResponse _getDirectoryContents(string sDir, ActuateFileType enumFileType,
                                                                string sFilter, bool bLatestVersion)
        {

            ArrayList retValues = new ArrayList();

            // Prepare GetFolderItems request
            Server_Proxy.localhost.GetFolderItems l_req = new Server_Proxy.localhost.GetFolderItems();
            l_req.ItemElementName = Server_Proxy.localhost.ItemChoiceType36.FolderName;
            l_req.Item = sDir;
            l_req.LatestVersionOnly = bLatestVersion;
            l_req.LatestVersionOnlySpecified = true;
            l_req.ResultDef = new string[7];
            l_req.ResultDef[0] = "FileType";
            l_req.ResultDef[1] = "Description";
            l_req.ResultDef[2] = "Owner";
            l_req.ResultDef[3] = "TimeStamp";
            l_req.ResultDef[4] = "Version";
            l_req.ResultDef[5] = "PageCount";
            l_req.ResultDef[6] = "UserPermissions";

            //	Prepare the search critaria: set fetch size = 2. So server will
            // return 2 items a time in loop until all itemes are listed.
            l_req.Search = new FileSearch();
            FileCondition[] l_searchCondition = new FileCondition[2];

            l_searchCondition[0] = new FileCondition();
            l_searchCondition[0].Field = FileField.FileType;
            string sFileType = "*";

            switch (enumFileType)
            {
                case ActuateFileType.All:
                    sFileType = "*";
                    break;

                case ActuateFileType.Directory:
                    sFileType = "Directory";
                    break;

                case ActuateFileType.ROI:
                    sFileType = "ROI";
                    break;

                case ActuateFileType.ROX:
                    sFileType = "ROX";
                    break;

            }

            l_searchCondition[0].Match = sFileType;

            l_searchCondition[1] = new FileCondition();
            l_searchCondition[1].Field = FileField.Name;
            l_searchCondition[1].Match = this.FormatSearchFile(sFilter);

            l_req.Search.Item = l_searchCondition;
            l_req.Search.FetchSize = 20000;
            l_req.Search.FetchSizeSpecified = true;
            l_req.Search.CountLimit = -1; // count all items
            l_req.Search.CountLimitSpecified = true;

            GetFolderItemsResponse l_res;
            int count = 0;

            //	Loop to retrieve all the items
            //do 
            //{
            try
            {
                l_res = l_proxy.getFolderItems(l_req);
            }
            catch (Exception ex)
            {
                // Handle Actuate Exception here
                throw ex;
            }

            return l_res;

            //for(int i = 0; i < l_res.ItemList.Length; i++)								
            //	retValues.Add(new ActuateFileAttributes(l_res.ItemList[i].Name, l_res.ItemList[i].FileType, l_res.ItemList[i].TimeStamp, l_res.ItemList[i].Version, l_res.ItemList[i].PageCount, l_res.ItemList[i].UserPermissions));

            //count += l_res.ItemList.Length;

            //	Reusing the fetch handle
            //l_req.Search.FetchHandle = l_res.FetchHandle;
            //} while(l_res.FetchHandle != null); // FetchHandle will be empty if there is no more items left

            //int nCount = retValues.Count;
            //ActuateFileAttributes[] tmp = new ActuateFileAttributes[nCount];

            //for (int yy=0;yy<nCount;yy++)
            //	tmp[yy] = ((ActuateFileAttributes)retValues[yy]);

            //return(tmp);
        }


        #endregion

        #region Rename File or Folder.

        /// <summary>
        /// Rename the file/folder in Actuate to a new name
        /// </summary>
        /// <param name="sOldFolderName">Full path to the original folder name.</param>
        /// <param name="sNewFolderName">Full path including the new folder name.</param>
        /// <example>RenameFileFolder("/(1) Accounting/cho - Mid Atlantic Region", "/(1) Accounting/cho - South Atlantic Region")</example>
        protected void RenameFileFolder(string sOldFolderName, string sNewFolderName)
        {

            MoveFile moveFile = new MoveFile();



            try
            {
                moveFile.Target = sNewFolderName;
                moveFile.ItemElementName = ItemChoiceType23.WorkingFolderName;
                moveFile.Item = sOldFolderName;
                moveFile.Item1ElementName = Item1ChoiceType12.Name;
                moveFile.Item1 = sOldFolderName;
                moveFile.ReplaceExisting = false;

                AdminOperation[] adm = new AdminOperation[1];
                adm[0] = new AdminOperation();
                adm[0].Item = moveFile;

                AdministrateResponse adminRes;

                adminRes = l_proxy.administrate(adm);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            Thread.Sleep(5);

        }


        #endregion

        #region Delete File or Folder

        /// <summary>
        /// Delete the specified file/folder in Actuate.
        /// </summary>
        /// <param name="sFolderPath">Full path to the folder name.</param>
        /// <param name="sFileFolderToDelete">File/Folder name you wish to delete.</param>
        /// <example>DeleteFileFolder("/(1) Accounting/cho - Mid Atlantic Region", "/My Report.ROI")</example>
        protected void DeleteFileFolder(string sFolderPath, string sFileFolderToDelete)
        {

            try
            {

                // Base folder to look in.
                DeleteFile deleteFile = new DeleteFile();
                FileSearch fsearch = new FileSearch();
                FileCondition fcond = new FileCondition();

                // File/Folder to delete				
                fcond.Field = FileField.Name;
                fcond.Match = FormatSearchFile(sFileFolderToDelete);
                deleteFile.Recursive = true;
                deleteFile.RecursiveSpecified = true;

                deleteFile.IgnoreMissing = false;
                deleteFile.IgnoreMissingSpecified = true;

                fsearch.Item = fcond;

                deleteFile.Item = sFolderPath;
                deleteFile.ItemElementName = ItemChoiceType22.WorkingFolderName;
                deleteFile.Item1 = fsearch;
                deleteFile.Item1ElementName = Item1ChoiceType11.Search;

                AdminOperation[] adminOperation = new AdminOperation[1];
                AdminOperation aOperation = new AdminOperation();

                aOperation.Item = deleteFile;
                adminOperation[0] = aOperation;

                AdministrateResponse adminResponse = l_proxy.administrate(adminOperation);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            Thread.Sleep(5);
        }


        #endregion

        #region Does File Exist

        /// <summary>
        /// Looks into the specified directory, trys to see if a particual file does in fact exist
        /// </summary>
        /// <param name="sFolderPath">Folder location to search</param>
        /// <param name="sFileFolderToFind">File or folder to search for.</param>
        /// <param name="latestVersionOnly">Latest version only, or all versions.</param>
        /// <param name="searchRecursive">Search recursive or sub folders.</param>
        /// <returns>True or False based on the search results</returns>
        /// <example>DoesFileExist("(1) Accounting/", "My Report.roi", false, false)</example>
        protected SelectFilesResponse DoesFileExist(string sFolderPath, string sFileFolderToFind, bool latestVersionOnly, bool searchRecursive)
        {

            FileSearch l_fileSearch = null;
            SelectFiles l_req = null;
            SelectFilesResponse l_res = null;
            FileCondition l_fileCondition = null;

            try
            {

                l_req = new SelectFiles();

                // Folder to search in
                l_req.Item = sFolderPath;
                l_req.ItemElementName = ItemChoiceType35.WorkingFolderName;
                l_req.LatestVersionOnly = latestVersionOnly;
                l_req.LatestVersionOnlySpecified = true;
                l_req.Recursive = searchRecursive;
                l_req.RecursiveSpecified = true;
                l_req.ResultDef = new string[1];
                l_req.ResultDef[0] = "TimeStamp";

                //	Prepare search criteria
                l_fileSearch = new FileSearch();
                l_fileCondition = new FileCondition();
                l_fileCondition.Field = FileField.Name;
                // File to search for
                l_fileCondition.Match = FormatSearchFile(sFileFolderToFind);
                l_fileSearch.Item = l_fileCondition;
                l_fileSearch.CountLimit = -1; // count all objects in query
                l_fileSearch.CountLimitSpecified = true;

                l_req.Item1 = l_fileSearch;
                l_req.Item1ElementName = Item1ChoiceType17.Search;

                l_res = l_proxy.selectFiles(l_req);



            }
            catch (Exception ex)
            {
                // throw(ex);
            }
            finally
            {
                l_fileSearch = null;
                l_req = null;
                l_fileCondition = null;
            }

            Thread.Sleep(5);
            return (l_res);
        }


        #endregion

        #region Create Folder

        protected void CreateFolder(string sLocationToCreateFolder, string sFolderToCreate)
        {
            CreateFolder(sLocationToCreateFolder, sFolderToCreate, "");
        }

        /// <summary>
        /// Create folder inside Actuate
        /// </summary>
        /// <param name="sLocationToCreateFolder">Full path to the location in which we will add the new folder.</param>
        /// <param name="sFolderToCreate">New folder name to be created.</param>
        /// <example>CreateFolder("/(1) Accounting/cho - Mid Atlantic Region", "/My New Folder")</example>
        protected void CreateFolder(string sLocationToCreateFolder, string sFolderToCreate, string description)
        {
            AdministrateResponse adminRes = null;
            CreateFolder createFolder = null;
            AdminOperation[] adm = null;

            try
            {
                createFolder = new CreateFolder();

                createFolder.FolderName = sLocationToCreateFolder + sFolderToCreate;
                createFolder.IgnoreDup = true;
                createFolder.IgnoreDupSpecified = true;
                createFolder.ItemElementName = ItemChoiceType21.WorkingFolderName;
                createFolder.Item = "/";
                createFolder.Description = description;

                adm = new AdminOperation[1];
                adm[0] = new AdminOperation();
                adm[0].Item = createFolder;

                adminRes = l_proxy.administrate(adm);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                adminRes = null;
                createFolder = null;
                adm = null;
            }

            Thread.Sleep(5);
        }


        #endregion

        #region Copy File/Folder

        protected void CopyFileFolder(string sSourceFolder, string sSourceFileFolderToMove, string sTarget, bool ReplaceExistingReport)
        {
            AdminOperation[] adminOperation = null;
            AdminOperation adminMoveFile = null;
            CopyFile copyFile = null;
            FileSearch fsearch = null;
            FileCondition fcond = null;

            try
            {
                adminOperation = new AdminOperation[1];
                adminMoveFile = new AdminOperation();
                copyFile = new CopyFile();
                fsearch = new FileSearch();
                fcond = new FileCondition();

                copyFile.Target = sTarget;
                fcond.Field = FileField.Name;
                fcond.Match = FormatSearchFile(sSourceFileFolderToMove);
                fsearch.Item = fcond;
                copyFile.Item = sSourceFolder;
                copyFile.ReplaceExisting = ReplaceExistingReport;
                copyFile.ReplaceExistingSpecified = true;
                //moveFile.MaxVersions = 1;
                //moveFile.MaxVersionsSpecified = true;
                copyFile.LatestVersionOnly = false;
                copyFile.LatestVersionOnlySpecified = true;
                copyFile.ItemElementName = ItemChoiceType24.WorkingFolderName;
                copyFile.Item1 = fsearch;
                copyFile.Item1ElementName = Item1ChoiceType13.Search;

                // Create The Administration Operations for each step in Actuate		
                adminMoveFile.Item = copyFile;

                adminOperation[0] = adminMoveFile;

                AdministrateResponse adminResponse = l_proxy.administrate(adminOperation);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                adminOperation = null;
                adminMoveFile = null;
                copyFile = null;
                fsearch = null;
                fcond = null;
            }

            Thread.Sleep(5);
        }


        #endregion

        #region Move File/Folder

        protected void MoveFileFolder(string sSourceFolder, string sSourceFileFolderToMove, string sTarget, bool ReplaceExistingReport)
        {
            AdminOperation[] adminOperation = null;
            AdminOperation adminMoveFile = null;
            MoveFile moveFile = null;
            FileSearch fsearch = null;
            FileCondition fcond = null;

            try
            {
                adminOperation = new AdminOperation[1];
                adminMoveFile = new AdminOperation();
                moveFile = new MoveFile();
                fsearch = new FileSearch();
                fcond = new FileCondition();

                moveFile.Target = sTarget;
                fcond.Field = FileField.Name;
                fcond.Match = FormatSearchFile(sSourceFileFolderToMove);
                fsearch.Item = fcond;
                moveFile.Item = sSourceFolder;
                moveFile.ReplaceExisting = ReplaceExistingReport;
                moveFile.ReplaceExistingSpecified = true;
                //moveFile.MaxVersions = 1;
                //moveFile.MaxVersionsSpecified = true;
                moveFile.LatestVersionOnly = false;
                moveFile.LatestVersionOnlySpecified = true;
                moveFile.ItemElementName = ItemChoiceType23.WorkingFolderName;
                moveFile.Item1 = fsearch;
                moveFile.Item1ElementName = Item1ChoiceType12.Search;

                // Create The Administration Operations for each step in Actuate		
                adminMoveFile.Item = moveFile;

                adminOperation[0] = adminMoveFile;

                AdministrateResponse adminResponse = l_proxy.administrate(adminOperation);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                adminOperation = null;
                adminMoveFile = null;
                moveFile = null;
                fsearch = null;
                fcond = null;
            }

            Thread.Sleep(5);
        }


        #endregion

        #region Archive File

        protected void ArchiveFileFolder(int nMonthsToArchive, string path, string fileFolder, System.DateTime dateTimeArchiveExpires)
        {
            ArchiveRule aRule = new ArchiveRule();
            //DateTime dat;


            aRule.FileType = fileFolder.Substring(fileFolder.Length - 3, 3).ToUpper();

            try
            {
                //				ActuateFileAttributes[] aa = getDirectoryContents(path, Compass.Reporting.Actuate.ActuateHelper.ActuateFileType.All , FormatSearchFile(fileFolder), true);
                //				
                //				if (aa.Length > 0)
                //					dat = aa[0].TimeStamp.AddMonths(nMonthsToArchive);
                //				else
                //					dat = System.DateTime.Now.AddMonths(nMonthsToArchive);

                // if the archive expieration date is still greater then today, delete the file and exit the function
                if (dateTimeArchiveExpires <= System.DateTime.Now)
                {
                    DeleteFileFolder(path, fileFolder);
                    return;
                }

                aRule.Item = dateTimeArchiveExpires;
                aRule.ArchiveOnExpiration = false;
                aRule.ArchiveOnExpirationSpecified = true;

                ArchiveRule[] archiveArray = new ArchiveRule[] { aRule };

                UpdateFile upFile = new UpdateFile();
                upFile.LatestVersionOnly = true;
                upFile.LatestVersionOnlySpecified = true;
                upFile.Recursive = false;
                upFile.RecursiveSpecified = true;
                upFile.IgnoreMissing = true;
                upFile.IgnoreMissingSpecified = true;
                UpdateFileOperation upFileOp = new UpdateFileOperation();
                upFileOp.Item = archiveArray;
                upFileOp.ItemElementName = ItemChoiceType26.AddArchiveRules;
                upFile.Item1 = path + fileFolder;
                upFile.Item1ElementName = Item1ChoiceType14.Name;
                UpdateFileOperation[] arr = new UpdateFileOperation[] { upFileOp };
                upFile.UpdateFileOperationGroup = arr;


                AdminOperation admOp = new AdminOperation();
                admOp.Item = upFile;

                AdminOperation[] l_adminRequest = new AdminOperation[] { admOp };
                AdministrateResponse res = l_proxy.administrate(l_adminRequest);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            Thread.Sleep(5);
        }


        #endregion

        #region DirSearch

        /// <summary>
        /// Loop through an actuate directory, default settings. Return ALL files.
        /// </summary>
        /// <param name="sDir"></param>
        public void DirSearch(string sDir)
        {
            DirSearch(sDir, Compass.Reporting.Actuate.ActuateHelper.ActuateFileType.All);
        }

        public void DirSearch(string sDir, Compass.Reporting.Actuate.ActuateHelper.ActuateFileType actuateFileType)
        {
            if (sDir.Length > 1)
            {
                if (sDir.Substring(0, 2) == "//")
                    sDir = sDir.Substring(1, sDir.Length - 1);
            }

            try
            {
                GetFolderItemsResponse countAttribs = getDirectoryContents(sDir, actuateFileType, true);

                if (countAttribs != null)
                {
                    for (int x = 0; x < countAttribs.TotalCount; x++)
                    {

                        ProcessFileDirectory(sDir, countAttribs.ItemList[x]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        #endregion

        #region Virtual Process File Directory

        protected virtual void ProcessFileDirectory(string sDirectory, Server_Proxy.localhost.File oActuateFileAttributes)
        {
            // Dummy funciton

        }


        #endregion

        #region Set File/Folder permissions

        protected void SetFolderPermissions(ArrayList aryRoleNames, string sFolderName, string sAccessRights, bool bOverrideExistingPermissions)
        {
            if (bOverrideExistingPermissions)
                _setFolderPermissions(aryRoleNames, sFolderName, sAccessRights, ItemChoiceType26.SetPermissions);
            else
                _setFolderPermissions(aryRoleNames, sFolderName, sAccessRights, ItemChoiceType26.GrantPermissions);

        }


        private void _setFolderPermissions(ArrayList aryRoleNames, string sFolderName, string sAccessRights, ItemChoiceType26 operation)
        {

            #region Local variables

            if (aryRoleNames == null)
                return;

            if (aryRoleNames.Count == 0)
                return;

            UpdateFile updateFile = new UpdateFile();
            UpdateFileOperation[] list = new UpdateFileOperation[1];
            Permission[] perm = new Permission[aryRoleNames.Count];
            UpdateFileOperation updateFileOp = new UpdateFileOperation();
            Permission permission = null;
            AdminOperation l_updatePrevileges = new AdminOperation();
            AdminOperation[] l_adminRequest = new AdminOperation[1];

            #endregion

            updateFile.Item1 = sFolderName;
            updateFile.Item1ElementName = Item1ChoiceType14.Name;
            updateFile.IgnoreMissing = true;
            updateFile.LatestVersionOnly = false;
            updateFile.LatestVersionOnlySpecified = true;
            updateFile.IgnoreMissingSpecified = true;
            updateFile.Recursive = true;
            updateFile.RecursiveSpecified = true;

            int x = 0;

            foreach (string sRole in aryRoleNames)
            {

                permission = new Permission();
                permission.Item = sRole;
                permission.ItemElementName = ItemChoiceType1.RoleName;
                permission.AccessRight = sAccessRights;

                perm[x] = permission;
                x++;

            }

            updateFileOp.ItemElementName = operation;
            updateFileOp.Item = perm;

            list[0] = updateFileOp;

            updateFile.UpdateFileOperationGroup = list;

            l_updatePrevileges.Item = updateFile;
            l_adminRequest[0] = l_updatePrevileges;

            try
            {
                l_proxy.administrate(l_adminRequest);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                updateFile = null;
                list = null;
                perm = null;
                updateFileOp = null;
                permission = null;
                l_updatePrevileges = null;
                l_adminRequest = null;
            }
        }


        #endregion

        #region Format Search File

        /// <summary>
        /// Replace the illegal search characters for Actuate functionality
        /// </summary>
        /// <param name="sFile"></param>
        /// <returns>Formated string for Actuate usage</returns>
        protected string FormatSearchFile(string sFile)
        {

            // Coment out the "-", if it has not already been done
            int n1 = sFile.IndexOf("-");
            int n2 = sFile.IndexOf("\\-");

            if (n1 > 0 && n2 == -1)
                sFile = sFile.Replace("-", "\\-");

            // Now, comment out the "," if it has not already been done
            int m1 = sFile.IndexOf(",");
            int m2 = sFile.IndexOf("\\,");

            if (m1 > 0 && m2 == -1)
                sFile = sFile.Replace(",", "\\,");

            // Next, comment out the "#", if it has not already been done.
            int o1 = sFile.IndexOf("#");
            int o2 = sFile.IndexOf("\\#");

            if (o1 > 0 && o2 == -1)
                sFile = sFile.Replace("#", "\\#");

            return (sFile);
        }


        #endregion

        #region Get Contents


        /// <summary>
        /// Method will take an ROI and save it as a PDF
        /// </summary>
        /// <param name="file">Full path and file name inside actuate</param>
        /// <param name="saveLocation">Full path and file name on the local Hard Drive</param>
        protected void GetContents(string file, string saveLocation)
        {
            GetContent o_get = new GetContent();

            ObjectIdentifier oI = new ObjectIdentifier();
            ViewParameter vp = new ViewParameter();
            ComponentType ct = new ComponentType();

            oI.Name = file;
            oI.Version = 1;
            oI.VersionSpecified = true;

            vp.Format = "PDF";
            vp.ScalingFactor = 100;
            vp.ScalingFactorSpecified = true;
            vp.AcceptEncoding = "text/xml";

            ct.Id = "0";

            o_get.Component = ct;
            o_get.Object = oI;
            o_get.ViewParameter = vp;
            o_get.DownloadEmbedded = true;

            byte[] data = null;

            GetContentResponse oGet = new GetContentResponse();
            try
            {
                oGet = l_proxy.getContent(o_get);

                Attachment a = oGet.ContentRef;
                data = a.ContentData;

            }
            catch (Exception e)
            {
                throw e;
            }

            MemoryStream l_memStream = new MemoryStream(data);
            FileStream l_fileStream = new FileStream(saveLocation, FileMode.Create);

            l_memStream.WriteTo(l_fileStream);
            l_fileStream.Close();
        }


        #endregion

        protected void DownloadFile(string file, string saveLocation)
        {

            try
            {
                DownloadFile l_req = new DownloadFile();
                l_req.Item = file;
                l_req.ItemElementName = ItemChoiceType34.FileName;
                l_req.DownloadEmbedded = true;

                DownloadFileResponse l_res;

                l_res = l_proxy.downloadFile(l_req);

                //	Save downloaded file
                MemoryStream l_memStream = new MemoryStream(((Attachment)l_res.Item).ContentData);
                FileStream l_fileStream = new FileStream(saveLocation, FileMode.Create);
                l_memStream.WriteTo(l_fileStream);
                l_fileStream.Close();

            }
            catch (Exception e)
            {
                throw e;
            }

        }

        #region Get Counters

        protected GetAllCounterValuesResponse GetAllCounters()
        {
            GetAllCounterValues o_getAllCounters = new GetAllCounterValues();

            try
            {
                return l_proxy.getAllCounterValues(o_getAllCounters);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        protected GetCounterValuesResponse GetCounters(long[] CounterIDList)
        {
            GetCounterValues o_getAllCounters = new GetCounterValues();
            o_getAllCounters.CounterIDList = CounterIDList;

            try
            {
                return l_proxy.getCounterValues(o_getAllCounters);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        #endregion

        #region Get Report Parameters

        protected GetReportParametersResponse GetReportParameters(string reportName)
        {
            // Prepare and send GetReportParameters request
            GetReportParameters l_parameter = new GetReportParameters();
            GetReportParametersResponse l_parameterRes = null;

            l_parameter.Item = reportName;
            l_parameter.ItemElementName = ItemChoiceType39.ReportFileName;

            try
            {
                l_parameterRes = l_proxy.getReportParameters(l_parameter);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return l_parameterRes;

        }
        //
        #endregion

        #region Execute Report

        protected void ExecuteReport(string reportName, Hashtable htParms, string jobName, string headLine)
        {
            // Get the report parameters first
            GetReportParametersResponse l_parameterRes = GetReportParameters(reportName);

            // Prepare SubmitJob request
            Server_Proxy.localhost.SubmitJob l_req = new Server_Proxy.localhost.SubmitJob();
            l_req.JobName = jobName;
            l_req.Headline = headLine;
            l_req.Priority = 1000;
            l_req.Item = reportName;
            l_req.ItemElementName = ItemChoiceType45.InputFileName;
            l_req.Operation = SubmitJobOperation.RunReport;

            if (l_parameterRes.ParameterList != null && l_parameterRes.ParameterList.Length > 0)
            {
                ParameterValue[] l_parameterValues = new ParameterValue[l_parameterRes.ParameterList.Length];
                for (int i = 0; i < l_parameterRes.ParameterList.Length; i++)
                {
                    l_parameterValues[i] = new ParameterValue();
                    l_parameterValues[i].Name = l_parameterRes.ParameterList[i].Name;

                    IDictionaryEnumerator myEnumerator = htParms.GetEnumerator();

                    while (myEnumerator.MoveNext())
                    {
                        if (l_parameterRes.ParameterList[i].Name == myEnumerator.Key.ToString())
                            l_parameterValues[i].Value = myEnumerator.Value.ToString();
                    }
                }

                l_req.Item1 = l_parameterValues;
                l_req.Item1ElementName = Item1ChoiceType21.ParameterValues;
            }

            //	Send request

            SubmitJobResponse l_res;
            try
            {
                l_res = l_proxy.submitJob(l_req);

            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }


        #endregion

        #region Upload File

        /// <summary>
        /// Overloaded method to upload a file into the actuate volume
        /// </summary>
        /// <param name="path">Path inside actuate to place the new file.</param>
        /// <param name="files">Array of full path and file names to be uploaded.</param>
        protected void UploadFile(string path, string[] files)
        {
            try
            {
                foreach (string file in files)
                {
                    UploadFile(path, file);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Overloaded method to upload a file into the actuate volume
        /// </summary>
        /// <param name="path">Path inside actuate to place the new file.</param>
        /// <param name="file">Full path and file name of the file you wish to upload.</param>
        protected void UploadFile(string path, string file)
        {
            // Create extended proxy inctance
            ActuateAPIEx l_proxyEx = new ActuateAPIEx();
            l_proxyEx.Url = l_proxy.Url;
            l_proxyEx.HeaderValue = new Header();
            l_proxyEx.HeaderValue.AuthId = l_proxy.HeaderValue.AuthId;

            // Prepare UploadFile request
            UploadFile l_req = new UploadFile();
            l_req.NewFile = new NewFile();
            char[] l_separators = { '/', '\\', ':' };
            string[] l_strList = file.Split(l_separators, 100);
            l_req.NewFile.Name = path + l_strList[l_strList.Length - 1];
            l_req.Content = new Attachment();
            l_req.Content.ContentType = "binary";
            l_req.Content.ContentId = "Attachment";

            //	Open the file to be uploaded
            try
            {
                ActuateAPIEx.UploadStream = new FileStream(file, FileMode.Open);
            }
            catch (Exception e)
            {
                Console.WriteLine("Can not open the file" + e.Message);
                return;
            }

            // Send request
            UploadFileResponse l_res;
            try
            {
                l_res = l_proxyEx.uploadFile(l_req);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            ActuateAPIEx.UploadStream.Close();
        }


        #endregion

        #region De-structor

        ~ActuateHelper()
        {
            if (l_proxy != null)
                l_proxy.Dispose();

            l_proxy = null;
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            if (l_proxy != null)
                l_proxy.Dispose();

            l_proxy = null;
        }

        #endregion

    }	// Class

}	// Namespace
