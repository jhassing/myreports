#region Copyright
//       	Compass.NET Framework  - version 1.0
//    Copyright 2003 - 2004 Compass Group North America
//    No part of this computer program may be reproduced, transmitted,
//    transcribed, stored in a retrieval system, or translated into any
//    language in any form by any means without the prior written consent
//    of Compass Group North America.
#endregion

///////////////////////////////////////////////////////////
//
//  EmailHelper.cs
//  Implementation of the Class EmailHelper
//  Generated by John Schiavarelli
//  Created on:      12-Sep-2004
//  Original author: 
//  
///////////////////////////////////////////////////////////
//  Modification history:
//		28-Sep-2004: Modified the attachments to handle "multiple" string array attachments. John Schiavarelli
//  
//
///////////////////////////////////////////////////////////

using System;
using System.Collections;
using System.Net.Mail;

namespace Compass.Utilities.Email
{
    /// <summary>
    /// Class handles all send mail routines and utilities.
    /// </summary>
    public class EmailHelper
    {
        /// <summary>
        /// This function sends email using the selected smtp server.
        /// </summary>
        /// <param name="SMTPserver">SMTP Server which to bounce mail.</param>
        /// <param name="subject">Email Subject line.</param>
        /// <param name="messagebody">Email Body. Note: This can be HTML format</param>		
        /// <param name="emailAddress">Semi-Colon delimited string of "TO" email addresses.</param>
        /// <param name="fromMailAddress"></param>
        /// <param name="ccEmailAddress">Semi-Colon delimited string of "CC" email addresses.</param>
        /// <param name="bccEmailAddress">Semi-Colon delimited string of "BCC" email addresses.</param>
        /// <param name="attachmentFileNames">String array of fully qualified file names and path to the files.</param>
        /// <param name="priority">System.Web.Mail.MailPriority flag.</param>
        /// <param name="mailFormat">System.Web.Mail.MailFormat flag.</param>
        public static void SendMail(string SMTPserver,
                                        string subject,
                                        string messagebody,
                                        MailAddressCollection to,
                                        MailAddress from,
                                        MailAddressCollection cc,
                                        MailAddressCollection bcc,
                                        ArrayList attachmentFileNames,
                                        System.Net.Mail.MailPriority mp,
                                        bool IsBodyHtml)
        {

            MailMessage message = new MailMessage();

            try
            {
                message.From = from;

                message.Subject = subject;
                message.Body = messagebody;

                #region Add the TO

                if (to != null)
                {
                    foreach (MailAddress maTO in to)
                    {
                        message.To.Add(maTO);
                    }
                }

                #endregion

                #region Add the CC

                if (cc != null)
                {
                    foreach (MailAddress maCC in cc)
                    {
                        message.CC.Add(maCC);
                    }
                }

                #endregion

                #region Add the BCC

                if (bcc != null)
                {
                    foreach (MailAddress maBCC in bcc)
                    {
                        message.Bcc.Add(maBCC);
                    }
                }

                #endregion

                message.Priority = mp;

                message.IsBodyHtml = IsBodyHtml;

                #region Add the attachments, if any

                if (attachmentFileNames != null)
                {
                    if (attachmentFileNames.Count > 0)
                    {
                        foreach (string attachmentFileName in attachmentFileNames)
                        {
                            // check to see if the file exsits
                            if (System.IO.File.Exists(attachmentFileName))
                            {
                                Attachment data = new Attachment(attachmentFileName);
                                // Add time stamp information for the file.
                                //ContentDisposition disposition = data.ContentDisposition;
                                //disposition.CreationDate = System.IO.File.GetCreationTime(file);
                                //disposition.ModificationDate = System.IO.File.GetLastWriteTime(file);
                                //disposition.ReadDate = System.IO.File.GetLastAccessTime(file);
                                // Add the file attachment to this e-mail message.
                                message.Attachments.Add(data);

                            }
                            else
                            {
                                // Could not file the file
                                throw new ApplicationException("Could not find file for attachment: " + attachmentFileName);
                            }
                        }
                    }
                }

                #endregion

                // Set the client SMTP Server
                SmtpClient client = new SmtpClient(SMTPserver);

                //client.Credentials = CredentialCache.DefaultNetworkCredentials;
                client.Send(message);

            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {
                // Dispose of the message
                if (message != null)
                    message.Dispose();
            }

        }

        public static void SendMail(string SMTPserver,
                                string subject,
                                string messagebody,
                                string to,
                                string from,
                                string cc,
                                string bcc,
                                ArrayList attachmentFileNames,
                                System.Net.Mail.MailPriority mp,
                                bool IsBodyHtml)
        {


            try
            {
                MailAddress maFrom = new MailAddress(from);

                MailAddressCollection macTo = new MailAddressCollection();
                if (to != null)
                {
                    string[] tmpTo = to.Split(';');

                    foreach (string tmp in tmpTo)
                    {
                        if (tmp.Trim().Length > 0)
                            macTo.Add(tmp.Trim());
                    }
                }

                MailAddressCollection macCc = new MailAddressCollection();
                if (cc != null)
                {
                    string[] tmpCC = cc.Split(';');

                    foreach (string tmp in tmpCC)
                    {
                        if (tmp.Trim().Length > 0)
                            macTo.Add(tmp.Trim());
                    }
                }


                MailAddressCollection macBcc = new MailAddressCollection();
                if (bcc != null)
                {
                    string[] tmpBcc = bcc.Split(';');

                    foreach (string tmp in tmpBcc)
                    {
                        if (tmp.Trim().Length > 0)
                            macTo.Add(tmp.Trim());
                    }
                }

                SendMail(SMTPserver,
                                subject,
                                messagebody,
                                macTo,
                                maFrom,
                                macCc,
                                macBcc,
                                attachmentFileNames,
                                mp,
                                IsBodyHtml);


            }
            catch (Exception exc)
            {
                throw (exc);
            }
            finally
            {

            }

        }
    }
}
