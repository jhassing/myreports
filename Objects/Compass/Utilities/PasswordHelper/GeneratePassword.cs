using System;
using System.Security.Cryptography;

namespace Compass.Utilities.PasswordHelper
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class PasswordHelper
	{
		private const int saltLength = 4;

		#region GeneratePassword
		public static string GeneratePassword( int Length)
		{
			bool blnOnVowel = false;
			string strTempLetter;
			string strPassword = "";
			int intCount;

			for ( intCount = 1; intCount <= Length; intCount++)
			{
				if ( !blnOnVowel )
				{
					//Choose a nice consonant - no C, X, Z, or Q
					string tlist = "B,D,F,G,H,J,K,L,M,N,P,R,S,T,V,W,Y";
					string[] list = tlist.Split(',');
					strTempLetter = list[GetRandomNumber(0, list.Length-1)].ToString();
					//Append it to the password string
					strPassword += strTempLetter;
					//Swich to vowel mode
					blnOnVowel = true;
				}
				else
				{
					//Choose a vowel
					string tlist = "A,E,I,O,U";
					string[] list = tlist.Split(',');
					strTempLetter = list[GetRandomNumber(0, list.Length-1)].ToString();
					//Append it to the password string
					strPassword += strTempLetter;
					//Switch back again, ready for next loop round
					blnOnVowel = false; 
				}
			}
			return strPassword;
		}							

		#endregion

		#region CreateDbPassword
		// create salted password to save in Db
		public static byte [] CreateDbPassword(byte[] unsaltedPassword)
		{
			//Create a salt value
			byte[] saltValue = new byte[saltLength];
			RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
			rng.GetBytes(saltValue);
			
			return CreateSaltedPassword(saltValue, unsaltedPassword);
		}
		
		// create a salted password given the salt value
		private static byte[] CreateSaltedPassword(byte[] saltValue, byte[] unsaltedPassword)
		{
			// add the salt to the hash
			byte[] rawSalted  = new byte[unsaltedPassword.Length + saltValue.Length]; 
			unsaltedPassword.CopyTo(rawSalted,0);
			saltValue.CopyTo(rawSalted,unsaltedPassword.Length);
			
			//Create the salted hash			
			SHA1 sha1 = SHA1.Create();
			byte[] saltedPassword = sha1.ComputeHash(rawSalted);

			// add the salt value to the salted hash
			byte[] dbPassword  = new byte[saltedPassword.Length + saltValue.Length];
			saltedPassword.CopyTo(dbPassword,0);
			saltValue.CopyTo(dbPassword,saltedPassword.Length);

			return dbPassword;
		}

		#endregion
		
		#region ComparePasswords
		// compare the hashed password against the stored password
		private static bool ComparePasswords(byte[] storedPassword, byte[] hashedPassword)
		{
			if (storedPassword == null || hashedPassword == null || hashedPassword.Length != storedPassword.Length - saltLength)
				return false;

			// get the saved saltValue
			byte[] saltValue = new byte[saltLength];
			int saltOffset = storedPassword.Length - saltLength;
			for (int i = 0; i < saltLength; i++)
				saltValue[i] = storedPassword[saltOffset + i];

			byte[] saltedPassword = CreateSaltedPassword(saltValue, hashedPassword);
		
			// compare the values
			return CompareByteArray(storedPassword, saltedPassword);
		}

		#endregion
		
		#region Private Methods
		// compare the contents of two byte arrays
		private static bool CompareByteArray(byte[] array1, byte[] array2)
		{
			if (array1.Length != array2.Length)
				return false;
			for (int i = 0; i < array1.Length; i++)
			{
				if (array1[i] != array2[i])
					return false;
			}
			return true;
		}
        			
		private static int GetRandomNumber( int Low, int High)
		{
			// I should test these values to see if they are not negitive
			if ( ( Low >= 0 ) & ( High >=0 ) )
			{
				System.Random objRandom = new Random( Convert.ToInt32(System.DateTime.Now.Ticks) % System.Int32.MaxValue);
				return objRandom.Next(Low, High);
			}
			else
			{
				return 0;
			}
		}

		#endregion
	}
}
