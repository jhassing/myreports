using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Xml;
using Compass.Security.Cryptography;

namespace Compass.Configuration.AppSettings
{
    /// <summary>
    /// Class provides static methods for reading simple and advanced configuration files with or without encryption
    /// </summary>
    public class AppSettingsReader
    {
        #region SimpleKey

        /// <summary>
        /// Read a simple key, not encrypted and return the value.
        /// </summary>
        /// <param name="key">Key to be read.</param>
        /// <returns>Value of the Key</returns>
        public static string ReadKeyValue(string key)
        {
            return (ConfigurationSettings.AppSettings[key]);
        }

        /// <summary>
        /// Read a simple key, encrypted or not. Note: Uses simple encryption
        /// </summary>
        /// <param name="key">Key to be read</param>
        /// <param name="encrypted">Is the key Encrypted?</param>
        /// <returns>Value of the key, decrypted</returns>
        public static string ReadKeyValue(string key, bool encrypted)
        {
            if (!encrypted)
                return (ReadKeyValue(key));
            else
                return (ReadKeyValue(key, encrypted, false));
        }

        /// <summary>
        /// Read a simple key, encrypted or not. Note: Uses simple or complex user/machine store encryption
        /// </summary>
        /// <param name="key">Key to be read</param>
        /// <param name="encrypted">Is the key Encrypted?</param>
        /// <param name="simpleEncryption">Are we using symple or complex encryption?</param>
        /// <returns>Value of the key, decrypted</returns>
        public static string ReadKeyValue(string key, bool encrypted, bool simpleEncryption)
        {
            if (!encrypted)
                return (ReadKeyValue(key));
            else
                return (EncryptUtility.Decrypt(ReadKeyValue(key), simpleEncryption));
        }

        #endregion

        #region ComplexKeys

        /// <summary>
        /// Read a complex apllication configuration section and key. This overloaded method expects the key not to be encrypted.
        /// </summary>
        /// <param name="section">Section of the key.</param>
        /// <param name="key">key</param>
        /// <returns>Value of the key</returns>
        public static string ReadKeyValue(string section, string key)
        {
            NameValueCollection SimpleSettings1 = (NameValueCollection)ConfigurationSettings.GetConfig(section);
            return (SimpleSettings1.Get(key));
        }

        /// <summary>
        /// Read a complex apllication configuration section and key. This overloaded method expects the key to be encrypted with simple encryption or not encrypted at all.
        /// </summary>
        /// <param name="section">Section of the key.</param>
        /// <param name="key">key</param>
        /// <param name="encrypted">Is the key Encrypted?</param>
        /// <returns>Value of the key, decrypted</returns>
        public static string ReadKeyValue(string section, string key, bool encrypted)
        {
            if (!encrypted)
                return (ReadKeyValue(section, key));
            else
                return (ReadKeyValue(section, key, encrypted, false));
        }

        /// <summary>
        /// Read a complex apllication configuration section and key. This overloaded method expects the key to be encrypted with simple encryption  or complex encryption or non at all.	
        /// </summary>
        /// <param name="section">Section of the key.</param>
        /// <param name="key">key</param>
        /// <param name="encrypted">Is the key Encrypted?</param>
        /// <param name="simpleEncryption">Is the encryption simple or complex? User/Machine store??</param>
        /// <returns>Value of the key, decrypted</returns>
        public static string ReadKeyValue(string section, string key, bool encrypted, bool simpleEncryption)
        {
            if (!encrypted)
                return (ReadKeyValue(section, key));
            else
                return (EncryptUtility.Decrypt(ReadKeyValue(section, key), simpleEncryption));
        }

        /// <summary>
        /// Read a complex apllication configuration section and key from a specified XML config file. This overloaded method expects the key not to be encrypted.
        /// </summary>
        /// <param name="configFile">config file to read keys from</param>
        /// <param name="section">Section of the key.</param>
        /// <param name="key">key</param>
        /// <returns>Value of the key</returns>
        public static string ReadKeyValue(string configFile, string section, string key)
        {
            XmlDocument doc = new XmlDocument();

            try
            {
                doc.Load(configFile);
            }
            catch (Exception ex)
            {
                throw new Exception("Config File not found: " + configFile, ex);
            }

            if (section.StartsWith("/"))
                section = section.Substring(1, section.Length - 1);

            if (!section.StartsWith("/Configuration/"))
                section = "/configuration/" + section;

            XmlNodeList nodesSource = doc.SelectNodes(section);

            for (int x = 0; x < nodesSource.Count; x++)
            {
                for (int y = 0; y < nodesSource.Item(x).ChildNodes.Count; y++)
                {
                    string configFileKey = nodesSource.Item(x).ChildNodes.Item(y).Attributes["key"].Value;

                    if (key == configFileKey)
                    {
                        return (nodesSource.Item(x).ChildNodes.Item(y).Attributes["value"].Value);
                    }
                }
            }

            return "";

        }



        /// <summary>
        /// Read a complex apllication configuration section and key from a specified XML config file. This overloaded method expects the key to be encrypted with simple encryption or not encrypted at all.
        /// </summary>
        /// <param name="configFile">config file to read keys from</param>
        /// <param name="section">Section of the key.</param>
        /// <param name="key">key</param>
        /// <param name="encrypted">Is the key Encrypted?</param>
        /// <returns>Value of the key, decrypted</returns>
        public static string ReadKeyValue(string configFile, string section, string key, bool encrypted)
        {
            if (!encrypted)
                return (ReadKeyValue(configFile, section, key));
            else
                return (ReadKeyValue(configFile, section, key, encrypted, false));
        }


        /// <summary>
        /// Read a complex apllication configuration section and key from a specified XML config file. This overloaded method expects the key to be encrypted with simple encryption  or complex encryption or non at all.	
        /// </summary>
        /// <param name="section">Section of the key.</param>
        /// <param name="configFile">config file to read keys from</param>
        /// <param name="key">key</param>
        /// <param name="encrypted">Is the key Encrypted?</param>
        /// <param name="simpleEncryption">Is the encryption simple or complex? User/Machine store??</param>
        /// <returns>Value of the key, decrypted</returns>
        public static string ReadKeyValue(string configFile, string section, string key, bool encrypted, bool simpleEncryption)
        {
            if (!encrypted)
                return (ReadKeyValue(section, key));
            else
                return (EncryptUtility.Decrypt(ReadKeyValue(configFile, section, key), simpleEncryption));
        }


        #endregion
    }
}
