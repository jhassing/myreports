//===============================================================================
// XMenu
// http://www.ComputerBuff.com/Development/XMenu
//
// MenuItemsCollection.cs
// This file defines the MenuItemsCollection custom collection.
//===============================================================================
// Copyright (C) 2003-2004 Brian Buff
// All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================

using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Collections;

namespace ComputerBuff.Controls.XMenu
{
	/// <summary>
	///		MenuItemsCollection collection class
	/// </summary>
	[Serializable,
	DefaultProperty("Item")]
	public class MenuItemsCollection : CollectionBase
	{
		/// <summary>
		///		Create a new MenuItemsCollection collection instance
		/// </summary>
		public MenuItemsCollection()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		///		Inserts a MenuItem at the specified location.
		/// </summary>
		/// <param name="index">Index at which you want to insert</param>
		/// <param name="menuItem">MenuItem that you want to insert</param>
		public void Insert (int index, MenuItem menuItem)
		{
			this.List.Insert(index, menuItem);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="array">The one-dimensional Array that is the destination of the elements copied from ArrayList.
		/// The Array must have zero-based indexing. </param>
		/// <param name="index">The zero-based index in array at which copying begins.</param>
		public void CopyTo(MenuItem[] array, int index)
		{
			((ICollection)this).CopyTo(array, index);
		}

		/// <summary>
		///		Determines the index of the specified MenuItem.
		/// </summary>
		/// <param name="menuItem">MenuItem that you want to find the index of.</param>
		public int IndexOf(MenuItem menuItem)
		{
			return this.List.IndexOf(menuItem);
		}

		/// <summary>
		///		Determines if the specified MenuItem already exists.
		/// </summary>
		/// <param name="menuItem">MenuItem to locate</param>
		public bool Contains (MenuItem menuItem)
		{
			return this.List.Contains(menuItem);
		}

		/// <summary>
		///		Add a new MenuItem to the collection
		/// </summary>
		/// <param name="menuItem">MenuItem that you want to add</param>
		public void Add(MenuItem menuItem)
		{
			this.List.Add(menuItem);
		}

		/// <summary>
		///		Removes a MenuItem from the collection
		/// </summary>
		/// <param name="menuItem">MenuItem you want to remove</param>
		public void Remove(MenuItem menuItem)
		{
			this.List.Remove(menuItem);
		}

		/// <summary>
		///		Gets/Sets the MenuItem at the given index
		/// </summary>
		public MenuItem this[int index]
		{
			get
			{
				return (MenuItem)base.List[index];
			}

			set 
			{
				base.List[index] = value;
			}			
		}
	}
}
