//===============================================================================
// XMenu
// http://www.ComputerBuff.com/Development/XMenu
//
// MenuItem.cs
// This file defines the MenuItem class.
//===============================================================================
// Copyright (C) 2003-2004 Brian Buff
// All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================

using System;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;
using System.Drawing;
using System.Drawing.Design;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.Design;
using ComputerBuff.Controls.XMenu.Design;

namespace ComputerBuff.Controls.XMenu
{
	/// <summary>
	///		TargetType enumeration
	/// </summary>
	public enum TargetType
	{
		/// <summary>
		///		Blank
		/// </summary>
		Blank = 0,
		/// <summary>
		///		Parent
		/// </summary>
		Parent = 1,
		/// <summary>
		///		Self
		/// </summary>
		Self = 2,
		/// <summary>
		///		Top
		/// </summary>
		Top = 3,
		/// <summary>
		///		Main
		/// </summary>
		Main = 4,
	}

	/// <summary>
	///		MenuItem class
	/// </summary>
	[Serializable,
	TypeConverter(typeof(MenuItemTypeConverter)),
	DefaultProperty("ID")]
	public class MenuItem
	{
		private string				navigateUrl		= null;
		private string				text			= String.Empty;
		private bool				showNewImage	= false;
		private TargetType			urlTarget		= TargetType.Top;
		private string				cssClass		= String.Empty;
		private string				cssImageClass	= String.Empty;
		private string				menuId			= null;
		private FontInfo			font;
		private Color				foreColor		= Color.Black;
		private Color				backColor		= Color.White;
		private string				imageUrl		= null;
		private bool				visible			= true;
		private MenuItemsCollection	menuItems		= null;
		private State				menuItemState	= State.Collapsed;

		#region Properties
		/// <summary>
		///		Gets all a collection of all MenuItems contained within the menu.
		/// </summary>
		[Category("Item"),
		DefaultValue("Items to display within the menu"),
		PersistenceMode(PersistenceMode.InnerProperty),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[Editor(typeof(MenuItemCollectionEditor), 
			 typeof(System.Drawing.Design.UITypeEditor))]
		public MenuItemsCollection MenuItems
		{
			get
			{
				return this.menuItems;
			}
		}

		/// <summary>
		///		Gets/Sets the visible state of the menu.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Visual state of the menu item")]
		[NotifyParentProperty(true)]
		public State State
		{
			get
			{
				return this.menuItemState;
			}

			set
			{
				this.menuItemState = value;
			}
		}

		/// <summary>
		///		Font property
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Font used for the menu item"),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		public FontInfo Font
		{
			get
			{
				return this.font;
			}

			set
			{
				this.font = value;
			}
		}

		/// <summary>
		///		BackColor property
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Color used for the menu item background"),
		DefaultValue(typeof(Color), "Color.White")]
		[NotifyParentProperty(true)]
		public Color BackColor
		{
			get
			{
				return this.backColor;
			}

			set
			{
				this.backColor = value;
			}
		}

		/// <summary>
		///		ForeColor property
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Color used for the menu item text"),
		DefaultValue(typeof(Color), "Color.Black")]
		[NotifyParentProperty(true)]
		public Color ForeColor
		{
			get
			{
				return this.foreColor;
			}

			set
			{
				this.foreColor = value;
			}
		}

		/// <summary>
		///		Programmatic id of the menu item
		/// </summary>
		[ParenthesizePropertyName(true),
		DefaultValue("")]
		[NotifyParentProperty(true)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Description("Internal use only - DO NOT MODIFY")]
		public string MenuId
		{
			get
			{
				return this.menuId;
			}

			set 
			{
				this.menuId = value;
			}
		}

		/// <summary>
		///		Indicates whether the menu item is visible and rendered.
		/// </summary>
		[Browsable(true),
		Category("Behavior"),
		Description("Indicates whether the menu item is visible and rendered.")]
		[NotifyParentProperty(true)]
		public bool Visible
		{
			get
			{
				return this.visible;
			}

			set 
			{
				this.visible = value;
			}
		}

		/// <summary>
		///		URL of the menu item
		/// </summary>
		//Editor(typeof(UrlEditor), typeof(UITypeEditor))
		[Browsable(true),
		Category("Navigation"),
		Description("URL of the menu item")]
		[NotifyParentProperty(true)]
		public string NavigateUrl
		{
			get
			{
				return this.navigateUrl;
			}

			set
			{
				this.navigateUrl = value;
			}
		}

		/// <summary>
		///		URL of the menu item
		/// </summary>
		//Editor(typeof(ImageUrlEditor), typeof(UITypeEditor))
		[Browsable(true),
		Category("Appearance"),
		Description("URL of the image for the menu item")]
		[NotifyParentProperty(true)]
		public string ImageUrl
		{
			get
			{
				return this.imageUrl;
			}

			set
			{
				this.imageUrl = value;
			}
		}

		/// <summary>
		///		Text of the menu item
		/// </summary>
		[Browsable(true),
		Category("Appearance"),
		Description("Text of the menu item")]
		[NotifyParentProperty(true)]
		public string Text
		{
			get
			{
				return this.text;
			}

			set
			{
				this.text = value;
			}
		}

		/// <summary>
		///		Text of the menu item
		/// </summary>
		[Browsable(true),
		Category("Appearance"),
		Description("CSS Class name to be applied to the menu item"),
		DefaultValue("")]
		[NotifyParentProperty(true)]
		public string CssClass
		{
			get
			{
				return this.cssClass;
			}

			set
			{
				this.cssClass = value;
			}
		}

		[Browsable(true),
		Category("Appearance"),
		Description("CSS Class name to be applied to the menu item image"),
		DefaultValue("")]
		[NotifyParentProperty(true)]
		public string CssImageClass
		{
			get
			{
				return this.cssImageClass;
			}

			set
			{
				this.cssImageClass = value;
			}
		}
		

		/// <summary>
		///		Indicates whether to show the 'New' image next to menu item
		/// </summary>
		[Browsable(true),
		Category("Appearance"),
		Description("Indicates whether to show the 'New' image next to menu item")]
		[NotifyParentProperty(true)]
		public bool ShowNewImage
		{
			get
			{
				return this.showNewImage;
			}

			set
			{
				this.showNewImage = value;
			}
		}

		/// <summary>
		///		Window target of the menu item
		/// </summary>
		[Browsable(true),
		Category("Navigation"),
		Description("Window target of the menu item")]
		[NotifyParentProperty(true)]
		public TargetType Target
		{
			get
			{
				return this.urlTarget;
			}

			set
			{
				this.urlTarget = value;
			}
		}
		#endregion Properties

		#region Methods
		/// <summary>
		///		Custom ToString
		/// </summary>
		public override string ToString()
		{
			return "Menu Item";
		}

		/// <summary>
		///		Creates a new menu item
		/// </summary>
		public MenuItem()
		{
			this.font = new Style().Font;
			this.menuItems = new MenuItemsCollection();

			if (this.menuId == null || this.menuId.Length == 0)
			{
				this.menuId = Guid.NewGuid().ToString();
				this.menuId = this.menuId.Substring(1, this.menuId.Length - 1).Replace("-", "");
			}
		}

		/// <summary>
		///		Creates a new menu item
		/// </summary>
		/// <param name="text">Text of the menu item</param>
		/// <param name="navigateUrl">URL of the menu item</param>
		/// <param name="urlTarget">Target of the menu item</param>
		public MenuItem(string text, string navigateUrl, TargetType urlTarget)
		{
			this.text = text;
			
			if (navigateUrl.Trim().Length > 0)
			{
				this.Target = urlTarget;
				this.navigateUrl = navigateUrl;
			}

			this.font = new Style().Font;
			this.menuItems = new MenuItemsCollection();

			if (this.menuId == null || this.menuId.Length == 0)
			{
				this.menuId = Guid.NewGuid().ToString();
				this.menuId = this.menuId.Substring(1, this.menuId.Length - 1).Replace("-", "");
			}
		}
		#endregion Methods
	}

	#region MenuItemTypeConverter
	internal class MenuItemTypeConverter : ExpandableObjectConverter
	{
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof(InstanceDescriptor))
			{
				return true;
			} 
			
			return base.CanConvertTo(context, destinationType);
		}
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, 
											object value, Type destinationType)
		{
			if (destinationType == typeof(InstanceDescriptor))
			{
				ConstructorInfo ci = typeof(MenuItem).GetConstructor(
						new Type[] {typeof(string), 
									typeof(string), 
									typeof(TargetType)});

				MenuItem menuItem = (MenuItem) value;
				return new InstanceDescriptor(ci, 
												new object[] {	menuItem.Text, 
																menuItem.NavigateUrl,
																menuItem.Target}, true);
			} 
			
			return base.ConvertTo(context, culture, value, destinationType);
		}
	}
	#endregion MenuItemTypeConverter
}
