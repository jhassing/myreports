//===============================================================================
// XMenu
// http://www.ComputerBuff.com/Development/XMenu
//
// XMenu.cs
// This file defines the core functionality of XMenu.
//===============================================================================
// Copyright (C) 2003-2004 Brian Buff
// All rights reserved.
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY
// OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT
// LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR
// FITNESS FOR A PARTICULAR PURPOSE.
//===============================================================================

using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Collections;
using System.Collections.Specialized;
using System.Web.UI.Design;
using System.Drawing;
using System.Drawing.Design;
using ComputerBuff.Controls.XMenu.Design;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using System.Security;
using System.Security.Permissions;

namespace ComputerBuff.Controls.XMenu
{
	/// <summary>
	///		State Enumeration
	/// </summary>
	public enum State
	{
		/// <summary>
		///		Collapsed Visible State
		/// </summary>
		Collapsed,
		/// <summary>
		///		Expanded Visible State
		/// </summary>
		Expanded
	}

	/// <summary>
	///		MenuItemState Enumeration
	/// </summary>
	public enum MenuItemState
	{
		/// <summary>
		///		CollapseAll Visible State
		/// </summary>
		CollapseAll,
		/// <summary>
		///		ExpandAll Visible State
		/// </summary>
		ExpandAll
	}

	/// <summary>
	///		XMenu menu control class
	/// </summary>
	//ToolboxBitmap(typeof(Bitmap), "NeptuneTab"),
	[DefaultProperty("HeaderText")]
	[ToolboxBitmap(typeof(XMenu))]
	[ToolboxData("<{0}:XMenu runat=server></{0}:XMenu>")]
	[ParseChildren(false)]
	[PersistChildren(false)]
	[ControlBuilder(typeof(Design.MenuItemBuilder))]
	[Designer(typeof(Design.XMenuDesigner))]
	public class XMenu : WebControl, INamingContainer, IComponent
	{
		#region Constants
		const string javaScriptVoid = "javascript:void(0)";
		#endregion Constants

		#region Fields
		private string				menuCollapseImageUrl	= null;
		private string				menuExpandImageUrl		= null;
		private string				submenuExpandImageUrl	= null;
		private string				submenuCollapseImageUrl = null;
		private string				newImageUrl				= null;
		private string				headerImageUrl			= null;
		private string				headerText;
		private string				headerTextCssClass		= "";
		private Unit				width					= 184;
		private MenuItemsCollection menuItems				= null;
		private Color				headerBackColor			= ColorTranslator.FromHtml("#6699cc");
		private Color				headerForeColor			= Color.White;
		private Color				borderColor				= Color.Black;
		private FontInfo			headerFont;
		private BorderStyle			borderStyle;
		private Unit				borderWidth;
		private string				groupName				= null;
		private State				state					= State.Expanded;
		private MenuItemState		menuItemState			= MenuItemState.CollapseAll;
		private object				dataSource				= null;
		private string				scriptPath				= null;
		private CultureInfo			currentUICulture		= null;
		#endregion Fields

		#region Properties
		/// <summary>
		///		Gets all a collection of all MenuItems contained within the menu.
		/// </summary>
		[Category("Item"),
		DefaultValue("Items to display within the menu"),
		PersistenceMode(PersistenceMode.InnerDefaultProperty),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
		NotifyParentProperty(true),
		Editor(typeof(MenuItemCollectionEditor), 
			 typeof(System.Drawing.Design.UITypeEditor))]
		public MenuItemsCollection MenuItems
		{
			get
			{
				return this.menuItems;
			}
		}

		/// <summary>
		///		The datasource used to populate the menu with menu items.  This can be an XmlDocument or a path to an XML file.
		/// </summary>
		[Bindable(true),
		Category("Data"),
		DefaultValue(null),
		Description("The data source used to build the menu."),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public object DataSource
		{
			get
			{
				return this.dataSource;
			}
			set
			{
				if (value is string || value is XmlDocument)
					this.dataSource = value;
				else
					throw new ArgumentException("DataSource must be a string or XmlDocument instance.");
			}
		}

		/// <summary>
		///		Image to display for the Show link of the menu.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Url for the submenu expand image"),
		Editor(typeof(ImageUrlEditor), typeof(UITypeEditor)),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public string SubmenuExpandImageUrl
		{
			get
			{
				return this.submenuExpandImageUrl;
			}

			set
			{
				this.submenuExpandImageUrl = value;
			}
		}

		/// <summary>
		///		Image to display for the Hide link of the menu.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Url for the submenu collapse image"),
		Editor(typeof(ImageUrlEditor), typeof(UITypeEditor)),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public string SubmenuCollapseImageUrl
		{
			get
			{
				return this.submenuCollapseImageUrl;
			}

			set
			{
				this.submenuCollapseImageUrl = value;
			}
		}

		/// <summary>
		///		Header text of the menu.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Menu header text"),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public string HeaderText
		{
			get
			{
				return headerText;
			}

			set
			{
				headerText = value;
			}
		}

		/// <summary>
		///		Header test stylesheet
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Menu header Style Sheet"),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public string HeaderTextCssClass
		{
			get
			{
				return headerTextCssClass;
			}

			set
			{
				headerTextCssClass = value;
			}
		}
		

		/// <summary>
		///		Gets/Sets the visible state of the menu.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Visual state of the menu"),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public State State
		{
			get
			{
				return this.state;
			}

			set
			{
				this.state = value;
			}
		}

		/// <summary>
		///		Gets/Sets the visible state of the menu items.  Individual menu items can override this setting via
		///		their State	property
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Visual state of the menu items.  CollapseAll = All menu items are collapsed.  ExpandAll = All menu items are expanded."),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public MenuItemState MenuItemState
		{
			get
			{
				return this.menuItemState;
			}

			set
			{
				this.menuItemState = value;

				foreach (MenuItem menuItem in this.MenuItems)
					this.UpdateMenuItemState(menuItem);
			}
		}

		/// <summary>
		///		Width of the menu.
		/// </summary>
		[Bindable(true),
		Category("Layout"),
		Description("Width of the control."),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public override Unit Width
		{
			get
			{
				return this.width;
			}

			set
			{	
				this.width = value;
			}
		}

		/// <summary>
		///		Image to display for the Hide link of the menu.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Url for the hide image"),
		Editor(typeof(ImageUrlEditor), typeof(UITypeEditor)),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public string MenuCollapseImageUrl
		{
			get
			{
				return this.menuCollapseImageUrl;
			}

			set
			{
				this.menuCollapseImageUrl = value;
			}
		}

		/// <summary>
		///		Image to display for the header image to the left of the menu text.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Url for the header image to the left of the menu text"),
		Editor(typeof(ImageUrlEditor), typeof(UITypeEditor)),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public string HeaderImageUrl
		{
			get
			{
				return this.headerImageUrl;
			}

			set
			{
				this.headerImageUrl = value;
			}
		}

		/// <summary>
		///		Image to display for the Show link of the menu.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Url for the show image"),
		Editor(typeof(ImageUrlEditor), typeof(UITypeEditor)),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public string MenuExpandImageUrl
		{
			get
			{
				return this.menuExpandImageUrl;
			}

			set
			{
				this.menuExpandImageUrl = value;
			}
		}

		/// <summary>
		///		Image to use for a "New" menu item.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Url for the image to display for menu items designated as 'New'"),
		Editor(typeof(ImageUrlEditor), typeof(UITypeEditor)),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public string NewImageUrl
		{
			get
			{
				return this.newImageUrl;
			}

			set
			{
				this.newImageUrl = value;
			}
		}

		/// <summary>
		///		Height property
		/// </summary>
		/// <remarks>Hide from propery explorer</remarks>
		[Browsable(false)]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				base.Height = value;
			}
		}

		/// <summary>
		///		CssClass property
		/// </summary>
		/// <remarks>Hide from propery explorer</remarks>
		[Browsable(false)]
		public override string CssClass
		{
			get
			{
				return base.CssClass;
			}
			set
			{
				base.CssClass = value;
			}
		}


		/// <summary>
		///		ForeColor property
		/// </summary>
		/// <remarks>Hide from property explorer</remarks>
		[Browsable(false)]
		public override Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		/// <summary>
		///		Font propery
		/// </summary>
		/// <remarks>Hide from property explorer</remarks>
		[Browsable(false)]
		public override FontInfo Font
		{
			get
			{
				return base.Font;
			}
		}

		/// <summary>
		///		BackColor property
		/// </summary>
		/// <remarks>Hide from property explorer</remarks>
		[Browsable(false)]
		public override Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		/// <summary>
		///		Header background color.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Color used for the menu header background"),
		DefaultValue("#6699cc"),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public Color HeaderBackColor
		{
			get
			{
				return this.headerBackColor;
			}

			set
			{
				this.headerBackColor = value;
			}
		}

		/// <summary>
		///		Path to the external XMenu javascript.
		/// </summary>
		[PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
		NotifyParentProperty(true),
		Description("Specifies external path and filename for javascript file."),
		DefaultValue(""),
		EditorAttribute(typeof(UrlEditor), typeof(UITypeEditor))]
		public string ScriptPath
		{
			get
			{
				return this.scriptPath;
			}
			set
			{
				this.scriptPath = value;
			}
		}

		/// <summary>
		///		Font used for the header text.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Font used for the menu header"),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
		NotifyParentProperty(true)]
		public FontInfo HeaderFont
		{
			get
			{
				if (this.headerFont == null)
					this.headerFont = base.Font;

				return this.headerFont;
			}

			set
			{
				this.headerFont = value;
			}
		}

		/// <summary>
		///		Header color of the menu.
		/// </summary>
		[Bindable(true),
		Category("Appearance"),
		Description("Color used for the menu header text"),
		DefaultValue(typeof(Color), "Color.White"),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public Color HeaderForeColor
		{
			get
			{
				return this.headerForeColor;
			}

			set
			{
				this.headerForeColor = value;
			}
		}

		/// <summary>
		///		Border color of the menu.
		/// </summary>
		[Browsable(true),
		Category("Appearance"),
		Description("Color of the border of the menu"),
		DefaultValue(typeof(Color), "Color.Black"),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public override Color BorderColor
		{
			get
			{
				return this.borderColor;
			}

			set
			{
				this.borderColor = value;
			}
		}	

		/// <summary>
		///		Border style of the menu.
		/// </summary>
		[Browsable(true),
		Category("Appearance"),
		Description("Style of the menu border"),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public override BorderStyle BorderStyle
		{
			get
			{
				return this.borderStyle;
			}

			set
			{
				this.borderStyle = value;
			}
		}

		/// <summary>
		///		Border width of the menu.
		/// </summary>
		[Browsable(true),
		Category("Appearance"),
		Description("Width of the menu border"),
		DefaultValue(typeof(Unit),"Unit.Empty"),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public override Unit BorderWidth
		{
			get
			{
				return this.borderWidth;
			}

			set
			{
				this.borderWidth = value;
			}
		}

		/// <summary>
		///		Group to which this menu belongs to.  The makes all panels in the same group mutually exclusive.
		/// </summary>
		[Browsable(true),
		Category("Behavior"),
		Description("Group that this XMenu belongs to"),
		DefaultValue(typeof(Unit),"Unit.Empty"),
		NotifyParentProperty(true),
		PersistenceMode(PersistenceMode.Attribute),
		DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public string GroupName
		{
			get
			{
				return this.groupName;
			}

			set
			{
				this.groupName = value;
			}
		}
		#endregion Properties

		#region Methods
		/// <summary>
		///		Creates a new XMenu object
		/// </summary>
		public XMenu()
		{
			this.menuItems = new MenuItemsCollection();

			//if (HttpContext.Current != null)
			//	this.currentUICulture = CultureInfo.CreateSpecificCulture(Context.Request.UserLanguages[0]);
			//else
			//	this.currentUICulture = CultureInfo.CurrentUICulture;
		}

		/// <summary>
		/// Event handler for the DataBinding event.
		/// </summary>
		/// <remarks>This method runs when the DataBind() method is called.  Essentially, it clears out the
		/// current state and builds up the menu from the specified <see cref="DataSource"/>.
		/// </remarks>
		protected override void OnDataBinding(EventArgs e)
		{
			// Start by resetting the Control's state
			if (HasChildViewState)
				ClearChildViewState();

			// load the datasource either as a string or XmlDocuemnt
			XmlDocument xmlDoc = new XmlDocument();

			if (this.DataSource is String)
				// Load the XML document specified by DataSource as a filepath
				xmlDoc.Load((string) this.DataSource);
			else if (this.DataSource is XmlDocument)
				xmlDoc = (XmlDocument) DataSource;
			else
				return; // exit - nothing to databind

			// Clear out the MenuItems and build them according to the XmlDocument
			this.menuItems.Clear();
			this.menuItems = ReadDataSourceMenuItems(xmlDoc.SelectSingleNode("/Menu"));

			this.CreateChildControls();

			this.ChildControlsCreated = true;

			if (!IsTrackingViewState)
				TrackViewState();
		}

		/// <summary>
		/// This method is used from the OnDataBinding method; it traverses the XML document,
		/// building up the object model.
		/// </summary>
		/// <param name="itemsNode">The current menuItem XmlNode</param>
		/// <returns>A set of MenuItems for this menu.</returns>
		private MenuItemsCollection ReadDataSourceMenuItems(XmlNode itemsNode)
		{
			// Make sure we have an XmlNode instance - it should never be null, else the
			// XML document does not have the expected structure
			if (itemsNode == null)
				throw new ArgumentException("The XML data for the Menu control is in an invalid format.");

			MenuItemsCollection items = new MenuItemsCollection();
//			if (IsTrackingViewState)
//				((IStateManager) items).TrackViewState();

			// iterate through each MenuItem
			XmlNodeList menuItems = itemsNode.ChildNodes;
			for (int i = 0; i < menuItems.Count; i++)
			{
				XmlNode menuItem = menuItems[i];

				// Create the menuitem
				if (menuItem.Name.ToLower(this.currentUICulture) == "menuitem")
					items.Add(BuildMenuItem(menuItem));
			}

			return items;
		}

		/// <summary>
		/// This method creates a single <see cref="MenuItem"/> and is called repeatedly from <see cref="ReadDataSourceMenuItems"/>.
		/// </summary>
		/// <param name="menuItem">The MenuItem XmlNode.</param>
		/// <returns>A new <see cref="MenuItem"/> instance.</returns>
		private MenuItem BuildMenuItem(XmlNode menuItem)
		{
			MenuItem mi = new MenuItem();
			//			if (IsTrackingViewState)
			//				((IStateManager) mi).TrackViewState();

			XmlNode textTextNode = menuItem.Attributes["Text"];
			XmlNode backColorTextNode = menuItem.Attributes["BackColor"];
			XmlNode targetTextNode = menuItem.Attributes["Target"];
			XmlNode cssClassTextNode = menuItem.Attributes["CssClass"];
			XmlNode navigateUrlTextNode = menuItem.Attributes["NavigateUrl"];
			XmlNode visibleTextNode = menuItem.Attributes["Visible"];
			XmlNode imageUrlTextNode = menuItem.Attributes["ImageUrl"];
			XmlNode showNewImageTextNode = menuItem.Attributes["ShowNewImage"];
			XmlNode foreColorTextNode = menuItem.Attributes["ForeColor"];
			XmlNode stateTextNode = menuItem.Attributes["State"];

			if (textTextNode == null)
				throw new ArgumentException("The XML data for the Menu control is in an invalid format: missing the <text> elements for a <MenuItem>.");

			if (textTextNode != null)
				mi.Text = textTextNode.Value;

			if (backColorTextNode != null)
				mi.BackColor = ColorTranslator.FromHtml(backColorTextNode.Value);

			if (targetTextNode != null)
				switch (targetTextNode.Value.ToUpper(this.currentUICulture))
				{
					case "_BLANK":
						mi.Target = TargetType.Blank;
						break;

					case "_PARENT":
						mi.Target = TargetType.Parent;
						break;

					case "_SELF":
						mi.Target = TargetType.Self;
						break;

					case "_TOP":
						mi.Target = TargetType.Top;
						break;

					case "MAIN":
						mi.Target = TargetType.Main;
						break;
				}

			if (cssClassTextNode != null)
				mi.CssClass = cssClassTextNode.Value;

			if (navigateUrlTextNode != null)
				mi.NavigateUrl = navigateUrlTextNode.Value;

			if (visibleTextNode != null)
				mi.Visible = Convert.ToBoolean(visibleTextNode.Value, this.currentUICulture);

			if (imageUrlTextNode != null)
				mi.ImageUrl = imageUrlTextNode.Value;

			if (showNewImageTextNode != null)
				mi.ShowNewImage = Convert.ToBoolean(showNewImageTextNode.Value, this.currentUICulture);

			if (foreColorTextNode != null)
				mi.ForeColor = ColorTranslator.FromHtml(foreColorTextNode.Value);
            
			if (stateTextNode != null)
				switch (stateTextNode.Value.ToUpper(this.currentUICulture))
				{
					case "COLLAPSED":
						mi.State = State.Collapsed; 
						break;
					case "EXPANDED":
						mi.State = State.Expanded;
						break;
				}

			// see if there is a submenu
			XmlNode subMenu = menuItem.SelectSingleNode("MenuItems");
			if (subMenu != null)
			{
				XmlNodeList menuItems = subMenu.SelectNodes("MenuItem");
				for (int i = 0; i < menuItems.Count; i++)
				{
					XmlNode item = menuItems[i];

					// Create the menuitem
					if (item.Name.ToLower(this.currentUICulture) == "menuitem")
						mi.MenuItems.Add(BuildMenuItem(item));
				}
			}

			return mi;
		}

		/// <summary>
		/// This member overrides Control.Render.
		/// </summary>
		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			if (IsDesignTime)
			{
				// these two call's are needed to show the control at design time
				ChildControlsCreated = false;
				EnsureChildControls();
			}

			// delegate the actual rendering to baseclass
			base.Render(writer); 
		}

		/// <summary>
		///		Adds a new menu item to the menu
		/// </summary>
		/// <param name="text">Text of the menu item</param>
		/// <param name="url">Url of the menu item</param>
		/// <param name="urlTarget">Target of the menu item</param>
		public void AddMenuItem(string text, string url, TargetType urlTarget)
		{
			this.MenuItems.Add(new MenuItem(text, url, urlTarget));
		}

		/// <summary>
		/// This member overrides Control.AddParsedSubObject.
		/// </summary>
		protected override void AddParsedSubObject(object obj)
		{
			MenuItem menuItem = obj as MenuItem;			
			if (menuItem != null)
			{
				this.MenuItems.Add(menuItem);
				return;
			} 
			
			base.AddParsedSubObject(obj); 
		}

		/// <summary>
		///		Overrides the CreateChildControls function of the WebControl base class
		/// </summary>
		protected override void CreateChildControls()
		{	
			// call base method
			base.CreateChildControls();

			HtmlTableRow tr = null;
			HtmlTableCell tc = null;
			HtmlTable menuItemTable = null;
			HtmlTable navTable = null;
			string panelID = "SectionPanel;" + this.GroupName + ";" + this.MenuExpandImageUrl;
			string headerText = this.HeaderText;

			if ((headerText == null || headerText.Length == 0) && IsDesignTime)
				headerText = "[" + this.ID + "]";

			navTable = new HtmlTable();
			navTable.CellPadding = 0;
			navTable.CellSpacing = 0;
			navTable.Width = this.Width.ToString(this.currentUICulture);
			navTable.Border = 0;
			navTable.ID = "MenuSection";

			//Create menu item table
			menuItemTable = new HtmlTable();
			menuItemTable.ID = panelID;
			menuItemTable.Border = 0;
			menuItemTable.CellPadding = 0;
			menuItemTable.CellSpacing = 0;
			menuItemTable.Width = this.Width.ToString(this.currentUICulture);

			if (this.BorderStyle != BorderStyle.None && this.BorderStyle != BorderStyle.NotSet)
			{
				navTable.Attributes.Add("style", "BORDER-RIGHT: " + this.BorderWidth.Value + "px " + this.BorderStyle.ToString(this.currentUICulture) + 
					"; BORDER-TOP: " + this.BorderWidth.Value + "px " + this.BorderStyle.ToString(this.currentUICulture) +
					"; BORDER-LEFT: " + this.BorderWidth.Value + "px " + this.BorderStyle.ToString(this.currentUICulture) +
					"; BORDER-BOTTOM: " + this.BorderWidth.Value + "px " + this.BorderStyle.ToString(this.currentUICulture));
				navTable.BorderColor = ColorTranslator.ToHtml(this.BorderColor);
			}

			//Start Header Row
			tr = new HtmlTableRow();
			navTable.Rows.Add(tr);
			tr.Attributes.Add("onmouseover", "HeaderOver(this);");
			tr.Attributes.Add("onmouseout", "HeaderOut(this);");

			//Add image (if needed)
			if (this.HeaderImageUrl != null && this.HeaderImageUrl.Length > 0)
			{
				tc = new HtmlTableCell();
				tc.Width = "24";				

				if (this.CssClass.Trim().Length == 0)
				{
					
					tc.BgColor = ColorTranslator.ToHtml(this.HeaderBackColor);
					tc.NoWrap = true;
				}
				else
				{
					tc.Attributes.Add("class",this.CssClass);
				}

				
				HyperLink lnk = new HyperLink();
				
				lnk.Text = "<img src=\"" + this.HeaderImageUrl + "\">";
				lnk.Attributes["href"] = XMenu.javaScriptVoid;

				lnk.Attributes["onclick"] = "expandit('" + this.ClientID + "_" + menuItemTable.ClientID + "', '" + this.MenuCollapseImageUrl + 
					"', '" + this.MenuExpandImageUrl + "', '" + this.GroupName + "')";				

				tc.Controls.Add(lnk);

				tr.Cells.Add(tc);
			}

			tc = new HtmlTableCell();
			tc.Width = "140";
			tc.Height = "19";
			if (this.CssClass.Trim().Length == 0)
			{
				tc.BgColor = ColorTranslator.ToHtml(this.HeaderBackColor);
				tc.NoWrap = true;
			}
			else
			{
				tc.Attributes.Add("class",this.CssClass);
			}

			HyperLink lnkHeader = new HyperLink();
			lnkHeader.Width = Unit.Empty;
			lnkHeader.EnableViewState = false;
			lnkHeader.Text = "&nbsp;" + headerText;
			lnkHeader.CssClass = this.HeaderTextCssClass;
			if (this.CssClass.Trim().Length == 0)
			{
				lnkHeader.ForeColor = this.HeaderForeColor;
				lnkHeader.Font.Bold  = this.HeaderFont.Bold;
				lnkHeader.Font.Italic = this.HeaderFont.Italic;
				lnkHeader.Font.Name = this.HeaderFont.Name;
				lnkHeader.Font.Size = this.HeaderFont.Size;
				lnkHeader.Font.Strikeout = this.HeaderFont.Strikeout;
				lnkHeader.Font.Underline = this.HeaderFont.Underline;
			}
			else
			{
				lnkHeader.Attributes.Add("class",this.CssClass);
			}

			lnkHeader.Attributes["href"] = XMenu.javaScriptVoid;
			lnkHeader.Style.Add("text-decoration", "none");
			lnkHeader.Attributes["onclick"] = "expandit('" + this.ClientID + "_" + menuItemTable.ClientID + "', '" + this.MenuCollapseImageUrl + 
												"', '" + this.MenuExpandImageUrl + "', '" + this.GroupName + "')";
			tc.Controls.Add(lnkHeader);
			tr.Cells.Add(tc);

			tc = new HtmlTableCell();
			//tc.Width = "5%";
			tc.Width = "16";
			tc.Height = "19";
			tc.Align = "Right";
			if (this.CssClass.Trim().Length == 0)
			{
				tc.BgColor = ColorTranslator.ToHtml(this.HeaderBackColor);
				tc.NoWrap = true;
			}
			else
			{
				tc.Attributes.Add("class",this.CssClass);
			}

			if (this.MenuCollapseImageUrl != null && this.MenuCollapseImageUrl.Length > 0)
			{
				System.Web.UI.WebControls.Image sectionHeaderArrowImage = new System.Web.UI.WebControls.Image();
				sectionHeaderArrowImage.ID = "ArrowImage";

				if (this.State == State.Expanded)
					sectionHeaderArrowImage.ImageUrl = this.MenuCollapseImageUrl;
				else
					sectionHeaderArrowImage.ImageUrl = this.MenuExpandImageUrl;

				HyperLink sectionHeaderLink = new HyperLink();
				sectionHeaderLink.Attributes["href"] = XMenu.javaScriptVoid;
				sectionHeaderLink.Attributes["onClick"] = "expandit('" + this.ClientID + "_" + menuItemTable.ClientID + "', '" + this.MenuCollapseImageUrl + "', '" + this.MenuExpandImageUrl + "', '" + this.GroupName + "')";
			
				//add image to link
				sectionHeaderLink.Controls.Add(sectionHeaderArrowImage);
				tc.Controls.Add(sectionHeaderLink);
			}

			tr.Cells.Add(tc);
			//End Header Row

			//Start Menu Items Row
			tr = new HtmlTableRow();
			navTable.Rows.Add(tr);

			tc = new HtmlTableCell();
			tc.ColSpan = 3;
	
			tc.Height = "1px";
			tc.Width = this.Width.ToString(this.currentUICulture);

			if (!IsDesignTime)
			{
				if (this.State == State.Expanded)
					menuItemTable.Attributes["style"] = "display:inline";
				else
					menuItemTable.Attributes["style"] = "display:none";
			}

			tc.Controls.Add(menuItemTable);
			tr.Cells.Add(tc);

			//Menu Items
			if (this.menuItems != null)
				this.CreateRootMenuItems(menuItemTable);

			//Add table to the page
			this.Controls.Add(navTable);

			if (!this.IsDesignTime && this.ScriptPath != null && this.ScriptPath.Length > 0)
			{
				if (!this.Page.IsClientScriptBlockRegistered("RuntimeScript"))
					this.Page.RegisterClientScriptBlock("RuntimeScript", "<script language=\"javascript\" src=\"" + this.ResolveUrl(this.ScriptPath) + "\"></script>");
			}
		}

		/// <summary>
		///		Updates the menu item state property to that of the menu's MenuItemState
		/// </summary>
		/// <param name="menuItem">MenuItem to update</param>
		private void UpdateMenuItemState(MenuItem menuItem)
		{
			if (this.MenuItemState == MenuItemState.CollapseAll)
				menuItem.State = State.Collapsed;
			else
				menuItem.State = State.Expanded;

			foreach (MenuItem item in menuItem.MenuItems)
				UpdateMenuItemState(item);
		}

		/// <summary>
		///		Handles the creation of root menu items.
		/// </summary>
		/// <param name="menuItemTable">Table in which to build the root menu items.</param>
		private void CreateRootMenuItems(HtmlTable menuItemTable)
		{
			HtmlTableRow tr = null;
			int depth = 0;

			//Menu Items
			foreach(MenuItem menuItem in this.menuItems)
			{
				tr = new HtmlTableRow();
				menuItemTable.Rows.Add(tr);

				this.BuildMenuItem(menuItem, tr, depth);

				if (menuItem.MenuItems != null && menuItem.MenuItems.Count > 0)
					this.CreateSubMenuItems(menuItemTable, menuItem, depth);
			}
		}

		/// <summary>
		///		Handles the creation of sub menu items.
		/// </summary>
		/// <param name="parentMenuItemTable">The table in which to create the sub menu items.</param>
		/// <param name="parentMenuItem">Parent menu item.</param>
		/// <param name="depth">Depth of the current menu item.</param>
		private void CreateSubMenuItems(HtmlTable parentMenuItemTable, MenuItem parentMenuItem, int depth)
		{
			HtmlTableRow tr = null;
			HtmlTableRow parentRow = null;
			HtmlTableCell tc = null;
			HtmlTable menuItemTable;

			depth++;

			//Create menu item table
			menuItemTable = new HtmlTable();
			menuItemTable.ID = parentMenuItem.MenuId + "_SubMenu";
			menuItemTable.Border = 0;
			menuItemTable.CellPadding = 0;
			menuItemTable.CellSpacing = 0;
			menuItemTable.Width = "100%";

			if (!IsDesignTime)
			{	
				if (this.MenuItemState == MenuItemState.CollapseAll || parentMenuItem.State == State.Collapsed)
					menuItemTable.Attributes["style"] = "display:none";
				
				if (parentMenuItem.State == State.Expanded)
					menuItemTable.Attributes["style"] = "display:inline";
			}

			//Menu Items
			foreach(MenuItem menuItem in parentMenuItem.MenuItems)
			{
				if (menuItem.Visible)
				{
					parentRow = new HtmlTableRow();
					parentMenuItemTable.Rows.Add(parentRow);

					tc = new HtmlTableCell();
					tc.ColSpan=2;
					tc.Controls.Add(menuItemTable);

					parentRow.Controls.Add(tc);
		
					tr = new HtmlTableRow();
					menuItemTable.Rows.Add(tr);

					this.BuildMenuItem(menuItem, tr, depth);

					if (menuItem.MenuItems != null && menuItem.MenuItems.Count > 0)
						this.CreateSubMenuItems(menuItemTable, menuItem, depth);
				}
			}
		}

		/// <summary>
		///		Builds a menu item row
		/// </summary>
		/// <param name="menuItem">The menu item to build.</param>
		/// <param name="tr">The table row in which to build the menu item.</param>
		/// <param name="depth">Depth of the current menu item.</param>
		private void BuildMenuItem(MenuItem menuItem, HtmlTableRow tr, int depth)
		{
			HtmlTableCell tc = null;
			string space = "";

			//Image cell
			tc = new HtmlTableCell();
			tc.Width = "1px";
					
			if (menuItem.CssImageClass.Trim().Length == 0)
			{
				tc.BgColor = ColorTranslator.ToHtml(menuItem.BackColor);
			}

			space = "";
					
			for(int x = 1; x <= depth; x++)
			{
				space += "&nbsp;&nbsp;&nbsp;";
			}

				if (menuItem.CssImageClass != null && menuItem.CssImageClass.Length > 0)
				tc.Attributes.Add("class", menuItem.CssImageClass);

			tc.Controls.Add(new LiteralControl(space));

			//Submenu expand image
			if (this.SubmenuExpandImageUrl != null && this.SubmenuExpandImageUrl.Length > 0 && menuItem.MenuItems != null && menuItem.MenuItems.Count > 0)
			{
				System.Web.UI.WebControls.Image sectionHeaderArrowImage = new System.Web.UI.WebControls.Image();
				sectionHeaderArrowImage.ID = menuItem.MenuId + "_SubMenuArrowImage";

				sectionHeaderArrowImage.ImageUrl = this.SubmenuExpandImageUrl;

				HyperLink subMenuLink = new HyperLink();
				subMenuLink.Attributes["href"] = XMenu.javaScriptVoid;
				subMenuLink.Attributes["onClick"] = "expandSubMenu('" + this.ClientID + "_" + 
					menuItem.MenuId + "_SubMenu', '" + 
					this.SubmenuCollapseImageUrl + "', '" + 
					this.SubmenuExpandImageUrl + "', '')";
		
				//add image to link
				subMenuLink.Controls.Add(sectionHeaderArrowImage);
				tc.Controls.Add(subMenuLink);
			} 
			else 
			{
				if (menuItem.ImageUrl != null && menuItem.ImageUrl.Length > 0)
				{
					System.Web.UI.WebControls.Image image = new System.Web.UI.WebControls.Image();
					image.ImageUrl = menuItem.ImageUrl;
					image.ImageAlign = ImageAlign.Middle;
					
					tc.Controls.Add(image);
				}
				else
				{
					tc.Controls.Add(new LiteralControl(space));
				}
			}					

			tr.Controls.Add(tc);
				
			//Menu Item Cell
			tc = new HtmlTableCell();

			tc.Width = String.Empty;
			tc.BgColor = ColorTranslator.ToHtml(menuItem.BackColor);
				
			tc.Controls.Add(new LiteralControl("&nbsp;"));

			if (menuItem.CssClass != null && menuItem.CssClass.Length > 0)
				tc.Attributes.Add("class", menuItem.CssClass);

			HyperLink lnkMenuItem = new HyperLink();
			lnkMenuItem.Style.Add("text-decoration", "none");
					
			if (menuItem.NavigateUrl != null && menuItem.NavigateUrl.Length > 0)
				lnkMenuItem.NavigateUrl = menuItem.NavigateUrl;
			else
				lnkMenuItem.NavigateUrl = javaScriptVoid;

			lnkMenuItem.Text = menuItem.Text;
			if (menuItem.CssClass == null || menuItem.CssClass.Trim().Length == 0)
			{
				lnkMenuItem.ForeColor = menuItem.ForeColor;
				lnkMenuItem.Font.Bold  = menuItem.Font.Bold;
				lnkMenuItem.Font.Italic = menuItem.Font.Italic;
				lnkMenuItem.Font.Name = menuItem.Font.Name;
				lnkMenuItem.Font.Size = menuItem.Font.Size;
				lnkMenuItem.Font.Strikeout = menuItem.Font.Strikeout;
				lnkMenuItem.Font.Underline = menuItem.Font.Underline;
			}
			else
			{
				lnkMenuItem.CssClass = menuItem.CssClass;
			}

			if (menuItem.MenuItems != null && menuItem.MenuItems.Count > 0)
				lnkMenuItem.Attributes["onclick"] = "expandSubMenu('" + this.ClientID + "_" + 
					menuItem.MenuId + "_SubMenu', '" + 
					this.SubmenuCollapseImageUrl + "', '" + 
					this.SubmenuExpandImageUrl + "', '')";

			switch (menuItem.Target)
			{
				case TargetType.Blank:
					lnkMenuItem.Target = "_blank";
					break;

				case TargetType.Parent :
					lnkMenuItem.Target = "_parent";
					break;

				case TargetType.Self:
					lnkMenuItem.Target = "_self";
					break;

				case TargetType.Top:
					lnkMenuItem.Target = "_top";
					break;

				case TargetType.Main:
					lnkMenuItem.Target = "main";
					break;

				default:
					lnkMenuItem.Target = "_blank";
					break;
			}

			tc.Controls.Add(lnkMenuItem);

			if (this.NewImageUrl != null && menuItem.ShowNewImage)
			{
				System.Web.UI.WebControls.Image image = new System.Web.UI.WebControls.Image();
				image.ImageUrl = this.NewImageUrl;
				image.ImageAlign = ImageAlign.AbsMiddle;
				tc.Controls.Add(new LiteralControl("&nbsp;"));
				tc.Controls.Add(image);
			}

			tr.Cells.Add(tc);
		}

		/// <summary>
		///		IsDesignTime property
		/// </summary>
		protected virtual bool IsDesignTime
		{
			get 
			{
				return (Context == null);
			}
		}
		#endregion Methods
	}
}

namespace ComputerBuff.Controls.XMenu.Design
{
	using ComputerBuff.Controls;
	using System.Globalization;

	/// <summary>
	/// Interacts with the parser to build a PanelBar control.
	/// </summary>
	public class MenuItemBuilder : ControlBuilder
	{
		/// <summary>
		/// This member overrides ControlBuilder.GetChildControlType.
		/// </summary>
		public override Type GetChildControlType(string tagName, IDictionary attributes)
		{
			// check if the tag is a MenuItem tag
			if (tagName.ToLower(CultureInfo.CurrentUICulture).IndexOf("menuitem") >= 0)
			{
				// yes, so return MenuItem type
				return typeof(MenuItem);
			}
			return base.GetChildControlType(tagName,attributes);
		}
	}

	/// <summary>
	///		This class is used for design-time support only.
	/// </summary>
	/// <remarks>This class cannot be inherited</remarks>
	public sealed class XMenuDesigner : ControlDesigner 
	{
		private XMenu xMenu;
		private DesignerVerbCollection m_Verbs;

		/// <summary>
		///		DesignerVerbCollection is overridden from ComponentDesigner
		/// </summary>
		public override DesignerVerbCollection Verbs
		{
			get 
			{
				if (m_Verbs == null) 
				{
					// Create and initialize the collection of verbs
					m_Verbs = new DesignerVerbCollection();
            
//					m_Verbs.Add( new DesignerVerb("Add Menu Items...", new 
//						EventHandler(OnAddMenuItemsSelected)) );

					m_Verbs.Add( new DesignerVerb("About XMenu...", new 
						EventHandler(OnAboutSelected)) );
				}
				return m_Verbs;
			}
		}

		private void OnAddMenuItemsSelected(object sender, EventArgs args) 
		{
		}

		private void OnAboutSelected(object sender, EventArgs args) 
		{
			About about = new About();

			about.ShowDialog();
		}

		/// <summary>
		///		Gets the HTML that is used to represent the control at design time.
		/// </summary>
		/// <returns>string - Design-Time HTML</returns>
		public override string GetDesignTimeHtml()
		{
			// get PanelBar control
			xMenu = (XMenu)Component;
			// return designtime html. Render the control to a stringwriter and return the result
			using(StringWriter stringWriter = new StringWriter(CultureInfo.CurrentUICulture)) 
			using(HtmlTextWriter htmlTextWriter = new HtmlTextWriter(stringWriter))
			{
				xMenu.RenderControl(htmlTextWriter);
				return stringWriter.ToString(); 
			}
		}

		/// <summary>
		///		Gets the persistable inner HTML of the control.
		/// </summary>
		/// <returns>The persistable inner HTML of the control.</returns>
		public override string GetPersistInnerHtml()
		{
			return base.GetPersistInnerHtml();
		}

		/// <summary>
		///		Gets the HTML that provides information about the specified exception.
		///		This method is typically called after an error has been encountered at design time.
		/// </summary>
		/// <param name="e">The exception that occurred. </param>
		/// <returns>string - The HTML for the specified exception.</returns>
		protected override string GetErrorDesignTimeHtml(Exception e)
		{
			return e.Message;
		}
	}

	/// <summary>
	///		Menu Item Collection Editor
	/// </summary>
	public class MenuItemCollectionEditor : CollectionEditor 
	{ 
		// The base class has its own version of this property 
		// cached CollectionForm 
		private CollectionForm collectionForm; 

		/// <summary>
		///		Creates a new instance of the MenuItemCollectionEditor
		/// </summary>
		/// <param name="type">The CollectionEditor to use for editing the collection.</param>
		public MenuItemCollectionEditor(Type type) : base(type) 
		{ 
		} 
 
		/// <summary>
		///		Edits the value of the specified object using the specified service provider and context.
		/// </summary>
		/// <param name="context">An ITypeDescriptorContext that can be used to gain additional context information. </param>
		/// <param name="provider">A service provider object through which editing services can be obtained.</param>
		/// <param name="value">The object to edit the value of.</param>
		/// <returns>The new value of the object. If the value of the object has not changed, this should return the same object it was passed.</returns>
		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{ 
			if(this.collectionForm != null && this.collectionForm.Visible) 
			{ 
				// If the CollectionForm is already visible, then create a new instance 
				// of the editor and delegate this call to it. 
				MenuItemCollectionEditor editor = new MenuItemCollectionEditor(this.CollectionType); 
 
				return editor.EditValue(context, provider, value); 
			} 
			else return base.EditValue(context, provider, value); 
		} 
 
		/// <summary>
		///		Creates a new form to display and edit the current collection.
		/// </summary>
		/// <returns>An instance of CollectionEditor.CollectionForm to provide as the user interface for editing the collection.</returns>
		protected override CollectionForm CreateCollectionForm() 
		{ 
			// Cache the CollectionForm being used. 
			this.collectionForm = base.CreateCollectionForm();  
			return this.collectionForm; 

		}  
	} 
}
