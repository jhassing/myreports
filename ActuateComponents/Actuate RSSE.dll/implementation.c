/*
 * implementation.c
 */

#ifdef _WIN32
#include "stdafx.h"
#endif


#include "rsse.h"
#include "implementation.h"
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <windows.h>



#include <stdarg.h>

#include <string.h>	
#include <winldap.h>
//#include <ldap.h>
//#include <lber.h>
/*
#include "rsse.h"
#include "implementation.h"
#include <ctype.h>
#include <math.h>
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <winldap.h>
*/

#include <errno.h>


/* the following items are initialized by the scanner */
extern int ldapPort;

extern char *userBaseDN;
extern char *userSearchString;
extern char *userObject;
extern char *roleBaseDN;
extern char *roleObject;
extern char *groupBaseDN;
extern char *groupObject;
extern char *groupToNotify;
extern char *allRoleName;
extern char *operatorRoleName;
extern char *adminRoleName;
extern char *queryAccount;
extern char *queryPassword;
extern char *ldapServer;

AcRSSELogFunc rsse_logFunction;

/* the following function are defined in the scanner */
PropEntry *propByID(AcRSSEProperty propID);
const char *propIDToAttr(AcRSSEProperty propID);
AcRSSEProperty propStringToID(const char *propName);

#ifdef _WIN32
#include <string.h>
#define strcasecmp _stricmp
#define index strchr
#else
#include <strings.h>
#endif

#ifndef _WIN32
#include <unistd.h>
#endif

#ifdef __cpluscplus
extern "C" {
#endif

typedef struct {
	int     index;
	char  **values;
	char	do_dealloc;
	char	mem_from_ldap;
}       MultiValue;

static void my_set_errno(int err)
{
	errno = err;
}

static int my_get_errno(void)
{
	return errno;
}


/*-----------------------------------------------------------------------------
 * The following functions work together to support user property retrieval.
 */

typedef struct {
	LDAP   *ld;
	LDAPMessage *result;
	LDAPMessage *entry;
}       PropFetch;

AcRSSEStatus 
fetchProp(const char *funcName, PropFetch * f, const char *dn, const char* filter, const char **props)
{
	int     lderr;
	char    msg[1024];
	
	ULONG version = LDAP_VERSION3;
	
	LOG_INFORM("top of fetchProp");
	f->ld = 0;
	f->result = 0;
	f->entry = 0;
	

	if ((f->ld = ldap_init(ldapServer, ldapPort)) == NULL) {
		sprintf(msg, "%s: ldap_init failed", funcName);
		LOG_FATAL(msg);
		return AC_RSSE_ERROR_UNKNOWN;
	}
	LOG_DEBUG("after ldap_init");

	lderr = ldap_set_option(f->ld, LDAP_OPT_PROTOCOL_VERSION, (void*)&version);

	lderr = ldap_set_option(f->ld, LDAP_OPT_REFERRALS, LDAP_OPT_OFF );

	if ( lderr < 0 ) {
		sprintf(msg, "%s: ldap_set_option failed: %s ", funcName, ldap_err2string(lderr));
		LOG_FATAL(msg);
		ldap_unbind(f->ld);
		return AC_RSSE_ERROR_UNKNOWN;
	}

	if ((lderr = ldap_simple_bind_s(f->ld, queryAccount, queryPassword)) != LDAP_SUCCESS) {
		sprintf(msg, "%s: ldap_simple_bind_s failed: %s ", funcName, ldap_err2string(lderr));
		LOG_FATAL(msg);
		ldap_unbind(f->ld);
		return AC_RSSE_ERROR_UNKNOWN;
	}
	LOG_DEBUG("after ldap_simple_bind_s");

	sprintf(msg, "doing query with \n\tdn=\"%s\"\n\tfilter=\"%s\"\n", dn, filter);
	LOG_INFORM(msg);
	switch (lderr = ldap_search_s(f->ld, dn, LDAP_SCOPE_SUBTREE, filter, (char **) props, 0, &f->result)) {
		case LDAP_SUCCESS:
			break;
		case LDAP_NO_SUCH_OBJECT:
			sprintf(msg, "%s: ldap_search_s failed to find %s", funcName, dn);
			LOG_WARN(msg);
			if (f->result) 
				ldap_msgfree(f->result);
			ldap_unbind(f->ld);
			return AC_RSSE_ERROR_INVALID_USER;
		default:
			sprintf(msg, "%s: ldap_search_s failed: %s", funcName, ldap_err2string(lderr));
			LOG_FATAL(msg);
			if (f->result) 
				ldap_msgfree(f->result);
			ldap_unbind(f->ld);
			return AC_RSSE_ERROR_UNKNOWN;
	}

	if ((f->entry = ldap_first_entry(f->ld, f->result)) == NULL) {
		sprintf(msg, "%s: ldap_first_entry returned null", funcName);
		LOG_WARN(msg);
		if (f->result)
			ldap_msgfree(f->result);
		ldap_unbind(f->ld);
		return AC_RSSE_ERROR_INVALID_USER;
	}
	LOG_INFORM("bottom of fetchProp");
	return AC_RSSE_OK;
}

void 
freeUserProp(PropFetch * f)
{
	if (f->result)
		ldap_msgfree(f->result);
	if (f->ld)
		ldap_unbind(f->ld);
}

static int
verifyQueryAccount(char* queryAccount, char* queryPassword)
{
	LDAP* ld;
	int lderr;
	char msg[1024];

	ULONG version = LDAP_VERSION3;

	if ((ld = ldap_init(ldapServer, ldapPort)) == NULL) {
		LOG_FATAL("ldap_init_failed.\n");
		return -1;
	}

	lderr = ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, (void*)&version);	
	lderr = ldap_set_option(ld, LDAP_OPT_REFERRALS, LDAP_OPT_OFF );

	if ((lderr = ldap_simple_bind_s(ld, queryAccount, queryPassword)) != LDAP_SUCCESS) {
		sprintf(msg, "ldap_simple_bind_s failed: %s. Check your query account: %s.",
			ldap_err2string(lderr), queryAccount);
		sprintf("query password: %s", queryPassword);
		LOG_FATAL(msg);
		ldap_unbind(ld);
		return -1;
	}
	ldap_unbind(ld);
	return 0;
}

AcRSSEStatus
startMultiValue(MultiValue** mvp, const char *dn, const char* filter, const char* prop)
{
	PropFetch	fetch;
	AcRSSEStatus	status;
	const char	*propNames[2];
	BerElement	*ber = 0;
	char		*attr = 0;
	char		**vals;
	char		msg[1024];

	LOG_INFORM("top of startMultiValue");

	propNames[0] = prop;
	propNames[1] = 0;

	status = fetchProp("startMultiValue", &fetch, dn, filter, propNames);
	if (status != AC_RSSE_OK)
		return status;

	if ((attr = ldap_first_attribute(fetch.ld, fetch.entry, &ber)) == NULL) {
		sprintf(msg, "startMultiValue: ldap_first_attribute returned null");
		LOG_WARN(msg);
		freeUserProp(&fetch);
		if (ber) ber_free(ber, 0);
		return AC_RSSE_ERROR_NOT_RESPONSIBLE;
	}
	if ((vals = ldap_get_values(fetch.ld, fetch.entry, attr)) == NULL) {
		sprintf(msg, "startMultiValue: ldap_get_values returned null");
		LOG_WARN(msg);
		ldap_memfree(attr);
		freeUserProp(&fetch);
		if (ber) ber_free(ber, 0);
		return AC_RSSE_ERROR_NOT_RESPONSIBLE;
	}
	if (ber) ber_free(ber,0);
	ldap_memfree(attr);
	ldap_value_free(vals);
	/* search through returned attrs for the one requested */
	/* we should only get the one we requested anyway */
	attr = ldap_first_attribute(fetch.ld, fetch.entry, &ber);
	while (attr != NULL) {
		if (strcasecmp(propNames[0], attr) == 0) {

			vals = ldap_get_values(fetch.ld, fetch.entry, attr);
			ldap_memfree(attr);
			break;
		}
		ldap_memfree(attr);
		attr = ldap_next_attribute(fetch.ld, fetch.entry, ber);
	}

	if (vals == 0) {
		if (ber) ber_free(ber, 0);
		freeUserProp(&fetch);
		LOG_INFORM("startMultiValue returning without value");
		return AC_RSSE_ERROR_NOT_RESPONSIBLE;
	}

	*mvp = (MultiValue *) malloc(sizeof(MultiValue));
	(*mvp)->index = 0;
	(*mvp)->values = vals;
	(*mvp)->do_dealloc = 1;
	(*mvp)->mem_from_ldap = 1;

	if (ber) ber_free(ber, 0);
	freeUserProp(&fetch);

	LOG_INFORM("bottom of startMultiValue");
	return AC_RSSE_OK;
}

AcRSSEStatus
getNextMultiValue(MultiValue* mv, char *propValue, unsigned long valueLength)
{
	char   *val;

	LOG_INFORM("top of getNextMultiValue");

	val = mv->values[mv->index];

	if (val == 0) {
		return AC_RSSE_ERROR_AT_END;
	}
	/* this line errors out if buffer too small */
	if (strlen(val) + 1 > valueLength) {
		return AC_RSSE_ERROR_OVERFLOW;
	}
	/* the data returned was legal, so return it */
	strcpy(propValue, val);
	mv->index++;

	LOG_INFORM("bottom of getNextMultiValue");
	return AC_RSSE_OK;
}

AcRSSEStatus
endMultiValue(MultiValue* mv)
{
	LOG_INFORM("top of endMultiValue");

	if (mv->do_dealloc) {
		if (mv->mem_from_ldap)
			ldap_value_free(mv->values);
		else
			free(mv->values);
	}
	free(mv);

	LOG_INFORM("bottom of endMultiValue");
	return AC_RSSE_OK;
}

static const char	*junk_attr[] = { "junk", 0 };

/*-----------------------------------------------------------------------------
 * The following function verifies existence of an object.
 */

AcRSSEStatus verifyLDAPrecord(const char *funcName, const char *dn, const char* filter)
{
	int		lderr;
	char		msg[1024];
	LDAP*		ld;
	LDAPMessage	*rrresult = NULL;
	ULONG version = LDAP_VERSION3;


	LOG_INFORM("top of verifyLDAPrecord");

	if ((ld = ldap_init(ldapServer, ldapPort)) == NULL) {
		sprintf(msg, "%s: ldap_init failed", funcName);
		LOG_FATAL(msg);
		return AC_RSSE_ERROR_UNKNOWN;
	}
	LOG_DEBUG("after ldap_init");   	

	lderr = ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, (void*)&version);	
	if ( lderr < 0 ) {
		sprintf(msg, "%s: ldap_set_option failed: %s ", funcName, ldap_err2string(lderr));
		LOG_FATAL(msg);
		ldap_unbind(ld);
		return AC_RSSE_ERROR_UNKNOWN;
	}

	lderr = ldap_set_option(ld, LDAP_OPT_REFERRALS, LDAP_OPT_OFF );
	if ( lderr < 0 ) {
		sprintf(msg, "%s: ldap_set_option failed: %s ", funcName, ldap_err2string(lderr));
		LOG_FATAL(msg);
		ldap_unbind(ld);
		return AC_RSSE_ERROR_UNKNOWN;
	}

	if ((lderr = ldap_simple_bind_s(ld, queryAccount, queryPassword)) != LDAP_SUCCESS) {
		sprintf(msg, "%s: ldap_simple_bind_s failed: %s ", funcName, ldap_err2string(lderr));
		LOG_FATAL(msg);
		ldap_unbind(ld);
		return AC_RSSE_ERROR_UNKNOWN;
	}
	LOG_DEBUG("after ldap_simple_bind_s");

	sprintf(msg, "search filter is \"%s\"", filter);
	LOG_DEBUG(msg);
	sprintf(msg, "search base dn is \"%s\"", dn);
	LOG_DEBUG(msg);
	switch (lderr = ldap_search_s(ld, dn, LDAP_SCOPE_SUBTREE, filter, (char **) junk_attr, 2, &rrresult)) {
		case LDAP_SUCCESS:
			ldap_msgfree(rrresult);
			ldap_unbind(ld);
			LOG_INFORM("verifyLDAPrecord returned success");
			return AC_RSSE_OK;
		case LDAP_NO_SUCH_OBJECT:
			sprintf(msg, "%s: ldap_search_s failed to find %s", funcName, dn);
			LOG_WARN(msg);
			ldap_unbind(ld);
			return AC_RSSE_ERROR_INVALID_USER;
		default:
			sprintf(msg, "%s: ldap_search_s failed: %s", funcName, ldap_err2string(lderr));
			LOG_FATAL(msg);
			ldap_unbind(ld);
			return AC_RSSE_ERROR_INVALID_USER;
	}
}

/*-----------------------------------------------------------------------------
 * The following function verifies existence of an object.
 */

AcRSSEStatus searchStart(const char* funcName, const char* dn, const char* filter, int maxNum, MultiValue** mv)
{
	int		lderr;
	char		msg[1024];
	LDAP*		ld;
	LDAPMessage	*result;
	LDAPMessage	*entry;
	int		num;
	int		i;
	ULONG version = LDAP_VERSION3;
	int		sz_lim = 0;

	LOG_INFORM("top of searchStart");

	if ((ld = ldap_init(ldapServer, ldapPort)) == NULL) {
		sprintf(msg, "%s: ldap_init failed", funcName);
		LOG_FATAL(msg);
		return AC_RSSE_ERROR_UNKNOWN;
	}
	LOG_DEBUG("after ldap_init");

	lderr = ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, (void*)&version);
	lderr = ldap_set_option(ld, LDAP_OPT_REFERRALS, LDAP_OPT_OFF );

	if ( lderr < 0 ) {
		sprintf(msg, "%s: ldap_set_option failed: %s ", funcName, ldap_err2string(lderr));
		LOG_FATAL(msg);
		ldap_unbind(ld);
		return AC_RSSE_ERROR_UNKNOWN;
	}

	sprintf(msg, "queryAccount is \"%s\"", queryAccount);
	LOG_DEBUG(msg);
	sprintf(msg, "queryPassword is \"%s\"", queryPassword);
	LOG_DEBUG(msg);
	if ((lderr = ldap_simple_bind_s(ld, queryAccount, queryPassword)) != LDAP_SUCCESS) {
		sprintf(msg, "%s: ldap_simple_bind_s failed: %s ", funcName, ldap_err2string(lderr));
		LOG_FATAL(msg);
		ldap_unbind(ld);
		return AC_RSSE_ERROR_UNKNOWN;
	}
	LOG_DEBUG("after ldap_simple_bind_s");

	if (maxNum == 0) maxNum = 2000;
	ldap_set_option(ld, LDAP_OPT_SIZELIMIT, &maxNum);

	sprintf(msg, "search filter is \"%s\"", filter);
	LOG_DEBUG(msg);
	sprintf(msg, "search base dn is \"%s\"", dn);
	LOG_DEBUG(msg);
	result = 0;
	switch (lderr = ldap_search_s(ld, dn, LDAP_SCOPE_SUBTREE, filter, (char **) junk_attr, 1, &result)) {
		case LDAP_SUCCESS:
			LOG_DEBUG("ldap_search_s returned LDAP_SUCCESS");
			break;
		case LDAP_SIZELIMIT_EXCEEDED:
			LOG_WARN("ldap_search_s returned LDAP_SIZELIMIT_EXCEEDED");
			sz_lim = 1;
			break;
		case LDAP_TIMELIMIT_EXCEEDED:
			LOG_WARN("ldap_search_s returned LDAP_TIMELIMIT_EXCEEDED");
			if (result) ldap_msgfree(result);
			ldap_unbind(ld);
			return AC_RSSE_ERROR_TIMELIMIT_EXCEEDED;
		case LDAP_NO_SUCH_OBJECT:
			break;
		default:
			sprintf(msg, "%s: ldap_search_s failed: %s", funcName, ldap_err2string(lderr));
			LOG_FATAL(msg);
			ldap_unbind(ld);
			return AC_RSSE_ERROR_UNKNOWN;
	}
		

	if ((lderr == LDAP_NO_SUCH_OBJECT) || (result == 0) || ((num = ldap_count_entries(ld, result)) == 0)) {
		sprintf(msg, "%s: ldap_search_s returned no results", funcName);
		LOG_WARN(msg);
		if (result) ldap_msgfree(result);
		ldap_unbind(ld);
		*mv = (MultiValue *) malloc(sizeof(MultiValue));
		(*mv)->index = 0;
		(*mv)->values = (char **) calloc(1, sizeof(char*));
		(*mv)->values[0] = 0;
		return AC_RSSE_OK;
	}

	sprintf(msg, "ldap_count_entries returned %d", num);
	LOG_DEBUG(msg);

	//LOG_DEBUG("before ldap_sort_entries");
	//if ((lderr = ldap_sort_entries(ld, &result, NULL, strcasecmp)) != LDAP_SUCCESS) {
	//	sprintf(msg, "%s: ldap_sort_entries failed: %s", funcName, ldap_err2string(lderr));
	//	LOG_FATAL(msg);
	//	ldap_msgfree(result);
	//	ldap_unbind(ld);
	//	return AC_RSSE_ERROR_UNKNOWN;
	//}
	LOG_DEBUG("after ldap_sort_entries");

	*mv = (MultiValue *) malloc(sizeof(MultiValue));
	(*mv)->index = 0;
	(*mv)->values = (char **) calloc(num + 1, sizeof(char*));

	entry = ldap_first_entry(ld, result);
	i = 0;
	do {
		char*	dn;
		char*	leader;

		dn = ldap_get_dn(ld, entry);
		leader = dn;
		while (*leader != '=' && *leader != 0) leader++;
		if (*leader == '=') {
			char*	trailer;

			leader++;
			trailer = leader;
			while (*trailer != ',' && *trailer != 0) trailer++;
			if (*trailer == ',') *trailer = 0;
			(*mv)->values[i] = strdup(leader);
			(*mv)->values[i+1] = 0;
			i++;
		}
		ldap_memfree(dn);
		entry = ldap_next_entry(ld, entry);
	} while(entry && i < num);

	ldap_msgfree(result);
	ldap_unbind(ld);

	LOG_INFORM("bottom of searchStart");
	if (sz_lim) return AC_RSSE_ERROR_OVERFLOW;
	return AC_RSSE_OK;
}

AcRSSEStatus searchNext(const char* funcName, MultiValue* mv, char* buffer, unsigned long bufferLength)
{
	char   *val;

	LOG_INFORM("top of searchNext");
	val = mv->values[mv->index];

	if (val == 0) {
		LOG_INFORM("searchNext indicates at end");
		return AC_RSSE_ERROR_AT_END;
	}

	/* this line errors out if buffer too small */
	if (strlen(val) + 1 > bufferLength) {
		LOG_INFORM("searchNext indicates buffer too small");
		return AC_RSSE_ERROR_OVERFLOW;
	}

	/* the data returned was legal, so return it */
	strcpy(buffer, val);
	free(val);
	mv->index++;

	LOG_INFORM("bottom of searchNext");
	return AC_RSSE_OK;
}

AcRSSEStatus searchEnd(const char* funcName, MultiValue* mv)
{
	LOG_INFORM("top of searchEnd");
	free(mv->values);
	free(mv);
	LOG_INFORM("bottom of searchEnd");
	return AC_RSSE_OK;
}

/*-----------------------------------------------------------------------------
 * Functions defined below are called externally.
 */
extern int scanner(char *filename, char *err);

AcRSSEStatus
initialize(const char *serverHome, AcRSSELogFunc lf)
{
	char    msg[1024];
	char    config[1024];

	rsse_logFunction = lf;

	sprintf(config, "%s/etc/ldap.conf", serverHome);

	LOG_INFORM("initialize preparing to scan \"%s\"");
	if (scanner(config, msg) < 0) {
		LOG_FATAL(msg);
		return AC_RSSE_ERROR_UNKNOWN;
	}
	
	if (verifyQueryAccount(queryAccount, queryPassword)) {
		return AC_RSSE_ERROR_UNKNOWN;
	}

	if (userBaseDN == 0) {
		LOG_FATAL("config file did not define USER_BASE_DN");
		return AC_RSSE_ERROR_UNKNOWN;
	}

	if (userObject == 0) {
		LOG_FATAL("config file did not define USER_OBJECT");
		return AC_RSSE_ERROR_UNKNOWN;
	}

	if (roleBaseDN == 0) {
		LOG_FATAL("config file did not define ROLE_BASE_DN");
		return AC_RSSE_ERROR_UNKNOWN;
	}

	if (roleObject == 0) {
		LOG_FATAL("config file did not define ROLE_OBJECT");
		return AC_RSSE_ERROR_UNKNOWN;
	}

	if (groupBaseDN == 0) {
		LOG_FATAL("config file did not define GROUP_BASE_DN");
		return AC_RSSE_ERROR_UNKNOWN;
	}

	if (groupObject == 0) {
		LOG_FATAL("config file did not define GROUP_OBJECT");
		return AC_RSSE_ERROR_UNKNOWN;
	}

	LOG_INFORM("initialize successfully scanned file");
	return AC_RSSE_OK;
}

AcRSSEStatus
getIntegrationLevel(AcRSSEIntegrationLevel * level)
{
	*level = AC_RSSE_EXTERNAL_REGISTRATION;

	//AC_RSSE_EXTERNAL_NONE = 0,
	//AC_RSSE_EXTERNAL_AUTHENTICATION = 1,
	//AC_RSSE_EXTERNAL_REGISTRATION = 2

	//*level = AC_RSSE_EXTERNAL_AUTHENTICATION;

	return AC_RSSE_OK;
}

/*
 * This example does not illustrate the use of extended credentials.
 */
AcRSSEStatus
authenticateUser(const char *user, const char *password,
   const char *extendedCredentials, unsigned long extendedCredentialsLength)
{
	LDAP   *ld;
	char    dn[4096];
	char    msg[1024];
	AcRSSEStatus status;
	int     lderr;
	
	ULONG version = LDAP_VERSION3;

	// Force OK
	return AC_RSSE_OK;

	//AcRSSEStatus mystatus;

	if (user == 0) return AC_RSSE_ERROR_INVALID_USER;
	if (user[0] == 0) return AC_RSSE_ERROR_INVALID_USER;
	if (password == 0) return AC_RSSE_ERROR_INVALID_PASSWORD;
	if (password[0] == 0) return AC_RSSE_ERROR_INVALID_PASSWORD;

	LOG_DEBUG("top of authenticateUser");
	if ((ld = ldap_init(ldapServer, ldapPort)) == NULL) {
		sprintf(msg, "AuthenticateUser: ldap_init failed");
		LOG_FATAL(msg);
		return AC_RSSE_ERROR_UNKNOWN;
	}

	lderr = ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, (void*)&version);
	lderr = ldap_set_option(ld, LDAP_OPT_REFERRALS, LDAP_OPT_OFF );
	
	if ( lderr < 0 ) {
		sprintf(msg, "AuthenticateUser: ldap_set_option failed: %s ", ldap_err2string(lderr));
		LOG_FATAL(msg);
		ldap_unbind(ld);
		return AC_RSSE_ERROR_UNKNOWN;
	}

	LOG_DEBUG("Ok, do it here");

	sprintf(dn, getDN(user));
	LOG_DEBUG("The User IS::: ");
	LOG_DEBUG(dn);

	switch (lderr = ldap_simple_bind_s(ld, dn, password)) {
		case LDAP_SUCCESS:
			sprintf(msg, "AuthenticateUser(%s) successful", dn);
			LOG_INFORM(msg);
			status = AC_RSSE_OK;
			break;
		case LDAP_NO_SUCH_OBJECT:
			sprintf(msg, "AuthenticateUser(%s) failed: no such object", dn);
			LOG_INFORM(msg);
			status = AC_RSSE_ERROR_INVALID_USER;
			break;
		case LDAP_INVALID_CREDENTIALS:
		case LDAP_INAPPROPRIATE_AUTH:
			sprintf(msg, "AuthenticateUser(%s) failed: invalid password", dn);
			LOG_INFORM(msg);
			status = AC_RSSE_ERROR_INVALID_PASSWORD;
			break;
		default:
			sprintf(msg, "AuthenticateUser: ldap_simple_bind_s failed: %s ", ldap_err2string(lderr));
			LOG_FATAL(msg);		
			//status = AC_RSSE_ERROR_UNKNOWN;
			status = AC_RSSE_ERROR_INVALID_USER;

			break;
	}

	ldap_unbind(ld);
	LOG_DEBUG("bottom of authenticateUser");
	return status;
}

AcRSSEStatus
isUserPropertyExternal(AcRSSEProperty propID, AcRSSEBoolean * isExternal)
{
	PropEntry *p;

	p = propByID(propID);

	if (p == 0)
		*isExternal = AC_RSSE_FALSE;
	else
		*isExternal = AC_RSSE_TRUE;

	return AC_RSSE_OK;
}

/*-----------------------------------------------------------------------------
 * The following functions work together to get properties of an external user.
 */
typedef struct {
	int     index;
	int     ids[MAX_PROPERTIES];
	char   *values[MAX_PROPERTIES];
}       UserPropRequest;

AcRSSEStatus
startUserProperties(AcRSSEUserPropHandle * handle, const char *user,
		    const AcRSSEProperty * propIDs)
{
	UserPropRequest	*request;
	BerElement	*ber = 0;
	char		*attr = 0;
	int		i;
	PropFetch	fetch;
	AcRSSEStatus	status;
	const char	*propertyArray[20];
	char		dn[4096];
	char		filter[4096];

	LOG_DEBUG("top of startUserProperties");

	i = 0;
	while (propIDs[i] != AC_RSSE_NULL_PROPERTY) {
		propertyArray[i] = propIDToAttr(propIDs[i]);
		if (propertyArray[i])
			i++;
	}
	propertyArray[i] = 0;

	sprintf(dn, getDN(user));
	sprintf(filter, "(objectClass=%s)", userObject);

	status = fetchProp("startUserProperties", &fetch, dn, filter, propertyArray);
	if (status != AC_RSSE_OK)
		return status;

	LOG_DEBUG("after fetchProp");

	request = (UserPropRequest *) malloc(sizeof(UserPropRequest));
	request->index = 0;

	attr = ldap_first_attribute(fetch.ld, fetch.entry, &ber);
	LOG_DEBUG("after ldap_first_attribute");
	while (attr != NULL) {
		char  **vals;

		if ((vals = ldap_get_values(fetch.ld, fetch.entry, attr)) != NULL) {
			LOG_DEBUG("ldap_get_values returned a value");
			request->ids[request->index] = propStringToID(attr);
			request->values[request->index] = strdup(vals[0]);
			request->index++;
		}
	
		ldap_value_free(vals);
		ldap_memfree(attr);

		attr = ldap_next_attribute(fetch.ld, fetch.entry, ber);
		LOG_DEBUG("after ldap_next_attribute");
	}

	if (ber) ber_free(ber, 0);
	LOG_DEBUG("before freeUserProp");
	freeUserProp(&fetch);
	LOG_DEBUG("after freeUserProp");

	*handle = request;

	LOG_DEBUG("bottom of startUserProperties");
	return AC_RSSE_OK;
}

AcRSSEStatus
getUserProperty(AcRSSEUserPropHandle handle, AcRSSEProperty propID,
		char *propValue, unsigned long valueLength)
{
	int     i;
	char    errbuf[1024];
	char    msg[1024];
	char   *val;
	UserPropRequest *request;
	PropEntry *p;

	LOG_DEBUG("top of getUserProperties");

	p = propByID(propID);
	if (p == 0) {
		sprintf(msg, "getUserProperties doesn't known id=%d", propID);
		LOG_WARN(msg);
		return AC_RSSE_ERROR_NOT_RESPONSIBLE;
	}
	request = (UserPropRequest *) handle;

	for (i = 0; i < request->index; i++) {
		if (propID == request->ids[i])
			goto got_ldap_val;
	}

	/*
	 * this point means LDAP didn't return a value, so lets consider
	 * defaults
	 */
	if (p->defaultData == 0) {
		return AC_RSSE_ERROR_LOOKUP_FAILED;
	}
	/* we do have a default (verified and config time), so lets return it */
	val = (char *) p->defaultData;
	goto check_len_and_ret;

got_ldap_val:
	/* this point mean LDAP did return a value */
	val = request->values[i];

	/* if there is no verifyFunc, we can just return the value */
	if (p->verifyFunc == 0)
		goto check_len_and_ret;

	/* lets check that the LDAP value is legal */
	if ((*p->verifyFunc) (errbuf, val, p->verifyData) < 0) {
		sprintf(msg, "getUserProperties failed to verify id=%d", propID);
		LOG_WARN(msg);
		return AC_RSSE_ERROR_LOOKUP_FAILED;
	}
check_len_and_ret:
	/* this line errors out if buffer too small */
	if (strlen(val) + 1 > valueLength) {
		return AC_RSSE_ERROR_OVERFLOW;
	}
	/* the data returned was legal, so return it */
	strcpy(propValue, val);
	LOG_DEBUG("bottom of getUserProperties");
	return AC_RSSE_OK;
}

AcRSSEStatus
endUserProperties(AcRSSEUserPropHandle handle)
{
	int     i;

	UserPropRequest *request = (UserPropRequest *) handle;

	LOG_DEBUG("top of endUserProperties");

	for (i = 0; i < request->index; i++)
		free(request->values[i]);
	free(request);

	LOG_DEBUG("bottom of endUserProperties");
	return AC_RSSE_OK;
}

/*-----------------------------------------------------------------------------
 * The following functions to fetch user default privledges.
 */
AcRSSEStatus
startUserDefPriv(
		 AcRSSEUserDefPrivHandle * handle,
		 const char *user)
{
	PropEntry*	p;
	AcRSSEStatus	status;
	char		dn[4096];
	char		filter[4096];

	if ((p = propByID(AC_RSSE_USER_DEFAULT_OBJECT_PRIVILEGES)) == 0) {
		char	msg[1024];

		sprintf(msg, "startUserDefPriv: attribute undefined");
		LOG_WARN(msg);
		return AC_RSSE_ERROR_NOT_RESPONSIBLE;
	}

	sprintf(dn, getDN(user));

	sprintf(filter, "(objectClass=%s)", userObject);

	status =  startMultiValue((MultiValue**)handle, dn, filter, p->propName);
	if (status == AC_RSSE_ERROR_NOT_RESPONSIBLE) {
		MultiValue**	mvp = (MultiValue**) handle;

		*mvp = (MultiValue *) malloc(sizeof(MultiValue));
		(*mvp)->index = 0;
		(*mvp)->values = (char**) p->defaultData;
		(*mvp)->do_dealloc = 0;
		return AC_RSSE_OK;
	}
	return status;
}

AcRSSEStatus
getNextDefPriv(
	       AcRSSEUserDefPrivHandle handle,
	       AcRSSEPrivType * roleOrUser,
	       char *name,
	       unsigned long nameLength,
	       AcRSSEPrivMask * permissionMask)
{
	unsigned long     len;
	char   *ptr;
	MultiValue *request;
	char   *val;

	LOG_DEBUG("top of getNextDefPriv");

	request = (MultiValue *) handle;
	val = request->values[request->index];

	if (val == 0)
		return AC_RSSE_ERROR_AT_END;
	len = 0;
	ptr = val;
	for (;;) {
		switch (*ptr) {
			case 0:
			case '~':
				*roleOrUser = AC_RSSE_IS_ROLE;
				goto leave_loop;
			case '=':
				*roleOrUser = AC_RSSE_IS_USER;
				goto leave_loop;
			default:
				len++;
				ptr++;
				continue;
		}
	}
leave_loop:
	if (len + 1 > nameLength)
		return AC_RSSE_ERROR_OVERFLOW;
	*ptr = 0;
	ptr++;
	strcpy(name, val);
	*permissionMask = 0;
	for (*permissionMask = 0; *ptr; ptr++) {
		switch (*ptr) {
			case 'r':
			case 'R':
				*permissionMask |= AC_RSSE_PRIV_READ;
				continue;
			case 'w':
			case 'W':
				*permissionMask |= AC_RSSE_PRIV_WRITE;
				continue;
			case 'e':
			case 'E':
				*permissionMask |= AC_RSSE_PRIV_EXECUTE;
				continue;
			case 'd':
			case 'D':
				*permissionMask |= AC_RSSE_PRIV_DELETE;
				continue;
			case 'v':
			case 'V':
				*permissionMask |= AC_RSSE_PRIV_VISIBLE;
				continue;
			case 's':
			case 'S':
				*permissionMask |= AC_RSSE_PRIV_SECURE_READ;
				continue;
			case 'g':
			case 'G':
				*permissionMask |= AC_RSSE_PRIV_GRANT;
				continue;
			case ' ':
			case '\t':
				continue;
			default:
				return AC_RSSE_ERROR_LOOKUP_FAILED;
		}
	}
	request->index++;

	LOG_DEBUG("bottom of getNextDefPriv");
	return AC_RSSE_OK;
}

AcRSSEStatus
endUserDefPriv(AcRSSEUserDefPrivHandle handle)
{
	MultiValue *request;

	LOG_DEBUG("top of endUserDefPriv");

	request = (MultiValue *) handle;
	if (request->do_dealloc) {
		if (request->mem_from_ldap)
			ldap_value_free(request->values);
		else
			free(request->values);
	}
	free(request);

	LOG_DEBUG("bottom of endUserDefPriv");
	return AC_RSSE_OK;
}

/*-----------------------------------------------------------------------------
 * The following functions to fetch channels of user
 */

AcRSSEStatus
startUserChannels(AcRSSEChannelsOfUserHandle * handle, const char *user)
{
	PropEntry*	p;
	AcRSSEStatus	status;
	char		dn[4096];
	char		filter[4096];

	if ((p = propByID(AC_RSSE_USER_CHANNEL_SUBSCRIPTION_LIST)) == 0) {
		char	msg[1024];

		sprintf(msg, "startUserChannels: attribute undefined");
		LOG_WARN(msg);
		return AC_RSSE_ERROR_NOT_RESPONSIBLE;
	}

	sprintf(dn, getDN(user));
	sprintf(filter, "(objectClass=%s)", userObject);

	status =  startMultiValue((MultiValue**)handle, dn, filter, p->propName);
	if (status == AC_RSSE_ERROR_NOT_RESPONSIBLE) {
		MultiValue**	mvp = (MultiValue**) handle;

		*mvp = (MultiValue *) malloc(sizeof(MultiValue));
		(*mvp)->index = 0;
		(*mvp)->values = (char**)p->defaultData;
		(*mvp)->do_dealloc = 0;
		return AC_RSSE_OK;
	}
	return status;
}

AcRSSEStatus
getNextChannel(AcRSSEChannelsOfUserHandle handle, char *propValue, unsigned long valueLength)
{
	return getNextMultiValue((MultiValue*)handle, propValue, valueLength);
}

AcRSSEStatus
endUserChannels(AcRSSEChannelsOfUserHandle handle)
{
	return endMultiValue((MultiValue*)handle);
}

/*****************************************************************************/
/********* Functions used by level 3 and above *******************************/
/*****************************************************************************/

AcRSSEStatus
doesUserExist(const char *user, AcRSSEBoolean * exists)
{
	AcRSSEStatus	status;
	char		dn[4096];
	char		filter[4096];

	sprintf(dn, getDN(user));
	sprintf(filter, "(objectClass=%s)", userObject);

	switch (status = verifyLDAPrecord("doesUserExist", dn, filter)) {
		case AC_RSSE_OK:
			*exists = AC_RSSE_TRUE;
			return AC_RSSE_OK;
		case AC_RSSE_ERROR_INVALID_USER:
			*exists = AC_RSSE_FALSE;
			return AC_RSSE_OK;
		default:
			return status;
	}
}

AcRSSEStatus
doesRoleExist(const char *role, AcRSSEBoolean * exists)
{
	AcRSSEStatus	status;
	char		dn[4096];
	char		filter[4096];

	sprintf(dn, "cn=%s, %s", role, roleBaseDN);
	sprintf(filter, "(objectClass=%s)", roleObject);

	switch (status = verifyLDAPrecord("doesRoleExist", dn, filter)) {
		case AC_RSSE_OK:
			*exists = AC_RSSE_TRUE;
			return AC_RSSE_OK;
		case AC_RSSE_ERROR_INVALID_USER:
			*exists = AC_RSSE_FALSE;
			return AC_RSSE_OK;
		default:
			return status;
	}
}

/*-----------------------------------------------------------------------------
 * The following functions work together to find an external user.
 */
AcRSSEStatus
startUserQuery(AcRSSEUserHandle * handle,
	       const char *pattern,
	       unsigned long maxNumber)
{
	char	filter[4096];

	/*
	 * If pattern is not a null pointer and not a zero length string,
	 * then use the pattern in the filter.
	 */
	if (pattern && pattern[0])
//		sprintf(filter, "(&(uid=%s)(objectClass=%s))", pattern, userObject);

		//sprintf(filter, "(&(cn=%s)(objectClass=%s)(actuateUser=TRUE))", pattern, userObject);
		sprintf(filter, "(&(cn=%s)(objectClass=%s)%s", pattern, userObject, userSearchString);

	else
		//sprintf(filter, "(&(objectClass=%s)(actuateUser=TRUE))", userObject);
		sprintf(filter, "(&(objectCategory=user)%s", userObject, userSearchString);



	LOG_DEBUG(filter);

	return searchStart("startUserQuery", userBaseDN, filter, maxNumber, (MultiValue**)handle);
}

AcRSSEStatus
getNextUser(AcRSSEUserHandle handle,
	    char *buffer,
	    unsigned long bufferLength)
{
	return searchNext("getNextUser", (MultiValue*)handle, buffer, bufferLength);
}

AcRSSEStatus
endUserQuery(AcRSSEUserHandle handle)
{
	return searchEnd("endUserQuery", (MultiValue*)handle);
}

/*-----------------------------------------------------------------------------
 * The following functions work together to find an external role.
 */
AcRSSEStatus
startRoleQuery(AcRSSERoleHandle * handle,
	       const char *pattern,
	       unsigned long maxNumber)
{
	char	filter[4096];

	/*
	 * If pattern is not a null pointer and not a zero length string,
	 * then use the pattern in the filter.
	 */
	if (pattern && pattern[0])
		sprintf(filter, "(&(cn=%s)(objectClass=%s))", pattern, roleObject);
	else
		sprintf(filter, "(objectClass=%s)", roleObject);
	return searchStart("startRoleQuery", roleBaseDN, filter, maxNumber, (MultiValue**)handle);
}

AcRSSEStatus
getNextRole(AcRSSERoleHandle handle,
	    char *buffer,
	    unsigned long bufferLength)
{
	return searchNext("getNextRole", (MultiValue*)handle, buffer, bufferLength);
}

AcRSSEStatus
endRoleQuery(AcRSSERoleHandle handle)
{
	return searchEnd("endRoleQuery", (MultiValue*)handle);
}

/*-----------------------------------------------------------------------------
 * The following functions work together to find an external group.
 */
AcRSSEStatus
startGroupQuery(AcRSSEGroupHandle * handle,
		const char *pattern,
		unsigned long maxNumber)
{
	char	filter[4096];

	if (pattern && pattern[0])
		sprintf(filter, "(&(cn=%s)(objectClass=%s))", pattern, groupObject);
	else
		sprintf(filter, "(objectClass=%s)", groupObject);

	return searchStart("startGroupQuery", groupBaseDN, filter, maxNumber, (MultiValue**)handle);
}

AcRSSEStatus
getNextGroup(AcRSSEGroupHandle handle,
	     char *buffer,
	     unsigned long bufferLength)
{
	AcRSSEStatus l_status;
	for (;;) 
	{
		l_status = searchNext("getNextGroup", (MultiValue*)handle, buffer, bufferLength);
		if (groupToNotify != NULL)
		{
			if ((strcasecmp(buffer, groupToNotify) != 0) ||
				(l_status == AC_RSSE_ERROR_AT_END))
				break;
		}
		else
			break;
	}
	
		return l_status;
	}

AcRSSEStatus
endGroupQuery(AcRSSEGroupHandle handle)
{
	return searchEnd("endGroupQuery", (MultiValue*)handle);
}

/*-----------------------------------------------------------------------------
 * The following functions work together to find the users to notify.
 */


AcRSSEStatus
startUsersToNotify(AcRSSEUsersToNotifyHandle * handle) {
	if (groupToNotify == NULL) {
		return AC_RSSE_ERROR_NOT_RESPONSIBLE;
	}
	return startGroupUserNames ((AcRSSEUsersOfGroupHandle*)handle, groupToNotify);
}

AcRSSEStatus
getNextUserToNotify(AcRSSEUsersToNotifyHandle handle,
					char *buffer, unsigned long bufferLength) {
	return getNextGroupUserName((AcRSSEUsersOfGroupHandle) handle,buffer, bufferLength);
}

AcRSSEStatus
endUsersToNotify(AcRSSEUsersToNotifyHandle handle) {
	return endGroupUserNames((AcRSSEUsersOfGroupHandle) handle);
}



/*-----------------------------------------------------------------------------
 * The following functions work together to list roles of an external user.
 */
AcRSSEStatus
startRolesOfUser(AcRSSERolesOfUserHandle * handle,
		 const char *user)
{
	char	filter[4096];
	char	dn[4096];

	LOG_DEBUG("Get user Roles function");

	sprintf(dn, getDN(user));

	sprintf(filter, "(&(objectclass=Group)(member=%s))",dn);
	//sprintf(filter, "(&(objectclass=Group)(member=cn=John Test,dc=na,dc=compassdev,dc=corp))");
	
	LOG_DEBUG("this is the DN");

	LOG_DEBUG(dn);
	
	return searchStart("startRolesOfUser", roleBaseDN, filter, 0, (MultiValue**)handle);
	
}

AcRSSEStatus
getNextRoleOfUser(AcRSSERolesOfUserHandle handle,
		  char *buffer,
		  unsigned long bufferLength)
{
	return searchNext("getNextRoleOfUser", (MultiValue*)handle, buffer, bufferLength);
}

AcRSSEStatus
endRolesOfUser(AcRSSERolesOfUserHandle handle)
{
	return searchEnd("endRolesOfUser",(MultiValue*)handle);
}

/*-----------------------------------------------------------------------------
 * The following functions work together to list users of an external group.
 */
AcRSSEStatus
startGroupUserNames(AcRSSEUsersOfGroupHandle * handle,
		    const char *group)
{
	char	dn[4096];

	sprintf(dn, "cn=%s, %s", group, groupBaseDN);

	return startMultiValue((MultiValue**)handle, dn, "(objectClass=*)", "uniquemember");
}

AcRSSEStatus
getNextGroupUserName(AcRSSEUsersOfGroupHandle handle,
		     char *buffer,
		     unsigned long bufferLength)
{
	char   *val;
	char	*ptr;
	char	buf[100];
	char	*bptr;
	MultiValue*	mv;

	LOG_DEBUG("top of getNextGroupUserName");

	mv = (MultiValue*)handle;

	val = mv->values[mv->index];

	if (val == 0)  return AC_RSSE_ERROR_AT_END;

	ptr = val;
	while (*ptr != 0 && *ptr != '=') ptr++;
	ptr++;
	bptr = buf;
	while (*ptr != 0 && *ptr != ',') {
		*bptr++ = *ptr++;
		*bptr = 0;
	}

	/* this line errors out if buffer too small */
	if (strlen(buf) + 1 > bufferLength) {
		return AC_RSSE_ERROR_OVERFLOW;
	}

	/* the data returned was legal, so return it */
	strcpy(buffer, buf);
	mv->index++;

	LOG_DEBUG("bottom of getNextGroupUserName");
	return AC_RSSE_OK;
}

AcRSSEStatus
endGroupUserNames(AcRSSEUsersOfGroupHandle handle)
{
	return endMultiValue((MultiValue*)handle);
}

/*-----------------------------------------------------------------------------
 * Functions that support special roles
 */

AcRSSEStatus
getTranslatedRoleName(const char *internalRole,
		      char *externalRole, unsigned long roleLength)
{
	char** ptr;

	if (strcasecmp(internalRole, "all") == 0) {
		ptr = &allRoleName;
	}
	else if(strcasecmp(internalRole, "administrator") == 0) {
		ptr = &adminRoleName;
	}
	else if(strcasecmp(internalRole, "operator") == 0) {
		ptr = &operatorRoleName;
	}
	else {
		return AC_RSSE_ERROR_NOT_RESPONSIBLE;
	}

	if (*ptr == 0) return AC_RSSE_ERROR_NOT_RESPONSIBLE;

	if (strlen(*ptr) + 1 > roleLength) return AC_RSSE_ERROR_OVERFLOW;

	strcpy(externalRole, *ptr);
	return AC_RSSE_OK;
}



char const *getDN(const char *user)
{
	char const dnName[4096];
	char pSearchDN[4096];		

	ULONG version = LDAP_VERSION3;

    LDAP* pLdapConnection = NULL;
	LDAPMessage	*rrresult, *ett;
	
    ULONG getOptSuccess = 0;
    ULONG connectSuccess = 0;
    INT iRtn = 0;

	sprintf(pSearchDN, "(&(samaccountname=%s)%s", user, userSearchString);

	LOG_DEBUG(pSearchDN);

	LOG_DEBUG("Connecting....");

    pLdapConnection = ldap_init(ldapServer, LDAP_PORT);

    if (pLdapConnection == NULL)
    {
        LOG_DEBUG( "ldap_init failed");
		goto error_exit;
        
    }
    else
        LOG_DEBUG("ldap_init succeeded \n");


	iRtn = ldap_set_option(pLdapConnection, LDAP_OPT_PROTOCOL_VERSION, (void*)&version);

    if(iRtn == LDAP_SUCCESS)
        LOG_DEBUG("ldap_set_option succeeded - version set to 3");
    else
    {
        LOG_DEBUG("SetOption Error");
        goto error_exit;
    }

	iRtn = ldap_set_option(pLdapConnection, LDAP_OPT_REFERRALS, LDAP_OPT_OFF );

    LOG_DEBUG("Binding ...");

	iRtn = ldap_simple_bind_s(pLdapConnection,  queryAccount, queryPassword);

    if(iRtn == LDAP_SUCCESS)
    {
        LOG_DEBUG("ldap_bind_s succeeded \n");
    }
    else
    {
        LOG_DEBUG("ldap_bind_s failed ");
        ldap_unbind(pLdapConnection);
    }
						
		iRtn = ldap_search_s(pLdapConnection, userBaseDN, 2, pSearchDN, NULL, 0, &rrresult);
		LOG_DEBUG("Return Code is");
		LOG_DEBUG(userBaseDN);
		LOG_DEBUG(pSearchDN);
		
		LOG_DEBUG("LDAP MAESSAGE:");
		

		if (iRtn == LDAP_SUCCESS)
		{				
			LOG_DEBUG("1");
			if ((ett = ldap_first_entry( pLdapConnection, rrresult )) != NULL)
				{
					LOG_DEBUG("2");
					if ( (sprintf(dnName,ldap_get_dn( pLdapConnection, ett) )) != NULL )
					{
						LOG_DEBUG(dnName);
						ldap_unbind(pLdapConnection);
						if (rrresult != NULL) ldap_msgfree(rrresult);
						return(dnName);
					}
			}
			else
			{
				LOG_DEBUG("3");
				ldap_unbind(pLdapConnection);
				if (rrresult != NULL) ldap_msgfree(rrresult);
				return("cn=Invalid DN");
			}

		}
		else
		{
			LOG_DEBUG("ldap_search_s failed");
		}

	error_exit:
    ldap_unbind(pLdapConnection);
	if (rrresult != NULL) ldap_msgfree(rrresult);

	return("cn=Invalid DN");

}



#ifdef __cpluscplus
}
#endif
