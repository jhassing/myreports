/* $Revision: 11 $
// $Archive: /Development/Server/RSSE/LDAP/cpid.h $

//----------------------------------------------------------------------
// cpid.h
//
// Copyright 1994-2002, Actuate Software Corp., All rights reserved.

// Declares the list of encoding ID.
//----------------------------------------------------------------------*/

/* ======================== IMPORTANT NOTE ===========================
//
// DO NOT CHANGE THE VALUE OF THE #DEFINES UNLESS YOU MAKE THE CORRESPONSING
// CHANGES IN  /osd/acencodings.cpp  and icu/common/.../unistr.cpp
//
//--------------------------------------------------------------------*/
#ifndef __RSAPI_CPID_H__
#define __RSAPI_CPID_H__

#define AC_UNDEFINED_CODEPAGE 0x7fff /* undefined encoding */

#define AC_DEFAULT_CODEPAGE	0		/* system encoding */
#define AC_ASCII			1		/* ASCII */

/* ISO Codepages*/
#define AC_ISO_8859_1		2		/* ISO Latin1 (West European) */
#define AC_ISO_8859_2		3		/* ISO Latin2 (Central European) */
#define AC_ISO_8859_3		4		/* ISO Latin3 */
#define AC_ISO_8859_4		5		/* ISO Latin4 */
#define AC_ISO_8859_5		6		/* ISO Cyrillic */
#define AC_ISO_8859_6		7		/* ISO Arabic */
#define AC_ISO_8859_7		8		/* ISO Greek */
#define AC_ISO_8859_8		9		/* ISO Hebrew */
#define AC_ISO_8859_9 		10		/* ISO Latin5 (Turkish) */
#define AC_ISO_LATIN_1		AC_ISO_8859_1
#define AC_ISO_LATIN_2		AC_ISO_8859_2
#define AC_ISO_LATIN_3		AC_ISO_8859_3
#define AC_ISO_LATIN_4		AC_ISO_8859_4
#define AC_ISO_LATIN_5		AC_ISO_8859_9
#define AC_ISO_CYRILLIC		AC_ISO_8859_5
#define AC_ISO_ARABIC		AC_ISO_8859_6
#define AC_ISO_GREEK           AC_ISO_8859_7
#define AC_ISO_HEBREW		AC_ISO_8859_8
#define AC_ISO_THAI			AC_ISO_8859_11

/* Windows Codepages */
#define AC_WINDOWS_1250		11		/* Central European */
#define AC_WINDOWS_1251		12		/* Cyrillic */
#define AC_WINDOWS_1252		13		/* Western */
#define AC_WINDOWS_1253		14		/* Greek */	
#define AC_WINDOWS_1254		15		/* Turkish */
#define AC_WINDOWS_1255		16		/* Hebrew */
#define AC_WINDOWS_1256		17		/* Arabic */
#define AC_WINDOWS_1257		18		/* Baltic */
#define AC_WINDOWS_936		19		/* Simplified Chinese (GB2312) */
#define AC_WINDOWS_950		21		/* Traditional Chinese (BIG5) */
#define AC_WINDOWS_932		23		/* Shift-JIS */ 
#define AC_WINDOWS_949		25		/* Korean */
#define AC_WINDOWS_874		27		/* Thai */

/* EUC Codepages */
#define AC_EUC_CN			20		/* Simplified Chinese (EUC) */
#define AC_EUC_TW			22		/* Traditional Chinese (EUC) */
#define AC_EUC_JP			24		/* Japanese (EUC) */
#define AC_EUC_KR			26		/* Korean (EUC) */

/* Unicode */
#define AC_UTF_8			28		/* UTF-8, a type of Unicode encoding     */
#define AC_UCS_2			29		/* UCS-2, a type of Unicode encoding     */
									/* UCS-2 is reserved for Actuate         */
									/* internal use. Not supported in RSAPI. */

#define	AC_ISO_8859_11		30		/*	Latin/Thai character set( TIS620 )	 */
#define	AC_THAI_EXTENDED	31		/*	For Actuate internal use only		 */

#endif /* __RSAPI_CPID_H__ */

/* End of file */

