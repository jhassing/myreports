#define MAX_LENGTH      1024
#define MAX_PROPERTIES  20

#ifdef __cpluscplus
extern "C" {
#endif

extern AcRSSELogFunc rsse_logFunction;

#define LOG_DEBUG(msg)  (*rsse_logFunction)(AC_RSSE_LOG_DEBUG, msg)
#define LOG_INFORM(msg) (*rsse_logFunction)(AC_RSSE_LOG_INFORM, msg)
#define LOG_WARN(msg)   (*rsse_logFunction)(AC_RSSE_LOG_WARN, msg)
#define LOG_FATAL(msg)  (*rsse_logFunction)(AC_RSSE_LOG_FATAL, msg)

typedef int (*TableFunc)(char* err, const char* arg, ...);

typedef struct {
	int		propID;
	const char*	propName;
	TableFunc	verifyFunc;
	void*		verifyData;
	void*		defaultData;
} PropEntry;

AcRSSEStatus
initialize(const char *serverHome, AcRSSELogFunc lf);

void stop();
char const *getDN(const char *user);

AcRSSEStatus
getIntegrationLevel(AcRSSEIntegrationLevel * level);

AcRSSEStatus
authenticateUser(const char *user, const char *password,
   const char *extendedCredentials, unsigned long extendedCredentialsLength);

AcRSSEStatus
isUserPropertyExternal(AcRSSEProperty propID, AcRSSEBoolean * isExternal);


AcRSSEStatus
startUserProperties(AcRSSEUserPropHandle * handle, const char *user,
		    const AcRSSEProperty * propIDs);


AcRSSEStatus
getUserProperty(AcRSSEUserPropHandle handle, AcRSSEProperty propID,
		char *propValue, unsigned long valueLength);

AcRSSEStatus
endUserProperties(AcRSSEUserPropHandle handle);

AcRSSEStatus
startUserDefPriv(
		 AcRSSEUserDefPrivHandle * handle,
		 const char *user);


AcRSSEStatus
getNextDefPriv(
	       AcRSSEUserDefPrivHandle handle,
	       AcRSSEPrivType * roleOrUser,
	       char *name,
	       unsigned long nameLength,
	       AcRSSEPrivMask * permissionMask);

AcRSSEStatus
endUserDefPriv(AcRSSEUserDefPrivHandle handle);

AcRSSEStatus
startUserChannels(AcRSSEChannelsOfUserHandle * handle, const char *user);

AcRSSEStatus
getNextChannel(AcRSSEChannelsOfUserHandle handle, 
		char *propValue, unsigned long valueLength);

AcRSSEStatus
endUserChannels(AcRSSEChannelsOfUserHandle handle);

AcRSSEStatus
doesUserExist(const char *user, AcRSSEBoolean * exists);

AcRSSEStatus
doesRoleExist(const char *role, AcRSSEBoolean * exists);

AcRSSEStatus
startUserQuery(AcRSSEUserHandle * handle,
	       const char *pattern,
	       unsigned long maxNumber);

AcRSSEStatus
getNextUser(AcRSSEUserHandle handle,
	    char *buffer,
	    unsigned long bufferLength);

AcRSSEStatus
endUserQuery(AcRSSEUserHandle handle);

AcRSSEStatus
startRoleQuery(AcRSSERoleHandle * handle,
	       const char *pattern,
	       unsigned long maxNumber);

AcRSSEStatus
getNextRole(AcRSSERoleHandle handle,
	    char *buffer,
	    unsigned long bufferLength);

AcRSSEStatus
endRoleQuery(AcRSSERoleHandle handle);

AcRSSEStatus
startGroupQuery(AcRSSEGroupHandle * handle,
		const char *pattern,
		unsigned long maxNumber);

AcRSSEStatus
getNextGroup(AcRSSEGroupHandle handle,
	     char *buffer,
	     unsigned long bufferLength);

AcRSSEStatus
endGroupQuery(AcRSSEGroupHandle handle);


AcRSSEStatus
startRolesOfUser(AcRSSERolesOfUserHandle * handle,
		 const char *user);

AcRSSEStatus
getNextRoleOfUser(AcRSSERolesOfUserHandle handle,
		  char *buffer,
		  unsigned long bufferLength);


AcRSSEStatus
endRolesOfUser(AcRSSERolesOfUserHandle handle);


AcRSSEStatus
startGroupUserNames(AcRSSEUsersOfGroupHandle * handle,
		    const char *group);


AcRSSEStatus
getNextGroupUserName(AcRSSEUsersOfGroupHandle handle,
		     char *buffer,
		     unsigned long bufferLength);

AcRSSEStatus
endGroupUserNames(AcRSSEUsersOfGroupHandle handle);

AcRSSEStatus
getTranslatedRoleName(const char *internalRole,
		      char *externalRole, unsigned long roleLength);

AcRSSEStatus
startUsersToNotify(AcRSSEUsersToNotifyHandle * handle);

AcRSSEStatus
getNextUserToNotify(AcRSSEUsersToNotifyHandle handle,
			   char *buffer, unsigned long bufferLength);

AcRSSEStatus
endUsersToNotify(AcRSSEUsersToNotifyHandle handle);


#ifdef __cpluscplus
}
#endif

