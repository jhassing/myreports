/*
 * The following ifdef block is the standard way of creating macros which make
 * exporting from a DLL simpler. All files within this DLL are compiled with
 * the RSSE_EXPORTS symbol defined on the command line. This symbol should not
 * be defined on any project that uses this DLL. This way any other project
 * whose source files include this file see RSSE_API functions as being
 * imported from a DLL, wheras this DLL sees symbols defined with this macro
 * as being exported.
 */

#ifndef __EXAMPLES_RSSE_H__
#define __EXAMPLES_RSSE_H__

#if defined (_WIN32)

#ifdef RSSE_EXPORTS
#define RSSE_API __declspec(dllexport)
#else
#define RSSE_API __declspec(dllimport)
#endif

#else
#define RSSE_API
#endif

#if defined(__cplusplus)
extern  "C"
{
#endif

typedef enum {
	AC_RSSE_OK = 0,
	AC_RSSE_PROPERTY_NOT_FOUND = 10,
	AC_RSSE_WARNING_LAST = 100,
	AC_RSSE_ERROR_OVERFLOW = 101,
	AC_RSSE_ERROR_LOOKUP_FAILED = 102,
	AC_RSSE_ERROR_INVALID_USER = 103,
	AC_RSSE_ERROR_INVALID_PASSWORD = 104,
	AC_RSSE_ERROR_AUTHENTICATION_FAILED = 105,
	AC_RSSE_ERROR_TIMELIMIT_EXCEEDED = 106,
	AC_RSSE_ERROR_UNKNOWN = 107,
	AC_RSSE_ERROR_NOT_RESPONSIBLE = 108,
	AC_RSSE_ERROR_AT_END = 109
} AcRSSEStatus;

typedef enum {
	AC_RSSE_LOG_DEBUG = 0,
	AC_RSSE_LOG_INFORM = 7000,
	AC_RSSE_LOG_WARN = 8000,
	AC_RSSE_LOG_FATAL = 8000
} AcRSSELogLevel;

typedef enum {
	AC_RSSE_NULL_PROPERTY = 0,
	AC_RSSE_USER_EMAIL_ID = 10,
	AC_RSSE_USER_HOME_FOLDER = 11,
	AC_RSSE_USER_EMAIL_FORM = 12,
/* internal name NOTIFICATION_TYPE */
	AC_RSSE_USER_EMAIL_WHEN = 13,
/* internal name SERVER_NOTICE */
	AC_RSSE_USER_FOLDER_WHEN = 14,
	AC_RSSE_USER_SUCCESS_NOTICE_EXPIRATION = 15,
	AC_RSSE_USER_FAIL_NOTICE_EXPIRATION = 16,
	AC_RSSE_USER_DEFAULT_OBJECT_PRIVILEGES = 17,
	AC_RSSE_USER_MAX_PRIORITY = 18,
	AC_RSSE_USER_MAX_COMPLETED = 19,
	AC_RSSE_USER_VIEWING_PREFERENCE = 20,
	AC_RSSE_USER_CHANNEL_SUBSCRIPTION_LIST = 21
} AcRSSEProperty;

#if !defined (__AC_SECURITY_INTEGRATION_LEVEL)
#define __AC_SECURITY_INTEGRATION_LEVEL

typedef enum {
	AC_RSSE_EXTERNAL_NONE = 0,
	AC_RSSE_EXTERNAL_AUTHENTICATION = 1,
	AC_RSSE_EXTERNAL_REGISTRATION = 2
} AcRSSEIntegrationLevel;

#endif

typedef enum {
	AC_RSSE_FALSE = 0,
	AC_RSSE_TRUE = 1
} AcRSSEBoolean;

typedef void *AcRSSEHandle;

/*****************************************************************************/
/********* Functions used by page security and above  ************************/
/*****************************************************************************/

/* We cannot change the return type on the AcRSSEStart function we use this
function in the page security example*/

RSSE_API void AcRSSEStart(char *serverHome);

RSSE_API int AcRSSEGetUserACL (char* userName, char* buffer, int length);

RSSE_API void AcRSSEStop();


/*****************************************************************************/
/********* Functions used by external authentication and above ***************/
/*****************************************************************************/
typedef void (*AcRSSELogFunc)(int level, const char* msg);

RSSE_API AcRSSEStatus 
AcRSSEStartEx(const char *serverHome,
	      AcRSSELogFunc logFunction);


RSSE_API AcRSSEStatus AcRSSEGetIntegrationLevel(AcRSSEIntegrationLevel * level);

RSSE_API AcRSSEStatus
AcRSSEAuthenticate(const char *userName,
		   const char *password,
		   const char *extendedCredentials,
		   unsigned long extendedCredentialsLength);

RSSE_API AcRSSEStatus 
AcRSSEGetEncoding(unsigned int* encoding);

/*****************************************************************************/
/********* Functions used by external properties and above *******************/
/*****************************************************************************/

RSSE_API AcRSSEStatus 
AcRSSEIsUserPropertyExternal(AcRSSEProperty property,
			     AcRSSEBoolean * isExternal);

RSSE_API AcRSSEStatus 
AcRSSEPassthrough(const char *input,
		  unsigned long inputLength,
		  int *returnCode,
		  char *output,
		  unsigned long *outputLength);

/*-----------------------------------------------------------------------------
 * The following functions work together to find an external user.
 */

typedef AcRSSEHandle AcRSSEUserPropHandle;

/* props are null terminated */
RSSE_API AcRSSEStatus
AcRSSEStartUserProperties(AcRSSEUserPropHandle * handle,
			  const char *user,
			  const AcRSSEProperty * properties);

RSSE_API AcRSSEStatus 
AcRSSEGetUserProperty(AcRSSEUserPropHandle handle,
		      AcRSSEProperty propertyName,
		      char *propertyValue,
		      unsigned long valueLength);

RSSE_API AcRSSEStatus AcRSSEEndUserProperties(AcRSSEUserPropHandle handle);

/*-----------------------------------------------------------------------------
 * The following functions to fetch user default privledges.
 */
typedef AcRSSEHandle AcRSSEUserDefPrivHandle;

typedef enum {
	AC_RSSE_IS_USER = 1,
	AC_RSSE_IS_ROLE = 2
} AcRSSEPrivType;

typedef unsigned long AcRSSEPrivMask;
#define	AC_RSSE_PRIV_READ		0x01
#define	AC_RSSE_PRIV_WRITE		0x02
#define	AC_RSSE_PRIV_EXECUTE		0x04
#define	AC_RSSE_PRIV_DELETE		0x08
#define	AC_RSSE_PRIV_VISIBLE		0x10
#define	AC_RSSE_PRIV_SECURE_READ	0x20
#define	AC_RSSE_PRIV_GRANT		0x40

RSSE_API AcRSSEStatus 
AcRSSEStartUserDefPriv(AcRSSEUserDefPrivHandle * handle,
		       const char *user);

RSSE_API AcRSSEStatus 
AcRSSEGetNextDefPriv(AcRSSEUserDefPrivHandle handle,
		     AcRSSEPrivType * roleOrUser,
		     char *name,
		     unsigned long nameLength,
		     AcRSSEPrivMask * permissionsMask);

RSSE_API AcRSSEStatus AcRSSEEndUserDefPriv(AcRSSEUserDefPrivHandle handle);

/*-----------------------------------------------------------------------------
 * The following functions to fetch channels of user
 */
typedef AcRSSEHandle AcRSSEChannelsOfUserHandle;

RSSE_API AcRSSEStatus 
AcRSSEStartChannelsOfUser(AcRSSEChannelsOfUserHandle * handle,
			  const char *user);

RSSE_API AcRSSEStatus 
AcRSSEGetNextChannelOfUser(AcRSSEChannelsOfUserHandle handle,
			   char *name,
			   unsigned long nameLength);

RSSE_API AcRSSEStatus AcRSSEEndChannelsOfUser(AcRSSEChannelsOfUserHandle handle);

/*****************************************************************************/
/********* Functions used by externally registered users and above ***********/
/*****************************************************************************/

RSSE_API AcRSSEStatus 
AcRSSEDoesUserExist(const char *user,
		    AcRSSEBoolean * exists);

RSSE_API AcRSSEStatus 
AcRSSEDoesRoleExist(const char *role,
		    AcRSSEBoolean * exists);

RSSE_API AcRSSEStatus
AcRSSEGetTranslatedRoleName(const char *internalRole,
							char *externalRole,
							unsigned long roleLength);

/*-----------------------------------------------------------------------------
 * The following functions work together to find an external user.
 */
typedef AcRSSEHandle AcRSSEUserHandle;

RSSE_API AcRSSEStatus 
AcRSSEStartUserQuery(AcRSSEUserHandle * handle,
		     const char *pattern,
		     unsigned long maxNumber);

RSSE_API AcRSSEStatus 
AcRSSEGetNextUser(AcRSSEUserHandle handle,
		  char *buffer,
		  unsigned long bufferLength);

RSSE_API AcRSSEStatus AcRSSEEndUserQuery(AcRSSEUserHandle handle);

/*-----------------------------------------------------------------------------
 * The following functions work together to find an external role.
 */
typedef AcRSSEHandle AcRSSERoleHandle;

RSSE_API AcRSSEStatus 
AcRSSEStartRoleQuery(AcRSSERoleHandle * handle,
		     const char *pattern,
		     unsigned long maxNumber);

RSSE_API AcRSSEStatus 
AcRSSEGetNextRole(AcRSSERoleHandle handle,
		  char *buffer,
		  unsigned long bufferLength);

RSSE_API AcRSSEStatus AcRSSEEndRoleQuery(AcRSSERoleHandle handle);

/*-----------------------------------------------------------------------------
 * The following functions work together to find an external group.
 */
typedef AcRSSEHandle AcRSSEGroupHandle;

RSSE_API AcRSSEStatus 
AcRSSEStartGroupQuery(AcRSSEGroupHandle * handle,
		      const char *pattern,
		      unsigned long maxNumber);

RSSE_API AcRSSEStatus 
AcRSSEGetNextGroup(AcRSSEGroupHandle handle,
		   char *buffer,
		   unsigned long bufferLength);

RSSE_API AcRSSEStatus AcRSSEEndGroupQuery(AcRSSEGroupHandle handle);

/*-----------------------------------------------------------------------------
 * The following functions work together to list roles of an external user.
 */
typedef AcRSSEHandle AcRSSERolesOfUserHandle;

RSSE_API AcRSSEStatus 
AcRSSEStartRolesOfUser(AcRSSERolesOfUserHandle * handle,
		       const char *user);

RSSE_API AcRSSEStatus 
AcRSSEGetNextRoleOfUser(AcRSSERolesOfUserHandle handle,
			char *buffer, unsigned long bufferLength);

RSSE_API AcRSSEStatus AcRSSEEndRolesOfUser(AcRSSERolesOfUserHandle handle);

/*-----------------------------------------------------------------------------
 * The following functions work together to list users of an external group.
 */
typedef AcRSSEHandle AcRSSEUsersOfGroupHandle;

RSSE_API AcRSSEStatus
AcRSSEStartGroupUserNames(AcRSSEUsersOfGroupHandle * handle,
			  const char *group);

RSSE_API AcRSSEStatus
AcRSSEGetNextGroupUserName(AcRSSEUsersOfGroupHandle handle,
			   char *buffer, unsigned long bufferLength);

RSSE_API AcRSSEStatus
        AcRSSEEndGroupUserNames(AcRSSEUsersOfGroupHandle handle);

/*-----------------------------------------------------------------------------
 * The following functions work together to list users who are to get
 * status for all requests in their completed request folders.
 */
typedef AcRSSEHandle AcRSSEUsersToNotifyHandle;

RSSE_API AcRSSEStatus
AcRSSEStartUsersToNotify(AcRSSEUsersToNotifyHandle * handle);

RSSE_API AcRSSEStatus
AcRSSEGetNextUserToNotify(AcRSSEUsersToNotifyHandle handle,
			   char *buffer, unsigned long bufferLength);

RSSE_API AcRSSEStatus
        AcRSSEEndUsersToNotify(AcRSSEUsersToNotifyHandle handle);

/*****************************************************************************/
/********* External authorization functions **********************************/
/*****************************************************************************/

RSSE_API AcRSSEStatus
AcRSSEGetPrivilegesOnFolderItem(const char *user, const char *path,
								unsigned long versionNumber,
								AcRSSEPrivMask *permissionsMask);

RSSE_API AcRSSEStatus
AcRSSEGetPrivilegesOnChannel(const char *user, const char *channelName,
							 AcRSSEPrivMask *permissionsMask);

#if defined(__cplusplus)
}
#endif

#endif

