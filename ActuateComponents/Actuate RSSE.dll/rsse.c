#ifdef _WIN32
#include "stdafx.h"
#endif

#include "rsse.h"
#include "implementation.h"
#include "cpid.h"


#ifdef _WIN32
BOOL    APIENTRY
DllMain(HANDLE hModule,
	DWORD ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call) {
			case DLL_PROCESS_ATTACH:
			case DLL_THREAD_ATTACH:
			case DLL_THREAD_DETACH:
			case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}

#endif

#ifdef _AIX
void
__start()
{;
}

#endif

/*
 *
 * //--------------------------------------------------------------------------
 * // LDAP DLL Interface
 *
 * // This is the C-style interface that the Report Server calls. Each function
 * turns // around and calls a platform independent implementation function.
 */

#ifdef __cpluscplus
extern  "C" {
#endif

/*****************************************************************************/
/********* Functions used by level 1 and above *******************************/
/*****************************************************************************/

RSSE_API AcRSSEStatus 
AcRSSEStartEx(const char *serverHome,
	      void (*logFunction) (int level, const char *string))
{
	return initialize(serverHome, logFunction);
}

RSSE_API void 
AcRSSEStop()
{
}

RSSE_API AcRSSEStatus 
AcRSSEGetIntegrationLevel(AcRSSEIntegrationLevel * level)
{
	return (getIntegrationLevel(level));
}

RSSE_API AcRSSEStatus 
AcRSSEGetEncoding(unsigned int* encoding)
{
	*encoding = AC_UTF_8;
	return AC_RSSE_OK;
}

RSSE_API AcRSSEStatus 
AcRSSEAuthenticate(const char *userName, const char *password,
   const char *extendedCredentials, unsigned long extendedCredentialsLength)
{
	return authenticateUser(userName, password, extendedCredentials, extendedCredentialsLength);
}

/*****************************************************************************/
/********* Functions used by level 2 and above *******************************/
/*****************************************************************************/

RSSE_API AcRSSEStatus 
AcRSSEIsUserPropertyExternal(AcRSSEProperty property, AcRSSEBoolean * isExternal)
{
	return (isUserPropertyExternal(property, isExternal));
}

 /* For the current example, this function does not do anything */
RSSE_API AcRSSEStatus 
AcRSSEPassThrough(const char *input, unsigned long inputLength,
		  int *returnCode, char *output, unsigned long *outputLength)
{
	return AC_RSSE_OK;
}

/*-----------------------------------------------------------------------------
 * The following functions work together to find properties of an external user.
 */

RSSE_API AcRSSEStatus 
AcRSSEStartUserProperties(AcRSSEUserPropHandle * handle, const char *user,
			  const AcRSSEProperty * properties)
{
	return (startUserProperties(handle, user, properties));
}

RSSE_API AcRSSEStatus 
AcRSSEGetUserProperty(AcRSSEUserPropHandle handle, AcRSSEProperty propertyName,
		      char *propertyValue, unsigned long valueLength)
{
	return (getUserProperty(handle, propertyName, propertyValue, valueLength));
}

RSSE_API AcRSSEStatus 
AcRSSEEndUserProperties(AcRSSEUserPropHandle handle)
{
	return (endUserProperties(handle));
}

/*-----------------------------------------------------------------------------
 * The following functions to fetch user default privledges.
 */

RSSE_API AcRSSEStatus 
AcRSSEStartUserDefPriv(AcRSSEUserDefPrivHandle * handle,
		       const char *user)
{
	return startUserDefPriv(handle, user);
}

RSSE_API AcRSSEStatus 
AcRSSEGetNextDefPriv(AcRSSEUserDefPrivHandle handle,
		     AcRSSEPrivType * roleOrUser,
		     char *name,
		     unsigned long nameLength,
		     AcRSSEPrivMask * permissionMask)
{
	return getNextDefPriv(handle, roleOrUser, name, nameLength, permissionMask);
}

RSSE_API AcRSSEStatus 
AcRSSEEndUserDefPriv(AcRSSEUserDefPrivHandle handle)
{
	return endUserDefPriv(handle);
}

/*-----------------------------------------------------------------------------
 * The following functions to fetch channels of user
 */

RSSE_API AcRSSEStatus 
AcRSSEStartChannelsOfUser(AcRSSEChannelsOfUserHandle * handle,
			  const char *user)
{
	return startUserChannels(handle, user);
}

RSSE_API AcRSSEStatus 
AcRSSEGetNextChannelOfUser(AcRSSEChannelsOfUserHandle handle,
			   char *name,
			   unsigned long nameLength)
{
	return getNextChannel(handle, name, nameLength);
}

RSSE_API AcRSSEStatus 
AcRSSEEndChannelsOfUser(AcRSSEChannelsOfUserHandle handle)
{
	return endUserChannels(handle);
}

/*****************************************************************************/
/********* Functions used by level 3 and above *******************************/
/*****************************************************************************/

RSSE_API AcRSSEStatus 
AcRSSEDoesUserExist(const char *user,
		    AcRSSEBoolean * exists)
{
	return doesUserExist(user, exists);
}

RSSE_API AcRSSEStatus 
AcRSSEDoesRoleExist(const char *role,
		    AcRSSEBoolean * exists)
{
	return doesRoleExist(role, exists);
}

/*-----------------------------------------------------------------------------
 * The following functions work together to find an external user.
 */

RSSE_API AcRSSEStatus 
AcRSSEStartUserQuery(AcRSSEUserHandle * handle,
		     const char *pattern,
		     unsigned long maxNumber)
{
	return startUserQuery(handle, pattern, maxNumber);
}

RSSE_API AcRSSEStatus 
AcRSSEGetNextUser(AcRSSEUserHandle handle,
		  char *buffer,
		  unsigned long bufferLength)
{
	return getNextUser(handle, buffer, bufferLength);
}


RSSE_API AcRSSEStatus 
AcRSSEEndUserQuery(AcRSSEUserHandle handle)
{
	return endUserQuery(handle);
}

/*-----------------------------------------------------------------------------
 * The following functions work together to find an external role.
 */

RSSE_API AcRSSEStatus 
AcRSSEStartRoleQuery(AcRSSERoleHandle * handle,
		     const char *pattern,
		     unsigned long maxNumber)
{
	return startRoleQuery(handle, pattern, maxNumber);
}

RSSE_API AcRSSEStatus 
AcRSSEGetNextRole(AcRSSERoleHandle handle,
		  char *buffer,
		  unsigned long bufferLength)
{
	return getNextRole(handle, buffer, bufferLength);
}

RSSE_API AcRSSEStatus 
AcRSSEEndRoleQuery(AcRSSERoleHandle handle)
{
	return endRoleQuery(handle);
}

/*-----------------------------------------------------------------------------
 * The following functions work together to find an external group.
 */

RSSE_API AcRSSEStatus 
AcRSSEStartGroupQuery(AcRSSEGroupHandle * handle,
		      const char *pattern,
		      unsigned long maxNumber)
{
	return startGroupQuery(handle, pattern, maxNumber);
}


RSSE_API AcRSSEStatus 
AcRSSEGetNextGroup(AcRSSEGroupHandle handle,
		   char *buffer,
		   unsigned long bufferLength)
{
	return getNextGroup(handle, buffer, bufferLength);
}

RSSE_API AcRSSEStatus 
AcRSSEEndGroupQuery(AcRSSEGroupHandle handle)
{
	return endGroupQuery(handle);
}

/*-----------------------------------------------------------------------------
 * The following functions work together to list roles of an external user.
 */

RSSE_API AcRSSEStatus 
AcRSSEStartRolesOfUser(AcRSSERolesOfUserHandle * handle,
		       const char *user)
{
	return startRolesOfUser(handle, user);
}

RSSE_API AcRSSEStatus 
AcRSSEGetNextRoleOfUser(AcRSSERolesOfUserHandle handle,
			char *buffer, unsigned long bufferLength)
{
	return getNextRoleOfUser(handle, buffer, bufferLength);
}


RSSE_API AcRSSEStatus 
AcRSSEEndRolesOfUser(AcRSSERolesOfUserHandle handle)
{
	return endRolesOfUser(handle);
}

/*-----------------------------------------------------------------------------
 * The following functions work together to list users of an external group.
 */

RSSE_API AcRSSEStatus 
AcRSSEStartGroupUserNames(AcRSSEUsersOfGroupHandle * handle,
			  const char *group)
{
	return startGroupUserNames(handle, group);
}


RSSE_API AcRSSEStatus 
AcRSSEGetNextGroupUserName(AcRSSEUsersOfGroupHandle handle,
			   char *buffer, unsigned long bufferLength)
{
	return getNextGroupUserName(handle, buffer, bufferLength);
}


RSSE_API AcRSSEStatus 
AcRSSEEndGroupUserNames(AcRSSEUsersOfGroupHandle handle)
{
	return endGroupUserNames(handle);
}


/*-----------------------------------------------------------------------------
 * The following functions work together to find the users to notify.
 */


RSSE_API AcRSSEStatus
AcRSSEStartUsersToNotify(AcRSSEUsersToNotifyHandle * handle) {

	return startUsersToNotify ((AcRSSEUsersOfGroupHandle*)handle);
}

RSSE_API AcRSSEStatus
AcRSSEGetNextUserToNotify(AcRSSEUsersToNotifyHandle handle,
					char *buffer, unsigned long bufferLength) {
	return getNextUserToNotify((AcRSSEUsersOfGroupHandle) handle,buffer, bufferLength);
}

RSSE_API AcRSSEStatus
AcRSSEEndUsersToNotify(AcRSSEUsersToNotifyHandle handle) {
	return endUsersToNotify((AcRSSEUsersOfGroupHandle) handle);
}


/*-----------------------------------------------------------------------------
 * Functions that support special roles
 */


RSSE_API AcRSSEStatus
AcRSSEGetTranslatedRoleName(const char *internalRole,
			    char *externalRole, unsigned long roleLength)
{
	return getTranslatedRoleName(internalRole, externalRole, roleLength);
}

#ifdef __cpluscplus
}
#endif


/* End of file. */
