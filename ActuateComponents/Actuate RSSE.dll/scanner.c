#include "rsse.h"
#include "implementation.h"

#ifdef _WIN32
#include "stdafx.h"
#endif

#include <malloc.h>
#include <stdio.h>
#include <ctype.h>

#ifdef _WIN32
#include <string.h>
#define strcasecmp _stricmp
#define index strchr
#else
#include <strings.h>
#endif

#ifdef __cpluscplus
extern "C" {
#endif

int ldapPort			= 0;
char *userBaseDN		= NULL;
char *userSearchString	= NULL;
char *userObject		= NULL;
char *roleBaseDN		= NULL;
char *roleObject		= NULL;
char *groupBaseDN		= NULL;
char *groupObject		= NULL;
char *allRoleName		= NULL;
char *operatorRoleName	= NULL;
char *adminRoleName		= NULL;
char *queryAccount		= NULL;
char *queryPassword		= NULL;
char *ldapServer		= NULL;
char *groupToNotify		= NULL;

static char** split_fields(char* arg)
{
	char**  vals;
	char**	val_ptr;
	char*   ptr;
	int     num_fields;
	int entry_counter=0;

	ptr = arg;
	num_fields = 0;
	while (*ptr) {
		if (*ptr == ',') num_fields++;	/* count all commas */
		ptr++;
	}
	num_fields += 2;	/* there is one more field than number of commas */
                        /* and one slot for null terminator */

	vals = (char **) malloc(num_fields * sizeof(char*));

	ptr = arg;				/* initialize pointer to head of master string */
	val_ptr = vals;				/* initialize pointer to head of pointer array */
	while (*ptr) {
		char    *fptr;
		char    field[256];

		fptr = field;
		*fptr = 0;			/* make sure copy space is null terminated */
		while (*ptr && *ptr != ',') {
			*fptr++ = *ptr++;
		}
		*fptr = 0;		/* make sure copy space is null terminated */
		*val_ptr++ = strdup(field);
		entry_counter++;
		if (*ptr == 0) break;		/* if you have null (not comma) then quit */
		ptr++;						/* move past comma */
		while (isspace(*ptr) && (entry_counter!=(num_fields-1))) {  
		/*stripping of the leading white space after a comma */
			*ptr++;
		}
	}
	*val_ptr = 0;				/* null terminate array of pointers */

	return vals;
}

static int
verifyInteger(char* err, const char* arg)
{
	int	junk;

	if (sscanf(arg, "%d", &junk) != 1) {
		sprintf(err, "Can't convert \"%s\" to an integer", arg);
		return -1;
	}
	return 0;
}

static int
verifyString(char* err, const char* arg, const char** vals)
{
	const char** ptr = vals;

	while (*ptr) {
		if (strcasecmp(arg, *ptr) == 0) return 0;
		ptr++;
	}

	sprintf(err, "\"%s\" is not one of <", arg);
	ptr = vals;
	while (*ptr) {
		strcat(err, *ptr);
		if (ptr[1]) strcat(err, ", ");
		ptr++;
	}
	strcat(err, ">");
	return -1;
}

static PropEntry propTable[MAX_PROPERTIES];

int propTableMax = 0;

PropEntry* propByID(AcRSSEProperty propID)
{
	int	i;

	for (i = 0; i < propTableMax; i++) {
		if (propTable[i].propID == propID) return &propTable[i];
	}
	return 0;
}

static void propInsert(AcRSSEProperty propID, const char* propName, TableFunc verifyFunc, void* verifyData, void* defaultData)
{
	int	i;

	for (i = 0; i < propTableMax; i++) {
		if (propTable[i].propID == propID) {
			propTable[i].verifyFunc = verifyFunc;
			propTable[i].verifyData = verifyData;
			propTable[i].defaultData = defaultData;
			return;
		}
	}

	propTable[i].propID = propID;
	propTable[i].propName = strdup(propName);
	propTable[i].verifyFunc = verifyFunc;
	propTable[i].verifyData = verifyData;
	propTable[i].defaultData = defaultData;
	propTableMax++;
}

const char* propIDToAttr(AcRSSEProperty propID)
{
	PropEntry*	p = propByID(propID);

	if (p == 0) return 0;
	return p->propName;
}

AcRSSEProperty propStringToID(const char* propName)
{
	int	i;

	for (i = 0; i < propTableMax; i++) {
		if (strcmp(propTable[i].propName, propName) == 0)
			return (AcRSSEProperty)propTable[i].propID;
	}
	return AC_RSSE_NULL_PROPERTY;
}

static int
getServerName(char *arg, char *err)
{
	ldapServer = strdup(arg);
	return 0;
}

static int
getPortNo(char *arg, char *err)
{
	if (sscanf(arg, "%d", &ldapPort) != 1) {
		sprintf(err, "Can't convert \"%s\" to an integer", arg);
		return -1;
	}
	return 0;
}

static int
getUserBaseDN(char *arg, char *err)
{
	userBaseDN = strdup(arg);
	return 0;
}

static int
getUserSearchString(char *arg, char *err)
{
	userSearchString = strdup(arg);
	return 0;
}

static int
getUserObject(char *arg, char *err)
{
	userObject = strdup(arg);
	return 0;
}

static int
getRoleBaseDN(char *arg, char *err)
{
	roleBaseDN = strdup(arg);
	return 0;
}

static int
getRoleObject(char *arg, char *err)
{
	roleObject = strdup(arg);
	return 0;
}

static int
getGroupBaseDN(char *arg, char *err)
{
	groupBaseDN = strdup(arg);
	return 0;
}

static int
getGroupObject(char *arg, char *err)
{
	groupObject = strdup(arg);
	return 0;
}

static int
getGroupToNotify(char *arg, char *err)
{
	groupToNotify = strdup(arg);
	return 0;
}

static int
getQueryAccount(char *arg, char *err)
{
	queryAccount = strdup(arg);
	return 0;
}

static int
getQueryPassword(char *arg, char *err)
{
	queryPassword = strdup(arg);
	return 0;
}

static int getAllRoleName(char* arg, char* err)
{
	allRoleName = strdup(arg);
	return 0;
}

static int getOperatorRoleName(char* arg, char* err)
{
	operatorRoleName = strdup(arg);
	return 0;
}

static int getAdminRoleName(char* arg, char* err)
{
	adminRoleName = strdup(arg);
	return 0;
}

static int
getUserEmailIDAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_EMAIL_ID, arg, 0, 0, 0);
	return 0;
}

static int
getUserHomeFolderAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_HOME_FOLDER, arg, 0, 0, 0);
	return 0;
}

/*----------------------------------------------------------*/

static const char *when_vals[] = {
	"never",
	"always",
	"failures",
	"successes",
	0
};

static int
getUserEmailWhenAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_EMAIL_WHEN, arg, (TableFunc)verifyString, (void*)when_vals, 0);
	return 0;
}

static int
getUserEmailWhenDefault(char *arg, char *err)
{
	if (verifyString(err, arg, when_vals) < 0) return -1;
	propInsert(AC_RSSE_USER_EMAIL_WHEN, 0, (TableFunc)verifyString, (void*)when_vals, strdup(arg));
	return 0;
}

static int
getUserFolderWhenAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_FOLDER_WHEN, arg, (TableFunc)verifyString, (void*)when_vals, 0);
	return 0;
}

static int
getUserFolderWhenDefault(char *arg, char *err)
{
	if (verifyString(err, arg, when_vals) < 0) return -1;
	propInsert(AC_RSSE_USER_FOLDER_WHEN, 0, (TableFunc)verifyString, (void*)when_vals, strdup(arg));
	return 0;
}

/*----------------------------------------------------------*/
static const char *email_form_vals[] = {
	"included",
	"linked",
	0
};

static int
getUserEmailFormAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_EMAIL_FORM, arg, (TableFunc)verifyString, (void*)email_form_vals, 0);
	return 0;
}

static int
getUserEmailFormDefault(char *arg, char *err)
{
	if (verifyString(err, arg, email_form_vals) < 0) return -1;
	propInsert(AC_RSSE_USER_EMAIL_FORM, 0, (TableFunc)verifyString, (void*)email_form_vals, strdup(arg));
	return 0;
}

/*----------------------------------------------------------*/
static int
getUserMaxPrioAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_MAX_PRIORITY, arg, (TableFunc)verifyInteger, 0, 0);
	return 0;
}

static int
getUserMaxPrioDefault(char *arg, char *err)
{
	if (verifyInteger(err, arg) < 0) return -1;
	propInsert(AC_RSSE_USER_MAX_PRIORITY, 0, (TableFunc)verifyInteger, 0, strdup(arg));
	return 0;
}

/*----------------------------------------------------------*/
static int
getUserMaxCompletedAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_MAX_COMPLETED, arg, (TableFunc)verifyInteger, 0, 0);
	return 0;
}

static int
getUserMaxCompletedDefault(char *arg, char *err)
{
	if (verifyInteger(err, arg) < 0) return -1;
	propInsert(AC_RSSE_USER_MAX_COMPLETED, 0, (TableFunc)verifyInteger, 0, strdup(arg));
	return 0;
}

/*----------------------------------------------------------*/
static int
getUserSuccessNoticeExpAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_SUCCESS_NOTICE_EXPIRATION, arg, (TableFunc)verifyInteger, 0, 0);
	return 0;
}

static int
getUserSuccessNoticeExpDefault(char *arg, char *err)
{
	if (verifyInteger(err, arg) < 0) return -1;
	propInsert(AC_RSSE_USER_SUCCESS_NOTICE_EXPIRATION, 0, (TableFunc)verifyInteger, 0, strdup(arg));
	return 0;
}

/*----------------------------------------------------------*/
static int
getUserFailNoticeExpAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_FAIL_NOTICE_EXPIRATION, arg, (TableFunc)verifyInteger, 0, 0);
	return 0;
}

static int
getUserFailNoticeExpDefault(char *arg, char *err)
{
	if (verifyInteger(err, arg) < 0) return -1;
	propInsert(AC_RSSE_USER_FAIL_NOTICE_EXPIRATION, 0, (TableFunc)verifyInteger, 0, strdup(arg));
	return 0;
}

/*----------------------------------------------------------*/
static const char *view_pref_vals[] = {
	"default",
	"lrx",
	"dhtml",
	0
};

static int
getUserViewingPrefAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_VIEWING_PREFERENCE, arg, (TableFunc)verifyString, (void*)view_pref_vals, 0);
	return 0;
}

static int
getUserViewingPrefDefault(char *arg, char *err)
{
	if (verifyString(err, arg, view_pref_vals) < 0) return -1;
	propInsert(AC_RSSE_USER_VIEWING_PREFERENCE, 0, (TableFunc)verifyString, (void*)view_pref_vals, strdup(arg));
	return 0;
}

/*----------------------------------------------------------*/
static int
getUserDefaultPrivAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_DEFAULT_OBJECT_PRIVILEGES, arg, 0, 0, 0);
	return 0;
}

static int
getUserDefaultPrivDefault(char *arg, char *err)
{
	char** ptr;
	char** vals = split_fields(arg);

	ptr = vals;
	while (*ptr) {
		char	msg[256];
		sprintf(msg, "field value \"%s\"", *ptr);
		LOG_DEBUG(msg);
		ptr++;
	}

	propInsert(AC_RSSE_USER_DEFAULT_OBJECT_PRIVILEGES, arg, 0, 0, vals);
	return 0;
}

/*----------------------------------------------------------*/
static int
getUserChannelListAttr(char *arg, char *err)
{
	propInsert(AC_RSSE_USER_CHANNEL_SUBSCRIPTION_LIST, arg, 0, 0, 0);
	return 0;
}

static int
getUserChannelListDefault(char *arg, char *err)
{
	char** ptr;
	char** vals = split_fields(arg);

	ptr = vals;
	while (*ptr) {
		char	msg[256];
		sprintf(msg, "field value \"%s\"", *ptr);
		LOG_DEBUG(msg);
		ptr++;
	}

	propInsert(AC_RSSE_USER_CHANNEL_SUBSCRIPTION_LIST, arg, 0, 0, vals);
	return 0;
}

static struct {
	const char   *name;
	int     (*func) (char *, char *);
}       func_array[] = {

	"SERVER",									getServerName,
	"PORT",										getPortNo,
	"USER_BASE_DN",								getUserBaseDN,
	"USER_SEARCH_STRING",						getUserSearchString,
	"USER_OBJECT",								getUserObject,
	"ROLE_BASE_DN",								getRoleBaseDN,
	"ROLE_OBJECT",								getRoleObject,
	"GROUP_BASE_DN",							getGroupBaseDN,
	"GROUP_OBJECT",								getGroupObject,
	"GROUP_TO_NOTIFY",							getGroupToNotify,
	"QUERY_ACCOUNT",							getQueryAccount,
	"QUERY_PASSWORD",							getQueryPassword,
	"ALL_ROLE_NAME",							getAllRoleName,
	"OPERATOR_ROLE_NAME",						getOperatorRoleName,
	"ADMIN_ROLE_NAME",							getAdminRoleName,
	"USER_MAX_PRIO_ATTR",						getUserMaxPrioAttr,
	"USER_MAX_PRIO_DEFAULT",					getUserMaxPrioDefault,
	"USER_MAX_COMPLETED_ATTR",					getUserMaxCompletedAttr,
	"USER_MAX_COMPLETED_DEFAULT",				getUserMaxCompletedDefault,
	"USER_EMAIL_ID_ATTR",						getUserEmailIDAttr,
	"USER_HOME_FOLDER_ATTR",					getUserHomeFolderAttr,
	"USER_EMAIL_WHEN_ATTR",						getUserEmailWhenAttr,
	"USER_EMAIL_WHEN_DEFAULT",					getUserEmailWhenDefault,
	"USER_FOLDER_WHEN_ATTR",					getUserFolderWhenAttr,
	"USER_FOLDER_WHEN_DEFAULT",					getUserFolderWhenDefault,
	"USER_EMAIL_FORM_ATTR",						getUserEmailFormAttr,
	"USER_EMAIL_FORM_DEFAULT",					getUserEmailFormDefault,
	"USER_DEFAULT_PRIV_ATTR",					getUserDefaultPrivAttr,
	"USER_DEFAULT_PRIV_DEFAULT",				getUserDefaultPrivDefault,
	"USER_SUCCESS_NOTICE_EXPIRATION_ATTR",		getUserSuccessNoticeExpAttr,
	"USER_SUCCESS_NOTICE_EXPIRATION_DEFAULT",	getUserSuccessNoticeExpDefault,
	"USER_FAIL_NOTICE_EXPIRATION_ATTR",			getUserFailNoticeExpAttr,
	"USER_FAIL_NOTICE_EXPIRATION_DEFAULT",		getUserFailNoticeExpDefault,
	"USER_VIEWING_PREF_ATTR",					getUserViewingPrefAttr,
	"USER_VIEWING_PREF_DEFAULT",				getUserViewingPrefDefault,
	"USER_CHANNEL_LIST_ATTR",					getUserChannelListAttr,
	"USER_CHANNEL_LIST_DEFAULT",				getUserChannelListDefault,
	0, 0
};

int scanner(char *filename, char *err)
{
	FILE   *file;

	char    line_buffer[256];

	int     line_counter = 0;

	if ((file = fopen(filename, "r")) == NULL) {
		sprintf(err, "Failed to open file \"%s\".", filename);
		LOG_FATAL(err);
		return -1;
	}
	while (fgets(line_buffer, sizeof(line_buffer), file) != 0) {
		char	msg[256];
		int     (*func_ptr) (char *, char *);
		char   *leader = line_buffer;
		char   *trailer;
		int     i;
		int     num_quotes;

		i = strlen(line_buffer);
		line_buffer[i-1] = 0;

		sprintf(msg, "parsing \"%s\"", line_buffer);
		LOG_DEBUG(msg);

		line_counter++;
next_char:
		switch (*leader) {
			case ' ':
			case '\t':
				leader++;
				goto next_char;
			case 0:
			case '#':
				continue;
		}
		trailer = leader;
next_char2:
		switch (*trailer) {
			case ' ':
			case '\t':
				*trailer = 0;
				trailer++;
				goto next_char2;
			case ',':
				*trailer = 0;
				trailer++;
				LOG_DEBUG("breaking out");
				break;
			case 0:
				sprintf(err, "On line %d of \"%s\": Missing argument.", line_counter, filename);
				fclose(file);
				return -1;
			default:
				trailer++;
				goto next_char2;
		}

		LOG_DEBUG("starting function match loop");
		for (func_ptr = 0, i = 0; func_array[i].name; i++) {
			if (strcasecmp(leader, func_array[i].name) == 0) {
				sprintf(msg, "matched %s", func_array[i].name);
				LOG_DEBUG(msg);
				func_ptr = func_array[i].func;
				break;
			}
		}
		if (func_ptr == 0) {
			sprintf(err, "On line %d of \"%s\": Can't find keyword \"%s\"",
				line_counter, filename, leader);
			fclose(file);
			return -1;
		}
		for (num_quotes = 0, i = 0; trailer[i]; i++) {
			if (trailer[i] == '"')
				num_quotes++;
		}
		if (num_quotes == 0) {
			char    err_buf[100];

			while (*trailer == ' ' || *trailer == '\t')
				trailer++;
			if ((*func_ptr) (trailer, err_buf) < 0) {
				fclose(file);
				sprintf(err, "On line %d of \"%s\" (keyword \"%s\"): %s",
					line_counter, filename, leader, err_buf);
				return -1;
			}
		} else if (num_quotes == 2) {
			char    err_buf[100];

			char   *begin = index(trailer, '"') + 1;

			char   *fin = index(begin, '"');

			*fin = 0;
			if ((*func_ptr) (begin, err_buf) < 0) {
				fclose(file);
				sprintf(err, "On line %d of \"%s\" (Keyword \"%s\"): %s",
					line_counter, filename, leader, err_buf);
				return -1;
			}
		} else {
			sprintf(err, "On line %d of \"%s\": Quotes don't match.", line_counter, filename);
			fclose(file);
			return -1;
		}
	}
	fclose(file);
	return 0;
}

#ifdef __cpluscplus
}
#endif
