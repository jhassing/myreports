Function getDM(sParms as String) As String

    On Error GoTo ErrorHandler
    
    Dim sDM As String
    Dim nStart As Integer
        nStart = Len(getReportName(sParms)) + Len(getUnit(sParms)) + Len(getRegion(sParms)) + 5
    
    sDM = Mid(sParms, nStart, InStr(nStart, sParms, "~") - nStart)

    
    getDM = IIf(InStr(sDM, "~") = 0, sDM, "")
    
Exit Function

ErrorHandler:
    getDM = ""

End Function

Function getRegion(sParms as String) As String

    On Error GoTo ErrorHandler
    
    Dim sRegion As String
    Dim nStart As Integer
    
    nStart = Len(getReportName(sParms)) + Len(getUnit(sParms)) + 4
    
    sRegion = Mid(sParms, nStart, InStr(nStart, sParms, "~") - nStart)
    
    getRegion = IIf(InStr(sRegion, "~") = 0, sRegion, "")
    
Exit Function

ErrorHandler:
    getRegion = ""

End Function

Function getReportName(sParms as String) As String

    On Error GoTo ErrorHandler
    
    Dim sReportName As String
    
    sReportName = Mid$(sParms, 2, InStr((InStr(sParms, "~") + 1), sParms, "~") - 2)
    
    getReportName = IIf(InStr(sReportName, "~") = 0, sReportName, "")

Exit Function

ErrorHandler:
     getReportName = ""

End Function

Function getUnit(sParms as String) As String

    On Error GoTo ErrorHandler
    
    Dim sUnit As String
    Dim nStart As Integer
    
    nStart = Len(getReportName(sParms)) + 3
    
    sUnit = Mid(sParms, nStart, InStr(nStart, sParms, "~") - nStart)
    
    getUnit = IIf(InStr(sUnit, "~") = 0, sUnit, "")
    
Exit Function

ErrorHandler:
    getUnit = ""

End Function

Function getFolderStructure(sParms as String) As String

    On Error GoTo ErrorHandler
   
    Dim nTemp1 as integer
    Dim nTemp2 As Integer

    nTemp1  = InStr(sParms , "|") + 1
    nTemp2 = InStr(nTemp1, sParms, "|")
    
    getFolderStructure  = Trim$(Mid$(sParms, nTemp1, nTemp2 - nTemp1) + ".roi")
	    
Exit Function

ErrorHandler:
    getFolderStructure = ""

End Function

