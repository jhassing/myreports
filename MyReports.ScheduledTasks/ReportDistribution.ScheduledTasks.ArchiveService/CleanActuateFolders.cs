using System;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Burst.Common;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.ReportArchive;
using MyReports.Burst.SqlServerDal;
using Server_Proxy.localhost;

namespace ReportDistribution.ScheduledTasks.ArchiveService
{
    /// <summary>
    /// Summary description for CleanActuateFolders.
    /// </summary>
    public class CleanActuateFolders : ArchiveBase
    {
        private readonly DataSet _dsReportArchiveValues;
        private string[] _baseFolders;
        private int _daysToHoldReports;

        public CleanActuateFolders()
        {
            Reports oReports = new Reports(ConfigurationReader.Db);
            _dsReportArchiveValues = oReports.GetReportArchiveValues();
        }

        private void GetReportArchiveValues(string reportCode, out enum_OverwriteValues overwriteCode,
                                            out int archiveLength, out int reportTypeId,
                                            out enum_ArchiveTimeUnits archiveTimeUnits)
        {
            DataRow[] drRow = _dsReportArchiveValues.Tables[0].Select("ReportCode='" + reportCode.Split('.')[0] + "'");

            if (drRow.Length <= 0)
            {
                overwriteCode = 0;
                archiveLength = 0;
                reportTypeId = 0;
                archiveTimeUnits = 0;
                return;
            }

            overwriteCode = (enum_OverwriteValues) drRow[0]["OverwriteCode"];
            archiveLength = (int) drRow[0]["ArchiveLength"];
            reportTypeId = (int) drRow[0]["ReportTypeID"];
            archiveTimeUnits = (enum_ArchiveTimeUnits) drRow[0]["ArchiveValueID"];
        }

        /// <summary>
        /// Function should be called once a week or so. Will loop through all the RD folders inside actuate (not including the National 
        /// Accounts folders, and apply Archive business rules to any files older then 2 months.
        /// </summary>
        public void ArchiveOldFiles()
        {
            try
            {
                // Get the archive folder name (Base archive folder name)
                string[] sTemp1 = ConfigurationReader.RdSettings.FolderProperties.FolderNames.Split(',');

                string[] archiveFolderMonths =
                    ConfigurationReader.RdSettings.ArchiveProperties.ArchiveFolderMonths.Split(',');

                _baseFolders = new string[sTemp1.Length + archiveFolderMonths.Length];
                sTemp1.CopyTo(_baseFolders, 0);
                archiveFolderMonths.CopyTo(_baseFolders, sTemp1.Length);

                _daysToHoldReports = Convert.ToInt32(ConfigurationReader.RdSettings.ArchiveProperties.DaysToHoldReports);


                //  Console.WriteLine("FOR DEBUGGING ONLY:  _daysToHoldReports = 2:  COMMENT OUT BEFORE RELEASE!");
                //  _daysToHoldReports = 2;

                foreach (string t in sTemp1)
                {
                    try
                    {
                        string sFolderToSearch = "/" + t + "/";

                        GetFolderItemsResponse oActuateFileAttributes = GetDirectoryContents(sFolderToSearch);


                        try
                        {
                            foreach (File file in oActuateFileAttributes.ItemList)
                            {
                                switch (file.FileType.ToLower())
                                {
                                    case "directory":
                                        {
                                            string sFolderName = sFolderToSearch + file.Name;
                                            DirSearch(sFolderName);


                                            // check and see if the folder is empty.
                                            // if the folder is empty, delete the folder contents
                                            if (sFolderName.IndexOf("(0) Archives") == -1)
                                            {
                                                if (GetDirectoryContents(sFolderName).TotalCount == 0)
                                                {
                                                    DeleteFileFolder(sFolderToSearch, file.Name);
                                                    Console.WriteLine("* deleted  " + sFolderToSearch + "; " + file.Name);
                                                }
                                            }
                                        }
                                        break;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            ExceptionManager.Publish(ex);
                            continue;
                        }
                    }
                    catch (Exception ex)
                    {
                        ExceptionManager.Publish(ex);
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
            }
        }

        protected override void ProcessFileDirectory(string sDirectory, File oActuateFileAttributes)
        {
            string sSearch = oActuateFileAttributes.Name + "/";

            Console.WriteLine("searching folder: " + sDirectory);

            switch (oActuateFileAttributes.FileType.ToLower())
            {
                case "directory":
                    if (!sDirectory.EndsWith("/"))
                        sDirectory = sDirectory + "/";
                    DirSearch(sDirectory + sSearch);
                    if (GetDirectoryContents(sDirectory + sSearch).TotalCount == 0)
                    {
                        string stmp1 = sDirectory;
                        string stmp2 = sSearch;

                        if (stmp1.EndsWith("/"))
                            stmp1 = sDirectory.Substring(0, sDirectory.Length - 1);

                        if (stmp2.EndsWith("/"))
                            stmp2 = sSearch.Substring(0, sSearch.Length - 1);

                        // If this is a base folder, then do NOT delete, otherwise, delete it
                        if (!IsBaseFolder(stmp2))
                            DeleteFileFolder(stmp1, stmp2);
                    }
                    break;
                case "pdf":
                case "roi":
                    {
                        enum_OverwriteValues overwriteCode;
                        int archiveLength;
                        int reportTypeId;
                        enum_ArchiveTimeUnits archiveTimeUnits;

                        // If we are in the archives folder, see if the report has been here longer then the time allowed
                        if (sDirectory.IndexOf("(0) Archives") > 0)
                        {
                            string reportName = oActuateFileAttributes.Name;
                            DateTime dt = oActuateFileAttributes.TimeStamp;
                            DateTime dt2 = DateTime.Now;

                            GetReportArchiveValues(
                                                      Functions.GetReportCodeFromReportName(oActuateFileAttributes.Name),
                                                      out overwriteCode,
                                                      out archiveLength, out reportTypeId, out archiveTimeUnits);

                            #region Get the archive length and Time units

                            switch (archiveTimeUnits)
                            {
                                case enum_ArchiveTimeUnits.Daily:
                                    dt2 = dt.AddDays(archiveLength);
                                    break;
                                case enum_ArchiveTimeUnits.Weekly:
                                    dt2 = dt.AddDays(archiveLength*7);
                                    break;
                                case enum_ArchiveTimeUnits.Monthly:
                                    dt2 = dt.AddMonths(archiveLength);
                                    break;
                            }

                            #endregion

                            if (DateTime.Now > dt2)
                            {
                                // Looks like it is time to delete this report
                                DeleteFileFolder(sDirectory, reportName);
                                Console.WriteLine(" * DEL Folder - " + sDirectory + reportName);
                            }
                        }
                        else
                        {
                            // See if this file is older then 32 days
                            if (IsFileOld(sDirectory, oActuateFileAttributes))
                            {
                                // Get the archive values from the database.   
                                Reports oReports = new Reports(ConfigurationReader.Db);
                                oReports.GetReportArchiveValues(
                                                                   Functions.GetReportCodeFromReportName(
                                                                                                            oActuateFileAttributes
                                                                                                                .Name),
                                                                   out overwriteCode,
                                                                   out archiveLength, out reportTypeId,
                                                                   out archiveTimeUnits);

                                if (archiveLength == 0)
                                {
                                    // Delete this report
                                    Console.WriteLine(sDirectory + " DELETING " + oActuateFileAttributes.Name);
                                    DeleteFileFolder(sDirectory, oActuateFileAttributes.Name);                                    
                                }
                                else
                                {
                                    Console.WriteLine(" MOVING " + sDirectory + oActuateFileAttributes.Name);
                                    _archiveFile(Functions.FormatFolder(sDirectory),
                                                      oActuateFileAttributes.Name,
                                                      GetFileCreatedMonthFormated(oActuateFileAttributes.TimeStamp),
                                                      null,
                                                      false,
                                                      archiveLength,
                                                      archiveTimeUnits);
                                }
                            }
                        }
                    }
                    break;
            }
        }

        private bool IsFileOld(string sFolder, File oActuateFileAttributes)
        {
            try
            {
                DateTime dt = oActuateFileAttributes.TimeStamp.AddDays(_daysToHoldReports);

                return dt < DateTime.Now;
            }
            catch (Exception ex)
            {
                NameValueCollection nv = new NameValueCollection
                                             {{"Folder", sFolder}};
                ExceptionManager.Publish(ex, nv);
            }

            return (false);
        }

        private bool IsBaseFolder(string folder)
        {
            return _baseFolders.Any(baseFolder => baseFolder.Equals(folder));
        }
    }

    // Class
}

// Namespace