using System;
using ReportDistribution.ScheduledTasks.ArchiveService;
using ReportDistribution.ScheduledTasks.DailyEmail;
using Microsoft.ApplicationBlocks.ExceptionManagement;


namespace ScheduledTasks
{
    public static class Entry
    {

        [STAThread]
        public static void Main(string[] args)
        {
           
            try
            {

                if (args.Length > 0)
                {

                    Console.WriteLine("Starting " + args[0].ToLower() + " @ " + DateTime.Now);

                    if (args[0].ToLower() == "dailyemail")
                    {
                        Reports.StartDailyEmail();
                    }
                    if (args[0].ToLower() == "archivereports")
                    {
                        new CleanActuateFolders().ArchiveOldFiles();
                    }
                    if (args[0].ToLower() == "archivepermissionsreset")
                    {
                        ResetFolderPermissions oResetFolderPermissions = new ResetFolderPermissions();
                        oResetFolderPermissions.Start("/(0) Archives/");
                    }
                    if (args[0].ToLower() == "checkarchiveburst")
                    {
                        CheckActuateBurstFolder ocheckactuateburstfolder = new CheckActuateBurstFolder();
                        ocheckactuateburstfolder.Start();
                    }
                    if (args[0].ToLower() == "runfolderrename")
                    {
                        AutomatedFolderRename oAutomatedFolderRename = new AutomatedFolderRename();
                        oAutomatedFolderRename.Start();
                    }
                }

                else 
                {
                    AutomatedFolderRename oAutomatedFolderRename = new AutomatedFolderRename();
                    oAutomatedFolderRename.Start();
                }
               
            }
                    
            catch (Exception ex)
            {
                ExceptionManager.Publish(ex);
            }
        }
    }
}
