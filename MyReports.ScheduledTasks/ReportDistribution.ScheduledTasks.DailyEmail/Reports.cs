using System;
using System.Data;
using System.Net.Mail;
using System.Text;
using Compass.Utilities.Email;
using MyReports.Burst.Common.Configuration.Helper;
using MyReports.Burst.SqlServerDal;

namespace ReportDistribution.ScheduledTasks.DailyEmail
{
    public static class Reports
    {
        public static void StartDailyEmail()
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                BurstProcessStatus o = new BurstProcessStatus(ConfigurationReader.Db);

                DateTime dtNow = DateTime.Now;
                DateTime dtStart = dtNow.AddHours(-24);
                // DateTime dtStart = dtNow.AddDays(-112); // For testing in DEV
                DateTime dtEnd = dtNow;

                DataSet ds = o.GetBurstProcessStatus(dtStart, dtEnd);

                sb.Append("Start Date/Time: " + dtEnd + " --- End Date/Time: " + dtStart);

                sb.Append("\r\n");
                sb.Append("\r\n");

                foreach (DataRow myRow in ds.Tables[0].Rows)
                {
                    for (int y = 0; y < ds.Tables[0].Columns.Count; y++)
                    {
                        if (y == 0)
                        {
                            string reportName = myRow["ReportName"].ToString();

                            reportName = reportName.PadRight(70, ' ');
                            sb.Append(reportName + "\t");
                        }
                        if (y == 2)
                        {
                            sb.Append(myRow["StartedTime"].ToString());
                        }
                    }

                    sb.Append("\r\n");
                }

                string smtp = ConfigurationReader.RdSettings.EmailProperties.SmtpServer;


                string subject = ConfigurationReader.RemoteSettings.RemoteMachine + "Daily Email Report Listing";

                string message = sb.ToString();
                string to = ConfigurationReader.RdSettings.EmailProperties.DailyEmailListing;

                string from = ConfigurationReader.RdSettings.EmailProperties.EmailFromAccount;

                EmailHelper.SendMail(
                    smtp, // SMTP Server
                    subject, //Subject
                    message, // Message																																//Message Body
                    to, // To
                    from, // From
                    "", // CC
                    "", // BCC
                    null, // Attachments
                    MailPriority.Normal, // Priority
                    false); // Is HTML format
            }
            catch (Exception ex)
            {
                //Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex);
                //ExceptionManager.Publish(ex);
            }
        }
    }
}