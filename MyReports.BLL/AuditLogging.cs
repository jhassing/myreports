using System;

using MyReports.DAL;
using MyReports.Common;

namespace MyReports.BLL
{
	/// <summary>
	/// Summary description for AuditLogging.
	/// </summary>
	public class AuditLogging
	{
		private string m_sConnectionString;

		public AuditLogging(string sConnectionString)
		{
			m_sConnectionString = sConnectionString;
		}

		public void InsertAudit(	string domainName, string SessionID	, string UserName, 
			string platform, string browser, string browserVersion, 
			string userHostAddress, string workstationName )
		{
			new MyReports.DAL.AuditLogging(AppValues.MyReports_DB_ConnectionString).InsertAudit(domainName, SessionID, UserName, platform, browser, browserVersion, userHostAddress, workstationName);
		}
	}
}
