/**
 * 
 *
 * @author  Actuate Corporation
 * @version 1.0
 * $Revision::	$
 */

var url = null;
// This function does some error checking for the entered page range
function isValidPageRange(range)
{
	// strip off white spaces	
	var newRange = range.replace(/ /g, "");
	// need to add white spaces in front and behind to make it work -- don't know why
	newRange = " " + newRange + " ";
				
	// check for neg. numbers
	if( (newRange.search(/\D-\d/) != -1) || (newRange.search(/\d-\D/) != -1) )
	{
		g_pageRange = "";
	}
	else
	{
		g_pageRange = newRange.replace(/ /g, "");
	}
}

function selectAll()
{
	var allPages = "1-" + TotalPages;
	
	isValidPageRange(allPages);
	
	applyURL = "viewpage.aspx" + replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "operation" , "print") , "range" , allPages);
	
	applyURL = removeParams(applyURL, "page");
		
}

function selectCurrent()
{
	applyURL = "viewpage.aspx" + replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "operation" , "print") , "page" , currentPage);
	
	applyURL = removeParams(applyURL, "range");	
	document.forms['frmPageRange'].CustomRange.onkeypress=catchEnter;
}

function selectRange(formNo)
{
	// If its Netscape on SunOS, then the onkeyup method is not called for the input page range text box.
	// Thus the hyperlink is never updated correctly. For Netscape on SunOS, the hyperlink around the apply button
	// is changed to be handled by a method - applyLinkClick().
	var platform = navigator.platform;
	g_saveAsForm = document.forms[formNo];
	if (ns4 && platform.indexOf("SunOS") != -1)
	{
		applyLinkClick(0 , formNo);	
		applyLinkClick(1 , formNo);
	}
	else
	{ 
		isValidPageRange(g_saveAsForm.CustomRange.value);
    	applyURL = "viewpage.aspx" + replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "operation" , "print") , "range" , g_pageRange);
    	
    	applyURL = removeParams(applyURL, "page");
	}
}

function changeRange(formNo)
{
	g_saveAsForm = document.forms[formNo];
	if( g_saveAsForm.elements[2].checked )
	{
		isValidPageRange(g_saveAsForm.CustomRange.value);
		
		applyURL = "viewpage.aspx" + replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "operation" , "print") , "range" , g_pageRange);
		
    	applyURL = removeParams(applyURL, "page");
	}
} 

function applyLinkClick(action, formNo)
{
	var fmtSelected = action;

	// format buttons are in the first form 
	g_saveAsForm = document.forms['frmFileFormat']; 
	if (g_saveAsForm.elements['fileformat'][1].checked)
	{
		fmtSelected = 2 + action;
	}
	else if (g_saveAsForm.elements['fileformat'][2].checked)
	{
		fmtSelected = 4 + action;
	}
	else if (g_saveAsForm.elements['fileformat'][3].checked)
	{
		fmtSelected = 6 + action;
	}
	else if (g_saveAsForm.elements['fileformat'][4].checked)
	{
		fmtSelected = 8 + action;
	}
	else
	{
		fmtSelected = action;
	}
	
	// PDF quality is in second form
	var pdfQuality = 100;
	if(g_saveAsForm.elements['pdf_quality'].value)
	{
		pdfQuality = g_saveAsForm.elements['pdf_quality'].value;
	}
	
	// Get split large pages
	var splitLargePages = 'default';
	var pageWidth = 0;
	var pageHeight = 0;
	
	if(g_saveAsForm.elements['split_large_pages'].value)
	{
		splitLargePages = g_saveAsForm.elements['split_large_pages'].value;
	}
	
	switch (splitLargePages)
	{
		case 'default':
		case 'no':
		{
			break;
		}
		case 'yes':
		{
			if(g_saveAsForm.elements['page_width'].value)
			{
				pageWidth = g_saveAsForm.elements['page_width'].value;
				pageWidth *= 1440;
			}
			
			if(g_saveAsForm.elements['page_height'].value)
			{
				pageHeight = g_saveAsForm.elements['page_height'].value;
				pageHeight *= 1440;
			}
			
			break;
		}
	}

	// Page range butons are in the third form
	g_saveAsForm = document.forms['frmPageRange'];   
	if(g_saveAsForm.elements[0].checked)
	{
		selectAll();
	}
	else if(g_saveAsForm.elements[1].checked)
	{
		selectCurrent();
	}
	else if(g_saveAsForm.elements[2].checked)
	{
		selectRange(formNo);
	}
	isValidPageRange(g_saveAsForm.CustomRange.value);
	
	switch (fmtSelected)
	{
		case 0:
			url = replaceDuplicateParams(applyURL , "action" , "view");
			url = replaceDuplicateParams(url, "format", "PDF");
			url = replaceDuplicateParams(url, "pdfQuality", pdfQuality);
			switch (splitLargePages)
			{
				case 'no':
				{
					url = replaceDuplicateParams(url, "splitOversizePages", "false");
					break;
				}
				case 'yes':
				{
					url = replaceDuplicateParams(url, "splitOversizePages", "true");

					if (pageWidth)
					{
						url = replaceDuplicateParams(url, "pageWidth", pageWidth);
					}

					if (pageHeight)
					{
						url = replaceDuplicateParams(url, "pageHeight", pageHeight);
					}

					break;
				}
			}

			break;
		case 1:
			url = replaceDuplicateParams(applyURL , "action" , "save");
			url = replaceDuplicateParams(url, "format", "PDF");
			url = replaceDuplicateParams(url, "pdfQuality", pdfQuality);
			switch (splitLargePages)
			{
				case 'no':
				{
					url = replaceDuplicateParams(url, "splitOversizePages", "false");
					break;
				}
				case 'yes':
				{
					url = replaceDuplicateParams(url, "splitOversizePages", "true");

					if (pageWidth)
					{
						url = replaceDuplicateParams(url, "pageWidth", pageWidth);
					}

					if (pageHeight)
					{
						url = replaceDuplicateParams(url, "pageHeight", pageHeight);
					}

					break;
				}
			}

			break;
		case 2:
			url = replaceDuplicateParams(replaceDuplicateParams(applyURL , "action" , "view") , "format" , "EXCELData");
			break;
		case 3:
			url = replaceDuplicateParams(replaceDuplicateParams(applyURL , "action" , "save") , "format" , "EXCELData");
			break;
		case 4:
			url = replaceDuplicateParams(replaceDuplicateParams(applyURL , "action" , "view") , "format" , "EXCELDisplay");
			break;
		case 5:
			url = replaceDuplicateParams(replaceDuplicateParams(applyURL , "action" , "save") , "format" , "EXCELDisplay");
			break;
		case 6:
			url = replaceDuplicateParams(replaceDuplicateParams(applyURL , "action" , "view") , "format" , "RTF");
			break;
		case 7:
			url = replaceDuplicateParams(replaceDuplicateParams(applyURL , "action" , "save") , "format" , "RTF");
			break;
		case 8:
			url = replaceDuplicateParams(replaceDuplicateParams(applyURL , "action" , "view") , "format" , "RTFFullyEditable");
			break;
		case 9:
			url = replaceDuplicateParams(replaceDuplicateParams(applyURL , "action" , "save") , "format" , "RTFFullyEditable");
			break;
		default:
			return;
	}

	if ( action == 1 )
		document.location.replace(url);
	else
	{
		var current = (new Date()).getUTCMilliseconds();
		// this is done in order to give different names to each windows
		//else Excel plugin creates problem opening two files with same name at a time
		url += "&dummy=" + current;
		
		var winName = "__saveas" + current;
		openNewWindow(url, winName);
	}
}

function catchEnter(keypressed) 
{
	var key;
			        
    if (ns4) 
    {
		key=keypressed.which;
	}
	else 
	{
		key=getEventKeyCode(keypressed);
	}
					      
	// Check each key pressed for character code 13 (carriage return), AND
	// then SUBMIT the form for the user
	if (key==13) 
	{
		if( g_saveAsForm.elements[2].checked )
		{
			isValidPageRange(g_saveAsForm.CustomRange.value);
			if(g_saveAsForm.name == "frmSaveasEXL")
				newURL = "viewpage.aspx" + replaceDuplicateParams(replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "operation" , "print") , "range" , g_pageRange) , "format" , "EXCELDisplay");
			else if(g_saveAsForm.name == "frmSaveasPDF")
				newURL = "viewpage.aspx" + replaceDuplicateParams(replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "operation" , "print") , "range" , g_pageRange) , "format" , "PDF");
			else 
				newURL = g_viewPageURL + replaceDuplicateParams(replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "operation" , "print") , "range" , g_pageRange) , "format" , "RTF");
			newURL = removeParams(newURL, "page");
			g_saveAsForm.action = newURL;
			g_saveAsForm.submit();
		}
		else
		{
			g_saveAsForm.action = "#";
			alert("Please enter the page range and click the corresponding radio button to proceed.");
		}
    }
}

///////////// Use this instead of window.open
function openNewWindow(url, windowName)
{
	window.open(url, windowName, "toolbar=no, resizable=yes, menubar=yes, height=580, width=780, scrollbars=yes");
}

