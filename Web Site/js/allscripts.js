

var SCH_DT_FORMAT = "M/d/yy"; // HARDCODED DEFAULTS; WILL BE OVERRIDDEN BY PARENT JSP PAGE
var SCH_TM_FORMAT = "h:mm a"; // HARDCODED DEFAULTS; WILL BE OVERRIDDEN BY PARENT JSP PAGE
var TZ_OFFSET = 0;
var BUFFER_STATUS = false;

var LOCALIZED_AM = "AM";
var LOCALIZED_PM = "PM";

// This is global variable which holds the 'src' for the help link.
// This is a workaround to bring up the context sensitive help in NS4
var slink = "";

/**
 * This function is called when the mouse pointer brought over 
 * the Help link provided in the index.jsp pages. The function
 * changes the 'src' of the document.link[] object to bring the 
 * Context Sensitive help page.
 * This is a workaround to bring up the context sensitive help 
 * in NS4 
 */
function changeSrc()
{
	if (document.layers)
	{
		document.links[2].href = slink;
	}
}

/**
 *
 */
function attachHelp(sHelpBase, sTopic)
{
	sLink = sHelpBase + "wwhelp.htm?single=true&content=jsp_rc&topic=" + sTopic;
	if (document.all)
	{
		if (document.anchors("helpHyperlink") == null) return;
		document.anchors("helpHyperlink").href = sLink;
	}
	else if (!document.layers)
	{
		if (document.anchors["helpHyperlink"] == null) return;
		document.anchors["helpHyperlink"].href = sLink;
	}
	else if (document.layers)
	{
		//slink = sLink;
		//alert("Failed to update HELP link for NS4\nThis needs to be implemented.");
	}
}

/**
 *  This is the original implemetation
 */

/*
function gotoFolder(doc, workingFolder, childFolder, sFilter, sOnlyLatest, sShowFolders, sShowDocuments, sShowExecutables)
{
	if (workingFolder == "/") workingFolder = "";
	sFolder = workingFolder + "/" + childFolder;

	sURL = "../filesfolders/index.jsp?folder="+encode(sFolder);
	if (sFilter != null && sFilter != "") sURL += "&filter=" + encode(sFilter);
	if (sOnlyLatest != null && sOnlyLatest != "") sURL += "&onlyLatest=" + sOnlyLatest;
	if (sShowFolders != null && sShowFolders != "") sURL += "&showFolders=" + sShowFolders;
	if (sShowDocuments != null && sShowDocuments != "") sURL += "&showDocuments=" + sShowDocuments;
	if (sShowExecutables != null && sShowExecutables != "") sURL += "&showExecutables=" + sShowExecutables;

	window.location.href = sURL;
}
*/

/******************************************************************************
 *   For CS report cast only
 ******************************************************************************/


function gotoFolder(doc, parentFolder, childFolder, sFormName)
{
	createParameter(doc, "AcFolder", parentFolder+"/"+childFolder, sFormName);
	doc.forms[sFormName].submit();
}


/**
 *  Switch the job request tab
 */
function switchTab(doc, tabName, sFormName)
{
	createParameter(doc, "tabName", tabName, sFormName);
	doc.forms[sFormName].submit();
}

function switchSection(doc, sectionName, sFormName)
{
    createParameter(doc, "sectionName", sectionName, sFormName);	
	doc.forms[sFormName].submit();
}

/**
 *
 */
function updateFilter(doc, sFormName, sFolder)
{
	createParameter(doc, "folder", sFolder, sFormName);
	createParameter(doc, "applyFilter", "OK", sFormName);
	doc.forms[sFormName].submit();
}

/**
 *
 */
function restoreFilter(doc, sFormName, sFolder)
{
	createParameter(doc, "folder", sFolder, sFormName);
	createParameter(doc, "resetFilter", "OK", sFormName);
	doc.forms[sFormName].submit();
}

/**
 *
 */
function createParameter(doc, sName, sValue, formIndex)
{
	fld = doc.createElement("input");
	fld.name = sName;
	fld.type = "hidden";
	fld.value = sValue;
	doc.forms[formIndex].appendChild(fld);
}

/**
 *
 */
function createParameter(doc, sName, sValue, formName)
{
	fld = doc.createElement("input");
	fld.name = sName;
	fld.type = "hidden";
	fld.value = sValue;
	doc.forms[formName].appendChild(fld);
}

function go_to(sURL)
{
	window.location.href = sURL;
}

function applyJobFilter(doc, sFormName)
{
	createParameter(doc, "applyJobFilter", "OK", sFormName);
	doc.forms[sFormName].submit();
}

function restoreJobFilter(doc, sFormName)
{
	createParameter(doc, "restoreJobFilter", "OK", sFormName);
	doc.forms[sFormName].submit();
}

/******************************************************************************
 *   For CS report cast only
 ******************************************************************************/

/**
 *
 */
function returnToFilesFolders(doc, workingFolder)
{
	if (workingFolder == "/") workingFolder = "";
	sURL = "index.jsp?folder="+encode(workingFolder);
	window.location.href = sURL;
}


/**
 *
 */
function workInProgress()
{
	alert("Not yet implemented");
}

/**
 *
 */
function viewFileDetail(sFQFileName, sServerURL, sVolume)
{
	window.location.href = "index.jsp?subpage=_detail&name="+encode(sFQFileName) + "&serverURL=" + encode(sServerURL) + "&volume=" + encode(sVolume);
}

/**
 *
 */
function viewJobDetail(sJobID, sServerURL, sVolume)
{
	window.location.href = "../requests/index.jsp?subpage=_detail&jobID="+sJobID + "&serverURL=" + encode(sServerURL) + "&volume=" + encode(sVolume);
}

/**
 *
 */
function viewDocument(sFQFileName, sServerURL, sVolume, sViewNewBrowserWindow)
{
	var sUrl = "";
	var sBrowserNewWindowName = "";
	if(sViewNewBrowserWindow == null) sViewNewBrowserWindow = "false";
	sFileExtension = getFileExtension(sFQFileName);

	if (sFileExtension.toUpperCase() != "ROI") // DOWNLOAD ANYTHING THAT'S NOT AN ROI
	{
		if (sViewNewBrowserWindow == 'true')
		{
			sBrowserNewWindowName = "__view" + (new Date()).getUTCMilliseconds();
			sUrl = "../servlet/downloadFile?name=" + encode(sFQFileName) + "&serverURL=" + encode(sServerURL) + "&volume=" + encode(sVolume);
			openNewDefaultWindow(sUrl, sBrowserNewWindowName);
		} else 
		{
			window.location.href = "../servlet/downloadFile?name=" + encode(sFQFileName) + "&serverURL=" + encode(sServerURL) + "&volume=" + encode(sVolume);
		}
	}
	else // VIEW ROI'S IN DHTML VIEWING FRAMEWORK
	{
		if(sViewNewBrowserWindow == 'true'){
			sBrowserNewWindowName = "__view" + (new Date()).getUTCMilliseconds();
			sUrl = "../viewer/viewframeset.jsp?name=" + encode(sFQFileName) + "&serverURL=" + encode(sServerURL) + "&volume=" + encode(sVolume);
			openNewDefaultWindow(sUrl, sBrowserNewWindowName);
		} 
		else 
		{
			window.location.href = "../viewer/viewframeset.jsp?name=" + encode(sFQFileName) + "&serverURL=" + encode(sServerURL) + "&volume=" + encode(sVolume);
		}
	}
}

/**
 *
 */
function deleteFile(sFQFileName, sServerURL, sVolume)
{
	if (confirm("Are you sure you want to delete\nthe file " + sFQFileName))
		window.location.href = "../filesfolders/do_drop.jsp?name=" + encode(sFQFileName) + "&serverURL=" + encode(sServerURL) + "&volume=" + encode(sVolume);
}

/**
 *
 */
function getFileExtension(sFQFileName)
{
	if (sFQFileName == null) return null;
	iDot = sFQFileName.lastIndexOf(".");
	if (iDot == -1) return "";
	iSemiColon = sFQFileName.lastIndexOf(";");
	if (iSemiColon == -1) iSemiColon = sFQFileName.length;
	return sFQFileName.substring(iDot + 1, iSemiColon);
}


/**
 *
 */
function viewActiveRequests(sJobState, sServerURL, sVolume)
{
	window.location.href = "index.jsp?serverURL=" + sServerURL 
	                     + "&volume=" + sVolume 
						 + "&subpage=_" + sJobState;
}

/**
 *
 */
function deleteJobCompletionNotice(doc, frm, sJobID, sJobState)
{
	createParameter(doc, "jobID", sJobID, frm.name);
	createParameter(doc, "jobState", sJobState, frm.name);
	frm.action = "do_deletestatus.jsp";
	frm.submit();
}

/**
 *
 */
function deleteJob(doc, frm, sJobID, sJobState)
{
	createParameter(doc, "jobID", sJobID, frm.name);
	createParameter(doc, "jobState", sJobState, frm.name);
	frm.action = "do_deletejob.jsp";
	frm.submit();
}

/**
 *
 */
function cancelRequest(doc, frm, sJobID, sJobState)
{
	createParameter(doc, "jobID", sJobID, frm.name);
	createParameter(doc, "jobState", sJobState, frm.name);
	frm.action = "do_drop.jsp";
	frm.submit();
}

/**
 *
 */
function asyncRequest(sFQExecName, sServerURL, sVolume)
{
	sURI = "../newrequest/index.jsp?__requestType=scheduled&__executableName=" + encode(sFQExecName) 
	     + "&serverURL=" + encode(sServerURL) 
		 + "&volume=" + encode(sVolume);
	window.location.href = sURI;
}

/**
 *
 */
function syncRequest(sFQExecName, sServerURL, sVolume)
{
	sURI = "../newrequest/index.jsp?__requestType=immediate&__executableName=" + encode(sFQExecName) 
	     + "&serverURL=" + encode(sServerURL) 
		 + "&volume=" + encode(sVolume);
	window.location.href = sURI;
}

/**
 *
 */
function clearFilter(tf)
{
	tf.value = "";
}


/**
 *
 */
function getRadioValue(radioElements)
{
	for (i = 0; i < radioElements.length; i++)
	{
		if (radioElements[i].checked) 
			return radioElements[i].value;
	}
	return null;
}

/**
 *
 */
 
function getRadioGroupFromID(radioGroupID)
{
 	var firstRadio = GetElement(radioGroupID + "_0");
	if (firstRadio == null)
	{
		return null;
	}

	var radioElements = document.getElementsByName(firstRadio.name);
	if (radioElements.length == 0)
	{
		return null;
	}
	
	return radioElements;
}










/**
 *
 */
function clearCompletedRequestFilter(frmCompleted)
{
	frmCompleted.filter.value = "";
	frmCompleted.cbSuccess.value = "CHECKED";
	frmCompleted.cbFail.value = "CHECKED";
}

/**
 *
 */
function changeRequestTab(doc, sSubPage, sFilter, sSuccess, sFail)
{
	frm = doc.forms["frmSubmit"];
	if (frm == null)
	{
		alert("The form was not found possibly due to an error on the page.\nIgnoring the request for tab change");
		return;
	}

	if (sFilter != "") createParameter(doc, "filter", sFilter, frm.name);
	if (sSuccess != "") createParameter(doc, "cbSuccess", sSuccess, frm.name);
	if (sFail != "") createParameter(doc, "cbFail", sFail, frm.name);
	createParameter(doc, "subpage", "_" + sSubPage, frm.name);

	frm.submit();
}


/**
 *
 * Feature: Options (General Tab)
 */
function buffer_general(frmGeneral, frmDispatch)
{
	frmDispatch.email.value = frmGeneral.email.value;
	frmDispatch.oldKey.value = frmGeneral.oldKey.value;
	frmDispatch.newKey.value = frmGeneral.newKey.value;
	frmDispatch.confirmKey.value = frmGeneral.confirmKey.value;
	frmDispatch.docChanFilters.value = "" + frmGeneral.docChanFilters.checked;
	frmDispatch.requestFilters.value = "" + frmGeneral.requestFilters.checked;
	frmDispatch.channelIcons.value = "" + frmGeneral.channelIcons.checked;
	frmDispatch.viewNewBrowserWindow.value = "" + frmGeneral.viewNewBrowserWindow.checked;
	frmDispatch.newLocale.value = frmGeneral.newLocale.value;
	frmDispatch.newTimeZone.value = frmGeneral.newTimeZone.value;
	BUFFER_STATUS = true;
	return "";
}

/**
 *
 * Feature: Options (Notification Tab)
 */
function buffer_notification(frmNotification, frmDispatch)
{
	frmDispatch.succEmail.value = "" + frmNotification.succEmail.checked;
	frmDispatch.succComp.value = "" + frmNotification.succComp.checked;
	frmDispatch.failEmail.value = "" + frmNotification.failEmail.checked;
	frmDispatch.failComp.value = "" + frmNotification.failComp.checked;
	BUFFER_STATUS = true;
	return "";
}

/**
 *
 * Feature: Options (Channels Tab)
 */
function buffer_channels(frmChannels, frmDispatch)
{
	bAnySubscriptions = false;
	sSuffix = "";
	for (i = 0; i < frmChannels.elements.length; i++)
	{
		sChanName = frmChannels.elements[i].name;
		if (sChanName == "channels")
		{
			//alert("found subscribed channel " + frmChannels.elements[i].value + "; updating...");
			if (frmChannels.elements[i].type == "hidden" || frmChannels.elements[i].checked) 
			{
				sSuffix += "channels=" + frmChannels.elements[i].value + "&";
				bAnySubscriptions = true;
			}
			//alert("updated sSuffix is " + sSuffix);
		}
	}
	if (!bAnySubscriptions) sSuffix += "channels=__none&";

	BUFFER_STATUS = true;
	//alert("suffix is " + sSuffix);
	return sSuffix;
}

/** 
 *
 */
function updateChannelOptionsFilter(frmChannels, frmDispatch, sAction)
{
	if (sAction == "apply")
	{
		frmDispatch.channelListFilter.value = frmChannels.__filter.value;
		changeOptionsTab(frmDispatch, "channels", buffer_channels(frmChannels, frmDispatch));
	}
	else if (sAction == "clear")
	{
		clearFilter(frmDispatch.channelListFilter);
		changeOptionsTab(frmDispatch, "channels", buffer_channels(frmChannels, frmDispatch));
	}
	else
	{
		alert("Illegal action specified for\nchannel list filter. Ignoring silently.");
	}
}

/**
 *
 */
function buffer_about(frmAbout, frmDispatch)
{
	// Er ... Nothing needs to be buffered here
	BUFFER_STATUS = true;
	return "";
}


/** 
 *
 */
function validate_parameters(frmParameters, frmDispatch)
{
	
}


/**
 *
 * Feature: NewRequest (Scheduled & Immediate)
 */
function buffer_parameters(frmParameters, frmDispatch)
{
	//alert("all elements are ... " + frmParameters.elements.length);
	if (!validateReportParameters(frmParameters)) return;

	for (i = 0; i < frmParameters.elements.length; i++)
	{
		sParamName = frmParameters.elements[i].name;
		if (sParamName.indexOf("$") != -1)
		{
			//alert("found report parameter " + sParamName + "; updating...");
			frmDispatch.elements[sParamName].value = frmParameters.elements[i].value;
			//alert("update "+sParamName+" value is " + frmDispatch.elements[sParamName].value);
		}
	}
	BUFFER_STATUS = true;
	return;
}

function setDefaultFormats(dtFormat, tmFormat, tzOffset)
{
	SCH_DT_FORMAT = dtFormat;
	SCH_TM_FORMAT = tmFormat;
	TZ_OFFSET = tzOffset;
}

function NumToHex(iNum) // ACCEPTS UPTO 64k
{
	if (iNum > 255)
	{
		iQuo = iNum / 256;
		iRem = iNum % 256;
		iQuo = iQuo - (iRem / 256);
		return NumToHex(iQuo) + NumToHex(iRem);
	}
	base = iNum / 16;
	rem = iNum % 16;
	base = base - (rem / 16);
	baseS = MakeHex(base);
	remS = MakeHex(rem);
	return baseS + '' + remS;
}

function MakeHex(x) 
{
	if((x >= 0) && (x <= 9))
	return x;
	else {
	switch(x) {
	case 10: return "A"; 
	case 11: return "B";  
	case 12: return "C";  
	case 13: return "D";  
	case 14: return "E";  
	case 15: return "F";  
		  }
	   }
}


function debugText(sText)
{
	s = "";
	for (i = 0; i < sText.length; i++)
	{
		c = sText.charCodeAt(i);
		s += "[" + NumToHex(c) + "]";
	}
	return s;
}





/**
 * Returns true if date string matches format of format string and
 * is a valid date. Else returns false.
 *
 * It is recommended that you trim whitespace around the value before
 * passing it to this function, as whitespace is NOT ignored!
 */
function isDateValid(dtString, expectedFormat) 
{
	var dt = parseDate(dtString, expectedFormat);
	if (dt == 0) return false;
	return true;
}

/**
 * Returns a date in the output format specified.
 */
function formatDate(date, format) 
{
	//alert("formatDate : Localized AM is " + LOCALIZED_AM);
	
	
	var MONTH_NAMES = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
	format = format+"";
	var result = "";
	var i_format = 0;
	var c = "";
	var token = "";
	var y = date.getYear() + "";
	var M = date.getMonth() + 1;
	var d = date.getDate();
	var H = date.getHours();
	var m = date.getMinutes();
	var s = date.getSeconds();
	var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
	if (y.length < 4) y = y-0+1900;
	y = "" + y;
	yyyy = y;
	yy = y.substring(2,4);

	// Month
	if (M < 10) MM = "0" + M; else MM = M;
	MMM = MONTH_NAMES[M-1];
	
	// Date
	if (d < 10) dd = "0"+d; else dd = d;

	// Hour
	h = H; K = H; k = H + 1;
	if (h > 12) { h-=12; }
	if (h == 0) { h=12; }
	if (h < 10) { hh = "0"+h; } else { hh = h; }
	if (H < 10) { HH = "0"+K; } else { HH = H; }
	if (K > 11) { K-=12; }
	if (K < 10) { KK = "0"+K; } else { KK = K; }
	if (k < 10) { kk = "0"+k; } else { kk = k; }
	if (H > 11) { ampm = LOCALIZED_PM; } else { ampm = LOCALIZED_AM; }
	if (m < 10) { mm = "0"+m; } else { mm = m; }
	if (s < 10) { ss = "0"+s; } else { ss = s; }

	// Now put them all into an object!
	var value = new Object();
	value["yyyy"] = yyyy;
	value["yy"] = yy;
	value["y"] = y;
	value["MMM"] = MMM;
	value["MM"] = MM;
	value["M"] = M;
	value["dd"] = dd;
	value["d"] = d;
	value["hh"] = hh;
	value["h"] = h;
	value["HH"] = HH;
	value["H"] = H;
	value["KK"] = KK;
	value["K"] = K;
	value["kk"] = kk;
	value["k"] = k;
	value["mm"] = mm;
	value["m"] = m;
	value["ss"] = ss;
	value["s"] = s;
	value["t"] = ampm;
	value["tt"] = ampm;
	while (i_format < format.length) 
	{
		// Get next token from format string
		c = format.charAt(i_format);
		token = "";
		while ((format.charAt(i_format) == c) && (i_format < format.length)) {
			token += format.charAt(i_format);
			i_format++;
			}
		if (value[token] != null) {
			result = result + value[token];
			}
		else {
			result = result + token;
			}
	}
	return result;
}

// INTERNALLY USED
function _isInteger(sValue, bChkSymbol, cPosSymbol, cNegSymbol) 
{
	var digits = "1234567890";
	for (var i = 0; i < sValue.length; i++) 
	{
		c = sValue.charAt(i);
		if (digits.indexOf(c) == -1) 
		{
			if (bChkSymbol)
			{
				if (c != cPosSymbol && c != cNegSymbol) return false;
				if (i > 0) return false;
				continue; // CONTINUE VALIDATION OF NEXT CHARACTER
			}
			return false;
		}
	}
	return true;
}

// INTERNALLY USED
function _getInt(str, i, minlength, maxlength) 
{
	for (x = maxlength; x >= minlength; x--) 
	{
		var token = str.substring(i, i + x);
		if (token.length < minlength) return null;
		if (_isInteger(token)) return token;
	}
	return null;
}

function getDateUTC(dt, tzOffset)
{
	dt.setTime(dt.getTime() - tzOffset);
	sYear = "" + dt.getYear();
	sMonth = "" + (dt.getMonth() + 1); // UTC REQUIRES MONTHS = 1 to 12
	sDate = "" + (dt.getDate());
	while (sMonth.length < 2) sMonth = "0" + sMonth;
	while (sDate.length < 2) sDate = "0" + sDate;
	return sYear + "-" + sMonth + "-" + sDate;
}

function getTimeUTC(dt, tzOffset)
{
	dt.setTime(dt.getTime() - tzOffset);
	sHour = "" + dt.getHours();
	sMinute = "" + dt.getMinutes()
	sSecond = "" + dt.getSeconds();
	while (sHour.length < 2) sHour = "0" + sHour;
	while (sMinute.length < 2) sMinute = "0" + sMinute;
	while (sSecond.length < 2) sSecond = "0" + sSecond;
	return sHour + ":" + sMinute + ":" + sSecond;
}

function getDateTimeUTC(dt, tzOffset)
{
	dtUTC = new Date(dt.getTime());
	sDate = getDateUTC(dtUTC, tzOffset);
	dtUTC = new Date(dt.getTime());
	sTime = getTimeUTC(dtUTC, tzOffset);
	return sDate + "T" + sTime;
}

// INTERNALLY USED
function parseDate(dtString, format) 
{
	if (dtString == null) return null;
	val = trim(dtString.toUpperCase());
	format = format+"";
	var i_val = 0;
	var i_format = 0;
	var c = "";
	var token = "";
	var token2= "";
	var x,y;
	var now   = new Date();
	var year  = now.getYear();
	var month = now.getMonth()+1;
	var date  = now.getDate();
	var hh    = now.getHours();
	var mm    = now.getMinutes();
	var ss    = now.getSeconds();
	var ampm  = "";
	
	while (i_format < format.length) {
		// Get next token from format string
		c = format.charAt(i_format);
		token = "";
		while ((format.charAt(i_format) == c) && (i_format < format.length)) {
			token += format.charAt(i_format);
			i_format++;
			}
		// Extract contents of value based on format token
		if (token=="yyyy" || token=="yy" || token=="y") {
			if (token=="yyyy") { x=4;y=4; }// 4-digit year
			if (token=="yy")   { x=2;y=2; }// 2-digit year
			if (token=="y")    { x=2;y=4; }// 2-or-4-digit year
			year = _getInt(val,i_val,x,y);
			if (year == null) { return 0; }
			i_val += year.length;
			if (year.length == 2) 
			{
				if (year > 70) 
				{
					year = 1900+(year-0);
				}
				else 
				{
					year = 2000+(year-0);
				}
			}
		}
		else if (token=="MMM"){// Month name
			month = 0;
			for (var i=0; i<MONTH_NAMES.length; i++) {
				var month_name = MONTH_NAMES[i];
				if (val.substring(i_val,i_val+month_name.length).toLowerCase() == month_name.toLowerCase()) {
					month = i+1;
					if (month>12) { month -= 12; }
					i_val += month_name.length;
					break;
					}
				}
			if (month == 0) { return 0; }
			if ((month < 1) || (month>12)) { return 0; }
			// TODO: Process Month Name
		}
		else if (token=="MM" || token=="M") {
			x=1; y=2;
			month = _getInt(val,i_val,x,y);
			if (month == null) { return 0; }
			if ((month < 1) || (month > 12)) { return 0; }
			i_val += month.length;
		}
		else if (token=="dd" || token=="d") {
			x=1; y=2;
			date = _getInt(val,i_val,x,y);
			if (date == null) { return 0; }
			if ((date < 1) || (date>31)) { return 0; }
			i_val += date.length;
		}
		else if (token=="hh" || token=="h") {
			x=1; y=2;
			hh = _getInt(val,i_val,x,y);
			if (hh == null) { /*alert("null 'hh' field");*/ return 0; }
			if ((hh < 1) || (hh > 12)) { /*alert("out of range hour");*/ return 0; }
			i_val += hh.length;
			hh--;
		}
		else if (token=="HH" || token=="H") {
			x=1; y=2;
			hh = _getInt(val,i_val,x,y);
			if (hh == null) { return 0; }
			if ((hh < 0) || (hh > 23)) { return 0; }
			i_val += hh.length;
		}
		else if (token=="KK" || token=="K") {
			x=1; y=2;
			hh = _getInt(val,i_val,x,y);
			if (hh == null) { return 0; }
			if ((hh < 0) || (hh > 11)) { return 0; }
			i_val += hh.length;
		}
		else if (token=="kk" || token=="k") {
			x=1; y=2;
			hh = _getInt(val,i_val,x,y);
			if (hh == null) { return 0; }
			if ((hh < 1) || (hh > 24)) { return 0; }
			i_val += hh.length;
			h--;
		}
		else if (token=="mm" || token=="m") {
			x=1; y=2;
			mm = _getInt(val,i_val,x,y);
			if (mm == null) { /*alert("null 'mm' field");*/ return 0; }
			if ((mm < 0) || (mm > 59)) { /*alert("out of range minute");*/ return 0; }
			i_val += mm.length;
		}
		else if (token=="ss" || token=="s") {
			x=1; y=2;
			ss = _getInt(val,i_val,x,y);
			if (ss == null) { return 0; }
			if ((ss < 0) || (ss > 59)) { return 0; }
			i_val += ss.length;
		}
		else if (token=="a" || token=="tt" )
		 {  
		    var AmLength = LOCALIZED_AM.length;
		    var PmLength = LOCALIZED_PM.length;
			if (trim(val.substring(i_val,i_val+ AmLength)) == LOCALIZED_AM.toUpperCase())
			{
				ampm = LOCALIZED_AM.toUpperCase();
			}
			else if (trim(val.substring(i_val,i_val+ PmLength)) == LOCALIZED_PM.toUpperCase()) 
			{
				ampm = LOCALIZED_PM.toUpperCase();
			}
			else 
			{
				//alert("failed to parse AM/PM"); 
				return 0;
			}
		}
		else {
			if (val.substring(i_val,i_val+token.length) != token) {
				//alert("internal unknown error"); 
				return 0;
				}
			else {
				i_val += token.length;
				}
			}
		}
	// If there are any trailing characters left in the value, it doesn't match
	/*if (i_val != val.length) {
		alert("found trailing chars"); 
		return 0;
		}*/
	// Is date valid for month?
	if (month == 2) 
	{
		if ( ( (year%4 == 0)&&(year%100 != 0) ) || (year%400 == 0) ) { // leap year
			if (date > 29){ return false; }
		}
		else 
		{
			if (date > 28) { return false; }
		}
	}
	if ((month==4)||(month==6)||(month==9)||(month==11)) 
	{
		if (date > 30) { return false; }
	}
	if (hh<12 && ampm==LOCALIZED_PM.toUpperCase()) 
	{
		hh+=12;
	}
	else if (hh>11 && ampm==LOCALIZED_AM.toUpperCase()) 
	{
		hh-=12;
	}
	dt = new Date(year, month-1, date, hh, mm, ss);
	return dt.getTime();
}

/**
 *
 */
function ltrim(str) 
{
	for (var i=0; str.charAt(i)==" "; i++);
	return str.substring(i,str.length);
}

/**
 *
 */
function rtrim(str) 
{
	for (var i=str.length-1; str.charAt(i)==" "; i--);
	return str.substring(0,i+1);
}

/**
 *
 */
function trim(str) 
{
	return ltrim(rtrim(str));
}

/**
 *
 */
function requestFocus(obj)
{
	obj.select();
	obj.focus();
}


/**
 * Returns the name of a file object as a fully qualified name as required by most API/URL Parameters, etc.
 * NOTE: sVersion is optional; the method may be called using only 2 arguments.
 */
function getFullyQualifiedName(sFolder, sFileName, sVersion)
{
	if (sFolder != null && sFolder.length > 0 && sFolder.charAt(sFolder.length - 1) != "/") sFolder += "/";
	sFQName = sFolder + sFileName;
	if (sVersion != null && sVersion.length > 0) sFQName += ";" + sVersion;
	return sFQName;
}

/**
 * Quick lookup table for converting 8BIT to HEX
 */
var hex = new Array(
	"%00", "%01", "%02", "%03", "%04", "%05", "%06", "%07",
	"%08", "%09", "%0a", "%0b", "%0c", "%0d", "%0e", "%0f",
	"%10", "%11", "%12", "%13", "%14", "%15", "%16", "%17",
	"%18", "%19", "%1a", "%1b", "%1c", "%1d", "%1e", "%1f",
	"%20", "%21", "%22", "%23", "%24", "%25", "%26", "%27",
	"%28", "%29", "%2a", "%2b", "%2c", "%2d", "%2e", "%2f",
	"%30", "%31", "%32", "%33", "%34", "%35", "%36", "%37",
	"%38", "%39", "%3a", "%3b", "%3c", "%3d", "%3e", "%3f",
	"%40", "%41", "%42", "%43", "%44", "%45", "%46", "%47",
	"%48", "%49", "%4a", "%4b", "%4c", "%4d", "%4e", "%4f",
	"%50", "%51", "%52", "%53", "%54", "%55", "%56", "%57",
	"%58", "%59", "%5a", "%5b", "%5c", "%5d", "%5e", "%5f",
	"%60", "%61", "%62", "%63", "%64", "%65", "%66", "%67",
	"%68", "%69", "%6a", "%6b", "%6c", "%6d", "%6e", "%6f",
	"%70", "%71", "%72", "%73", "%74", "%75", "%76", "%77",
	"%78", "%79", "%7a", "%7b", "%7c", "%7d", "%7e", "%7f",
	"%80", "%81", "%82", "%83", "%84", "%85", "%86", "%87",
	"%88", "%89", "%8a", "%8b", "%8c", "%8d", "%8e", "%8f",
	"%90", "%91", "%92", "%93", "%94", "%95", "%96", "%97",
	"%98", "%99", "%9a", "%9b", "%9c", "%9d", "%9e", "%9f",
	"%a0", "%a1", "%a2", "%a3", "%a4", "%a5", "%a6", "%a7",
	"%a8", "%a9", "%aa", "%ab", "%ac", "%ad", "%ae", "%af",
	"%b0", "%b1", "%b2", "%b3", "%b4", "%b5", "%b6", "%b7",
	"%b8", "%b9", "%ba", "%bb", "%bc", "%bd", "%be", "%bf",
	"%c0", "%c1", "%c2", "%c3", "%c4", "%c5", "%c6", "%c7",
	"%c8", "%c9", "%ca", "%cb", "%cc", "%cd", "%ce", "%cf",
	"%d0", "%d1", "%d2", "%d3", "%d4", "%d5", "%d6", "%d7",
	"%d8", "%d9", "%da", "%db", "%dc", "%dd", "%de", "%df",
	"%e0", "%e1", "%e2", "%e3", "%e4", "%e5", "%e6", "%e7",
	"%e8", "%e9", "%ea", "%eb", "%ec", "%ed", "%ee", "%ef",
	"%f0", "%f1", "%f2", "%f3", "%f4", "%f5", "%f6", "%f7",
	"%f8", "%f9", "%fa", "%fb", "%fc", "%fd", "%fe", "%ff"
);

/**
 * Encode a string to the "x-www-form-urlencoded" form, enhanced
 * with the UTF-8-in-URL proposal. This is what happens:
 *
 * <ul>
 * <li><p>The ASCII characters 'a' through 'z', 'A' through 'Z',
 *        and '0' through '9' remain the same.
 *
 * <li><p>The unreserved characters - _ . ! ~ * ' ( ) remain the same.
 *
 * <li><p>The space character ' ' is converted into a plus sign '+'.
 *
 * <li><p>All other ASCII characters are converted into the
 *        3-character string "%xy", where xy is
 *        the two-digit hexadecimal representation of the character
 *        code
 *
 * <li><p>All non-ASCII characters are encoded in two steps: first
 *        to a sequence of 2 or 3 bytes, using the UTF-8 algorithm;
 *        secondly each of these bytes is encoded as "%xx".
 * </ul>
 *
 * @param s The string to be encoded
 * @return The encoded string
 */

function encode(s)
{
	var sbuf = new String("");
	var len = s.length;
	for (var i = 0; i < len; i++)
	{
		var ch = s.charAt(i);
		var chCode = s.charCodeAt(i);

		if (('A' <= ch && ch <= 'Z')  // UPPERCASE LATIN-1
		 || ('a' <= ch && ch <= 'z')  // LOWERCASE LATIN-1
		 || ('0' <= ch && ch <= '9')) // NUMERICAL DIGITS
		{		
			sbuf += ch;
		}
		else if (chCode <= 0x007f)	// RANGE (00 - 7f)
		{
			sbuf += hex[chCode];
		}
		else if (chCode <= 0x07ff) // RANGE (007f - 07ff)
		{
			sbuf += (hex[0xc0 | (chCode >> 6)]);
			sbuf += (hex[0x80 | (chCode & 0x3F)]);
		}
		else // RANGE (07ff - ffff)
		{
			sbuf += (hex[0xe0 | (chCode >> 12)]);
			sbuf += (hex[0x80 | ((chCode >> 6) & 0x3f)]);
			sbuf += (hex[0x80 | (chCode & 0x3f)]);
		}
	}
	return sbuf;
}


/**
 *
 */
function validateDecimal(val, sep, cPlusSign, cMinusSign) 
{
	val = trim('' + val); // cast val to string
	if (!sep) sep = '.'; // if separator isn't specified, hardcode as decimal point '.'
	
	if (val.indexOf(sep) > -1) 
	{
		pre = val.substring(0, val.indexOf(sep));
		post = val.substring(val.indexOf(sep) + 1);
		if (!validateInteger(pre, true, cPlusSign, cMinusSign) || !validateInteger(post)) return false;
	} 
	else 
	{
		return validateInteger(val, true, cPlusSign, cMinusSign);
	}
	
	return true;
}

/**
 *
 */
function validateInteger(val, b, c1, c2)
{
	return _isInteger(val, b, c1, c2);
}

/**
 *
 */
function validateBoolean(val)
{
	val = trim('' + val);
	val = val.toUpperCase();
	return (val == "FALSE" || val == "TRUE");
}

/**
 *
 * bAsPrefix = true = PREFIX
 * bAsPrefix = false = SUFFIX
 */
function validateCurrency(sym, bAsPrefix, val, sep) 
{
	return true;
}

function validateSaveOutput( saveDocument,
							 OutputFolderType,
							 AbsoluteFolderID,
							 msgInvalidOutputFolder,
							 msgInvalidStartChar,
							 documentNameID,
							 msgRequiredDocumentName,
							 numVersionsChkBoxID,
							 numVersionsTxtID,
							 CreateReplace,
							 msgInvalidNumVersions,
							 validJobHiddenID )
{

	var DocumentNameTxt =  GetElement( documentNameID );
	var NumVersionTxt =    GetElement( numVersionsTxtID );
	var NumVersionChkBox = GetElement( numVersionsChkBoxID );
	var ValidJobHiddenTxt = GetElement( validJobHiddenID );
	
	//  Initialize the Value of the hidden field
	ValidJobHiddenTxt.value = "true";

	if ( saveDocument.toLowerCase() == "true" )
	{
		if ( trim(DocumentNameTxt.value) == "" )
		{
			alert( msgRequiredDocumentName );
			ValidJobHiddenTxt.value = "false";
			return false;
		}
		
		if ( OutputFolderType == "absolute" )
		{
			var AbsoluteFolderTxt = GetElement( AbsoluteFolderID );
			var sOutputFolder = AbsoluteFolderTxt.value;
			
			if (!validateHomeFolder(sOutputFolder, msgInvalidOutputFolder, msgInvalidStartChar ))
			{
				requestFocus( AbsoluteFolderTxt );
				ValidJobHiddenTxt.value = "false";
				return false;
			}
		}
	}

	// Check that  the number of versions to retain is a valid integer	
	if ( CreateReplace == "create" )
	{
		if ( NumVersionChkBox.checked )
		{
			if (!validateInteger(NumVersionTxt.value) || trim(NumVersionTxt.value) == "")
			{
		  		 alert( msgInvalidNumVersions );
		  		ValidJobHiddenTxt.value = "false";
				return false;
	 		}	
		}
	}
	
	return true;
}


// Utility functions used by the Options tab to
// comapre the Old and New Passwords
function ValidatePasswords( actualPassword,
							oldPasswordID,
							NewPasswordID,
							ConfirmPasswordID,
							validOptionsID,
							msgWrongExistingPassword,
							msgWrongReenteredPassword
							)
{
   var OldPasswordTxt = document.forms[0].elements[ oldPasswordID ];
   var NewPasswordTxt = document.forms[0].elements[ NewPasswordID ];
   var ConfPasswordTxt = document.forms[0].elements[ ConfirmPasswordID ];
   var ValidOptionsTxt = document.forms[0].elements[ validOptionsID ];
   
   
   ValidOptionsTxt.value = "true";
     
	if ( actualPassword != OldPasswordTxt.value  )
	{
		alert( msgWrongExistingPassword );
		ValidOptionsTxt.value = "false";
		ResetHiddenPasswordFields( OldPasswordTxt, NewPasswordTxt, ConfPasswordTxt ) 
		return false;
	}
	else if ( NewPasswordTxt.value != ConfPasswordTxt.value )
	{
		alert ( msgWrongReenteredPassword );
		ValidOptionsTxt.value = "false";
		ResetHiddenPasswordFields( OldPasswordTxt, NewPasswordTxt, ConfPasswordTxt ) 
		return false;
	}
   
   return true;
}
			
function ValidateSchedule(  JobNameId,
							OnceDateId, 
							OnceTimeId,
							RecurringTimeId,
							UseStartDateChkBoxId, 
							StartDateId, 
							UseEndDateChkBoxId,
							EndDateId,
							ShortDatePattern, 
							ShortTimePattern,
							ScheduleTypeId,
							PriorityRadioId, 
							PriorityTextId,
							MaxPriorityHiddenId, 														 
							msgMissingJobName,
							msgIndalidOnceDate,
							msgInvalidOnceTime,
							msgInvalidRecurringTime,
							msgInvalidStartDate,
							msgInvalidEndDate ,
							msgInvalidPriorityValue,
							msgInvalidPriorityRange,
							ValidJobId)
{
	var JobNameCtl = GetElement( JobNameId );
    var ValidJobCtl = GetElement( ValidJobId );
    ValidJobCtl.value = "true";
    
    var ScheduleTypeCtl = GetElement( ScheduleTypeId );
    
    var ScheduleType = ScheduleTypeCtl.value.toLowerCase();
    
      if (  JobNameCtl.value == null || trim( JobNameCtl.value) == "" )
      {
			alert( msgMissingJobName );
			ValidJobCtl.value = "false";
			return false;
      }
    
      
     if ( ScheduleType == "once" )
     {

        var OnceDate = GetElement( OnceDateId );
        var OnceTime = GetElement( OnceTimeId );
        
		if ( ! isDateValid( OnceDate.value , ShortDatePattern ))
		{
			alert(messageFormat( msgIndalidOnceDate,
							     new Array( OnceDate.value, ShortDatePattern)));
			ValidJobCtl.value = "false";
			return false;
		}
		
		if ( ! isDateValid( OnceTime.value, ShortTimePattern ))
		{
			alert(messageFormat( msgInvalidOnceTime,
							     new Array( OnceTime.value, ShortTimePattern)));

			ValidJobCtl.value = "false";
			return false;
		}
	}
	else if ( ScheduleType == "recurring" )
	{
	  var RecurringTimeCtl = GetElement( RecurringTimeId );
	  var RecurringTime = RecurringTimeCtl.value;
	  
	  var StartDateCtl  = GetElement( StartDateId );
	  var StartDate = StartDateCtl.value;
	  
	  var EndDateCtl = GetElement( EndDateId );
	  var EndDate = EndDateCtl.value;
	  
	   if ( ! isDateValid( RecurringTime, ShortTimePattern ))
	   {
		   alert(messageFormat( msgInvalidRecurringTime,
						     new Array( RecurringTime, ShortTimePattern)));
   
	       ValidJobCtl.value = "false";
	       return false;
	   }
	   
	   var StartDateChkBox = GetElement(UseStartDateChkBoxId);
	   
	   if ( StartDateChkBox.checked )
	   {
			if ( ! isDateValid( StartDate, ShortDatePattern ))
			{
				alert(messageFormat( msgInvalidStartDate,
								     new Array( StartDate, ShortDatePattern)));
   
				ValidJobCtl.value = "false";
				return false;
			}
		}
		
		
		var EndDateChkBox = GetElement( UseEndDateChkBoxId );
		
		if ( EndDateChkBox.checked )
		{
	   
		   if ( ! isDateValid ( EndDate, ShortDatePattern ))
		   {
		   
				alert(messageFormat( msgInvalidEndDate,
							     new Array(  EndDate, ShortDatePattern)));
	          ValidJobCtl.value  = "false";
	          return false;
			}
		}
	   
	}
		
	// For users with very low priority ranges, no priority radio buttons are displayed	
	var priorityRadios = getRadioGroupFromID(PriorityRadioId);		
	if (priorityRadios == null || getRadioValue(priorityRadios) == "-1")
	{
		// VALIDATE 'OTHER' PRIORITY (TEXT BOX)
		var sPriority = GetElement(PriorityTextId).value;			
		if (!validateInteger(sPriority) || trim(sPriority) == "")
		{
			 alert(messageFormat(msgInvalidPriorityValue, new Array(sPriority)));
			 ValidJobCtl.value = "false";
			 requestFocus(GetElement(PriorityTextId));
			 return false;
		}

		var maxJobPriority = GetElement(MaxPriorityHiddenId).value;

		sPriority = parseInt(sPriority);
		if (sPriority < 1 || sPriority > maxJobPriority)
		{
			 alert(messageFormat(msgInvalidPriorityRange, new Array(sPriority , maxJobPriority)));
			 ValidJobCtl.value = "false";
			 requestFocus(GetElement(PriorityTextId));
			 return false;
		}
	}
	
	return true;
}
                         

function UpdateHiddenFields( ActualPasswordID,
							 OldPasswordID,
							 NewPasswordID,
							 ConfPassswordID,
							 ActualPassword,
							 OldPassword,
							 NewPassword,
							 ConfPassword )
{

  var ActualPasswordTxt = document.forms[0].elements[ ActualPasswordID ];
  var OldPasswordTxt = document.forms[0].elements[ OldPasswordID ];
  var NewPasswordTxt = document.forms[0].elements[ NewPasswordID ];
  var ConfPasswordTxt = document.forms[0].elements[ ConfPassswordID ];

 ActualPasswordTxt.value = ActualPassword;
 OldPasswordTxt.value = OldPassword;
 NewPasswordTxt.value = NewPassword;
 ConfPasswordTxt.value = ConfPassword;
 

 return true;
  
}


function ResetHiddenPasswordFields( OldPasswordTxt, NewPasswordTxt, ConfPasswordTxt ) 
{
  OldPasswordTxt.value = "";
  NewPasswordTxt.value = "";
  ConfPasswordTxt.value = "";
}




function BrowserIs ()

{

    var agt=navigator.userAgent.toLowerCase();



    this.Major = parseInt(navigator.appVersion);

    this.Minor = parseFloat(navigator.appVersion);



    this.Netscape  = ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1)

                && (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1)

                && (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1));

    this.Netscape2 = (this.Netscape && (this.Major == 2));

    this.Netscape3 = (this.Netscape && (this.Major == 3));

    this.Netscape4 = (this.Netscape && (this.Major == 4));

    this.Netscape4Up = (this.Netscape && (this.Major >= 4));

    this.NetscapeOnly      = (this.Netscape && ((agt.indexOf(";nav") != -1) ||

                          (agt.indexOf("; nav") != -1)) );

    this.Netscape6 = (this.Netscape && (this.Major == 5));

    this.Netscape6Up = (this.Netscape && (this.Major >= 5));

    this.Gecko = (agt.indexOf('gecko') != -1);



    this.IE     = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));

    this.IE3    = (this.IE && (this.Major < 4));

    this.IE4    = (this.IE && (this.Major == 4) && (agt.indexOf("msie 4")!=-1) );

    this.IE4Up  = (this.IE  && (this.Major >= 4));

    this.IE5    = (this.IE && (this.Major == 4) && (agt.indexOf("msie 5.0")!=-1) );

    this.IE5_5  = (this.IE && (this.Major == 4) && (agt.indexOf("msie 5.5") !=-1));

    this.IE5Up  = (this.IE  && !this.IE3 && !this.IE4);

    this.IE5_5Up =(this.IE && !this.IE3 && !this.IE4 && !this.IE5);

    this.IE6    = (this.IE && (this.Major == 4) && (agt.indexOf("msie 6.")!=-1) );

    this.IE6Up  = (this.IE  && !this.IE3 && !this.IE4 && !this.IE5 && !this.IE5_5);

    

    this.W3cDom1 = this.IE5Up || this.Netscape6Up || document.getEltementById;

}


function GetElement( ClientID )
{
     return document.forms[0].elements( ClientID );
}

function GetElt( elt ) 

{

	var browserIs = new BrowserIs();

	if( browserIs.W3cDom1 )

	{

		var obj = document.getElementById( elt );

		if( obj != null )

			return obj;

		return document.getElementByName( elt );

	}

	else if( BrowserIs.IsNavigator4Up ) 

	{

		var currentLayer = document.layers[ GetElt.arguments[0] ];

		for( var i=1; i < GetElt.arguments.length && currentLayer; i++ )

			currentLayer = currentLayer.document.layers[ GetElt.arguments[ i ] ];

		return currentLayer;

    } 

	else if( BrowserIs.IE4Up )

	{

	  	var search = GetElt.arguments[ GetElt.arguments.length-1 ];

		return document.all.item( search );

	}

}


function SetControlClass( Id, className )
{
   var control = GetElt( Id );
  control.className = className;
   
}



/**
 * Provides a subset of functionality similar to MessageFormat.format(String, Object[])
 */
function messageFormat(sBase, oaInserts)
{
	if (sBase == null) return null; // NPE PROTECTION - 1
	if (oaInserts == null) return sBase; // NPE PROTECTION - 2
	nInserts = oaInserts.length; // NUMBER OF INSERTS IN THE STRING
	for (i = 0; i < nInserts; i++)
	{
		sTag = "{" + i + "}"; // THE TAG TO SEARCH FOR
		iTag = sBase.indexOf(sTag); // THE LOCATION OF THE TAG IN THE STRING
		if (iTag == -1) continue; // NPE PROTECTION - 3
		if (oaInserts[i] != null) // NPE PROTECTION - 4
		{
			sPrefix = sBase.substring(0, iTag); // BEFORE THE TAG
			sSuffix = sBase.substring(iTag + 3); // AFTER THE TAG
			sBase = sPrefix + oaInserts[i] + sSuffix; // REBUILD THE BASE
		}
	}
	return sBase;
}

function validateHomeFolder(homeFolder, msgInvalidOutputFolder, msgInvalidStartChar )
{
	if (homeFolder.length == 0)
	{
		alert( msgInvalidOutputFolder );
		return false;
	}
	if (homeFolder.length > 0 && homeFolder.charAt(0) != "/" && homeFolder.charAt(0) != "~")
	{
		alert( msgInvalidStartChar );
		return false;
	}

	var i;
	var invalidCharsArray = new Array("?", "\\", ";", "*", "<", ">", "\"");
	for (i = 0; i<homeFolder.length; i++)
	{
		if (homeFolder.charAt(i) == "/" && i < (homeFolder.length - 1) && homeFolder.charAt(i+1) == "/")
		{
			alert( msgInvalidOutputFolder );
			return false;
		}
		for (var j=0; j < invalidCharsArray.length; j++)
		{
			if (homeFolder.charAt(i) == invalidCharsArray[j])
			{
				alert( msgInvalidOutputFolder );
				return false;
			}
		}
	}
	return true;
}

// Used for the tabs



function getStyleObject(objectId) {
    // cross-browser function to get an object's style object given its id
    if(document.getElementById && document.getElementById(objectId)) {
	// W3C DOM
	return document.getElementById(objectId).style;
    } else if (document.all && document.all(objectId)) {
	// MSIE 4 DOM
	return document.all(objectId).style;
    } else if (document.layers && document.layers[objectId]) {
	// NN 4 DOM.. note: this won't find nested layers
	return document.layers[objectId];
    } else {
	return false;
    }
} // getStyleObject

function changeObjectDisplay(objectId, newVisibility) {
    // get a reference to the cross-browser style object and make sure the object exists
    var styleObject = getStyleObject(objectId);
    if(styleObject) {
	styleObject.display = newVisibility;
	}
}

function changeObjectClass( objectId, classValue )
{

	var object = document.getElementById(objectId);
    if(object) {
	object.className = classValue;
	//alert( objectId + " = " + classValue );
	}
	else
	{
	//alert( objectId + " not found" );
	}
}
    
function ShowMenu(menuNumber,numMenus,menubaseID ) {
    
    for(counter = 0; counter < numMenus; counter++) {
	changeObjectDisplay(menubaseID + counter, 'none');
	}
    var menuId = menubaseID + menuNumber;
    changeObjectDisplay(menuId, 'block');
	
    
}
function setCursor( objectId, pointerValue )
{
	var styleObject = getStyleObject(objectId);
    if(styleObject) {
	styleObject.cursor = pointerValue;
	}
}

function SetActiveTab( tabNumber,numTabs, tabbaseID, hiddenActiveTabFieldID, selectedCssClass, defaultCssClass, selectedLinkCssClass, linkCssClass, disabledLinkCssClass, leftSelectedCssClass, leftDefaultCssClass, rightSelectedCssClass,rightDefaultCssClass )
{
	hiddenActiveTabField = document.getElementById( hiddenActiveTabFieldID );
	for(counter = 0; counter < numTabs; counter++)
	{

		if ( counter != tabNumber )
		{
			changeObjectClass(tabbaseID + 'Left' + counter, leftDefaultCssClass);
			changeObjectClass(tabbaseID + counter, defaultCssClass);
			setCursor( tabbaseID + counter, "pointer" );
			changeObjectClass(tabbaseID + 'Right' + counter, rightDefaultCssClass);
			changeObjectClass(tabbaseID + '_link' + counter, linkCssClass );
		}
	}
    changeObjectClass(tabbaseID + 'Left' + tabNumber, leftSelectedCssClass);
	changeObjectClass(tabbaseID + tabNumber, selectedCssClass);
	changeObjectClass(tabbaseID + '_link' + tabNumber, selectedLinkCssClass );
	changeObjectClass(tabbaseID + 'Right' + tabNumber, rightSelectedCssClass);
	setCursor( tabbaseID + tabNumber, "default" );
	
	hiddenActiveTabField.value = tabNumber.toString( ); 
}

var TableParamWindow = null;
function DisplayTableParameters( url )
									  
{
	var windowName = "AcTableParamWindow";	
	if  ( TableParamWindow != null )
	{
		StrcutureParamWindow.close();
		StrcutureParamWindow = null;
	}
	
	TableParamWindow = window.open( url , windowName, "resizable=1,scrollbars=1");
}

var TableParamEditorWindow = null;
function DisplayTableParameterEditor( url )
									  
{
	
	var windowName = "AcTableParamEditorWindow";	
	
	if  ( TableParamEditorWindow != null )
	{
		TableParamEditorWindow.close();
		TableParamEditorWindow = null;
	}
	
	TableParamEditorWindow = window.open( url , windowName, "resizable=1,scrollbars=1");
	
}

function CloseParameterDisplay()
{
	window.close();
}

function UpdateNotificationList( baseText, attachmentList, notificationListId )
{
	notificationList = document.getElementById( notificationListId );
	attachmentList = document.getElementById( attachmentList );
	if ( attachmentList != null )
	{
		option = attachmentList.options[attachmentList.selectedIndex];
		// notification is visible and has 2 items
		
		if ( notificationList != null && notificationList.length == 2)
		{
		
			notificationList.options[1].text = messageFormat( baseText, new Array( option.text ));
			//notificationList.options[1].value = option.value;
		}
	}
}

