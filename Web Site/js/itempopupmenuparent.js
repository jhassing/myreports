/**
 * WARNING: The complexity of this code is required to deal with asynchronous events.
 *          In particular, we may get a mouseover from the menu before we get a
 *          mouseout from the drop zone. We may also get events a considerable time
 *          after the original physical event occurred and the granularity of the
 *          timers may not be particularly fine.
 *
 *          Bottom line: this code works, do not try to "simplify" it or change
 *          the timings!
 *
 * @author	Actuate Corporation
 *			Copyright (C) 2001 Actuate Corporation. All rights reserved.
 * @version	1.0
 */

// Note different syntax to get the frame and the window/document


var itemPopUpMenuFrame = document.getElementById("ItemPopUpMenuFrame");
var isNetscape = (navigator.appName == "Netscape");

if(isNetscape)
{
	var itemPopUpMenuDocument = itemPopUpMenuFrame;	
}	
else
{
	var itemPopUpMenuDocument = document.frames("ItemPopUpMenuFrame").document;												
}	

var itemPopUpMenuFrameStyle = itemPopUpMenuFrame.style;

var itemPopUpMenuState = "Inactive";
var itemPopUpMenuShowTimer = null;
var itemPopUpMenuHideTimer = null;
var itemPopZone = null;
var activeItemPopZone = null;
var itemPopZoneState = "Inactive";
var param = "";

//	This variable stores the height of a particular menuitem 
//	(Variable specifically usd for NS 6).This variable is set in the ItemPopupMenu.js
var htOfMenuItem = 0;

//	This is the total frame height ie the height of the popup menu
//	(Variable specifically usd for NS 6).This variable is set in the ItemPopupMenu.js
var totalFrameHeight = 0;

function clearActiveItemPopZone() {
	if(activeItemPopZone != null) {
		//activeItemPopZone.parentNode.style.backgroundColor = "";
		//Clear line highlight back to original Row background color
		if (activeItemPopZone.parentNode.className == "TableBody2")  {
			activeItemPopZone.parentNode.parentNode.style.backgroundColor = "#efebd8";
		}
		else  {
			activeItemPopZone.parentNode.parentNode.style.backgroundColor = "";
		}
	}
	activeItemPopZone = null;
}

function scheduleShowItemPopUpMenu() {
	// Cancel any pending showing
	window.clearTimeout(itemPopUpMenuShowTimer);	
	
	itemPopUpMenuShowTimer = window.setTimeout("showItemPopUpMenu();", 350);
}

function scheduleHideItemPopUpMenu() 
{
	window.clearTimeout(itemPopUpMenuHideTimer);	
	itemPopUpMenuHideTimer = window.setTimeout("hideItemPopUpMenu();", 350);
}

function hideItemPopUpMenu() 
{	
	//	This has been done because for each itempopzone the height has to be set to the
	//	total frame height.If we do not do this then the popup menu does not appear correctly 
	if(isNetscape && activeItemPopZone != itemPopZone)
	{
		itemPopUpMenuFrameStyle.height = totalFrameHeight ;
	}
	
	if ( itemPopZoneState == "Inactive"
	    && itemPopUpMenuState == "Inactive") 
	{
		hideItemPopUpMenuImmediately();
	}
}

function hideItemPopUpMenuImmediately() {
	clearActiveItemPopZone();
	itemPopUpMenuFrameStyle.display = "none";
}

/*	
	This function is called for IE only. For Netscape a seperate method is written 
	in the ItemPopupMenu.js which is called on the onload of each itempopup menu.
	This is done because for Netscape we are not able to get the itempopupmenu document here.
	We only get the HTMLIFrameElement.So we handle this seperately on the onload of  
	itempopupmenu.jsp.
	This method gets the disabled menu names from the list page which is passed in the 
	longDesc attribute of img tag in the list page. Depending on the list we disable the
	menu items
*/
function setActiveMenuItems()
{
	if (!isNetscape)
	{		
		param = itemPopZone.longDesc;	
			
		var index = param.lastIndexOf("&DBM=");
			
		var disabledMenuNames = "";
		if (index != -1)
		{
			disabledMenuNames = param.substring(param.lastIndexOf("&DBM=")+5);
		}		
		
		var popUpMenuTable = itemPopUpMenuDocument.getElementById("ItemPopUpMenu");

		var itemPopUpMenuItems = popUpMenuTable.rows;
			
		for (i=0; i < itemPopUpMenuItems.length; i++) 
		{
			//	we store the menu names in the lang attribute			
			var menuItemOps = itemPopUpMenuItems[i].cells[0].lang;
			if (!isStrInArray(menuItemOps, disabledMenuNames))
			{
				itemPopUpMenuItems[i].style.display = "inline";
			}
			else 
			{
				itemPopUpMenuItems[i].style.display = "none";
				if (i-1 != -1 && itemPopUpMenuItems[i-1].cells[0].lang == "LINE" && i+1 < itemPopUpMenuItems.length && itemPopUpMenuItems[i+1].cells[0].lang == "LINE")
				{
					parent.htOfMenuItem  += itemPopUpMenuItems[i-1].offsetHeight ;
					itemPopUpMenuItems[i-1].style.display = "none" ;
				}			
			}
		}
	}	
}

/*
	returns true if str is in the comma separated Str 
*/
function isStrInArray(str, csvStr)
{
	//if lang was not specified, return false...
	if (str == "")
	{
		return false;
	}
	var arr = csvStr.split(",");
	for (var i=0; i<arr.length; i++)
	{
		if (arr[i] == str)
		{
			return true;
		}
	}
	return false;
}

//	This function returns the total height of the disabled menu items.This height 
//	is subtracted from the total frame height to calculate the actual frame height of
//	the itempopup menu.
function calHtOfDsblMenuItems()
{	
	param = itemPopZone.longDesc;
	var index = param.lastIndexOf("&DBM=");
	var disabledMenuNames = "";
	
	if (index != -1)
	{
		disabledMenuNames = param.substring(param.lastIndexOf("&DBM=")+5);
	}		
	//if lang was not specified, return false...
	var arr = disabledMenuNames.split(",");	
	var len = 0;
	
	
	//	if the disabled menu item is a line then the 
	//	height of that menu item is just 1px
	// Disabled menu items should only be considered if a name is present
	//Hence checking for the length of the name passed.
	var count = 0;
	for(var i = 0; i < arr.length; i++)
	{		
		if(arr[i].length > 0)
		{
			len++;
		}
		if(arr[i] == "LINE")
		{
			count++;
		}
	}	
	//alert((count + htOfMenuItem * (len - count)));
	return (count + htOfMenuItem * (len - count));	
}


//	This function is used for the repositioning of the itempopup menu
function repositionItemPopUpMenu()
{

	var menuLeft = itemPopZone.offsetLeft;
	var menuTop = itemPopZone.offsetTop;
	var oP = itemPopZone.offsetParent;
	while (oP != null) 
	{
		menuLeft = menuLeft + oP.offsetLeft;
		menuTop = menuTop + oP.offsetTop;
		oP = oP.offsetParent;
	}
	var horizontalOffset = itemPopZone.offsetWidth + 3;
	var verticalOffset = itemPopZone.offsetHeight + 3;
	
	if(isNetscape)
	{

		//	This value is with "px" appended to it.
		var frameHtInPixels = itemPopUpMenuFrameStyle.height;	
		
		// This value is without "px" prepended 
		var frameHt = frameHtInPixels.substring(0,frameHtInPixels.indexOf("px"));
		
		//This variable is used to calculate the height of disabled menu items
		var length = calHtOfDsblMenuItems();
		
		//	Actual frame height is equal to total frame height - height of disabled 
		//	menu items.
		frameHt  = totalFrameHeight - length;
		
		//	this condition not very accurate. can be corrected
		if ((menuTop + verticalOffset + parseInt(frameHt))
    	> (window.pageYOffset + window.innerHeight - 15))
		{			
			//alert("In If" + verticalOffset);
			verticalOffset = - (parseInt(frameHt)+ 3);
		}
	}	
	else		//	handling for IE
	{
		
		if ((menuTop + verticalOffset + itemPopUpMenuFrameStyle.pixelHeight)
	    > (document.body.scrollTop + document.body.offsetHeight - 15))
		{			
			verticalOffset = - (itemPopUpMenuFrameStyle.pixelHeight + 3);
		}		
	}
	
	//alert( itemPopUpMenuFrameStyle.left );
	menuLeft = menuLeft + horizontalOffset;
	menuTop = menuTop + verticalOffset;
	itemPopUpMenuFrameStyle.left = menuLeft;
	itemPopUpMenuFrameStyle.top = menuTop;		
	
	//alert(menuLeft);
	//alert(menuTop);
}

//	This function handles the resizing of the popup menu.
function resizeItemPopUpMenu(doc)
{
		var itemPopUpMenu = doc.getElementById("ItemPopUpMenu");
		
		itemPopUpMenuFrameStyle.width = itemPopUpMenu.offsetWidth ;		
		if(isNetscape)
		{
			//	calculates the height of the disabled menu items
			var htOfDsblMenus = calHtOfDsblMenuItems();
			
			//	This is the actual frame height of the popup menu
			itemPopUpMenuFrameStyle.height = totalFrameHeight - htOfDsblMenus;

		}
		else			// handling for IE
		{			
			itemPopUpMenuFrameStyle.height = itemPopUpMenu.offsetHeight;			
		}
}


//	This is the actual function which shows the itempopup menu
//	This call the setActiveMenuItems function which is used for displaying only specified
//	menu items.This function is only called for IE.For Netscape the menus are set on the 
//	onload of the itempopup menu.jsp.In Netscape the popup menu is loaded everytime we
//	take the mouse over some image item.

function showItemPopUpMenu() 
{	
	if (itemPopZoneState == "Active") 
	{	
		if(!isNetscape)
		{					
			setActiveMenuItems();
		}	
		
		var temp = itemPopZone.longDesc;
		
		

		
		//	This handling has been done if there are no menu items to 
		//	be displayed ie. if for a particular item in the list page if the 
		//	popup menu does not appear then the longDesc parameter is passed as ""
		//	eg: this has been handled for All Role in Security Roles

		//if(temp == "")
		//{
		
			//itemPopUpMenuFrame.style.display = "none";//vis	
		//}
		//else	// Annoyingly, the menu has to be visible to be resized!
		//{
			itemPopUpMenuFrame.style.display = "inline";//vis
		//}				
		
		if(!isNetscape)	
		{
			resizeItemPopUpMenu(itemPopUpMenuDocument);
		}
		
		
		repositionItemPopUpMenu();
		itemPopZone.parentNode.parentNode.style.backgroundColor = "#c3dffb";
		if (activeItemPopZone != itemPopZone)
		{
			clearActiveItemPopZone();
		}
		
		activeItemPopZone = itemPopZone;
	}
}


// Immediate (not scheduled) display
function showItemPopUpMenuImmediately()
{
	if (!isPopupJspLoaded && !isNetscape)
	{
		return;
	}
	showItemPopUpMenu();
	if (!isNetscape)
	{
		window.event.cancelBubble = true;
	}
}

function enterItemPopZone(imgItem) 
{
	
	if (!isPopupJspLoaded && !isNetscape)
	{
		return;
	}
	itemPopZone = imgItem;
	/*if(isNetscape)
	{		
		itemPopZone = event.target;
	}
	else
	{		
		itemPopZone = window.event.srcElement;
	}*/
		
	//	This handling has been done because in Netscape when we move the mouse pointer from
	//	one image to other the list of popup menu was not getting refreshed.So whenever we 
	//	move the mouse pointer from one image to other then we first hide the popup menu and
	//	then call scheduleShowItemPopUpMenu method.
	if(activeItemPopZone != itemPopZone)
	{
		hideItemPopUpMenu();
	}
		
	itemPopZoneState = "Active";
	
	scheduleShowItemPopUpMenu();
}

function leaveItemPopZone()
{
	if (!isPopupJspLoaded && !isNetscape)
	{
		return;
	}
	
	itemPopZoneState = "Inactive";
	scheduleHideItemPopUpMenu();
	
}

function enterItemPopUpMenu() 
{
	itemPopUpMenuState = "Active";
	scheduleShowItemPopUpMenu();
}

function leaveItemPopUpMenu() 
{	
	itemPopUpMenuState = "Inactive";
	scheduleHideItemPopUpMenu();
}

// Capture events
/*var ItemPopZone = document.getElementsByTagName("img");
var tempId = null;
if (ItemPopZone)
{	
	for (i = 0; i < ItemPopZone.length; i++) 
	{
		if (ItemPopZone[i].id != "TableIcon")
		{			
			ItemPopZone[i].onclick = showItemPopUpMenuImmediately;
			ItemPopZone[i].onmouseover = enterItemPopZone;
			ItemPopZone[i].onmouseout = leaveItemPopZone;
		}
	}
}*/

// POPUP ISSUE
// setting the variable in General.js to indicate loading of the js file

isPopupJsLoaded = true;