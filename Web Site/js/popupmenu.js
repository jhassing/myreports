	var disabledMenus = null;
	var popupMenuParent = null;
	var showMenuTimer = null;
	var hideMenuTimer = null;
	var firstTime = true;
	var itemId = null;
	var itemName = null;
	var lastHighLight = null;
	var highlightedElement = null;
	var FileType = null;
	var description = null;
	var popupMenuWidth = 200;
	
	function disableItems(fileType, isExecutable, disabledList)
	{
		//Default assumption is document
		disabledMenus = "openFolder,viewXLS,viewPDF,viewAnalysis,createQuery,editQuery,executeQuery,scheduleQuery,newBackgroundJob,newImmediateJob";
		if(fileType.toLowerCase() == "directory")
		{
			disabledMenus = "openDocument,viewXLS,viewPDF,viewAnalysis,createQuery,editQuery,executeQuery,scheduleQuery,newBackgroundJob,newImmediateJob";
		}
		else if(fileType.toLowerCase() == "dov")
		{
			disabledMenus = "openFolder,openDocument,viewXLS,viewPDF,viewAnalysis,createQuery,newBackgroundJob,newImmediateJob";
		}
		else if(fileType.toLowerCase() == "dox")
		{
			disabledMenus = "openFolder,openDocument,viewXLS,viewPDF,viewAnalysis,editQuery,executeQuery,scheduleQuery,newBackgroundJob,newImmediateJob";
		}
		else if(fileType.toLowerCase() == "doi")
		{
			disabledMenus = "openFolder,createQuery,editQuery,executeQuery,scheduleQuery,newBackgroundJob,newImmediateJob";
		}
		else if(fileType.toLowerCase() == "odp")
		{
			disabledMenus = "openFolder,viewXLS,viewPDF,viewAnalysis,createQuery,editQuery,executeQuery,scheduleQuery,newBackgroundJob,newImmediateJob";
		}
		else if(isExecutable || fileType.toLowerCase() == "rov")
		{
			disabledMenus = "openFolder,openDocument,viewXLS,viewPDF,viewAnalysis,createQuery,editQuery,executeQuery,scheduleQuery";
		}
		disabledMenus += "," + disabledList
	}

	// attention: querydescription must be the last field in the signature, JSP AP does not use it.
	function scheduleShowMenu(source, fileType, isExecutable, id, name, disabledList, querydescription)
	{
		disableItems(fileType, isExecutable, disabledList);	
		if(hideMenuTimer != null)
		{
			hideMenu();
		}
		if(showMenuTimer != null)
		{
			window.clearTimeout(showMenuTimer);
		}
		popupMenuParent = source;
		itemId = id;
		description = querydescription
		itemName = name;
		FileType = fileType;
		makeAllVisible();
		showMenuTimer = window.setTimeout("showMenu()", 500);
	}
	
	function makeAllVisible()
	{
		popupMenu = document.getElementById("popup");
		tables = popupMenu.getElementsByTagName("table");
		if( tables.length == 1)
		{
			menuTable = tables.item(0);

			menuRows = menuTable.rows;
			for(m = 0; m < menuRows.length; m++)
			{
				menuRow = menuRows[m];
				menuRow.style.display = "";
			}
		}
		popupMenu.style.height = menuTable.offsetHeight;
	}

	function showMenu()
	{
		if(showMenuTimer != null)
		{
			window.clearTimeout(showMenuTimer);
			showMenutimer = null;
		}
		if(hideMenuTimer != null)
		{
			window.clearTimeout(hideMenuTimer);
		}

		popupMenu = document.getElementById("popup");

		topOffset = sumOffsetTop(popupMenuParent);
		leftOffset = sumOffsetLeft(popupMenuParent);
		popupTop = topOffset + popupMenuParent.offsetHeight / 2;
		popupLeft = leftOffset + popupMenuParent.offsetWidth / 2;
		
		if(firstTime)
		{
			//We need to do this twice because the first time the size is calculated incorrectly.
			popupMenu.style.visibility="visible";
			resizeMenu(popupMenu);
			//popupMenu.style.width = popupMenuWidth;
			popupMenu.style.left = popupLeft;
			popupMenu.style.top = popupTop;
			popupMenu.style.visibility="hidden";
			firstTime = false;
		}
		
		if(resizeMenu(popupMenu))
		{
			//popupMenu.style.width = popupMenuWidth;
			popupMenu.style.left = popupLeft;
			popupMenu.style.top = popupTop;
			
			popupBottom = 0;
			fullHeight = 0;
			currentTopY = 0;
			popupRight = 0;
			fullWidth = 0;
			currentLeftX = 0;
			if(document.body.scrollTop == undefined)
			{
				//NS 6
				popupBottom =  popupTop + popupMenu.offsetHeight;
				currentTopY = window.pageYOffset;
				fullHeight = currentTopY + window.innerHeight;
				
				popupRight = popupLeft + popupMenu.offsetWidth;
				currentLeftX = window.pageXOffset;
				fullWidth = currentLeftX + window.innerWidth;
			}
			else
			{
				//IE, Fire bird
				popupBottom =  popupTop + popupMenu.offsetHeight;
				currentTopY = document.body.scrollTop;
				fullHeight = currentTopY + document.body.clientHeight;

				popupRight = popupLeft + popupMenu.offsetWidth;
				currentLeftX = document.body.scrollLeft;
				fullWidth = currentLeftX + document.body.clientWidth;
			}
			
			if(popupBottom > fullHeight)
			{
				popupTop = popupTop - popupMenu.offsetHeight;
				
				popupBottom =  popupTop + popupMenu.offsetHeight;

				//If by centering it we are going below the scroll bar then move it above the icon
				if(popupBottom > fullHeight)
				{
					popupTop = popupTop - (popupMenuParent.offsetHeight / 2);
				}
				
				if(popupTop < currentTopY)
				{
					popupTop = currentTopY;
				}
				popupMenu.style.top = popupTop;
			}
			
			if(popupRight > fullWidth)
			{
				popupLeft = popupLeft - popupMenu.offsetWidth;
				
				popupRight = popupLeft + popupMenu.offsetWidth;
				//If by centering it we are going below the scroll bar then move it to the left of the icon
				if(popupRight > fullWidth)
				{
					popupLeft = popupLeft - (popupMenuParent.offsetWidth / 2);
				}
				
				if(popupLeft < currentLeftX)
				{
					popupLeft = currentLeftX;
				}
				popupMenu.style.left = popupLeft;
			}
			
			popupMenu.style.visibility="visible";
			
			if(highlightedElement != null)
			{
				highlightedElement.className = lastHighlight;
			}
			//Now highlight the item that activated this
			highlightedElement = document.getElementById("item" + itemId);
			if(highlightedElement != null)
			{
				lastHighlight = highlightedElement.className;
				highlightedElement.className = "popupMenuSelection";
			}
		}
	}

	function resizeMenu(menuToResize)
	{
		tables = menuToResize.getElementsByTagName("table");

		arrDisabledItems = null;
		if(disabledMenus != null && disabledMenus.length > 0 )
		{
			arrDisabledItems = disabledMenus.split(",");
		}

		//There should just be one table present.
		//If not just don't resize it and return a false from here.

		if( tables.length == 1)
		{
			menuTable = tables.item(0);

			if (arrDisabledItems != null)
			{
				menuRows = menuTable.rows;
				continuousSeparators = 0;
				lastVisibleRow = -1;
				lastVisibleSeparator = 0;
				for(m = 0; m < menuRows.length; m++)
				{
					menuRow = menuRows[m];
					bHide = false;
					if( menuRow.id == "popup__separator" )
					{
						continuousSeparators = continuousSeparators + 1;
						if(continuousSeparators == 2 || lastVisibleRow == -1)
						{
							continuousSeparators = continuousSeparators - 1;
							menuRow.style.display = "none";
						}
						else
						{
							lastVisibleSeparator = m;
						}
					}
					else
					{
						for( n = 0; n < arrDisabledItems.length && !bHide; n++)
						{
							if(arrDisabledItems[n] == menuRow.id)
							{
								bHide = true;
							}
						}

						if(bHide)
						{
							menuRow.style.display = "none";
						}
						else
						{
							menuRow.style.display = "";
							lastVisibleRow = m;
							continuousSeparators = 0;
						}
					}
				}

				//if the last item is a separator then hide it
				if(lastVisibleRow < lastVisibleSeparator)
				{
					menuRows[lastVisibleSeparator].style.display = "none";
				}
			}
			menuToResize.style.height = menuTable.offsetHeight + (menuTable.offsetTop * 2);

			return true;
		}

		return false;
	}

	
	function hideMenu()
	{
		if(showMenuTimer != null)
		{
			window.clearTimeout(showMenuTimer);
			showMenuTimer = null;
		}
		
		if(hideMenuTimer != null)
		{
			window.clearTimeout(hideMenuTimer);
		}
		
		if(highlightedElement != null && lastHighlight != null)
		{
			if(highlightedElement != null)
			{
				highlightedElement.className = lastHighlight;
			}
			lastHighlight = null;
			highlightedElement = null;
		}
		
		hideMenuTimer = null;
		popupMenu = document.getElementById("popup");
		popupMenu.style.visibility="hidden";
	}
	
	function scheduleHideMenu()
	{
		if(showMenuTimer != null)
		{
			window.clearTimeout(showMenuTimer);
			showMenuTimer = null;
		}
		
		if(hideMenuTimer != null)
		{
			window.clearTimeout(hideMenuTimer);
		}

		hideMenuTimer = window.setTimeout("hideMenu()", 500);
	}
	
	function highlightItem(item, bHighlight)
	{
		item.className = (bHighlight?'popupMenuItemSelected':'popupMenuItem');
	}
	
	function enterMenu()
	{
		if(hideMenuTimer != null)
		{
			window.clearTimeout(hideMenuTimer);
		}
	}

	function leaveMenu()
	{
		scheduleHideMenu();
	}

	function sumOffsetTop(element) 
	{
		offsetTop = 0;
		while (element != null) 
		{
			offsetTop += element.offsetTop;
	      	element = element.offsetParent;
		}
		return offsetTop;
	}
	    
	function sumOffsetLeft(element) 
	{
		offsetLeft = 0;
		while (element != null) 
		{
			offsetLeft += element.offsetLeft;
	      	element = element.offsetParent;
		}
		return offsetLeft;
	}

	
	function performAction(source)
	{
		doAction(itemId, itemName, source.id, description);
		hideMenu();
	}
