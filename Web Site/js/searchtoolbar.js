/**
 * 
 *
 * @author  Actuate Corporation
 * @version 1.0
 */
 
function changeClass(id, cName)
{
	//handling for NS4 needs work around, as nothing seems to work
	if (!ns4)
	{
		document.getElementById(id).className = cName;
	}
}

function setClose( classname )
{
    if( buttonsEnabled )
        changeClass('closeButton', classname);
}

function setSearchnow( classname )
{
    if( buttonsEnabled ) {
        changeClass('searchnowButton', classname);
    }
}

function setNewsearch( classname )
{
    if( buttonsEnabled )
       changeClass('newsearchButton', classname);
}

function setHelp( classname )
{
    if( buttonsEnabled )
        changeClass('helpButton', classname);
}

function clickClose()
{
    if( buttonsEnabled )
        parent.frames.toolbarframe.searchClick();
}

function clickNewsearch()
{
    if( ! buttonsEnabled )
	    return;

    if(  isInRequestMode())
	{
	    if( ! parent.frames.searchframe.clearButton )
		    parent.frames.searchframe.location.replace( parent.g_searchFrameURL );
		else
		    parent.frames.searchframe.clearButton();
	}
	else
	{
	    if( ! parent.frames.searchframe.showSearchRequestForm ) {
			parent.g_searchResults = false;
		    parent.frames.searchframe.location.replace( parent.g_searchFrameURL );
			document.location.replace(location.href);
		}
		else
		    parent.frames.searchframe.showSearchRequestForm();

	}
}

function clickSearchnow()
{
    if( ! buttonsEnabled )
	    return;

    if( parent.frames.searchframe.searchButton )
	    parent.frames.searchframe.searchButton();
}

function enableSearchToolbar()
{ 
    buttonsEnabled = true;
	setNewsearch( classLinkOff );
	if (isInRequestMode())
 		setSearchnow( classLinkOff );
	else
 		setSearchnow( classLinkDisable );
	setClose( classLinkOff );
}  

function disableSearchToolbar()
{ 
	setNewsearch( classLinkDisable );
	setSearchnow( classLinkDisable );

	setClose( classLinkDisable );
    buttonsEnabled = false;
}

function isInRequestMode()
{
	return (parent.g_inSearchRequestForm || parent.g_searchResults == false);
}

function STOnLoad() 
{
	// For browsers with the onload handler problem, we make sure onLoad was
	// not already called by forceLoad()
	if (g_browserType.doesNotAlwaysCallOnloadHandler && searchToolbarLoaded)
		return;

	if (ns4)
	{
		if (parent.g_bReportFrameLoaded)
			enableSearchToolbar();
	}
	else
	{
		enableSearchToolbar();
	}

	searchToolbarLoaded = true;
}

function forceOnLoad()
{
	if (g_browserType.doesNotAlwaysCallOnloadHandler && !searchToolbarLoaded)
		STOnLoad();
}
