/**
 *
 *
 * @author	Actuate Corporation
 *			Copyright (C) 2001 Actuate Corporation. All rights reserved.
 * @version	1.0
 */

function highlightPopUpMenuItem(element) {
	menuItem = element;//vis
	menuItem.style.color="#336699";
	menuItem.style.backgroundColor="#c3dffb";
	menuItem.style.borderTop = menuItem.style.borderBottom = menuItem.style.borderLeft = menuItem.style.borderRight = "1px solid #336699";
	if(!parent.isNetscape)
		parent.resizeItemPopUpMenu(parent.itemPopUpMenuDocument);
}

function unHighlightPopUpMenuItem(element) {
	menuItem = element;
	menuItem.style.color="";
	menuItem.style.backgroundColor="";
}

function selectPopUpMenuItem() {
	menuItem = window.event.srcElement;
	window.parent.execScript("hideItemPopUpMenuImmediately();");
	doMenuAction(menuItem.value);
}

function enterMenu() 
{
	window.parent.enterItemPopUpMenu();	
}

function leaveMenu() 
{	
	callResizeFunc();
	window.parent.leaveItemPopUpMenu();
}

/*
 * Used to persist the pop up menu whenever a confirm is shown.
 */
	function showConfirm(par, msg)
	{
		par.itemPopZoneState = "Active";
        par.scheduleShowItemPopUpMenu();
        var con = confirm(msg);
        par.itemPopZoneState = "Inactive";
        par.scheduleHideItemPopUpMenu();
        return con;
	}

/*
 * Used for setting the size of the popupmenuframe if the browser is NS
 */
function callResizeFunc()
{	
	if(parent.isNetscape)
	{
		parent.resizeItemPopUpMenu(document);
	}
}

/*	
	This function is called for NS only. For IE a seperate method is written 
	in the ItemPopupMenuParent.js.This is done because for Netscape we are not able to get
	the itempopupmenu document.
	We only get the HTMLIFrameElement.So we handle this seperately on the onload of  
	itempopupmenu.jsp.
	This method gets the disabled menu names from the list page which is passed in the 
	longDesc attribute of img tag in the list page. Depending on the list we disable the
	menu items
*/
function setActiveMenus()
{
	parent.isPopupJspLoaded = true;
	if (navigator.appName == "Netscape")
	{		
		parent.param = parent.itemPopZone.longDesc;				
		var disabledMenuNames = parent.param.substring(parent.param.lastIndexOf("&DBM=")+5);
		
		var popUpMenuTable = document.getElementById("ItemPopUpMenu");
		var itemPopUpMenuItems = popUpMenuTable.rows;
		parent.totalFrameHeight = 0;
		var storeOldLine = "--no -- line --"; //Set a dummy value for the first line in case there are no disabled items.
		for (i = 0; i < itemPopUpMenuItems.length; i++) 
		{
			var menuItemOps = itemPopUpMenuItems[i].cells[0].lang;
			
			parent.htOfMenuItem = itemPopUpMenuItems[i].offsetHeight;
			parent.totalFrameHeight += itemPopUpMenuItems[i].offsetHeight;			
			if (!parent.isStrInArray(menuItemOps, disabledMenuNames))
			{					
				// This has been handled to check if lang value == LINE and if after disabling some of 
				//	the menu items the two LINE options comes together. If this is the case then we have to
				//	hide one LINE option otherwise in Netscape we get a thick line because the two lines are 
				//  drawn adjacent to each other.
				
				if(storeOldLine == menuItemOps)
				{
					//itemPopUpMenuItems[i].style.visibility = "hidden";
					itemPopUpMenuItems[i].style.display = "none";
				}
				else
				{
					//	This has been done because if we use 
					//	itemPopUpMenuItems[i].style.display="inline" 
					//  it is not working properly				
					itemPopUpMenuItems[i].style.visibility = "visible";
				}
				storeOldLine = menuItemOps;
			}						
			else 						
			{
				//itemPopUpMenuItems[i].style.visibility = "hidden";
				itemPopUpMenuItems[i].style.display = "none";				
			}
		}		
		callResizeFunc();
		parent.repositionItemPopUpMenu();
	}
}


// Hide the frame
var parentFrameStyle = window.parent.document.getElementById("ItemPopUpMenuFrame").style;
//parentFrameStyle.display = "none";

// Capture events
/*var PopUpMenuItem = document.all.item("PopUpMenuItem");

if(PopUpMenuItem)
{
	for (i = 0; i < PopUpMenuItem.length; i++) {
		PopUpMenuItem[i].onmouseover = highlightPopUpMenuItem;
		PopUpMenuItem[i].onmouseout = unHighlightPopUpMenuItem;
		PopUpMenuItem[i].onclick = selectPopUpMenuItem;
	}
}*/


document.onmouseover = enterMenu;
document.onmouseout = leaveMenu;