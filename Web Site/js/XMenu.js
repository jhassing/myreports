<!--
var _version = 1.0;

if (parseInt(navigator.appVersion.charAt(0)) >= 3)
{
	_version = 1.1;
}

if (_version >= 1.1)
{
	var arrowDown = new Image();
	var arrowUp = new Image();
}

var divGroupName = "";
var openDisplay = "inline";
var closedDisplay = "none";

//*********
//expandit
//*********
function expandit(itemId, hideImage, showImage, groupName)
{
	if (_version>=1.1)
	{
		window.setTimeout("toggleSection('" + itemId + "', '" + groupName + "')", 10);
		arrowDown.src = hideImage;
		arrowUp.src = showImage;
	}
}

//**************
//expandSubMenu
//**************
function expandSubMenu(itemId, hideImage, showImage, groupName)
{
	if (_version>=1.1)
	{
		window.setTimeout("toggleSubMenuSection('" + itemId + "', '" + groupName + "')", 10);
		arrowDown.src = hideImage;
		arrowUp.src = showImage;
	}
}

//**************
//toggleSection
//**************
function toggleSection(itemId, groupName)
{
	if (_version>=1.1)
	{
		var arrowId = itemId.substring(0, itemId.indexOf(";")).replace("SectionPanel", "ArrowImage");
		var arrow, item;

		if (document.all)
		{
			item = document.all[itemId];
			arrow = document.all[arrowId];
		}

		if (!document.all && document.getElementById)
		{
			item = document.getElementById(itemId);
			arrow = document.getElementById(arrowId);
		}
    
		if (item.style.display == closedDisplay)
		{
			if (groupName != '')
			{
				var divs = document.getElementsByTagName('table');			
				for (var i=0; i < divs.length; i++ )
				{
					divGroupName = divs[i].id.substring(divs[i].id.indexOf(";") + 1, divs[i].id.lastIndexOf(";"));
					if (divs[i].id.indexOf("SectionPanel") > -1 && divGroupName == groupName)
					{
						divs[i].style.display = closedDisplay;
						var arrowname = divs[i].id.substring(0, itemId.indexOf(";")).replace("SectionPanel", "ArrowImage");
						arrow1 = document.getElementById(arrowname);

						if (arrow1 != null)
						arrow1.src = itemId.substring(itemId.lastIndexOf(";") + 1);
					}
					divGroupName = "";
				}
			}

			if (arrow != undefined)
				arrow.src = arrowDown.src;
			
			item.style.display = openDisplay;
		} else {
			if (arrow != undefined)
				arrow.src = arrowUp.src;
			
			item.style.display = closedDisplay;
		}
	}
}

//*********************
//toggleSubMenuSection
//*********************
function toggleSubMenuSection(itemId, groupName)
{
	if (_version>=1.1)
	{
		var arrowId = itemId + 'ArrowImage';
		var arrow, item;

		if (document.all)
		{
			item = document.all[itemId];
			arrow = document.all[arrowId];
		}

		if (!document.all && document.getElementById)
		{
			item = document.getElementById(itemId);
			arrow = document.getElementById(arrowId);
		}

		if (item.style.display == closedDisplay)
		{
			if (groupName != '')
			{
				var divs = document.getElementsByTagName('table');			
				for (var i=0; i < divs.length; i++ )
				{
					divGroupName = divs[i].id.substring(divs[i].id.indexOf(";") + 1, divs[i].id.lastIndexOf(";"));
					if (divs[i].id.indexOf("SubMenu") > -1 && divGroupName == groupName)
					{
						divs[i].style.display = closedDisplay;
						var arrowname = arrowId;

						arrow1 = document.getElementById(arrowname);
						arrow1.src = itemId.substring(itemId.lastIndexOf(";") + 1);
					}
					divGroupName = "";
				}
			}

			if (arrow != undefined)
				arrow.src = arrowDown.src;
			
			item.style.display = openDisplay;
		} else {
			if (arrow != undefined)
				arrow.src = arrowUp.src;
		
			item.style.display = closedDisplay;
		}
	}
}

function HeaderOver(id)
{
	var nChild = id.childNodes.length-1;
	
	id.firstChild.className = "XMenu_Over";	
	for (var y=0;y<nChild;y++)
		id.childNodes(y).nextSibling.className = "XMenu_Over";			
}
function HeaderOut(id)
{
	var nChild = id.childNodes.length-1;
	
	id.firstChild.className = "XMenu_Out";	
	for (var y=0;y<nChild;y++)
		id.childNodes(y).nextSibling.className = "XMenu_Out";	
}
// -->
