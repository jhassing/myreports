/**
 *
 *
 * @author	Actuate Corporation
 *			Copyright (C) 2001 Actuate Corporation. All rights reserved.
 * @version	1.0
 */


/**
 * This vaiable stores the forward url to perform the escape event
 */
var fwdURL = null;

/**
 * These variables store the server url and volume
 */
 var sServerURL = null;
 var sVolume = null;


/**
 * Disables fields according to external properties set.
 * @param frmName The full name of the form whose fields are to be disabled.
 * @param extSecurityCredentials The external security credentials for the logged in user.
 *		This string is in the format:
 *		Each field name is separated with a "^", and the information for each
 *		tab is separated with a "|".
 *		For eg : "tab1FldName1^tab1FldName2|tab2FldName1^tab2FldName2" (for tabbed sheets)
 *				"fldName1^fldName2" (for non-tabbed sheets).
 * @param tabIndex The index of the tab starting with 0. This should be 0 for non-tabbed sheets.
 */
function disableFieldsForSecurity(frmName, extSecurityCredentials, tabIndex)
{
	if (extSecurityCredentials == "") return;

	var arrTabTokens = extSecurityCredentials.split("|");
	//to avoid any js error.
	if (arrTabTokens.length == 0 || arrTabTokens.length <= tabIndex)
	{
		return;
	}

	var sTabToken = arrTabTokens[tabIndex];
	var arrFieldTokens = sTabToken.split("^");
	var fldObject = null;
	for (var fldItr=0; fldItr < arrFieldTokens.length; fldItr++)
	{
		var sFieldToken = arrFieldTokens[fldItr];

		if (sFieldToken == "") continue;

		fldObject = eval(frmName+"."+sFieldToken);

		if (fldObject != null)
		{

			//if the element has a length property...
			if (fldObject.length != null)
			{
				//if it is a radio set array,
				if (fldObject.length > 0 && fldObject[0].type == "radio")
				{
					toggleRadioSet(fldObject, false, -1);
					continue;
				}
			}
			switch (fldObject.type)
			{
				case "text" :
					enableText(fldObject, false);
					break;
				case "password" :
					enableText(fldObject, false);
					break;
				case "file" :
					enableText(fldObject, false);
					break;
				case "textarea" :
					enableText(fldObject, false);
					break;
				case "checkbox" :
					fldObject.disabled = true;
					break;
				case "radio" :
					fldObject.disabled = true;
					break;
				case "select-one" :
					enableText(fldObject, false);
					break;
				case "select-multiple" :
					enableText(fldObject, false);
					break;
				case "submit" :
					fldObject.disabled = true;
					break;
				case "button" :
					fldObject.disabled = true;
					break;
				case "reset" :
					fldObject.disabled = true;
					break;
			}
		}
	}
}

/**
 * used in all drop down menu jsps to get single/multiple selection status.
 * @return the checkbox object -> if only one is selected.
 * 			null -> If multiple items are selected or
 * 			0    -> if no item is selected
 * @param formName the fully qualified name of the form
 * @author Vaishali
 */
function getSingleCheckboxStatus(formName)
{
	var frm = eval(formName);

	var selectedItems = 0;
	var selectedCheckBox = null;

	for (var i = 1; i < frm.elements.length; i++)
	{
		if (frm.elements[i].type == "checkbox")
		{
			if (frm.elements[i].checked == true)
			{
				selectedItems++;
				//if multiple items are selected then return null
				if (selectedItems > 1)
				{
					return null;
				}
				selectedCheckBox = frm.elements[i];
			}
		}

	}
	//if no item is selected then return 0
	if (selectedItems == 0)
	{
		return 0;
	}
	//if only single item is selected then return the item
	return selectedCheckBox;
}

/**
 * returns the number of items selected in the list / result page.
 * @param formName the fully qualified name of the form
 * @author Rasagna
 */
function getNumberOfItemsSelected(formName)
{
	var frm = eval(formName);

	var selectedItems = 0;

	for (var i = 1; i < frm.elements.length; i++)
	{
		if (frm.elements[i].type == "checkbox")
		{
			if (frm.elements[i].checked == true)
				selectedItems++;
		}
	}
	return selectedItems;
}

function validateVar(obj)
{
	if ((eval("typeof  obj") ) !="undefined" && obj != null)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * is called when Esc key is pressed and incorporates the
 * action handled by the cancel button in all the pages.
 * @param the window event of the page.
 * @param url OPTIONAL parameter. Needs to be passed if u dont want to use the
 * 		fwdURL variable, but need to redirect to other url.
 */
function onEscapeKey(event, url)
{
	var tmpURL = null;
	if (validateVar(fwdURL))
	{
		tmpURL = fwdURL;
	}
	else
	{
		tmpURL = 'main.jsp';
	}

	//if the url parameter has been passed, then consider that.
	if (url)
	{
		tmpURL = url;
	}

	if (!event)
	{
		if (window.event.keyCode == 27)
		{
			location.href = tmpURL;
		}
	}
	else
	{
		event.cancelBubble=true;
		if (event.keyCode == 27)
		{
			location.href= tmpURL;
		}
	}
}

/**
 * This Function is called when Esc key is pressed
 * to close the pop up window.
 * @ param the window event of the page.
 */
function onEscClosePopupWindow(event)
{
	if (!event)
	{
		if (window.event.keyCode == 27)
		{
			window.close();
		}
	}
	else
	{
		event.cancelBubble=true;
		if (event.keyCode == 27)
		{
			window.close();
		}
	}
}

/**
 * selects/deselects all the checkboxes in a form.
 * @param cbObj the checkbox object on whose selection the other checkboxes have to be checked/unchecked.
 * @param formName the fully qualified name of the form
 * @param disableFlag whether all checkboxes have to be disabled.
 * @author Aarti
 */
function toggleAllChkBoxes(cbObj, formName, disableFlag)
{
	var status = cbObj.checked;
	var frm = eval(formName);

	if (frm == null) return;

	for (var i=0; i<frm.elements.length; i++)
	{
		if (frm.elements[i].type == "checkbox")
		{
			frm.elements[i].checked = status;
			if (disableFlag)
			{
				frm.elements[i].disabled = status;
			}
		}
	}
}

/**
 * selects/deselects the checkbox which is responsible for
 * selecting/deselecting all checkboxes in this list/resultlist form.
 * @param cbObj the checkbox object on whose selection the other checkboxes have to be checked/unchecked.
 * @param thisFormName the fully qualified name of the form in which the
 * 		clicked checkbox is.
 * @author Aarti
 */
function toggleParentCheckbox(cbObj, thisFormName)
{
	var frm = eval(thisFormName);

	if (frm == null) return;

	var allCb = null;
	var selCheckboxCnt = 0;
	var i = 0;

	//find the first checkbox in the form.
	for (i=0; i<frm.elements.length; i++)
	{
		if (frm.elements[i].type == "checkbox")
		{
			allCb = frm.elements[i];
			break;
		}
	}

	if (!cbObj.checked)
	{
		allCb.checked = false;
		return;
	}

	var bNotChecked = false;
	//iterate from next element onwards so that (first checkbox identified
	//above is skipped) Dinesh
	for (i=i+1; i<frm.elements.length; i++)
	{

		if (frm.elements[i].type == "checkbox")
		{
			if (!frm.elements[i].checked)
			{

				bNotChecked = true;
			}
			else
			{
				selCheckboxCnt++;
			}
			if (bNotChecked && selCheckboxCnt > 0)
			{
				break;
			}
		}
	}

	allCb.checked = !bNotChecked;
}

//used to enable/disable text boxes
function enableText(cmp, bEnable)
{
	if (bEnable)
	{
		cmp.disabled = false;
		cmp.style.background = "white";
	}
	else
	{
		cmp.disabled = true;
		cmp.style.background = "threedface";
	}
}

//used to enable/disable combo boxes
function enableComboBox(cmp, bEnable)
{
	if (bEnable)
	{
		cmp.disabled = false;
		cmp.style.background = "white";
	}
	else
	{
		cmp.disabled = true;
		cmp.style.background = "threedface";
	}
}

function changeFrameSrc(frameIndex, url)
{
	var myframe = document.getElementsByTagName("IFRAME").item(frameIndex);
	//	This fix is for Netscape. In Netscape when we click on the same tab(Eg In Job Pages) again then the frame was not getting
	//	refreshed. This was a Netscape specific behaviour. So we had to change the src twice.
	myframe.src = "";
	myframe.src = url;
}

//used to enable/disable radio buttons
function enableRadioButton(cmp, bEnable)
{
	if (bEnable)
	{
		for(var i=0; i < cmp.length; i++)
		{
			cmp[i].disabled = false;
		}
	}
	else
	{
		for(var i=0; i < cmp.length; i++)
		{
			cmp[i].disabled = true;
		}
	}
}

//adds one hidden form field with the specified name and value to the form passed to it.
function createHiddenFormField (form, name, value)
{
	var ip = document.createElement("input");
	ip.setAttribute ("type", "hidden");
	ip.setAttribute ("name", name);
	ip.setAttribute ("value", value);
	form.appendChild(ip);
}

//adds one hidden form field with the specified name and value to the form passed to it.
function createHiddenFormFieldDoc (doc, form, name, value)
{
	var ip = doc.createElement("input");
	ip.setAttribute ("type", "hidden");
	ip.setAttribute ("name", name);
	ip.setAttribute ("value", value);
	form.appendChild(ip);
}

/**
 * simply adds the specified value to the specified array.
 */
function addToArray(arr,value)
{
	arr[arr.length] = value;
}

/**
 * This takes an array of radio buttons and returns the index of the selected item.
 * If no radio is selected, then it returns -1.
 * @param noSelectionValue optional. value to be returned in case no radio is selected
 */
function getCheckedItemIndex(arr, noSelectionValue)
{
	for (var i =0; i < arr.length; i++)
	{
		if (arr[i].checked)
		{
			return i;
		}
	}
	if (noSelectionValue)
	{
		return noSelectionValue;
	}
	return -1;
}

/**
 * This takes an array of radio buttons and returns the index of the radio whose value
 * attribute matches the parameter.
 * If no radio value is matching, then it returns -1.
 * @param noSelectionValue optional. value to be returned in case no radio value is matching
 */
function getIndexForRadioValue(arr, value, noSelectionValue)
{
	for (var i =0; i < arr.length; i++)
	{
		if ( arr[i].value.toLowerCase() == value.toLowerCase() )
		{
			return i;
		}
	}
	if (noSelectionValue)
	{
		return noSelectionValue;
	}
	return -1;
}

/**
 * This will toggle the disabled as well as checked status for
 * a set of radio buttons.
 * @param rbArray the array of radio buttons
 * @param enableFlag whether the set is to be enabled
 * @param checkIndex which element of the array is to be checked.
 *			if -1 is passed, then the radio buttons checked status will be unchanged.
 *			NOTE : You can pass any other -ve value to uncheck all the radio buttons.
 * @author Aarti
 */
function toggleRadioSet(rbArray, enableFlag, checkIndex)
{
	for (var i=0; i < rbArray.length; i++)
	{
		rbArray[i].disabled = !enableFlag;
		if (checkIndex == -1) continue;
		rbArray[i].checked = (i == checkIndex);
	}
}

/**
 * changes the background color of a particular TD to make it appear active.
 * called on load of all tab pages. (to support 'Back', 'Forward')
 */
function makeTabActive(doc, tableIndex, selectedTabIndex, disabledTabs)
{
	//toggle the classnames.
	var mytable = doc.getElementsByTagName("TABLE").item(tableIndex);

	/* EACH TAB CONSISTS OF 2 CELLS */
	var mycells = mytable.rows[0].cells;

	//depending upon the type of the tab, select the class of the tab.
	var unselectedTabClassName, selectedTabClassName;

	unselectedTabClassName = "SubMenuTab";
	selectedTabClassName = "SubMenuTabSelected";

	for (var i=0; i < mycells.length; i++)
	{
		//selected tab text <TD>
		if (selectedTabIndex * 2 == i)
		{
			mycells[i].className = selectedTabClassName;
		}
		//selected tab image <TD>
		else if ((selectedTabIndex * 2) + 1 == i)
		{
			mycells[i].className = "SubMenuActiveImage";
		}
		//unselected tab text
		else if (i % 2 == 0)
		{
			mycells[i].className = unselectedTabClassName;
		}
		//unselected tab image <TD>
		else if (i % 2 == 1)
		{
			mycells[i].className = "SubMenuInactiveImage";
		}
	}

	var myLinks = doc.getElementsByName("lnkTabs");
	for (var i=0; i < myLinks.length; i++)
	{
		myLinks[i].className = "SubMenuLink";
		if (i == selectedTabIndex)
		{
			myLinks[i].className = "SubMenuSelected";
		}
	}

	// Disabling the restricted tab pages
	if (disabledTabs)
	{
		for (var i=0; i < disabledTabs.length; i++)
		{
			if( disabledTabs[i] == -1)
				continue;
			myLinks[disabledTabs[i]].className = "SubMenuLinkDisable";
		}
	}
}

//changes the background color of a particular TD to make it appear active.
//called on load of all tab pages. (to support 'Back', 'Forward')
//is used wherever the tabs are split into two or more rows.
function makeLargeTabActive(doc, selectedTabIndex)
{
	//toggle the classnames.tbody
	var tables = doc.getElementsByTagName("TABLE");

	var cellIndex = 0;

	/* EACH TAB CONSISTS OF 2 CELLS */
	for (var tablIndex=0; tablIndex < tables.length; tablIndex++)
	{
		var mytable = tables[tablIndex];
		if (mytable.id == 'TabTable')
		{
			var mycells = mytable.rows[0].cells;

			for (var i=0; i < mycells.length; i++)
			{
				//selected tab text <TD>
				if (selectedTabIndex * 2 == cellIndex)
				{
					mycells[i].className = "SubMenuTabSelected";
				}
				//selected tab image <TD>
				else if ((selectedTabIndex * 2) + 1 == cellIndex)
				{
					mycells[i].className = "SubMenuActiveImage";
				}
				//unselected tab text
				else if (cellIndex % 2 == 0)
				{
					mycells[i].className = "SubMenuTab";
				}
				//unselected tab image <TD>
				else if (cellIndex % 2 == 1)
				{
					mycells[i].className = "SubMenuInactiveImage";
				}
				cellIndex++;
			}
		}
	}

	var myLinks = doc.getElementsByName("lnkTabs");

	for (var i=0; i < myLinks.length; i++)
	{
		myLinks[i].className = "SubMenuLink";
		if (i == selectedTabIndex)
		{
			myLinks[i].className = "SubMenuSelected";
		}
	}
}

/**
 * Disables all the tabs.
 */
function disableAllTabs(doc, tableIndex)
{
	//toggle the classnames.
	var mytable = doc.getElementsByTagName("TABLE").item(tableIndex);

	/* EACH TAB CONSISTS OF 2 CELLS */
	var mycells = mytable.rows[0].cells;

	for (var i=0; i < mycells.length; i++)
	{
		//unselected tab text
		if (i % 2 == 0)
		{
			mycells[i].className = "SubMenuTab";
		}
		//unselected tab image <TD>
		else if (i % 2 == 1)
		{
			mycells[i].className = "SubMenuInactiveImage";
		}
	}

	var myLinks = doc.getElementsByName("lnkTabs");
	for (var i=0; i < myLinks.length; i++)
	{
		myLinks[i].className = "SubMenuLinkDisable";
	}
}

/**
 * Used to disable all large tabs i.e. two or more rows of tabs
 */
function disableAllLargeTabs(doc)
{
	//toggle the classnames.tbody
	var tables = doc.getElementsByTagName("TABLE");

	var cellIndex = 0;

	/* EACH TAB CONSISTS OF 2 CELLS */
	for (var tablIndex=0; tablIndex < tables.length; tablIndex++)
	{
		var mytable = tables[tablIndex];
		if (mytable.id == 'TabTable')
		{
			var mycells = mytable.rows[0].cells;

			for (var i=0; i < mycells.length; i++)
			{
				//unselected tab text
				if (cellIndex % 2 == 0)
				{
					mycells[i].className = "SubMenuTab";
				}
				//unselected tab image <TD>
				else if (cellIndex % 2 == 1)
				{
					mycells[i].className = "SubMenuInactiveImage";
				}
				cellIndex++;
			}
		}
	}

	var myLinks = doc.getElementsByName("lnkTabs");
	for (var i=0; i < myLinks.length; i++)
	{
		myLinks[i].className = "SubMenuLinkDisable";
	}
}

/**
 * this method set the height of the iframes dynamically.Used only for NS support
 * @author Vishal
 */
function setFrameHeight(doc, frameID, frmHtReduceBy)
{
	if (navigator.appName == "Netscape")
	{
		var temp;
		temp = doc.getElementById(frameID);
		if (temp != null)
		{
			var frameHeight = parent.window.innerHeight - frmHtReduceBy;			
			
			if (frameHeight > 260)
			{
				temp.height = frameHeight;
			}
			else
			{
				temp.height = 260;
			}
			var setFrameWidthForBrowser = doc.getElementById("ItemsDropDownMenuFrame");
			if (setFrameWidthForBrowser != null)
			{
				setFrameWidthForBrowser.width = 0;
			}
		}
	}
}


/**
 * this method set the height of the iframes in the manager pages dynamically.Used only for NS support
 * @author Vishal
 */
function setMgrFrameHeight(doc)
{
	if (navigator.appName == "Netscape")
	{
		var temp = new Array();
		temp = doc.getElementsByTagName("iframe");
		var temp1;
		for(var i = 0; i < temp.length; i++)
		{
			temp1 = temp[i];
			if (temp[i].name == "sideBarMenu" || temp[i].name == "TFrame")
			{
				temp1.height = window.innerHeight - 40;
			}
		}
	}
}

/* called on mouseover, and onmouseout of all tab links. */
function hoverLink(linkObj, hoverTabName, currentTab, makeActive, isTabDisabled, isTabLoaded)
{
	// if the tab is disabled, dont create any highlighting/unhighlighting effects.
	if (isTabDisabled || isTabLoaded)
	{
		linkObj.style.cursor = "";
		return;
	}
	if (makeActive)
	{
		linkObj.className = "SubMenuLinkHover";
		changeCursor(linkObj);
	}
	else
	{
		if (hoverTabName != currentTab)
		{
			linkObj.className = "SubMenuLink";
		}
	}
}

function changeCursor(obj)
{
	var sCursorName = "hand";
	if (navigator.appName == "Netscape")
	{
		sCursorName = "pointer";
	}
	obj.style.cursor = sCursorName;
}

//reset the value of a Text box or a combo box
function resetText(txtBox)
{
	txtBox.value = " ";
}

/**
 * Always keeps the date text field enabled (since Calendar provided is optional.)
 * Only changes the background color.
 */
function enableDateText(cmp, bEnable)
{
	//cmp.disabled = false;
	if (bEnable)
	{
		cmp.disabled = false;
		cmp.style.background = "white";
	}
	else
	{
		cmp.disabled = true;
		cmp.style.background = "threedface";
	}
}

/**
 * Opens a window. If window is already open, brings it into focus.
 * @param winVar the variable which u are using to keep track of the window.
 *		You can use it to close the window.
 * @param name Optional. A name for the window.
 * @return winVar.
 */
function openWindow(winVar, url, width, height, name, scrollbars)
{
	if(scrollbars == null)
	{
		scrollbars = "NO";
	}
	var tmpName = "";
	if (name)
	{
		tmpName = name;
	}
	if (winVar != null && !winVar.closed)
	{
		winVar.focus();
		return winVar;
	}

	winVar = window.open(url, tmpName, 'width='+width+',height='+height+',left=100,top=100,toolbar=no,locationbar=no,status=no,menubar=no,resizable=yes,scrollbars=' + scrollbars);
	winVar.focus();
	return winVar;
}

/**
 * Closes a window, if the window is not already closed.
 *
 * @param		winVar		The window which is to be closed
 */
function closeWindow(winVar)
{
	if 	(winVar != null && !winVar.closed)
	{
		winVar.close();
	}
}

function onEscapeKeyCloseWindow(win)
{
	if (win.event.keyCode == 27)
	{
		win.close();
	}
}

/**
 * closes the window when the escape key is hit
 */
function onEscapeCloseWin(event)
{
	if (!event)
	{
		if (window.event.keyCode == 27)
		{
			window.close();
		}
	}
	else
	{
		event.cancelBubble=true;
		if (event.keyCode == 27)
		{
			window.close();
		}
	}
}

function isNetscape()
{
	return (navigator.appName == "Netscape");
}

function hasBackwardSlash(str)
{
	if (str.length != 0 && str.indexOf("\\") != -1)
	{
		return 1;
	}
	return 0;
}

/**
 * REMOVES ALL OPTIONS IN THE <SELECT> BOX.
 * @param objList the html select object
 * @param fromIndex the index (starting with 0) from which u
 *		you want to remove all <option>s. This is an optional parameter.
 */
function clearOptions(objList, fromIndex)
{
	var srcListSize = objList.options.length;
	var iFromIndex = 0;
	if (fromIndex)
	{
		iFromIndex = fromIndex;
	}
	for (var i = iFromIndex; i<srcListSize; i++)
	{
		objList.options[i] = null;
		srcListSize = objList.options.length;
		i--;
	}
}

/**
 * changes the innerHTML of any HTML element
 * @param doc the document in which the HTML element is present
 * @param id the id of the HTML element
 * @param newContent the new innerHTML
 */
function changeInnerHTML(doc, id, newContent)
{
	var spanObj = doc.getElementById(id);
	spanObj.innerHTML = newContent;
}

/**
 * toggles the disabled and enabled state of a checkbox
 * @param cbObj the checkbox object
 * @param enableFlag whether the checkbox is to be enabled
 * @param checkedFlag whether the checkbox is to be checked
 */
function toggleCheckbox(cbObj, enableFlag, checkedFlag)
{
	cbObj.disabled = !enableFlag;
	cbObj.checked = checkedFlag;
}


// POPUP MENU ISSUE

/**
 *	 This variable is used to indicate that the itempopupmenuparent.js is completely loaded
 */
var isPopupJsLoaded = false;

/**
 *	 This variable is used to indicate that the itempopupmenu.jsp is completely loaded
 */
var isPopupJspLoaded = false;

/**
 * handles the onmouseover event on the popup menu
 */
 function activatePopup(PopupZone)
 {
	if (isPopupJsLoaded)
	{		
		enterItemPopZone(PopupZone);
	}
 }

/**
 * handles the onclick event on the popup menu
 */
 function activatePopupImmediately()
 {
	if (isPopupJsLoaded)
	{
		showItemPopUpMenuImmediately();
	}
 }

/**
 * handles the onmouseout event on the popup menu
 */
 function deActivatePopup(PopupZone)
 {
	if (isPopupJsLoaded)
	{
		leaveItemPopZone(PopupZone);
	}
 }

 /**
  * This strips the file name from path
  */
 function stripFileFromPath(sFilePath)
 {
 	if (sFilePath == null) return null;
 	var iLastDot = sFilePath.lastIndexOf(".");
 	if (iLastDot == -1) return sFilePath;
 	iLastDot = sFilePath.lastIndexOf("/");
 	if (iLastDot == -1) return sFilePath;
 	return sFilePath.substring(0, iLastDot);
}

/**
 *
 */
function stripFileExtension(sFilename)
{
	if (sFilename == null) return null;
	var iLastDot = sFilename.lastIndexOf(".");
	if (iLastDot == -1) return sFilename;
	return sFilename.substring(0, iLastDot);
}

/**
 *
 */
function stripFolderName(sFilename, sSlashChar)
{
	if (sFilename == null) return null;
	var iLastSlash = sFilename.lastIndexOf(sSlashChar);
	if (iLastSlash == -1) return sFilename;
	return sFilename.substring(iLastSlash + 1);
}

/**
 *
 */
function stripVersionNumber(sFQFilename)
{
	if (sFQFilename == null) return null;
	var iLastSC = sFQFilename.lastIndexOf(";");
	if (iLastSC == -1) return sFQFilename;
	return sFQFilename.substring(0, iLastSC);
}

/**
 * removes the element at the specified index and returns a new array
 * @param arr a javascript array.
 * @param removeIndex the index from which the element is to be removed.
 */
function removeElementFromArray(arr, removeIndex)
{
	//if array is null or length is 0, return as it is.
	if (arr == null || arr.length == 0)
	{
		return arr;
	}
	//if first element is to be removed...
	if (removeIndex == 0)
	{
		return (arr.slice(1));
	}
	//if last element is to be removed...
	else if (removeIndex == arr.length - 1)
	{
		return (arr.slice(0, arr.length - 1));
	}
	//if middle element is to be removed...
	else
	{
		//take left part of array...
		var tmpArr = arr.slice(0, removeIndex);
		//now take each of the right elements, and append to left array...
		for (var i = removeIndex+1; i < arr.length; i++)
		{
			tmpArr[tmpArr.length] = arr[i];
		}
		return tmpArr;
	}
}

/**
 * makes an element visible/hidden.
 */
function changeVisibility(doc, id, makeVisible)
{
	var item = doc.getElementById(id);
	if (item == null) return;
	item.style.visibility = makeVisible ? "visible" : "hidden";
}

/**
 * enables/disables any button/submit/reset type input within the element
 */
function enableChildButtons(doc, id, enableFlag)
{
	var tblItem = doc.getElementById(id);
	if (tblItem == null) return;
	var nodes = tblItem.getElementsByTagName("INPUT");

	for (var i=0; i < nodes.length; i++)
	{
		if ((nodes[i].type == "submit" || nodes[i].type == "reset" || nodes[i].type == "button") || (nodes[i].type == "SUBMIT" || nodes[i].type == "RESET" || nodes[i].type == "BUTTON"))
		{
			nodes[i].disabled = !enableFlag;
		}
	}
}


/**
 * clears the selection in select box.
 * Provided for netscape 6.0.1 support
 */
function clearComboBox(selectComponent)
{
	for(var i=0;i<selectComponent.size;i++)
	{
		selectComponent.options[i].selected = false;
	}
	selectComponent.selectedIndex = 0;
}

/**
 * Brings the first focusable form element into focus.
 */
function focusElement(frmObj)
{
	if (frmObj == null) return;

	var frmElements = frmObj.elements;
	for (var i=0; i<frmElements.length; i++)
	{
		if (!frmElements[i].disabled && frmElements[i].type != "hidden")
		{
			frmElements[i].focus();
			return;
		}
	}
}

 /////////////////// BACK BUTTON ISSUE /////////////////////////

/**
 * if the hidden field is present, sets the value,
 * else creates the field and sets the value.
 * @param frmName name of the form. This form should be present in the scope of the
 *		document where this js file is included.
 * @param fldName the hidden field object
 * @param value value for the hidden field
 */
function setOrCreateHiddenField(frmName, fldName, value)
{
	var fldObject = eval("document."+frmName+"."+fldName);
	var frmObject = eval("document."+frmName);

	if (fldObject == null)
	{
		fldObject = document.createElement("input");
		fldObject.setAttribute("type", "hidden");
		fldObject.setAttribute("name", fldName);
		frmObject.appendChild(fldObject);
	}

	fldObject.setAttribute("value", value);
}

/**
 * If the hidden field is present, sets the value,
 * This is to avoid any js errors.
 * @param frmName name of the form. This form should be present in the scope of the
 *		document where this js file is included.
 * @param fldName name of the hidden field
 * @param value value for the hidden field
 */
function setHiddenField(frmName, fldName, value)
{
	var fldObject = eval("document."+frmName+"."+fldName);

	if (fldObject != null)
	{
		fldObject.setAttribute("value", value);
	}
}

/**
 * Returns the value for the specified field name. This method first checks the
 * form submitted for the field. If the field does not exist in this form, it searches
 * the form which stores all original values.
 * @param newFrmName name of the form which is submitted.
 * @param oldFrmName name of the form which stores the original values.
 * @param fldName name of the hidden field
 *
 * @return value value for the found hidden field
 */
function getNewOrOldFieldValue(newFrmName, oldFrmName, fldName)
{
	var newFldObject = eval("document."+newFrmName+"."+fldName);

	if (newFldObject != null)
	{
		return newFldObject.value;
	}

	var oldFldObject = eval("document."+oldFrmName+"."+fldName);
	return oldFldObject.value;
}

/**
 * Checks if the field value in both forms is the same, If it is the same,
 * and if the last parameter is provided or true, then removes the field
 * from the new form i.e. the form which will be submitted..
 * @param newFrmName name of the form which is submitted.
 * @param oldFrmName name of the form which stores the original values.
 * @param fldName name of the hidden field
 * @param bDontRemove if true, then will not remove the field.
 * @param isInfoDirty the isDirty variable maintained by individual js files.
 *
 * @return false if the field was removed (If field was removed, it means that info was not dirty).
 */
function removeHiddenFieldIfSame(newFrmName, oldFrmName, fldName, bDontRemove, isInfoDirty)
{
	if (bDontRemove) return true;

	var newFrmObject = eval("document."+newFrmName);
	var newFldObject = eval("document."+newFrmName+"."+ fldName);
	var oldFldObject = eval("document."+oldFrmName+"."+ fldName);

	if (newFldObject.value == oldFldObject.value)
	{
		newFrmObject.removeChild(newFldObject);
		return isInfoDirty;
	}

	return true;
}

/**
 * Trims the text/hidden field's value and returns it.
 * NOTE : THIS METHOD HAS DEPENDENCY ON validate.js.
 * ENSURE THAT "validate.js" IS INCLUDED AT THE SAME LEVEL AS THIS JS.
 */
function getTrimmedFieldValue(fldObject)
{
	fldObject = eval(fldObject);

	var fldValue = trim(fldObject.value);
	fldObject.value = fldValue;
	return fldValue;
}

/**
 * returns true/false depending whether the hidden field's value is true or false
 */
function getBooleanFieldValue(fldObject)
{
	fldObject = eval(fldObject);

	if (fldObject)// ras
	{
		return (fldObject.value == "true");
	}
	else
	{
		return false;
	}
}

/**
 * returns the innerHTML of an HTML element.
 * DEPENDENCY NOTE : this method uses a method in encoder.js
 */
function getHTMLInnerText(objHTMLElement)
{
	if (!isNetscape())
	{
		return objHTMLElement.innerText;
	}
	else
	{
		return decodeInnerHTML(objHTMLElement.innerHTML);
	}
}

/**
 * Can be used to find if the hidden field data information is dirty.
 * @param newFrmName name of the form which is submitted.
 * @param oldFrmName name of the form which stores the original values.
 * @param fldName name of the hidden field
 * @param isInfoDirty the isDirty variable maintained by individual js files.
 */
function isFieldDataDirty(newFrmName, oldFrmName, fldName, isInfoDirty)
{
	var newFldObject = eval("document."+newFrmName+"."+ fldName);
	var oldFldObject = eval("document."+oldFrmName+"."+ fldName);

	if (newFldObject.value != oldFldObject.value)
	{
		return true;
	}

	return isInfoDirty;
}

/**
 * Removes the field if its value is blank
 *
 * @param newFrmName name of the form which is submitted.
 * @param fldName name of the hidden field
 * @param isInfoDirty the isDirty variable maintained by individual js files.
 *
 * @return false if the field was removed (If field was removed, it means that info was not dirty).
 */
function removeFieldIfBlank(newFrmName, fldName, isInfoDirty)
{
	var newFrmObject = eval("document."+newFrmName);
	var newFldObject = eval("document."+newFrmName+"."+ fldName);

	if (newFldObject.value == "")
	{
		newFrmObject.removeChild(newFldObject);
		return isInfoDirty;
	}

	return true;
}

/**
 * Removes the field if its value is the same as specified in the argument.
 *
 * @param newFrmName name of the form which is submitted.
 * @param fldName name of the hidden field
 * @param defaultValue value to be compared with.
 * @param isInfoDirty the isDirty variable maintained by individual js files.
 *
 * @return false if the field was removed (If field was removed, it means that info was not dirty).
 */
function removeFieldWithDefaultValue(newFrmName, fldName, defaultValue, isInfoDirty)
{
	var newFrmObject = eval("document."+newFrmName);
	var newFldObject = eval("document."+newFrmName+"."+ fldName);
	defaultValue = "" + defaultValue;//to convert to string

	if (newFldObject.value == defaultValue)
	{
		newFrmObject.removeChild(newFldObject);
		return isInfoDirty;
	}

	return true;
}

/**
 * adds a field to 'frmSubmit' in case the fld's value is not the same as 'sDefaultValue'.
 * The new field has the same name as the fld object.
 */
function createFieldIfNotDefault(frmSubmit, fld, sDefaultValue, isInfoDirty)
{
	if (fld.value != sDefaultValue)
	{
		createHiddenFormField(frmSubmit, fld.name, fld.value);
		return true;
	}
	return isInfoDirty;
}

/**
 * adds a field to 'frmSubmit' in case the fld's value is not the same as old fld's value.
 * The new field has the same name as the newFld object.
 */
function createFieldIfDirty(newFld, orgFld, isInfoDirty, bCreateAlways)
{
	var frmSubmit = document.frmSubmit;
	if (newFld.value != orgFld.value || bCreateAlways)
	{
		createHiddenFormField(frmSubmit, newFld.name, newFld.value);
		return true;
	}
	return isInfoDirty;
}

/**
 * adds a field to 'frmSubmit' in case the fld exists.
 * The new field has the same name as the fld object.
 */
function createFieldIfExists(frmSubmit, fld)
{
	if (fld != null)
	{
		createHiddenFormField(frmSubmit, fld.name, fld.value);
	}
}

/**
 * Removes the field/array of fields with the specified name.
 * @param frmName The name of the form from which fields are to be removed.
 * @param name The name of the field/array of fields
 */
function removeFieldsWithName(frmName, name)
{
	var frmObject = eval("document."+frmName);
	var fldObject = eval("document."+frmName+"."+name);
	if (fldObject == null) return;

	if (fldObject.length)
	{
		for (var i=0; i < fldObject.length; i++)
		{
			frmObject.removeChild(fldObject[i]);
			i--;
		}
	}
	else
	{
		frmObject.removeChild(fldObject);
	}
}

/**
 * Removes all fields with the specified prefix/es.
 * @param frmName The name of the form from which fields are to be removed.
 * @param arrStartWith An array holding all the prefixes
 */
function removeFieldsStartingWith(frmName, arrStartWith)
{
	var frmObject = eval("document."+frmName);
	for (var i=0; i<frmObject.elements.length; i++)
	{
		for (var j=0; j < arrStartWith.length; j++)
		{
			if (frmObject.elements[i].name.substring(0, arrStartWith[j].length) == arrStartWith[j])
			{
				frmObject.removeChild(frmObject.elements[i]);
				i--;
			}
		}
	}
}

/**
 * Removes all fields except the names contained in the array.
 * @param frmName The name of the form from which fields are to be removed.
 * @param arrExceptNames An array holding all the exempt names.
 */
function removeAllFieldsExcept(frmName, arrExceptNames)
{
	var frmObject = eval("document."+frmName);
	for (var i=0; i < frmObject.elements.length; i++)
	{
		var bRemove = true;
		for (var j=0; j < arrExceptNames.length; j++)
		{
			if (frmObject.elements[i].name == arrExceptNames[j])
			{
				bRemove = false;
			}
		}
		if (bRemove)
		{
			frmObject.removeChild(frmObject.elements[i]);
			i--;
		}
	}
}

/**
 * Returns the value for the specified field name.
 * @param newFrmName name of the form which is submitted.
 * @param fldName name of the hidden field
 * @return value for the found hidden field
 */
function getFieldValue(newFrmName, fldName)
{
	var newFldObject = eval("document."+newFrmName+"."+fldName);
	return newFldObject.value;
}

/**
 * returns integer value of hidden field's value
 */
function getIntegerFieldValue(fldObject)
{
	return parseInt(fldObject.value);
}

/**
 * Stores a js array as pipe separated value in the form field.
 * Eg: "elem1|elem2|elem3|elem4".
 */
function storeArrayAsPSV(arr, fld)
{
	var sFldValue = "";
	for (var i=0; i<arr.length; i++)
	{
		sFldValue += encodeCookieSeparators(arr[i]);
		if (i+1 < arr.length)
		{
			sFldValue += "|";
		}
	}
	fld.value = sFldValue;
}

/**
 * Returns the index for "iOccur"th occurrence of "ch" in the "str" (if any)
 * @param iOccur starts with 1.
 * Example : indexOfOccurrence("AXXXAXX", "A", 2) will return 4
 */
function indexOfOccurrence(str, ch, iOccur)
{
	var index = 0;
	str = ""+str;

	for (var i=0; i<iOccur; i++)
	{
		index = str.indexOf(ch, ((i == 0)?0:index+1));

		if (index == -1) return index;
	}

	return index;
}

/**
 * Replaces substring of the string with the specified value.
 * @param str the string which needs to be modified
 * @param from If -1, then it will be considered as start of str.
 * @param to If -1, then it will be considered as end of str.
 */
function replaceSubstring(str, from, to, replaceStr)
{
	str = ""+str;

	if (from == -1) from = 0;
	if (to == -1) to = str.length;

	var returnStr = str.substring(0, from);
	returnStr += replaceStr;
	returnStr += str.substring(to);
	return returnStr;
}

/**
 * This function gets the file extension from the file name
 */
function getFileExtension(sFilename)
{
	if (sFilename == null) return null;
	var iLastDot = sFilename.lastIndexOf(".");
	if (iLastDot == -1 || iLastDot == sFilename.length) return sFilename;
	return sFilename.substring(iLastDot + 1);
}

function FormHandler(_formObject)
{
	this.formObject = _formObject;
	this.setTarget = setTargetArea;
	this.setAction = setActionPage;
	this.addHiddenType = addHiddenField;
}

function setTargetArea(_targetArea)
{
	this.formObject.target = _targetArea;
}

function setActionPage(_actionPage)
{
	this.formObject.action = _actionPage;
}

function addHiddenField(_fieldName, _value)
{
	createHiddenFormField(this.formObject, _fieldName, _value);
}


function removeField(_formObject, _name)
{
	frmObject = _formObject;
	fldObject = frmObject.elements[_name];//eval(frmObject + "." +_name);
	if (fldObject == null) return;

	if (fldObject.length)
	{
		for (var i=0; i < fldObject.length; i++)
		{
			frmObject.removeChild(fldObject[i]);
			i--;
		}
	}
	else
	{
		frmObject.removeChild(fldObject);
	}
}

function removeAllFields(_formObject)
{
	frmObject = _formObject;
	fldObjects = frmObject.elements;
	if (fldObjects == null) return;

	if (fldObjects.length)
	{
		for (var i=0; i < fldObjects.length; i++)
		{
			frmObject.removeChild(fldObjects[i]);
			i--;
		}
	}
}
