	function SelectTreeNode(tree, obj,  nodetxtid, selectedState, selectedStatePrerender, expandedStateIndex , IdSelectedNode, IdSelectedNodeValue, SelectedNodeValue  )
	{
	
	    // Get the previously selected node
	    //alert( "PrevSelectedNode " + IdSelectedNode );
	    //alert( "PrevSelectedNode " + IdSelectedNodeValue ); 
	    
	    var PrevSelectedNodeId  = document.getElementById( IdSelectedNode );
	    	    
	    if( PrevSelectedNodeId != null && PrevSelectedNodeId.value != "")
	    {
		   //alert( "PrevSelectedNodeId.value" + PrevSelectedNodeId.value );
	       var PrevSelectedNode = document.getElementById( PrevSelectedNodeId.value );
	       PrevSelectedNode.style.backgroundColor = "white";
	       PrevSelectedNode.style.color = "black";
	    }
	    else 
	    {
			//alert( "null " );
	    }
	
	    // Select the Current Node
		//obj.style.backgroundColor = "blue";
		obj.style.backgroundColor = "#0A246A";
		obj.style.color = "white";
		//obj.style.Class  = "treeControlSelectedItem";
		
		// Store the Value of the Selected Node
		var SelectedNodeValue = document.getElementById( IdSelectedNodeValue );
		if ( SelectedNodeValue != null )
		{
		  SelectedNodeValue.value = SelectedNodeValue;
		  //alert(  SelectedNodeValue.value );
		}
		else
		{
			//alert( "null " );
		}
		
		if ( PrevSelectedNodeId != null )
		{
			PrevSelectedNodeId.value = nodetxtid;
		}
		
		var selectedStateString ;
		
		if ( selectedStatePrerender != null && selectedStatePrerender.value.length > 0 )
		{
			selectedStateString = selectedStatePrerender.value; 
		}
		else
		{
			selectedStateString = selectedState.value; 
		}
		
		// UnSelect All Other Nodes
		//if ( selectedState != null && selectedState.value.length > 0 )
		//{
			for ( var i=0; i < selectedStateString.length; i++ )
			{
			      if (selectedStateString.charAt(i) == 's' )
			      {
					//selectedState.value[i] = 'u';
					
					selectedStateString = selectedStateString.substring(0, i) + 'u' + selectedStateString.slice(i + 1);
			      }
			}
		//}
		
		// Set expandedState hidden field to reflect change
        //index = parseInt(obj.expandedStateIndex);
        selectedState.value =  selectedStateString.substring(0, expandedStateIndex) + 's' + selectedStateString.slice(expandedStateIndex + 1);
        //alert( "selectedState.value  " + selectedState.value );
        selectedStatePrerender.value =  selectedState.value;
        
        //alert( selectedState.value );
        //alert( selectedStatePrerender.value );

}

