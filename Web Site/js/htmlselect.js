// functions for <SELECT> <OPTION> 

// move selected options
function moveSelectedOptions(sourceId, targetId, doSort) 
{
    var source = document.forms[0].elements[sourceId];
    var target = document.forms[0].elements[targetId];
    
    var length = source.length;
    var numSelected  = 0;
   	var newOptions = new Array( length );
   	
   	// Copy the original options
   	
   	for ( i=0; i<length; i++ )
   	{
   	     newOptions[i] = source.options[i];
   	}
   	
   	// save the current selected index in target
   	
    var currentTargetIndex = target.selectedIndex;
    
    
    var movedOne = false;
    var lastSourceIndexMoved = -1;
    
    for ( i=0; i < length ;i++)
	{
		if (source.options[i].selected )
		{
			if ( source.options[i].value > "")
			{
				// If we move anything, deselect the current selection in target
				
				if ( movedOne == false )
				{
					target.selectedIndex = -1; 
					movedOne = true;
				}
				
				// remember the last one we moved
				
				lastSourceIndexMoved = i;
				
				var j  = target.length;
				if ( doSort == true )
				{
					j = findCurrentIndex( source.options[i].value, target );
					if ( j == -1 )
					{
						j = target.length;
					}
				}
				
				oText = source.options[i].text;
				oValue = source.options[i].value;
				if ( j != target.length )
				{
					var itargetLength = target.length ;
					target.options[ itargetLength ] = new Option( );
					
					// move everything down
					for ( var k = itargetLength; k > j ; k-- )
					{
						
						target.options[k].text = target.options[k-1].text;
						target.options[k].value = target.options[k-1].value;
						target.options[k].selected = target.options[k-1].selected;
					}
					
				}
				target.options[j] = new Option (oText, oValue, false, true);
				newOptions[i] = null;
				
			}
		}	
	}
	
	
	
	// Clear the Old Options
	for ( i=0; i < length ;i++)
	{
		source.options[0] = null;
	}
		
	
	var j=0;
	for (i=0 ; i < length; i++ )
	{
		if ( newOptions[i] != null   )
	  	{
	  		source.options[j] = newOptions[i];
	  		
	  		// select the next one, if the previous was the last moved
	  		if ( lastSourceIndexMoved == i - 1)
	  		{
	  			source.options[j].selected = true;
	  		}
			 j  = j + 1;
		}
	}
	
	// If the last moved was also the last, then select the last again
	
	if ( lastSourceIndexMoved == length - 1 && length > 0 && source.options.length > 0)
	{
		source.options[ source.options.length - 1 ].selected = true;
	}
	
		
}  

function getOriginalPosition( sValue )
{
	var iLastindex = sValue.lastIndexOf( "_" );
	var sPosition = sValue.substr( iLastindex + 1 );
	if ( sPosition == "" )
	{
		return -1;
	}
	return parseInt( sPosition );
}

function findCurrentIndex( sValue, available )
{
	var iPosition = getOriginalPosition( sValue );
	if ( iPosition == -1 )
	{
		return -1;
	}
	
	for ( var i = 0; i < available.options.length ; i++ )
	{
		if ( iPosition <= getOriginalPosition( available.options[i].value ) )
		{
			return i;
		}
	}
	return available.options.length;
}

// move all options
function selectOptionMoveAll(sourceId,targetId,doSort) 
{
  var source  = document.forms[0].elements[sourceId];
  var target = document.forms[0].elements[targetId];
  
  // Select all and move
  l = source.length;
  for (i=0;i<l;i++)
  {
	source.options[i].selected = true;
  }
	
  moveSelectedOptions(sourceId, targetId,doSort);
  	
  // Select the first one
  if ( target.length > 0 )
  {
	target.selectedIndex = 0;
  }
}

/* down = 0 (up) or 1 (down) */
function orderOption(down, selectControlId) 
{
	
	
  var selectControl = document.forms[0].elements[selectControlId];
  sl = selectControl.selectedIndex;
  if (sl != -1 && selectControl.options[sl].value > "") {
    oText = selectControl.options[sl].text;
    oValue = selectControl.options[sl].value;
    if (selectControl.options[sl].value > "" && sl > 0 && down == 0) {
      selectControl.options[sl].text = selectControl.options[sl-1].text;
      selectControl.options[sl].value = selectControl.options[sl-1].value;
      selectControl.options[sl-1].text = oText;
      selectControl.options[sl-1].value = oValue;
      selectControl.selectedIndex--;
    } else if (sl < selectControl.length-1 && selectControl.options[sl+1].value > "" && down == 1) {
      selectControl.options[sl].text = selectControl.options[sl+1].text;
      selectControl.options[sl].value = selectControl.options[sl+1].value;
      selectControl.options[sl+1].text = oText;
      selectControl.options[sl+1].value = oValue;
      selectControl.selectedIndex++;
    }
  }
}

// convert option list to comma separated list
function makeList(selectControl) 
{
  val = "";
  for (j=0; j<selectControl.length; j++) {
    if (val > "") { val += ","; }
    if (selectControl.options[j].value > "") 
	val += selectControl.options[j].value;
  } 
  return val;
}

function dummy()
{
}

