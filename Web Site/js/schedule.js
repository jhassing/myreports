/**
 * Schedule.js
 *
 * @author  Actuate Corporation
 * @version 1.0
 * $Revision::	$
 */

///////////////////////////////////////////////////////////////////////////////
// 
///////////////////////////////////////////////////////////////////////////////


function dummy()
{
}

function newUserDate( Offset )
{

	var date = new Date();
	date.setTime(  date.getTime() + ( Offset + date.getTimezoneOffset() ) * 60 * 1000  ) ;
	return date;
}

function ScheduleNowRdBtnClicked(   OnceDate,
									OnceTime, 
									WeekDaysList,
									RecurringTimeTxt,
									RecurringStartChkBox,
									StartDateTxt,
									useEndDateTime,
									endDateTxt ,
									ScheduleTypeFieldId,
									OnceDateLink,
									StartDateLink,
									EndDateLink)
{

	document.forms[0].elements[OnceDate].disabled = true;
	document.forms[0].elements[OnceTime].disabled  = true;
	document.forms[0].elements[WeekDaysList].disabled  = true;
	document.forms[0].elements[RecurringTimeTxt].disabled  = true;
	document.forms[0].elements[RecurringTimeTxt].value  = "";
	document.forms[0].elements[RecurringStartChkBox].disabled  = true;
	document.forms[0].elements[StartDateTxt].disabled  = true;
	document.forms[0].elements[StartDateTxt].value  = "";
	document.forms[0].elements[useEndDateTime].disabled  = true;
	document.forms[0].elements[endDateTxt].disabled  = true;
	document.forms[0].elements[endDateTxt].value  = "";
	
	var OnceDateHyperLink  = GetElt( OnceDateLink );
	var StartDateHyperLink = GetElt( StartDateLink );
	var EndDateHyperLink = GetElt( EndDateLink );
	
	
	OnceDateHyperLink.disabled = true;
	StartDateHyperLink.disabled = true;
	EndDateHyperLink.disabled = true;
	
	document.forms[0].elements[ScheduleTypeFieldId].value = "immediate";
}
									



function ScheduleOnceRdBtnClicked(   OnceDate,
								   OnceTime, 
								   dtFormat,
								   tmFormat ,
								   SchedReccuringRdBtn, 
								   WeekDaysList,
								   RecurringTimeTxt,
								   StartingChkBox,
								   StartDateTxt,
								   useEndDateTime,
								   endDateTxt,
								   ScheduleTypeFieldId ,
								   loaclizedAMString,
								   localizedPMString,
								   OnceDateLink,
								   StartDateLink,
								   EndDateLink, 
								   TimeOffset
								  )
{

	var ns4 = (document.layers)? true:false
	var dtOnce = newUserDate( TimeOffset )
	dtOnce.setTime(dtOnce.getTime() + 600000);
	tmOnce = dtOnce;
	
	LOCALIZED_AM = loaclizedAMString;
	LOCALIZED_PM = localizedPMString;
	
	OnceDateText = document.forms[0].elements[OnceDate];
	OnceTimeText = document.forms[0].elements[OnceTime];
	
	OnceDateText.disabled = false;
	OnceTimeText.disabled  = false;

	OnceDateText.value =  formatDate(dtOnce, dtFormat);
	OnceTimeText.value = formatDate(tmOnce, tmFormat);

	
	//Disbale the Other elements
	//document.forms[0].elements[SchedReccuringRdBtn].bgColor = "#d6d3ce";
	document.forms[0].elements[WeekDaysList].selectIndex = 0;
	document.forms[0].elements[WeekDaysList].disabled = true;
	document.forms[0].elements[WeekDaysList].selected = true;
	

	//Recurring Time
	document.forms[0].elements[RecurringTimeTxt].disabled   = true;
	document.forms[0].elements[RecurringTimeTxt].value  = "";
	
	
	// Starting check box
	document.forms[0].elements[StartingChkBox].disabled   = true;
	document.forms[0].elements[StartingChkBox].checked   = false;
	//document.forms[0].elements[StartingChkBox].style.backgroundColor = "d6d3ce";
	
	// Starting Date text
	document.forms[0].elements[StartDateTxt].disabled = true;
	document.forms[0].elements[StartDateTxt].value = "";
	
	
	// Until Check box
	document.forms[0].elements[useEndDateTime].disabled = true;
	document.forms[0].elements[useEndDateTime].checked  = false;
	
	
	// Until Date text
	
	document.forms[0].elements[endDateTxt].disabled = true;
	document.forms[0].elements[endDateTxt].value = "";
	
	var OnceDateHyperLink  = GetElt( OnceDateLink );
	var StartDateHyperLink = GetElt( StartDateLink );
	var EndDateHyperLink = GetElt( EndDateLink );
	
	
	OnceDateHyperLink.disabled = false;
	StartDateHyperLink.disabled = true;
	EndDateHyperLink.disabled = true;
	
	document.forms[0].elements[ScheduleTypeFieldId].value = "once";
	
}


function ScheduleRecurringRdBtnClicked( WeekDaysList,
								   RecurringTimeTxt, 
								   tmFormat ,
								   OnceDateTxt,
								   OnceTimeTxt,
								   StartingChkBox,
								   StartDateTxt,
								   useEndDateTime,
								    endDateTxt  ,
								    ScheduleTypeFieldId ,
								    loaclizedAMString,
								    localizedPMString ,
   								   OnceDateLink,
								   StartDateLink,
								   EndDateLink,
								   TimeOffset)

								    
{

	dtOnce = newUserDate( TimeOffset )
	dtOnce.setTime(dtOnce.getTime() + 600000 );
	tmOnce = dtOnce;
	LOCALIZED_AM = loaclizedAMString;
	LOCALIZED_PM = localizedPMString;
	
	
	document.forms[0].elements[WeekDaysList].disabled = false;
	document.forms[0].elements[WeekDaysList].selctindex = 0;
	
	
	RecurringTimeText = document.forms[0].elements[RecurringTimeTxt];
	RecurringTimeText.disabled = false;
	RecurringTimeText.value = formatDate(tmOnce, tmFormat);
		
	
	//Once Date
	document.forms[0].elements[OnceTimeTxt].disabled   = true;
	document.forms[0].elements[OnceTimeTxt].value  = "";
	
	// Once time
	document.forms[0].elements[OnceDateTxt].disabled   = true;
	document.forms[0].elements[OnceDateTxt].value  = "";
	
	// Starting check box
	
	document.forms[0].elements[StartingChkBox].disabled   = false;
	document.forms[0].elements[StartingChkBox].checked   = false;

	// Until Check box
	document.forms[0].elements[useEndDateTime].disabled = false;
	document.forms[0].elements[useEndDateTime].checked = false;
	
	var OnceDateHyperLink  = GetElt( OnceDateLink );
	var StartDateHyperLink = GetElt( StartDateLink );
	var EndDateHyperLink = GetElt( EndDateLink );
	
	
	OnceDateHyperLink.disabled = true;
	StartDateHyperLink.disabled = true;
	EndDateHyperLink.disabled = true;

	document.forms[0].elements[ScheduleTypeFieldId].value = "Recurring";
	
}

function RecurringStartClicked( RecurringStartDateTxt, 
							    DateFormat,
							    RecurringStartChkBox,
							    OnceDateLink,
								StartDateLink,
								EndDateLink,
								TimeOffset )
{
	dtOnce = newUserDate( TimeOffset );
	var chkBox = document.forms[0].elements[RecurringStartChkBox];
	
	RecurringStartDateTextBox = document.forms[0].elements[RecurringStartDateTxt];
	
	RecurringStartDateTextBox.disabled = !RecurringStartDateTextBox.disabled ;
	if ( chkBox.checked )
	{
		RecurringStartDateTextBox.value =  formatDate(dtOnce, DateFormat);
	}
	else
	{  
		RecurringStartDateTextBox.value =  "";
	}
	
	
	var OnceDateHyperLink  = GetElt( OnceDateLink );
	var StartDateHyperLink = GetElt( StartDateLink );
	var EndDateHyperLink = GetElt( EndDateLink );
	
	OnceDateHyperLink.disabled = true;
	
	if ( chkBox.checked )
		StartDateHyperLink.disabled = false;
	else
		StartDateHyperLink.disabled = true;
		

}

function RecurringEndClicked( RecurringEndDateTxt,
						      DateFormat,
						      RecurringEndChkBox,
							  OnceDateLink,
							  StartDateLink,
							  EndDateLink,
							  TimeOffset)
{
	dtOnce = newUserDate( TimeOffset )
	dtOnce.setDate(dtOnce.getDate() + 1);
	var chkBox = document.forms[0].elements[RecurringEndChkBox];
	RecurringEndDateText = document.forms[0].elements[RecurringEndDateTxt];
	RecurringEndDateText.disabled = !RecurringEndDateText.disabled;
	
	if ( chkBox.checked )
	{
		RecurringEndDateText.value =  formatDate(dtOnce, DateFormat);
	}
	else
	{
		RecurringEndDateText.value =  "";
	}
	
	
	var OnceDateHyperLink  = GetElt( OnceDateLink );
	var StartDateHyperLink = GetElt( StartDateLink );
	var EndDateHyperLink = GetElt( EndDateLink );
	
	
	
	OnceDateHyperLink.disabled = true;
	
	if ( chkBox.checked )
		EndDateHyperLink.disabled = false;
	else
		EndDateHyperLink.disabled = true;
	
}


var CalendarWindow = null;

function GetCalendar(targetPage, 
				     targetWindow,
				     textBoxId ,
				     linkId)
{
  
  // Note: The name of the Page which contains the Calendar control       
  
   var ns4 = (document.layers)? true:false
   var link  = GetElt( linkId );
   
   if ( link.disabled )
   {
       return;
   }
       
   calWindowUrl = "../common/calendar.aspx";
         
	var formName =  document.forms[0].name ;
	var txtBox = document.forms[0].elements[textBoxId];
	
	var txtBoxName = document.forms[0].elements[textBoxId].name ;
	var WindowWidth = 300;
	var WindowHeight = 250;
	var posX;
	var posY;
	posX = findPosX( link );
	posY = findPosY( link );
	
	
	if ( document.all )
	{
	    posX = window.event.x ;
		posY = window.event.y ;
		posY += 100;
	}
	else
	{
		posX = findPosX( link );
		posY = findPosY( link );
	}
	
			
	var url = calWindowUrl + "?" + "formName=" + formName + "&TextBoxName=" + txtBoxName;
	if ( txtBox )
	{
	   var scheduleDate = trim(txtBox.value);
	  if  ( scheduleDate != "" )
	  {
		url += "&ScheduleDate=" + scheduleDate;
	  }
	}
	
	var windowName = "AcCalendarWindow";	
	
	if  ( CalendarWindow != null )
	{
		CalendarWindow.close();
		CalendarWindow = null;
	}
	
	CalendarWindow = window.open( url , windowName, "width=" + WindowWidth +",height=" + WindowHeight + ",resizable=1" + ",top=" + posY + ",left="+ posX );
	
	
}

// This function is called on after the schedule.ascx control is loaded
function schedule_OnLoad()
{
  var scheduleType = document.forms[0].scheduleTypeField.value;
}


function findPosX(obj)
{
	var curleft = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

function findPosY(obj)
{
	var curtop = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
	return curtop;
}

