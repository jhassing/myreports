/**
 * Search.js
 *
 * @author  Actuate Corporation
 * @version 1.0
 * $Revision::	$
*/

///////////////////////////////////////////////////////////////////////////////
// Globals
///////////////////////////////////////////////////////////////////////////////

var ns4 = (document.layers)? true:false

// g_amountHitsPerPage is the number of search results which get display per page.
// If the total number of matches exceeds this the user will need to use
// the previous/next links on the results page to move through the results.
// The value is set from viewframeset.achtml and can be chnaged if needed.

var	g_amountHitsPerPage;
var g_imageDir = "../images/viewer/";

var g_widthCheck;
var g_heightCheck;

var g_searchIdList = null;
var g_searchDisplayList = null;
var g_searchClassIdList = null;
var g_searchValueList = null;
var g_searchSelectedList = null;

var g_searchDocument;
var g_searchFrame;
var g_reportFrame = null;

var g_tagMarker		= "$$$Marker";
var g_tagArea		= "$$$Area";
var g_tagIdPrefix	= "I";


var g_selectedItemDeleteButtonOn;
var g_selectedItemDeleteButtonOff;
var g_imgSelectedItemDeleteButtonOn = g_imageDir + "cmd_removefield_n.gif";
var g_imgSelectedItemDeleteButtonOff = g_imageDir + "cmd_removefield_n.gif";

var g_imgBlank = g_imageDir + "img_blank.gif";

var g_platform = navigator.platform;
var g_browserVersion = parseFloat(window.navigator.appVersion);

var searchToolbarPage = "../viewer/searchtoolbar.aspx";


// In Navigator >= 4.6, this variable determines whether the search frame
// is reloaded when updating the search form.  The search frame is to be
// reloaded only when fields are added.
var g_bReloadSearchFrame = false;

///////////////////////////////////////////////////////////////////////////////

function blockSubmit()
{
	return false;
}

///////////////////////////////////////////////////////////////////////////////

function stripTag( inputString, tag )
{
	var stringValue = "";
	var pos = inputString.indexOf( tag );

	if ( pos != -1 )
	{
		stringValue = inputString.slice( 0, pos );
	}
	else
	{
		stringValue = inputString;
	}

	return stringValue;

}

///////////////////////////////////////////////////////////////////////////////

function getSearchId( inputString )
{
   var idString = "";

   //strip the "$$$Area"
   idString = stripTag( inputString, g_tagArea );

   //strip the leading "I"
   idString = idString.slice( 1, idString.length );

   return idString;
}

///////////////////////////////////////////////////////////////////////////////

function searchButton()
{

	var errorMessage = findSearchError();
	if ( errorMessage )
		messageBox( "The requested search was not run.  " + errorMessage );

	// Get the current settings of the search fields

	storeSearchFieldsInfo();

	g_bReloadSearchFrame = false;

	// Recreate and reload the search form
	reloadSearchForm();

	g_bReloadSearchFrame = true;

	if ( ns4 )
	{
		searchHiddenForm = g_searchDocument.layers['searchFieldSet'].document.forms['searchFieldsHiddenForm'];
	}
	else
	{
		searchHiddenForm = elementLocate(g_searchDocument, 'searchFieldsHiddenForm');
	}

	if ( searchHiddenForm && !findSearchError() )
	{

		searchHiddenForm.submit();
		parent.g_searchInitiated = true;
		g_reportFrame.hideAllHashItemsInPage();
	}
}

///////////////////////////////////////////////////////////////////////////////

function findSearchError()
{

	if (!g_searchIdList)
		return "No search criteria have been specified.";

	if (g_searchIdList.length == 0)
		return "No search criteria have been specified.";

	// Test to see whether any of the boxes are checked
	getCurrentSearch(getSearchFieldsForm());
	for (var i = 0; i < g_searchSelectedList.length; i++)
	{
		// Make sure at least one checkbox is selected
		if (g_searchSelectedList[i])
			return null;
	}

	return "Please select at least one search criteria for display.";
}
///////////////////////////////////////////////////////////////////////////////

function reloadSearchForm()
{
	// Rewrite the search frame.

	var searchFieldsDocument;
	if ( ns4 )
	{
		if ( g_searchDocument )
			searchFieldsDocument = g_searchDocument.layers['searchFields'].document;
	}
	else
	{
		if ( g_searchDocument )
			searchFieldsDocument = elementLocate(g_searchDocument, 'searchFields').document;
	}


	var str = "";

	var str1 = writeSearchHead( searchFieldsDocument );
	var str2 = writeSearchDetail( searchFieldsDocument  );
	var str3 = writeSearchTail( searchFieldsDocument  );
	var str4 = writeSearchHiddenForm( searchFieldsDocument );
	str = str1 + str2 + str3 + str4 ;

	createSearchFieldSet( str );
}

///////////////////////////////////////////////////////////////////////////////

function clearButton()
{
	if ( g_searchIdList == null )
	{
		g_searchIdList = eval( 'parent.frames.toolbarframe.g_searchIdList' );
	}

	if ( g_searchDisplayList == null)
	{
		g_searchDisplayList = eval( 'parent.frames.toolbarframe.g_searchDisplayList' );
	}

	if ( g_searchClassIdList == null)
	{
		g_searchClassIdList	= eval( 'parent.frames.toolbarframe.g_searchClassIdList' );
	}

	if ( g_searchValueList == null)
	{

		g_searchValueList = eval( 'parent.frames.toolbarframe.g_searchValueList' );
	}

	if ( g_searchSelectedList == null)
	{
		g_searchSelectedList = eval( 'parent.frames.toolbarframe.g_searchSelectedList' );
	}

    if( g_searchIdList.length != 0 ) {

		// Clear the search field set
		createSearchFieldSet( "" );

		emptyArray( g_searchIdList );
		emptyArray( g_searchDisplayList );
		emptyArray( g_searchClassIdList );
		emptyArray( g_searchValueList );
		emptyArray( g_searchSelectedList );

		// Clear the hash lists
		if ( g_reportFrame == null)
		{
				g_reportFrame = eval( 'parent.frames.reportframe' );
		}

		g_reportFrame.clearSearchLists();
    }
}

///////////////////////////////////////////////////////////////////////////////

function dbout( message )
{
	// alert( message );
}

///////////////////////////////////////////////////////////////////////////////

function messageBox( message )
{
	alert( message );
}

///////////////////////////////////////////////////////////////////////////////

function getSearchFieldsForm()
{
	var searchForm = null;

	if ( ns4 )
	{
		if ( g_searchDocument )
		{
			if ( g_searchDocument.layers['searchFieldSet'] )
			{
				searchForm = g_searchDocument.layers['searchFieldSet'].document.forms['searchFieldsForm'];
			}
		}
	}
	else
	{
		if ( g_searchDocument )
		{
			if ( elementLocate(g_searchDocument, 'searchFieldsForm') )
			{
				searchForm = elementLocate(g_searchDocument, 'searchFieldsForm');
			}
		}
	}

	return searchForm;
}

///////////////////////////////////////////////////////////////////////////////

function getCurrentSearch( searchForm )
{

	var itemIndex = 0;

	if ( !g_searchIdList )
		return;

	for ( var index = 0; index < g_searchIdList.length; index++ )
	{
		var valueObject		= eval( 'searchForm.elements.' + g_searchIdList[index] + 'SearchValue' );
		var selectedObject	= eval( 'searchForm.elements.' + g_searchIdList[index] + 'SearchSelected' );

		dbout( 'searchForm.elements = ' + searchForm.elements );
		dbout( valueObject );
		dbout( selectedObject );

		if ( valueObject )
			g_searchValueList[index] = valueObject.value;


		g_searchSelectedList[index] = selectedObject.checked;
	}

}

///////////////////////////////////////////////////////////////////////////////

function writeSearchHead( searchDocument )
{
	var str = "<FORM name='searchFieldsForm' ONSUBMIT='return blockSubmit()'>";

	


	if ( ns4 )
	{
		str += "<TABLE BGCOLOR='#006699' BORDER='0' CELLPADDING='4' CELLSPACING='0'>";
		str += "<TR>";
		
		if(parent != null)
		{
			str += "<TH NOWRAP WIDTH='100' ALIGN='left'>";
			str += "<DIV STYLE=\"position:relative; top:3px; z-index:3;\"><IMG SRC='" + g_imgBlank + "' height=10 width=80 alt='"+parent.sTTRepField+"'></IMG></DIV>";
			str += "<DIV STYLE=\"position:relative; top:-10px; z-index:2;\"><FONT FACE='Arial' COLOR='white' POINT-SIZE='10' WEIGHT='500'><U>"+parent.sRepField+"</U></FONT></DIV></TH>";

			str += "<TH NOWRAP WIDTH='75' ALIGN='left'>";
			str += "<DIV STYLE=\"position:relative; top:3px; z-index:3;\"><IMG SRC='" + g_imgBlank + "' height=10 width=38 alt='"+parent.sTTRepValue+"'></IMG></DIV>";
			str += "<DIV STYLE=\"position:relative; top:-10px; z-index:2;\"><FONT FACE='Arial' COLOR='white' POINT-SIZE='10' WEIGHT='500'><U>"+parent.sRepValue+"</U></FONT></DIV></TH>";

			str += "<TH NOWRAP WIDTH='10' ALIGN='left'>";
			str += "<DIV STYLE=\"position:relative; top:3px; z-index:3;\"><IMG SRC='" + g_imgBlank + "' height=10 width=48 alt='"+parent.sTTRepDisplay+"'></IMG></DIV>";
			str += "<DIV STYLE=\"position:relative; top:-10px; z-index:2;\"><FONT FACE='Arial' COLOR='white' POINT-SIZE='10' WEIGHT='500'><U>"+parent.sRepDisplay+"</U></FONT></DIV></TH>";
		}
		else
		{
			str += "<TH NOWRAP WIDTH='100' ALIGN='left'>";
			str += "<DIV STYLE=\"position:relative; top:3px; z-index:3;\"><IMG SRC='" + g_imgBlank + "' height=10 width=80 alt='Fields to search or include in results'></IMG></DIV>";
			str += "<DIV STYLE=\"position:relative; top:-10px; z-index:2;\"><FONT FACE='Arial' COLOR='white' POINT-SIZE='10' WEIGHT='500'><U>Report Field</U></FONT></DIV></TH>";

			str += "<TH NOWRAP WIDTH='75' ALIGN='left'>";
			str += "<DIV STYLE=\"position:relative; top:3px; z-index:3;\"><IMG SRC='" + g_imgBlank + "' height=10 width=38 alt='Values to search for'></IMG></DIV>";
			str += "<DIV STYLE=\"position:relative; top:-10px; z-index:2;\"><FONT FACE='Arial' COLOR='white' POINT-SIZE='10' WEIGHT='500'><U>Value</U></FONT></DIV></TH>";

			str += "<TH NOWRAP WIDTH='10' ALIGN='left'>";
			str += "<DIV STYLE=\"position:relative; top:3px; z-index:3;\"><IMG SRC='" + g_imgBlank + "' height=10 width=48 alt='Check to include in results'></IMG></DIV>";
			str += "<DIV STYLE=\"position:relative; top:-10px; z-index:2;\"><FONT FACE='Arial' COLOR='white' POINT-SIZE='10' WEIGHT='500'><U>Display</U></FONT></DIV></TH>";

		}
	}
	else
	{
		str += "<TABLE class='searchPanel' BORDER='0' CELLPADDING='4' CELLSPACING='0'>";
		str += "<TR>";
		if(parent != null)
		{
			str += "<TH class='searchHeader' NOWRAP WIDTH='60%' ALIGN='left'><A class='searchHeader' TITLE='"+parent.sTTRepField+"'>"+parent.sRepField+"</A></TH>";
			str += "<TH class='searchHeader' NOWRAP WIDTH='20%' ALIGN='left'><A class='searchHeader' TITLE='"+parent.sTTRepValue+"'>"+parent.sRepValue+"</A></TH>";
			str += "<TH class='searchHeader' NOWRAP WIDTH='20%' ALIGN='left'><A class='searchHeader' TITLE='"+parent.sTTRepDisplay+"'>"+parent.sRepDisplay+"</A></TH>";
		}
		else
		{
			str += "<TH class='searchHeader' NOWRAP WIDTH='60%' ALIGN='left'><A class='searchHeader' TITLE='Fields to search or include in results'>Report Field</A></TH>";
			str += "<TH class='searchHeader' NOWRAP WIDTH='20%' ALIGN='left'><A class='searchHeader' TITLE='Values to search for'>Value</A></TH>";
			str += "<TH class='searchHeader' NOWRAP WIDTH='20%' ALIGN='left'><A class='searchHeader' TITLE='Check to include in results'>Display</A></TH>";

		}
	}

	str += "</TR>";
	return str;
}

///////////////////////////////////////////////////////////////////////////////

function deleteItem(index)
{
	g_reportFrame.removeFromSearch( g_searchIdList[index] );
}

///////////////////////////////////////////////////////////////////////////////

function escapeQuotes(str)
{
	var pattern = /"/g;
	str = str.replace(pattern, "&quot;");
	return str;
}

///////////////////////////////////////////////////////////////////////////////

function unescapeQuotes(str)
{
	var pattern = /&quot;/g;
	str = str.replace(pattern, "\"");
	return str;
}


///////////////////////////////////////////////////////////////////////////////


function writeSearchDetail( searchDocument )
{
	var str = "";

	if ( !g_searchIdList )
		return;

	for ( var index = 0; index < g_searchIdList.length; index++ )
	{
		var editSize;

		if( ns4 )
			editSize = 13;
		else
			editSize = 15;

		str += "<TR>";

		dbout( 'g_searchDisplayList[index] = ' + g_searchDisplayList[index] );

		str += "<TD NOWRAP class='searchContent'>";
		str += escapeQuotes(g_searchDisplayList[index]);
		str += "</TD>";

		str += "<TD class='searchContent' NOWRAP WIDTH='130'><INPUT NAME='";
		str += g_searchIdList[index] + "SearchValue";
		str += "' TYPE='TEXT' SIZE=" + editSize + " VALUE=\"";
		str += escapeQuotes(g_searchValueList[index]);
		str += "\""
		str += "></TD>";


		str += "<TD class='searchContent' NOWRAP WIDTH='60' ALIGN='center'>";
		str += "<INPUT NAME='"
		str += g_searchIdList[index] + "SearchSelected";
		str += "' TYPE='CHECKBOX' ";
		if ( g_searchSelectedList[index] != 'false' && g_searchSelectedList[index] != false )
		{
			str += "CHECKED ";
		}
		str += "ONCLICK=" + "'" + "onSelectClick();" + "'" + ">";

		str += "&nbsp;&nbsp;&nbsp;&nbsp;<A HREF='javascript:myVoid()' ONCLICK='deleteItem(";
		str += index;
		str += "); return false;' ONMOUSEOVER='return false' ONMOUSEOUT='return false;'>";
		str += "<IMG ID='DeleteSelectedItemButton' SUPPRESS='TRUE' BORDER=0 SIZE=0 SRC='" + g_imgSelectedItemDeleteButtonOff + "' HSPACE=2 ALT='Remove Item from List'></IMG>";
		str += "</A></TD>";

		str += "</TR>";
	}

	return str;
}

///////////////////////////////////////////////////////////////////////////////

function addItemToForm (formString, item)
{
	// This function adds items to a form based on whether the
	// browser sends the items backwards or forwards.
	if (g_browserType.submitsFormItemsBackward)
	{
		return item + formString;
	}
	else
	{
		return formString + item;
	}
}

///////////////////////////////////////////////////////////////////////////////

function writeSearchHiddenForm( searchDocument )
{

	if ( !g_searchIdList )
		return;

	var str = "";

	// Create the hidden form, which will get submitted
	// to the Web Agent
	str += "<FORM NAME=" + "'" + "searchFieldsHiddenForm" + "'" + " METHOD=" + "'" + "POST" + "'" + " ACTION=" + "'" + g_submitSearchURL + "&startingPoint=0&hits=" + g_amountHitsPerPage + "'" + ">";
	var searchHiddenFormInputStr = "";
	var allSearchFieldsStr = "";
	for ( var index = 0; index < g_searchIdList.length; index++ )
	{
		var id = getSearchId( g_searchIdList[index] );

		var searchParam = "";
		searchParam += "<INPUT TYPE=" + "'" + "HIDDEN" + "'" + " NAME=" + "\"";
		if (g_searchClassIdList[index].substring(0,2) == "($" )
		{
			searchParam += (g_searchClassIdList[index]);
			allSearchFieldsStr += (g_searchClassIdList[index]);
		}
		else
		{
			searchParam += id;
			allSearchFieldsStr += id;
		}
		if (g_searchValueList[index])//Search
		{

		    if ( g_searchSelectedList[index] )//Search + Select
		    {
		        searchParam += (":include" );
		        allSearchFieldsStr += (":include" );
		    }
		    else//Only Search and not select
		    {
		        searchParam += (":exclude");
		        allSearchFieldsStr += (":exclude" );
		    }
			searchParam += "\"";
            searchParam += " value=" + "\"" + (g_searchValueList[index]);
            searchParam += "\"";
		}
		else
		{

		    searchParam += (":select" );
		    searchParam += "\"";
		    allSearchFieldsStr += (":select" );
		    if ( g_searchSelectedList[index] )
		    {
		        searchParam += " value=" + "\"true\""  ;
		    }
		    else
		    {
                searchParam += " value=" + "\"false\""  ;
            }
        }



		searchParam += "\"" + ">";
		allSearchFieldsStr += "|";
		// Add Parameter to form String
		searchHiddenFormInputStr = addItemToForm(searchHiddenFormInputStr, searchParam);
	}
	str += "<INPUT TYPE='hidden' NAME='format' VALUE='" + "XMLDisplay" + "'>";
	str += "<INPUT TYPE='hidden' NAME='allSearchFields' VALUE='" + (allSearchFieldsStr) + "'>";
	//CONNECTION HANDLER CHANGE...........
	if (parent.sConnectionHandle)
	{
		str += "<INPUT TYPE='hidden' NAME='connectionHandle' VALUE='" + parent.sConnectionHandle + "'>";
	}
	str += searchHiddenFormInputStr + "</FORM>";
	dbout(str);

	return str;
}


///////////////////////////////////////////////////////////////////////////////

function writeSearchTail( searchDocument )
{
	var str = "";
	str += "<TR HEIGHT='2'>";
	str += "<TD NOWRAP>&nbsp;</TD>";
	str += "<TD NOWRAP>&nbsp;</TD>";
	str += "<TD NOWRAP>&nbsp;</TD>";
	str += "</TR>";
	str += "</TABLE>";

	str += "<TABLE BORDER='0' CELLPADDING='0' CELLSPACING='0' >"
	str += "<TR><TD>&nbsp;</TD></TR>  <TR>";
	str += "<TD ALIGH=LEFT><INPUT TYPE='BUTTON' VALUE='"+BTN_SEARCH_NOW+"' ONCLICK='searchButton()'></TD>";

	str += "</TR>";
	str += "</TABLE>";
	str += "</FORM>";

	return str;
}

///////////////////////////////////////////////////////////////////////////////

function createSearchFieldSet( content )
{
	dbout( 'in createSearchFieldSet()' );

	if ( !parent.frames.toolbarframe)
		return;

	if ( !parent.frames.toolbarframe.recycleLayer )
		return;

	if ( !parent.frames.toolbarframe.getRecycledLayer )
		return;

	var layer;

	// To prevent Netscape v4.6 and above from crashing, we replace the Search Frame
	// when each item is selected.
	if (ns4 && g_browserVersion >= 4.60) {
		if (g_bReloadSearchFrame) {
			window.onUnload = myVoid;
			window.location.replace(window.location.href);
			return;
		}
	}

	if ( ns4 )
	{
		if ( g_searchDocument )
			layer = g_searchDocument.layers['searchFieldSet'];
	}
	else
	{
		if ( g_searchDocument )
			layer = elementLocate(g_searchDocument, 'searchFieldSet');
	}

	if ( g_searchDocument && g_searchFrame )
	{
		if ( layer != null )
		{
			// Delete the layer

			var layerName = 'searchFieldSet';

			deleteLayer( layerName, null, g_searchDocument );

			// Recycle the layer
			parent.frames.toolbarframe.recycleLayer( layerName, window, g_searchDocument );

		}

		var left	= getLeft(   'searchFields', g_searchDocument );
		var top		= getTop(    'searchFields', g_searchDocument );
		var width	= getWidth(	 'searchFields', g_searchDocument );
		var height	= getHeight( 'searchFields', g_searchDocument );

		var layerObject = parent.frames.toolbarframe.getRecycledLayer( width, window );

		g_searchFrame.createLayer( 'searchFieldSet'
								 , null
								 , layerObject
								 , left
								 , top
								 , null
								 , null
								 , content
								 , null
								 , null
								 , 'visible'
								 );
	}
}

///////////////////////////////////////////////////////////////////////////////

function updateSearchFrame( bAdd, selectedObjectId, searchValue, searchSelected, searchDisplay, searchClassId )
{

	dbout( 'in updateSearchFrame() selectedObjectId = ' + selectedObjectId + ' searchSelected = ' + searchSelected + ' searchValue = ' + searchValue);

	// Get the current settings of the search fields

	if ( !g_searchIdList )
		return;

	var searchForm = getSearchFieldsForm();
	if ( searchForm )
		getCurrentSearch( searchForm );

	if ( bAdd )
	{
		// Add the item

		dbout(' Adding item to search list' );

		// Check if the item class already exists.
		// If so, don't add it.

		var itemIndex = findItem( g_searchClassIdList, searchClassId );
		if ( itemIndex != -1 )
			return;

		// Add the object to the search lists.
		g_searchIdList[ g_searchIdList.length ]	= selectedObjectId;

		if ( searchValue )
		{
			g_searchValueList[ g_searchValueList.length ] = escapeQuotes(searchValue);
		}
		else
			g_searchValueList[ g_searchValueList.length ] = "";

		g_searchSelectedList[ g_searchSelectedList.length ] = searchSelected;

		if ( searchDisplay )
			g_searchDisplayList[ g_searchDisplayList.length ] = decode(searchDisplay);
		else
			g_searchDisplayList[ g_searchDisplayList.length ] = "";

		if ( searchClassId )
			g_searchClassIdList[ g_searchClassIdList.length ] = searchClassId;
		else
			g_searchClassIdList[ g_searchClassIdList.length ] = "";
	}
	else
	{
		// Remove the item

		// Find the object in the search lists.
		var itemIndex = findItem( g_searchIdList, selectedObjectId );

		// If found, remove the object from the search lists.
		if ( itemIndex != -1 )
		{
			removeItem( g_searchIdList,			itemIndex );
			removeItem( g_searchDisplayList,	itemIndex );
			removeItem( g_searchClassIdList,	itemIndex );
			removeItem( g_searchValueList,		itemIndex );
			removeItem( g_searchSelectedList,	itemIndex );
		}
	}

	// Rewrite the hidden search form
	reloadSearchForm();
}

///////////////////////////////////////////////////////////////////////////////

function clearSearchCookies()
{
	for ( var index = 0; true; index++ )
	{
		var value = readCookie( "searchId" + index );

		if ( value == "" )
			break;

		writeCookie( "searchId"			+ index, "" );
		writeCookie( "searchDisplay"	+ index, "" );
		writeCookie( "searchValue"		+ index, "" );
		writeCookie( "searchSelected"	+ index, "" );
	}
}

///////////////////////////////////////////////////////////////////////////////

function storeSearchFieldsInfo()
{

	if ( !g_searchIdList )
		return;

	// Get the current settings of the search fields


	var searchForm = getSearchFieldsForm();
	if ( searchForm )
		getCurrentSearch( searchForm );


}

///////////////////////////////////////////////////////////////////////////////

function resizeFix()
{
	if ( g_browserVersion > 4.08 || g_widthCheck != window.innerWidth || g_heightCheck != window.innerHeight )
	{
		// For MacOS, we should reload the whole frameset. Reloading the frame only will cause the
		// Netscape browser to crash with a "type 2"(memory) error.

		if (g_platform.indexOf("Mac") != -1)
		{
			parent.location.reload();
		}
		else
		{
			storeSearchFieldsInfo();
			window.stop();
			window.setTimeout( 'window.location.reload();', 1000 );
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

function myVoid()
{
	return true;
}

///////////////////////////////////////////////////////////////////////////////

function onSelectClick()
{
	dbout( 'in onSelectClick()' );
}

