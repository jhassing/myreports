/**
 * 
 *
 * @author  Actuate Corporation
 * @version 1.0
 */

///////////////////////////////////////////////////////////////////////////////
function onLoad()
{
	parent.g_bSearchFrameLoaded = false;

	ns4 = (document.layers)? true:false
	g_platform = navigator.platform;
	g_browserVersion = parseFloat(window.navigator.appVersion);

	g_searchFrame		= eval( 'parent.frames.searchframe' );
	
	if (g_searchFrame)
		g_searchDocument	= eval( 'parent.frames.searchframe.document' );
	
	g_reportFrame		= eval( 'parent.frames.reportframe' );

	// Netscape Resize Fix
	if ( document.layers ) 
	{
		g_widthCheck	= window.innerWidth;
		g_heightCheck	= window.innerHeight;
		window.onResize = resizeFix;
	}

	if (parent.frames.toolbarframe)
	{
		g_searchIdList			= eval( 'parent.frames.toolbarframe.g_searchIdList' );
		g_searchDisplayList		= eval( 'parent.frames.toolbarframe.g_searchDisplayList' );
		g_searchClassIdList     = eval( 'parent.frames.toolbarframe.g_searchClassIdList' );
		g_searchValueList		= eval( 'parent.frames.toolbarframe.g_searchValueList' );
		g_searchSelectedList	= eval( 'parent.frames.toolbarframe.g_searchSelectedList' );
	}

	g_selectedItemDeleteButtonOn 	= new Image(8, 8); 	
	g_selectedItemDeleteButtonOff	= new Image(8, 8); 	
	g_selectedItemDeleteButtonOn.src = g_imgSelectedItemDeleteButtonOn;
	g_selectedItemDeleteButtonOff.src = g_imgSelectedItemDeleteButtonOff;

	g_amountHitsPerPage = parent.g_amountSearchHitsPerPage;
	
	g_bReloadSearchFrame = false;
	reloadSearchForm();
	g_bReloadSearchFrame = true;

	parent.g_bSearchFrameLoaded = true;
	//	SCR 63886
	//parent.g_searchOpen         = true;
	parent.g_searchResults      = false;
	parent.g_searchInitiated    = false;

	if ( g_reportFrame && g_reportFrame.setupSearch )
	{
		if (g_reportFrame.hideBalloons)
		{
			g_reportFrame.hideBalloons();
		}
	}

	if ( g_reportFrame && g_reportFrame.checkIfAllLoaded )
	{
		g_reportFrame.checkIfAllLoaded();
	}


	parent.g_inSearchRequestForm = true;

	if (parent.frames.searchtoolbar && parent.frames.searchtoolbar.searchToolbarLoaded)
		parent.frames.searchtoolbar.enableSearchToolbar();
}

///////////////////////////////////////////////////////////////////////////////

function onUnload()
{
	storeSearchFieldsInfo();
	if ( parent.frames.toolbarframe )
	{
		if( parent.g_bToolbarFrameLoaded && parent.frames.toolbarframe.clearRecycledLayers )
			parent.frames.toolbarframe.clearRecycledLayers( window );
	}
}

///////////////////////////////////////////////////////////////////////////////
