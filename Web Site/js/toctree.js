/*
 *	@author 	Actuate Corporation
 *				Copyright (C) 2001 Actuate Corporation. All rights reserved.
 *	@version	1.0
 */

///////////////////////////////////////////////////////////////////////////////
//
// toctree.js
//
// Copyright (c) 2000 Actuate Corporation
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Globals
///////////////////////////////////////////////////////////////////////////////

var ns4 = (document.layers)? true:false;
var g_platform = navigator.platform;
var g_browserVersion = parseFloat(window.navigator.appVersion);

var g_imageDir = "../images/viewer/";

var g_Tree;
var g_cNodes;
var g_nodesArray;
var g_nodesToOpenArray;

var g_bDrawRoot; // Set g_bDrawRoot to false, prevents drawing the root.

var g_writeBuffer;

var g_bExpandMode;

///////////////////////////////////////////////////////////////////////////////
// Global Images
///////////////////////////////////////////////////////////////////////////////

var g_imgTocMinusNodeStr;
var g_imgTocPlusNodeStr;
var g_imgTocPlusOverNodeStr;
var g_imgTocMinusOverNodeStr;
var g_imgTocBlankStr;

var g_imgObjectPlusLastNode;
var g_imgObjectMinusLastNode;
var g_imgObjectMinusNode;
var g_imgObjectPlusNode;
var g_imgObjectPlusOverNode;
var	g_imgObjectMinusOverNode;
var g_imgObjectBlank;

///////////////////////////////////////////////////////////////////////////////
// Global Html Elements
///////////////////////////////////////////////////////////////////////////////

var g_imageSrc;
var g_imgNodeIcon;
var g_imgFolderIcon;
var g_imageDimNoBorder;
var g_imageIconEnd;

var g_archorEnd;
var g_DoubleQuote;
var g_folderLinkVoid1;
var g_folderLinkVoid2;
var g_folderLinkDisabled;

var g_width;
var g_imageDimEnd;

///////////////////////////////////////////////////////////////////////////////
// Global Layer Elements
///////////////////////////////////////////////////////////////////////////////

var g_fPreviousLayerOpen;
var g_fPreviousLayerHadChildren;
var g_cLayers;
var g_layerBgColor;
var g_currentLayerObject;
var g_currentFolderObject;

///////////////////////////////////////////////////////////////////////////////
// Global Anchor Elements
///////////////////////////////////////////////////////////////////////////////

var g_anchorString;

var g_anchorString1a;
var g_anchorString2a;
var g_anchorString3a;
var g_anchorString4a;

var g_anchorString1b;
var g_anchorString4b;

var g_anchorString1c;

///////////////////////////////////////////////////////////////////////////////
// Global Scrollbar position values
// The values are stored in the toolbar achtml file viewnav.achtml.
// These values can then be retreived from there when the toc frame
// is loaded again when a node is expanded for the very first time.
///////////////////////////////////////////////////////////////////////////////

var g_tocScrollBarPositionX;
var g_tocScrollBarPositionY;

///////////////////////////////////////////////////////////////////////////////

var g_widthCheck = window.innerWidth;
var g_heightCheck = window.innerHeight;

///////////////////////////////////////////////////////////////////////////////

var g_toolbarFrameReference = null;
var g_tocFrameReference = null;

///////////////////////////////////////////////////////////////////////////////

var g_linkErrorPage = "../common/linkmessage.html";

///////////////////////////////////////////////////////////////////////////////

function myVoid() {
	return true;
}

///////////////////////////////////////////////////////////////////////////////

function initGlobals()
{
	g_Tree						= 0
	g_nodesArray				= new Array
	g_nodesToOpenArray			= new Array
	g_cNodes					= 0

	g_imgTocMinusNodeStr		= g_imageDir + "img_minus.gif";
	g_imgTocPlusNodeStr			= g_imageDir + "img_plus.gif";
	g_imgTocPlusOverNodeStr		= g_imageDir + "img_plusred.gif";
	g_imgTocMinusOverNodeStr	= g_imageDir + "img_minusred.gif";
	g_imgTocBlankStr			= g_imageDir + "img_tocblank.gif";

	g_imgObjectMinusNode		= preLoadImage( g_imgTocMinusNodeStr );
	g_imgObjectPlusNode			= preLoadImage( g_imgTocPlusNodeStr );
	g_imgObjectPlusOverNode		= preLoadImage( g_imgTocPlusOverNodeStr );
	g_imgObjectMinusOverNode	= preLoadImage( g_imgTocMinusOverNodeStr );
	g_imgObjectBlank			= preLoadImage( g_imgTocBlankStr );

	g_imageSrc					= "<img src='";
	g_imgNodeIcon				= "<img name='nodeIcon";
	g_imgFolderIcon				= "<img name='folderIcon";
	g_imageDimNoBorder			= "' width=19 height=19 border=0 align='center'>";
	g_imageIconEnd				= "' border=0 align='center'></a>";
	g_imgHandleMouseMsgsPart1	= "' onmouseover='onMouseOver(";
	g_imgHandleMouseMsgsPart2	= ");' onmouseout='onMouseOut(";
	g_imgHandleMouseMsgsPart3	= ");";

	g_archorEnd					= "</a>";
	g_DoubleQuote				= '"';

	// Since we got rid of the stylesheet in the template
	g_folderLinkVoid1			= "<a href='" + g_linkErrorPage + "' onClick='clickOnFolder(";

	// Since we got rid of the stylesheet in the template
	g_folderLinkVoid2			= "); return false'><font color='#000000' point-size=10 size='2' face='Arial'>";

	// Since we got rid of the stylesheet in the template
	g_folderLinkDisabled		= "</font><a>";

	g_width						= "' width=";
	g_imageDimEnd				= " height=19 border=0 align='center'>";

	g_fPreviousLayerOpen		= -1;
	g_fPreviousLayerHadChildren = false;
	g_cLayers					= 0;
	g_layerBgColor				= 'yellow';
	g_currentLayerObject		= null;
	g_currentFolderObject		= null;

	g_anchorString				= "";

	g_anchorString1a			= "<a href='" + g_linkErrorPage + "' onmouseover='onMouseOver(";
	g_anchorString2a			= ");' onmouseout='onMouseOut(";
	g_anchorString3a			= ");' onClick='clickOnNode(";
	g_anchorString4a			= "); return false;'";

	g_anchorString1b			= "<a href='" + g_linkErrorPage + "' onClick='expandNode(";
	g_anchorString2b			= "); return false;' onmouseover='onMouseOver(";
	g_anchorString4b			= ");'"

	g_anchorString1c			= "<a>";

	g_bDrawRoot					= false;

	g_writeBuffer				= "";

	g_tocSrollBarPositionX		= 0;
	g_tocScrollBarPositionY		= 0;
}

///////////////////////////////////////////////////////////////////////////////

function preLoadImage( imageNameStr )
{
	var imageObject = new Image();
	imageObject.src = imageNameStr;

	return imageObject;
}

///////////////////////////////////////////////////////////////////////////////

var g_beginTime = 0;
function beginTime()
{
	g_beginTime = getTime();
}

///////////////////////////////////////////////////////////////////////////////

function endTime()
{
	var endTime = getTime();

	var totalTime = endTime - g_beginTime;

	dbout( 'total time to display folders t = ' + totalTime );
}

///////////////////////////////////////////////////////////////////////////////

function getTime()
{
	var dateObject = new Date();
	var time = dateObject.getTime();

	//delete dateObject;

	return time;
}

///////////////////////////////////////////////////////////////////////////////

function dbout( outputString )
{
	//alert( outputString );
}

///////////////////////////////////////////////////////////////////////////////

// Definition of class Folder

function Folder( objID, folderDescription, hreference, expand, isExpandable )
{

	//constant data
	this.desc				= folderDescription
	this.hreference			= hreference
	this.expand				= expand

	if (this.expand == "")
	   this.expand = null

	this.id					= -1
	this.parentId			= 0;
	this.navObj				= 0
	this.level				= 0
	this.iconImg			= 0
	this.nodeImg			= 0
	this.isLastNode			= 0
	this.parentObjID		= 0
	this.objID				= objID
	this.isExpandable		= isExpandable
	this.layerObject		= null;
	this.writeToObject		= null;

	//dynamic data
	this.isOpen				= false;
	this.iconSrc			= ""; //g_imgTocFolderOpenStr;
	this.children			= new Array;
	this.nChildren			= 0;

	//methods
	this.addChild				= addChild;
	this.initTreePart1			= initFolderPart1;
	this.initTreePart2			= initFolderPart2;
	this.drawNode				= drawFolder;
	this.createTreeArray		= createTreeArray;
	this.createLink				= createFolderLink;
	this.openLayer				= openFolderLayer;
	this.closeLayer				= closeFolderLayer;
	this.setupLayer				= setupLayer;
	this.writeToDocument		= writeToDocument;
	this.writeToDocumentForce	= writeToDocumentForce;
}

///////////////////////////////////////////////////////////////////////////////

function createFolderObject( objID, description, hreference, expand, isExpandable )
{
	dbout( 'in create folder - href = ' + hreference);
	dbout( 'in createFolderObject() desc = ' + description );
	folder = new Folder( objID, description, hreference, expand, isExpandable )
	return folder
}

///////////////////////////////////////////////////////////////////////////////

function insertFolder( parentFolder, childFolder )
{
//	dbout( 'in insertFolder parentFolder = ' + parentFolder.desc + ' childFolder = ' + childFolder.desc );

	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}

	if ( parentFolder != null && childFolder != null )
	{
	   return parentFolder.addChild( childFolder );
	}
	else
	{
	   return null;
	}
}

///////////////////////////////////////////////////////////////////////////////

function addChild( childNode )
{

	dbout( 'in addChild() this = ' + this.desc + ' childNode = ' + childNode.desc );

	this.children[this.nChildren] = childNode;
	this.nChildren++;

	childNode.parentObjID = this.objID;

	return childNode;
}

///////////////////////////////////////////////////////////////////////////////

function initFolderPart1()
{
	dbout( 'in initFolderPart1() this.desc = ' + this.desc + ' nChildren = ' + this.nChildren );

	this.createTreeArray();

	var nChildren = this.nChildren;

	if ( nChildren > 0 )
	{
		for ( var index = 0 ; index < nChildren; index++ )
		{
			this.children[index].parentId = this.id;
			this.children[index].initTreePart1();
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

function setupLayer( level )
{
	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}


	if ( level == 0 )
	{
		this.openLayer( g_cLayers, g_layerBgColor );
		g_currentLayerObject = this.layerObject;

		g_layerBgColor == 'red' ? g_layerBgColor = 'aqua' : g_layerBgColor = 'red';
		g_cLayers++;
	}

	if ( level == 1 )
	{
		if ( g_fPreviousLayerOpen == -1 )
		{
			this.closeLayer();
			this.openLayer( g_cLayers, g_layerBgColor );
			g_currentLayerObject = this.layerObject;

			g_layerBgColor == 'red' ? g_layerBgColor = 'aqua' : g_layerBgColor = 'red';
			g_cLayers++;

			if ( this.isOpen )
				g_fPreviousLayerOpen = 1;
			else
				g_fPreviousLayerOpen = 0;
		}
		else if ( this.isOpen )
		{
			this.closeLayer();
			this.openLayer( g_cLayers, g_layerBgColor );
			g_currentLayerObject = this.layerObject;

			g_layerBgColor == 'red' ? g_layerBgColor = 'aqua' : g_layerBgColor = 'red';
			g_cLayers++;

			g_fPreviousLayerOpen = 1;
		}
		else if ( !this.isOpen && this.expand == null )
		{
			// This node had been expanded earlier, is now closed, and has been cached,
			// It has to be in a separate layer.

			this.closeLayer();
			this.openLayer( g_cLayers, g_layerBgColor );
			g_currentLayerObject = this.layerObject;

			g_layerBgColor == 'red' ? g_layerBgColor = 'aqua' : g_layerBgColor = 'red';
			g_cLayers++;

			g_fPreviousLayerOpen = 1;
		}
		else if ( g_fPreviousLayerOpen == 1 && !this.isOpen )
		{
			this.closeLayer();
			this.openLayer( g_cLayers, g_layerBgColor );
			g_currentLayerObject = this.layerObject;

			g_layerBgColor == 'red' ? g_layerBgColor = 'aqua' : g_layerBgColor = 'red';
			g_cLayers++;

			g_fPreviousLayerOpen = 0;
		}
		else
		{
			if ( g_fPreviousLayerHadChildren )
			{
				this.closeLayer();
				this.openLayer( g_cLayers, g_layerBgColor );
				g_currentLayerObject = this.layerObject;

				g_cLayers++;

				g_fPreviousLayerOpen = 1;
			}
		}

		if ( this.nChildren )
			g_fPreviousLayerHadChildren = true;
		else
			g_fPreviousLayerHadChildren = false;
	}

	this.layerObject = g_currentLayerObject;
	g_currentFolderObject = this;
}

///////////////////////////////////////////////////////////////////////////////

function initFolderPart2( level, lastNode, bSingleLayer )
{
	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}

	this.level = level;

	if ( bSingleLayer == null || !bSingleLayer )
	{
		this.setupLayer( level );
	}

	g_anchorString = null;
	g_anchorString = "";


	if ( this.expand == null )
	{
		// This nastiness is to work around a bug in Netscape where it
		// doesn't replace the hourglass cursor with the hand after
		// returning from clickOnNode.

		dbout( 'anchor1 ' + g_anchorString2a );
		g_anchorString = g_anchorString1a + this.id + g_anchorString2a + this.id + g_anchorString3a + this.id + g_anchorString4a;
	}
	else
	{
		dbout( 'anchor2 ' + g_anchorString2a );
		g_anchorString = g_anchorString1b + this.id + g_anchorString2b + this.id + g_anchorString2a + this.id + g_anchorString4b;
	}

	if ( level > 0 )
	{
		if ( this.isExpandable > 0 )
		{
			var nodeIconStr = g_imgTocPlusNodeStr
			if ( this.isOpen )
			{
				nodeIconStr = g_imgTocMinusNodeStr
			}
			this.drawNode( g_anchorString, g_imgNodeIcon + this.id + "' src='" + nodeIconStr + g_imgHandleMouseMsgsPart1 + this.id + g_imgHandleMouseMsgsPart2 + this.id + g_imgHandleMouseMsgsPart3 + g_imageDimNoBorder, g_archorEnd )
		}
		else
		{
		   this.drawNode( "", "", "" )
		}

		this.isLastNode = 0
	}
	else
	{
		// Set g_bDrawRoot to false, prevents the root folder from being drawn.
		if ( g_bDrawRoot )
		{
			this.drawNode( "", "", "" );
		}
	}

   	var bContinue = true;

	if ( level != 0 && !this.isOpen )
	{
		bContinue = false;
	}

	var nChildren = this.nChildren;



	if ( nChildren > 0 && bContinue )
	{
		level = level + 1

		for ( var i = 0 ; i < nChildren; i++ )
		{

			if ( i == nChildren - 1 )
			{
				this.children[i].initTreePart2( level, 1, bSingleLayer );
			}
			else
			{
				this.children[i].initTreePart2( level, 0, bSingleLayer );
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

function drawFolder( anchorBegin, leftSide, anchorEnd )
{
	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}

	var levelWidth = ( this.level - 1 ) * 19;
	var imageDim = g_width + levelWidth + g_imageDimEnd;

	var newLeftSide = "";
	if ( anchorBegin.length > 0 && anchorEnd.length > 0 )
	{
		var imageDim = g_width + levelWidth + g_imageDimEnd;
		newLeftSide = anchorBegin + ">" + g_imageSrc + g_imgTocBlankStr + imageDim + leftSide + anchorEnd;
	}
	else
	{
		levelWidth += 19;
		var imageDim = g_width + levelWidth + g_imageDimEnd;
		newLeftSide = g_imageSrc + g_imgTocBlankStr + imageDim + leftSide;
	}

	imageDim = null;
	imageDim = "";

	var str = newLeftSide;
	newLeftSide = null;
	newLeftSide = "";

	var linkstr = this.createLink();
	str += linkstr; // + linkstr;
	linkstr = null;
	linkstr = "";

	// This padding using &nbsp; is for neatness.
	// Since we got rid of the stylesheet in the template
	str += this.desc + "</a></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<br>";

	dbout( str );

	this.writeToDocument( str );
	str = null;
	str = "";
}

///////////////////////////////////////////////////////////////////////////////

function writeToDocument( stringToWrite )
{

	if ( g_bExpandMode) {
		saveAndReload();
		return true;
	}
	if (!this.layerObject) {
		// This is for when the TOC is reloading
		// and the layer object of this folder has not
		// been restored.
		if (this.level > 1)
		{
			// All child nodes are drawn in the same layer as the level 1 parent.
			// The parent's layerObject is not null at this point.
			// Hence, assigning parent's layerObject to this child's layerObject.
			var parentFolder = g_nodesArray[this.parentId];
			this.layerObject = parentFolder.layerObject;
		}
		else
		{
			this.layerObject = g_currentLayerObject;
		}
	}

	dbout( stringToWrite );

	g_writeBuffer += stringToWrite;

	var bufferLengthMax;
	if ( ns4 )
		bufferLengthMax = 1000;
	else
		bufferLengthMax = 10000;

	if ( g_writeBuffer.length < bufferLengthMax )
		return true;

	if ( ns4 )
	{
		if ( this.writeToObject == null )
		{
			document.write( g_writeBuffer );
		}
		else
		{
			this.layerObject.document.write( g_writeBuffer );
		}
	}
	else
	{
		HTMLInsert(this.layerObject, "BeforeEnd", g_writeBuffer );
	}

	g_writeBuffer = null;
	g_writeBuffer = "";
}

///////////////////////////////////////////////////////////////////////////////

function writeToDocumentForce()
{
	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}

	if ( g_writeBuffer.length <= 0 )
	{
		g_writeBuffer = null;
		g_writeBuffer = "";
		return;
	}

	dbout( 'in writeToDocumentForce writeBuffer = ' + g_writeBuffer );

	if ( ns4 )
	{
		if ( this.writeToObject == null )
		{
			document.write( g_writeBuffer );
		}
		else
		{
			this.layerObject.document.write( g_writeBuffer );
		}
	}
	else
	{
		HTMLInsert(this.layerObject, "BeforeEnd", g_writeBuffer );
	}
	g_writeBuffer = null;
	g_writeBuffer = "";
}

///////////////////////////////////////////////////////////////////////////////

function createFolderLink()
{
	var str = "";

	if (this.hreference && this.hreference > 0 )
	{
		str += g_folderLinkVoid1 + this.hreference + g_folderLinkVoid2;
		dbout('In create folder link = ' + this.hreference);
	}
	else
	{
		str += g_folderLinkDisabled;
	}

	return str;
}

///////////////////////////////////////////////////////////////////////////////

function createTreeArray()
{
	this.id = g_cNodes;
	g_nodesArray[g_cNodes] = this;
	g_cNodes++;
}

///////////////////////////////////////////////////////////////////////////////

function clickOnFolder( objID )
{
	if (!parent.frames.toolbarframe || !parent.frames.reportframe)
		return;


	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}

	dbout( 'in clickOnFolder()' + objID );

	//newURL = g_vp + objID;
	newURL = replaceDuplicateParams(g_vp , "componentid" , objID);

	var scalingFactorValue = readCookie("scalingFactor");
	if( scalingFactorValue == "")
	{
		scalingFactorValue = "100";
	}

	newURL += "&scalingFactor=" + scalingFactorValue;
	if( parent.g_bToolBarFrameLoaded )
	{
		parent.toolbarframe.disableToolBarButtons();
		parent.toolbarframe.setPageNumber("");
	}

	parent.reportframe.location.replace( newURL );
}

///////////////////////////////////////////////////////////////////////////////

function onMouseOver( folderId )
{
	dbout( 'in onMouseOver()' );

	if ( g_bExpandMode ) {
		return true;
	}

	// Get a pointer to the folder.
	var folder = g_nodesArray[folderId];

	if ( ns4 )
	{
		imageObject = eval( "folder.layerObject.document.images.nodeIcon" + folder.id );
	}
	else
	{
		imageObject = eval( "document.images.nodeIcon" + folder.id );
	}

	var nodeIconStr = g_imgObjectMinusOverNode.src;
	if ( folder.isOpen )
	{
		nodeIconStr = g_imgObjectPlusOverNode.src;
	}

	imageObject.src = nodeIconStr;
}

///////////////////////////////////////////////////////////////////////////////

function onMouseOut( folderId )
{
	if ( g_bExpandMode ) {
		return true;
	}

	// Get a pointer to the folder.
	var folder = g_nodesArray[folderId];

	if ( ns4 )
	{
		imageObject = eval( "folder.layerObject.document.images.nodeIcon" + folder.id );
	}
	else
	{
		imageObject = eval( "document.images.nodeIcon" + folder.id );
	}

	var nodeIconStr = g_imgObjectPlusNode.src
	if ( folder.isOpen )
	{
		nodeIconStr = g_imgObjectMinusNode.src
	}

	imageObject.src = nodeIconStr;
}

///////////////////////////////////////////////////////////////////////////////

function clickOnNode( folderId )
{
	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}

	// Fix for IE4.  DCS 18887
	if(! ns4 )
	{
		setTocScrollBarPositionInfo();
	}

	// Get a pointer to the clicked on folder.
	var clickedFolder = g_nodesArray[folderId]

	// Toggle the state of the folder.
	clickedFolder.isOpen = !clickedFolder.isOpen;

	// For Netscape 4, reload the TOC.
	if (ns4) {
		nsReloadTOC();
		return true;
	}

	// Find the level 1, parent folder
	while ( clickedFolder.level != 1 )
	{
		clickedFolder = g_nodesArray[clickedFolder.parentId];
	}

	// All folders should descend from a
	// level 1 folder.
	if ( clickedFolder.level != 1 )
		return;

	// Clear out the old contents of the layer
	if ( ns4 )
	{
		clickedFolder.writeToObject = clickedFolder.layerObject;
		clickedFolder.layerObject.document.open();

		var tableStr = "<table border=0 cellspacing=0 cellpadding=0>\n<tr>\n<td nowrap>";
		clickedFolder.layerObject.document.write( tableStr );
	}
	else
	{
		clickedFolder.layerObject.innerHTML = "";

		if (g_browserType.doesNotAdjustDimensionsDynamically) {
			// This is to force the layer to redraw itself
			getElementParent(clickedFolder.layerObject).style.width  = '0px'
			getElementParent(clickedFolder.layerObject).style.height = '0px'
			getElementParent(clickedFolder.layerObject).style.width  = ''
			getElementParent(clickedFolder.layerObject).style.height = ''
		}
	}

	// Redraw the entire layer, starting from the level 1 node.
	clickedFolder.initTreePart2( 1, 0, true );

	clickedFolder.writeToDocumentForce();

	// Close the layer
	if ( ns4 )
	{
		clickedFolder.layerObject.document.write( "</td>\n</tr>\n</table>\n" );

		clickedFolder.writeToObject = null;
		clickedFolder.layerObject.document.close();
	}

	// Readjust the position of the layers.
	moveLayers();

	// Fix for IE4.  DCS 18887
	if( !ns4 )
	{
		positionTocScrollBar();
	}
}

///////////////////////////////////////////////////////////////////////////////

function moveLayers()
{
	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}

	dbout( 'moveLayers()' );

	if ( ns4 )
	{
		var cLayers = document.layers.length;

		if ( cLayers == 0 )
			return;

		document.layers[0].visibility = 'show';

		for ( var index = 0; index < cLayers - 1; index++ )
		{
			var top		= document.layers[index].top;
			var height	= document.layers[index].clip.height;
			var newTop = top + height;

			document.layers[index+1].top = newTop;

			document.layers[index+1].visibility = 'show';
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

function openFolderLayer( layerName, backgroundColor )
{
	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}

	dbout( 'in openFolderLayer()' );

	var bgColor = 'white'; //backgroundColor;

	var layerStr = "";
	var tableStr = "";

	if ( ns4 )
	{
		layerStr = "<layer id='layer" + layerName + "' visibility='hidden' bgcolor='" + bgColor + "'>\n";
		tableStr = "<table border=0 cellspacing=0 cellpadding=0>\n<tr>\n<td nowrap>\n";
		document.write( layerStr + tableStr );

		this.layerObject = eval( "document.layers['layer" + layerName + "']" );
	}
	else
	{
		layerStr = "<div id='layer" + layerName + "' visibility='hidden'>\n</div>\n";
		HTMLInsert(elementLocate(document, "toclayer"), "BeforeEnd", layerStr );

		tableStr = "<table border=0 cellspacing=0 cellpadding=0><tr><td id='tdlayer" + layerName + "' nowrap></td>\n</tr>\n</table>\n"
		var layerObject = elementLocate( document, "layer" + layerName );
		HTMLInsert( layerObject, "BeforeEnd", tableStr );

		this.layerObject = elementLocate( document, "tdlayer" + layerName );
	}
}

///////////////////////////////////////////////////////////////////////////////

function closeFolderLayer()
{
	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}

	dbout( 'in closeLayer()' );

	if ( g_currentFolderObject )
	{
		g_currentFolderObject.writeToDocumentForce();
	}

	if ( ns4 )
	{
		document.write( "</td>\n</tr>\n</table>\n</layer>\n" );
	}
}

///////////////////////////////////////////////////////////////////////////////

function openNodes()
{
	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}

	for ( var index = 0; index < g_nodesToOpenArray.length; index++ )
	{
		var folderId = g_nodesToOpenArray[index].id;
		var folder = g_nodesArray[folderId];

		dbout( 'open folder = ' + folder.desc );

		folder.isOpen = true;
	}
}

///////////////////////////////////////////////////////////////////////////////

function generateFormFromExistingNodes( entries, numberOfEntries, submitURL )
{
	// This function returns a string representing an HTML form that
	// can be submitted as part of the ?ViewTOC URL to the web agent.
	// The form consists of hidden inputs that describe the topology
	// of the currently-existing tree.

	var returnForm = "";

	returnForm += "<FORM NAME='existingTree' METHOD=POST ACTION='";
	returnForm += submitURL + "'>\n";

	for ( var i = 0; i < numberOfEntries; i++ )
	{
		with ( entries[i] )
		{
			returnForm += "<INPUT TYPE='hidden' NAME='"
			returnForm += objID + "_desc'"
			returnForm += " VALUE='" + desc + "'"
			returnForm += ">\n"
			returnForm += "<INPUT TYPE='hidden' NAME='"
			returnForm += objID + "_parent'"

			if ( parent == 0 )
			  returnForm += " VALUE='ROOT'"
			else
			  returnForm += " VALUE='" + parent.objID + "'"

			returnForm += ">\n"
			returnForm += "<INPUT TYPE='hidden' NAME='"
			returnForm += objID +"_expand'"

			if ( expand == null )
			  returnForm += " VALUE='0'"
			else
			  returnForm += " VALUE='1'"

			returnForm += ">\n"
			returnForm += "<INPUT TYPE='hidden' NAME='"
			returnForm += objID +"_open'"

			if ( isOpen )
			  returnForm += " VALUE='1'"
			else
			  returnForm += " VALUE='0'"

			returnForm += ">\n"
			returnForm += "<INPUT TYPE='hidden' NAME='"
			returnForm += objID +"_expandable'"

			if ( isExpandable > 0 )
			  returnForm += " VALUE='1'"
			else
			  returnForm += " VALUE='0'"

			returnForm += ">\n"
		}
	}

	returnForm += "</FORM>\n"

	return returnForm
}

///////////////////////////////////////////////////////////////////////////////

function generateURLFromExistingNodes( entries, numberOfEntries, submitURL )
{
	// This function returns a string representing an HTML form that
	// can be submitted as part of the ?ViewTOC URL to the web agent.
	// The form consists of hidden inputs that describe the topology
	// of the currently-existing tree.

	var returnURL = submitURL + "&"

	for ( var i = 0; i < numberOfEntries; i++ )
	{
		with ( entries[i] )
		{
			returnURL += encode(objID) + "_desc"
			returnURL += "=" + encode(desc)
			returnURL += "&"
			returnURL += encode(objID) + "_parent="

			if ( parent == 0 )
			  returnURL += "ROOT"
			else
			  returnURL += encode(parent.objID)

			returnURL += "&"
			returnURL += encode(objID) +"_expand="

			if ( expand == null )
			  returnURL += "0"
			else
			  returnURL += "1"

			returnURL += "&"
			returnURL += encode(objID) +"_open="

			if ( isOpen )
			  returnURL += "1"
			else
			  returnURL += "0"

			returnURL += "&"
			returnURL += encode(objID) +"_expandable="

			if ( isExpandable > 0 )
			  returnURL += "1"
			else
			  returnURL += "0"

			returnURL += "&"
		}
	}

	if ( returnURL.charAt( returnURL.length - 1 ) == "&" )
	   returnURL = returnURL.slice(0, -1)

	return returnURL
}

///////////////////////////////////////////////////////////////////////////////


function expandNode( expandID )
{
	if (!parent.frames.toolbarframe)
		return;

	if (g_bExpandMode) {
		saveAndReload();
		return true;
	}

	dbout( 'In expand url = ' + expandID);

	g_bExpandMode = true;

	parent.toolbarframe.document.saveArray = new Array( g_cNodes )

	g_nodesArray[expandID].expand = null;
	g_nodesArray[expandID].isOpen = true;

	objID = g_nodesArray[expandID].objID;

	expandURL = replaceDuplicateParams(g_vt , "componentid" , objID);

	for ( var index = 0; index < g_cNodes; index++ )
	{
		parent.toolbarframe.document.saveArray[index] = g_nodesArray[index]
	}

	for ( var index = 0; index < g_cNodes; index++ )
	{
		folderObject = g_nodesArray[index];
		delete folderObject;
	}

	g_nodesArray.length = 0;

	dbout( 'expand url = ' + expandURL);

	//The current scroll bar positions for the toc frame are
	//stored so that when the toc page is loaded again, the
	//scroll bar position can be restored.
	//The only time this needs to be done is when the node is
	//expanded the first time. Second and further expansions of
	//a node do not result in the page being loaded again, and
	//the scroll bar state is restored automatically.

	setTocScrollBarPositionInfo();

	// Save the location to go to in the toolbar.
	parent.toolbarframe.document.tocExpandURL = expandURL;

	parent.g_bTocFrameLoaded = false;
	location.replace( expandURL );
}

///////////////////////////////////////////////////////////////////////////////

function restoreNodes()
{
	var bProceed = false;

	if ( ns4 )
	{
		if ( parent.toolbarframe.document.saveArray != null && parent.toolbarframe.document.saveArray != undefined )
			bProceed = true
	}
	else
	{
	   if ( parent.toolbarframe.document.saveArray != null )
		  bProceed = true
	}

	if ( bProceed )
	{
	   with ( parent.toolbarframe.document.saveArray[0] )
	   {
			g_Tree = createFolderObject( objID, desc, hreference )
	   }
	   for ( var i = 1; i < parent.toolbarframe.document.saveArray.length; i++ )
	   {
			with ( parent.toolbarframe.document.saveArray[i] )
			{
				if ( parentObjID == parent.toolbarframe.document.saveArray[0].objID )
				{
				    g_nodesArray["var" + objID] = insertFolder( g_Tree, createFolderObject( objID, desc, hreference, expand, isExpandable ) )
				}
				else
				{
					g_nodesArray["var" + objID] = insertFolder( g_nodesArray["var" + parentObjID], createFolderObject( objID, desc, hreference, expand, isExpandable ) )
				}

				if ( isOpen && isExpandable > 0 )
				{
					g_nodesToOpenArray[g_nodesToOpenArray.length] = g_nodesArray["var" + objID]
				}
			}
		}

		parent.toolbarframe.document.saveArray.length = 0
		parent.toolbarframe.document.saveArray = null
	}
}


/////////////////////////////////////////////////////////////////////////////////
//Use the values stored in g_tocScrollBarPositionX, g_tocScrollBarPositionY
//to move the scroll bar, to restore it to the state before the node was expanded.

function positionTocScrollBar()
{
	if( g_tocFrameReference )
	{
		if( ns4 )
		{
			g_tocFrameReference.scrollTo(parseInt(g_tocScrollBarPositionX),parseInt(g_tocScrollBarPositionY));
		}
		else
		{
			g_tocFrameReference.document.body.scrollLeft = g_tocScrollBarPositionX;
			g_tocFrameReference.document.body.scrollTop  = g_tocScrollBarPositionY;
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////
//Store and save the current scroll bar position values in the global variables
//g_tocScrollBarPositionX, g_tocScrollBarPositionY in the viewnav.achtml(toolbar file).

function setTocScrollBarPositionInfo()
{
	if ( g_toolbarFrameReference )
	{
		if( ns4 )
		{
			g_toolbarFrameReference.g_tocScrollBarPositionX = g_tocFrameReference.pageXOffset;
			g_toolbarFrameReference.g_tocScrollBarPositionY = g_tocFrameReference.pageYOffset;
		}
		else
		{
			g_toolbarFrameReference.g_tocScrollBarPositionX = document.body.scrollLeft;
			g_toolbarFrameReference.g_tocScrollBarPositionY = document.body.scrollTop;
			g_tocScrollBarPositionX = g_toolbarFrameReference.g_tocScrollBarPositionX;
			g_tocScrollBarPositionY = g_toolbarFrameReference.g_tocScrollBarPositionY;
		}
	}
}

// Should be called only when needed. As of now its called only for Netscape on MacOS.
// This because when the browser is resized the event is only caught here, not in the
// toolbar or in the report frame. For Netscape on MacOS, the whole frameset needs to be
// reloaded, or else the browser crashes with a type 2 ("memory error")

function resizeFix()
{
	if (g_browserVersion > 4.08 || g_widthCheck != window.innerWidth || g_heightCheck != window.innerHeight )
	{
		if (g_platform.indexOf("Mac") != -1)
		{
			parent.location.reload();
		}
		else
		{
			parent.g_bTocFrameLoaded = false;
			window.stop();
			window.setTimeout( 'window.location.reload();', 1000 );
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////
//Read and save the current scroll bar position values values in g_tocScrollBarPositionX,
//g_tocScrollBarPositionY. The values are actually stored in variables in the viewnav.achtml
//(toolbar file).


function initTocScrollBarPositionInfo()
{

	parent.g_bTocFrameLoaded = true;

	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}

    g_toolbarFrameReference = parent.frames['toolbarframe'];
    g_tocFrameReference = parent.frames['tocframe'];

	if ( g_toolbarFrameReference )
	{
		g_tocScrollBarPositionX = g_toolbarFrameReference.g_tocScrollBarPositionX;
		g_tocScrollBarPositionY = g_toolbarFrameReference.g_tocScrollBarPositionY;
		positionTocScrollBar();
	}
}

///////////////////////////////////////////////////////////////////////////////

function initDocument()
{
	ns4 = (document.layers)? true:false
	g_platform = navigator.platform;
	g_browserVersion = parseFloat(window.navigator.appVersion);

	// Netscape Resize Fix
	// The resize handler should be set only for Netscape on MacOS.
	// This because when the browser is resized the event is only caught here, not in the
	// toolbar or in the report frame.

	if ( ns4 && g_platform.indexOf("Mac") != -1)
	{
		g_widthCheck	= window.innerWidth;
		g_heightCheck	= window.innerHeight;
		window.onResize = resizeFix;
	}

	dbout( 'before beginTime()' );

	beginTime();

	if ( g_Tree )
	{
		// First, create the tree structure
		dbout( 'before call to initTreePart1()' );
		g_Tree.initTreePart1();
	}
	else	// This is neccessary for IE.
	{
		//g_tree not defined - the browser back button was hit.
		//Reload the toc frame to make sure all variables were
		//set.
		parent.g_bTocFrameLoaded = false;
		parent.tocframe.location.replace(replaceDuplicateParams(g_vt , "componentid" , "0"));
		return;
	}

	// Second, set any open nodes to open, before we
	// draw the folders.
	dbout( 'before call to openNodes()' );
	openNodes();

	// Now, draw out the folders.
	dbout( 'before call to initTreePart2()' );

	if( g_Tree )
	{
		g_Tree.initTreePart2( 0, 1 );
	}

	// Close the last layer
	dbout( 'before call to closeFolderLayer()' );
	closeFolderLayer();

	// Adjust the positions of the layers
	dbout( 'before call to moveLayers()' );
	moveLayers();

	//	parent.parent.g_tocframe = parent.tocframe; // never used

	endTime();

	initTocScrollBarPositionInfo();

	// While a node was being expanded for the
	// first time, everything was "frozen" - see expandNode()
	// Now, we can take the freeze off
	g_bExpandMode = false;

	parent.g_bTocFrameLoaded = true;
}

///////////////////////////////////////////////////////////////////////////////

function unloadTocFrame()
{
	if ( g_bExpandMode ) {
		saveAndReload();
		return true;
	}


	for ( var index = 0; index < g_cNodes; index++ )
	{
		var folderObject = g_nodesArray[index];
		delete folderObject;
	}

	g_nodesArray.length = 0;
	g_cNodes = 0;
}

//////////////////////////////////////////////////////////////////////////////

function saveAndReload()
{
	// This is for when the TOC is "frozen", i.e. g_bExpandMode
	// is true and the user tries anything.
	// It just loads what was previously being loaded.

	setTocScrollBarPositionInfo();
	parent.g_bTocFrameLoaded = false;
	location.replace(parent.toolbarframe.document.tocExpandURL);
	return true;
}

//////////////////////////////////////////////////////////////////////////////

function nsReloadTOC()
{
	// To prevent Netscape 4 from crashing, we replace the TOC Frame
	// even when an item is contracted or re-expanded.
	setTocScrollBarPositionInfo()

	parent.toolbarframe.document.saveArray = new Array( g_cNodes )

	for ( var index = 0; index < g_cNodes; index++ )
	{
		parent.toolbarframe.document.saveArray[index] = g_nodesArray[index];
	}

	parent.g_bTocFrameLoaded = false;

	location.replace(g_vt);
	return true;
}


//////////////////////////////////////////////////////////////////////////////


