/**
 * viewer.js
 *
 * @author  Actuate Corporation
 * @version 1.0
 */
 function checkKey()
{			
	
	var linkObj;
	var btn;
	var sState;
	
	if (event.keyCode == 33)
	{
		// "PageUp"   ASCII value = 33
		linkObj = getLinkById(findButtonId("previouspage"))
	
		if (ns4)
		{
			btn = findButton(linkObj.name);
			if (btn)
			{
				sState = btn.sClassName;
			}
		}
		else
		{
			sState = linkObj.className;
		}
		
		if (sState != "LinkDisable")			
			previousPageClick();
	
	}			
	else if (event.keyCode == 34)
	{
		// "PageDown" ASCII value = 34
		linkObj = getLinkById(findButtonId("nextpage"))
	
		if (ns4)
		{
			btn = findButton(linkObj.name);
			if (btn)
			{
				sState = btn.sClassName;
			}
		}
		else
		{
			sState = linkObj.className;
		}
		
		if (sState != "LinkDisable")			
			nextPageClick();
	}
	else if (event.keyCode == 36)
	{
		// "Home" ASCII value = 36
		linkObj = getLinkById(findButtonId("firstpage"))
	
		if (ns4)
		{
			btn = findButton(linkObj.name);
			if (btn)
			{
				sState = btn.sClassName;
			}
		}
		else
		{
			sState = linkObj.className;
		}
		
		if (sState != "LinkDisable")
			firstPageClick();
			
	}
	else if (event.keyCode == 35)
	{
		// "End" ASCII value = 35
		linkObj = getLinkById(findButtonId("lastpage"))
	
		if (ns4)
		{
			btn = findButton(linkObj.name);
			if (btn)
			{
				sState = btn.sClassName;
			}
		}
		else
		{
			sState = linkObj.className;
		}
		
		if (sState != "LinkDisable")			
			lastPageClick();
	}

}

function replaceDuplicateParams(sURL, sRegExp, sValue)
{
	var pattern = sRegExp + "=\[A-Za-z0-9_%]*&?";
	var regExp = new RegExp( pattern, "i");
	//alert("replaceDuplicateParams sURL= "+sURL+" regExp= "+regExp); 
	var resultArray = regExp.exec(sURL);
	var listArray = sURL.split(regExp);
	var sRetURL = null;
	
	// Check if the parameter is already existing in the url
	if( resultArray != null)
	{
		//Check if more than one occurances are found in the url for the given parameter
		if( resultArray.length == 1)
		{
			// Replace the value of the parameter with the new value
			
			if( listArray.length == 1 || listArray[1] == "")
			{
				//sRetURL = listArray[0] + sRegExp + sValue;
				sRetURL = sURL.replace(regExp, sRegExp +"="+ sValue);
			}
			else
			{
				sRetURL = sURL.replace(regExp, sRegExp + "="+ sValue + "&");
				
			}
			
			// End of replace the value of the parameter with the new value
		}
		else
		{
		}
	}
	else
	{
		sRetURL = sURL + "&" + sRegExp + "="+ sValue;
	}
	return sRetURL;
}

/**
 *
 */
function removeParams(sObjURL, sParam)
{
	var pattern = sParam + "=[^&]*&?";
	var regExp = new RegExp( pattern, "i");
	var resultArray = regExp.exec(sObjURL);
	var listArray = sObjURL.split(regExp);
	var sRetURL = null;

	// Check if the parameter is existing in the url
	if( resultArray != null)
	{
		// Remove the  the parameter 
			if( listArray.length == 1 || listArray[1] == "")
			{
				sRetURL = sObjURL.substring(0 , (listArray[0].length-1));
			}
			else
			{
				sRetURL = sObjURL.replace(regExp, "");
			}
	}
	else
	{
			sRetURL = sObjURL;
	}
	return sRetURL;
}

