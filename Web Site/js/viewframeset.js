/**
 * This javascript file is included in viewframeset.achtml. It implements the class
 * that keeps track of the last page viewed for each report visited for the current
 * browser session.
 *
 * @author  Actuate Corporation
 * @version 1.0
 */

// The constructor function creates a LocationStore object.
function LocationStore(name)
{
	// data
	this.cookieKey = name;
	this.assocSeparator = "&";
	this.assocValueSeparator = ":";
	this.urlArray = new Array();
	this.pageArray = new Array();
	this.currentAction = "UNKNOWN";


	//methods

	this.readLocationStoreCookie = readLocationStoreCookie;
	this.writeLocationStoreCookie = writeLocationStoreCookie;
	this.setAction = setAction;
	this.getAction = getAction;
	this.storeAssociations = storeAssociations;
	this.modifyAssociation = modifyAssociation;
	this.findAssociation = findAssociation;
	this.addAssociation = addAssociation;
	this.createCookieValue = createCookieValue;
	this.getAssociationPage = getAssociationPage;
	this.getAssociationUrl = getAssociationUrl;
	this.modifyLocationStoreCookie = modifyLocationStoreCookie;
	this.removeFirstURLEntry = removeFirstURLEntry;


	var value = this.readLocationStoreCookie();

	if (value != "")
	{
		this.storeAssociations(value);
	}
}


///////////////////////////////////////////////////////////////////////////////

function readLocationStoreCookie()
{
	var value = "";
	var allcookies = document.cookie;
	var pos = allcookies.indexOf( this.cookieKey );

	if ( pos != -1 )
	{
		var start = pos + this.cookieKey.length;
		var end = allcookies.indexOf( ";", start );

		if ( end == -1 )
			end = allcookies.length;

		value = allcookies.slice( start+1, end );
	}

	return value;
}

///////////////////////////////////////////////////////////////////////////////

function getObjectIDCookieValue( objectID)
{
	var value = "";
	var allcookies = document.cookie;
	var pos = allcookies.indexOf( encodeCookie(objectID) + "=" );

	if ( pos != -1 )
	{
		var start = pos + encodeCookie(objectID).length;
		var end = allcookies.indexOf( ";", start );

		if ( end == -1 )
			end = allcookies.length;

		value = allcookies.slice( start+1, end );
	}

	return value;
}

///////////////////////////////////////////////////////////////////////////////

function writeLocationStoreCookie(value)
{
	//Don't store this into the cookie at all.
	//This is no longer used.
	
/*	var cookieString = this.cookieKey + "=" + value;
	cookieString += ";path=" + "/";
	document.cookie = cookieString;*/
}


///////////////////////////////////////////////////////////////////////////////

function setAction(value)
{
	if (value != "")
		this.currentAction = value;
	else
		this.currentAction = "UNKNOWN";
}

///////////////////////////////////////////////////////////////////////////////

function getAction()
{
	return this.currentAction;
}

///////////////////////////////////////////////////////////////////////////////


function storeAssociations(value)
{
	var assocArray = value.split(this.assocSeparator);
	var arrayCount = assocArray.length;

	for (var i = 0; i < arrayCount - 1; i++)
	{
		var assocNameValueArray = assocArray[i].split(this.assocValueSeparator);
		this.urlArray[i] = decodeCookie(assocNameValueArray[0]);
		this.pageArray[i] = assocNameValueArray[1];
	}

	var assocNameValueArray = assocArray[i].split(this.assocValueSeparator);
	this.setAction(assocNameValueArray[1]);
}


///////////////////////////////////////////////////////////////////////////////

function modifyAssociation(name, value)
{
	var index = this.findAssociation(name);

	if (index != -1)
	{
		this.pageArray[index] = value;
		return true;
	}
	else
		return false;
}

///////////////////////////////////////////////////////////////////////////////

function addAssociation(name, value)
{
	this.urlArray[this.urlArray.length] = name;
	this.pageArray[this.pageArray.length] = value;
}

///////////////////////////////////////////////////////////////////////////////

// return index of found association

function findAssociation(name)
{
	for (var i = 0; i < this.urlArray.length; i++)
		if (this.urlArray[i] == name)
			return i;

	var requestID = "";
	requestID = getObjectIDCookieValue( name );

	if ( requestID == "" )
		return -1;

	requestID = decodeCookie( requestID );

	for (var i = 0; i < this.urlArray.length; i++)
	{
		if (this.urlArray[i] == requestID)
		{
			this.urlArray[i] = name;
			document.cookie = encodeCookie(name) + "=; expires=Thu,01-Jan-1970 00:00:01 GMT; path=/";
			return i;
		}
	}
	document.cookie = encodeCookie(name) + "=; expires=Thu,01-Jan-1970 00:00:01 GMT; path=/";
	return -1;
}

///////////////////////////////////////////////////////////////////////////////

function getAssociationUrl(index)
{
	var url = "";

	if (index < this.urlArray.length)
		url = this.urlArray[index];

	return url;
}


///////////////////////////////////////////////////////////////////////////////

function getAssociationPage(index)
{
	var page = -1;

	if (index < this.pageArray.length)
		page = this.pageArray[index];

	return page;
}

///////////////////////////////////////////////////////////////////////////////

function createCookieValue()
{
	var value = "";

	for (var i = 0; i < this.urlArray.length; i++)
	{
		if (value != "")
			value += this.assocSeparator;

		value += encodeCookie(this.urlArray[i]) + this.assocValueSeparator + this.pageArray[i];
	}

	if (value != "")
			value += this.assocSeparator;

	value += "Action" + this.assocValueSeparator + this.currentAction;

	if (this.cookieKey.length + value.length > 3000)
	{
		this.removeFirstURLEntry();
		value = this.createCookieValue();
	}

	return value;
}

function removeFirstURLEntry()
{
	this.urlArray.shift();
	this.pageArray.shift();
}


///////////////////////////////////////////////////////////////////////////////

function modifyLocationStoreCookie(name, action, pageNumber)
{
	this.setAction(action);

	if (!this.modifyAssociation(name, pageNumber))
	{
		this.addAssociation(name, pageNumber);
	}

	value = this.createCookieValue();
	this.writeLocationStoreCookie(value);
}

///////////////////////////////////////////////////////////////////////////////





