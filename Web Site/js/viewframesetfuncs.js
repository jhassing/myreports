/**
 * Viewframesetfuncs.js
 *
 * @author  Actuate Corporation
 * @version 1.0
 */
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Functions/Methods
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Should be called only when needed. As of now its called only for Netscape on MacOS.
// This because when the browser is resized the event is only caught here, not in the 
// toolbar or in the report frame.
function resizeFix() 
{
	if (g_browserVersion > 4.08 || g_widthCheck != window.innerWidth || g_heightCheck != window.innerHeight )
	{
		window.stop();
		window.location.reload();
	}
}

//Extract parameters from the URL

function getValueFromUrl(key, url)
{
	var value = "";
	var urlLength = url.length;
	var keyLength = key.length;

	var pos = url.lastIndexOf(key);
	if (pos != -1)
	{
		pos = pos + keyLength;
		while (url.charAt(pos) != "&"  && pos < urlLength)
		{
			value = value + url.charAt(pos);
			pos = pos + 1;
		}
	}
	return value;
}

// This is called from converter.js - handleLinkClick(). 
// We cannot write the cookie once, when the frameset unloads, since the for netscape
// the document is no longer in scope and for MSIE if the frameset never loaded fully
// the unload() will not be called. we do it when for each page navigation. Also, the 
// action is updated in converter.js - handleLinkClick(), when the user clicks on the 
// external search hyperlink.

function updateLocationStoreCookie()
{
	g_locationStoreObject.modifyLocationStoreCookie(g_objectURL /*g_objectID*/, g_loadAction, g_currentPageNumber);
}

function unescapeAmpSequence(str)
{
	str = str.replace(/&amp;/g,  "&");
	str = str.replace(/&quot;/g, "\"");
	str = str.replace(/&lt;/g,   "<");
	str = str.replace(/&gt;/g,   ">");
	return str;
}