
/*
	Actuate Corporation
	Copyright (C) 2003 Actuate Corporation. All rights reserved.
	
	- Functions used by Skin Customization
	
 */
 
/////////////////////////////////////////////////////////////////////////////
//							Common functions							   //
///////////////////////////////////////////////////////////////////////////// 

function updateField(fieldName, newValue)
{
	var element = document.getElementById(fieldName);
	if (element==null) return false;
	if (element.href != undefined )
	{
		element.src = newValue;
	}
	else
	{	
		element.value = newValue;
	}
	
	return true;
} 


////////////////////////////////////////////////////////////////////////////////
//							Skin Manager Functions							  //
////////////////////////////////////////////////////////////////////////////////

function cloneSkin(name, clonename, nameop, nameskin1, nameskin2, formid, jsprompt, jserror)
{
	var name2 = prompt(jsprompt, clonename);
	if(name2==null)
		return;
	
	name2 = trim(name2);
	if(name2=='')
	{
		alert(jserror);
		return;
	}
	updateField(nameop, 'clone');
	updateField(nameskin1, name);		
	updateField(nameskin2, name2);
	var theform = document.getElementById(formid);
	theform.submit();
}

function trim(s) 
{
  while (s!='' && s.substring(0,1) == ' ') {
    s = s.substring(1,s.length);
  }
  while (s!='' && s.substring(s.length-1,s.length) == ' ') {
    s = s.substring(0,s.length-1);
  }
  return s;
}
	
function deleteSkin(name, nameop, nameskin, formid, prompt)
{
	if (confirm(prompt))
	{ 
		updateField(nameop, 'delete');
		updateField(nameskin, name);
		var theform = document.getElementById(formid);
		theform.submit();
	}
}

function previewSkin(name, nameop, nameskin, formid)
{

	updateField(nameop, 'preview');
	updateField(nameskin, name);
	var theform = document.getElementById(formid);
	theform.submit();
}

function setDefault(name, idop, idskinname, formid)
{
	updateField(idop, 'default');
	updateField(idskinname, name);
	var theform = document.getElementById(formid);
	theform.submit();

}

function setPublic(name, idop, idskinname, formid)
{
	updateField(idop, 'public');
	updateField(idskinname, name);
	var theform = document.getElementById(formid);
	theform.submit();

}

////////////////////////////////////////////////////////////////////////////////
//								Image Tab Functions							  //
////////////////////////////////////////////////////////////////////////////////

function popUpUpload( imageId, fieldId)
{
	//////////// window.open, center on opener, and focus it
	
	openPopupWindow( 'fileupload.aspx?imageId=' + imageId + '&fieldId=' + fieldId, '', 260, 100, '' )
}


////////////////////////////////////////////////////////////////////////////////
//								Color Tab Functions							  //
////////////////////////////////////////////////////////////////////////////////



function showColorPicker( title, openerFieldID, redFieldID, greenFieldID, blueFieldID, sampleColorFieldID )
{
	// Code to populate color picker window
	var colors = new Array("#000000","#000033","#000066","#000099","#0000CC","#0000FF","#330000","#330033","#330066","#330099","#3300CC",
							"#3300FF","#660000","#660033","#660066","#660099","#6600CC","#6600FF","#990000","#990033","#990066","#990099",
							"#9900CC","#9900FF","#CC0000","#CC0033","#CC0066","#CC0099","#CC00CC","#CC00FF","#FF0000","#FF0033","#FF0066",
							"#FF0099","#FF00CC","#FF00FF","#003300","#003333","#003366","#003399","#0033CC","#0033FF","#333300","#333333",
							"#333366","#333399","#3333CC","#3333FF","#663300","#663333","#663366","#663399","#6633CC","#6633FF","#993300",
							"#993333","#993366","#993399","#9933CC","#9933FF","#CC3300","#CC3333","#CC3366","#CC3399","#CC33CC","#CC33FF",
							"#FF3300","#FF3333","#FF3366","#FF3399","#FF33CC","#FF33FF","#006600","#006633","#006666","#006699","#0066CC",
							"#0066FF","#336600","#336633","#336666","#336699","#3366CC","#3366FF","#666600","#666633","#666666","#666699",
							"#6666CC","#6666FF","#996600","#996633","#996666","#996699","#9966CC","#9966FF","#CC6600","#CC6633","#CC6666",
							"#CC6699","#CC66CC","#CC66FF","#FF6600","#FF6633","#FF6666","#FF6699","#FF66CC","#FF66FF","#009900","#009933",
							"#009966","#009999","#0099CC","#0099FF","#339900","#339933","#339966","#339999","#3399CC","#3399FF","#669900",
							"#669933","#669966","#669999","#6699CC","#6699FF","#999900","#999933","#999966","#999999","#9999CC","#9999FF",
							"#CC9900","#CC9933","#CC9966","#CC9999","#CC99CC","#CC99FF","#FF9900","#FF9933","#FF9966","#FF9999","#FF99CC",
							"#FF99FF","#00CC00","#00CC33","#00CC66","#00CC99","#00CCCC","#00CCFF","#33CC00","#33CC33","#33CC66","#33CC99",
							"#33CCCC","#33CCFF","#66CC00","#66CC33","#66CC66","#66CC99","#66CCCC","#66CCFF","#99CC00","#99CC33","#99CC66",
							"#99CC99","#99CCCC","#99CCFF","#CCCC00","#CCCC33","#CCCC66","#CCCC99","#CCCCCC","#CCCCFF","#FFCC00","#FFCC33",
							"#FFCC66","#FFCC99","#FFCCCC","#FFCCFF","#00FF00","#00FF33","#00FF66","#00FF99","#00FFCC","#00FFFF","#33FF00",
							"#33FF33","#33FF66","#33FF99","#33FFCC","#33FFFF","#66FF00","#66FF33","#66FF66","#66FF99","#66FFCC","#66FFFF",
							"#99FF00","#99FF33","#99FF66","#99FF99","#99FFCC","#99FFFF","#CCFF00","#CCFF33","#CCFF66","#CCFF99","#CCFFCC",
							"#CCFFFF","#FFFF00","#FFFF33","#FFFF66","#FFFF99","#FFFFCC","#FFFFFF");
	var total = colors.length;
	var width = 18;
	var cp_contents = "";
	var windowRef = "window.opener.";
	cp_contents += "<HTML><HEAD><TITLE>" + title + "</TITLE></HEAD>";
	cp_contents += "<BODY MARGINWIDTH=0 MARGINHEIGHT=0 LEFMARGIN=0 TOPMARGIN=0><CENTER>";
	cp_contents += '<TABLE BORDER=1 CELLSPACING=1 CELLPADDING=0 ALIGN="LEFT">';
	var use_highlight = (document.getElementById || document.all)?true:false;
	for (var i=0; i<total; i++) 
	{
		if ((i % width) == 0) 
		{ 
			cp_contents += "<TR>"; 
		}
		if (use_highlight) 
		{ 
			var mo = ' onMouseOver="' + windowRef + 'ColorPicker_highlightColor(\'' + colors[i] + '\',window.document)" '; 
			mo = mo +  ' onMouseOut="if(window.opener!=null) ' + windowRef +  'ColorPicker_highlightColor(window.opener.document.getElementById(\'' + openerFieldID + '\').value, window.document)"';
		}
		else 
		{ 
			mo = ""; 
		}
		
		var on_click =    ' onClick= '
						+ ' "if (window.opener!=null) ' 
						+ '  {' 
						+ '	    var  openerField = window.opener.document.getElementById(\'' + openerFieldID + '\');'
						+ '	    var  redField = window.opener.document.getElementById(\'' + redFieldID + '\');'
						+ '	    var  greenField = window.opener.document.getElementById(\'' + greenFieldID + '\');'
						+ '	    var  blueField = window.opener.document.getElementById(\'' + blueFieldID + '\');'
						+ '	    var  sampleColorField = window.opener.document.getElementById(\'' + sampleColorFieldID + '\');'
						+ '		openerField.value =\'' + colors[i] + '\';'
						+ '		window.opener.setSampleColor( openerField , sampleColorField);'
						+ '		window.opener.setRGBValue( openerField ,redField,greenField,blueField);'
						+ '  }'
						+ '  window.close();" ';
						 
		cp_contents += '<TD BGCOLOR="'+colors[i]+'"><FONT SIZE="-3"><A HREF="#"  '+mo+ on_click +' STYLE="text-decoration:none;">&nbsp;&nbsp;&nbsp;</A></FONT></TD>';
		if ( ((i+1)>=total) || (((i+1) % width) == 0)) 
		{ 
			cp_contents += "</TR>";
		}
	}
	// If the browser supports dynamically changing TD cells, add the fancy stuff
	if (document.getElementById) 
	{
		var width1 = Math.floor(width/2);
		var width2 = width = width1;
		var  openerFieldVal = '#ffffff';
		var openerField = document.getElementById(openerFieldID);
		if(openerField!=null)
		{
			openerFieldVal = openerField.value;
		}
		cp_contents += "<TR><TD COLSPAN='"+width1+"' BGCOLOR='" + openerFieldVal + "' ID='colorPickerSelectedColor'>&nbsp;</TD><TD COLSPAN='"+width2+"' ALIGN='CENTER' ID='colorPickerSelectedColorValue'>" + openerFieldVal + "</TD></TR>";
	}
	cp_contents += "</TABLE></CENTER>";
	cp_contents += "</BODY></HTML>";
	// end populate code
	
	//show window and set contents
	cpicker = openPopupWindow( '', '', 240, 210, '' )
	cpicker.document.write(cp_contents);
}

// This function runs when you move your mouse over a color block, if you have a newer browser
function ColorPicker_highlightColor(c) 
{
	var thedoc = (arguments.length>1)?arguments[1]:window.document;
	var d = thedoc.getElementById("colorPickerSelectedColor");
	d.style.backgroundColor = c;
	d = thedoc.getElementById("colorPickerSelectedColorValue");
	d.innerHTML = c;
}

//function to allow only numbers to be entered
function maskKeyPress(objEvent) 
{
	  var iKeyCode;  	
	  iKeyCode = objEvent.keyCode;
	  if(iKeyCode>=48 && iKeyCode<=57) return true;
	  return false;
}

function handleTab(objEvent)
{
	return ( ! ( objEvent.keyCode == 9 ));
}

// function to update Hex when RGB changed by user
function setHexValue( hexField, redField, greenField, blueField)
{
	checkFieldRange( redField );
	checkFieldRange( greenField );
	checkFieldRange( blueField );
	hexField.value = getHex( redField.value, greenField.value, blueField.value );
}

function getHex( red, green, blue )
{
	return "#" + stringToHex(red) + stringToHex(green) + stringToHex(blue);
}

function checkFieldRange( field )
{
	var code =  parseInt( field.value );
	if ( code > 255 )
	{
		code = 255;
	}
	field.value = code;
}
// convert a 10-base string to a 16-base string
function stringToHex( val )
{
	intVal = new Number( parseInt( val ) );
	check = new String( intVal.toString( 16 ));
	check = check.toUpperCase();
	if (check.length < 2)
	{
		return "0" + check;
	}
	else
	{
		return check;
	}
}
		
// function to update RGB when Hex changed by user
function setRGBValue( hexFieldID, redFieldID, greenFieldID, blueFieldID )
{
	hexFieldID.value = hexFieldID.value.replace(/\s*/ig,"");
	var colorValue = ColorToHex( hexFieldID.value );
	redFieldID.value = HexToInt(colorValue.substr(1,2));
	greenFieldID.value = HexToInt(colorValue.substr(3,2));
	blueFieldID.value = HexToInt(colorValue.substr(5,2));
}

function HexToInt(hexString)
{
	return "" + parseInt(hexString, 16 ) ;
}

function setSampleColor(hexField, cellID)
{
	try
	{
		cellID.style.backgroundColor=hexField.value;
	}
	catch(e)
	{}
}
	

var rgbPattern = "[\\s]*[rR][gG][bB][\\s]*\\([\\s]*([0-9]+)[\\s]*,[\\s]*([0-9]+)[\\s]*,[\\s]*([0-9]+)\\)";
var hexPattern = "#[a-fA-F0-9][a-fA-F0-9][a-fA-F0-9][a-fA-F0-9][a-fA-F0-9][a-fA-F0-9]";
			
function ColorToHex( theColor)
{
	
	
	theColor = theColor.toLowerCase( );
	
	rgb = new RegExp(rgbPattern);
	hex = new RegExp(hexPattern);
	toRet = new String("#000000");
	switch ( theColor )
	{
		case "aqua":
			toRet = "#00FFFF";
			break;
		case "black":
			toRet = "#000000";
			break;
		case "blue":
			toRet = "#0000FF";
			break;
		case "fuchsia":
			toRet = "#FF00FF";
			break;
		case "gray":
			toRet = "#808080";
			break;
		case "navy":
			toRet ="#000080";
			break;
		case "green":
			toRet = "#008000";
			break;
		case "lime":
			toRet = "#00FF00";
			break;
		case "maroon":
			toRet = "#800000";
			break;
		case "olive":
			toRet = "#808000";
			break;
		case "purple":
			toRet = "#800080";
			break;
		case "red":
			toRet = "#FF0000";
			break;
		case "silver":
			toRet = "#C0C0C0";
			break;
		case "teal":
			toRet = "#008080";
			break;
		case "white":
			toRet =	"#FFFFFF";
			break;
		case "yellow":
			toRet = "#FFFF00";
			break;
		default:
		{
			
			matchArray = rgb.exec( theColor );
			
				
			if(matchArray!= null && matchArray.length == 4)
			{
				red = matchArray[1];
				green = matchArray[2];
				blue = matchArray[3];
				
				toRet = getHex(red.toString(), green.toString(), blue.toString());
			}
			else
			{
				match = hex.exec(theColor);
				if ( match != null )
				
				{
					toRet = theColor;
				}
				else
				{
					//TODO: Unsuported color!
				}
			}
		}
			break;
	}
	return toRet;
}

function switchVisibility( control, hiddenField )
{
	if ( control.style.display == "none" )
	{
		control.style.display = "block";
		hiddenField.value = "display:block";
	}
	else
	{
		control.style.display = "none";
		hiddenField.value = "display:none";
	}
}	
////////////////////////////////////////////////////////////////////////////////
//								Font Tab Functions							  //
////////////////////////////////////////////////////////////////////////////////

function setFontValues(val, id)
{
	var element = document.getElementById(id);
	element.style.fontFamily =  val;
}

////////////////////////////////////////////////////////////////////////////////
//								Reload Frames (for treeview)				  //
////////////////////////////////////////////////////////////////////////////////
function reloadTop()
{
	if(top.frames.topbanner != null) top.frames.topbanner.location.reload();
	if(top.frames.side != null) top.frames.side.location = top.frames.side.location;
	if(top.frames.footer != null) top.frames.footer.location.reload();
}