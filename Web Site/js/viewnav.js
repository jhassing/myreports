/**
 * ViewNav.js - File contains code to display the Toolbar.
 *
 * @author  Actuate Corporation
 * @version 1.0
 * $Revision::	$
 */

///////////////////////////////////////////////////////////////////////////////
// Globals
///////////////////////////////////////////////////////////////////////////////

var ns4 = (document.layers)? true:false

var g_imageDir = "../images/viewer/";

var tb;

var theDownloadWindow = null;
var thePrintWindow = null;

var _top		= 0;
var _bottom		= 26;

var editValue = js_CurrentPage;   //js_CurrentPage from viewnav.achtml
var hiddenValue = editValue;

var g_cancelled = 0;

var g_tocScrollBarPositionX = 0;
var g_tocScrollBarPositionY = 0;

var g_tocCols		= "245,*";
var g_searchCols	= "350,*";
var g_reportCols	= "0,*";

var g_tocRows       = "*,0";// for IE only
var g_searchRows    = "0,*";// for IE only

var bDebugging		= false;

var currentButton	= -1;
var btnArray		= new Array();

var g_recycledReportLayersList;
var g_recycledSearchLayersList;

var g_areaIdList			= new Array();
var g_areaClassIdList		= new Array();
var g_hashIdList			= new Array();
var g_hashDisplayList		= new Array();
var g_hashClassIdList		= new Array();

var	g_searchIdList			= new Array();
var	g_searchDisplayList		= new Array();
var g_searchClassIdList		= new Array();
var	g_searchValueList		= new Array();
var	g_searchSelectedList	= new Array();

var g_balloonIdList			= new Array();

var g_scalingFactorValueList = new Array();

var g_searchPreviouslyOpen	= false;

var g_reportFrame;

var g_platform = navigator.platform;

var classLinkOff = "LinkOff";
var classLinkOver = "LinkOver";
var classLinkDisable = "LinkDisable";

// Images used in the report frame during search. Needed here to preload before the search
// is conducted. Sometimes the image wouldn't be loaded in time when the mouse moves over
// an searchable item.
var imgMarker	= g_imageDir + "img_marker.gif";
var imgHash		= g_imageDir + "img_hash8.gif";
var imgBlank	= g_imageDir + "img_blank.gif";

var noStyle1 = "Style sheets may be disabled in your browser.\n"
var noStyle2 = "Until style sheets are enabled, this report\n"
var noStyle3 = "may not be displayed as intended.\n"

// EXTERNALIZED RESOURCES USED IN JAVASCRIPT METHODS; INITIALLY DEFINED AS MISSING RESOURCES; UPDATED BY JSP
var ERRMSG_INVALID_PAGE_NUMBER = "Internal Error\nERRMSG_INVALID_PAGE_NUMBER: Missing Resource";

///////////////////////////////////////////////////////////////////////////////

function preLoadImage( imageNameStr )
{
	var imageObject = new Image();
	imageObject.src = imageNameStr;

	return imageObject;
}

///////////////////////////////////////////////////////////////////////////////

function onResize()
{
	var l = tb.layer;
	var cLeft, cRight, cTop, cBottom;

	cLeft = 0;
	cTop  = 0;

	if ( ns4 )
		cRight = 5000;
	else
		cRight = document.body.clientWidth;

	cBottom = _bottom;

	if ( ns4 )
	{
		l.clip.left		= cLeft;
		l.clip.right	= cRight;
		l.clip.top		= cTop;
		l.clip.bottom	= cBottom + 4;
		var NSVersion = parseFloat(window.navigator.appVersion);
		// Only need to do this for NS4.7
		if (NSVersion >= 4.7)
		{
			var replacementUrl = "../viewer/viewnavigation.aspx" + replaceDuplicateParams(js_ObjURL , "page" , hiddenValue);
			document.location.replace (replacementUrl);
		}
	}
	else
	{
		l.style.pixelWidth	= cRight - cLeft;
		l.style.pixelHeight = cBottom - cTop;
		l.style.clip		= "rect("+cTop+","+cRight+","+cBottom+","+cLeft+")";
	}
}

///////////////////////////////////////////////////////////////////////////////

function setCurrentButton( newValue )
{
	currentButton = newValue;
}

///////////////////////////////////////////////////////////////////////////////

function getCurrentButton()
{
	return currentButton;
}

///////////////////////////////////////////////////////////////////////////////

// Button class
var BTN_HAS_ACTION = 0;
var BTN_HAS_NOACTION = 1;
var TXT_OF="of";

function Button()
{
	this.Id = "";
	this.bName = "";
	this.bDescription = "";
	this.fState = "";
	this.fTwoState = "";
	this.bClassOn = "";
	this.bClassOff = "";
	this.bClassDown = "";
	this.bClassOver = "";

	this.sAlign = "";
	this.aCaptions = "";
	this.sToolTip = "";
	this.sClassName = "";

	this.btnType = BTN_HAS_ACTION;
	
	//USED ONLY FOR RETRIEVING PART
	this.editValue = "";
	this.convertBtnToHtml = convertBtnToHtml;
	
	//FOR NS4
	this.left = 0;
}

function convertBtnToHtml()
{
	var str = "";
	if (ns4)
	{
		if (this.btnType == BTN_HAS_ACTION) // DEFINE EVENT HANDLING FOR ALL BUTTONS EXCEPT 'RETRIEVING' (WHICH IS A LABEL ONLY)
		{
			str += "<b>"
				+ "<a id='" + this.Id
				+ "' name='" + this.bName 
				+ "' class='" + this.sClassName+"' " 
				+ this.sToolTip +" "
				+ " href='javascript:myVoid()' "
				+ "onClick='swapLinkClass(this, event, " + this.Id + "); return false;' "
				+ "onDragStart='return false;' "
				+ "onMouseOver='swapLinkClass(this, event, " + this.Id + ")' "
				+ "onMouseOut='swapLinkClass(this, event, " + this.Id + ")' "
				+ "onMouseDown='swapLinkClass(this, event, " + this.Id + "); return false;' "
				+ "onMouseUp='swapLinkClass(this, event, " + this.Id + ")'> "
				+ this.aCaptions
				+ "</a></b>";
		}
		else 
		{
			str += "<form name='form1' onSubmit='handlePageEdit(event); return false' action='javascript:myVoid()'>"
				+ "<font class='"+ this.sClassName + "'>"
				+ "<a id='" + this.Id+ "' name='" + this.bName +"'></a>"
				+ this.aCaptions
				+ "</font>"
				+ "&nbsp;&nbsp;"
				+ "<input type=text name='editThis' size=5 value=\"" + this.editValue + "\">"
				+ "<font class='"+ this.sClassName + "'>"
				+ "&nbsp;" + TXT_OF +"&nbsp;"+ js_TotalPages
				+ "</font>";
			if (!js_IsReportCompleted) str += "+";
			str += "&nbsp;&nbsp;&nbsp;&nbsp;" + getScalingFactorMenu()
				+ "</form>";
		}
	}
	else
	{
		str += "<td nowrap" + this.sAlign + ">&nbsp;&nbsp;" + ((this.btnType == BTN_HAS_NOACTION) ? "" : "<b>") // NO <B> BOLD STYLE FOR LABEL
			+ "<a id='" + this.Id
			+ "' name='" + this.bName 
			+ "' class='" + this.sClassName 
			+ "' style='text-decoration: none'"
			+ " " + this.sToolTip;

		if (this.btnType == BTN_HAS_ACTION) // DEFINE EVENT HANDLING FOR ALL BUTTONS EXCEPT 'RETRIEVING' (WHICH IS A LABEL ONLY)
		{
			str += "href='javascript:myVoid()' "
				 + "onClick='swapLinkClass(this, event, " + this.Id + "); return false;' "
				 + "onDragStart='return false;' "
				 + "onMouseOver='swapLinkClass(this, event, " + this.Id + ")' "
				 + "onMouseOut='swapLinkClass(this, event, " + this.Id + ")' "
				 + "onMouseDown='swapLinkClass(this, event, " + this.Id + "); return false;' "
				 + "onMouseUp='swapLinkClass(this, event, " + this.Id + ")'>";
			str += this.aCaptions;
			str += "</a></b>&nbsp;&nbsp;</td>";
		}
		else 
		{
			str += ">"; // DONT FORGET TO CLOSE THE OPEN '<a' TAG
			str += this.aCaptions;
			str += "</a>&nbsp;&nbsp;</td>";

			str += "<td nowrap><input type=text name='editThis' size=5 value=\"" + editValue + "\"></td>";
			str += "<td nowrap valign='middle'>&nbsp;" + TXT_OF +"&nbsp;"+ js_TotalPages;
			if (!js_IsReportCompleted) str += "+";
			str += "&nbsp;&nbsp;</td><td nowrap valign='middle'>&nbsp;&nbsp;" + getScalingFactorMenu() + "</td>";
		}
	}
	return str;
}


///////////////////////////////////////////////////////////////////////////////

function findButtonId( description )
{
	for ( var index = 0; index < btnArray.length; index++ )
	{
		if ( description == btnArray[index].bDescription )
		{
			return btnArray[index].Id;
		}
	}
	return -1;
}


function findButton (name)
{
	for ( var index = 0; index < btnArray.length; index++ )
	{
		if (name == btnArray[index].bName)
		{
			return btnArray[index];
		}
	}
	return false;
}

///////////////////////////////////////////////////////////////////////////////

function getCommonEvent( eventObject , thisObj)
{
	// Get event data
	commonEvent = new Object();

	// Get event type
	commonEvent.type = eventObject.type;

	// Get event source
	if ( ns4 )
	{
		commonEvent.source = eventObject.target;
		commonEvent.source.id = -1;
	}
	else
	{
		//commonEvent.source = getEventTarget(eventObject);
		commonEvent.source = thisObj;
	}

	// Get event button
	if ( ns4 )
	{
		commonEvent.button = eventObject.which;
	}
	else
	{
		commonEvent.button = eventObject.button;
	}

	// Get event altKey
	if ( ns4 )
	{
		commonEvent.altKey = eventObject.modifiers || Event.ALT_MASK;
	}
	else
	{
		commonEvent.altKey = eventObject.altKey;
	}

	// Get event ctrlKey
	if ( ns4 )
	{
		commonEvent.ctrlKey = eventObject.modifiers || Event.CONTROL_MASK;
	}
	else
	{
		commonEvent.ctrlKey = eventObject.ctrlKey;
	}

	// Get event shiftKey
	if ( ns4 )
	{
		commonEvent.shiftKey = eventObject.modifiers || Event.SHIFT_MASK;
	}
	else
	{
		commonEvent.shiftKey = eventObject.shiftKey;
	}

	// Get event mouse position
	if ( ns4 )
	{
		commonEvent.pageX	= eventObject.pageX;
		commonEvent.pageY	= eventObject.pageY;
		commonEvent.screenX = eventObject.screenX;
		commonEvent.screenY = eventObject.screenY;
	}
	else
	{
		commonEvent.pageX	= eventObject.clientX;
		commonEvent.pageY	= eventObject.clientY;
		commonEvent.screenX = eventObject.screenX;
		commonEvent.screenY = eventObject.screenY;
	}
	return commonEvent;
}

///////////////////////////////////////////////////////////////////////////////
function swapLinkClass(thisObj, event, id)
{
	var targetLink;
	var newLink;

	//Get the common event data
	commonEvent = getCommonEvent(event, thisObj);
	targetLink = getLinkById(id);

	//Get the new image
	newLink = btnArray[id-1];


	//If the button is disabled, we don't need to do anything else.
	if (newLink && newLink.fState == "dis")
	{
		return;
	}

	//Get the new currentButton which is being clicked
	if (event.type == "mousedown")
	{
		currentButton = commonEvent.source.id.toString();
	}
	if (ns4 && (event.type == "click" || event.type == "mousedown"))
	{
		currentButton = id;
	}

	if ( bDebugging )
	{
		if ( id == 1 || id == 2 )
		{
			var element = new Option;
			element.text = "";
			element.text += " id = ";
			element.text += commonEvent.source.id.toString();
			element.text += " type = ";
			element.text += commonEvent.type;
			element.text += " button = ";
			element.text += commonEvent.button;
			element.text += " currentButton = ";
			element.text += currentButton;

			var cItems = parent.frames.toolbarframe.document.forms[0].elements[0].options.length;
			parent.frames.toolbarframe.document.forms[0].elements[0].options[cItems] = element;
		}
	}
	
	for ( var index = 0; index < btnArray.length; index++ )
	{
		if ((ns4) ? (id == btnArray[index].Id) : (commonEvent.source.id == btnArray[index].Id))
		{
			if (commonEvent.type == "mouseover")
			{
				if (currentButton == commonEvent.source.id)
				{
					if (newLink.fState != "on")
					{
						setClassName(targetLink, newLink.bClassDown);
					}
				}
				else if (currentButton == -1 && newLink.fState != "on")
				{
					setClassName(targetLink, newLink.bClassOver);
				}
			}
			else if (commonEvent.type == "mouseout")
			{
				if (newLink.fState != "on")
				{
					setClassName(targetLink, newLink.bClassOff);
				}

				if (!ns4)
					currentButton = -1;
			}
			else if ( commonEvent.type == "mousedown" )
			{
				if (newLink.fState != "on")
					setClassName(targetLink, newLink.bClassDown);
			}
			else if (commonEvent.type == "mouseup" || commonEvent.type == "click")
			{
				if ( currentButton != -1 && ((ns4) ? (currentButton == id) : (currentButton == commonEvent.source.id)))
				{
					if (newLink.fState == "on")
					{
						setClassName(targetLink, newLink.bClassOver);
						newLink.fState = "off";
					}
					else
					{
						if ( newLink.fTwoState )
						{
							setClassName(targetLink, newLink.bClassOn);
							newLink.fState = "on";
						}
						else
						{
							setClassName(targetLink, newLink.bClassOver);
						}
					}
					
					// Take the action.
					buttonClick( (ns4 ? id : commonEvent.source.id));
				}
			}
			else
			{
				setClassName(targetLink, newLink.bClassOff);
			}

			break;
		}
	}

	if ( event.type == "mouseup" || event.type == "click" )
	{
		currentButton = -1;
	}
}


///////////////////////////////////////////////////////////////////////////////


function disableToolBarButtons()
{
	//Disable the "Previous Page" and "First Page" buttons
	changeClass( findButtonId( "firstpage" ),    "disable" );
	changeClass( findButtonId( "previouspage" ), "disable" );

	//Disable the "Next Page" and "Last Page" buttons
	changeClass( findButtonId( "lastpage" ),     "disable" );
	changeClass( findButtonId( "nextpage" ),     "disable" );
	changeClass( findButtonId( "goto" ),         "disable" );
	changeClass( findButtonId( "search" ),       "disable" );
	changeClass( findButtonId( "download" ),     "disable" );
	changeClass( findButtonId( "print" ),        "disable" );
	
	if (!js_IsReportCompleted)
	{
		changeClass( findButtonId( "cancel" ), "disable" );
	}

	//Change from "Page" to "Retrieving"
	var id = findButtonId( "pagelabel" );

	targetLink = getLinkById(id)

	//Get the new image
	newLink = btnArray[id-1];
	changeCaptionAndStyle(targetLink, g_Retrieving, newLink.bClassOff);
	newLink.fState = "off";

}

///////////////////////////////////////////////////////////////////////////////

function enableToolBarButtons()
{
	UpdatePageButtons();

	changeClass( findButtonId( "goto" ), "enable" );


	if (!js_IsReportCompleted)
	{
		changeClass( findButtonId( "search" ),   "disable");
		changeClass( findButtonId( "download" ), "disable");
		changeClass( findButtonId( "print" ),    "disable" );
		changeClass( findButtonId( "toc" ),      "disable");
		
		changeClass( findButtonId( "cancel" ),   "enable" );
	}
	else
	{
		changeClass( findButtonId( "download" ), "enable" );
		changeClass( findButtonId( "print" ),     "enable" );
		//IF BOTH THE VALUES ARE TRUE THEN ONLY ONE SHOULD BE SELECTED - Bose
		if (parent.g_searchOpen)
		{
			if (parent.tocOpen)
			{
				changeClass( findButtonId( "search" ), "off" );
			}
			else
			{
				changeClass( findButtonId( "search" ), "on" );
			}
		}
		else
		{
			changeClass( findButtonId( "search" ), "off" );
		}
	}

	//Change from "Retrieving" to "Page"
	var id = findButtonId( "pagelabel" );

	targetLink = getLinkById(id)

	// Get the new image
	newLink = btnArray[id-1];
	changeCaptionAndStyle(targetLink, g_Page, newLink.bClassOn);
	newLink.fState = "on";
}

///////////////////////////////////////////////////////////////////////////////

function disableSearchToolBarButtons()
{
	if( parent.g_bSearchFrameLoaded &&
		parent.frames.searchtoolbar &&
		parent.frames.searchtoolbar.disableSearchToolbar)
	{
		parent.frames.searchtoolbar.disableSearchToolbar();
	}
}

///////////////////////////////////////////////////////////////////////////////

function enableSearchToolBarButtons()
{
	if( parent.g_bSearchFrameLoaded &&
		parent.frames.searchtoolbar &&
		parent.frames.searchtoolbar.enableSearchToolbar)
	{
		parent.frames.searchtoolbar.enableSearchToolbar();
	}
}

///////////////////////////////////////////////////////////////////////////////

function getLinkById(id)
{
	var targetLink;

	// Get the target link, which is to be swapped
	if ( ns4 )
	{
		//alert(targetLink = document.layers[0].document.layers.length);
		targetLink = document.layers[0].document.layers[id-1].document.anchors[0];
	}
	else
	{
		targetLink = elementLocate(document, id);
	}

	return targetLink;
}

///////////////////////////////////////////////////////////////////////////////

function changeClass( id, state )
{
	var targetLink;
	var newLink;

	// Get the target image, which is to be swapped
	targetLink = getLinkById(id)

	// Get the new image
	newLink = btnArray[id-1];

	if (state == "off")
	{
		setClassName(targetLink, newLink.bClassOff);
		newLink.fState = "off";
	}
	else if ( state == "on" )
	{
		setClassName(targetLink, newLink.bClassOn);
		newLink.fState = "on";
	}
	else if ( state == "over" )
	{
		setClassName(targetLink, newLink.bClassOver);
	}
	else if ( state == "down" )
	{
		setClassName(targetLink, newLink.bClassDown);
	}
	else if ( state == "enable" )
	{
		setClassName(targetLink, newLink.bClassOff);
		newLink.fState = "off";
	}
	else if ( state == "disable" )
	{
		setClassName(targetLink, newLink.bClassDis);
		newLink.fState = "dis";
	}
}

///////////////////////////////////////////////////////////////////////////////

function buttonClick( id )
{
	var btnClicked = btnArray[id - 1];
	var buttonDescription = btnClicked.bDescription;

	if ( buttonDescription == 'toc' )
	{
		tocClick();
	}
	else if ( buttonDescription == 'search' )
	{
		searchClick();
	}
	else if ( buttonDescription == 'firstpage' )
	{
		firstPageClick();
	}
	else if ( buttonDescription == 'lastpage' )
	{
		lastPageClick();
	}
	else if ( buttonDescription == 'previouspage' )
	{
		previousPageClick();
	}
	else if ( buttonDescription == 'nextpage' )
	{
		nextPageClick();
	}
	else if ( buttonDescription == 'goto' )
	{
		gotoClick();
	}
	else if ( buttonDescription == 'cancel' )
	{
		CancelClick();
	}	
	else if ( buttonDescription == 'help' )
	{
		helpClick();
	}
	else if ( buttonDescription == 'download' )
	{
	    downloadClick();
	}
	else if ( buttonDescription == 'print' )
	{
	    printClick();
	}
	else if( buttonDescription == 'exit' )
	{
		if (btnClicked.bDestroy)
			parent.window.close();
		else
			exitClick();
	}
}

///////////////////////////////////////////////////////////////////////////////

function exitClick()
{
	if (!js_IsReportCompleted && !js_IsPersistent)
	{
		// If it is a transient report and the generation is not done, we cancel the execution.
		// The click on the "X" button becomes equivalent to a click on the "Cancel" button.
		CancelClick();
	}
	else
	{
		var curHistoryLength = parent.history.length;

		// If the DHTML Client frameset is embedded in another frame, then we are trying to go back using
		// that frame's history list. If the DHTMl Client frameset URL is the first one in the history list
		// of the embedding frame, the exit button will have no action.

		if (curHistoryLength == 0)
		{
			parent.window.close();
		}
		else
		{
			parent.history.go(parent.g_beforeHistoryLength - curHistoryLength - 1);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

function CancelClick()
{
	g_cancelled = 1;
	// disable all active buttons first
	changeClass( findButtonId( "cancel" ),       "disable" );
	changeClass( findButtonId( "firstpage" ),    "disable" );
	changeClass( findButtonId( "previouspage" ), "disable" );
	changeClass( findButtonId( "lastpage" ),     "disable" );
	changeClass( findButtonId( "nextpage" ),     "disable" );
	changeClass( findButtonId( "goto" ),         "disable" );

	// TODO: make it .aspx independent
	var url = "../newrequest/cancelreport.aspx" + js_ObjURL;

	if (parent != null && parent.sConnectionHandle)
	{
		url = replaceDuplicateParams( url, "connectionHandle", encode(parent.sConnectionHandle));
	}

	window.location = url;
}

///////////////////////////////////////////////////////////////////////////////

function helpClick()
{
	var sLink = g_helpBase + "wwhelp.htm?single=true&context=jsp_viewer&topic=ViewingFramework_mainview";
	theHelpWindow = openNewDefaultWindow( sLink, "helpwindow");
	theHelpWindow.focus();
}

///////////////////////////////////////////////////////////////////////////////
function framesetURLWithObjectID(loc)
{
	var TOCRE    = /ShowTOC=(FALSE|TRUE)/;
	var SearchRE = /ShowSearch=(FALSE|TRUE)/;

	var orgURL = loc.href;

	var retURL = "../viewer/viewframeset.aspx" + js_ObjURL;
	var match;

	match = orgURL.match( TOCRE );
	if( match != null )
	{
		if(!TOCRE.test(js_ObjURL))
			retURL += "&" + match[0];
	}

	match = orgURL.match( SearchRE );
	if( match != null )
	{
		if(!SearchRE.test(js_ObjURL))
			retURL += "&" + match[0];
	}

	return retURL;
}

///////////////////////////////////////////////////////////////////////////////

function toggleSearchFramesetNewPage(searchOpen)
{
	if (parent.g_loadAction)
		parent.g_loadAction = "SEARCH_CLICK";

	newURL = framesetURLWithObjectID(parent.window.location);

	newURL = replaceDuplicateParams(newURL , "page" , hiddenValue);

	searchString = "ShowSearch=";
	match = newURL.lastIndexOf( searchString );

	falseRegExp = new RegExp( "ShowSearch=FALSE" );
	trueRegExp	= new RegExp( "ShowSearch=TRUE" );

	if ( searchOpen )
	{
		// Close the Search Frame
		if ( match != -1 )
		{
		   newURL = replaceDuplicateParams(newURL , "ShowSearch" , "FALSE" );
		}
		else
		{
		   newURL = replaceDuplicateParams(newURL , "ShowSearch" , "FALSE");
		}

		parent.window.location.replace( newURL )
	}
	else
	{
		// Open the Search Frame
		if ( match != -1 )
		{
		   newURL = replaceDuplicateParams(newURL , "ShowSearch" , "TRUE" );
		}
		else
		{
		   newURL = replaceDuplicateParams(newURL , "ShowSearch" , "TRUE");
		}

		searchString = "ShowTOC=";

		match = newURL.lastIndexOf( searchString );

		falseRegExp = new RegExp( "ShowTOC=FALSE" );
		trueRegExp	= new RegExp( "ShowTOC=TRUE" );

		if ( match != -1 )
		{
		   newURL = replaceDuplicateParams(newURL , "ShowTOC" , "FALSE" );
		}
		else
		{
		   newURL = replaceDuplicateParams(newURL , "ShowTOC" , "FALSE");
		}

		parent.window.location.replace( newURL )
	}
}

///////////////////////////////////////////////////////////////////////////////

function toggleSearchFramesetSamePage(searchOpen)
{
	if ( searchOpen )
	{
		// Close the Search Frame
		parent.reportframe.hideAllHashItemsInPage();

		// If the Toc Frame was previously open... then reopen it.
		if ( parent.tocOpen )
		{
			elementLocate(parent.document, "reportframeset").cols = g_tocCols;
			elementLocate(parent.document, "searchframe").noResize = false;
			elementLocate(parent.document, "tocAndSearch").rows   = g_tocRows;
			changeClass( findButtonId( "toc" ), "on" );
		}
		else
		{
			elementLocate(parent.document, "reportframeset").cols = g_reportCols;
			elementLocate(parent.document, "searchframe").noResize = true;
			changeClass( findButtonId( "toc" ), "off" );
		}
	}
	else
	{
		// Open the Search Frame
		if (!parent.g_bSearchFrameLoaded)
		{
			//CONNECTION HANDLE CHANGE....
			if (parent.sConnectionHandle)
			{
				parent.g_searchFrameURL = replaceDuplicateParams(parent.g_searchFrameURL , "connectionHandle", encode(parent.sConnectionHandle));
			}
			parent.frames.searchframe.location.replace( parent.g_searchFrameURL );
			parent.frames.searchtoolbar.location.replace( parent.g_searchTBFrameURL );
		}
		elementLocate(parent.document, "reportframeset").cols = g_searchCols;
		elementLocate(parent.document, "searchframe").noResize = false;
		elementLocate(parent.document, "tocAndSearch").rows   = g_searchRows;
		parent.reportframe.hideBalloons();
		parent.reportframe.setupSearch();

		// Close the Toc Frame
		if (parent.tocOpen)
		{
			changeClass(findButtonId("toc"), "off");
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

function toggleTOCFramesetNewPage()
{
	if (parent.g_loadAction)
		parent.g_loadAction = "TOC_CLICK";

	newURL = framesetURLWithObjectID(parent.window.location);//parent.window.location.href;
	newURL = replaceDuplicateParams(newURL , "page" , hiddenValue);

	searchString = "ShowTOC=";
	match = newURL.lastIndexOf( searchString );

	falseRegExp = new RegExp("ShowTOC=FALSE");
	trueRegExp	= new RegExp("ShowTOC=TRUE");

	if ( parent.tocOpen )
	{
		// Close the Toc Frame
		if ( match != -1 )
		{
		   newURL = replaceDuplicateParams(newURL , "ShowTOC" , "FALSE");
		}
		else
		{
		   newURL = replaceDuplicateParams(newURL , "ShowTOC" , "FALSE");
		}

		parent.window.location.replace( newURL );
	}
	else
	{
		// Open the Toc Frame
		if ( match != -1 )
		{
			newURL = replaceDuplicateParams(newURL , "ShowTOC" , "TRUE");
		}
		else
		{
			newURL = replaceDuplicateParams(newURL , "ShowTOC" , "TRUE");
		}

		searchString = "ShowSearch=";

		match = newURL.lastIndexOf( searchString );

		falseRegExp = new RegExp( "ShowSearch=FALSE" );
		trueRegExp	= new RegExp( "ShowSearch=TRUE" );

		if ( match != -1 )
		{
			newURL = replaceDuplicateParams(newURL , "ShowSearch" , "FALSE" );
		}
		else
		{
			newURL = replaceDuplicateParams(newURL, "ShowSearch" , "FALSE");
		}
		parent.window.location.replace( newURL );
	}
}

///////////////////////////////////////////////////////////////////////////////

function toggleTOCFramesetSamePage()
{
	if (!parent.tocframe)
		return;

	if ( parent.g_searchOpen )
	{
		searchClick();
		g_searchPreviouslyOpen = true;
	}

	if ( parent.tocOpen )
	{
		// Close the Toc Frame
		elementLocate(parent.document, "reportframeset").cols = g_reportCols;

		elementLocate(parent.document, "searchframe").noResize = true;

		if ( g_searchPreviouslyOpen )
		{
			searchClick();
			g_searchPreviouslyOpen = false;
		}
	}
	else
	{
		if (!parent.g_bTocFrameLoaded)
		{
			//CONNECTION HANDLE ISSUE
			parent.g_tocFrameURL = replaceDuplicateParams(parent.g_tocFrameURL , "connectionHandle", encode(parent.sConnectionHandle));
			parent.frames.tocframe.location.replace( parent.g_tocFrameURL );
		}

		// Open the Toc Frame
		elementLocate(parent.document, "reportframeset").cols = g_tocCols;

		elementLocate(parent.document, "searchframe").noResize = false;
		elementLocate(parent.document, "tocAndSearch").rows   = g_tocRows;
	}
}

///////////////////////////////////////////////////////////////////////////////

function searchClick()
{
	var searchOpen = parent.g_searchOpen;
	parent.g_searchOpen = !parent.g_searchOpen;

	if ( ns4 )
	{
		toggleSearchFramesetNewPage(searchOpen);
	}
	else
	{
		if (g_browserType.cannotDynamicallyAdjustFrameset)
			toggleSearchFramesetNewPage(searchOpen);
		else
			toggleSearchFramesetSamePage(searchOpen)
	}

	if ( parent.g_searchOpen )
	{
		changeClass( findButtonId( "search" ), "on" );
	}
	else
	{
		changeClass( findButtonId( "search" ), "off" );
		g_reportFrame.hideSearchAreas();
	}
} // end searchClick()



///////////////////////////////////////////////////////////////////////////////

function tocClick()
{
	if ( ns4 )
	{
		toggleTOCFramesetNewPage();
	}

	else
	{
		if (g_browserType.cannotDynamicallyAdjustFrameset)
			toggleTOCFramesetNewPage();
		else
			toggleTOCFramesetSamePage()
	}

	parent.tocOpen = !parent.tocOpen;

	if ( parent.tocOpen )
	{
		changeClass( findButtonId( "toc" ), "on" );
	}
	else
	{
		changeClass( findButtonId( "toc" ), "off" );
	}
}

///////////////////////////////////////////////////////////////////////////////

function ReplaceFrame(newURL)
{
    parent.reportframe.location.replace( newURL );
}

//////////////////////////////////////////////////////////////////////////////

function GetReplacementUrl(pageNumber, scalingFactor)
{
	// Note: the order of the elements in the following query string
	// need to match the order of the elements of the ViewPage URLs in
	// viewframeset.achtml in order for the first page of the report
	// to be cached when DHTML report caching is enabled.

	var replacementUrl = "viewpage.aspx" + replaceDuplicateParams(replaceDuplicateParams(js_ObjURL , "page" , pageNumber) , "scalingfactor" , scalingFactor);
	replacementUrl = removeParams(replacementUrl, "mode");
	replacementUrl = removeParams(replacementUrl, "range");
	return replacementUrl;
}

///////////////////////////////////////////////////////////////////////////////

function setPageNumber(page)
{
	var editText = null;
	if ( ns4 )
	{
		if ( document.layers.toolbar.document.layers[6].document.forms.length > 0 )
		{
			editText = document.layers.toolbar.document.layers[6].document.forms[0].editThis;
		}
	}
	else
	{
		if ( document.forms.length > 0 )
		{
			editText = document.forms[0].editThis;
		}
	}

	if (editText)
	{
		editText.value = page;
		return true;
	}
	else
	{
		return false;
	}
}


///////////////////////////////////////////////////////////////////////////////

function getPageNumber()
{
	return hiddenValue;
}


///////////////////////////////////////////////////////////////////////////////

function firstPageClick()
{
	disableToolBarButtons();
	disableSearchToolBarButtons();

    if (hiddenValue > 1)
	{
		editValue = 1;
		hiddenValue = editValue;
		scalingFactor = readScalingFactorCookie();

		setPageNumber(editValue);

		if (!js_IsReportCompleted && !((g_browserType.name == "Netscape") && (g_browserType.version == 5)))
		{
			parent.g_bToolBarFrameLoaded = false;
			parent.g_clickActionRefresh = true;
		}

		ReplaceFrame(GetReplacementUrl(editValue, scalingFactor));

		if (!js_IsReportCompleted && !((g_browserType.name == "Netscape") && (g_browserType.version ==5)) )
		{
			location.reload();
		}
    }
}

///////////////////////////////////////////////////////////////////////////////

function previousPageClick()
{
	disableToolBarButtons();
	disableSearchToolBarButtons();
    editValue = hiddenValue;

    if (editValue > 1)
	{
		--editValue;
		hiddenValue = editValue;
		scalingFactor = readScalingFactorCookie();

		setPageNumber(editValue);
		if (!js_IsReportCompleted && !((g_browserType.name == "Netscape") && (g_browserType.version == 5)))
		{
			parent.g_bToolBarFrameLoaded = false;
			parent.g_clickActionRefresh = true;
		}
		ReplaceFrame(GetReplacementUrl(editValue, scalingFactor));
		if (!js_IsReportCompleted && !((g_browserType.name == "Netscape") && (g_browserType.version ==5)) )
		{
			location.reload();
		}

    }
}

///////////////////////////////////////////////////////////////////////////////

function nextPageClick()
{
	disableToolBarButtons();
	disableSearchToolBarButtons();

    editValue = hiddenValue;

    if ((editValue < js_TotalPages) || (!js_IsReportCompleted))
    {
		if (editValue < js_TotalPages) ++editValue;
		hiddenValue = editValue;
		scalingFactor = readScalingFactorCookie();

		setPageNumber(editValue);

		if (!js_IsReportCompleted && !((g_browserType.name == "Netscape") && (g_browserType.version ==5)))
		{
			parent.g_bToolBarFrameLoaded = false;
			parent.g_clickActionRefresh = true;
		}

		ReplaceFrame(GetReplacementUrl(editValue, scalingFactor));

		if (!js_IsReportCompleted && !((g_browserType.name == "Netscape") && (g_browserType.version ==5)) )
		{
			location.reload();
		}
    }
}

///////////////////////////////////////////////////////////////////////////////

function lastPageClick()
{
	disableToolBarButtons();
	disableSearchToolBarButtons();

    if (( hiddenValue < js_TotalPages) || (!js_IsReportCompleted))
    {
		editValue = js_TotalPages;
		hiddenValue = editValue;
		scalingFactor = readScalingFactorCookie();

		setPageNumber(editValue);

		if (!js_IsReportCompleted && !((g_browserType.name == "Netscape") && (g_browserType.version ==5)))
		{
			parent.g_bToolBarFrameLoaded = false;
			parent.g_clickActionRefresh = true;
		}

		ReplaceFrame(GetReplacementUrl(editValue, scalingFactor));
		if (!js_IsReportCompleted && !((g_browserType.name == "Netscape") && (g_browserType.version ==5)) )
		{
			location.reload();
		}
    }
}

///////////////////////////////////////////////////////////////////////////////

function isInteger(value)
{
	var digits = "1234567890";
	for (var i=0; i < value.length; i++)
	{
		if (digits.indexOf(value.charAt(i)) == -1)
		{
			return false;
		}
	}
	return true;
}

///////////////////////////////////////////////////////////////////////////////

function ltrim(str)
{
	for (var i=0; str.charAt(i)==" "; i++);
	return str.substring(i,str.length);
}

///////////////////////////////////////////////////////////////////////////////

function rtrim(str)
{
	for (var i=str.length-1; str.charAt(i)==" "; i--);
	return str.substring(0,i+1);
}

///////////////////////////////////////////////////////////////////////////////

function trim(str)
{
	return ltrim(rtrim(str));
}

///////////////////////////////////////////////////////////////////////////////

function gotoClick()
{
	disableToolBarButtons();
	disableSearchToolBarButtons();

    var newEditValue;
    var editText;

    if ( ns4 )
    {
		editText = document.layers.toolbar.document.layers[6].document.forms[0].editThis;
	}
    else
		editText = document.forms[0].editThis;

    newEditValue = trim(editText.value);

    // Check that the new page number is both an integer and in the valid range.

    if ( !( newEditValue >= 1 && newEditValue <= js_TotalPages && isInteger(newEditValue) ) )
	{
	    alert(ERRMSG_INVALID_PAGE_NUMBER);
	    editText.value = editValue;
	    editText.focus();
	    editText.select();
		enableToolBarButtons();
		enableSearchToolBarButtons();
	}
    else if ( newEditValue != editValue )
	{
	    editValue = newEditValue;
	    hiddenValue = editValue;
	    scalingFactor = readScalingFactorCookie();
	    if (!js_IsReportCompleted && !((g_browserType.name == "Netscape") && (g_browserType.version ==5)))
	    {
	    	parent.g_bToolBarFrameLoaded = false;
	    	parent.g_clickActionRefresh = true;
	    }

	    ReplaceFrame(GetReplacementUrl(editValue, scalingFactor));
	    if (!js_IsReportCompleted && !((g_browserType.name == "Netscape") && (g_browserType.version ==5)) )
	    {
	    	location.reload();
	    }
	}
	else
	{
		enableToolBarButtons();
		enableSearchToolBarButtons();
	}
}

///////////////////////////////////////////////////////////////////////////////

function downloadClick()
{
	var url = "saveas.aspx" + replaceDuplicateParams(replaceDuplicateParams(replaceDuplicateParams(js_ObjURL , "page" , editValue) , "calledfrom" , "nav") , "pagecount" , js_TotalPages) ;
	if (parent != null && parent.sConnectionHandle)
	{
		url = replaceDuplicateParams(url , "connectionHandle", encode(parent.sConnectionHandle));
	}

	if( theDownloadWindow && ! theDownloadWindow.closed )
	{
			theDownloadWindow.focus();
			return;
	}

	theDownloadWindow = window.open( url, "saveAsWindow", 'toolbar=no,resizable=yes,height=250,width=700,scrollbars=yes' );
	theDownloadWindow.opener = this;
	theDownloadWindow.focus();
}

function printClick()
{
	var url = "print.aspx" + replaceDuplicateParams(replaceDuplicateParams(js_ObjURL ,"calledfrom" , "nav") , "pagecount" , js_TotalPages) ;
	if (parent != null && parent.sConnectionHandle)
	{
		url = replaceDuplicateParams(url , "connectionHandle", encode(parent.sConnectionHandle));
	}
	url = removeParams(url, "page");

	if( thePrintWindow && ! thePrintWindow.closed )
	{
			thePrintWindow.focus();
			return;
	}

	thePrintWindow = window.open( url, "printWindow", 'toolbar=no,resizable=yes,height=580,width=780,scrollbars=yes' );
	thePrintWindow.opener = this;
	thePrintWindow.focus();
}

///////////////////////////////////////////////////////////////////////////////

function UpdatePageButtons()
{
	if (ns4)
		editValue = document.layers.toolbar.document.layers[6].document.forms[0].editThis.value;
	else
		editValue = document.forms[0].editThis.value;

	hiddenValue = editValue;

	// Check if we are on the first page.

	if ( editValue == 1 )
	{
		// Disable the "Previous Page" and "First Page" buttons
		changeClass( findButtonId( "firstpage" ), "disable" );
		changeClass( findButtonId( "previouspage" ), "disable" );
	}
	else
	{
		// Enable the "Previous Page" and "First Page" buttons
		changeClass( findButtonId( "firstpage" ), "enable" );
		changeClass( findButtonId( "previouspage" ), "enable" );
	}

	// Check if we are on the last page. Disable only if report is completed

	if (( editValue == js_TotalPages ) && (js_IsReportCompleted))
	{
		// Disable the "Next Page" and "Last Page" buttons
		changeClass( findButtonId( "lastpage" ), "disable" );
		changeClass( findButtonId( "nextpage" ), "disable" );
	}
	else
	{
		// Enable the "Next Page" and "Last Page" buttons
		changeClass( findButtonId( "lastpage" ), "enable" );
		changeClass( findButtonId( "nextpage" ), "enable" );
	}
}

///////////////////////////////////////////////////////////////////////////////

function handlePageEdit(event)
{
    gotoClick()
}


///////////////////////////////////////////////////////////////////////////////

// called from the onLoad function in this file.
function populateScalingFactorValueList()
{
	g_scalingFactorValueList[0] = 25;
	g_scalingFactorValueList[1] = 50;
	g_scalingFactorValueList[2] = 75;
	g_scalingFactorValueList[3] = 100;
	g_scalingFactorValueList[4] = 150;
	g_scalingFactorValueList[5] = 200;
	g_scalingFactorValueList[6] = 300;
	g_scalingFactorValueList[7] = 400;
}


///////////////////////////////////////////////////////////////////////////////


function readScalingFactorCookie()
{
	var scalingFactorValue = readCookie("scalingFactor");
	if( scalingFactorValue == "" )
	{
		scalingFactorValue = "100";
	}

	return scalingFactorValue;
}


///////////////////////////////////////////////////////////////////////////////



function writeScalingFactorCookie(scalingFactorValue)
{
	var nextYear = new Date();
	nextYear.setFullYear(nextYear.getFullYear() + 1);
	var expirationDate = nextYear.toGMTString();
	writeCookie("scalingFactor", scalingFactorValue, expirationDate, "/");
}


///////////////////////////////////////////////////////////////////////////////


function handleScalingFactorChange(form)
{
	var editValue;
	var scalingFactorValue = form.scalingFactor.options[form.scalingFactor.selectedIndex].value;

	writeScalingFactorCookie(scalingFactorValue);

	if (ns4)
		editValue = document.layers.toolbar.document.layers[6].document.forms[0].editThis.value;
		//editValue = document.layers.toolbar.document.forms[0].editThis.value
	else
		editValue = document.forms[0].editThis.value

	disableToolBarButtons();

	if ( parent.frames.searchtoolbar )
		disableSearchToolBarButtons();

	if ( parent.frames.reportframe )
		ReplaceFrame(GetReplacementUrl(editValue, scalingFactorValue));
}

///////////////////////////////////////////////////////////////////////////////


function getScalingFactorMenu()
{
	var str = "";
	var scalingFactorValue = readScalingFactorCookie();

	// Have to align the drop down select list to the top for MSIE on MacOS
	if (ns4)
	{
		str += "<select name=\"scalingFactor\" size=1 onChange =\"handleScalingFactorChange(this.form)\">";
	}
	else
	{
		if (g_browserType.name == "IE" && g_browserType.platform == "Mac")
			str += "<select align=\"top\" name=\"scalingFactor\" size=1 onChange =\"handleScalingFactorChange(this.form)\">";
		else
			str += "<select name=\"scalingFactor\" size=1 onChange =\"handleScalingFactorChange(this.form)\">";
	}

	for (var i = 0; i < g_scalingFactorValueList.length; i++)
	{
		if (scalingFactorValue == g_scalingFactorValueList[i])
			str += "<option value=" + '"' + g_scalingFactorValueList[i] + '"' + "selected>" + g_scalingFactorValueList[i] + "%"+"</option>";
		else
			str += "<option value=" + '"' + g_scalingFactorValueList[i] + '"' + ">" + g_scalingFactorValueList[i] + "%"+"</option>";
	}

	str += "</select>";
	return str;
}

/////////////////////////////////////////////////////////////////////////////////
function toolBuild()
{
	var l = this.layer;

	// Test if style sheets disabled
	if (!l) {
		alert(noStyle1 + noStyle2 + noStyle3);
		window.onError = null;
	}


 	if ( ns4 )
  	{
    	l.visibility		= "inherit";
    	l.bgColor			= this.bgColor;
    	l.top				= _top;
    	l.left				= 0;
	}
  	else
  	{
    	l.style.visibility		= "inherit";
    	l.classname				= "navbar";
    	l.style.top				= '0px';
    	l.style.left			= '0px';
  	}

	var cLeft, cRight, cTop, cBottom;

	cLeft = 0;
	cTop  = 0;

	if ( ns4 )
		cRight = 5000;
	else
		cRight = document.body.clientWidth;

	cBottom = _bottom;

	if ( ns4 )
	{
		l.clip.left		= cLeft;
		l.clip.right	= cRight;
		l.clip.top		= cTop;

		//toolbar size needs to be increased for Netscape on SunOS
		if (g_platform.indexOf("SunOS") != -1)
		{
			l.clip.bottom	= cBottom + 20;
		}
		else
		{
			l.clip.bottom	= cBottom + 4;
		}
	}
	else
	{
		// Increase the bottonm a wee bit to fit the toolbar frame for MSIE on MacOS,
		// and for Netscape 6
		if ( (g_browserType.name == "IE" && g_browserType.platform == "Mac")
								||
			 (g_browserType.name == "Netscape") )
		{
			cBottom = _bottom + 4;
		}

		l.style.width	= (cRight - cLeft) + 'px';
		l.style.height =  (cBottom - cTop) + 'px';
		l.style.clip		= "rect("+cTop+","+cRight+","+cBottom+","+cLeft+")";
	}

	var sFrmNavBar = getNavigationBarForm();

	if (ns4)
	{
		l.document.open();
		l.document.write(sFrmNavBar);
		l.document.close();
	}
	else
	{
		l.innerHTML = sFrmNavBar;
	}
}

///////////////////////////////////////////////////////////////////////////////

function Toolbar( layer )
{
	this.layer = layer;
	this.Name = "toolbar";
	this.toolBuildEx = toolBuild;
	this.zIndex = 0;
	this.bgColor = "#31699C";
	this.collapsedBgColor = "#31699C";
}

///////////////////////////////////////////////////////////////////////////////

function dbout( message )
{
	//alert( message );
}

///////////////////////////////////////////////////////////////////////////////

function getRecycledLayer( width, layerWindow )
{
	if ( ns4 )
	{
		var layer;
		dbout( 'in getRecycledLayer' );

		if ( layerWindow.name == 'reportframe' )
		{
			dbout( 'g_recycledReportLayersList.length = ' + g_recycledReportLayersList.length );

			if ( g_recycledReportLayersList.length )
			{
				dbout( 'using recycled layer' );
				layer = g_recycledReportLayersList[g_recycledReportLayersList.length-1];
				g_recycledReportLayersList.length--;
			}
			else
			{
				layer = new Layer( width, layerWindow );
			}
		}

		if ( layerWindow.name == 'searchframe' )
		{
		        if ( g_recycledSearchLayersList && g_recycledSearchLayersList.length )
			{
			        dbout( 'using recycled layer' );
				layer = g_recycledSearchLayersList[g_recycledSearchLayersList.length-1];
				g_recycledSearchLayersList.length--;
			}
			else
			{
			        layer = new Layer( width, layerWindow );
			}
		}

		return layer;
	}
}

///////////////////////////////////////////////////////////////////////////////

function recycleLayer( layerName, layerWindow, targetDocument )
{
	if ( ns4 )
	{
		var layerObject;
		layerObject = targetDocument.layers[layerName];

		dbout( 'in recycleLayer layerWindow.name = ' + layerWindow.name + ', layerObject = ' + layerObject );

		if ( layerObject )
		{
			if ( layerWindow.name == 'reportframe' )
			{
				g_recycledReportLayersList[g_recycledReportLayersList.length] = layerObject;
			}
			if ( layerWindow.name == 'searchframe' )
			{
				g_recycledSearchLayersList[g_recycledSearchLayersList.length] = layerObject;
			}

			targetDocument.layers[layerName] = null;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

function clearRecycledLayers( layerWindow )
{
	if ( ns4 )
	{
		if ( layerWindow.name == 'reportframe' )
		{
			for ( var index = 0; index < g_recycledReportLayersList.length; index++ )
			{
				delete g_recycledReportLayersList[g_recycledReportLayersList.length-index-1];
			}
			g_recycledReportLayersList.length = 0;
		}
		if ( layerWindow.name == 'searchframe' )
		{
			for ( var index = 0; index < g_recycledSearchLayersList.length; index++ )
			{
				delete g_recycledSearchLayersList[g_recycledSearchLayersList.length-index-1];
			}
			g_recycledSearchLayersList.length = 0;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////

function loadSearch()
{
		if ( parent.frames.searchframe )
		{
			if (g_browserType.replaceFrameBreaksHistoryList)
				parent.frames.searchframe.location.href = parent.g_searchFrameURL;
			else
				parent.frames.searchframe.location.replace(parent.g_searchFrameURL);
		}

		if ( parent.frames.searchtoolbar )
		{
			parent.frames.searchtoolbar.location.replace( parent.g_searchTBFrameURL );
		}
}

///////////////////////////////////////////////////////////////////////////////

function onLoad()
{
	if (!js_IsReportCompleted)
		editValue = "";

	// These global variables need to get initialized in
	// the onLoad() method, so that when the page is
	// reloaded the variables are correct reinitialized.
	ns4 = (document.layers) ? true:false;
	g_platform = navigator.platform;

	theDownloadWindow = null;
	parent.g_bToolBarFrameLoaded = false;

	g_reportFrame = eval( 'parent.frames.reportframe' );

	g_recycledReportLayersList = new Array();
	g_recycledSearchLayersList = new Array();
	populateScalingFactorValueList();

	if ( ns4 )
		tb = new Toolbar( document.layers.toolbar );
	else
		tb = new Toolbar( elementLocate(document, "toolbar") );

	tb.toolBuildEx();

	setCurrentButton( -1 );

	// preload some images needed in the report frame for searching
	preLoadImage( imgMarker );
	preLoadImage( imgHash );
	preLoadImage( imgBlank );
	if (!js_IsReportCompleted)
		changeClass( findButtonId( "toc" ), "disable" );
	else if ( parent.tocOpen )
		changeClass( findButtonId( "toc" ), "on" );
	else
		changeClass( findButtonId( "toc" ), "off" );

	parent.g_bToolBarFrameLoaded = true;

	if (ns4)
	{
		loadSearch();
	}
	else
	{
		if (g_browserType.cannotDynamicallyAdjustFrameset)
			loadSearch();
		else if (parent.g_commandName == "searchReport")
			loadSearch();
	}

	if (ns4)
	{
		if (parent.frames.tocframe)
			parent.frames.tocframe.location.replace( parent.g_tocFrameURL );
	}
	else
	{
		if (g_browserType.cannotDynamicallyAdjustFrameset && parent.frames.tocframe)
		{
			if (g_browserType.doesNotAlwaysLoadFrame)
				parent.frames.tocframe.location.replace("../viewer/blanknav.html");

			parent.frames.tocframe.location.replace( parent.g_tocFrameURL )
		}
	}
	
	if( !parent.g_bToolbarPageNumberSet)
	{

		if (parent.g_currentPageNumber)
		{
			hiddenValue = parent.g_currentPageNumber;
		}
		setPageNumber(hiddenValue);
		parent.g_bToolbarPageNumberSet = true;
	}
	
	if( parent.g_bReportFrameLoaded )
	{
		enableToolBarButtons();
	}
	else
	{
		if (g_browserType.replaceFrameBreaksHistoryList)
		{
			parent.frames.reportframe.location.href = parent.g_reportFrameURL;
		}
		else if (g_browserType.doesNotAlwaysLoadFrame && parent.frames.reportframe)
		{
			parent.frames.reportframe.location.replace( parent.g_reportFrameURL );
			enableToolBarButtons();
		}
	}



	if (!parent.stopRefresh && !parent.g_clickActionRefresh)
	{
		if(parent.g_locationStoreObject != null)
		{
	 		var index = parent.g_locationStoreObject.findAssociation(parent.g_objectURL); /*g_objectID*/
			if (index != -1)
			{
				hiddenValue = parent.g_locationStoreObject.getAssociationPage(index);
				setPageNumber(hiddenValue);
			}
	 		if (js_IsReportCompleted)
	 		{
	 			parent.stopRefresh = true;
	 		}
	 		scalingFactor = readScalingFactorCookie();
	 		ReplaceFrame(GetReplacementUrl(hiddenValue, scalingFactor));
	 	}
 	}

 	parent.g_clickActionRefresh = false;
 } // end onLoad()

function onUnload()
{
	if (theDownloadWindow)
		if (!theDownloadWindow.closed)
			theDownloadWindow.close();
}

/**
 * Changes the style of button
 */
function setClassName(linkObj, className)
{
	if (ns4)
	{
		var btnLayer = eval("document.layers[0].document.layers."+linkObj.name);
		var btnDoc = btnLayer.document;
		
		var btn = findButton(linkObj.name);
		if (btn)
		{
			btn.sClassName = className;
			btnDoc.open();
			btnDoc.write(btn.convertBtnToHtml());
			btnDoc.close();
		}
	}
	else
	{
		linkObj.className = className;
	}
}

/**
 * Changes the caption and style of button
 */
function changeCaptionAndStyle(linkObj, sCaptions, className)
{
	if (ns4)
	{
		var btnLayer = eval("document.layers[0].document.layers."+linkObj.name);
		var btnDoc = btnLayer.document;
		var btn = findButton(linkObj.name);

		if (btn)
		{
			if (btn.btnType == BTN_HAS_NOACTION)
			{
				btn.editValue = trim(btnDoc.forms[0].editThis.value);
			}
				
			btn.aCaptions = sCaptions;
			btn.sClassName = className;
			btnDoc.open();
			btnDoc.write(btn.convertBtnToHtml());
			btnDoc.close();
		}
	}
	else
	{
		linkObj.innerHTML=sCaptions;
	}
}

/**
 * Redirecting the Mouseout event of layer to link
 */
function layerOnMouseOut(thisObj, mouseevent, btnID)
{
	thisObj = thisObj.document.anchors[0];
	swapLinkClass(thisObj, mouseevent, btnID);
}

/**
 * Does Nothing
 */
function myVoid(){}

/**
 * returns Location of buttons according to the window's width
 */
function getBtnLocation(btnArray)
{

	if (ns4)
	{
		var _B_LETTER_WIDTH = 9;
		var _LETTER_WIDTH = 7;
		var _INTERVAL = 5;
		var _RETRIVE_CONTROL_INDEX = 6;
		var _RETRIVE_CONTROL_WIDTH = 205;
		var _MIN_STR_LENGTH = 4;

		var btnLocation = new Array(btnArray.length);
		var winWidth = window.innerWidth;
		var currentLocation = _INTERVAL;
		
		for (var i=0; i<btnArray.length; i++)
		{
			var btnCaption = btnArray[i].aCaptions;
			btnLocation[i] = currentLocation;
			
			var len_Cptn = btnCaption.length;
			len_Cptn = (len_Cptn >= _MIN_STR_LENGTH) ? len_Cptn : _MIN_STR_LENGTH;
			
			if (i == _RETRIVE_CONTROL_INDEX)
			{
				currentLocation = currentLocation + len_Cptn * _LETTER_WIDTH + _RETRIVE_CONTROL_WIDTH + _INTERVAL;
			}
			else
			{
				currentLocation = currentLocation + len_Cptn * _B_LETTER_WIDTH + _INTERVAL;
			}
		}

		// THIS NEED TO BE UNCOMMENTED AFTER TESTING IN ALL LOCALE
		/*if (currentLocation > winWidth)
		{
			for (var i=8; i<saCaptions.length; i++)
			{
				btnLocation[i] = currentLocation;
				var btnCaption = saCaptions[i];
				currentLocation = currentLocation + btnCaption.length * _LETTER_WIDTH + _INTERVAL;
			}
		}
		else
		{
			var lastTotalLenght = ((saCaptions[8].length + saCaptions[9].length) * _LETTER_WIDTH) + _INTERVAL * 3;
			var avlLenght = winWidth - currentLocation;
			
			if (avlLenght > lastTotalLenght)
			{
				var btnCaption = saCaptions[8];
				currentLocation = winWidth - lastTotalLenght;
				btnLocation[8] = currentLocation;
				
				btnCaption = saCaptions[9];
				currentLocation = (winWidth - (btnCaption.length * _LETTER_WIDTH + 2 * _INTERVAL));
				btnLocation[9] = currentLocation;
			}
			else
			{
				for (var i=8; i<saCaptions.length; i++)
				{
					btnLocation[i] = currentLocation;
					var btnCaption = saCaptions[i];
					currentLocation = btnCaption.length * _LETTER_WIDTH + _INTERVAL;
				}
			}
		}*/
		return btnLocation;
	}
	else
	{
		return new Array(5, 45, 85, 125, 165, 200, 250, 500, 560, 630, 700, 740);
	}
	
}
