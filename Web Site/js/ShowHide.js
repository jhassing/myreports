

//Used to show or Hide div tag selection item screen
function Image_Click(focusControl)
{			 				
	imageControl = focusControl.parentElement.firstChild;
	
	dataDiv = imageControl.parentElement.nextSibling;				
	
	if (dataDiv.style.display == "none")
	{
		dataDiv.style.display="block";
		imageControl.src="../images/minus.gif";
	}
	else
	{
		dataDiv.style.display="none";
		imageControl.src="../images/plus.gif";
	}				
}