<%@ Page language="c#" Inherits="DistributionPortal.Migrated_Login" CodeFile="Login.aspx.cs" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<HTML>
	<HEAD>

		<title>MyReports Login</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="css/allstyles.css" type="text/css" rel="stylesheet">
		<link href="css/loginstyles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		<!--
		
		function onBodyLoad()
		{
		
			var txtUserName		=	document.getElementById( "txtUserName" );
			var txtPassword		=	document.getElementById( "txtPassword" );
			
			var userNameLength	=	txtUserName.value.length;
			
			if ( userNameLength == 0 )
			{
				txtUserName.focus();		
			}
			else
			{
				txtPassword.focus();							
			}
		}

		function login_clicked()
		{
			// Hide any error text
			var FailureText		=	document.getElementById( "FailureText" );
			FailureText.style.display="none";
							
			var divUserName = document.getElementById( "divUserName" );
			divUserName.style.display="none";
			
			var divPassword = document.getElementById( "divPassword" );
			divPassword.style.display="none";						
			
			var txtUserName		=	document.getElementById( "txtUserName" );
			var txtPassword		=	document.getElementById( "txtPassword" );
			
			var userNameLength	=	txtUserName.value.length;
			var userPassLength	=	txtPassword.value.length;
			
			if ( userNameLength == 0 )
			{

				var divUserName = document.getElementById( "divUserName" );
				divUserName.style.display="inline";
				txtUserName.focus();
											
				return false;
			
			}
			
			if ( userPassLength == 0 ) 
			{
				var divPassword = document.getElementById( "divPassword" );
				divPassword.style.display="inline";
				txtPassword.focus();				
				
				return false;
			}
		}
		
		//-->
		</script>
	</HEAD>
	<body  id="AcBody" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0" onload="onBodyLoad()">
		<form id="frmLogin" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr class="banner">
					<td width="170" rowSpan="2"><img 
      style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px" 
      alt="" src="<%=Context.Request.ApplicationPath%>/images/MyReportsLogo.gif" 
      align=middle></td>
					<td align="left"><span class="bannerLabel" id="SystemNameKey">System:</span>&nbsp; <span class="bannerValue" id="SystemName">
							<%=MyReports.Common.AppValues.ActuateServerName%>
						</span>
					</td>
					<td align="right" rowSpan="2">&nbsp;&nbsp;</td>
				</tr>
				<tr class="banner" id="RowVersion">
					<td><span class="bannerLabel" id="labelcontrol1">Version:</span> <span class="bannerValue" id="LblVersion">
							<%=Global.PortalVersion%>
						</span>
					</td>
				</tr>
			</table>
			<table height="40" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td>
						<table height="40" cellSpacing="0" cellPadding="0" width="760" border="0">
							<tr>
								<td width="753" class="css0073">
									<asp:Label CssClass="css0074" runat="server" Text="LOGIN_SUB_HEADER" ID="LOGIN_SUB_HEADER" meta:resourcekey="LOGIN_SUB_HEADER" />
								</td>
							</tr>
						</table>
					</td>
					<td class="banner" width="100%">&nbsp;
					</td>
				</tr>
			</table>
			<table cellSpacing="1" cellPadding="1" width="770" bgColor="white" border="0">
				<tr>
					<td style="WIDTH: 400px" noWrap><br>

					</td>
					<td style="WIDTH: 330px" vAlign="top">
						<table style="BORDER-RIGHT: #261f9c 2px solid; MARGIN-TOP: 4px; BORDER-LEFT: #261f9c 2px solid; BORDER-BOTTOM: #261f9c 2px solid"
							cellSpacing="0" cellPadding="0" width="100%" bgColor="#d7e4f2" border="0">
							<tr vAlign="top">
								<td class="banner" vAlign="middle" noWrap colSpan="2" height="20">								   
									<asp:Label CssClass="css0002" runat="server" Text="SIGN_IN_HEADER" ID="SIGN_IN_HEADER" meta:resourcekey="SIGN_IN_HEADER" />
								</td>
							</tr>
							<tr>
								<td colspan="2" class="css0063"><div id="divUserName" style="DISPLAY:none">
										<asp:Label CssClass="css0062" runat="server" Text="SIGN_USER_ID_MISSING" ID="SIGN_USER_ID_MISSING" meta:resourcekey="SIGN_USER_ID_MISSING" />
									</div>
								</td>
							</tr>
							<tr>
								<td noWrap valign="top" class="css0058" align="right">
									<asp:Label  runat="server" Text="SIGN_USER_ID" ID="SIGN_USER_ID" meta:resourcekey="SIGN_USER_ID" />
								</td>
								<td noWrap valign="top" class="css0092">
									<asp:textbox Runat="server" id="txtUserName" maxlength="113" autocomplete="off" style="IME-MODE:disabled"
										CssClass="css0034" meta:resourcekey="txtUserNameResource1"></asp:textbox>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="css0063"><div id="divPassword" style="DISPLAY:none">
										<asp:Label CssClass="css0062" runat="server" Text="SIGN_USER_PASSWORD_MISSING" ID="SIGN_USER_PASSWORD_MISSING" meta:resourcekey="SIGN_USER_PASSWORD_MISSING" /></div>
								</td>
							</tr>
							<tr>
								<td noWrap valign="top" class="css0059" align="right">
									<asp:Label runat="server" Text="SIGN_USER_PASSWORD" ID="SIGN_USER_PASSWORD" meta:resourcekey="SIGN_USER_PASSWORD" />
								</td>
								<td noWrap valign="top" class="css0093">
									<asp:textbox id="txtPassword" MaxLength="64" Runat="server" TextMode="Password" autocomplete="off"
										style="IME-MODE:disabled" CssClass="css0034" meta:resourcekey="txtPasswordResource1"></asp:textbox>
								</td>
							</tr>
							<tr>
								<td noWrap valign="top" class="css0059" align="right"><label for="timeZone">Time Zone:</label></td>
								<td noWrap valign="top" class="css0093"><asp:dropdownlist id="AcLstTimeZone" runat="server" CssClass="css0034" meta:resourcekey="AcLstTimeZoneResource1"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td noWrap align="right" colSpan="2" class="css0079">
									<asp:imagebutton id="btnLogin" runat="server" ImageUrl="images/login.gif" meta:resourcekey="btnLoginResource1" OnClick="btnLogin_Click"></asp:imagebutton>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="css0063">
									<asp:label CssClass="css0062" id="FailureText" Runat="server" EnableViewState="False" meta:resourcekey="FailureText"></asp:label></td>
							</tr>
							<tr class="banner">
								<td colSpan="2" class="css0081">
									<asp:Label CssClass="css0082" runat="server" Text="SIGN_IN_HELP_TEXT" ID="SIGN_IN_HELP_TEXT" meta:resourcekey="SIGN_IN_HELP_TEXT" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="css0101">
						<asp:Label CssClass="css0102" runat="server" Text="SECURITY_DISCLAIMER_HEADER" ID="SECURITY_DISCLAIMER_HEADER" meta:resourcekey="SECURITY_DISCLAIMER_HEADER" />
					</td>
				</tr>
				<tr>
					<td colspan="2" class="css0103">
						<asp:Label CssClass="css0104" runat="server" Text="SECURITY_DISCLAIMER_TEXT" ID="SECURITY_DISCLAIMER_TEXT" meta:resourcekey="SECURITY_DISCLAIMER_TEXT" />
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
