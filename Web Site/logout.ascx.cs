/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Text;
	using System.Web.UI.HtmlControls;
	using activeportal.classes;
	

	/// <summary>
	///		Summary description for logout.
	/// </summary>
	public partial class logout : System.Web.UI.UserControl
	{

		protected actuate_i18n.AcHyperLinkControl AcLnkLogOut;

		private String m_logoutTarget = ""; 
		public String LogoutTarget
		{
			set { m_logoutTarget = value; }
			get { return m_logoutTarget; }
		}
			
		protected void Page_Load(object sender, System.EventArgs e)
		{
			AcUrlBuilder target = new AcUrlBuilder( m_logoutTarget );

			//target.AddKeyValue( AcParams.Name.serverURL, AcParams.Get( Page, AcParams.Name.serverURL ) ); 
			//target.AddKeyValue( AcParams.Name.userID, AcParams.Get( Page, AcParams.Name.userID ) );
			//target.AddKeyValue( AcParams.Name.volume, AcParams.Get( Page, AcParams.Name.volume ) );
			//target.AddKeyValue( AcParams.Name.logout, "true" );

			Session.Abandon( );

            Session.Clear();
            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();

            GC.Collect();

			Response.Redirect( target.ToString( ) );
			
			
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
