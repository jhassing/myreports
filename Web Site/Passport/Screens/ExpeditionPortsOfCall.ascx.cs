
using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.ComponentModel;

using System.Web.SessionState;
using System.Web.UI;

using Compass.Common;

using Passport.BLL;

public partial class ExpeditionPortsOfCall : basePassportUserControl
{

    private const string newLine = "<br>";
    protected System.Web.UI.WebControls.Label Label2;
    protected System.Web.UI.WebControls.Label Label3;
    protected System.Web.UI.WebControls.Label Label4;
    protected System.Web.UI.WebControls.Label Label5;
    protected System.Web.UI.WebControls.Label Label6;
    protected System.Web.UI.WebControls.Label Label7;
    protected System.Web.UI.WebControls.Label Label8;
    protected System.Web.UI.WebControls.Label Label9;
    protected System.Web.UI.WebControls.Label Label10;
    protected System.Web.UI.WebControls.Label Label11;
    protected System.Web.UI.WebControls.Label Label12;
    protected System.Web.UI.WebControls.Label Label13;

    public event Delegates.ActivateAssociateClicked ActivateAssociate_OnClick;
    private int[] _teamPocCount;


    #region Properties

    public string[] pocCount
    {
        get
        {
            return (string[])ViewState["pocCount"];
        }
        set
        {
            ViewState["pocCount"] = value;
        }
    }

    public ArrayList CompletedPortsOfCall
    {
        get
        {
            return ViewState["CompletedPortsOfCall"] != null ? (ArrayList)ViewState["CompletedPortsOfCall"] : new ArrayList();
        }
        set
        {
            ViewState["CompletedPortsOfCall"] = value;
        }
    }


    #endregion

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        //if (Visible)
        this.pocCount = new string[10];
        this._teamPocCount = new int[10];
    }

    #region Save Button clicked

    protected void btnSave_Click(object sender, System.EventArgs e)
    {
        // Get the original ArrayListing back from the page viewstate
        ArrayList ary = this.CompletedPortsOfCall;
        
        int count = Repeater2.Items.Count;

        // Loop through each row of the repeater
        for (int i = 0; i < count; i++)
        {
            // The ArrayList we will pass into the function to update the database
            ArrayList aryPortsOfCall = new ArrayList();

            // Container for the Ports Of call to be removed
            ArrayList aryPortsOfCallToBeRemoved = new ArrayList();

            string message = "";
            bool isDirty = false;
            DateTime portOfCall;

            // Get the PersonnelNumber ID for the database
            int assocID = System.Convert.ToInt32(((Label)rptNames.Items[i].FindControl("lblPrimaryKey")).Text);

            // Loop through each Port Of Call field
            for (int y = 1; y < 11; y++)
            {
                if ("1" == ((TextBox)Repeater2.Items[i].FindControl("txtPortCall" + y.ToString() + "_State")).Text)
                {	// This fields text has changed

                    // Get the NEW text from the box
                    string temp = ((TextBox)Repeater2.Items[i].FindControl("txtPortCall" + y.ToString())).Text;

                    if (temp.Length > 0)
                    {
                        if (Functions.IsDate(temp))
                        {
                            //bool doAdd = false;

                            // Current textbox listing of the PortsOfCall
                            portOfCall = System.Convert.ToDateTime(temp);
    
             /*    if (((tAssociateOperation)ary[i]).PortsOfCall.Count > (y - 1))
                            {
                                // Get the original date
                                DateTime existingPOC = ((tPortOfCall)((tAssociateOperation)ary[i]).PortsOfCall[y - 1]).DateCompleted;

                                // Compare the 2 dates
                                doAdd = !portOfCall.Equals(existingPOC);

                            }
                            else
                                doAdd = true;
            */

                            // if the existing date and the new date are NOT the same then flag to be changes
                        //if (doAdd)
                                aryPortsOfCall.Add(new tPortOfCall(y, portOfCall));

                        }
                        else
                        {
                            // This is an invalid date.
                            isDirty = true;
                            message += InvalidDateMessage(y);

                        }
                    }
                    else
                    {
                        // The date was at one point here, but is not any longer. Delete that record.
                        aryPortsOfCallToBeRemoved.Add(y);
                    }
                }
            }

            // First, save the dates that are in the correct format
            AssociatePortsOfCall oAssociatePortsOfCall = new AssociatePortsOfCall(MyReports.Common.AppValues.Passport_DB_ConnectionString);

            if (aryPortsOfCall.Count > 0)
                oAssociatePortsOfCall.InsertAssociatesCompletedPortsOfCall(assocID, this.ExpeditionID, this.OperationNumber, this.UserNetworkID, aryPortsOfCall);

            if (aryPortsOfCallToBeRemoved.Count > 0)
                oAssociatePortsOfCall.RemoveAssociateCompletedPortsOfCall(assocID, this.ExpeditionID, this.OperationNumber, aryPortsOfCallToBeRemoved);


            // Now, display an error showing the user the dates are invalid
            if (isDirty)
            {
                // Data is dirty, display the red message on the screen
                this.lblMessage.Visible = true;
                this.lblMessage.Text = message;
                this.lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }

        //Reload the data, and store inside the view state
        //LoadData();

    }


    #endregion

    #region LoadData

    public void LoadData()
    {
        AssociatePortsOfCall oo = new AssociatePortsOfCall(MyReports.Common.AppValues.Passport_DB_ConnectionString);

        ArrayList ary = oo.GetAssociateCompletedPortsOfCall(this.ExpeditionID, this.OperationNumber);

        //init or reset _teamPocCount array
        //for (int i = 0; i < _teamPocCount.Length; i++)
        //{
        //    this._teamPocCount[i] = 0;
        //}//end init _teamPocCount array

        // Place the data inside the ViewState of the page
        this.CompletedPortsOfCall = ary;
        
        rptNames.DataSource = ary;
        rptNames.DataBind();

        Repeater2.DataSource = ary;
        Repeater2.DataBind();

    }


    #endregion

    #region Invalid Date Message

    private string InvalidDateMessage(int poc)
    {
        return "The date/time for Port of Call " + poc.ToString() + " is not in the corret format." + newLine;
    }

    #endregion

    #region Repeater Item Data bound

    protected void Repeater2_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem ||
                e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.SelectedItem)
            {
                tAssociateOperation oMyStruct = (tAssociateOperation)e.Item.DataItem;

                // Loop through the Ports of call, add the onChange event
                for (int x = 1; x < 11; x++)
                {
                    TextBox txtPort = (TextBox)e.Item.FindControl("txtPortCall" + x.ToString());
                    TextBox txtPortState = (TextBox)e.Item.FindControl("txtPortCall" + x.ToString() + "_State");

                    txtPortState.Style.Add("display", "none");
                    txtPortState.Text = "";

                    txtPort.Attributes.Add("onchange", "doChange(" + txtPortState.ClientID + ");");
                    txtPort.Attributes.Add("onblur", "LostFocus( this )");
                    txtPort.Attributes.Add("onkeydown", "KeyDown()");

                    if (oMyStruct.ActiveFlag)
                        txtPort.Style.Add("background-color", "lightgray");
                }

                ArrayList aryPortsOfCall = oMyStruct.PortsOfCall;

                // Now, loop through each port of call that was returned from the database and 
                // Populate the value
                for (int x = 0; x < aryPortsOfCall.Count; x++)
                {
                    tPortOfCall oPortOfCall = (tPortOfCall)aryPortsOfCall[x];

                    TextBox txtPort = (TextBox)e.Item.FindControl("txtPortCall" + oPortOfCall.PortOfCallNumber);

                    if (txtPort != null)
                    {
                        txtPort.Text = oPortOfCall.DateCompleted.ToString("MM/dd/yyyy");

                        this._teamPocCount[oPortOfCall.PortOfCallNumber-1]++;

                        //int t = System.Convert.ToInt32(this.pocCount[oPortOfCall.PortOfCallNumber -1]);
                        //t++;
                        //this.pocCount[oPortOfCall.PortOfCallNumber] = t.ToString();
                        
                    }

                }

                #region Percent

                TextBox txtPercent = (TextBox)e.Item.FindControl("txtPercentComplete");
                txtPercent.Text = aryPortsOfCall.Count * 10 + "%";
                txtPercent.ForeColor = System.Drawing.Color.Red;
                txtPercent.ReadOnly = true;

                #endregion

            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {

                for (int i = 1; i <= 10; i++)
                {
                    // Set the bottom percentage
                    Label lblPort = (Label)e.Item.FindControl("lblPort" + i.ToString() + "Percent");

                    int m_teamPocPerecentComplete = 0;

                    if (this.CompletedPortsOfCall.Count > 0)
                    {
                        m_teamPocPerecentComplete = Convert.ToInt16((double)this._teamPocCount[i - 1] * 100D / (double)this.CompletedPortsOfCall.Count);
                    }

                    lblPort.Text = m_teamPocPerecentComplete.ToString() + "%";

                    //                    if (lblPort != null)
                    //                    {
                    //                        //lblPort.Text = System.Convert.ToInt32(this.pocCount[i - 1]).ToString() + "%";
                    //                        float m_teamPocPerecentComplete = (float)this._teamPocCount[i-1]*100F/(float)this.CompletedPortsOfCall.Count;
                    //                        lblPort.Text = m_teamPocPerecentComplete.ToString() + "%";
                    //                    }
                }
            }
 
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    #endregion

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Visible)
        {
            LoadData();

            PortsOfCallListing1.LoadExpeditions(base.ExpeditionID);
        }
    }

    protected void btnActivateAssociate_Click(object sender, System.EventArgs e)
    {
        // Bubble the event to the main page

    }

    protected void rptNames_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {


        if (e.Item.ItemType == ListItemType.AlternatingItem ||
            e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.SelectedItem)
        {
            tAssociateOperation oMyStruct = (tAssociateOperation)e.Item.DataItem;

            #region  Associate Name

            TextBox txtAssociateName = (TextBox)e.Item.FindControl("txtAssociateName");
            txtAssociateName.Text = oMyStruct.associateName;
            txtAssociateName.ForeColor = System.Drawing.Color.Blue;
            txtAssociateName.ReadOnly = true;

            #endregion

            #region  Associate Personnel Number

            TextBox txtAssociatePersonnel = (TextBox)e.Item.FindControl("txtAssociatePersonnel");
            txtAssociatePersonnel.Text = oMyStruct.AssocPerNum;
            txtAssociatePersonnel.ForeColor = System.Drawing.Color.Blue;
            txtAssociatePersonnel.ReadOnly = true;

            #endregion

            #region  Primary Key

            Label lblPrimaryKey = (Label)e.Item.FindControl("lblPrimaryKey");
            lblPrimaryKey.Text = oMyStruct.AssocID.ToString();
            lblPrimaryKey.Visible = false;

            #endregion
        }
    }
}