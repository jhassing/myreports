<%@ Control Language="c#" Inherits="ExpeditionListing" CodeFile="ExpeditionListing.ascx.cs" %>
<br>
<table>
	<tr>
		<td colspan="3" Class="titlebar">
			<label for="securityDisclaimer" class="css0102">
				<%=Global.ResourceValue( "Expedition Listing" )%>
			</label>
		</td>
	</tr>
	<asp:repeater id="Repeater4" runat="server" OnItemDataBound="Repeater4_ItemDataBound">
		<ItemTemplate>
			<tr>
				<td>&nbsp;&nbsp;</td>
				<td width="200px">
					<asp:LinkButton id="btnExpName" CssClass="hyperlink" runat="server" OnCommand="expLnkButton_Command"></asp:LinkButton>
				</td>
				<td width="400px">
					<asp:Label id="lblExpDesc" CssClass="XX-Small" runat="server"></asp:Label>
				</td>
			</tr>
		</ItemTemplate>
	</asp:repeater>
</table>
