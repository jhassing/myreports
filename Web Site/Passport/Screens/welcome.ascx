<%@ Control language="c#" Inherits="PassportWelcome" CodeFile="Welcome.ascx.cs" %>
<table>
	<tr>
		<td nowrap>
			<asp:Label id="Label1" runat="server" Font-Bold="True" Font-Size="Large">Passport News</asp:Label>&nbsp;&nbsp;
		</td>
		<td width="100%">
			<asp:Button id="Button1" CssClass="MyReportsAdminConsoleButton" runat="server" Text="Expeditions"
				Width="104px" onclick="Button1_Click" CommandName="ShowExpeditionsScreen"></asp:Button>
		</td>
		<td>
			<asp:Button id="Button2" CssClass="MyReportsAdminConsoleButton" runat="server" Text="Transfer Associates"
				Visible="False" onclick="Button2_Click"></asp:Button>
		</td>
		<td>
			<asp:Button id="Button3" CssClass="MyReportsAdminConsoleButton" runat="server" Text="Add a New Operation"
				Visible="False"></asp:Button>
		</td>
	</tr>
	<tr>
		<td colspan="4"><br>
			<asp:Label id="Label2" runat="server" Font-Bold="True" ForeColor="Red" Width="488px">Congratulations ! You have successfully signed into the application.</asp:Label>
		</td>
	</tr>
	<tr>
		<td colspan="4"><br>
			<asp:Label id="Label3" runat="server" Font-Bold="True" Width="160px">Welcome to Passport !</asp:Label>
		</td>
	</tr>
	<tr>
		<td colspan="4"><br>
			<asp:Label id="Label4" runat="server" Width="660px">This is the new Training and Education Tracking System for Compass Group Managers. This application will allow you the ability to keep track of your Associates throughout their career with Compass Group.</asp:Label>
		</td>
	</tr>
</table>
