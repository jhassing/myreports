using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

using Passport.BLL;



public partial class ExpeditionListing : basePassportUserControl
{

    #region LoadData

    public void LoadData()
    {
        // Load the data from the database
        Expedition oExpeditions = new Expedition(MyReports.Common.AppValues.Passport_DB_ConnectionString);

        Repeater4.DataSource = oExpeditions.GetExpeditionListing();
        Repeater4.DataBind();
    }

    #endregion

    protected void Repeater4_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem ||
            e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.SelectedItem)
        {
            tExpeditions oExp = (tExpeditions)e.Item.DataItem;

            // Name
            LinkButton expLnkButton = (LinkButton)e.Item.FindControl("btnExpName");
            expLnkButton.Text = oExp.ExpeditionName;
            expLnkButton.CommandName = "ShowPortsOfCallScreen";
            expLnkButton.Command += new CommandEventHandler(expLnkButton_Command);
            expLnkButton.CommandArgument = oExp.ExpeditionName + "|" + oExp.ExpeditionID.ToString();

            // Description
            Label expDescLbl = (Label)e.Item.FindControl("lblExpDesc");
            expDescLbl.Text = oExp.Description;
        }
    }

    protected override bool OnBubbleEvent(object source, EventArgs args)
    {
        return base.OnBubbleEvent(source, args);
    }

    protected void expLnkButton_Command(object sender, CommandEventArgs e)
    {
        RaiseBubbleEvent(sender, e);
        
    }
}