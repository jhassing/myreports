using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Passport.BLL;

public partial class AssociateListing : basePassportUserControl
{
    protected System.Web.UI.WebControls.CheckBox chkAdd;
    protected System.Web.UI.WebControls.Label lblPersonnelNumber;

    public event Delegates.SaveAssociateActiveSettingsClicked SaveAssociateActiveSettings_OnClick;


    #region Properties

    #region Search Compass Active  Checkbox

    public bool SearchCompassActive
    {
        get
        {
            return ViewState["SearchCompassActive"] != null ? System.Convert.ToBoolean(ViewState["SearchCompassActive"]) : true;
        }
        set
        {
            ViewState["SearchCompassActive"] = value;
        }
    }

    #endregion

    #region Search Compass Non-Active  Checkbox

    public bool SearchCompassNonActive
    {
        get
        {
            return ViewState["SearchCompassNonActive"] != null ? System.Convert.ToBoolean(ViewState["SearchCompassNonActive"]) : false;
        }
        set
        {
            ViewState["SearchCompassNonActive"] = value;
        }
    }

    #endregion

    #region Search Non-Compass Active  Checkbox

    public bool SearchNonCompassActive
    {
        get
        {
            return ViewState["SearchNonCompassActive"] != null ? System.Convert.ToBoolean(ViewState["SearchNonCompassActive"]) : true;
        }
        set
        {
            ViewState["SearchNonCompassActive"] = value;
        }
    }

    #endregion

    #region Search Non-Compass Non-Active  Checkbox

    public bool SearchNonCompassNonActive
    {
        get
        {
            return ViewState["SearchNonCompassNonActive"] != null ? System.Convert.ToBoolean(ViewState["SearchNonCompassNonActive"]) : false;
        }
        set
        {
            ViewState["SearchNonCompassNonActive"] = value;
        }
    }

    #endregion

    public ArrayList AssociatesListing
    {

        set
        {
            ViewState["AssociatesListing"] = value;
        }
        get
        {
            return ViewState["AssociatesListing"] != null ? (ArrayList)ViewState["AssociatesListing"] : null;
        }
    }

    public tAssociates SingleAssociate
    {
        set
        {
            ViewState["SingleAssociate"] = value;
        }
        get
        {
            return (tAssociates)ViewState["SingleAssociate"];
        }
    }


    public bool AddNewUser
    {
        get
        {
            return ViewState["AddNewUser"] != null ? System.Convert.ToBoolean(ViewState["AddNewUser"]) : false;

        }
        set
        {
            ViewState["AddNewUser"] = value;
        }
    }


    public int ShowPanel
    {

        get
        {
            return ViewState["ShowPanel"] != null ? System.Convert.ToInt32(ViewState["ShowPanel"]) : 0;
        }
        set
        {
            ViewState["ShowPanel"] = value;

            switch (value)
            {
                case 1:
                    this.panAssociateListing.Visible = true;
                    this.panUserDetails.Visible = false;
                    break;

                case 2:
                    this.panAssociateListing.Visible = false;
                    this.panUserDetails.Visible = true;
                    break;
            }

        }
    }

    #endregion

    protected void Page_Load(object sender, System.EventArgs e)
    {
        // Put user code to initialize the page here
    }

    #region LoadData

    public void LoadData()
    {

        // Load the data from the database
        Associate oAssociate = new Associate(General.Passport_DB_ConnectionString);

        ArrayList ary = oAssociate.GetAssociates_ByOperationsNumber(this.OperationNumber, base.ExpeditionID);

        AssociateList.DataSource = ary;
        AssociateList.DataBind();

        // Place the data inside the ViewState of the page
        this.AssociatesListing = ary;
    }


    #endregion

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.AssociateList.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.AssociateList_ItemDataBound);
        this.PreRender += new System.EventHandler(this.AssociateListing_PreRender);

    }
    #endregion

    #region PreRender

    protected void AssociateListing_PreRender(object sender, EventArgs e)
    {
        if (Visible)
        {
            LoadData();
        }
    }

    #endregion

    #region Associate Item Data Bound

    private void AssociateList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem ||
            e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.SelectedItem)
        {
            System.Drawing.Color orange = System.Drawing.Color.Orange;
            System.Drawing.Color red = System.Drawing.Color.Red;
            System.Drawing.Color green = System.Drawing.Color.Green;
            System.Drawing.Color black = System.Drawing.Color.Black;

            // Get a reference to the data
            tAssociates oExp = (tAssociates)e.Item.DataItem;

            // First, we need to get the personnel number of this user
            string personnelNumber = oExp.PersonnelNumber;

            // Get the fields from the client front page
            CheckBox chkActive = (CheckBox)e.Item.FindControl("chkActive");
            Label lblFirstName = (Label)e.Item.FindControl("lblFirstName");
            Label lblPersonnelNumber = (Label)e.Item.FindControl("lblPersonnelNumber");
            Label lblStatus = (Label)e.Item.FindControl("lblStatus");
            LinkButton lnkPersonnelNumber = (LinkButton)e.Item.FindControl("lnkPersonnelNumber");
            LinkButton lnkSetInActive = (LinkButton)e.Item.FindControl("lnkSetInActive");



            // Name field text				
            lblFirstName.Text = oExp.LastName + ", " + oExp.FirstName;

            if (oExp.CompassAssociate)
            {

                #region Compass Employee

                // this is a compas employee, show only a label, not a link
                lnkPersonnelNumber.Visible = false;
                lblPersonnelNumber.Visible = true;

                // Do not show the set inactive link
                lnkSetInActive.Visible = false;

                // Display the personnel number
                lblPersonnelNumber.Text = personnelNumber;

                if (oExp.ActiveFlag)
                {
                    // the associate is a compass employee and is currently active
                    chkActive.ForeColor = black;
                    lblFirstName.ForeColor = black;
                    lblPersonnelNumber.ForeColor = black;
                    lnkPersonnelNumber.ForeColor = black;
                    lnkSetInActive.ForeColor = black;
                    lblStatus.ForeColor = black;
                    lblStatus.Text = "Compass Employee: Active";

                }
                else
                {
                    // associate is a compass employee and is currently IN-active
                    chkActive.ForeColor = orange;
                    lblFirstName.ForeColor = orange;
                    lblPersonnelNumber.ForeColor = orange;
                    lnkPersonnelNumber.ForeColor = orange;
                    lnkSetInActive.ForeColor = orange;
                    lblStatus.ForeColor = orange;
                    lblStatus.Text = "Compass Employee: In-Active";

                }

                #endregion

            }
            else
            {

                #region Non-Compass Employee

                // this is a non compass employee.					
                lnkPersonnelNumber.Visible = true;
                lblPersonnelNumber.Visible = false;
                lnkPersonnelNumber.Text = personnelNumber;
                lnkPersonnelNumber.Command += new CommandEventHandler(lnkPersonnelNumber_Command);
                lnkPersonnelNumber.CommandName = "ViewSingleAssoc";
                lnkPersonnelNumber.CommandArgument = oExp.AssocID.ToString();

                // Display the Setinactive link
                lnkSetInActive.Visible = true;
                lnkSetInActive.Command += new CommandEventHandler(lnkSetInActive_Command);
                lnkSetInActive.CommandArgument = oExp.AssocID.ToString();
                lnkSetInActive.CommandName = oExp.ActiveFlag.ToString();

                if (oExp.ActiveFlag)
                {
                    // the associate is a non compass employee and is currently active
                    chkActive.ForeColor = green;
                    lblFirstName.ForeColor = green;
                    lblPersonnelNumber.ForeColor = green;
                    lnkPersonnelNumber.ForeColor = green;
                    lnkSetInActive.ForeColor = green;
                    lblStatus.ForeColor = green;
                    lblStatus.Text = "Non-Compass Employee: Active";

                    // Active Link
                    lnkSetInActive.Text = "Set Inactive";

                }
                else
                {
                    // associate is a non compass employee and is currently IN-active
                    chkActive.ForeColor = red;
                    lblFirstName.ForeColor = red;
                    lblPersonnelNumber.ForeColor = red;
                    lnkPersonnelNumber.ForeColor = red;
                    lnkSetInActive.ForeColor = red;
                    lblStatus.ForeColor = red;
                    lblStatus.Text = "Non-Compass Employee: In-Active";

                    // Active Link
                    lnkSetInActive.Text = "Set Active";

                }

                #endregion

            }
        }
    }


    #endregion

    protected void lnkPersonnelNumber_Command(object sender, CommandEventArgs e)
    {
        // The personnel number was clicked.
        // Hide the first panel, and show the 2nd.
        this.ShowPanel = 2;

        // Set the page state
        this.AddNewUser = false;

        // Get the users details from the SQL Server

        tAssociates oUser = new Associate(MyReports.Common.AppValues.Passport_DB_ConnectionString).GetAssociateByAssocID(System.Convert.ToInt32(e.CommandArgument.ToString()));

        // Save the data for use later
        this.SingleAssociate = oUser;

        // Populate the text fields
        this.txtFirstName.Text = oUser.FirstName;
        this.txtLastName.Text = oUser.LastName;
        this.txtPersonnelNumber.Text = oUser.PersonnelNumber;
        this.txtUnitNumber.Text = oUser.OperationNumber.ToString();


    }

    protected void Button1_Click(object sender, System.EventArgs e)
    {
        // Was this an addition or modification? Check the page viewstate
        int operationNumber = System.Convert.ToInt32(this.txtUnitNumber.Text);
        string fName = this.txtFirstName.Text.Trim();
        string lName = this.txtLastName.Text.Trim();
        string perNum = this.txtPersonnelNumber.Text.Trim();

        if (this.AddNewUser)
        {
            // Add this user as a new one
            new Associate(MyReports.Common.AppValues.Passport_DB_ConnectionString).InsertNewAssociate(
                                operationNumber, perNum, fName, lName, true, false);

        }
        else
        {
            // This is a modification

            // Get the AssocID
            int assocID = this.SingleAssociate.AssocID;

            // Update the associate record.
            new Associate(MyReports.Common.AppValues.Passport_DB_ConnectionString).ModifyAssociate(
                                            assocID, operationNumber, fName, lName);



        }

        // Show the proper panels
        this.ShowPanel = 1;

    }

    protected void Button2_Click(object sender, System.EventArgs e)
    {
        // Loop through the listing, find out which checkboxes have changed.
        for (int x = 0; x < this.AssociatesListing.Count; x++)
        {
            bool pageChecked = ((CheckBox)this.AssociateList.Items[x].FindControl("chkActive")).Checked;
            //bool previousChecked =	((tAssociates)this.AssociatesListing[x]).ActiveForExpedition;

            //if (  pageChecked != previousChecked)
            //{
            // State has changed, send to the database 
            //	Associate oAssoc = new Associate( MyReports.Common.AppValues.Passport_DB_ConnectionString );
            //	oAssoc.SetAssociateActive( ((tAssociates)this.AssociatesListing[x]).AssocID, this.ExpeditionID, pageChecked ); 					
            //}
        }

        // Raise the event to the parent page so he can navigate accordingly
        if (SaveAssociateActiveSettings_OnClick != null)
            SaveAssociateActiveSettings_OnClick();

    }

    protected void btnAddNewUser_Click(object sender, System.EventArgs e)
    {
        // Flag the page viewstate.
        this.AddNewUser = true;

        // Show the single user screen
        this.ShowPanel = 2;

        // Clear the First and Last name fields
        this.txtFirstName.Text = "";
        this.txtLastName.Text = "";

        // Get the next available personnel number
        this.txtPersonnelNumber.Text = new Associate(MyReports.Common.AppValues.Passport_DB_ConnectionString).GenerateNewPersonnelNumber();

        // Load the current Operations Number
        this.txtUnitNumber.Text = this.OperationNumber.ToString();
    }

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        // Show the proper panels
        this.ShowPanel = 1;
    }


    protected void lnkSetInActive_Command(object sender, CommandEventArgs e)
    {
        int AssocID = System.Convert.ToInt32(e.CommandArgument.ToString().Trim());
        bool currentlyActive = System.Convert.ToBoolean(e.CommandName.ToString());

        Associate oAssociate = new Associate(MyReports.Common.AppValues.Passport_DB_ConnectionString);
        oAssociate.SetAssociateActive(AssocID, !currentlyActive);




    }

    protected void btnRefresh_Click(object sender, System.EventArgs e)
    {

    }
}