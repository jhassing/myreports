<%@ Register TagPrefix="cc1" Namespace="AegisControls" Assembly="AegisControls" %>
<%@ Register TagPrefix="uc1" TagName="PortsOfCallListing" Src="../Common/PortsOfCallListing.ascx" %>
<%@ Control Language="c#" Inherits="ExpeditionPortsOfCall" CodeFile="ExpeditionPortsOfCall.ascx.cs" %>
<script language="javascript">
<!--
function ValidateUSDate(p_strValue)
{
	/************************************************
	DESCRIPTION: Validates that a string contains only
		valid dates with 2 digit month, 2 digit day,
		4 digit year. Date separator can be ., -, or /.
		Uses combination of regular expressions and
		string parsing to validate date.
		Ex. mm/dd/yyyy or mm-dd-yyyy or mm.dd.yyyy

	PARAMETERS:
	p_strValue - String to be tested for validity

	RETURNS:
	True if valid, otherwise false.

	REMARKS:
	Avoids some of the limitations of the Date.parse()
	method such as the date separator character.
	*************************************************/
	var objRegExp = /^\d{1,2}(\-|\/|\.)\d{1,2}\1\d{4}$/

	// Check to see if in correct format
	if (!objRegExp.test(p_strValue))
	{
		alert("Date is not in a valid format: " + p_strValue);
		return false; //doesn't match pattern, bad date
	}
	else
	{
		var strSeparator = p_strValue.substring(2, 3) //find date separator
		if ( (strSeparator != '/') && (strSeparator != '.') && (strSeparator != '-') )
		{
			var strSeparator = p_strValue.substring(1, 2) //find date separator
		}
		
	
			
		var arrayDate = p_strValue.split(strSeparator); //split date into month, day, year

		// Create a lookup for months not equal to Feb.
		var arrayLookup = { '01' : 31, '03' : 31, '04' : 30, '05' : 31, '06' : 30, '07' : 31,
							'08' : 31, '09' : 30, '10' : 31, '11' : 30, '12' : 31}
		//var intDay = parseInt(arrayDate[1]);
		var intDay = arrayDate[1];

		// Check if month value and day value agree
		if(arrayLookup[arrayDate[0]] != null)
		{	
			if ((intDay <= arrayLookup[arrayDate[0]]) && (intDay != 0))
				return true; //found in lookup table, good date
		}

		// Check for February
		var intYear = parseInt(arrayDate[2]);
		var intMonth = parseInt(arrayDate[0]);

		if (parseInt(intMonth) == 2)
		{
			if (((((intYear % 4 == 0) && (intDay <= 29)) || ((intYear % 4 != 0) && (intDay <= 28)))) && (intDay != 0))
				return true; //Feb. had valid number of days
			else
				alert("Invalid February date: " + p_strValue);
		}
	}
	
	alert("Date is not in a valid format: " + p_strValue);
	
	return false; //any other values, bad date
}



function LostFocus( box )
{
	if ( box.value != '' )
	{
		if ( !ValidateUSDate( box.value ) )
		{
			// Failed
			box.value = '';
			box.focus();
		}
	}

}

function KeyDown()
{
	if (	((event.keyCode > 47) && (event.keyCode < 58))	||			// 0 through 9
			((event.keyCode > 95) && (event.keyCode < 106)) ||			// 0 through 9 on the NUM keypad
			(event.keyCode == 191)		||								// Slash (/)
			(event.keyCode == 9)		||								// Tab key
			(event.keyCode == 8)		||								// Back Space
			(event.keyCode == 111)		||								// Slash (/) ( from the NUM keypad )
			(event.keyCode == 37)		||								// Left arrow
			(event.keyCode == 39)		||								// Right arrow
			(event.keyCode == 46) )										// Delete Key
	{
		// Good key
		//alert ( event.keyCode );
	}
	else
	{				
		// otherwise, bad key
		event.returnValue=false;

		
	}

}

		function doChange( txtBox )
		{		
			txtBox.value = "1";									
		}


//-->
</script>
<br>
<table cellpadding="0" border="0" cellspacing="1" width="90%">
	<tr>
		<td Class="titlebar" colspan="2" width="100%">
			<uc1:PortsOfCallListing id="PortsOfCallListing1" runat="server"></uc1:PortsOfCallListing></td>
	</tr>
	<tr>
		<td colspan="2">
			<hr>
		</td>
	</tr>
</table>

<table cellpadding="0" border="0" cellspacing="1" width="90%">
	<tr>
		<td align="left" valign="top"  width="200px">
			<table cellpadding="0" border="0" cellspacing="1">
				<tr>
					<td nowrap width="100%" valign=bottom height=26px>
						<asp:Label CssClass="XX-Small" id="Label1" runat="server">Active Associates</asp:Label>
					</td>
					<td nowrap valign=bottom height=26px>
						<asp:Label CssClass="XX-Small" id="headLabel2" runat="server">Personnel #</asp:Label>
					</td>
				</tr>
				<asp:repeater id="rptNames" runat="server" OnItemDataBound="rptNames_ItemDataBound">
					<ItemTemplate>
						<tr>
							<td>
								<asp:TextBox CssClass="XX-Small" Width="100%" id="txtAssociateName"  runat="server"></asp:TextBox>
							</td>
							<td>
								<asp:TextBox CssClass="XX-Small" id="txtAssociatePersonnel" style="center" width="70px" runat="server"></asp:TextBox>
							</td>
						    <td width="0px">
						        <asp:Label Width="0px" Visible="False" CssClass="XX-Small" id="lblPrimaryKey" runat="server"></asp:Label>								
							</td>							
						</tr>
					</ItemTemplate>
				</asp:repeater>
			</table>
		</td>
		<td align="left" valign="top">
			<div STYLE="OVERFLOW: scroll; width:600px; HEIGHT: 100%; TEXT-ALIGN: left">
				<table cellpadding="0" border="0" cellspacing="1" style="PADDING-RIGHT: 2px; PADDING-BOTTOM: 0px">
					<tr>
						<td valign=bottom>
							<asp:Label CssClass="XX-Small" id="headLabel3" width="75px" runat="server">% Complete</asp:Label>
						</td>
						<td align=center>
							<asp:Label CssClass="XX-Small" id="headLabel4" width="75px" runat="server">Port of <br /> Call 1</asp:Label>
						</td>
						<td align=center>
							<asp:Label CssClass="XX-Small" id="headLabel5" width="75px" runat="server">Port of <br /> Call 2</asp:Label>
						</td>
						<td align=center>
							<asp:Label CssClass="XX-Small" id="headLabel6" width="75px" runat="server">Port of <br /> Call 3</asp:Label>
						</td>
						<td align=center>
							<asp:Label CssClass="XX-Small" id="headLabel7" width="75px" runat="server">Port of <br /> Call 4</asp:Label>
						</td>
						<td align=center>
							<asp:Label CssClass="XX-Small" id="headLabel8" width="75px" runat="server">Port of <br /> Call 5</asp:Label>
						</td>
						<td align=center>
							<asp:Label CssClass="XX-Small" id="headLabel9" width="75px" runat="server">Port of <br /> Call 6</asp:Label>
						</td>
						<td align=center>
							<asp:Label CssClass="XX-Small" id="headLabel10" width="75px" runat="server">Port of <br /> Call 7</asp:Label>
						</td>
						<td align=center>
							<asp:Label CssClass="XX-Small" id="headLabel11" width="75px" runat="server">Port of <br /> Call 8</asp:Label>
						</td>
						<td align=center>
							<asp:Label CssClass="XX-Small" id="headLabel12" width="75px" runat="server">Port of <br /> Call 9</asp:Label>
						</td>
						<td align=center>
							<asp:Label CssClass="XX-Small" id="headLabel13" width="75px" runat="server">Port of <br /> Call 10</asp:Label>
						</td>
					</tr>
					<asp:repeater id="Repeater2" runat="server" OnItemDataBound="Repeater2_ItemDataBound">
						<ItemTemplate>
							<tr>
								<td>
									<asp:TextBox CssClass="XX-Small" autocomplete="off" id="txtPercentComplete" style="center" width="75px" runat="server"></asp:TextBox>
								</td>
								<td>
									<asp:TextBox maxlength="10" autocomplete="off" CssClass="XX-Small" id="txtPortCall1" style="center" width="75px"
										runat="server"></asp:TextBox>
									<asp:TextBox CssClass="XX-Small" autocomplete="off" id="txtPortCall1_State" style="center" width="0px" runat="server"></asp:TextBox>
								</td>
								<td>
									<asp:TextBox maxlength="10" autocomplete="off" CssClass="XX-Small" id="txtPortCall2" style="center" width="75px"
										runat="server"></asp:TextBox>
									<asp:TextBox CssClass="XX-Small" autocomplete="off" id="txtPortCall2_State" style="center" width="0px" runat="server"></asp:TextBox>
								</td>
								<td>
									<asp:TextBox maxlength="10" autocomplete="off" CssClass="XX-Small" id="txtPortCall3" style="center" width="75px"
										runat="server"></asp:TextBox>
									<asp:TextBox CssClass="XX-Small" autocomplete="off" id="txtPortCall3_State" style="center" width="0px" runat="server"></asp:TextBox>
								</td>
								<td>
									<asp:TextBox maxlength="10" autocomplete="off" CssClass="XX-Small" id="txtPortCall4" style="center" width="75px"
										runat="server"></asp:TextBox>
									<asp:TextBox CssClass="XX-Small" autocomplete="off" id="txtPortCall4_State" style="center" width="0px" runat="server"></asp:TextBox>
								</td>
								<td>
									<asp:TextBox maxlength="10" autocomplete="off" CssClass="XX-Small" id="txtPortCall5" style="center" width="75px"
										runat="server"></asp:TextBox>
									<asp:TextBox CssClass="XX-Small" autocomplete="off" id="txtPortCall5_State" style="center" width="0px" runat="server"></asp:TextBox>
								</td>
								<td>
									<asp:TextBox maxlength="10" autocomplete="off" CssClass="XX-Small" id="txtPortCall6" style="center" width="75px"
										runat="server"></asp:TextBox>
									<asp:TextBox CssClass="XX-Small" autocomplete="off" id="txtPortCall6_State" style="center" width="0px" runat="server"></asp:TextBox>
								</td>
								<td>
									<asp:TextBox maxlength="10" autocomplete="off" CssClass="XX-Small" id="txtPortCall7" style="center" width="75px"
										runat="server"></asp:TextBox>
									<asp:TextBox CssClass="XX-Small" autocomplete="off" id="txtPortCall7_State" style="center" width="0px" runat="server"></asp:TextBox>
								</td>
								<td>
									<asp:TextBox maxlength="10" autocomplete="off" CssClass="XX-Small" id="txtPortCall8" style="center" width="75px"
										runat="server"></asp:TextBox>
									<asp:TextBox CssClass="XX-Small" autocomplete="off" id="txtPortCall8_State" style="center" width="0px" runat="server"></asp:TextBox>
								</td>
								<td>
									<asp:TextBox maxlength="10" autocomplete="off" CssClass="XX-Small" id="txtPortCall9" style="center" width="75px"
										runat="server"></asp:TextBox>
									<asp:TextBox CssClass="XX-Small" autocomplete="off" id="txtPortCall9_State" style="center" width="0px" runat="server"></asp:TextBox>
								</td>
								<td>
									<asp:TextBox maxlength="10" autocomplete="off" CssClass="XX-Small" id="txtPortCall10" style="center" width="75px"
										runat="server"></asp:TextBox>
									<asp:TextBox CssClass="XX-Small" autocomplete="off" id="txtPortCall10_State" style="center" width="0px" runat="server"></asp:TextBox>
								</td>
							</tr>
						</ItemTemplate>
						<FooterTemplate>
							<tr>
								<td>
									<asp:Label CssClass="XX-Small" id="lblFinalPercent" width="75px" runat="server">% Complete</asp:Label>
								</td>
								<td>
									<asp:Label CssClass="XX-Small" id="lblPort1Percent" width="75px" runat="server"></asp:Label>
								</td>
								<td>
									<asp:Label CssClass="XX-Small" id="lblPort2Percent" width="75px" runat="server"></asp:Label>
								</td>
								<td>
									<asp:Label CssClass="XX-Small" id="lblPort3Percent" width="75px" runat="server"></asp:Label>
								</td>
								<td>
									<asp:Label CssClass="XX-Small" id="lblPort4Percent" width="75px" runat="server"></asp:Label>
								</td>
								<td>
									<asp:Label CssClass="XX-Small" id="lblPort5Percent" width="75px" runat="server"></asp:Label>
								</td>
								<td>
									<asp:Label CssClass="XX-Small" id="lblPort6Percent" width="75px" runat="server"></asp:Label>
								</td>
								<td>
									<asp:Label CssClass="XX-Small" id="lblPort7Percent" width="75px" runat="server"></asp:Label>
								</td>
								<td>
									<asp:Label CssClass="XX-Small" id="lblPort8Percent" width="75px" runat="server"></asp:Label>
								</td>
								<td>
									<asp:Label CssClass="XX-Small" id="lblPort9Percent" width="75px" runat="server"></asp:Label>
								</td>
								<td>
									<asp:Label CssClass="XX-Small" id="lblPort10Percent" width="75px" runat="server"></asp:Label>
								</td>
							</tr>
						</FooterTemplate>
					</asp:repeater>
				</table>
			</div>
		</td>
	</tr>
</table>

<br>
<div id="div2" align="center">
	<asp:label id="lblMessage" runat="server" Visible="False" Font-Bold="True" CssClass="XX-Small"></asp:label></div>
<br>
<asp:Button CssClass="MyReportsAdminConsoleButton" id="btnActivateAssociate" runat="server"
	Text="Non-Compass Associates" Width="160px" onclick="btnActivateAssociate_Click" CommandName="ShowAssociateListingScreen"></asp:Button>&nbsp;&nbsp;&nbsp;
<asp:Button CssClass="MyReportsAdminConsoleButton" id="btnSave" runat="server" Text="Save Changes"
	Width="128px" onclick="btnSave_Click"></asp:Button>
