<%@ Control Language="c#" Inherits="AssociateListing" CodeFile="AssociateListing.ascx.cs" %>
<br />
<asp:panel id="panAssociateListing" Visible="true" Runat="server">
	<TABLE cellSpacing="1" cellPadding="0" border="0">
		<TR>
			<TD colSpan="6">
				<asp:Button id="btnRefresh" Runat="server" CssClass="MyReportsAdminConsoleButton" Text="Refresh" onclick="btnRefresh_Click"></asp:Button>&nbsp;&nbsp;
				<asp:Button id="Button2" Runat="server" CssClass="MyReportsAdminConsoleButton" Text="Save Changes" onclick="Button2_Click"></asp:Button>&nbsp;&nbsp;
				<asp:Button id="btnAddNewUser" Runat="server" CssClass="MyReportsAdminConsoleButton" Text="Add New User" onclick="btnAddNewUser_Click"></asp:Button></TD>
		</TR>
		<TR>
			<TD colSpan="6">
				<HR>
			</TD>
		</TR>
		<TR>
			<TD>&nbsp;&nbsp;</TD>
			<TD></TD>
			<TD noWrap width="185">
				<asp:Label id="lblUserName" Runat="server" CssClass="XX-SmallBold">Associate's Name</asp:Label></TD>
			<TD width="90">
				<asp:Label id="lblPersonnelNumberTitle" Runat="server" CssClass="XX-SmallBold">Passport ID</asp:Label></TD>
			<TD noWrap width="220">
				<asp:Label id="Label3" Runat="server" CssClass="XX-SmallBold">Status</asp:Label></TD>
			<TD>&nbsp;</TD>
		</TR>
		<asp:repeater id="AssociateList" runat="server">
			<ItemTemplate>
				<tr>
					<td nowrap>&nbsp;&nbsp;</td>
					<td nowrap>
						<asp:CheckBox id="chkActive" runat="server"></asp:CheckBox></td>
					<td nowrap>
						<asp:Label id="lblFirstName" CssClass="XX-Small" runat="server">Joe Lunatti</asp:Label>
					</td>
					<td nowrap>
						<asp:Label id="lblPersonnelNumber" CssClass="XX-Small" runat="server">00000000</asp:Label>
						<asp:LinkButton id="lnkPersonnelNumber" CssClass="hyperlink" OnCommand="lnkPersonnelNumber_Command"
							runat="server"></asp:LinkButton>
					</td>
					<td nowrap>
						<asp:Label id="lblStatus" CssClass="XX-Small" runat="server">Compass Employee: Active</asp:Label></td>
					<td nowrap>
						<asp:LinkButton id="lnkSetInActive" CssClass="hyperlink" OnCommand="lnkSetInActive_Command" runat="server"></asp:LinkButton>
					</td>
				</tr>
			</ItemTemplate>
		</asp:repeater></TABLE>
</asp:panel><asp:panel id="panUserDetails" Visible="False" Runat="server">
	<TABLE>
		<TR>
			<TD colSpan="2">
				<H4>Add New Passport Users</H4>
			</TD>
		</TR>
		<TR>
			<TD colSpan="2">
				<asp:checkbox id="Checkbox1" Runat="server" Visible="False" Text="Active" Checked></asp:checkbox></TD>
		</TR>
		<TR>
			<TD>
				<asp:label id="Label1" Runat="server">
					<b>First Name:</b></asp:label></TD>
			<TD align="left">
				<asp:textbox id="txtFirstName" Runat="server" MaxLength="20" Width="250px"></asp:textbox></TD>
		</TR>
		<TR>
			<TD>
				<asp:label id="lblLastName" Runat="server">
					<b>Last Name:</b></asp:label></TD>
			<TD>
				<asp:textbox id="txtLastName" Runat="server" MaxLength="20" Width="250px"></asp:textbox></TD>
		</TR>
		<TR>
			<TD>
				<asp:Label id="Label2" Runat="server">
					<b>Personnel #:</b></asp:Label></TD>
			<TD>
				<asp:textbox id="txtPersonnelNumber" Runat="server" MaxLength="8" Width="100px" ReadOnly="True"></asp:textbox></TD>
		</TR>
		<TR>
			<TD>
				<asp:Label id="lblUnitNumber" Runat="server">
					<b>Unit Number:</b></asp:Label></TD>
			<TD>
				<asp:TextBox id="txtUnitNumber" Runat="server" MaxLength="6" Width="75px" ReadOnly="True"></asp:TextBox></TD>
		</TR>
		<TR>
			<TD colSpan="2"><BR>
			</TD>
		</TR>
		<TR>
			<TD align="right" colSpan="2">
				<asp:Button id="btnCancel" Runat="server" CssClass="MyReportsAdminConsoleButton" Text="Cancel" onclick="btnCancel_Click"></asp:Button>&nbsp;&nbsp;
				<asp:Button id="Button1" Runat="server" CssClass="MyReportsAdminConsoleButton" Text="Save" onclick="Button1_Click"></asp:Button></TD>
		</TR>
	</TABLE>
</asp:panel>
