using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.ComponentModel;

using System.Web.SessionState;
using System.Web.UI;

using Compass.Common;

using Passport.BLL;


public partial class PortsOfCallListing : basePassportUserControl
{

    protected void Page_Load(object sender, System.EventArgs e)
    {

    }

    public void LoadExpeditions(int expeditionID)
    {
        ArrayList ary = new PortsOfCall(MyReports.Common.AppValues.Passport_DB_ConnectionString).Get_PortsOfCall_By_Expedition(expeditionID);

        DataList1.RepeatColumns = ary.Count / 2;
        DataList1.DataSource = ary;
        DataList1.DataBind();
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.DataList1.ItemDataBound += new System.Web.UI.WebControls.DataListItemEventHandler(this.DataList1_ItemDataBound);

    }
    #endregion

    private void DataList1_ItemDataBound(object sender, System.Web.UI.WebControls.DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem ||
            e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.SelectedItem)
        {
            tPortsOfCall oPortOfCall = (tPortsOfCall)e.Item.DataItem;

            Label lblPOC = (Label)e.Item.FindControl("lblPOC");
            lblPOC.Text = oPortOfCall.PortNumber.ToString() + ") " + oPortOfCall.PortOfCallName + "&nbsp;&nbsp;";


        }
    }
}