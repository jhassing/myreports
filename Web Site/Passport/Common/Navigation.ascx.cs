using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

using Passport.BLL;

public partial class Navigation : basePassportUserControl
{
    protected System.Web.UI.WebControls.Label lblPassport;

    public event Delegates.ExpeditionLinkClicked ExpeditionLink_OnClick;
    public event Delegates.WelcomeLinkClicked WelcomeLink_OnClick;
    public event Delegates.ActivateAssociateClicked ActivateAssociate_OnClick;
    public event Delegates.UnitExpeditionClicked UnitExpedition_OnClick;

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.PreRender += new System.EventHandler(this.Navigation_PreRender);

    }
    #endregion

    #region Properties

    #region Operation Number

    public override int OperationNumber
    {
        get
        {
            base.OperationNumber = System.Convert.ToInt32(this.drpOperationsListing.SelectedValue);
            return System.Convert.ToInt32(this.drpOperationsListing.SelectedValue);
        }
        set
        {
            base.OperationNumber = System.Convert.ToInt32(this.drpOperationsListing.SelectedValue);
        }
    }

    #endregion

    #region Display Expedition Link

    public bool Display_ExpeditionListingLink
    {
        set
        {
            this.SelectExpeditionLink.Visible = value;
            this.SelectExpeditionLink_Sep.Visible = value;
            this.drpOperationsListing.Visible = value;
            this.OperationsListing_Sep.Visible = value;
        }
    }

    #endregion

    #region Display Ports Of Call Link

    public bool Display_PortsOfCallLink
    {
        set
        {
            this.SelectExpeditionLink.Visible = value;
            this.SelectExpeditionLink_Sep.Visible = value;
            this.UnitExpeditionLink.Visible = value;
            this.UnitExpeditionLink.Text = "Unit " + this.OperationNumber + " - " + this.ExpeditionName;
            this.UnitExpeditionLink.CommandArgument = this.ExpeditionName + "|" + this.ExpeditionID;
            this.UnitExpeditionLink.CommandName = "ShowPortsOfCallScreen";
            this.UnitExpeditionLink_Sep.Visible = value;
        }
    }

    void Navigation_ExpeditionLink_OnClick()
    {
        throw new Exception("The method or operation is not implemented.");
    }

    #endregion

    public string ReportName
    {
        get
        {
            return ViewState["ReportName"] == null ? "" : ViewState["ReportName"].ToString();
        }
        set
        {
            ViewState["ReportName"] = value;
        }
    }

    #endregion

    #region Hide Items

    public void HideItems()
    {
        this.Display_ExpeditionListingLink = false;
        this.Display_PortsOfCallLink = false;
    }

    #endregion

    #region Page Load

    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (Visible)
        {
            if (!Page.IsPostBack)
            {
                this.UserNetworkID = MyReports.Common.AppValues.UserName;

                // Populate the Operations Numbers box.
                ArrayList ary = new Associate(MyReports.Common.AppValues.Passport_DB_ConnectionString).GetOperationListing(MyReports.Common.AppValues.distinguishedUserName);

                //drpOperationsListing.Visible = true;

                // Load the operations into the box
                for (int x = 0; x < ary.Count; x++)
                    drpOperationsListing.Items.Add(new System.Web.UI.WebControls.ListItem("Unit - " + ary[x].ToString(), ary[x].ToString()));


            }

            //this.drpOperationsListing.Visible = false;
            //this.OperationsListing_Sep.Visible = false;
            //this.SelectExpeditionLink.Visible = false;
            //this.SelectExpeditionLink_Sep.Visible = false;
            //this.UnitExpeditionLink.Visible = false;
            //this.UnitExpeditionLink_Sep.Visible = false;
            //this.ActivateAssociate.Visible = false;
            //this.ActivateAssociate_Sep.Visible = false;
        }
    }

    #endregion

    #region Pre - Render

    protected void Navigation_PreRender(object sender, EventArgs e)
    {

        if (Visible)
        {

            // Select which menu BreadCrums to display
            switch (this.Screen)
            {
                case enumScreens.Expedition_Listing:
                    this.Display_ExpeditionListingLink = true;
                    break;

                case enumScreens.Ports_Of_Call:
                    this.Display_PortsOfCallLink = true;
                    break;

                case enumScreens.Associate_Listing:
                    this.SelectExpeditionLink.Visible = true;
                    this.SelectExpeditionLink_Sep.Visible = true;
                    this.UnitExpeditionLink.Visible = true;
                    this.UnitExpeditionLink.Text = "Unit " + this.OperationNumber + " - " + this.ExpeditionName;
                    this.UnitExpeditionLink_Sep.Visible = true;
                    this.ActivateAssociate.Visible = true;
                    this.ActivateAssociate_Sep.Visible = true;
                    break;

                case enumScreens.Report_Listing:
                    this.ReportListing.Visible = true;
                    this.ReportListing_Sep.Visible = true;
                    this.WelcomeLink.Visible = false;
                    this.WelcomeLink_Sep.Visible = false;
                    break;

                case enumScreens.Report_Name:
                    this.ReportListing.Visible = true;
                    this.ReportListing_Sep.Visible = true;
                    this.lblReportName.Text = this.ReportName;
                    this.lblReportName.Visible = true;
                    this.WelcomeLink.Visible = false;
                    this.WelcomeLink_Sep.Visible = false;
                    break;

            }
        }
    }

    #endregion

    #region Events

    protected void ActivateAssociate_Click(object sender, System.EventArgs e)
    {
        if (ActivateAssociate_OnClick != null)
            ActivateAssociate_OnClick();
    }


    protected void UnitExpeditionLink_Click(object sender, EventArgs e)
    {
        if (UnitExpedition_OnClick != null)
            UnitExpedition_OnClick();
    }

    protected void WelcomeLink_Click(object sender, System.EventArgs e)
    {
        if (WelcomeLink_OnClick != null)
            WelcomeLink_OnClick();
    }

    protected void SelectExpeditionLink_Click(object sender, System.EventArgs e)
    {
        if (ExpeditionLink_OnClick != null)
            ExpeditionLink_OnClick();
    }


    #endregion

}