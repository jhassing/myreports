<%@ Control Language="c#" Inherits="Navigation" CodeFile="Navigation.ascx.cs" %>
<table class="breadcrumbs" cellSpacing="0" cellPadding="3" width="100%" border="0">
	<tr>
		<td align="left" width="100%">
			<!-- Passport -->
			<asp:Label id="Label1" CssClass="breadcrumb" runat="server">Passport > </asp:Label>
			<!-- Welcome -->
			<asp:LinkButton id="WelcomeLink" CssClass="breadcrumb" runat="server" CommandName="ShowWelcomeScreen" onclick="WelcomeLink_Click">Welcome</asp:LinkButton>
			<asp:Label id="WelcomeLink_Sep" CssClass="breadcrumb" runat="server"> > </asp:Label>
			<!-- Welcome -->
			<asp:LinkButton id="SelectExpeditionLink" Visible="False" CssClass="breadcrumb" runat="server" onclick="SelectExpeditionLink_Click" CommandName="ShowExpeditionsScreen">Select Expedition</asp:LinkButton>
			<asp:Label id="SelectExpeditionLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
			<!-- Operations DropDown -->
			<asp:DropDownList id="drpOperationsListing" CssClass="XX-SmallDropDown" Visible="False" runat="server"></asp:DropDownList>
			<asp:Label id="OperationsListing_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
			<!-- Unit Expedition -->
			<asp:LinkButton id="UnitExpeditionLink" Visible="False" CssClass="breadcrumb" runat="server" onclick="UnitExpeditionLink_Click">Unit 6412 - Operations</asp:LinkButton>
			<asp:Label id="UnitExpeditionLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
			<!-- Associate Listing -->
			<asp:LinkButton id="ActivateAssociate" Visible="False" CssClass="breadcrumb" runat="server" onclick="ActivateAssociate_Click">Associates</asp:LinkButton>
			<asp:Label id="ActivateAssociate_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
			<!-- Reports Listing -->
			<asp:LinkButton id="ReportListing" Visible="False" CssClass="breadcrumb" CommandName="DisplayReportListing" runat="server">Reports</asp:LinkButton>
			<asp:Label id="ReportListing_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
			<!-- Reports Listing -->
			<asp:Label id="lblReportName" Visible="False" CssClass="breadcrumb" runat="server">[Report Name.roi]</asp:Label>		
		</td>
	</tr>
</table>
