using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services.Protocols;


using System.Collections;

//  Using sub name spaces
using activeportal.classes;
using activeportal.classes.api.proxy;
using activeportal.classes.api;
using actuate_i18n;
using actuate_i18n.classes;
using activeportal.classes.configuration;
using activeportal.classes.functionality;

using DistributionPortal;
using Microsoft.ApplicationBlocks.ExceptionManagement;

public partial class Passport_Administration_Reports_ParameterScreen : System.Web.UI.UserControl
{

    protected HtmlTable TableParamData;
    private string m_tableParamName;
    private FieldValue[][] m_TableValues = null;
    private FieldValue[] m_InsertedRecord = null;
    private FieldDefinition[] m_recordDefinition = null;
    private string m_tableParamDisplayMode = "insert";
    private int m_tableEditedRowNumber = 0;
    private int m_rowToBeDeleted = 0;
    private ParameterDefinition[] m_parameterList = null;
    private string m_usedForActuateQuery = "false";


    private string m_executableName;
    private Hashtable m_paramTable;
    protected Compass.Web.WebControls.CgButtonControl btnOKTop;
    protected Compass.Web.WebControls.CgButtonControl btnOKBottom;
    protected Compass.Web.WebControls.CgButtonControl btnCancelBottom;
    protected Compass.Web.WebControls.CgButtonControl btnSubmitBottom; // holds the name of the parameter and if it is hidden
    private bool m_dataChanged = false;

    public string ExecutableName
    {
        get
        {
            m_executableName = (string)ViewState["ExecutableFileName"];

            if (m_executableName == null)
            {
                m_executableName = AcParams.Get(Page, AcParams.Name.__executableName);
                ViewState["ExecutableName"] = m_executableName;
            }
            return m_executableName;
        }
    }

    public ArrayList ReportParameters
    {
        get
        {
            return (ArrayList)ViewState["ReportParameters"];
        }
        set
        {
            ViewState["ReportParameters"] = value;
        }
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        this.lblMessage.Text = "";
        this.lblMessage.Visible = false;
    }

    #region DisplayError

    private void DisplayError(Exception ex)
    {
        this.lblMessage.Visible = true;
        string errorMessages = "";

        switch (ex.GetBaseException().GetType().Name)
        {
            case "SoapException":
                #region If this is a SOAP error, load the aditional info
                
                SoapException l_e  = (SoapException)ex.GetBaseException();

                errorMessages += "# SOAP Fault received: ";
                for (int i = 0; i < l_e.Detail.ChildNodes.Count; i++)
                {
                    if (l_e.Detail.ChildNodes.Item(i).Name[0] != '#')
                    {
                        if (l_e.Detail.ChildNodes.Item(i).Name == "Description")
                        {
                            for (int j = 0; j < l_e.Detail.ChildNodes.Item(i).ChildNodes.Count; j++)
                            {
                                if (l_e.Detail.ChildNodes.Item(i).ChildNodes.Item(j).Name[0] != '#')
                                {
                                    errorMessages += (l_e.Detail.ChildNodes.Item(i).ChildNodes.Item(j).Name + ": "
                                        + l_e.Detail.ChildNodes.Item(i).ChildNodes.Item(j).InnerText) + "<br>";
                                }
                            }
                        }
                        else
                        {
                            errorMessages += (l_e.Detail.ChildNodes.Item(i).Name + ": " + l_e.Detail.ChildNodes.Item(i).InnerText) + Environment.NewLine;
                        }
                    }
                }

                #endregion
                break;

            default:
                errorMessages = ex.GetBaseException().Message;
                break;

        }
                
        if (ex.InnerException != null)
        {
            errorMessages += "<br>";
            errorMessages += ex.InnerException.Message;
        }

        this.lblMessage.Text = errorMessages;

        ExceptionManager.Publish(ex);
    }

    #endregion

    protected override void OnPreRender(EventArgs e)
    {
        try
        {
            base.OnPreRender(e);            

            if (!Page.IsPostBack)
            {
                if (Visible)
                {
                    this.OperationNumber1.Visible = false;
                    this.Year1.Visible = false;

                    ArrayList aryParams = new ArrayList();

                    GetReportParametersResponse reportParameters = GetReportParameters();

                    if (reportParameters != null)
                    {
                        foreach (ParameterDefinition parmDef in reportParameters.ParameterList)
                        {

                            if (!parmDef.IsHidden)
                            {
                                switch (parmDef.Name.ToLower())
                                {
                                    case "as_operationnumber":
                                        this.OperationNumber1.Visible = true;
                                        aryParams.Add("as_operationnumber");
                                        break;

                                    case "as_region":
                                        this.RegionCodes1.Visible = true;
                                        aryParams.Add("as_Region");
                                        break;

                                    case "as_sector":
                                        this.SectorCodes1.Visible = true;
                                        aryParams.Add("as_Sector");
                                        break;

                                    case "as_division":
                                        this.DivisionCodes1.Visible = true;
                                        aryParams.Add("as_Division");
                                        break;

                                }
                            }
                        }
                    }

                    if (aryParams.Count == 0)
                    {
                        lblMessage.Text = "This report does not have any parameters, please click on submit.";
                        lblMessage.ForeColor = System.Drawing.Color.Black;
                        lblMessage.Visible = true;
                    }

                    this.ReportParameters = aryParams;
                }
            }
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }

    private GetReportParametersResponse GetReportParameters()
    {
        AcGetReportParameters reportParameters = null;

        if (!IsPostBack)
        {

            reportParameters = new AcGetReportParameters(this.ExecutableName, "ReportFileName", Page);
            m_parameterList = reportParameters.paramList();


            return reportParameters.Response;

        }
        else
        {
            return null;
        }
    }

    protected void btnSubmitTop_Click(object sender, System.EventArgs e)
    {
        ExecuteReport();
    }

    private void btnSubmitBottom_Click(object sender, System.EventArgs e)
    {
        ExecuteReport();
    }

    private void ExecuteReport()
    {
        AcUrlBuilder link = new AcUrlBuilder();

        try
        {
            // Set the base URL
            link.SetBaseUrl(Context.Request.ApplicationPath + "/newrequest/do_executereport.aspx", true);

            // Get the current requests parameters
            ArrayList aryParams = this.ReportParameters;

            foreach (string param in aryParams)
            {

                switch (param.ToLower())
                {
                    case "as_operationnumber":
                        link.AddKeyValue("as_operationnumber", OperationNumber1.SelectedGroups);
                        break;

                    case "as_region":
                        link.AddKeyValue("as_region", this.RegionCodes1.SelectedGroups);
                        break;

                    case "as_sector":
                        link.AddKeyValue("as_sector", this.SectorCodes1.SelectedSector);
                        break;

                    case "as_division":
                        link.AddKeyValue("as_division", DivisionCodes1.SelectedDivision);
                        break;

                }


            }

            link.AddKeyValue("ODBC", "Passport_DEV");

            link.AddKeyValue(AcParams.Name.__executableName, this.ExecutableName);
            link.AddKeyValue(AcParams.Name.__SourceOfRequest__, "ReportListing.aspx");

            string sJobName = AcParams.Get(Page, AcParams.Name.__jobname);
            if (sJobName == null)
            {
                sJobName = ActuUtil.stripFileExtension(ActuUtil.stripFolderName(this.ExecutableName, "/"));
                link.AddKeyValue(AcParams.Name.__jobname, sJobName);
            }

            // Addd the __wait parameter 
            string sWait = AcParams.Get(Page, AcParams.Name.__wait);

            if (sWait != null &&
                sWait.Trim() != String.Empty)
            {
                link.AddKeyValue(AcParams.Name.__wait, sWait);
            }

            link.AddKeyValue(AcParams.Name.__scheduleType, "immediate");

          
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }

        //Server.Transfer(link.ToString());
        Response.Redirect(link.ToString(), false);
    }
}