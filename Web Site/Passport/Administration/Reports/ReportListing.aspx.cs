using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Passport_Administration_Reports_ReportListingPage : System.Web.UI.Page
{
    private bool Display_ReportListing
    {
        set { HideScreens(); ReportListing1.LoadReports(); ReportListing1.Visible = value; }
    }

    private bool Display_ParameterScreen
    {
        set { HideScreens(); ParameterScreen1.Visible = value; }
    }

    private void HideScreens()
    {
        ReportListing1.Visible = false;
        ParameterScreen1.Visible = false;
    }

    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!Page.IsPostBack)
        {            
            if (Request.QueryString["ShowParmScreen"] == null)
            {
                this.Navigation1.Screen = basePassportUserControl.enumScreens.Report_Listing;
                this.Display_ReportListing = true;                 
            }
            else
            {
                string ReportEXEname = "";

                if (Request.QueryString["__executableName"] != null)
                {
                    string tmp = Request.QueryString["__executableName"].ToString();

                    string[] tmp1 = tmp.Split('/');

                    if (tmp1.Length == 0)
                        ReportEXEname = tmp;
                    else
                    {
                        ReportEXEname = tmp1[tmp1.Length - 1];
                    }
                }

                this.Navigation1.ReportName = ReportEXEname;
                this.Navigation1.Screen = basePassportUserControl.enumScreens.Report_Name;
                this.Display_ParameterScreen = true;
            }

            

        }
    }

    

    protected override bool OnBubbleEvent(object source, EventArgs args)
    {
        
        if (args is CommandEventArgs)
        {
            CommandEventArgs ce = (CommandEventArgs)args;

            switch (ce.CommandName)
            {
                case "DisplayReportListing":                    
                    //this.Display_ReportListing = true;    
                    Response.Redirect("reportlisting.aspx");
                    break;
            }                
        }

        return true;
    }
}