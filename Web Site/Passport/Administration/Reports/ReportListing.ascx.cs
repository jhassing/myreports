using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using System.Reflection;
using System.Text;
using System.Configuration;

//  Using sub name spaces
using activeportal.classes;
using activeportal.classes.api.proxy;
using activeportal.classes.api;
using actuate_i18n;
using actuate_i18n.classes;

using Compass.Reporting.Actuate;
using MyReports.Common;

//using activeportal.usercontrols.common;

using activeportal.usercontrols.filesfolders;

public partial class Passport_Administration_Reports_ReportListing : System.Web.UI.UserControl
{
    public void LoadReports()
    {
       // if (Visible)
        //{
        try
        {
            Categoriesview1.FolderTarget = "../index/index.aspx?app=bbd&subpage=_list";
            Categoriesview1.DetailTarget = "../index/index.aspx?app=bbd&subpage=_detail";

            Categoriesview1.ViewingTarget = Context.Request.ApplicationPath + "/viewer/viewFrameset.aspx";
            Categoriesview1.ContentTarget = "../viewer/view3partyreport.aspx";
            Categoriesview1.DownloadTarget = "../viewer/view3partyreport.aspx";
            Categoriesview1.ExecuteTarget = Context.Request.ApplicationPath + "/Passport/Administration/Reports/ReportListing.aspx?ShowParmScreen=0";
            Categoriesview1.SubmitJobTarget = "../viewer/view3partyreport.aspx";
        }
        catch (Exception ex)
        {
            Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex);
        }

       // }
    }    
}