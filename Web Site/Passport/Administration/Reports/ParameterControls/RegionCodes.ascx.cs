using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Passport_Administration_Reports_ParameterControls_RegionCodes : System.Web.UI.UserControl
{

    public string SelectedGroups
    {
        get
        {
            //string groups = "";

            //int x = this.drpRegionCodes.Items.Count;

            //for (int y = 1; y < x; y++)
            //{
            //    // loop through each item.. if it is selected, then add it
            //    if (this.drpRegionCodes.Items[y].Selected)
            //    {
            //        groups += this.drpRegionCodes.Items[y].Text + "|";
            //    }
            //}

            //return groups;

            return this.drpRegionCodes.SelectedValue;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            foreach (string group in MyReports.Common.AppValues.UserRegionalGroupMembership)
            {

                ListItem li = new ListItem(group, group);

                if (!this.drpRegionCodes.Items.Contains(li))
                    this.drpRegionCodes.Items.Add(li);

            }
        }
    }
}