<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DivisionCodes.ascx.cs" Inherits="Passport_Administration_Reports_ParameterControls_DivisionCodes" %>
<table cellSpacing="0" cellPadding="1" width="100%" border="0">
	<tr>
		<td vAlign="top" noWrap style="width: 180px">
		    <asp:Label  CssClass="boldlabel" id="lblNote" Text="Please select a Division code:" runat="server" Width="180px"></asp:Label>
       </td>
		<td vAlign="middle" noWrap align="left">&nbsp;
			<asp:DropDownList id="drpGroups" runat="server" Height="250px" DataValueField="Division" DataTextField="DivisionName" DataSourceID="ObjectDataSource1"></asp:DropDownList>
	    </td>
	</tr>
</table>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OnObjectCreating="ObjectDataSource1_ObjectCreating"
    SelectMethod="GetDivisionListing" TypeName="Passport.BLL.Hierarchy"></asp:ObjectDataSource>
