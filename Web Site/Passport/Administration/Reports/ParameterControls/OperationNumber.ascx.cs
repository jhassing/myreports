using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

using DistributionPortal;

public partial class Passport_Administration_Reports_ParameterControls_OperationNumber : System.Web.UI.UserControl
{

    public string SelectedGroups
    {

        get
        {
            string groups = "";

            if (this.drpGroups.Items[0].Selected)
            {
                // All was selected
                int x = this.drpGroups.Items.Count;

                for (int y = 1; y < x; y++)
                {
                    groups += this.drpGroups.Items[y].Text + "|";
                }
            }
            else
            {
                int x = this.drpGroups.Items.Count;

                for (int y = 1; y < x; y++)
                {
                    // loop through each item.. if it is selected, then add it

                    if (this.drpGroups.Items[y].Selected)
                    {
                        groups += this.drpGroups.Items[y].Text + "|";
                    }

                }
            }

            return groups;

        }
    }

    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            drpGroups.Items.Add("All");

            ArrayList currentGroups = MyReports.Common.AppValues.UserGroupMembership;

            for (int x = 0; x < MyReports.Common.AppValues.UserGroupMembership.Count; x++)
            {
                string group = MyReports.Common.AppValues.UserGroupMembership[x].ToString().ToLower();

                if ((group.IndexOf(MyReports.Common.AppValues.FinancialGroups.ToLower()) > 0) ||
                    (group.IndexOf(MyReports.Common.AppValues.PayrollSalaryGroups.ToLower()) > 0) ||
                    (group.IndexOf(MyReports.Common.AppValues.PayrollHourlyGroups.ToLower()) > 0))
                {

                    if (group.IndexOf(MyReports.Common.AppValues.FinancialGroups.ToLower()) > 0)
                        group = group.Substring(0, group.Length - 4);
                    else if ((group.IndexOf(MyReports.Common.AppValues.PayrollSalaryGroups.ToLower()) > 0) ||
                        (group.IndexOf(MyReports.Common.AppValues.PayrollHourlyGroups.ToLower()) > 0))
                        group = group.Substring(0, group.Length - 5);

                    ListItem li = new ListItem(group, group);

                    if (!this.drpGroups.Items.Contains(li))
                        this.drpGroups.Items.Add(li);

                }
            }

        }
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {

    }
    #endregion
}