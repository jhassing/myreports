<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SectorCodes.ascx.cs" Inherits="Passport_Administration_Reports_ParameterControls_SectorCodes" %>
<table cellSpacing="0" cellPadding="1" width="100%" border="0">
	<tr>
		<td vAlign="top" noWrap style="width: 180px; height: 24px;">
		    <asp:Label CssClass="boldlabel"  id="lblNote" Text="Please select a Sector code:" Width="180px" runat="server"></asp:Label>
		</td>
		<td vAlign="middle" noWrap align="left" style="height: 24px">&nbsp;
			<asp:DropDownList id="drpSectorCodes" runat="server" Width="300px" Height="168px" DataValueField="Sector" DataTextField="SectorName" DataSourceID="ObjectDataSource1">
            </asp:DropDownList>
	    </td>
	</tr>
</table>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetSectorListing"
    TypeName="Passport.BLL.Hierarchy" OnObjectCreating="ObjectDataSource1_ObjectCreating"></asp:ObjectDataSource>
