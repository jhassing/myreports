<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RegionCodes.ascx.cs" Inherits="Passport_Administration_Reports_ParameterControls_RegionCodes" %>
<table cellSpacing="0" cellPadding="1" width="100%" border="0">
	<tr>
		<td vAlign="top" noWrap style="width: 180px">
		    <asp:Label CssClass="boldlabel" id="lblNote" Text="Please select a Region code:" Width="180px" runat="server"></asp:Label>
		</td>
		<td vAlign="middle" noWrap align="left">&nbsp;
			<asp:DropDownList id="drpRegionCodes" runat="server" Width="300px" Height="168px"></asp:DropDownList>
	    </td>
	</tr>
</table>