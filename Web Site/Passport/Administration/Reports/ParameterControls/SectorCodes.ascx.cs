using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Passport_Administration_Reports_ParameterControls_SectorCodes : System.Web.UI.UserControl
{
    public string SelectedSector
    {
        get
        {
            return this.drpSectorCodes.SelectedValue;
        }

    }

    protected void ObjectDataSource1_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        try
        {
            e.ObjectInstance = new Passport.BLL.Hierarchy(MyReports.Common.AppValues.Passport_DB_ConnectionString);
        }
        catch (Exception ex)
        {
            //DisplayError(ex);
        }
    }
}
