<%@ Register TagPrefix="uc1" TagName="ReportListing" Src="ReportListing.ascx" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportListing.aspx.cs" Inherits="Passport_Administration_Reports_ReportListingPage" %>
<%@ Register TagPrefix="uc1" TagName="ParameterScreen" Src="ParameterScreen.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Navigation" Src="../../Common/Navigation.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Passport Reports</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="<%=Context.Request.ApplicationPath%>/css/allstyles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<uc1:Navigation id="Navigation1" runat="server"></uc1:Navigation>
			<uc1:ReportListing id="ReportListing1" runat="server" Visible="False"></uc1:ReportListing>
			<uc1:ParameterScreen id="ParameterScreen1" runat="server" Visible="False"></uc1:ParameterScreen>
		</form>
	</body>
</HTML>
