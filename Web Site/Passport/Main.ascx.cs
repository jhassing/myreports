using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.ExceptionManagement;

public partial class Passport_Main : basePassportUserControl
{
    #region DisplayError

    private void DisplayError(Exception ex)
    {
        this.lblMessage.Visible = true;
        string mess = ex.Message;
        mess += "\r\n";

        if (ex.InnerException != null)
            mess += ex.InnerException.Message;

        this.lblMessage.Text = mess;

        ExceptionManager.Publish(ex);
    }

    #endregion

    #region HideControls

    private void HideControls()
    {
        this.PassportWelcome1.Visible = false;
        this.PassportExpeditions1.Visible = false;
        this.AssociateListing1.Visible = false;
        this.ExpeditionPortsOfCall1.Visible = false;

    }

    #endregion

    #region ShowWelcomeScreen

    private void ShowWelcomeScreen()
    {
        this.Navigation1.HideItems();
        this.HideControls();
        this.PassportWelcome1.Visible = true;
    }

    #endregion

    #region ShowExpeditionsScreen

    private void ShowExpeditionsScreen()
    {
        this.Navigation1.HideItems();
        this.HideControls();
        this.PassportExpeditions1.LoadData();
        this.PassportExpeditions1.Visible = true;
        this.Navigation1.Display_ExpeditionListingLink = true;
    }

    #endregion

    #region Show the ports of call screen

    public void ShowPortsOfCallScreen(CommandEventArgs ce)
    {
        string[] tmp = ((string)ce.CommandArgument).Split('|');

        string ExpeditionName = tmp[0];
        int ExpeditionID = System.Convert.ToInt32(tmp[1]);

        this.HideControls();
        this.ExpeditionPortsOfCall1.Visible = true;

        this.ExpeditionName = ExpeditionName;
        this.Navigation1.ExpeditionName = ExpeditionName;
        this.Navigation1.ExpeditionID = ExpeditionID;
        
        this.ExpeditionPortsOfCall1.UserNetworkID = MyReports.Common.AppValues.UserName;

        this.ExpeditionPortsOfCall1.ExpeditionID = ExpeditionID;
        this.ExpeditionPortsOfCall1.ExpeditionName = ExpeditionName;        
        this.ExpeditionPortsOfCall1.OperationNumber = System.Convert.ToInt32(this.Navigation1.OperationNumber);

        this.Navigation1.HideItems();
        this.Navigation1.Display_PortsOfCallLink = true;
    }

    #endregion

    #region Show Activate Associate Screen

    public void ShowActivateAssociateScreen()
    {
        HideControls();

        // Make sure the 1st tab is visible


        this.AssociateListing1.Visible = true;
        this.AssociateListing1.OperationNumber = this.ExpeditionPortsOfCall1.OperationNumber;
        this.AssociateListing1.ExpeditionID = this.ExpeditionPortsOfCall1.ExpeditionID;
        this.AssociateListing1.ShowPanel = 1;

        //this.Navigation1.Screen = enumScreens.Associate_Listing;


    }

    #endregion

    protected override bool OnBubbleEvent(object source, EventArgs args)
    {

        try
        {
            bool handled = false;
            if (args is CommandEventArgs)
            {
                CommandEventArgs ce = (CommandEventArgs)args;
                switch (ce.CommandName)
                {
                    case "ShowWelcomeScreen":
                        ShowWelcomeScreen();
                        break;

                    case "ShowExpeditionsScreen":
                        ShowExpeditionsScreen();
                        break;

                    case "ShowPortsOfCallScreen":
                        ShowPortsOfCallScreen(ce);
                        break;

                    case "ShowAssociateListingScreen":
                        ShowActivateAssociateScreen();
                        break;

                }

            }

            return handled;
        }
        catch (Exception ex)
        {
            DisplayError(ex);

            return false;
        }
    }
}
