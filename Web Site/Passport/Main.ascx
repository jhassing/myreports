<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Main.ascx.cs" Inherits="Passport_Main" %>
<%@ Register TagPrefix="uc1" TagName="ExpeditionPortsOfCall" Src="Screens/ExpeditionPortsOfCall.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PassportWelcome" Src="Screens/Welcome.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Navigation" Src="Common/Navigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PassportExpeditions" Src="Screens/ExpeditionListing.ascx" %>
<%@ Register TagPrefix="uc1" TagName="AssociateListing" Src="Screens/AssociateListing.ascx" %>

<table width="100%" >
    <tr>
        <td vAlign="top" align="left">
            <uc1:navigation id="Navigation1" runat="server"></uc1:navigation>
            <uc1:passportwelcome id="PassportWelcome1" runat="server" Visible="true"></uc1:passportwelcome>
            <uc1:passportexpeditions id="PassportExpeditions1" runat="server" Visible="False"></uc1:passportexpeditions>
            <uc1:ExpeditionPortsOfCall id="ExpeditionPortsOfCall1" runat="server" Visible="False"></uc1:ExpeditionPortsOfCall>
            <uc1:AssociateListing id="AssociateListing1" runat="server" Visible="False"></uc1:AssociateListing>
        </td>
    </tr>
</table>
<br /> &nbsp;
<asp:Label Visible=False ID="lblMessage" ForeColor="Red" runat="server" CssClass="X-Small" Text="[Message]" Height="52px" Width="608px"></asp:Label>