<%@ Page Buffer = "true" language="c#" Inherits="DistributionPortal.Index" CodeFile="Index.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<TITLE>MyReports</TITLE>
		<META http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta content="True" name="vs_showGrid">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/allstyles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<form runat="server" id="AcForm">

		<frameset rows="70,*, 20" framespacing="1" frameborder="0">
			<frame marginwidth="0" marginheight="0" scrolling="no" name="topbanner" src="../Common/Banner.aspx" />
			<frameset cols="172px,80%">
				<frame marginwidth="3" name="side" src="../SideMenu/SideMenu.aspx">
				<frame marginwidth="3" marginheight="0" name="main" src="<%=ContentPage%>">
			</frameset>
			<frame name="footer" src="../Common/footer.aspx" scrolling="no" noresize>
		</frameset>

	</form>
</HTML>
