using System;
using System.Collections;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

using DistributionPortal.Components;

using Compass.Reporting; 

namespace DistributionPortal
{
	public partial class Index : baseWebPage
	{
		protected Control wFeaturedProductUserControl = null;
		protected string _contentPage = "";

		protected string ContentPage
		{
			get
			{
				return this._contentPage;
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (Context.Session != null)
            {
                //Tested and the IsNewSession is more advanced then simply checking if 
                // a cookie is present, it does take into account a session timeout, because 
                // I tested a timeout and it did show as a new session
                if (Session.IsNewSession)
                {
                    // If it says it is a new session, but an existing cookie exists, then it must 
                    // have timed out (can't use the cookie collection because even on first 
                    // request it already contains the cookie (request and response
                    // seem to share the collection)
                    string szCookieHeader = Request.Headers["Cookie"];
                    if ((null != szCookieHeader) && (szCookieHeader.IndexOf("ASP.NET_SessionId") >= 0))
                    {
                        Response.Redirect("sessionTimeout.htm");
                    }
                }
            }

			try
			{
				if (Visible)
				{
					// register the timeout script
					RegisterTimeoutScript();	
	
					string control = "";				

					string queryString = "?doframe=true&" + Request.QueryString.ToString();

					switch( QueryStringValues.App )
					{

							#region Budget Distribution

						case enumApp.bd:
							_contentPage = Context.Request.ApplicationPath + "/BudgetDistribution/BudgetDistribution.aspx" + queryString;
							break;

							#endregion
                        
                    #region YEA

                        case enumApp.yea:
                            _contentPage = Context.Request.ApplicationPath + "/BudgetDistribution/YEA.aspx" + queryString;
                            break;

                        #endregion
						


							#region Report Distribution


						case enumApp.rd:												
							_contentPage = Context.Request.ApplicationPath + "/ReportDistribution/RDList.aspx" + queryString;	
						
							break;

							#endregion

							#region Report Distribution Administration

						case enumApp.rd_admin:
                            _contentPage = Context.Request.ApplicationPath + "/ReportDistribution/Administration/Administration.aspx" + queryString;
							break;

							#endregion

                            #region Report Distribution Selected Files

                            case enumApp.rd_selectedfiles:
                                _contentPage = Context.Request.ApplicationPath + "/ReportDistribution/SelectedFiles/Default.aspx" + queryString;

                            break;

                            #endregion

                            #region Blue Book Distribution

                            case enumApp.bbd:
							    _contentPage = Context.Request.ApplicationPath + "/ReportDistribution/RDList.aspx" + queryString;	
    						
							    break;

							    #endregion

							#region Blue Book Distribution Administration

						case enumApp.bbd_admin:
							control = "../BlueBookDistribution/Administration/BBD_Administration.ascx";
							break;

							#endregion

							#region Passport

						case enumApp.passport:
							_contentPage = Context.Request.ApplicationPath + "/Passport/PassportDefault.aspx" + queryString;
							break;

							#endregion 

							#region Passport Administration

						case enumApp.passport_admin:
							_contentPage = Context.Request.ApplicationPath + "/Passport/Administration/Reports/ReportListing.aspx" + queryString;

							break;

							#endregion

						#region User Management
                        case enumApp.user_management:
							_contentPage = Context.Request.ApplicationPath + "/UserManagement/UserManagementDefault.aspx" + queryString;
							break;
                        #endregion

                        #region Viewed Reports
                        case enumApp.viewed_reports:
                            _contentPage = Context.Request.ApplicationPath + "/ReportDistribution/ViewedReports/ViewedReports.aspx" + queryString;
                            break;
                        #endregion

                        default:
                            _contentPage = Context.Request.ApplicationPath + "/News/News_View.aspx" + queryString;
							break;
							
					}



					if ( control.Length > 0 )
					{
						wFeaturedProductUserControl = LoadControl( control );	

						wFeaturedProductUserControl.EnableViewState = true;		
						wFeaturedProductUserControl.Visible = true;
						//PlaceHolderMain.Controls.Add(wFeaturedProductUserControl);
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		public override void Dispose()
		{
			wFeaturedProductUserControl = null;
			base.Dispose ();
		}

		~Index()
		{
			wFeaturedProductUserControl = null;
		}
	}
}
