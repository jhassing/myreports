<%@ Page Language="C#" AutoEventWireup="true" CodeFile="YEA.aspx.cs" Inherits="YEA" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>YEAR END ALIGNMENT:  Budget File Fix</title>
</head>

<body>
    <form id="form1" runat="server">
      <div>
            <asp:Label ID="Label1" runat="server" Text="P13 Removal Utility" Font-Bold="True" Font-Size="Large"></asp:Label>
          <br />
          <asp:Button ID="Button1" runat="server"  Visible=false Text="Run Unattended" OnClick="Button1_Click" />
          
          <asp:RadioButtonList ID="radPath" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="radPath_SelectedIndexChanged">
              <asp:ListItem Value="0">Local</asp:ListItem>
              <asp:ListItem Value="1">Web</asp:ListItem>
          </asp:RadioButtonList>
          <asp:TextBox ID="txtPath" runat="server" Width="462px"></asp:TextBox><br />
          <hr align="left" color="#000099" width="450" />
          <asp:Label ID="lblLogPath" runat="server" Text="Log Path: " Width="75"></asp:Label>
          <asp:TextBox ID="txtLogPath" runat="server" Width="384px" BorderStyle="None" Enabled="False" ForeColor="#404040"></asp:TextBox>
          <p></p>
          
          
          <asp:TextBox ID="txtAdUnit" Width="75" runat="server"></asp:TextBox>
          <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add Unit" />
          <asp:Button ID="btnDel" runat="server" OnClick="btnDel_Click" Text="Delete Unit" />
          <asp:Button ID="btnRun1" runat="server" Text="Trim Unit" OnClick="btnRun1_Click" />
          <asp:Button ID="btnTrunc" runat="server" Text="Truncate Units" OnClick="btnTrunc_Click" />
          <asp:Button ID="btnLoadZ" runat="server" Text="Load from ZROPS" OnClick="btnLoadZ_Click" /><br />
          <br />
          <asp:DropDownList ID="DDLpkReg" runat="server" Width="75" AutoPostBack="true" OnSelectedIndexChanged="DDLpkReg_SelectedIndexChanged" BackColor="#FFFFC0" Font-Bold="True" ToolTip="Select a Region" />
          <asp:TextBox ID="txtStatus" Width="300" runat="server" BorderStyle="None" Enabled="False" ForeColor="#404040"></asp:TextBox><br />
          <br />
            <p></p>
            <asp:Button ID="btnRunBud" runat="server" Text="Run File Trim" OnClick="btnRunBud_Click" />
            
          <asp:SqlDataSource ID="SqlDataSource1" runat="server" >
          </asp:SqlDataSource>
            <p></p>
            <asp:GridView ID="dgUnits" runat="server">
            </asp:GridView>
            
            <asp:GridView ID="dgPKunit" runat="server" CellPadding="4" BackColor="White" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px">
                <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                <RowStyle BackColor="White" ForeColor="#003399" />
                <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                <HeaderStyle BackColor="#003399" Font-Bold="True" ForeColor="#CCCCFF" />
            </asp:GridView>
          &nbsp;&nbsp;
            </div>
    </form>
</body>
</html>
