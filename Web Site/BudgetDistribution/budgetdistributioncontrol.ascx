<%@ Register TagPrefix="uc1" TagName="separator" Src="../Common/separator.ascx" %>
<%@ Control Language="c#" Inherits="DistributionPortal.BudgetDistributionControl" CodeFile="BudgetDistributionControl.ascx.cs" %>
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr class="TableButtonRow">
		<td align="left" valign="middle" colSpan="5">&nbsp;&nbsp;&nbsp;&nbsp;<asp:button id="btnDownloadFiles" Text="Download Files" runat="server" onclick="btnDownloadFiles_Click"></asp:button>
			<asp:checkbox CssClass="X-Small" id="chbShowOnlyNonDownloadedFiles" Text="Show only non-downloaded files."
				runat="server" AutoPostBack="True" oncheckedchanged="chbShowOnlyNonDownloadedFiles_CheckedChanged"></asp:checkbox></td>
	</tr>
	<TR class="TableHeaderRow">
		<td align="right">&nbsp;&nbsp;</td>
		<TD noWrap width="60%">
			<asp:LinkButton CssClass="X-SmallBold_White" id="Linkbutton2" runat="server" onclick="Linkbutton2_Click">File Name</asp:LinkButton>
			<asp:Label CssClass="X-Small_White" id="lblCount" runat="server"></asp:Label>
			<!-- <asp:CheckBox id="chbDownloadAllFiles" runat="server" Text="File Name" CssClass="ListHeader"></asp:CheckBox> -->
		</TD>
		<TD noWrap width="20%">
			<asp:LinkButton CssClass="X-SmallBold_White" id="LinkButton1" runat="server" onclick="LinkButton1_Click">Creation Date</asp:LinkButton>
		</TD>
		<td nowrap width="20%"><asp:Label CssClass="X-SmallBold_White" id="Label3" runat="server">Download Date</asp:Label>
		</td>
		<td align="right">&nbsp;&nbsp;</td>
	</TR>
	<tr width="100%">
		<td colspan="7" width="100%"><img src="../images/vspace.gif" width="100%" height="4" border="0"></td>
	</tr>
	<asp:repeater id="Listing1" runat="server">
		<HeaderTemplate>
		</HeaderTemplate>
		<ItemTemplate>
			<tr runat="server" id="lll" class="TableRow">
				<td nowrap="true" width="1" align="right">
					<asp:Image id="Image1" ImageUrl='../images/green-arrow.gif' runat="server" />
				</td>
				<td nowrap="true">
					<asp:CheckBox CssClass="FileName" id="chbFileNames" runat="server"></asp:CheckBox>
				</td>
				<td nowrap="true">
					<asp:Label id="lblCreationTime" CssClass="X-Small" runat="server"></asp:Label></td>
				<td nowrap="true">
					<asp:Label id="lblDownloadedTime" CssClass="X-Small" runat="server"></asp:Label>
				</td>
				<td align="right">&nbsp;&nbsp;</td>
			</tr>
		</ItemTemplate>
		<FooterTemplate>
		</FooterTemplate>
	</asp:repeater></table>
<script language="javascript">
<!--
	function checkBox_Selected(id)
	{									
		var pa = id.parentElement.parentElement.parentElement;
			
		var oldBGColor = pa.Title;
		
		if (oldBGColor == null)
			oldBGColor = pa.bgColor;		
										
		if (id.checked)		
		{	
			pa.bgColor = "#cccc00";		
		}
		else	
		{
			pa.bgColor = oldBGColor;			
		}
		
		pa.Title = oldBGColor;	
												
	}
//-->
</script>
