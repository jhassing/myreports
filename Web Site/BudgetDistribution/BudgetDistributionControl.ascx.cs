namespace DistributionPortal
{
	using System;
	using System.Collections;
	using System.Collections.Specialized;
	using System.ComponentModel;
	using System.Drawing;
	using System.Web;
	using System.Web.SessionState;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.IO;
	using System.Web.Security;

	using Compass.FileSystem.Permissions;
	using Compass.Security.ActiveDirectory;
	using BudgetDistribution.BLL;
	using MyReports.Common;

	using Microsoft.ApplicationBlocks.ExceptionManagement;

	
	public partial class BudgetDistributionControl : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.CheckBox chbFileNames;

		private BudgetDistributionBLL _oBLL;
		private int _recordCount = 0;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (Visible)
			{
				if (!Page.IsPostBack)
				{		
					Session["showOnlyNonDownloadedFiles"] = false;
					Session["sortASC"] = true;

					PopulateDBFiles();

					_recordCount = 0;
				}
			}			
		}

		private void PopulateDBFiles()
		{
			BudgetDistributionFiles oBudgetDistributionFiles = null;

			try
			{
				string pathToFiles = AppValues.BudgetDistribution_FileLocation_Folder;
				string filter = "*.*";

				oBudgetDistributionFiles = new BudgetDistributionFiles(	AppValues.UserName, 
																		AppValues.Password, 
																		AppValues.FullyQualifiedDomain, 
																		pathToFiles, 
																		filter);
				oBudgetDistributionFiles.ImpersonateUser();
			
				Session["bdFiles"] = oBudgetDistributionFiles.getBudgetDistributionFiles();

				BindGrid("creationdate");

			}
			catch ( Exception ex )
			{
				NameValueCollection nv = new NameValueCollection();				

				ExceptionManager.Publish ( new Exception("Error populating the Budget Distribution Files", ex), nv );

				throw (ex);
			}	
			finally
			{
				if (oBudgetDistributionFiles != null)
					oBudgetDistributionFiles.UndoImpersonate();

				oBudgetDistributionFiles = null;
			}

		}


		private void BindGrid(string sortColumn)
		{
			try
			{
				_oBLL = new BudgetDistributionBLL( AppValues.BD_DB_ConnectionString );

				IComparer myComparer = null;

				if (sortColumn.ToLower() == "filename")
				{
					myComparer = new BudgetDistribution_SORT_BY_FileName();
				}
				else if (sortColumn.ToLower() == "creationdate")
				{
					myComparer = new BudgetDistribution_SORT_BY_CreationDate();
				}

				Array.Sort((BUDGET_DISTRIBUTION_FILES[])Session["bdFiles"], myComparer);

				if (! ((bool)Session["sortASC"]) )
					Array.Reverse((BUDGET_DISTRIBUTION_FILES[])Session["bdFiles"]);

				this.Listing1.DataSource = (BUDGET_DISTRIBUTION_FILES[])Session["bdFiles"];
				Listing1.DataBind();

				_oBLL = null;

			}
			catch ( Exception ex )
			{
	
				throw (ex);
			}	
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Listing1.ItemDataBound += new System.Web.UI.WebControls.RepeaterItemEventHandler(this.Listing1_ItemDataBound);

		}
		#endregion

		private void Listing1_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{		
			if ( e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.SelectedItem)			
			{

				DateTime downloadedTime;				

				BUDGET_DISTRIBUTION_FILES o = (BUDGET_DISTRIBUTION_FILES)e.Item.DataItem;
						
				// Find the checkbox control.
				CheckBox cb = (CheckBox)e.Item.FindControl("chbFileNames");	

				// Find out if the file has ever been downloaded before
				if (_oBLL.HasFileEverBeenDownloaded(MyReports.Common.AppValues.UserName, o.FileName, o.TimeStamp, out downloadedTime))
				{
					if ( (bool) Session["showOnlyNonDownloadedFiles"])
					{
						// we are not to show downloaded files.
						e.Item.Visible = false;
						return;
					}

					// Set the downloaded date
					Label lb = (Label)e.Item.FindControl("lblDownloadedTime");	
					lb.Text = downloadedTime.ToString("g");					

					// Change the "row" style of the downloaded file									
					HtmlTableRow row = (HtmlTableRow)e.Item.FindControl("lll");	
					row.BgColor = "gainsboro";

					// Set the checkbox text to also say downloaded
					cb.Text = o.FileName + " (Downloaded)";
				}
				else
				{
					// File has never been downloaded before
					//row.Attributes.CssStyle.Add("background-color", "#f2f8f1");
					cb.Text = o.FileName;	
				}
																			
				// Set the checkbox "onclick" properties
				cb.Attributes.Add("onclick", "Javascript:checkBox_Selected(this);");				

				// Set the Creation time properties
				Label lbl = (Label)e.Item.FindControl("lblCreationTime");			
				//lbl.ToolTip = o.LastAccessTime.ToString();
				lbl.Text = o.TimeStamp.ToString("g");

				_recordCount++;

			}

			if (e.Item.ItemType == ListItemType.Footer)
			{
				//display the  record count				
				this.lblCount.Text = "(" + _recordCount + " records)";

			}

		}

		protected void btnDownloadFiles_Click(object sender, EventArgs e)
		{

			// Transfer to another page to process
			//Response.Redirect("ProcessBudgetDistributionFiles.aspx", false);

			BudgetDistributionFiles oBudgetDistributionFiles = null;

			try
			{
				oBudgetDistributionFiles = new BudgetDistributionFiles(	MyReports.Common.AppValues.UserName, 
																		MyReports.Common.AppValues.Password, 
																		MyReports.Common.AppValues.FullyQualifiedDomain);
				oBudgetDistributionFiles.ImpersonateUser();
				ArrayList ary = new ArrayList();

				BudgetDistributionBLL oBLL = new BudgetDistributionBLL(AppValues.BD_DB_ConnectionString);		
			
				// First, gather all the selected files, copy them to the temp directory.
				for (int yy=0;yy<Listing1.Items.Count;yy++)
				{	
					CheckBox cb = (CheckBox)Listing1.Items[yy].FindControl("chbFileNames");						
				
					if (cb.Checked)
					{
						// Remove the text "(Downloaded)" if it appears
						string sText = cb.Text.Replace("(Downloaded)", "").Trim();					
						ary.Add(sText);									
					}
				}

				if (ary.Count > 0)
				{
					// Pass the arraylist of checked files into the next class for processing.
					string zipFileLocation = oBudgetDistributionFiles.CreateZipFilesForDownload(ary);										

					for (int yy=0;yy<ary.Count;yy++)
					{
					
						string sFileName = ary[yy].ToString();
			
						DateTime dtFileModDate = oBudgetDistributionFiles.GetLastWriteTime( AppValues.BudgetDistribution_FileLocation_Folder  + sFileName);									

						oBLL.InsertFileDownloaded( AppValues.UserName, sFileName, dtFileModDate );
					}
				
					// Redirect and download the file			
					string sURL = Request.Url.AbsoluteUri;
					sURL = sURL.Substring(0, sURL.LastIndexOf('/') );
					sURL = sURL.Substring(0, sURL.LastIndexOf('/') );												

					//string fileNameEncrypted = Server.UrlEncode(MyReports.Common.Functions.Encrypt(zipFileLocation));

					//PopulateDBFiles();	
	
					if (oBudgetDistributionFiles != null)
						oBudgetDistributionFiles.UndoImpersonate();

					//Response.Redirect ( "../index/index.aspx?app=bd&DoFileDownload=true&FileName=" + fileNameEncrypted );	
					//Response.Redirect ( "../index/index.aspx?app=bd&DoFileDownload=true&FileName=" + zipFileLocation , false);						
					//Response.Redirect ( "../index/index.aspx?app=bd&DoFileDownload=true&FileName=" ,false);	
                    Response.Redirect("../Common/FileDownload/dodownload.aspx?SaveFileName=Budget.zip&Filename=" + zipFileLocation, false);
					//Server.Transfer("../index/index.aspx?app=bd&DoFileDownload=true&FileName=" + fileNameEncrypted );	

					PopulateDBFiles();		
				
				}
				else
					PopulateDBFiles();
			}
			catch(Exception ex)
			{
				throw(ex);
			}
			finally
			{
				if (oBudgetDistributionFiles != null)
					oBudgetDistributionFiles.UndoImpersonate();

				oBudgetDistributionFiles = null;
			}

		}

		protected void chbShowOnlyNonDownloadedFiles_CheckedChanged(object sender, System.EventArgs e)
		{
			Session["showOnlyNonDownloadedFiles"] = chbShowOnlyNonDownloadedFiles.Checked;
		
			// Re-populate the listing
			PopulateDBFiles();
		}

		protected void LinkButton1_Click(object sender, System.EventArgs e)
		{
			Session["sortASC"] = !(bool)Session["sortASC"];
			BindGrid("creationdate");
		}

		protected void Linkbutton2_Click(object sender, System.EventArgs e)
		{
			Session["sortASC"] = !(bool)Session["sortASC"];
			BindGrid("filename");
		}
	}



}

