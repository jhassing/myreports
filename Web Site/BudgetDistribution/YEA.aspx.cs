using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using YEA_Tools;
using Compass.Data.MSSQLDataAccess;

using MyReports.Common;

public partial class YEA : System.Web.UI.Page
{
    DataSet ds;
    SqlConnection conn;
    SqlDataAdapter daUnits;
    SqlCommand sqlCMD;
    DataView dv;
    string conStr1;
    

    protected void Page_Load(object sender, System.EventArgs e)
    {
        //conStr1 = ConfigurationManager.AppSettings["CONN_STR1"];
       conStr1 = AppValues.MyReports_DB_ConnectionString;
        conn = new SqlConnection(conStr1);
        SqlDataSource1.ConnectionString = conStr1;

        if (radPath.SelectedValue == "")
        {
            radPath.SelectedValue = "0";
            txtPath.Text = ConfigurationManager.AppSettings["BUDGET_PATH"];
        }
        txtLogPath.Text = ConfigurationManager.AppSettings["YEA_LOG_PATH"]+ "\\YEALogs";
     if (radPath.SelectedValue == "0")
        validPath();

        //conn = new SqlConnection("Data Source=CGUSCHD310\\DEV;Initial Catalog=MyReports_DEV;Integrated Security=True");
        if (!Page.IsPostBack)
        {
            DisplayData();
            regionDdlData();
        }
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        //this.dgPKunit.SortCommand += new System.Web.UI.WebControls.DataGridSortCommandEventHandler(this.dgPKunit_SortCommand);

    }
    #endregion

    private void DisplayData()
    {
        //connection to database
        //conn = new SqlConnection("Data Source=CGUSCHD310\\DEV;Initial Catalog=MyReports_DEV;Integrated Security=True");
        
        // dataset with information from the unitNum table
       // daUnits = new SqlDataAdapter("select * from tbl_UnitNumbers", conn);
       //-->>>>>>>>>>>.
        //daUnits = new SqlDataAdapter("select replace(str(UnitNumber, 5), ' ', '0') Unit, max(isnull(DM,0)) DM, " +
        //            "max(isnull(Region,0)) REG FROM tbl_UnitNumbers Left Outer JOIN zrops ON UnitNumber = Unit " +
        //            "Group by UnitNumber order by 1", conn);
        ds = new DataSet();
        //daUnits.Fill(ds, "tbl_UnitNumbers");
        //--<<

        SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "yea_GetUnitList", ds, new string[] { "tbl_unitNumbers" });
        

        //bind the datagrid to the DefaultView of the dataset
        dgUnits.DataSource = ds;
        dgUnits.DataMember = "tbl_UnitNumbers";
        dgUnits.DataBind();
        txtStatus.Text = dgUnits.Rows.Count + " units found.";

        //create a DataView to be used with selection criteria
        dv = new DataView(ds.Tables["tbl_UnitNumbers"]);
        //dv.RowFilter = "REG = 'qbg'";
        dv.RowFilter = "REG = '" + "qba" + "'";
        dv.RowFilter = "REG = '" + Convert.ToString(DDLpkReg.SelectedValue) + "'";
        //Convert.ToString(this.ddlFolder.SelectedValue)
        dgPKunit.DataSource = dv;
        dgPKunit.DataBind();

        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
            Unattended UA = new Unattended();
            UA.Run_Silent();
    }
   
    
    
    protected void btnRunBud_Click(object sender, EventArgs e)
    {
        if (radPath.SelectedValue == "0")
        {
            if (ReturnPathValid(txtPath.Text))
            {
                YEA_Trim();
            }
            else
            {
                txtStatus.Text = " ** PATH NOT VALID ** ";
            }
        }
        else
            {
                YEA_Trim();
            }
    }

    protected void btnRun1_Click(object sender, EventArgs e)
        {
           string uNumber; 
           uNumber = txtAdUnit.Text;

            if (uNumber.Length < 5)
                {
                    uNumber = "0" + uNumber;
                }
            if (uNumber.Length == 5)
                {   YEA_Trim();
                    txtStatus.Text = "Budget trim has been run for unit: " + uNumber;
                    txtAdUnit.Text = "";
                }
            else
                {
                    txtStatus.Text = "unit number entered must be 4 0r 5 digits";
                    txtAdUnit.Text = "";
                }
        }
         
    
    protected void YEA_Trim()
   {

       txtStatus.Text = "Running file trim process ...";

       CodeFile CF = new CodeFile();
       CF.BudgetLocation = txtPath.Text;
       CF.CreateLogFile("A");
       TextWriter outLog = new StreamWriter(CF.YEALog);
       outLog.WriteLine("Run by user:  " + AppValues.UserName);
       outLog.WriteLine(CF.CurrentTime + "     Starting Process of files in: " + CF.BudgetLocation);
       //CF.RowCount = this.dgUnits.RowCount - 1;

       if (txtAdUnit.Text.Length == 0)
            {
               if (dgPKunit.Rows.Count > 0)
               { CF.RowCount = this.dgPKunit.Rows.Count; }
               else
               { CF.RowCount = this.dgUnits.Rows.Count; }
            }
           else
            { 
                CF.RowCount = 1;
            }
       outLog.WriteLine(CF.CurrentTime + "     " + CF.RowCount + " unit numbers have been provided for processing");
       outLog.WriteLine("");

       if (txtAdUnit.Text.Length > 3)
       {
           CF.UnitNumber = txtAdUnit.Text;
          // CF.BudgetLocation = txtPath.Text; // test path from web form
           CF.ProcessUnitsBudget(outLog);
       }
       else
       {
           CF.RowIndex = CF.RowCount - 1;
           for (int i = 0; i <= CF.RowIndex; i++)
           {
               //CF.UnitNumber = this.dgUnits.Rows[i].Cells[0].Value.ToString();
               if (dgPKunit.Rows.Count > 0)
               { CF.UnitNumber = this.dgPKunit.Rows[i].Cells[0].Text.ToString(); }
               else
               { CF.UnitNumber = this.dgUnits.Rows[i].Cells[0].Text.ToString(); }
               // outLog.WriteLine(CF.CurrentTime + "     Starting process for unit " + CF.UnitNumber);
               CF.BudgetLocation = txtPath.Text; // test path from web form
               CF.ProcessUnitsBudget(outLog);
               // outLog.WriteLine(CF.CurrentTime + "     Process complete for unit " + CF.UnitNumber);
               outLog.WriteLine("");
           }
        }
        
        
        
       outLog.WriteLine(CF.CurrentTime + "     Process Complete...");
       outLog.Close();

       txtStatus.Text = "Process Complete for " + CF.RowCount + " units";
    
    }
    protected void DDLpkReg_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        //daUnits = new SqlDataAdapter("select replace(str(UnitNumber, 5), ' ', '0') Unit, max(isnull(DM,0)) DM, " +
        //            "max(isnull(Region,0)) REG FROM tbl_UnitNumbers Left Outer JOIN zrops ON UnitNumber = Unit " +
        //            "Group by UnitNumber order by 1", conn);
        //ds = new DataSet();
        //daUnits.Fill(ds, "tbl_UnitNumbers");

        ds = new DataSet();
        SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "yea_GetUnitList", ds, new string[] { "tbl_unitNumbers" });
        
        //create a DataView to be used with selection criteria
        dv = new DataView(ds.Tables["tbl_UnitNumbers"]);
        dv.RowFilter = "REG = '" + "qba" + "'";
        dv.RowFilter = "REG = '" + Convert.ToString(DDLpkReg.SelectedValue) + "'";
        dgPKunit.DataSource = dv;
        dgPKunit.DataBind();

        if (dgPKunit.Rows.Count > 0)
        {
            txtStatus.Text = dgPKunit.Rows.Count + " units ready to process for selected region ";
            dgUnits.Visible = false;
        }
        else
        {
            txtStatus.Text = "No units found for selected region. ";
            dgUnits.Visible = true;
        }
       }

    public void regionDdlData()
    {
        //SqlDataAdapter daReg;
       // int i; i = 0;
       // daReg = new SqlDataAdapter("SELECT DISTINCT [Region] FROM [zrops] ORDER BY [Region]", conn);
       // ds = new DataSet();        
                
        ds = new DataSet();
        SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "yea_GetRegCodes", ds, new string[] { "zrops" });
        
        //daReg.Fill(ds, "zrops");
        DataTable dTable = ds.Tables[0];
        DDLpkReg.Items.Add("");
        foreach (DataRow dRow in dTable.Rows)
        {
           // i++;
           // txtStatus.Text = dTable.Rows.Count + " ROWS: " + i.ToString() + " Region: " + dRow["Region"].ToString();
            DDLpkReg.Items.Add(dRow["Region"].ToString());
        }
       
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
       string strSQL;
       string uNumber; 
       uNumber = txtAdUnit.Text;

        if (uNumber.Length < 5)
            {
                uNumber = "0" + uNumber;
            }
        if (uNumber.Length == 5)
            {
                //SqlDataAdapter da = new SqlDataAdapter();
                // da.SelectCommand = new SqlCommand("Select * from tbl_UnitNumbers", conn);
                // DataSet ds = new DataSet();
                // da.Fill(ds, "tbl_UnitNumbers");

                // DataTable dt = ds.Tables["tbl_UnitNumbers"];
                // DataRow newRow = dt.NewRow();
                //  newRow["UnitNumber"] = Convert.ToString(txtAdUnit.Text);
                //  dt.Rows.Add(newRow);

                CodeFile cf = new CodeFile();
                cf.updUnitTbl(uNumber,"A");

                //strSQL = "INSERT INTO [tbl_UnitNumbers] ([UnitNumberID], [UnitNumber]) VALUES (" + "''" + ", '" + uNumber + "')";
                //SqlCommand sqlCMD = new SqlCommand(strSQL, conn);

                //sqlCMD.Connection = conn;
                //conn.Open();
                //sqlCMD.ExecuteNonQuery();
                //conn.Close();
                DisplayData();

                txtStatus.Text = uNumber + " has been added to the database.";
                txtAdUnit.Text = "";
                //  = New SqlCommand(strSql, conn);
                // ExecuteNonQuery();

                // strSQL = "INSERT INTO [tbl_UnitNumbers] ([UnitNumberID], [UnitNumber]) VALUES (" + "''''" + "," + txtAdUnit.Text + ")";


                //DataRow dr = ds.Tables["tbl_UnitNumbers"].NewRow();
                //dr["UnitNumber"] = txtAdUnit.Text;
                //ds.Tables["UnitNumber"].Rows.Add(dr);
            }
        else
            {
                txtStatus.Text = "unit number entered must be 4 0r 5 digits";
                txtAdUnit.Text = "";
            }
    }

    protected void btnTrunc_Click(object sender, EventArgs e)
    {
        CodeFile cf = new CodeFile();
        cf.updUnitTbl("0000", "T");
        DisplayData();
    }

    protected void btnLoadZ_Click(object sender, EventArgs e)
    {
        CodeFile cf = new CodeFile();
        cf.updUnitTbl("0000", "Z");
        DisplayData();
    }

    protected void btnDel_Click(object sender, EventArgs e)
    {
        string strSQL;
        string uNumber;
        uNumber = txtAdUnit.Text;

        if (uNumber.Length < 5)
        {
            uNumber = "0" + uNumber;
        }
        if (uNumber.Length == 5)
        {
            
            CodeFile cf = new CodeFile();
            cf.updUnitTbl(uNumber, "D");
                        
            //strSQL = "delete from [tbl_UnitNumbers] where [UnitNumber]= " + uNumber + "";
            //SqlCommand sqlCMD = new SqlCommand(strSQL, conn);

            //sqlCMD.Connection = conn;
            //conn.Open();
            //sqlCMD.ExecuteNonQuery();
            //conn.Close();
            DisplayData();

            txtStatus.Text = uNumber + " has been DELETED from the database.";
            txtAdUnit.Text = "";
         
        }
        else
        {
            txtStatus.Text = "unit number entered must be 4 0r 5 digits";
            txtAdUnit.Text = "";
        }
    }

    protected void radPath_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (radPath.SelectedValue == "0")
            txtPath.Text = ConfigurationManager.AppSettings["BUDGET_PATH"];
        if (radPath.SelectedValue == "1")
            txtPath.Text = ConfigurationManager.AppSettings["BUDGET_WEB_PATH"];

        if (radPath.SelectedValue == "0")
            validPath();

    }

    protected void validPath()
    {
        // Validate folder for file trim

        try
        {
            DirectoryInfo folder = new DirectoryInfo(txtPath.Text);
            if (!folder.Exists)
            {
                txtStatus.Text = "PATH NOT VALID: " + txtPath.Text;
            }
        }
        catch (Exception er)
        {
            txtStatus.Text = "Path Format Error: " + er;
        }
        
      
    }



    /// Method to determine if a file is open
    /// <param name="Path">Path to check</param>
    /// <returns>Boolean value</returns>
    public static Boolean ReturnPathValid(string Path)
    {
        Boolean isValid = true;
        //always use a try...catch to deal 
        //with any exceptions that may occur
        try
        {
            DirectoryInfo folder = new DirectoryInfo(Path);
            if (!folder.Exists)
            {isValid = false; }
        }
        catch
        {
            isValid = false;
        }
        return isValid;
    }







 }
