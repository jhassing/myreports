<%@ Register TagPrefix="actuate" TagName="logout" Src="logout.ascx" %>
<%@ Page language="c#" Inherits="activeportal.Migrated_logout" CodeFile="logout.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>logout</title>
		<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Expires" content="-1">
	</HEAD>
	<body>
		<P>
			<actuate:logout id="Logout" LogoutTarget="~/login.aspx" runat="server"></actuate:logout></P>
	</body>
</HTML>
