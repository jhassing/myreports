<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RDList.aspx.cs" Inherits="DistributionPortal.RDList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register TagPrefix="uc1" TagName="categoriesview" Src="../filesfolders/categoriesview.ascx" %>
<%@ Register TagPrefix="uc1" TagName="navigation" Src="../Common/navigation.ascx" %>
<%@ Register TagPrefix="uc1" TagName="popupmenu" Src="../filesfolders/popupmenu.ascx" %>
<%@ Register TagPrefix="uc1" TagName="separator" Src="../Common/separator.ascx" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
		<LINK href="../css/allstyles.css" type="text/css" rel="stylesheet">
	</head>
	<body leftmargin="0" rightmargin="0" bottommargin="0" topmargin="0" scroll="auto">
		<form id="Form1" method="post" runat="server">
		     <div>
			    <uc1:navigation id="navigation1" runat="server"></uc1:navigation>
			    <uc1:categoriesview id="Categoriesview1" runat="server"></uc1:categoriesview>
			</div>			
		</form>
	</body>
</HTML>
