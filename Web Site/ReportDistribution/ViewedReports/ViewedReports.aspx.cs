using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI.WebControls;
// MyReports refs
using MyReports.Common;
// for error message reports to page
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.DAL;
using ReportDistribution.SQLServerDAL;

public partial class ReportDistribution_ViewedReports_ViewedReports : System.Web.UI.Page
{
    private DateTime dt_startdate = DateTime.Now.AddMonths(-1);
    private DateTime dt_enddate = DateTime.Now;
    private string s_reporttype;
    private List<string> regionsList = new List<string>();
    private List<string> districtsList = new List<string>();
    private List<string> costcentersList = new List<string>();
    private string s_costcenters;
//    private ViewedReportObject[] viewedReportList;
    private List<ViewedReportObject> viewedReportList = new List<ViewedReportObject>();

    /* private ViewedReportObject[] ViewedReportList
     {
         get 
         {
             //return (ViewedReportObject[])ViewState["ViewedReportList"];;
             return viewedReportList;
         }
         set
         {
             //ViewState["ViewedReportList"] = value;
             viewedReportList = value;
         }
     }*/
    private List<ViewedReportObject> ViewedReportList
     {
         get 
         {
             return viewedReportList;
         }
         set 
         {
             viewedReportList = value;
         }
     }
   
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        lblErrorMessage.Visible = false;
        
        // object[] objs = new object[dlsViewedReports.Items.Count];
        //viewedReportList = new ViewedReportObject[objs.Length];
        //dlsViewedReports.Items.CopyTo(objs, 0);
        //for (int i = 0; i < objs.Length; i++) 
        //{
        //    viewedReportList[i] = dlsViewedReports.Items[i];
        //}

        // clear the datalist dlsViewedReports, the DataSet is null due to page load/reload
        if (!IsPostBack)
        {
            SetToSearchMode();
        }
        dlsViewedReports.DataBind();
        if (txtStartDate.Text == "")
            txtStartDate.Text = dt_startdate.ToShortDateString();
        if (txtEndDate.Text == "")
            txtEndDate.Text = dt_enddate.ToShortDateString();
        if (ddlRegion.SelectedValue == "")
        {
            ddlDMs.Enabled = false;
            ddlCC.Enabled = false;
        }
        s_reporttype = rblReportType.SelectedValue;
    }
    #endregion

    #region Fill Region Drop Down Listbox
    private void FillRegionList()
    {
        try
        {
//            regionsList = new Zrops(MyReports.Common.AppValues.MyReports_DB_ConnectionString).GetDistinctRegionCodeListing();
            regionsList = new Zrops(MyReports.Common.AppValues.MyReportsDB).GetDistinctRegionCodeAndNameListing();
            regionsList.Insert(0, "");
            ddlRegion.DataSource = regionsList;
            ddlRegion.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Fill District Drop Down Listbox
    private void FillDistrictList()
    {
        try
        {
            if (ddlRegion.SelectedValue != "")
            {
                List<string> dl = new Zrops(MyReports.Common.AppValues.MyReportsDB).GetDmAndFolderNameByRegion(ddlRegion.SelectedValue.Split('-')[0].Trim());
                districtsList.Add("");
                districtsList.AddRange(dl);
                ddlDMs.DataSource = districtsList;
                ddlDMs.DataBind();
                ddlDMs.SelectedValue = "";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Fill Cost Center Drop Down Listbox
    private void FillCostCenterDropDownList()
    {
        try
        {
            List<string> ccl = new List<string>();
            if (ddlDMs.SelectedValue != "")
            {
                ccl = new Zrops(MyReports.Common.AppValues.MyReportsDB).GetUnitByDm(ddlDMs.SelectedValue.Split('-')[0].Trim());
            }
            else
            {
                ccl = new Zrops(MyReports.Common.AppValues.MyReportsDB).GetUnitByRegion(ddlRegion.SelectedValue.Split('-')[0].Trim());
            }
            costcentersList.Add("");
            costcentersList.AddRange(ccl);
            ddlCC.DataSource = costcentersList;
            ddlCC.DataBind();
            ddlCC.SelectedValue = "";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Fill Selected Cost Center List
    private void FillCostCenterList()
    {
        try
        {
            if (ddlCC.SelectedValue != "") { costcentersList.Add(ddlCC.SelectedValue); }
            else if (ddlDMs.SelectedValue != "") { costcentersList = new Zrops(MyReports.Common.AppValues.MyReportsDB).GetUnitByDm(ddlDMs.SelectedValue.Split('-')[0].Trim()); }
            else if (ddlRegion.SelectedValue != "") { costcentersList = new Zrops(MyReports.Common.AppValues.MyReportsDB).GetUnitByRegion(ddlRegion.SelectedValue.Split('-')[0].Trim()); }
            else { costcentersList.Clear(); }
            SortAndBindListBox(lstSelected, costcentersList);
            ApplyCostCenterFilter();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Fill Viewed Report List
    private void FillViewedReportList()
    {
        try
        {
            dt_startdate = Convert.ToDateTime(txtStartDate.Text);
            dt_enddate = Convert.ToDateTime(txtEndDate.Text).AddDays(1);
            s_reporttype = this.rblReportType.SelectedValue;
            s_costcenters = SetCostCentersList();
            //ViewedReportList = new ViewedReport(MyReports.Common.AppValues.MyReports_DB_ConnectionString).GetViewedReports(dt_startdate, dt_enddate, s_reporttype, s_costcenters, chbViewed.Checked, chbNotViewed.Checked).ToArray();
            ViewedReportList = new ViewedReport(MyReports.Common.AppValues.MyReportsDB).GetViewedReports(dt_startdate, dt_enddate, s_reporttype, s_costcenters, chbViewed.Checked, chbNotViewed.Checked);

            
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }

    }
    #endregion

    #region Sort And Bind List Box
    private void SortAndBindListBox(ListBox lb, List<string> items)
    {
        try
        {
            lb.Items.Clear();

            for (int i = 0; i < items.Count; i++)
            {
                lb.Items.Add(new ListItem(items[i]));
            }
        }
        catch (Exception ex)
        {
            ExceptionManager.Publish(ex);
        }
    }

    #endregion

    #region Set Cost Centers List
    private string SetCostCentersList()
    {
        string retstring = "";
        try
        {
            foreach (ListItem li in lstSelected.Items)
            {
                retstring += li.Text.Trim() + "|";
            }
            return retstring;
        }
        catch (Exception ex)
        {
            ExceptionManager.Publish(ex);
        }
        return retstring;
    }
    #endregion

    #region Apply Cost Center Filter
    private void ApplyCostCenterFilter()
    {
        try
        {
            lstAvailable.Items.Clear();
            foreach (string cc in costcentersList)
            {
                if (lstSelected.Items.Contains(new ListItem(cc)) != true)
                    lstAvailable.Items.Add(cc);
            }
        }
        catch (Exception ex)
        {
            ExceptionManager.Publish(ex);
        }
    }
    #endregion

    #region display error
    private void DisplayError(Exception ex)
    {
        this.lblErrorMessage.Visible = true;
        string mess = ex.Message;
        mess += "\r\n";

        if (ex.InnerException != null)
            mess += ex.InnerException.Message;

        this.lblErrorMessage.Text = mess;

        ExceptionManager.Publish(ex);
    }
    #endregion

    #region Make Report File
    private FileStream MakeReportFile(string reportfilename)
    {
        string tempdir = Functions.GetTempDirectoryForThisUser();
        if (Directory.Exists(tempdir) == false)
            Directory.CreateDirectory(tempdir);

        string reportfilepathname = tempdir + reportfilename;
        FileStream reportfile = new FileStream(reportfilepathname, FileMode.Create, FileAccess.ReadWrite);
        reportfile.Close();
        try
        {
            StreamWriter sw = new StreamWriter(reportfilepathname);
            string header = "\"" + s_reporttype + " reports from " + dt_startdate.ToShortDateString() + " to " + dt_enddate.ToShortDateString() + "\"";
            //string costcenterlist = "\"from units\",\"" + s_costcenters.TrimEnd('|').Replace("|", "\",\"") + "\"";
            string cols = "\"User Name\",\"Unit Number\",\"Unit Name\",\"Creation Time\",\"Viewed Time\",\"Viewed Delay\"";
            sw.WriteLine(header);
            //sw.WriteLine(costcenterlist); 
            sw.WriteLine(cols);
            //foreach (ViewedReportObject viewedreport in reportlisting)
            foreach (ViewedReportObject viewedreport in ViewedReportList)
            {
                string line = "";
                line += "\"";
                line += viewedreport.UserName + "\",\"";
                line += viewedreport.CostCenterID + "\",\"";
                line += viewedreport.CostCenterName + "\",\"";
                line += viewedreport.BurstDate + "\",\"";
                line += viewedreport.ViewedDate + "\",\"";
                line += viewedreport.Delay + "\"";
                sw.WriteLine(line);
            }

            sw.Close();
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
        return reportfile;
    }
    #endregion

    #region Download Report
    private void downloadreport(FileStream reportfile)
    {
        try
        {
            string reportfilepathname = reportfile.Name;
            string reportfilename = reportfilepathname.Split('\\')[reportfilepathname.Split('\\').Length-1].Trim();
            string newpage = "../../Common/FileDownload/dodownload.aspx";
            string qryString = "?SaveFileName=" + reportfilename + "&Filename=" + reportfilepathname;
            string url = newpage + qryString;

            //redirect to new file but keep the existing page up
            Response.Redirect(url, false);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    #endregion

    #region Set Mode
    private void SetToListMode()
    {
        tblView.Visible = true;
        tblSearch.Visible = false;
    }

    private void SetToSearchMode()
    {
        tblView.Visible = false;
        tblSearch.Visible = true;
    }
    #endregion

    #region Control Event Methods
    protected void btnView_Click(object sender, EventArgs e)
    {
        try
        {
            SetToListMode();
            FillViewedReportList();
            dlsViewedReports.DataSource = (object)ViewedReportList;
            dlsViewedReports.DataBind();
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    protected void ddlRegion_OnPreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
            FillRegionList();
        if (ddlRegion.SelectedValue == "")
            lblRegionTag.ForeColor = System.Drawing.Color.Red;
        else
            lblRegionTag.ForeColor = System.Drawing.Color.Black;
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            // make a list to hold all the new cost centers selected        
            ListItemCollection add = new ListItemCollection();

            foreach (ListItem li in lstAvailable.Items)
            {
                if (li.Selected)
                {
                    if (lstSelected.Items.Contains(li) == false)
                    {
                        add.Add(li);
                    }
                }
            }
            foreach (ListItem li in add)
            {
                lstSelected.Items.Add(li);
                lstAvailable.Items.Remove(li);
            }
            lstAvailable.DataBind();
            lstSelected.DataBind();
        }
        catch (Exception ex)
        {
            ExceptionManager.Publish(ex);
        }
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        try
        {
            ListItemCollection remove = new ListItemCollection();
            foreach (ListItem li in lstSelected.Items)
            {

                if (li.Selected == true)
                {
                    remove.Add(li);
                }
            }
            foreach (ListItem li in remove)
            {
                lstAvailable.Items.Add(li);
                lstSelected.Items.Remove(li);
            }
            lstAvailable.DataBind();
            lstSelected.DataBind();
        }
        catch (Exception ex)
        {
            ExceptionManager.Publish(ex);
        }
    }
    protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlRegion.SelectedValue != "")
        {
            btnView.Enabled = true;
            btnExport.Enabled = true;
            ddlDMs.Enabled = true;
            ddlCC.Enabled = true;
        }
        else 
        {
            btnView.Enabled = false;
            btnExport.Enabled = false;
            ddlDMs.Enabled = false;
            ddlCC.Enabled = false;
        }
        ddlDMs.Items.Clear();
        ddlCC.Items.Clear();
        FillDistrictList();
        FillCostCenterDropDownList();
        FillCostCenterList();
    }
    protected void ddlDMs_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlCC.Items.Clear();
        FillCostCenterDropDownList();
        FillCostCenterList();
    }  
    protected void ddlCC_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCostCenterList();
    }
    protected void btnExportToFile_Click(object sender, EventArgs e)
    {
        FillViewedReportList();
        string reportfilename = "ViewedReports_" + s_reporttype + "_" + dt_startdate.ToShortDateString().Replace('/', '-') + "_" + dt_enddate.ToShortDateString().Replace('/', '-') + ".csv";
        FileStream reportfile = MakeReportFile(reportfilename);
        downloadreport(reportfile);
    }
    protected void btnBack_OnClick(object sender, EventArgs e)
    {
        SetToSearchMode();
    }
    #endregion
}
