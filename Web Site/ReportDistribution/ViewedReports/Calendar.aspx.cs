using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ReportDistribution_ViewedReports_Calendar : System.Web.UI.Page
{
    protected Calendar calDate = new Calendar();

    private void Page_Load(object sender, EventArgs e)
    {
        control.Value = Request.QueryString.Get("Ctl").ToString();
    }

    protected void calStartDate_SelectionChanged(Object sender, EventArgs e)
    {
        calDate.SelectedDate = this.calStartDate.SelectedDate;
        String strScript = "<script>window.opener.document.forms(0)." + control.Value + ".value = '";
        strScript += calDate.SelectedDate.ToString("MM/dd/yyyy");
        strScript += "';self.close();";
        strScript += "</" + "script>";
        // DO NOT USE this.GetType() USE typeof() JUST IN CASE THIS PAGE EVER GETS INHERITED
        ClientScript.RegisterClientScriptBlock(typeof(Page), "none", strScript);
    }
}
