<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewedReports.aspx.cs" Inherits="ReportDistribution_ViewedReports_ViewedReports" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head runat="server">
        <title>Viewed Reports</title>
        <link href="../../css/allstyles.css" rel="stylesheet" type="text/css" />
        <script language=javascript>
            function PopupPicker(ctl,w,h)
            {
                var PopupWindow=null;
                if (navigator.appName == "Microsoft Internet Explorer")
                {
                    //x = event.x;
                    //y = event.y;
                    x = screenLeft + event.x;
                    y = screenTop + event.y;
                }
                else 
                {
                    x = screenX + e.x;
                    y = screenY + e.y;
                }
                // debug where was the mouse click and which browser was used
                // alert("Click at x = " + x + " and y = " + y + "\n" + "Browser: " + navigator.appName);
                settings='width='+ w + ',height='+ h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no, dependent=no, top=' + y + ', left=' + x;
                PopupWindow=window.open('Calendar.aspx?Ctl=' + ctl,'DatePicker', settings);
                PopupWindow.focus();
            }
        </script>
    </head>
    <body>
        <form id="form1" runat="server">
            <div>
                <table id=tblSearch runat="server">
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="lblDateRange" Text="Select a date range" CssClass="X-SmallBold" runat="server" />
                            <br /><br />
                            <asp:Label CssClass="XX-Small" ID="lblStartDateTag" Text="Starting Date:" runat="server" />
                            <asp:TextBox CssClass="XX-Small"  ID="txtStartDate" runat="server" />
                            <input type="image" src="../../images/calendar.gif" value="pick" onclick="PopupPicker('txtStartDate', 250, 190);">&nbsp;&nbsp;
                            <asp:Label CssClass="XX-Small" ID="lblEndDateTag" Text="Ending Date:" runat="server" />
                            <asp:TextBox CssClass="XX-Small" ID="txtEndDate" runat="server" />
                            <input type="image" src="../../images/calendar.gif" value="pick" onclick="PopupPicker('txtEndDate', 250, 190);">
                        </td>
                    </tr>
                    <tr><td colspan="3"><hr /></td></tr>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="lblReportType" runat="server" CssClass="X-SmallBold" Text="Select AR or Subsidy Reports" />
                            <br />
                            <asp:RadioButtonList CssClass="XX-Small" ID="rblReportType" runat="server" RepeatDirection="Horizontal" CellPadding="10" >
                                <asp:ListItem Text="AR Report" Value="ZCAR2201" />
                                <asp:ListItem Text="Subsidy Invoice" Value="ZCAR2821" Selected="True" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr><td colspan="3"><hr /></td></tr>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="lblViewed" runat="server" CssClass="X-SmallBold" Text="Make choices to see viewed and/or unviewed reports" />
                            <br /><br />
                            <asp:CheckBox ID="chbViewed" CssClass="XX-Small" runat="server" Text="Include Viewed Reports" Checked="true" />&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chbNotViewed" CssClass="XX-Small" runat="server" Text="Include Not Viewed Reports" Checked="true" />
                        </td>
                    </tr>
                    <tr><td colspan="3"><hr /></td></tr>
                    <tr>
                        <td colspan="3" valign="top">
                            <asp:Label ID="lblSelectOU" runat="server" CssClass="X-SmallBold" Text="Select a region and filter on District and/or Cost Center" />
                            <br /><br />
                            <asp:Label ID="lblRegionTag" runat="server" Text="Region:" CssClass="XX-Small" />  
                            <asp:DropDownList ID="ddlRegion" runat="server" CssClass="XX-Small" AutoPostBack="true" Width="210px" OnPreRender="ddlRegion_OnPreRender" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" />&nbsp;&nbsp;
                            <asp:Label ID="lblDMTag" runat="server" Text="District:" CssClass="XX-Small" />
                            <asp:DropDownList ID="ddlDMs" runat="server" CssClass="XX-Small" AutoPostBack="true" Width="150px" OnSelectedIndexChanged="ddlDMs_SelectedIndexChanged" />&nbsp;&nbsp;
                            <asp:Label ID="lblCCTag" runat="server" Text="Cost Center:" CssClass="XX-Small" />
                            <asp:DropDownList ID="ddlCC" CssClass="XX-Small" runat="server" AutoPostBack="true" Width="80px" OnSelectedIndexChanged="ddlCC_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr><td colspan=3><hr /></td>
                    </tr>
                    <tr>
                        <td><asp:Label ID=lblAvailable Font-Bold="true" CssClass="XX-Small" runat="server" Text="Available Cost Centers" /></td>
                        <td></td>
                        <td><asp:Label ID=lblSelected Font-Bold="true" CssClass="XX-Small" runat="server" Text="Selected Cost Centers" /></td>
                    </tr>                
                    <tr>
                        <td>
                            <asp:listbox id="lstAvailable" runat="server" CssClass="XX-Small" SelectionMode="Multiple" Height="220px" Width="270px" />
                        </td>
                        <td align="center" style="height: 202px">
                            <asp:button id="btnAdd" runat="server" CssClass="MyReportsAdminConsoleButton" Width="80px" Text="Add >>" OnClick="btnAdd_Click"></asp:button>
                            <br>
                            <br>
                            <asp:button id="btnRemove" runat="server" CssClass="MyReportsAdminConsoleButton" Width="80px" Text="<< Remove" OnClick="btnRemove_Click"></asp:button>
                        </td>
                        <td>
                            <asp:listbox id="lstSelected" runat="server" CssClass="XX-Small" SelectionMode="Multiple" Height="220px" Width="270px" />
                        </td>
                    </tr>
                    <tr><td colspan="3"><hr /></td></tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnView" runat="server" CssClass="XXL-MyReportsAdminConsoleButton" Text="View on page" OnClick="btnView_Click" Enabled="false" />
                        </td>
                        <td align="right">
                            <asp:button id="btnExport" runat="server" CssClass="XXL-MyReportsAdminConsoleButton" Text="Export to file" OnClick="btnExportToFile_Click" Enabled="false" />
                        </td>
                    </tr>
                </table>
                <table runat="server" id="tblView" width="725px" >
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="MyReportsAdminConsoleButton" Width="100" OnClick="btnBack_OnClick" />
                        </td>
                    </tr>                    
                </table>

                <asp:DataList ID="dlsViewedReports" runat="server" CssClass="XX-Small" Width="725">
                    <HeaderStyle Font-Bold="true" BackColor="lightgray" />
                    <AlternatingItemStyle BackColor="LightGray" />
                    <HeaderTemplate>
                        <table align="left" id=tblItemTemp runat="server" border="0" bordercolor="black" >
                            <tr>
                                <td width="140">User Name</td>
                                <td width="80">Unit Number</td>
                                <td width="200">Unit Name</td>
                                <td width="80">Creation Time</td>
                                <td width="80">Viewed Time</td>
                                <td width="80">Viewed Delay</td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table align="left" id=tblItemTemp runat="server" border="0" bordercolor="black">
                            <tr>
                                <td width="140"><%# Eval("UserName") %> </td>
                                <td width="80"><%# Eval("CostCenterID")%> </td>
                                <td width="200"><%# Eval("CostCenterName")%> </td>
                                <td width="80"><%# Eval("BurstDate")%> </td>
                                <td width="80"><%# Eval("ViewedDate")%> </td>
                                <td width="80"><%# Eval("Delay")%></td>
                            </tr>
                        </table>   
                    </ItemTemplate>
                </asp:DataList>
                <asp:Label ID="lblErrorMessage" runat="server" CssClass="X-Small" ForeColor="Red" Height="41px" Text="[Message]" Visible="False" Width="725px"></asp:Label>
            </div> 
        </form>
    </body>
</html>
