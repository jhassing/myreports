<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Calendar.aspx.cs" Inherits="ReportDistribution_ViewedReports_Calendar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
        <title>DatePicker</title>
        <script language="javascript"></script>
    </head>
    <body  leftmargin=0 rightmargin=0 topmargin=0 bottommargin=0>
        <form id="form1" runat="server">
            <div>
                <asp:Calendar ID="calStartDate" CssClass="XX-Small" runat="server" OnSelectionChanged="calStartDate_SelectionChanged" ></asp:Calendar>
                <input type="hidden" id="control" runat="server" />
                <asp:Label ID=lblCtrl runat="server" />
            </div>
        </form>
    </body>
</html>