<%@ Register TagPrefix="cc1" Namespace="AegisControls" Assembly="AegisControls" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailSearch.ascx.cs" Inherits="ReportDistribution_SelectedFiles_EmailSearch" %>
<TABLE style="margin-bottom:8px" cellSpacing="0" cellPadding="0" width="90%" border="0">
	<TR>
		<TD colspan="3" class="TableHeaderRow" id="td1" vAlign="middle" noWrap runat="server">
			<asp:Label id="Label7" CssClass="X-SmallBold_White" Runat="server">Client Email Search</asp:Label></TD>
	</TR>
</TABLE>
  <table border="0" cellpadding="0" cellspacing="6">
    <tr>
         <td nowrap style="width: 152px" align="right">
            <asp:Label ID="Label2" CssClass="X-Small" runat="server" Text="Client Email Address:"></asp:Label>
            </td>
         <td nowrap style="width: 400px" >
         <cc1:MaskedTextBox autocomplete="off"  id="txtClientEmailAddress" CssClass="XX-SmallTextBox" runat="server" Width="100%"
				MaxLength="200" Format="Text"></cc1:MaskedTextBox>
            </td>
    </tr>
    <tr>
        <td nowrap style="width: 152px" align="right">
            <asp:Label CssClass="X-Small" ID="Label1" runat="server" Text="Client Name:"></asp:Label></td>
         <td nowrap style="width: 400px" >
            <cc1:MaskedTextBox autocomplete="off"  id="txtClientName" CssClass="XX-Small" runat="server" Width="100%"
				MaxLength="200" Format="Text"></cc1:MaskedTextBox>
            </td>
    </tr>
    <tr>
         <td nowrap style="width: 152px" align="right">
            <asp:Label ID="Label3" runat="server" CssClass="X-Small" Text="Cost Center:"></asp:Label></td>
         <td nowrap style="width: 400px" >
          <cc1:MaskedTextBox autocomplete="off"  id="txtKeyWord" CssClass="XX-SmallTextBox" runat="server" Width="100%"
				MaxLength="200" Format="Text"></cc1:MaskedTextBox>
            </td>
    </tr>    
    <tr>
         <td nowrap style="width: 152px" align="right">
            <asp:Label ID="Label4" runat="server" CssClass="X-Small" Text="Accountant First Name:"></asp:Label></td>
         <td nowrap style="width: 400px" >
          <cc1:MaskedTextBox autocomplete="off"  id="txtAccountantFN" CssClass="XX-SmallTextBox" runat="server" Width="100%"
				MaxLength="50" Format="Text"></cc1:MaskedTextBox>
            </td>
    </tr>     
    <tr>
         <td nowrap style="width: 152px" align="right">
            <asp:Label ID="Label6" runat="server" CssClass="X-Small" Text="Accountant Last Name:"></asp:Label></td>
         <td nowrap style="width: 400px" >
          <cc1:MaskedTextBox autocomplete="off"  id="txtAccountantLN" CssClass="XX-SmallTextBox" runat="server" Width="100%"
				MaxLength="100" Format="Text"></cc1:MaskedTextBox>
            </td>
    </tr>   
    <tr>
         <td nowrap style="width: 152px" align="right">
            <asp:Label ID="Label8" runat="server" CssClass="X-Small" Text="Region:"></asp:Label></td>
         <td nowrap style="width: 400px" >
                 <cc1:MaskedTextBox autocomplete="off"  id="txtRegion" CssClass="XX-SmallTextBox" runat="server" Width="100%"
				MaxLength="20" Format="Text"></cc1:MaskedTextBox>
            </td>
    </tr>     
    <tr>
        <td nowrap colspan="2" align="right">
            
            <asp:Button ID="btnSearch" CssClass="MyReportsAdminConsoleButton" runat="server" Text="Search" CommandName="" OnClick="btnSearch_Click" />&nbsp;&nbsp;
            <asp:Button ID="btnAddNew" CssClass="MyReportsAdminConsoleButton" runat="server" Text="Add New" CommandName="addnewclientemailaddressfromsearch" OnClick="btnAddNew_Click" />
        </td>        
    </tr>      
</table>
<asp:Label Visible=false ID="lblMessage" ForeColor="Red" runat="server" CssClass="X-Small" Text="[Message]"></asp:Label>            
<br />

<asp:GridView ID="GridView1" runat="server" DataSourceID="ObjectDataSource1" Width="90%" CellPadding="2" 
            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" 
            DataKeyNames="ClientEmailAddressID,FromEmailAddress,CC1,CC2,BCC1" AllowSorting="false" AllowPaging="True" PageSize="6"
             OnRowCommand="GridView1_RowCommand">
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <EditRowStyle BackColor="#999999" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" Height="12px" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" CssClass="X-Small_White" HorizontalAlign="Left" VerticalAlign="Bottom" />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <Columns>
        <asp:CommandField SelectText="Edit" ShowSelectButton="True" EditImageUrl="../../images/edit.gif" EditText="" SelectImageUrl="~/images/edit.gif" ButtonType="Image" >
            <ItemStyle Wrap=False CssClass="hyperlink" />
            <HeaderStyle Width="25px" />
        </asp:CommandField>
        <asp:BoundField DataField="ClientEmailAddressID" HeaderText="ClientEmailAddressID"
            InsertVisible="False" ReadOnly="True" SortExpression="ClientEmailAddressID" ShowHeader="False" Visible="False" >
            <ItemStyle Wrap=False CssClass="X-Small" />
            <HeaderStyle Wrap=False />
        </asp:BoundField>
        <asp:ButtonField DataTextField="ClientEmailAddress" Text="Client Email Address" CommandName="EmailSelected" HeaderText="Client Email Address" >
            <ItemStyle Wrap=False CssClass="hyperlink" />
            <HeaderStyle Wrap=False />
        </asp:ButtonField>
        <asp:BoundField DataField="ClientName" HeaderText="Client Name" SortExpression="ClientName" >
            <ItemStyle Wrap=False CssClass="X-Small" />
            <HeaderStyle Wrap=False />
        </asp:BoundField>
        <asp:BoundField DataField="KeyWord" HeaderText="Client CC" SortExpression="KeyWord" >
            <ItemStyle Wrap=False CssClass="X-Small" />
            <HeaderStyle Wrap=False />
        </asp:BoundField>        
        <asp:BoundField DataField="Region" HeaderText="Region" SortExpression="Region" >
            <ItemStyle Wrap=False CssClass="X-Small" />
            <HeaderStyle Wrap=False />
        </asp:BoundField>        
        <asp:BoundField DataField="AccountantFN" HeaderText="First Name" SortExpression="AccountantFN" >
            <ItemStyle Wrap=False CssClass="X-Small" />
            <HeaderStyle Wrap=False />
        </asp:BoundField>        
        <asp:BoundField DataField="AccountantLN" HeaderText="Last Name" SortExpression="AccountantLN" >
            <ItemStyle Wrap=False CssClass="X-Small" />
            <HeaderStyle Wrap=False />
        </asp:BoundField>        
        <asp:BoundField DataField="FromEmailAddress" HeaderText="From" SortExpression="CC1" Visible="False" />
        <asp:BoundField DataField="CC1" HeaderText="CC1" SortExpression="CC1" Visible="False" />
        <asp:BoundField DataField="CC2" HeaderText="CC2" SortExpression="CC2" Visible="False" />
        <asp:BoundField DataField="CC3" HeaderText="BCC1" SortExpression="CC3" Visible="False" />
    </Columns>
                <PagerSettings Mode="NextPreviousFirstLast" />
                <EmptyDataTemplate>
                    Nothing
                </EmptyDataTemplate>
</asp:GridView>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="Get" TypeName="ReportDistribution.BLL.ClientEmail" OnObjectCreating="ObjectDataSource1_ObjectCreating" OnSelecting="ObjectDataSource1_Selecting">
    <SelectParameters>
        <asp:ControlParameter ControlID="txtClientEmailAddress" DefaultValue="" Name="clientEmailAddress"
            PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="txtClientName" DefaultValue="" Name="clientName"
            PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="txtKeyWord" DefaultValue="" Name="KeyWord"
            PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="txtAccountantFN" DefaultValue="" Name="accountantFN"
            PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="txtAccountantLN" DefaultValue="" Name="accountantLN"
            PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="txtRegion" DefaultValue="" Name="region" PropertyName="Text"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

