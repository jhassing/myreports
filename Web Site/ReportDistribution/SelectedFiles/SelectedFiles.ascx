<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectedFiles.ascx.cs" Inherits="ReportDistribution_SelectedFiles" %>

<%@ Register TagPrefix="acwc" Namespace="actuate.classes.webcontrols" Assembly="activeportal"%>
<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<%@ Register TagPrefix="actuate" TagName="filter" Src="~/Common/filter.ascx" %>
<%@ Register TagPrefix="actuate" TagName="separator" Src="~/Common/separator.ascx" %>
	
<TABLE cellSpacing="0" cellPadding="0" width="90%" border="0">
	<TR>
		<TD colspan="3" class="TableHeaderRow" id="tdReports" vAlign="middle" noWrap runat="server" >
			<asp:Label id="ReportCount" CssClass="X-SmallBold_White" Runat="server">Welcome to MySelected files. Please select from the following options.</asp:Label></TD>
	</TR>
	<tr><td colspan="3"><asp:Label ID="lblActions" Text="Report Actions" CssClass="X-SmallBold" runat="server"></asp:Label></td></tr>	
	<tr>
		<td nowrap>&nbsp;&nbsp;</td>
		<td nowrap width="153" >
			<asp:LinkButton CssClass="hyperlink" ID="lnkDownloadFiles" runat="server" CommandName="ShowDownloadFiles">Download Files</asp:LinkButton>
		</td>
		<td nowrap>
			<asp:Label Text="Downloads selected files, in a zip format, to a spcified location on your computer." CssClass="X-Small" runat="server" id="LocalizedLabel1" />
		</td>
	</tr>	
	<tr>
		<td nowrap>&nbsp;&nbsp;</td>
		<td nowrap width="153" >
			<asp:LinkButton CssClass="hyperlink" ID="lnkEmailFiles" runat="server" CommandName="ShowEmailScreen">Email Files</asp:LinkButton>
		</td>
		<td nowrap>
			<asp:Label Text="Emails selected and attached files to a selected email address." CssClass="X-Small" runat="server" id="Label2" />
		</td>
	</tr>
	<tr>
	    <td nowrap>&nbsp;&nbsp;</td>
	    <td nowrap width="153" >
	        <asp:LinkButton CssClass="hyperlink" ID="lnbAttachReport" runat=server CommandName="attachreport">Attach Report</asp:LinkButton>
	    </td>
	    <td nowrap>
	        <asp:Label Text="Attach a report from your computer to be emailed." CssClass="X-Small" runat=server ID="lblAttachReport" />
	    </td>
	</tr>
	<tr><td colspan="3" style="height: 10px"><hr style="height: 1px" /></td></tr>
	<tr><td colspan="3"><asp:Label ID="lblClientEmailAddress" Text="Client Email Address" CssClass="X-SmallBold" runat="server"></asp:Label></td></tr>	
	<tr>
		<td nowrap>&nbsp;&nbsp;</td>
		<td nowrap width="153" >
			<asp:LinkButton CssClass="hyperlink" ID="lnkAddNewClientEmail" runat="server" CommandName="addnewclientemailaddressfromhome">Add New</asp:LinkButton>
		</td>
		<td nowrap >
			<asp:Label Text="Add a new Client email address." CssClass="X-Small" runat="server" id="Label3" />
		</td>
	</tr>			
	<tr>
		<td nowrap>&nbsp;&nbsp;</td>
		<td nowrap width="153" >
			<asp:LinkButton CssClass="hyperlink" ID="LinkButton1" runat="server" CommandName="showemailsearchscreenfromhome">Edit Existing</asp:LinkButton>
		</td>
		<td nowrap >
			<asp:Label Text="Edit an existing Client email address." CssClass="X-Small" runat="server" id="Label4" />
		</td>
	</tr>
    <tr><td colspan="3" style="height: 10px"><hr style="height: 1px" /></td></tr>
	<tr><td colspan="3"><asp:Label ID="lblSendFromAddress" Text="Send From Address" CssClass="X-SmallBold" runat="server"></asp:Label></td></tr>			
	<tr>
		<td nowrap>&nbsp;&nbsp;</td>
		<td nowrap width="153" >
			<asp:LinkButton CssClass="hyperlink" ID="LinkButton2" runat="server" CommandName="addnewclientemailfromaddress">Add New</asp:LinkButton>
		</td>
		<td nowrap >
			<asp:Label Text="Add a new Client From email address." CssClass="X-Small" runat="server" id="Label5" />
		</td>
	</tr>
	<tr>
		<td nowrap>&nbsp;&nbsp;</td>
		<td nowrap width="153" >
			<asp:LinkButton CssClass="hyperlink" ID="LinkButton3" runat="server" CommandName="editexistingclientemailfrom">Edit Existing</asp:LinkButton>
		</td>
		<td nowrap >
			<asp:Label Text="Edit an existing Client From email address." CssClass="X-Small" runat="server" id="Label6" />
		</td>
	</tr>
	<tr><td colspan="3" style="height: 10px"><hr style="height: 1px" /></td></tr>
	<tr><td colspan="3"><asp:Label ID="lblCustomEmail" Text="Custom Email Message" CssClass="X-SmallBold" runat="server"></asp:Label></td></tr>	
	<tr>
	    <td nowrap>&nbsp;&nbsp;</td>
	    <td nowrap width="153" >
	        <asp:LinkButton CssClass="hyperlink" ID="lnbAddEmailMessage" runat=server CommandName="addnewemailmessage">Add New</asp:LinkButton>
	    </td>
	    <td nowrap>
	        <asp:Label Text="Add new custom email message." CssClass="X-Small" runat=server ID="Label7" />
	    </td>
	</tr>
	<tr>
	    <td nowrap>&nbsp;&nbsp;</td>
	    <td nowrap width="153" >
	        <asp:LinkButton CssClass="hyperlink" ID="lnbEditEmailMessage" runat=server CommandName="editexistingemailmessage">Edit or Remove Existing</asp:LinkButton>
	    </td>
	    <td nowrap>
	        <asp:Label Text="Edit or remove an existing custom email message." CssClass="X-Small" runat=server ID="lblEditEmailMessage" />
	    </td>
	</tr>		
	<tr><td colspan="3">&nbsp;</td></tr>
	<TR>
		<TD colspan="3" class="TableHeaderRow" id="td1" vAlign="middle" noWrap runat="server">
			<asp:Label id="Label1" CssClass="X-SmallBold_White" Runat="server">Selected Reports</asp:Label></TD>
	</TR>
	<%-- Documents --%>
	<asp:repeater id="DocumentList" runat="server" EnableViewState="true" OnItemDataBound="DocumentList_OnItemDataBound">
		<ItemTemplate>
			<tr id="lll" runat="server" style="PADDING-TOP: 4px">
			    <td nowrap>&nbsp;&nbsp;</td>
				<td colspan="2" valign="middle" nowrap>
                    <asp:ImageButton ImageUrl="../../images/remove.gif" OnCommand="lnkRemove_Command" ID="lnkRemove" runat="server" />&nbsp;&nbsp;&nbsp;				    
					<acwc:AcSafeHyperLink id="AcLnkDocumentName" CssClass="hyperlink" runat="server" />
				</td>
			</tr>
		</ItemTemplate>
	</asp:repeater>
	<tr id="trNoFilesSelected" runat=server>
	    <td nowrap>&nbsp;&nbsp;</td>
	    <td>&nbsp;&nbsp;&nbsp;<asp:Label Text="No files selected" CssClass="X-Small" runat="server" id="lblNoFilesSelected" /></td>
	</tr>
	<TR>
	    <TD colspan="3" class="TableHeaderRow" id="td2" vAlign="middle" noWrap runat="server" style="height: 19px">
		    <asp:Label id="Label8" CssClass="X-SmallBold_White" Runat="server">Attached Reports</asp:Label></TD>
	</TR>
	<asp:Repeater ID="AttachmentList" runat=server EnableViewState=true OnItemDataBound="AttachmentList_OnItemDataBound">
	    <ItemTemplate>
	        <tr runat=server style="padding-top: 4px">
	            <td nowrap>&nbsp;&nbsp;</td>
	            <td colspan="2" valign=middle nowrap>
	                <asp:ImageButton ImageUrl="../../images/remove.gif" OnCommand="lnkAttachedRemove_Command" ID="lnkAttachedRemove" runat="server" />&nbsp;&nbsp;&nbsp;
	                <acwc:AcSafeHyperLink ID="AttLnkDocName" CssClass="hyperlink" runat=server Text="filename_here" />
	            </td>
	        </tr>
	    </ItemTemplate>
	</asp:Repeater>
	<tr id="trNoAttachmentsSelected" runat=server>
	    <td nowrap>&nbsp;&nbsp;</td>
	    <td>&nbsp;&nbsp;&nbsp;<asp:Label Text="No attachments" CssClass="X-Small" runat=server ID=lblNoAttachmentsSelected /></td>
	</tr>
</TABLE>