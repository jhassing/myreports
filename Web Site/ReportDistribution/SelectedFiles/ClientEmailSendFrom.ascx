<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClientEmailSendFrom.ascx.cs" Inherits="FormView_ClientEmailSendFrom" %>
<TABLE cellSpacing="0" cellPadding="0" width="90%" border="0">
	<TR>
		<TD colspan="3" class="TableHeaderRow" id="td1" vAlign="middle" noWrap runat="server">
			<asp:Label id="lblSendFromHeader" CssClass="X-SmallBold_White" Runat="server">Edit Selected Send From</asp:Label></TD>
	</TR>
</TABLE>
<table runat="server" ID=tblMain border="0" cellpadding="4" cellspacing="0" >
    <tr>
        <td nowrap align="right" >
            <asp:Label ID="lblClienEmailFromTag" runat="server" CssClass="X-Small" Text="Client Email From Address:"></asp:Label>
        </td>
        <td nowrap style="width: 400px">
             <asp:TextBox Width="90%" ID="txtClientEmailFrom" CssClass="X-SmallTextBox" runat="server" ></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td nowrap align="right">
            <asp:Label ID="lblEmailTitleTag" runat="server" CssClass="X-Small" Text="Associated Email Message:"></asp:Label>
        </td>
        <td nowrap style="width: 400px" >
            <asp:DropDownList AutoPostBack="true" ID="ddlEmailTitle" runat="server" CssClass="X-Small" Width="90%" OnSelectedIndexChanged="ddlEmailTitle_SelectedIndexChanged" ></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td nowrap align="right">
            <asp:Label ID="lblSubjectTag" runat="server" CssClass="X-Small" Text="Subject:"></asp:Label>
        </td>
        <td style="width: 400px">
            <asp:TextBox ID="txtEmailSubject" ReadOnly="true" runat="server" CssClass="X-Small" Width="90%" ></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td nowrap align="right" valign="top" >
            <asp:Label ID="lblMessageTag" runat="server" CssClass="X-Small" Text="Message Body:"></asp:Label>
        </td>
        <td style="width: 400px">
            <asp:TextBox ID="txtEmailMessage" runat="server" CssClass="X-Small" Width="90%" ReadOnly="true" TextMode="MultiLine" Height="200px" ></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnInsert" runat="server" Text="Insert" OnClick="btnInsert_OnClick" />
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_OnClick" />
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnEdit"   runat="server" Text="Edit Existing" OnClick="btnEdit_OnClick" />
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnAddNew" runat="server" Text="Add New" OnClick="btnAddNew_OnClick" />
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnCancelUpdate" runat="server" Text="Cancel" OnClick="btnCancel_OnClick" />
        </td>
    </tr>
</table>
<asp:Label ID="lblErrorMessage" runat="server" CssClass="X-Small" ForeColor="Red" Height="41px" Text="[Message]" Visible="False" Width="473px"></asp:Label>