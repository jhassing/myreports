<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailSelectedFiles.ascx.cs" Inherits="ReportDistribution_SelectedFiles_EmailSelectedFiles" %>
<TABLE style="margin-bottom:8px" cellSpacing="0" cellPadding="0" width="90%" border="0">
	<TR>
		<TD colspan="3" class="TableHeaderRow" id="td1" vAlign="middle" noWrap runat="server">
			<asp:Label id="Label7" CssClass="X-SmallBold_White" Runat="server">Email Reports</asp:Label></TD>
	</TR>
</TABLE>

<table border="0" cellpadding="1" style="width: 483px">
    <tr>
        <td align="right" style="width: 66px; height: 24px;" >
            <asp:Label CssClass="XX-SmallBold" ID="Label4" runat="server" Text="From:"></asp:Label>
        </td>
        <td nowrap style="height: 24px; width: 403px;" >
            <asp:TextBox ReadOnly="true" BorderColor="#A7125B" BorderStyle=solid BorderWidth="1px" CssClass="XX-SmallBold" ID="lblFromEmailAddress" runat="server" Width="400px" BackColor="#F9DAE9"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td valign="bottom" align="right" nowrap style="width: 66px" rowspan="" >
            <asp:ImageButton CommandName="showemailsearchscreen" ID="ImageButton1" runat="server" Height="16px" ImageUrl="~/images/ADDRBOOK.GIF" Width="16px" />&nbsp;
            <asp:Label CssClass="XX-SmallBold" ID="Label2" runat="server" Text="To:"></asp:Label>
        </td>
        <td valign="bottom" nowrap style="width: 403px"  >
            <asp:TextBox ReadOnly="true" CssClass="XX-Small" ID="lblTo" runat="server" Width="400px"></asp:TextBox>
        </td>           
    </tr>
    <tr>
        <td align="right" nowrap style="width: 66px" >
            <asp:Label CssClass="XX-SmallBold" ID="Label1" runat="server" Text="CC 1:"></asp:Label></td>
         <td nowrap style="width: 403px" >    
            <asp:TextBox CssClass="XX-Small" ID="lblCC1" runat="server" Width="400px"></asp:TextBox></td>           
            
    </tr>
    <tr>
         <td align="right" nowrap style="width: 66px" >
            <asp:Label CssClass="XX-SmallBold" ID="Label3" runat="server" Text="CC 2:"></asp:Label></td>
         <td nowrap style="width: 403px" >
            <asp:TextBox CssClass="XX-Small" ID="lblCC2" runat="server" Width="400px"></asp:TextBox></td>           
            
    </tr>   
    <tr>
         <td align="right" nowrap style="width: 66px" >
            <asp:Label CssClass="XX-SmallBold" ID="Label6" runat="server" Text="BCC 1:"></asp:Label></td>
         <td nowrap style="width: 403px" >
            <asp:TextBox CssClass="XX-Small" ID="lblBCC1" runat="server" Width="400px"></asp:TextBox></td>           
            
    </tr>
    <tr>
         <td align="right" nowrap style="width: 66px; padding-bottom: 15px;"><asp:Label CssClass="XX-SmallBold" ID="Label5" runat="server" Text="Subject:"></asp:Label>
        </td>
        <td align="left" valign="top" style="padding-bottom: 15px; width: 403px;">
            <asp:TextBox CssClass=" XX-Small" ID="txtSubject" runat="server" Width="400px"></asp:TextBox></td>
    </tr>
    <tr>
        <td colspan="2" nowrap align="left" valign="top" >&nbsp;
            <asp:TextBox ID="txtBody" CssClass="XX-Small" runat="server" Height="140px" TextMode="MultiLine" Width="460px"></asp:TextBox></td>
    </tr>
    <tr>
        <td colspan=2 align="right">
            <HR>
            <asp:Label CssClass="XX-SmallBold" ID="lblEmailTitle" runat="server" Text="Selected Email Message:"></asp:Label>
            <asp:DropDownList CssClass="XX-Small" ID="ddlEmailTitle" runat="server" AutoPostBack="true" Width="330px" OnSelectedIndexChanged="ddlEmailTitle_SelectedIndexChanged"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td nowrap align="right" colspan="2" >
            <HR>
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnSendEmail" runat="server" CommandName="SendEmail" Text="Send Email" OnClick="btnSendEmail_Click" />&nbsp;&nbsp;
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnCancelEmail" runat="server" CommandName="showstartscreen" Text="Cancel" OnClick="btnCancelEmail_Click"/>
        </td>
    </tr>
</table>
