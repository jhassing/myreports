using System;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using ReportDistribution.SQLServerDAL.ClientEmail;

public partial class ReportDistribution_SelectedFiles_ClientEmailFromSearch : System.Web.UI.UserControl
{

    #region ClearScreen

    public void ClearScreen()
    {
        this.txtClientEmailFrom.Text = "";

        this.GridView1.DataSource = null;
        this.GridView1.DataBind();
        this.GridView1.Visible = false;

    }

    #endregion

    #region DoSearch

    public void DoSearch()
    {
        try
        {
            this.GridView1.Visible = true;
            this.GridView1.DataBind();
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }

    #endregion

    private void DisplayError(Exception ex)
    {
        this.GridView1.Visible = false;
        this.lblMessage.Visible = true;
        string mess = ex.Message;
        mess += "\r\n";

        if (ex.InnerException != null)
            mess += ex.InnerException.Message;

        this.lblMessage.Text = mess;

        ExceptionManager.Publish(ex);
    }

    protected void ObjectDataSource1_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = new ClientEmailSendFrom(MyReports.Common.AppValues.MyReportsDB);
    }
    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {

    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.ToLower() == "select")
            {
                int index = Convert.ToInt32(e.CommandArgument);

                int ind = System.Convert.ToInt32(this.GridView1.DataKeys[index]["ClientEmailSendFromID"].ToString());

                CommandEventArgs ce = new CommandEventArgs("EditIndividualFromEmailAddress", ind);

                RaiseBubbleEvent(sender, ce);
            }
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DoSearch();   
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {

    }
}
