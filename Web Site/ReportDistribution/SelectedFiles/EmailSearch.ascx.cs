using System;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using ReportDistribution.SQLServerDAL.ClientEmail;

public partial class ReportDistribution_SelectedFiles_EmailSearch : System.Web.UI.UserControl
{
    #region Properties

    public int ClientEmailAddressID
    {
        set { ViewState["ClientEmailAddressID"] = value; }
        get { return System.Convert.ToInt32(ViewState["ClientEmailAddressID"]); }
    }

    public string SelectedClientEmailAddress
    {
        get { return ViewState["ClientEmailAddress"].ToString(); }
        set { ViewState["ClientEmailAddress"] = value; }
    }

    public string FromEmailAddress
    {
        get { return ViewState["FromEmailAddress"].ToString(); }
        set { ViewState["FromEmailAddress"] = value; }
    }

    public string CC1
    {
        get { return ViewState["CC1"].ToString(); }
        set { ViewState["CC1"] = value; }
    }

    public string CC2
    {
        get { return ViewState["CC2"].ToString(); }
        set { ViewState["CC2"] = value; }
    }

    public string BCC1
    {
        get { return ViewState["BCC1"].ToString(); }
        set { ViewState["BCC1"] = value; }
    }


    #endregion

    #region ClearScreen

    public void ClearScreen()
    {
        this.txtAccountantFN.Text = "";
        this.txtAccountantLN.Text = "";
        this.txtClientEmailAddress.Text = "";
        this.txtClientName.Text = "";
        this.txtKeyWord.Text = "";
        this.txtRegion.Text = "";

        this.GridView1.DataSource = null;        
        this.GridView1.DataBind();
        this.GridView1.Visible = false;

    }

    #endregion

    #region DoSearch

    public void DoSearch()
    {
        try
        {
            this.GridView1.Visible = true;
            this.GridView1.DataBind();
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }

    #endregion

    private void DisplayError(Exception ex)
    {
        this.GridView1.Visible = false;
        this.lblMessage.Visible = true;
        string mess = ex.Message;
        mess += "\r\n";

        if (ex.InnerException != null)
            mess += ex.InnerException.Message;

        this.lblMessage.Text = mess;

        ExceptionManager.Publish(ex);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                this.GridView1.Visible = false;
            }
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {        

        DoSearch();                                                            
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        try
        {

            if (e.CommandName.ToLower() == "emailselected")
            {
                int index = Convert.ToInt32(e.CommandArgument);

                // Retrieve the row that contains the button clicked 
                // by the user from the Rows collection.
                GridViewRow row = GridView1.Rows[index];

                this.SelectedClientEmailAddress = ((LinkButton)row.Cells[2].Controls[0]).Text;

                this.FromEmailAddress = this.GridView1.DataKeys[index]["FromEmailAddress"].ToString();
                this.CC1 = this.GridView1.DataKeys[index]["CC1"].ToString();
                this.CC2 = this.GridView1.DataKeys[index]["CC2"].ToString();
                this.BCC1 = this.GridView1.DataKeys[index]["BCC1"].ToString();

                RaiseBubbleEvent(sender, e);
            }
            else if (e.CommandName.ToLower() == "select")
            {
                int index = Convert.ToInt32(e.CommandArgument);

                int ind = System.Convert.ToInt32(this.GridView1.DataKeys[index]["ClientEmailAddressID"].ToString());

                CommandEventArgs ce = new CommandEventArgs("selectindividualclientemail", ind);

                this.ClientEmailAddressID = System.Convert.ToInt32(this.GridView1.DataKeys[index]["ClientEmailAddressID"].ToString());

                RaiseBubbleEvent(sender, ce);
            }
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }

    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        RaiseBubbleEvent(sender, e);        
    }
    protected void ObjectDataSource1_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = new Address(MyReports.Common.AppValues.MyReportsDB);
    }
    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {

    }
}
