using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using ReportDistribution.SQLServerDAL;
using ReportDistribution.SQLServerDAL.ClientEmail;

public partial class ReportDistribution_SelectedFiles_EmailMessage : System.Web.UI.UserControl
{
    EmailMessage oEmailMessage = new EmailMessage(MyReports.Common.AppValues.MyReportsDB);
    List<MessageObject> listEmailMessage;
    MessageObject EmailMessageListing;

    #region Properties
    public int EmailMessageID
    {
        get { return System.Convert.ToInt32(ViewState["EmailMessageID"]); }
        set { ViewState["EmailMessageID"] = value; }
    }
    #endregion Properties

    #region page load
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblErrorMessage.Visible = false;
    }
    #endregion

    public void SetToEditMode()
    {
        try
        {
            listEmailMessage = this.oEmailMessage.Get(EmailMessageID);

            if (listEmailMessage.Count > 0)
            {
                EmailMessageListing = listEmailMessage[0];

                txtEmailTitle.Text = EmailMessageListing.EmailTitle;
                txtEmailSubject.Text = EmailMessageListing.EmailSubject;
                txtEmailMessage.Text = EmailMessageListing.EmailMessage;
                this.btnUpdate.Visible = true;
                this.btnAddNew.Visible = true;
                if (EmailMessageID != 1)
                    this.btnRemove.Visible = true;
                this.btnEdit.Visible = false;
                this.btnInsert.Visible = false;            
            }
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }

    public void SetToInsertMode()
    {
        try
        {
            this.txtEmailTitle.Text = null;
            this.txtEmailSubject.Text = null;
            this.txtEmailMessage.Text = null;
            this.btnUpdate.Visible = false;
            this.btnAddNew.Visible = false;
            this.btnRemove.Visible = false;
            this.btnEdit.Visible = true;
            this.btnInsert.Visible = true;
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }

    #region redirect to home screen
    private void ReDirectToHomeScreen(object sender, EventArgs e)
    {
        try
        {
            // Now re-direct back to the listing page
            CommandEventArgs ce = new CommandEventArgs("showstartscreen", "");

            RaiseBubbleEvent(sender, ce);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    #endregion

    #region display error
    private void DisplayError(Exception ex)
    {
        this.lblErrorMessage.Visible = true;
        string mess = ex.Message;
        mess += "\r\n";

        if (ex.InnerException != null)
            mess += ex.InnerException.Message;

        this.lblErrorMessage.Text = mess;

        ExceptionManager.Publish(ex);
    }
    #endregion

    #region control events methods
    #region button click events
    protected void btnAddNew_OnClick(object sender, EventArgs e)
    {
        SetToInsertMode();
    }
    protected void btnEdit_OnClick(object sender, EventArgs e)
    {
        if (this.EmailMessageID == 0)
        {
            CommandEventArgs ce = new CommandEventArgs("editexistingemailmessage", "");

            RaiseBubbleEvent(sender, ce);
        }
        else
        {
            SetToEditMode();
        }
    }
    protected void btnCancelUpdate_OnClick(object sender, EventArgs e)
    {
        ReDirectToHomeScreen(sender, e);
    }
    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        try
        {
            string emailTitle = txtEmailTitle.Text;
            string emailSubject = txtEmailSubject.Text;
            string emailMessage = txtEmailMessage.Text;
            string modifiedBy = MyReports.Common.AppValues.UserName;

            oEmailMessage.Update(EmailMessageID, emailTitle, emailSubject, emailMessage, modifiedBy);

            ReDirectToHomeScreen(sender, e);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {
        try
        {
            string emailTitle = txtEmailTitle.Text;
            string emailSubject = txtEmailSubject.Text;
            string emailMessage = txtEmailMessage.Text;
            string createdBy = MyReports.Common.AppValues.UserName;

            oEmailMessage.Insert(emailTitle, emailSubject, emailMessage, createdBy);

            ReDirectToHomeScreen(sender, e);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        try
        {
            if (EmailMessageID != 1) // 1 is the ID of the default Email Message
            {
                oEmailMessage.Delete(this.EmailMessageID);
            }
            else
            {
                throw new Exception("This is the default Email Message and it cannot be removed.");
            }
            ReDirectToHomeScreen(sender, e);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    #endregion  
    #endregion


}
