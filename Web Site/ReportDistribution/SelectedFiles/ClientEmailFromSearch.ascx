
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClientEmailFromSearch.ascx.cs" Inherits="ReportDistribution_SelectedFiles_ClientEmailFromSearch" %>
<TABLE style="margin-bottom:8px" cellSpacing="0" cellPadding="0" width="90%" border="0">
	<TR>
		<TD colspan="3" class="TableHeaderRow" id="td1" vAlign="middle" noWrap runat="server">
			<asp:Label id="Label7" CssClass="X-SmallBold_White" Runat="server">Client Email From Search</asp:Label></TD>
	</TR>
</TABLE>
  <table border="0" cellpadding="0" cellspacing="6">
    <tr>
         <td nowrap style="width: 152px" align="right">
            <asp:Label ID="Label2" CssClass="X-Small" runat="server" Text="Client Email From:"></asp:Label>
            </td>
         <td nowrap style="width: 400px" >
         <asp:TextBox autocomplete="off"  id="txtClientEmailFrom" CssClass="XX-SmallTextBox" runat="server" Width="100%"
				MaxLength="200" Format="Text"></asp:TextBox>
            </td>
    </tr>    
     <tr>
        <td nowrap colspan="2" align="right">
            
            <asp:Button ID="btnSearch" CssClass="MyReportsAdminConsoleButton" runat="server" Text="Search" OnClick="btnSearch_Click" />&nbsp;&nbsp;
            <asp:Button ID="btnAddNew" CssClass="MyReportsAdminConsoleButton" runat="server" Text="Add New" CommandName="addnewclientemailfromaddress" OnClick="btnAddNew_Click" />
        </td>        
    </tr>
</table>
<asp:Label Visible=false ID="lblMessage" ForeColor="Red" runat="server" CssClass="X-Small" Text="[Message]"></asp:Label>            
<br />
<asp:GridView ID="GridView1" runat="server" AllowPaging="True" CellPadding="2" 
    DataSourceID="ObjectDataSource1" DataKeyNames="ClientEmailSendFromID"
    ForeColor="#333333" GridLines="None" OnRowCommand="GridView1_RowCommand" PageSize="6"
    Width="90%" AutoGenerateColumns="False">
    <PagerSettings Mode="NextPreviousFirstLast" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <EmptyDataTemplate>
        Nothing
    </EmptyDataTemplate>
    <EditRowStyle BackColor="#999999" />
    
    <PagerStyle BackColor="#284775" ForeColor="White" Height="12px" HorizontalAlign="Center" />
    <HeaderStyle BackColor="#5D7B9D" CssClass="X-Small_White" Font-Bold="True" ForeColor="White"
        HorizontalAlign="Left" VerticalAlign="Bottom" />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <Columns>
        <asp:CommandField SelectText="" ShowSelectButton="True" EditImageUrl="../../images/edit.gif" EditText="" SelectImageUrl="~/images/edit.gif" ButtonType="Image" >
            <ItemStyle Wrap=False CssClass="hyperlink" />
            <HeaderStyle Width="25px" />
        </asp:CommandField>
        <asp:BoundField DataField="FromEmailAddress" HeaderText="From Email Address" SortExpression="FromEmailAddress" >
            <ItemStyle Wrap=False CssClass="X-Small" />
            <HeaderStyle Wrap=False />
        </asp:BoundField>        
     </Columns>
</asp:GridView>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OnObjectCreating="ObjectDataSource1_ObjectCreating"
    OnSelecting="ObjectDataSource1_Selecting" SelectMethod="Get" TypeName="ReportDistribution.BLL.ClientEmailSendFrom">
    <SelectParameters>
        <asp:ControlParameter ControlID="txtClientEmailFrom" Name="fromEmailAddress" PropertyName="Text"
            Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
