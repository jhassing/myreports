using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using MyReports.Common;
using ReportDistribution.SQLServerDAL;
using ReportDistribution.SQLServerDAL.ClientEmail;

public partial class ReportDistribution_SelectedFiles_EmailSelectedFiles : System.Web.UI.UserControl
{
    // Send from vars
    private ClientEmailSendFrom oSendFrom = new ClientEmailSendFrom(AppValues.MyReportsDB);

    //Email Message vars
    private EmailMessage oEmailMessage = new EmailMessage(AppValues.MyReportsDB);
    private List<MessageObject> listEmailMessage;
    private MessageObject _theMessage = new MessageObject();

    // property vars
    private int _emailMessageID;

    #region Properties

    public string ClientEmailAddress
    {
        get { return this.lblTo.Text; }
        set { this.lblTo.Text = value; }
    }

    public string CC1
    {
        get { return this.lblCC1.Text; }
        set { this.lblCC1.Text = value; }
    }

    public string CC2
    {
        get { return this.lblCC2.Text; }
        set { this.lblCC2.Text = value; }
    }

    public string BCC1
    {
        get { return this.lblBCC1.Text; }
        set { this.lblBCC1.Text = value; }
    }

    public string FromEmailAddress
    {
        get { return this.lblFromEmailAddress.Text; }
        set { this.lblFromEmailAddress.Text = value; }
    }

    public string Subject
    {
        get { return this.txtSubject.Text; }
        set { this.txtSubject.Text = value; }
    }

    public string Message
    {
        get { return this.txtBody.Text; }
        set { this.txtBody.Text = value; }
    }

    public int EmailMessageID
    {
        get { return _emailMessageID; }
        set { _emailMessageID = value; }
    }

    #endregion

    #region email message methods
    #region get email message list by title
    private List<MessageObject> GetEmailMessageListing(string emailTitle)
    {
        List<MessageObject> emailMessageList = oEmailMessage.Get(emailTitle);
        return emailMessageList;
    }
    #endregion

    #region get email message by ID
    private MessageObject GetEmailMessage(int emailMessageID)
    {
        MessageObject retMessage = new MessageObject();

        try
        {
            listEmailMessage = oEmailMessage.Get(emailMessageID);
            retMessage = listEmailMessage[0];
            return retMessage;
        }
        catch (Exception ex)
        {
            throw ex;
            return retMessage;
        }
    }
    #endregion

    #region init email message ID
    private int InitEmailMessageID()
    {
        int retEmailMessageID;

        try
        {
            //get the EmailMessageID from the ClientEmailSendFrom listing
            List<ClientEmailSendFromObject> mySendFromList = oSendFrom.Get(FromEmailAddress);
            ClientEmailSendFromObject theSendFromListing = mySendFromList[0];
            retEmailMessageID = theSendFromListing.EmailMessageID;
            return retEmailMessageID;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
    #endregion

    #region Display email message controls
    public void InitEmailMessageControls()
    {
        //set email message property and get the email message data
        this.EmailMessageID = InitEmailMessageID();
        this._theMessage = GetEmailMessage(EmailMessageID);

        // fill and set the drop down list to the selected email title value
        FillEmailTitleDropDownList();
        this.ddlEmailTitle.SelectedValue = _theMessage.EmailTitle;

        DisplayEmailMessage();
    }
    private void FillEmailTitleDropDownList()
    {
        ddlEmailTitle.Items.Clear();
        listEmailMessage = GetEmailMessageListing(null);
        for (int i = 0; i < listEmailMessage.Count; i++)
        {
            ddlEmailTitle.Items.Add(listEmailMessage[i].EmailTitle);
        }
    }
    private void DisplayEmailMessage()
    {
        // populate the subject and message properties
        this.Subject = this._theMessage.EmailSubject;
        this.Message = this._theMessage.EmailMessage;

        // populate [FROM] tokens
        this.Subject = Subject.Trim().Replace("[FROM]", FromEmailAddress);
        this.Message = Message.Trim().Replace("[FROM]", FromEmailAddress);
    }
    #endregion

    #region control event methods
    protected void btnSendEmail_Click(object sender, EventArgs e)
    {
        //CommandEventArgs ce = new CommandEventArgs("sendemail", "");
        RaiseBubbleEvent(sender, e);
    }
    protected void btnCancelEmail_Click(object sender, EventArgs e)
    {
        CommandEventArgs ce = new CommandEventArgs("cancelemail", "");
        RaiseBubbleEvent(sender, e);
    }
    #endregion
    protected void ddlEmailTitle_SelectedIndexChanged(object sender, EventArgs e)
    {
        // set the email message by the title
        List<MessageObject> myEmailListing = GetEmailMessageListing(ddlEmailTitle.SelectedValue);
        _theMessage = myEmailListing[0];

        // populate subject and message
        DisplayEmailMessage();
    }
}
