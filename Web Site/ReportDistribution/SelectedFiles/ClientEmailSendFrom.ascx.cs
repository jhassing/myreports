using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using ReportDistribution.SQLServerDAL;
using ReportDistribution.SQLServerDAL.ClientEmail;

public partial class FormView_ClientEmailSendFrom : System.Web.UI.UserControl
{
    #region instance variables
    private ClientEmailSendFrom oClientEmailSendFrom = new ClientEmailSendFrom(MyReports.Common.AppValues.MyReportsDB);
    private List<ClientEmailSendFromObject> listClientEmailSendFrom;
    private ClientEmailSendFromObject theClientEmailSendFrom;

    private EmailMessage oEmailMessage = new EmailMessage(MyReports.Common.AppValues.MyReportsDB);
    private List<MessageObject> listEmailMessage;
    private MessageObject theEmailMessage;
    #endregion

    #region page load
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblErrorMessage.Visible = false;
    }
    #endregion

    #region client email send from ID
    public int ClientEmailSendFromID
    {
        get { return System.Convert.ToInt32(ViewState["ClientEmailSendFromID"]); }
        set { ViewState["ClientEmailSendFromID"] = value; }
    }
    #endregion

    #region Set Mode
    public void SetToEditMode()
    {
        try
        {
            listClientEmailSendFrom = oClientEmailSendFrom.Get(this.ClientEmailSendFromID);

            if (listClientEmailSendFrom.Count == 1)
            {
                theClientEmailSendFrom = (ClientEmailSendFromObject)listClientEmailSendFrom[0];

                txtClientEmailFrom.Text = theClientEmailSendFrom.FromEmailAddress;
                FillEmailTitleDropDownList();
                theEmailMessage = GetEmailMessage(theClientEmailSendFrom.EmailMessageID);
                ddlEmailTitle.Items.FindByValue(theEmailMessage.EmailTitle).Selected = true;
                DisplayEmailControls();

                this.btnUpdate.Visible = true;
                this.btnAddNew.Visible = true;
                this.btnEdit.Visible = false;
                this.btnInsert.Visible = false;
            }
            else
            {
                this.lblErrorMessage.Visible = true;
                this.lblErrorMessage.Text = "ERROR:  search matches more than one Email Message.";
            }
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }

    public void SetToInsertMode()
    {
        try
        {
            btnUpdate.Visible = false;
            btnAddNew.Visible = false;
            btnInsert.Visible = true;
            btnEdit.Visible = true;
            FillEmailTitleDropDownList();
            txtClientEmailFrom.Text = null;
            txtEmailSubject.Text = null;
            txtEmailMessage.Text = null;            
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    #endregion

    #region get email message by ID
    private MessageObject GetEmailMessage(int emailMessageID)
    {
        MessageObject retEmailMessage = new MessageObject();

        try
        {
            listEmailMessage = oEmailMessage.Get(emailMessageID);
            retEmailMessage = listEmailMessage[0];
            return retEmailMessage;
        }
        catch (Exception ex)
        {
            DisplayError(ex);
            return retEmailMessage;
        }
    }
    #endregion

    #region get email message list by title
    private List<MessageObject> GetEmailMessageListing(string emailTitle)
    {
        List<MessageObject> emailMessageList = oEmailMessage.Get(emailTitle);
        return emailMessageList;
    }
    #endregion

    #region Display email message controls
    private void FillEmailTitleDropDownList()
    {
        ddlEmailTitle.Items.Clear();
        listEmailMessage = GetEmailMessageListing(null);
        for (int i = 0; i < listEmailMessage.Count; i++)
        {
            ddlEmailTitle.Items.Add(listEmailMessage[i].EmailTitle);
        }
    }

    private void DisplayEmailControls()
    {
        try
        {
            List<MessageObject> myEmailMessageListing = GetEmailMessageListing(this.ddlEmailTitle.SelectedValue);
            theEmailMessage = myEmailMessageListing[0];
            txtEmailSubject.Text = theEmailMessage.EmailSubject;
            txtEmailMessage.Text = theEmailMessage.EmailMessage;
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    #endregion

    #region redirect to home screen
    private void ReDirectToHomeScreen(object sender, EventArgs e)
    {
        try
        {
            // Now re-direct back to the listing page
            CommandEventArgs ce = new CommandEventArgs("showstartscreen", "");

            RaiseBubbleEvent(sender, ce);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    #endregion

    #region DisplayError

    private void DisplayError(Exception ex)
    {
        this.lblErrorMessage.Visible = true;
        string mess = ex.Message;
        mess += "\r\n";

        if (ex.InnerException != null)
            mess += ex.InnerException.Message;

        this.lblErrorMessage.Text = mess;

        ExceptionManager.Publish(ex);
    }

    #endregion

    #region control event methods
    protected void btnCancel_OnClick(object sender, EventArgs e)
    {
        ReDirectToHomeScreen(sender, e);
    }
    protected void btnInsert_OnClick(object sender, EventArgs e)
    {
        try
        {
            List<MessageObject> myEmailMessageListing = GetEmailMessageListing(this.ddlEmailTitle.SelectedValue);
            theEmailMessage = myEmailMessageListing[0];

            string fromEmailAddress = this.txtClientEmailFrom.Text;
            int emailMessageID = theEmailMessage.EmailMessageID;
            string createdBy = MyReports.Common.AppValues.UserName;

            oClientEmailSendFrom.Insert(fromEmailAddress, emailMessageID, createdBy);

            ReDirectToHomeScreen(sender, e);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    protected void btnAddNew_OnClick(object sender, EventArgs e)
    {
        SetToInsertMode();
    }
    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        try
        {
            List<MessageObject> myEmailMessageListing = GetEmailMessageListing(this.ddlEmailTitle.SelectedValue);
            theEmailMessage = myEmailMessageListing[0];

            string fromEmailAddress = this.txtClientEmailFrom.Text;
            int emailMessageID = theEmailMessage.EmailMessageID;
            string modifiedBy = MyReports.Common.AppValues.UserName;

            oClientEmailSendFrom.Update(ClientEmailSendFromID, fromEmailAddress, emailMessageID, modifiedBy);

            ReDirectToHomeScreen(sender, e);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    protected void btnEdit_OnClick(object sender, EventArgs e)
    {
        SetToEditMode();
    }
    protected void ddlEmailTitle_SelectedIndexChanged(object sender, EventArgs e)
    {
        DisplayEmailControls();
    }
    #endregion
}
