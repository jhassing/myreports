using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using MyReports.Common;

public partial class ReportDistribution_SelectedFiles_AttachReports : System.Web.UI.UserControl
{

    private ArrayList aFile_ary = null;
    private string sFileExt = null;
    private bool bAllFilesValid = true;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (Session["AttachedReports"] == null)
            Session["AttachedReports"] = new ArrayList();
    }

    protected void btnAttachReports_Click(object sender, EventArgs e)
    {
        lblErr.Visible = false;
        imgErr.Visible = false;

        aFile_ary = (ArrayList)Session["AttachedReports"];

        if (this.FileUpload1.HasFile)
            AddFileAttachment(FileUpload1.PostedFile);
        if (this.FileUpload2.HasFile)
            AddFileAttachment(FileUpload2.PostedFile);
        if (this.FileUpload3.HasFile)
            AddFileAttachment(FileUpload3.PostedFile);
        if (this.FileUpload4.HasFile)
            AddFileAttachment(FileUpload4.PostedFile);
        if (this.FileUpload5.HasFile)
            AddFileAttachment(FileUpload5.PostedFile);

        CommandEventArgs ce = new CommandEventArgs("showstartscreen", "");
        if (bAllFilesValid)
        {
            RaiseBubbleEvent(sender, ce);
        }
        else
        {
            imgErr.Visible = true;
            lblErr.Visible = true;
        }
    }

    private void AddFileAttachment(HttpPostedFile pFile) 
    {
        try
        {
            if (GetExtension(pFile.FileName.ToLower()) == "pdf" && pFile.ContentLength < 4000000)
            {
                // make the temp dir if it doesn't exist
                string tmpDir = Functions.GetTempDirectoryForThisUser();
                if (!System.IO.Directory.Exists(tmpDir))
                    System.IO.Directory.CreateDirectory(tmpDir);

                // get the filename from the client pathname (pFile.filename) and make server pathname
                string sFileName = "";
                string[] sClientPathName = pFile.FileName.Split('\\');
                sFileName = sClientPathName[sClientPathName.Length - 1];
                string sServerPathName = tmpDir + sFileName.ToLower();

                pFile.SaveAs(sServerPathName);

                if (File.Exists(sServerPathName) && !aFile_ary.Contains(sServerPathName))
                {
                    aFile_ary.Add(sServerPathName);
                }
            }
            else
            {
                bAllFilesValid = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private string GetExtension(string FileName)
    {
        string[] split = FileName.Split('.');
        string Extension = split[split.Length - 1];
        return Extension;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        CommandEventArgs ce = new CommandEventArgs("showstartscreen", "");
        RaiseBubbleEvent(sender, ce);
    }
}
