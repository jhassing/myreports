<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="ReportDistribution_SelectedFiles_Default" %>
<%@ Register Src="ClientEmailFromSearch.ascx" TagName="ClientEmailFromSearch" TagPrefix="uc7" %>
<%@ Register Src="../Common/navigation.ascx" TagName="navigation"           TagPrefix="uc6" %>
<%@ Register Src="ClientEmailSendFrom.ascx" TagName="ClientEmailSendFrom"   TagPrefix="uc1" %>
<%@ Register Src="ClientEmailAddress.ascx"  TagName="ClientEmailAddress"    TagPrefix="uc5" %>
<%@ Register Src="EmailSearch.ascx"         TagName="EmailSearch"           TagPrefix="uc2" %>
<%@ Register Src="EmailSelectedFiles.ascx"  TagName="EmailSelectedFiles"    TagPrefix="uc3" %>
<%@ Register Src="SelectedFiles.ascx"       TagName="SelectedFiles"         TagPrefix="uc4" %>
<%@ Register Src="AttachReports.ascx"       TagName="AttachReports"         TagPrefix="uc8" %>
<%@ Register Src="EmailMessageSearch.ascx"  TagName="EmailMessageSearch"    TagPrefix="uc9" %>
<%@ Register Src="EmailMessage.ascx"        TagName="EmailMessage"          TagPrefix="uc10" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head runat="server">
        <LINK href="../../css/allstyles.css" type="text/css" rel="stylesheet">
    </head>
    <body>
        <form id="form1" runat="server">
            <div>
                <uc6:navigation ID="Navigation1" runat="server" />
                <br />
                <uc4:SelectedFiles id="SelectedFiles1" runat="server" Visible="false" />        
                <uc3:EmailSelectedFiles ID="EmailSelectedFiles1" Visible="false" runat="server" />
                <uc2:EmailSearch ID="EmailSearch1" runat="server" Visible="false" />            
                <uc5:ClientEmailAddress ID="ClientEmailAddress1" Visible="false" runat="server" />
                <uc1:ClientEmailSendFrom ID="ClientEmailSendFrom1" Visible="false" runat="server" />
                <uc7:ClientEmailFromSearch ID="ClientEmailFromSearch1" Visible="false" runat="server" />
                <uc8:AttachReports ID="AttachReports1" Visible="false" runat="server" />
                <uc9:EmailMessageSearch ID="EmailMessageSearch1" Visible="false" runat="server" />
                <uc10:EmailMessage ID="EmailMessage1" Visible="false" runat="server" />
                &nbsp;&nbsp;<br />&nbsp;&nbsp;
                <asp:Label Visible=False ID="lblMessage" ForeColor="Red" runat="server" CssClass="X-Small" Text="[Message]" Height="52px" Width="608px"></asp:Label></div>
        </form>
    </body>
</html>