using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using Compass.Utilities.Email;

using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Common;
using ReportDistribution.SQLServerDAL.ClientEmail;

public partial class ReportDistribution_SelectedFiles_Default : System.Web.UI.Page
{
    #region OnLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.lblMessage.Visible = false;
            if (!Page.IsPostBack)
            {
                ShowStartScreen();
            }
        }
        catch (Exception ex)
        {
            DisplayError(ex);           
        }
    }

    #endregion

    #region DisplayError

    private void DisplayError(Exception ex)
    {
        this.lblMessage.Visible = true;
        string mess = ex.Message;
        mess += "\r\n";

        if (ex.InnerException != null)
            mess += ex.InnerException.Message;

        this.lblMessage.Text = mess;

        ExceptionManager.Publish(ex);
    }

    #endregion

    protected override bool OnBubbleEvent(object source, EventArgs args)
    {
        try
        {
            bool handled = false;
            if (args is CommandEventArgs)
            {
                CommandEventArgs ce = (CommandEventArgs)args;
                switch (ce.CommandName.ToLower())
                {
                    case "showstartscreen":
                        #region Show Start Screen
                        ShowStartScreen();
                        break;
                        #endregion

                    case "showemailscreen":
                        #region Show Email Screen

                        HideControls();
                        this.EmailSelectedFiles1.Visible = true;
                        this.Navigation1.HideItems();
                        this.Navigation1.Display_EmailLink = true;
                        break;

                        #endregion

                    case "showdownloadfiles":
                        #region Show Download Files Screen
                        //this.Navigation1.Display_DownloadLink = true;
                        DownloadFiles();
                        break;

                        #endregion

                    case "emailselected":
                        #region Search Was completed, populate screen

                        HideControls();
                        this.EmailSelectedFiles1.Visible = true;
                        this.EmailSelectedFiles1.ClientEmailAddress = this.EmailSearch1.SelectedClientEmailAddress;
                        this.EmailSelectedFiles1.FromEmailAddress = this.EmailSearch1.FromEmailAddress;
                        this.EmailSelectedFiles1.CC1 = this.EmailSearch1.CC1;
                        this.EmailSelectedFiles1.CC2 = this.EmailSearch1.CC2;
                        this.EmailSelectedFiles1.BCC1 = this.EmailSearch1.BCC1;
                        this.EmailSelectedFiles1.InitEmailMessageControls();
                        
                        this.Navigation1.HideItems();
                        this.Navigation1.Display_EmailLink = true;
                        break;

                        #endregion

                    case "addnewclientemailaddressfromsearch":
                        #region Add New Client Email Address was clicked

                        HideControls();
                        this.ClientEmailAddress1.Visible = true;
                        this.ClientEmailAddress1.Mode = FormViewMode.Insert;
                        this.Navigation1.Display_AddNewClientEmailLink_FromSearch = true;
                        break;

                        #endregion

                    case "addnewclientemailaddressfromhome":
                        #region Add New Client Email Address was clicked

                        HideControls();
                        this.ClientEmailAddress1.Visible = true;
                        this.ClientEmailAddress1.Mode = FormViewMode.Insert;
                        this.Navigation1.Display_AddNewClientEmailLink_FromHome = true;
                        break;

                        #endregion
                        
                    case "showemailsearchscreenfromhome":
                        #region Show Client Email Search Screen from Home

                        ShowEmailSearchScreen();
                        this.Navigation1.HideItems();
                        this.Navigation1.Display_ClientEmailSearchLink_FromHome = true;
                        this.Navigation1.EmailSearchCommandName = "showemailsearchscreenfromhome";
                        break;

                        #endregion

                    case "showemailsearchscreen":
                        #region Show Client Email Screen Screen

                        ShowEmailSearchScreen();
                        this.Navigation1.HideItems();
                        this.Navigation1.Display_EmailLink = true;
                        this.Navigation1.Display_ClientEmailSearchLink = true;
                        this.Navigation1.EmailSearchCommandName = "showemailsearchscreen";
                        break;

                        #endregion

                    case "selectindividualclientemail":
                        #region Show Individual client Email

                        HideControls();
                        this.Navigation1.HideItems();
                        this.Navigation1.Display_ClientEmailSearchLink = true;
                        this.Navigation1.Display_EditClientEmailLink = true;
                        this.ClientEmailAddress1.Visible = true;
                        this.ClientEmailAddress1.Mode = FormViewMode.Edit;
                        this.ClientEmailAddress1.ClientEmailAddressID = System.Convert.ToInt32(ce.CommandArgument);

                        break;

                        #endregion

                    case "editexistingclientemailfrom":
                        ShowEmailFromSearchScreen();                        
                        break;

                    case "editindividualfromemailaddress":
                        #region show individual from address
                        HideControls();
                        this.Navigation1.HideItems();
                        this.Navigation1.Display_SearchFromEmailLink = true;
                        this.Navigation1.Display_EditFromEmailLink = true;
                        //this.Navigation1.Display_EditClientEmailLink = true;
                        this.ClientEmailSendFrom1.Visible = true;
                        this.ClientEmailSendFrom1.ClientEmailSendFromID = System.Convert.ToInt32(ce.CommandArgument);
                        this.ClientEmailSendFrom1.SetToEditMode();
                        break;
                        #endregion

                    case "addnewclientemailfromaddress":
                        #region show new client email from address form
                        HideControls();
                        //this.Navigation1.HideItems();
                        this.Navigation1.Display_AddNewFromEmail = true;
                        this.ClientEmailSendFrom1.Visible = true;
                        this.ClientEmailSendFrom1.SetToInsertMode();
                        break;
                        #endregion

                    case "editexistingemailmessage":
                        ShowEmailMessageSearch();
                        break;

                    case "editindividualemailmessage":
                        HideControls();
                        this.Navigation1.Display_EditEmailMessage = true;

                        this.EmailMessage1.Visible = true;
                        this.EmailMessage1.EmailMessageID = System.Convert.ToInt32(ce.CommandArgument);
                        this.EmailMessage1.SetToEditMode();
                        break;

                    case "addnewemailmessage":
                        HideControls();
                        this.Navigation1.Display_EditEmailMessage = true;
                        this.EmailMessage1.Visible = true;
                        this.EmailMessage1.SetToInsertMode();
                        break;

                    case "attachreport":
                        HideControls();
                        this.Navigation1.Display_AttachReports = true;
                        this.AttachReports1.Visible = true;
                        break;

                    case "sendemail":
                        SendEmail();
                        break;
                }

            }

            return handled;
        }
        catch (Exception ex)
        {
            DisplayError(ex);

            return false;
        }

    }

    #region GetAttachmentList

    private ArrayList GetAttachmentList()
    {
        ArrayList aryRet = new ArrayList();
        aryRet = SaveFilesToTempDirectory();

        try
        {
            if (Session["AttachedReports"] != null)
            {
                ArrayList att_ary = (ArrayList)Session["AttachedReports"];

                if (att_ary.Count > 0)
                {
                    for (int i = 0; i < att_ary.Count; i++)
                    {
                        aryRet.Add(att_ary[i]);
                    }
                }
            }
            return aryRet;
        }
        catch (Exception ex)
        {
            DisplayError(ex);

            return new ArrayList();
        }
    }

    /// <summary>
    /// Saves the selected files from Actuate to the temp location
    /// </summary>
    /// <returns>String array of file locations</returns>
    private ArrayList SaveFilesToTempDirectory()
    {
        ArrayList aryRet = new ArrayList();

        try
        {
            if (Session["SelectedReports"] != null)
            {
                ArrayList ary = (ArrayList)Session["SelectedReports"];

                if (ary.Count > 0)
                {
                    string tmpDir = Functions.GetTempDirectoryForThisUser();

                    if (!System.IO.Directory.Exists(tmpDir))
                        System.IO.Directory.CreateDirectory(tmpDir);


                    MyReportsActuateHelper oo = new MyReportsActuateHelper(MyReports.Common.AppValues.ServiceAccountUser,
                                                                            MyReports.Common.AppValues.ServiceAccountPassword,
                                                                            MyReports.Common.AppValues.ReportServerName);

                    aryRet = oo.DownloadFiles(ary, tmpDir);

                }
            }

            return aryRet;
        }
        catch (Exception ex)
        {
            DisplayError(ex);

            return new ArrayList();
        }
    }

    #endregion

    #region DeleteFilesFromTempDirectory

    private void DeleteFilesFromTempDirectory(ArrayList files)
    {
        foreach (string file in files)
        {
            try
            {
                if (System.IO.File.Exists(file))
                    System.IO.File.Delete(file);
            }
            catch (Exception ex)
            {
                continue;
            }
        }
    }

    #endregion

    #region Delete Temp Directory

    private void DeleteTempDirectory()
    {
        try
        {
            string tmpDir = Functions.GetTempDirectoryForThisUser();
            if (System.IO.Directory.Exists(tmpDir))
            {
                // remove any stray files that might be left over from attached files
                string[] fFilelist = System.IO.Directory.GetFiles(tmpDir);
                foreach (string curFile in fFilelist)
                {
                    if (System.IO.File.Exists(curFile))
                    {
                        System.IO.File.SetAttributes(curFile, System.IO.FileAttributes.Normal);
                        System.IO.File.Delete(curFile);
                    }
                }

                // delete the temp directory
                System.IO.Directory.Delete(tmpDir);
            }
        }
        catch (Exception ex)
        {
            DisplayError(new Exception("Could not delete temp folder", ex));
        }
    }

    #endregion

    #region Zip Files

    private string Zipfiles(ArrayList aryFiles)
    {
        try
        {
            int x = 0;
            string[] tmp = new string[aryFiles.Count];
            foreach (string file in aryFiles)
            {
                tmp[x] = file;

                x++; 
            }

            string zipFile = Functions.GetTempDirectoryForThisUser() + "MyZipFileName.zip";
            Compass.FileSystem.Compression.ZipUtils.UpdateZipFile(zipFile, tmp);

            return zipFile;
        }
        catch (Exception ex)
        {
            DisplayError(new Exception("Could not Zip Files", ex));
            return "";
        }
    }

    #endregion

    #region DoDownload

    private void DoDownload(string zipLocation)
    {
        try
        {
            string zipFile = Functions.GetTempDirectoryForThisUser() + "MyZipFileName.zip";

            Response.Redirect("../../Common/FileDownload/dodownload.aspx?SaveFileName=MyZipFileName.zip&Filename=" + zipLocation, false);

        }
        catch (Exception ex)
        {
            //DisplayError(new Exception("Could not download zip Files", ex));

        }
    }

    #endregion

    #region DeleteZipFile

    private void DeleteZipFile(string zipLocation)
    {
        try
        {
            if (System.IO.File.Exists(zipLocation))
                System.IO.File.Delete(zipLocation);
        }
        catch (Exception ex)
        {
            DisplayError(new Exception("Could not delete zip file", ex));
        }
    }

    #endregion

    #region SendEmail

    private void SendEmail()
    {
        ArrayList attachments = new ArrayList();

        try
        {
            string SMTP = MyReports.Common.AppValues.SMTP;

            string subject = this.EmailSelectedFiles1.Subject.Trim();
            string message = this.EmailSelectedFiles1.Message.Trim();
            string toAddress = this.EmailSelectedFiles1.ClientEmailAddress.Trim();
            string fromAddress = this.EmailSelectedFiles1.FromEmailAddress.Trim();
            string CC1 = this.EmailSelectedFiles1.CC1.Trim();
            string CC2 = this.EmailSelectedFiles1.CC2.Trim();
            string BCC1 = this.EmailSelectedFiles1.BCC1.Trim();

            //attachments = SaveFilesToTempDirectory();
            attachments = GetAttachmentList();


            MailAddress maFrom = new MailAddress("DoNotReply@Compass-USA.com", fromAddress);
                                                
            MailAddressCollection macTO = new MailAddressCollection();
            if (toAddress.Length > 0)
                macTO.Add(toAddress.Trim());

            MailAddressCollection macCC = new MailAddressCollection();
            if (CC1.Length > 0)
                macCC.Add(CC1);

            if (CC2.Length > 0)
                macCC.Add(CC2);

            MailAddressCollection macBCC = new MailAddressCollection();
            if (BCC1.Length > 0)
                macBCC.Add(BCC1);

            EmailHelper.SendMail(SMTP,
                                    subject,
                                    message,
                                    macTO,
                                    maFrom,
                                    macCC,
                                    macBCC,
                                    attachments,
                                    System.Net.Mail.MailPriority.Normal,
                                    false);

            // Log the audit
            AuditTracking oAuditTracking = new AuditTracking(MyReports.Common.AppValues.MyReportsDB);
            int idPK = oAuditTracking.Insert(MyReports.Common.AppValues.UserName, toAddress, fromAddress, CC1, CC2, BCC1);

            // Log the files for the audit
            AuditTracking oAuditTrackingDetails = new AuditTracking(MyReports.Common.AppValues.MyReportsDB);
            oAuditTrackingDetails.InsertDetail(idPK, attachments);

            // Clear out the basket
            Session["SelectedReports"] = null;
            Session["AttachedReports"] = null;

            // Clear out email controls
            this.EmailSelectedFiles1.ClientEmailAddress = null;
            this.EmailSelectedFiles1.CC1 = null;
            this.EmailSelectedFiles1.CC2 = null;
            this.EmailSelectedFiles1.BCC1 = null;
            this.EmailSelectedFiles1.FromEmailAddress = null;
            this.EmailSelectedFiles1.Subject = "[FROM] Current period Invoice";
            this.EmailSelectedFiles1.Message = "Please find attached your current period subsidy bill and supporting financial statement(s).   \r\n\r\nPlease DO NOT reply to this e-mail.  If you have any questions, please contact the number provided at the bottom of your subsidy invoice, as this email address does not accept replies.\r\n\r\n\r\n\r\nThank you,\r\n\r\n[FROM] Department\r\n\r\n";

            // Return to the start screen
            ShowStartScreen();
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
        finally
        {
            // Cleanup the files
            DeleteFilesFromTempDirectory(attachments);

            // Clean up the tmp directory
            DeleteTempDirectory();
        }
    }

    #endregion

    #region Download Files

    private void DownloadFiles()
    {
        string zipFile = "";
        ArrayList aryFiles = new ArrayList();

        try
        {
            aryFiles = SaveFilesToTempDirectory();

            zipFile = Zipfiles(aryFiles);
            
            DoDownload(zipFile);
           
        }
        catch (Exception ex)
        {
            DisplayError(new Exception("Could not download zip file", ex));
        }
        finally
        {
           // DeleteFilesFromTempDirectory(aryFiles);
           // DeleteZipFile(zipFile);
           // DeleteTempDirectory();
        }
    }

    #endregion

    #region HideControls

    private void HideControls()
    {
        this.SelectedFiles1.Visible = false;
        this.ClientEmailAddress1.Visible = false;
        this.EmailSearch1.Visible = false;
        this.EmailSelectedFiles1.Visible = false;
        this.ClientEmailFromSearch1.Visible = false;
        this.ClientEmailSendFrom1.Visible = false;
        this.AttachReports1.Visible = false;
        this.EmailMessageSearch1.Visible = false;
        this.EmailMessage1.Visible = false;
    }

    #endregion

    #region Show Start Screen

    private void ShowStartScreen()
    {
        // Show the selected files listing.
        HideControls();
        this.SelectedFiles1.Visible = true;
        this.SelectedFiles1.LoadData();
        this.Navigation1.HideItems();
    }

    #endregion

    #region ShowEmailSearchScreen

    private void ShowEmailSearchScreen()
    {
        HideControls();
        this.EmailSearch1.Visible = true;
        this.EmailSearch1.ClearScreen();
    }

    #endregion

    #region Show Email From Search Screen

    private void ShowEmailFromSearchScreen()
    {
        HideControls();
        this.Navigation1.HideItems();
        this.Navigation1.Display_SearchFromEmailLink = true;

        this.ClientEmailFromSearch1.Visible = true;
        this.ClientEmailFromSearch1.ClearScreen();                
    }

    #endregion

    #region show email message search
    private void ShowEmailMessageSearch()
    {
        HideControls();
        this.Navigation1.HideItems();
        this.Navigation1.Display_EmailMessageSearch = true;

        this.EmailMessageSearch1.Visible = true;
        this.EmailMessageSearch1.ClearScreen();
    }
    #endregion

    # region Show Email Message Screen

    private void ShowEmailMessageScreen()
    {
        HideControls();
        this.EmailMessage1.Visible = true;
    }

    #endregion
}
