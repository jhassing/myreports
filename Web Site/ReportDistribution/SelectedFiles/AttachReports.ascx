<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttachReports.ascx.cs" Inherits="ReportDistribution_SelectedFiles_AttachReports" %>
<p style=" width: 90%" >Please select PDF files only.  Any other type of file will not be attached.  Also please do not exceed 4 MB in total file size.  Doing so will result in a "Cannot find server or DNS" error message or the page may go blank. In this case you must refresh the page and continue.</p>

<table width=90%>
    <tr>
        <td style="width: 110px" align=center><asp:Label runat="server" Text="Report 1:" ID="lblReport1" /></td>
        <td nowrap><asp:FileUpload ID="FileUpload1" runat="server" Width=100% /></td>
    </tr>
    <tr>
        <td style="width: 110px" align=center><asp:Label runat="server" Text="Report 2:" ID="lblReport2" /></td>
        <td nowrap><asp:FileUpload ID="FileUpload2" runat="server" Width=100% /></td>
    </tr>
    <tr>
        <td style="width: 110px" align=center><asp:Label runat="server" Text="Report 3:" ID="lblReport3" /></td>
        <td nowrap><asp:FileUpload ID="FileUpload3" runat="server" Width=100% /></td>
    </tr>
    <tr>
        <td style="width: 110px" align=center><asp:Label runat="server" Text="Report 4:" ID="lblReport4" /></td>
        <td nowrap><asp:FileUpload ID="FileUpload4" runat="server" Width=100% /></td>
    </tr>
    <tr>
        <td style="width: 110px" align=center><asp:Label runat="server" Text="Report 5:" ID="lblReport5" /></td>
        <td nowrap><asp:FileUpload ID="FileUpload5" runat="server" Width=100% /></td>
    </tr>
    <tr>
        <td align=center><asp:Image ID="imgErr" runat="server" Visible="false" ImageAlign="AbsMiddle" ImageUrl="../../images/red-error.gif" /></td>
        <td><asp:Label ID="lblErr" runat="server" Visible="false" Font-Size="Larger" ForeColor="red" Text="Error: Invalid file found. Please make sure all attachments are PDF files. Select again or cancel to return to the previous page" /></td>
    </tr>
    <tr>
        <td></td>
        <td><asp:Button ID="btnAttachReports" runat="server" Text="Attach Selected Reports" OnClick="btnAttachReports_Click" Width=200 /></td>
    </tr>
    <tr><td></td><td></td></tr>
    <tr>
        <td></td>
        <td><asp:Button ID="btnCancle" runat="server" Text="Cancel" OnClick="btnCancel_Click" Width=200 /></td>
    </tr>
</table>