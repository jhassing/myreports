
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
//  Using sub name spaces
using MyReports.Common;

public partial class ReportDistribution_SelectedFiles : System.Web.UI.UserControl
{
    private bool bSelectedReportsExist;
    private bool bAttachedReportsExist;

    #region LoadData

    public void LoadData()
    {
        //this.lblNoFilesSelected.Visible = false;
        this.trNoFilesSelected.Visible = false;
        this.trNoAttachmentsSelected.Visible = false;

        ArrayList selected_ary = null;
        ArrayList attached_ary = null;

        // first check to see if any selected or attached files exist and place them into an ArrayList if they do
        if (Session["SelectedReports"] != null)
        {
            selected_ary = (ArrayList)Session["SelectedReports"];
            if (selected_ary.Count != 0)
            {
                this.bSelectedReportsExist = true;
            }
        }
        if (Session["AttachedReports"] != null)
        {
            attached_ary = (ArrayList)Session["AttachedReports"];
            if (attached_ary.Count != 0)
            {
                this.bAttachedReportsExist = true;
                lnkEmailFiles.Enabled = true;
            }
        }

        if (Session["SelectedReports"] == null)
        {
            // No files to display
            ShowNoFiles();
        }
        else
        {
            if (selected_ary.Count == 0)
            {
                // No files to display
                ShowNoFiles();
            }
            else
            {
                // We have files. Display them.
                this.DocumentList.DataSource = selected_ary;
                this.DocumentList.DataBind();
            }
        }

        if (Session["AttachedReports"] == null)
        {
            ShowNoAttachments();
        }
        else
        {
            attached_ary = (ArrayList)Session["AttachedReports"];
            if (attached_ary.Count == 0)
            {
                ShowNoAttachments();
            }
            else
            {
                // we have attachments. Display them.
                List<string> lAttachments = new List<string>();

                foreach (string ary in attached_ary)
                {
                    string[] sFilePathName = ary.Split('\\');
                    string sFileName = sFilePathName[sFilePathName.Length - 1];
                    lAttachments.Add(sFileName);
                }

                this.AttachmentList.DataSource = lAttachments;
                this.AttachmentList.DataBind();
            }
        }
    }

    #endregion

    #region ShowNoFiles

    private void ShowNoFiles()
    {
        this.DocumentList.DataSource = null;
        this.DocumentList.DataBind();

        this.trNoFilesSelected.Visible = true;
        this.lnkDownloadFiles.Enabled = false;

        if (bAttachedReportsExist == false)
            this.lnkEmailFiles.Enabled = false;
    }

    private void ShowNoAttachments()
    {
        this.AttachmentList.DataSource = null;
        this.AttachmentList.DataBind();

        this.trNoAttachmentsSelected.Visible = true;

        if (bSelectedReportsExist == false)
            this.lnkEmailFiles.Enabled = false;
    }

    #endregion

    #region DocumentList_OnItemDataBound

    protected void DocumentList_OnItemDataBound(Object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.AlternatingItem ||
            e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.SelectedItem)
        {
            HyperLink AcLnkDocumentName = (HyperLink)e.Item.FindControl("AcLnkDocumentName");
            ImageButton lnkRemove = (ImageButton)e.Item.FindControl("lnkRemove");

            AcLnkDocumentName.Text = e.Item.DataItem.ToString();

            lnkRemove.CommandArgument = e.Item.DataItem.ToString().ToLower();
            
        }
    }

    protected void AttachmentList_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        HyperLink AttLnkDocName = (HyperLink)e.Item.FindControl("AttLnkDocName");
        ImageButton lnkAttachedRemove = (ImageButton)e.Item.FindControl("lnkAttachedRemove");

        AttLnkDocName.Text = e.Item.DataItem.ToString();
        lnkAttachedRemove.CommandArgument = e.Item.DataItem.ToString().ToLower();
    }

    protected void lnkRemove_Command(object sender, CommandEventArgs e)
    {
        string reportName = e.CommandArgument.ToString();

        ArrayList ary = (ArrayList)Session["SelectedReports"];

        if (ary.Contains(reportName))
            ary.Remove(reportName);

        Session["SelectedReports"] = ary;

        LoadData();
    }

    protected void lnkAttachedRemove_Command(object sender, CommandEventArgs e)
    {
        string tmpDir = Functions.GetTempDirectoryForThisUser();
        string reportName = tmpDir + e.CommandArgument.ToString();
        ArrayList attached_ary = (ArrayList)Session["AttachedReports"];

        if (attached_ary.Contains(reportName))
        {
            attached_ary.Remove(reportName);
            if (System.IO.File.Exists(reportName))
            {
                System.IO.File.Delete(reportName);
            }
        }

        Session["AttachedReports"] = attached_ary;

        LoadData();
    }

    #endregion

}
