using System;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using ReportDistribution.SQLServerDAL.ClientEmail;

public partial class ReportDistribution_SelectedFiles_EmailMessageSearch : System.Web.UI.UserControl
{
    public void ClearScreen()
    {
        this.txtEmailTitle.Text = "";

        this.GridView1.DataSource = null;
        this.GridView1.DataBind();
        this.GridView1.Visible = false;
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName.ToLower() == "select")
            {
                int index = Convert.ToInt32(e.CommandArgument);

                int ind = System.Convert.ToInt32(this.GridView1.DataKeys[index]["EmailMessageID"].ToString());

                CommandEventArgs ce = new CommandEventArgs("EditIndividualEmailMessage", ind);

                RaiseBubbleEvent(sender, ce);
            }
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }

    protected void ObjectDataSource1_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = new EmailMessage(MyReports.Common.AppValues.MyReportsDB);
    }

    private void DisplayError(Exception ex)
    {
        this.GridView1.Visible = false;
        this.lblErrorMessage.Visible = true;
        string mess = ex.Message;
        mess += "\r\n";

        if (ex.InnerException != null)
            mess += ex.InnerException.Message;

        this.lblErrorMessage.Text = mess;

        ExceptionManager.Publish(ex);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.GridView1.Visible = true;
            this.GridView1.DataBind();
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        CommandEventArgs ce = new CommandEventArgs("addnewemailmessage", "");

        RaiseBubbleEvent(sender, ce);
    }
}
