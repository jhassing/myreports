using System;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using ReportDistribution.SQLServerDAL;
using ReportDistribution.SQLServerDAL.ClientEmail;

public partial class FormView_ClientEmailAddress : System.Web.UI.UserControl
{
    public int ClientEmailAddressID
    {
        get { return System.Convert.ToInt32(ViewState["ClientEmailAddressID"]); }
        set { ViewState["ClientEmailAddressID"] = value; }
    }
        

    public FormViewMode Mode
    {
        set { this.FormView1.ChangeMode(value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblMessage.Visible = false;   
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        try
        {
            FormView1.DataBind();
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    protected void ObjectDataSource1_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        try
        {
            e.ObjectInstance = new Address(MyReports.Common.AppValues.MyReportsDB);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }

    }
    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        e.InputParameters["ClientEmailAddressID"] = this.ClientEmailAddressID;

    }
    protected void ObjectDataSource1_Updating(object sender, ObjectDataSourceMethodEventArgs e)
    {
        try
        {
            e.InputParameters["ClientEmailAddressID"] = this.ClientEmailAddressID;
            e.InputParameters["modifiedBy"] = MyReports.Common.AppValues.UserName;
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }

    }
    protected void ObjectDataSource1_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        try
        {
            e.InputParameters["createdBy"] = MyReports.Common.AppValues.UserName;
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    protected void ObjectDataSource1_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
    {
        e.InputParameters["ClientEmailAddressID"] = this.ClientEmailAddressID;
    }
    protected void ObjectDataSource2_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        try
        {
            e.ObjectInstance = new Zrops(MyReports.Common.AppValues.MyReportsDB);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    protected void ObjectDataSource2_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {

    }
    protected void ObjectDataSource2_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {

    }
    protected void odFromEmailAdd_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {        
        try
        {
            e.ObjectInstance = new ClientEmailSendFrom(MyReports.Common.AppValues.MyReportsDB);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }

    }
    protected void btnInsertNew_Click(object sender, EventArgs e)
    {
        this.Mode = FormViewMode.Insert;
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {

    }
    protected void btnCancelUpdate_Click(object sender, EventArgs e)
    {

    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {

    }
    protected void ObjectDataSource1_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        try
        {
            // Now re-direct back to the listing page
            CommandEventArgs ce = new CommandEventArgs("showemailsearchscreen", "");

            RaiseBubbleEvent(sender, ce);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }

    }

    #region DisplayError

    private void DisplayError(Exception ex)
    {
        this.lblMessage.Visible = true;
        string mess = ex.Message;
        mess += "\r\n";

        if (ex.InnerException != null)
            mess += ex.InnerException.Message;

        this.lblMessage.Text = mess;

        ExceptionManager.Publish(ex);
    }

    #endregion

    protected void btnUpdate_Click1(object sender, EventArgs e)
    {
        try
        {
            // Now re-direct back to the listing page
            CommandEventArgs ce = new CommandEventArgs("showstartscreen", "");

            RaiseBubbleEvent(sender, ce);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
    protected void btnCancelUpdate_Click1(object sender, EventArgs e)
    {
        try
        {
            // Now re-direct back to the listing page
            CommandEventArgs ce = new CommandEventArgs("showstartscreen", "");

            RaiseBubbleEvent(sender, ce);
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }
}
