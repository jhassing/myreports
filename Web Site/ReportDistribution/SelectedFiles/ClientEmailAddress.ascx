<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClientEmailAddress.ascx.cs" Inherits="FormView_ClientEmailAddress" %>
	<TABLE cellSpacing="0" cellPadding="0" width="90%" border="0">
	<TR>
		<TD colspan="3" class="TableHeaderRow" id="td1" vAlign="middle" noWrap runat="server">
			<asp:Label id="Label1" CssClass="X-SmallBold_White" Runat="server">Selected Reports</asp:Label></TD>
	</TR>
	</TABLE>
&nbsp;<asp:FormView ID="FormView1" runat="server" DataKeyNames="ClientEmailAddressID"
    DataSourceID="ObjectDataSource1" DefaultMode="Edit">
    <EditItemTemplate>
        <table border="0" cellpadding="3" cellspacing="0" width="475">
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label2" CssClass="X-SmallBold" runat="server" Text="Client Email Address:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="ClientEmailAddressTextBox" runat="server" Text='<%# Bind("ClientEmailAddress") %>'></asp:TextBox>               
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" ControlToValidate="ClientEmailAddressTextBox"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label1" CssClass="X-SmallBold" runat="server" Text="Client Name:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="ClientNameTextBox" runat="server" Text='<%# Bind("ClientName") %>'></asp:TextBox>               
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ControlToValidate="ClientNameTextBox"></asp:RequiredFieldValidator>
                </td>
            </tr>            
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label3" CssClass="X-SmallBold" runat="server" Text="Cost Center:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="txtCostCenter" runat="server" Text='<%# Bind("KeyWord") %>'></asp:TextBox>               
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="txtCostCenter"></asp:RequiredFieldValidator>
                </td>                
            </tr>              
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label10" CssClass="X-SmallBold" runat="server" Text="From Address:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:DropDownList CssClass="X-Small" autocomplete="off" ID="DropDownList2" runat="server" DataSourceID="odFromEmailAdd" Width="95%" DataTextField="FromEmailAddress" DataValueField="ClientEmailSendFromID" SelectedValue='<%# Bind("ClientEmailSendFromID") %>'></asp:DropDownList>
                </td>
                <td>
                    &nbsp;
                </td>                
            </tr> 
            
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label4" CssClass="X-SmallBold" runat="server" Text="CC 1:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="txtCC1" runat="server" Text='<%# Bind("CC1") %>'></asp:TextBox>               
                </td>
                <td>
                    &nbsp;
                </td>                   
            </tr>  
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label5" CssClass="X-SmallBold" runat="server" Text="CC 2:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="txtCC2" runat="server" Text='<%# Bind("CC2") %>'></asp:TextBox>               
                </td>
                <td>
                    &nbsp;
                </td>                   
            </tr>  
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label6" CssClass="X-SmallBold" runat="server" Text="BCC 1:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="TextBox4" runat="server" Text='<%# Bind("BCC1") %>'></asp:TextBox>               
                </td>
                <td>
                    &nbsp;
                </td>                   
            </tr>              
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label7" CssClass="X-SmallBold" runat="server" Text="Acct First Name:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="txtAcctFN" runat="server" Text='<%# Bind("AccountantFN") %>'></asp:TextBox>               
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ControlToValidate="txtAcctFN"></asp:RequiredFieldValidator>
                </td>                
            </tr>     
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label8" CssClass="X-SmallBold" runat="server" Text="Acct Last Name:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="txtAcctLN" runat="server" Text='<%# Bind("AccountantLN") %>'></asp:TextBox>               
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" ControlToValidate="txtAcctLN"></asp:RequiredFieldValidator>
                </td>                
            </tr> 
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label9" CssClass="X-SmallBold" runat="server" Text="Region:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:DropDownList CssClass="X-Small" autocomplete="off" ID="DropDownList1" runat="server" DataSourceID="ObjectDataSource2" SelectedValue='<%# Bind("Region") %>' Width="95%"></asp:DropDownList>       
                </td>
            </tr> 
            <tr>
                <td nowrap width="100%" align=right colspan="2">
                    <DIV style="HEIGHT: 2px">
                        <HR>
                    </DIV>                                        
                    <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnUpdate" runat="server" CommandName="Update" Text="Update" OnClick="btnUpdate_Click" />&nbsp;&nbsp;
                    <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnCancelUpdate" runat="server" CommandName="Cancel" Text="Cancel" OnClick="btnCancelUpdate_Click" CausesValidation="False" />&nbsp;&nbsp;                                
                    <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnDelete" runat="server" CommandName="Delete" Text="Delete" OnClick="btnDelete_Click" CausesValidation="False" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtAcctFN" ErrorMessage="The Accountant's First name is required." Display="Static"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtAcctLN" ErrorMessage="The Accountant's Last name is required." Display="Static"></asp:RequiredFieldValidator>
                </td>
            </tr>
                                                                                    
        </table>
    </EditItemTemplate>
    
    <InsertItemTemplate>
        <table border="0" cellpadding="3" cellspacing="0" width="475">
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label2" CssClass="X-SmallBold" runat="server" Text="Client Email Address:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="ClientEmailAddressTextBox" runat="server" Text='<%# Bind("ClientEmailAddress") %>'></asp:TextBox>               
                </td>
            </tr>
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label1" CssClass="X-SmallBold" runat="server" Text="Client Name:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="ClientNameTextBox" runat="server" Text='<%# Bind("ClientName") %>'></asp:TextBox>               
                </td>
            </tr>            
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label3" CssClass="X-SmallBold" runat="server" Text="Cost Center:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="TextBox1" runat="server" Text='<%# Bind("KeyWord") %>'></asp:TextBox>               
                </td>
            </tr>              
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label10" CssClass="X-SmallBold" runat="server" Text="From Address:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:DropDownList CssClass="X-Small" autocomplete="off" ID="DropDownList2" runat="server" DataSourceID="odFromEmailAdd" Width="95%" DataTextField="FromEmailAddress" DataValueField="ClientEmailSendFromID" SelectedValue='<%# Bind("ClientEmailSendFromID") %>'></asp:DropDownList>
                </td>
            </tr> 
            
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label4" CssClass="X-SmallBold" runat="server" Text="CC 1:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="TextBox2" runat="server" Text='<%# Bind("CC1") %>'></asp:TextBox>               
                </td>
            </tr>  
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label5" CssClass="X-SmallBold" runat="server" Text="CC 2:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="TextBox3" runat="server" Text='<%# Bind("CC2") %>'></asp:TextBox>               
                </td>
            </tr>  
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label6" CssClass="X-SmallBold" runat="server" Text="BCC 1:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="TextBox4" runat="server" Text='<%# Bind("BCC1") %>'></asp:TextBox>               
                </td>
            </tr>              
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label7" CssClass="X-SmallBold" runat="server" Text="Acct First Name:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="TextBox5" runat="server" Text='<%# Bind("AccountantFN") %>'></asp:TextBox>               
                </td>
            </tr>     
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label8" CssClass="X-SmallBold" runat="server" Text="Acct Last Name:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:TextBox Width="95%" CssClass="X-Small" autocomplete="off" ID="TextBox6" runat="server" Text='<%# Bind("AccountantLN") %>'></asp:TextBox>               
                </td>
            </tr> 
            <tr>
                <td nowrap align=right>
                    <asp:Label ID="Label9" CssClass="X-SmallBold" runat="server" Text="Region:"></asp:Label>                    
                </td>
                <td nowrap width="100%">
                    <asp:DropDownList CssClass="X-Small" autocomplete="off" ID="DropDownList1" runat="server" DataSourceID="ObjectDataSource2" SelectedValue='<%# Bind("Region") %>' Width="95%"></asp:DropDownList>       
                </td>
            </tr> 
            <tr>
                <td nowrap width="100%" align=right colspan="2">
                    <DIV style="HEIGHT: 2px">
                        <HR>
                    </DIV>                
                    <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnUpdate" runat="server" CommandName="Insert" Text="Insert" OnClick="btnUpdate_Click1" />&nbsp;&nbsp;
                    <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnCancelUpdate" runat="server" CommandName="Cancel" Text="Cancel" OnClick="btnCancelUpdate_Click1" />
                </td>
            </tr>
                                                                                    
        </table>
    </InsertItemTemplate>
   </asp:FormView>

&nbsp;<asp:Label ID="lblMessage" runat="server" CssClass="X-Small" ForeColor="Red"
    Height="41px" Text="[Message]" Visible="False" Width="473px"></asp:Label>
<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DeleteMethod="Delete"
    InsertMethod="Insert" OnDeleting="ObjectDataSource1_Deleting" OnInserting="ObjectDataSource1_Inserting"
    OnObjectCreating="ObjectDataSource1_ObjectCreating" OnSelecting="ObjectDataSource1_Selecting"
    OnUpdating="ObjectDataSource1_Updating" SelectMethod="Get" TypeName="ReportDistribution.BLL.ClientEmail"
    UpdateMethod="Update" OnUpdated="ObjectDataSource1_Updated">
    <DeleteParameters>
        <asp:Parameter Name="clientEmailAddressID" Type="Int32" />
    </DeleteParameters>
    <UpdateParameters>
        <asp:Parameter Name="clientEmailAddressID" Type="Int32" />
        <asp:Parameter Name="clientEmailAddress" Type="String" />
        <asp:Parameter Name="clientName" Type="String" />
        <asp:Parameter Name="KeyWord" Type="String" />
        <asp:Parameter Name="ClientEmailSendFromID" Type="Int32" />
        <asp:Parameter Name="CC1" Type="String" />
        <asp:Parameter Name="CC2" Type="String" />
        <asp:Parameter Name="BCC1" Type="String" />
        <asp:Parameter Name="accountantFN" Type="String" />
        <asp:Parameter Name="accountantLN" Type="String" />
        <asp:Parameter Name="region" Type="String" />
        <asp:Parameter Name="modifiedBy" Type="String" />
    </UpdateParameters>
    <SelectParameters>
        <asp:Parameter Name="clientEmailAddressID" Type="Int32" />
    </SelectParameters>
    <InsertParameters>
        <asp:Parameter Name="clientEmailAddress" Type="String" />
        <asp:Parameter Name="clientName" Type="String" />
        <asp:Parameter Name="KeyWord" Type="String" />
        <asp:Parameter Name="CC1" Type="String" />
        <asp:Parameter Name="CC2" Type="String" />
        <asp:Parameter Name="BCC1" Type="String" />
        <asp:Parameter Name="accountantFN" Type="String" />
        <asp:Parameter Name="accountantLN" Type="String" />
        <asp:Parameter Name="region" Type="String" />
        <asp:Parameter Name="createdBy" Type="String" />
    </InsertParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OnObjectCreating="ObjectDataSource2_ObjectCreating"
    OnSelected="ObjectDataSource2_Selected" OnSelecting="ObjectDataSource2_Selecting"
    SelectMethod="GetDistinctRegionCodeListing" TypeName="ReportDistribution.BLL.Zrops">
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="odFromEmailAdd" runat="server" OnObjectCreating="odFromEmailAdd_ObjectCreating"
    SelectMethod="Get" TypeName="ReportDistribution.BLL.ClientEmailSendFrom">
    <SelectParameters>
        <asp:Parameter Name="fromEmailAddress" Type="String" />
    </SelectParameters>

</asp:ObjectDataSource>
