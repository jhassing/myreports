<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailMessageSearch.ascx.cs" Inherits="ReportDistribution_SelectedFiles_EmailMessageSearch" %>
<TABLE cellSpacing="0" cellPadding="0" width="90%" border="0">
	<TR>
		<TD id="TD1" class="TableHeaderRow" vAlign="middle" noWrap runat="server">
			<asp:Label ID="lblHeader" CssClass="X-SmallBold_White" Runat="server">Edit Email Message Search</asp:Label>
	    </TD>
	</TR>
</TABLE>
<table border="0" cellpadding="0" cellspacing="6">
    <tr>
        <td nowrap style="width: 152px" align="right">
            <asp:Label ID="lblEmailTitleTag" CssClass="X-Small" runat="server" Text="Email Title:"></asp:Label>
        </td>
        <td nowrap style="width: 400px" >
            <asp:TextBox id="txtEmailTitle" CssClass="XX-SmallTextBox" runat="server" Width="100%"	MaxLength="50" ></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td nowrap colspan="2" align="right">
            <asp:Button ID="btnSearch" CssClass="MyReportsAdminConsoleButton" runat="server" Text="Search" OnClick="btnSearch_Click" />&nbsp;&nbsp;
            <asp:Button ID="btnAddNew" CssClass="MyReportsAdminConsoleButton" runat="server" Text="Add New" CommandName="addnewemailmessage" OnClick="btnAddNew_Click" />
        </td>        
    </tr>
</table>
<asp:Label Visible=false ID="lblErrorMessage" ForeColor="Red" runat="server" CssClass="X-Small" Text="[Message]"></asp:Label>  
<asp:GridView   ID="GridView1" runat="server" AllowPaging="True" CellPadding="2" 
                DataSourceID="ObjectDataSource1" DataKeyNames="EmailMessageID" ForeColor="#333333" GridLines="None" 
                OnRowCommand="GridView1_RowCommand" PageSize="6" Width="90%" AutoGenerateColumns="False">
        <PagerSettings Mode="NextPreviousFirstLast" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <EmptyDataTemplate>
        Nothing
    </EmptyDataTemplate>
    <EditRowStyle BackColor="#999999" />
    
    <PagerStyle BackColor="#284775" ForeColor="White" Height="12px" HorizontalAlign="Center" />
    <HeaderStyle BackColor="#5D7B9D" CssClass="X-Small_White" Font-Bold="True" ForeColor="White"
        HorizontalAlign="Left" VerticalAlign="Bottom" />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    
    <Columns>
        <asp:CommandField SelectText="" ShowSelectButton="True" EditImageUrl="../../images/edit.gif" EditText="" SelectImageUrl="~/images/edit.gif" ButtonType="Image" >
            <ItemStyle Wrap=False CssClass="hyperlink" />
            <HeaderStyle Width="25px" />
        </asp:CommandField>
        <asp:BoundField DataField="emailTitle" HeaderText="Email Message Title" SortExpression="emailTitle">
            <ItemStyle Wrap=False CssClass="X-Small" />
            <HeaderStyle Wrap=False />
        </asp:BoundField>
    </Columns>     
</asp:GridView>
<asp:ObjectDataSource 
    ID="ObjectDataSource1" 
    runat="server" 
    TypeName="ReportDistribution.BLL.EmailMessage" 
    SelectMethod="Get" 
    OnObjectCreating="ObjectDataSource1_ObjectCreating">
    <SelectParameters>
        <asp:ControlParameter ControlID="txtEmailTitle" Name="emailTitle" PropertyName="Text" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
