<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailMessage.ascx.cs" Inherits="ReportDistribution_SelectedFiles_EmailMessage" %>
<TABLE cellSpacing="0" cellPadding="0" width="90%" border="0">
	<TR>
	    <TD class="TableHeaderRow" id="td1" vAlign="middle" noWrap runat="server" style="height: 19px" width="710px">
			<asp:Label ID="lblHeader" CssClass="X-SmallBold_White" Runat="server">Edit Email Message</asp:Label>
        </TD>
	</TR>
</TABLE>
<table id=tblMain runat="server" border="0" cellpadding="4" cellspacing="0" width="90%" >
    <tr>
        <td nowrap align="right" width="130px">
            <asp:Label ID=lblEmailTitleTag CssClass="X-Small" Text="Email Title:" runat="server"></asp:Label>
        </td>
        <td nowrap width="400px" >
            <asp:TextBox ID=txtEmailTitle CssClass="X-Small" runat="server" width="90%"></asp:TextBox>
        </td>
        <td align="Left" valign="top" nowrap width="180px" >
            <asp:Label ID=lblEmailTitleInfo CssClass="X-Small" runat=server Text="Title is used to select from a list of Custom Email records."></asp:Label>
        </td>
        
    </tr>
    <tr>
        <td nowrap align="right" width="130px">
            <asp:Label ID=lblEmailSubjectTag CssClass="X-Small" Text="Email Subject:" runat="server"></asp:Label>
        </td>
        <td nowrap width="400px"  colspan="2">
            <asp:TextBox ID=txtEmailSubject Font-Size="X-Small" runat="server" width="90%" ></asp:TextBox >
        </td>
    </tr>
    <tr>
        <td nowrap align="right" width="130px" valign="top" >
            <asp:Label ID=lblEmailMessageTag CssClass="X-Small" Text="Email Message Body:" runat="server"></asp:Label>
        </td>
        <td width="400px" colspan="2">
            <asp:TextBox ID=txtEmailMessage CssClass="X-Small" runat="server" Width="90%" TextMode="MultiLine" Height="200px" ></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td></td>
        <td align="right">
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnInsert" runat="server" Text="Insert" OnClick="btnInsert_Click" />
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_OnClick" />
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnEdit" runat="server" Text="Edit Existing" OnClick="btnEdit_OnClick" />
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnAddNew" runat="server" Text="Add New" OnClick="btnAddNew_OnClick" />
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnRemove" runat="server" Text="Remove" OnClick="btnRemove_Click" />
            <asp:Button CssClass="MyReportsAdminConsoleButton" ID="btnCancelUpdate" runat="server" Text="Cancel" OnClick="btnCancelUpdate_OnClick" />
        </td>
    </tr>
</table>
<asp:Label ID="lblErrorMessage" runat="server" CssClass="X-Small" ForeColor="Red" Height="41px" Text="[Message]" Visible="False" Width="473px"></asp:Label>