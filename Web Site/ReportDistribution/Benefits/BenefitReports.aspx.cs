using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using actuate.classes.webcontrols;
using activeportal.classes;
using activeportal.classes.api.proxy;
using actuate_i18n;
using System.Collections.Specialized;
using System.Reflection;
using System.Text;

using activeportal.usercontrols.newrequest;

namespace DistributionPortal.ReportDistributionUI.Benefits
{
	/// <summary>
	/// Summary description for index.
	/// </summary>
	public partial class index : System.Web.UI.Page
	{

		private string m_executableFileName;
		public  string ExecutableFileName
		{
			get
			{
				m_executableFileName = (string) ViewState["ExecutableFileName"];
				
				if ( m_executableFileName == null )			
				{
					m_executableFileName = AcParams.Get( Page, AcParams.Name.__executableName );
					ViewState["ExecutableFileName"] = m_executableFileName;
				}
				return m_executableFileName;
			}	
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			string groups = "";

			AcUrlBuilder link = new AcUrlBuilder();

			// Set the base URL
			link.SetBaseUrl( "../../newrequest/do_executereport.aspx", true );

			// Get a pipe delimited string of all the groups this user is a member of
			for (int x=0;x<MyReports.Common.AppValues.UserGroupMembership.Count;x++)
			{
				string group = MyReports.Common.AppValues.UserGroupMembership[x].ToString().ToLower();

				if (	(group.IndexOf ( MyReports.Common.AppValues.FinancialGroups.ToLower()  ) > 0 ) ||
						(group.IndexOf ( MyReports.Common.AppValues.PayrollSalaryGroups.ToLower() ) > 0 ) ||
						(group.IndexOf ( MyReports.Common.AppValues.PayrollHourlyGroups.ToLower() ) > 0 ) )
				{
					
					if ( group.IndexOf ( MyReports.Common.AppValues.FinancialGroups.ToLower()  ) > 0 )
						group = group.Substring ( 0, group.Length - 4 );
					else if (	(group.IndexOf ( MyReports.Common.AppValues.PayrollSalaryGroups.ToLower() ) > 0 ) ||
								(group.IndexOf ( MyReports.Common.AppValues.PayrollHourlyGroups.ToLower() ) > 0 ) )
						group = group.Substring ( 0, group.Length - 5 );

					if ( Compass.Common.Functions.IsNumeric( group ) )
						groups += group + "|";
				}
			}

			// Add the Report parameters
			link.AddKeyValue( "as_NationalAccountNumber", groups );
			link.AddKeyValue( "ODBC", "BENEFITS" );

			string sExecutableName = new String( this.ExecutableFileName.ToCharArray() );

			link.AddKeyValue( AcParams.Name.__executableName, sExecutableName );
			link.AddKeyValue( AcParams.Name.__SourceOfRequest__, "BenefitReports.aspx" );

			string sJobName =  AcParams.Get( Page, AcParams.Name.__jobname );
			if ( sJobName == null )
			{
				sJobName = ActuUtil.stripFileExtension( ActuUtil.stripFolderName( sExecutableName , "/"));
				link.AddKeyValue( AcParams.Name.__jobname, sJobName );
			}

			// Addd the __wait parameter 

			string sWait = AcParams.Get( Page, AcParams.Name.__wait );

			if ( sWait != null && 
				sWait.Trim() != String.Empty )
			{
				link.AddKeyValue( AcParams.Name.__wait , sWait);
			}

			link.AddKeyValue( AcParams.Name.__scheduleType, "immediate" );

			Server.Transfer( link.ToString() );

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion
	}
}
