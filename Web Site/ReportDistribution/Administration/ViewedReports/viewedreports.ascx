<%@ Control Language="c#" Inherits="DistributionPortal.ViewedReports" CodeFile="ViewedReports.ascx.cs" %>
<TABLE class="MyReportsAdminConsoleBorder" cellSpacing="0" cellPadding="0" width="100%"
	border="0">
	<tr>
		<td colspan="4">
		    <asp:Label CssClass="XX-Small" ID="Label5" Runat="server">Start Date</asp:Label>
			<asp:TextBox CssClass="XX-Small" id="txtStartDate" runat="server"></asp:TextBox>
			<asp:Label CssClass="XX-Small" ID="Label6" Runat="server">Start Date</asp:Label>
			<asp:TextBox CssClass="XX-Small" id="txtEndDate" runat="server"></asp:TextBox>
			<asp:RadioButtonList CssClass="XX-Small" ID="rblReportType" runat="server">
			    <asp:ListItem Text="AR Report" Value="ZCAR2201"/>
			    <asp:ListItem Text="Subidy Invoice" Value="ZCAR2821" />
			</asp:RadioButtonList>
			&nbsp;&nbsp;&nbsp;
			<asp:Button CssClass="MyReportsAdminConsoleButton" id="Button1" runat="server" Text="btnSubmit" onclick="Button1_Click"></asp:Button>
		</td>
	</tr>
	<TR>
		<TD class="TableHeaderRow" colSpan="1" height="1"><SPAN class="XX-SmallBold_White">User 
				Name</SPAN>
		</TD>
		<TD class="TableHeaderRow" colSpan="1" height="1"><SPAN class="XX-SmallBold_White">Viewed 
				Date</SPAN>
		</TD>
		<TD class="TableHeaderRow" colSpan="1" height="1"><SPAN class="XX-SmallBold_White">Report 
				Location</SPAN>
		</TD>
		<TD class="TableHeaderRow" colSpan="2" height="1"><SPAN class="XX-SmallBold_White">Report 
				Name</SPAN>
		</TD>
	</TR>
	<TR width="100%">
		<TD width="100%" colSpan="6"><IMG height="4" src="../../../images/vspace.gif" width="100%" border="0"></TD>
	</TR>
	<asp:repeater id="Repeater1" runat="server" OnItemDataBound="Repeater1_OnItemDataBound">
		<AlternatingItemTemplate>
			<tr runat="server" class="AltTableRow" height="1" ID="Tr1" >
				<td nowrap>
					<asp:Label CssClass="XX-Small" ID="lblUserName" Runat="server"></asp:Label>
				</td>
				<td nowrap>
					<asp:Label CssClass="XX-Small" ID="lblViewedDate" Runat="server"></asp:Label>
				</td>
				<td nowrap>
					<asp:Label CssClass="XX-Small" ID="lblReportLocation" Runat="server"></asp:Label>
				</td>
				<td nowrap>
					<asp:Label CssClass="XX-Small" ID="lblReportName" Runat="server"></asp:Label>
				</td>
			</tr>
		</AlternatingItemTemplate>
		<ItemTemplate>
			<tr runat="server" class="TableRow" height="1" ID="Tr2" >
				<td nowrap>
					<asp:Label CssClass="XX-Small" ID="lblUserName" Runat="server"></asp:Label>
				</td>
				<td nowrap>
					<asp:Label CssClass="XX-Small" ID="lblViewedDate" Runat="server"></asp:Label>
				</td>
				<td nowrap>
					<asp:Label CssClass="XX-Small" ID="lblReportLocation" Runat="server"></asp:Label>
				</td>
				<td nowrap>
					<asp:Label CssClass="XX-Small" ID="lblReportName" Runat="server"></asp:Label>
				</td>
			</tr>
		</ItemTemplate>
	</asp:repeater></TABLE>
