using MyReports.Common;
using ReportDistribution.SQLServerDAL;

namespace DistributionPortal
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	/// <summary>
	///		Summary description for ViewedReports.
	/// </summary>
	public partial class ViewedReports : System.Web.UI.UserControl
	{

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PreRender += new System.EventHandler(this.ViewedReports_PreRender);

		}
		#endregion

		protected void ViewedReports_PreRender(object sender, EventArgs e)
		{

		}

		protected void Repeater1_OnItemDataBound( Object sender, RepeaterItemEventArgs e )
		{
			if (	e.Item.ItemType == ListItemType.AlternatingItem || 
				e.Item.ItemType == ListItemType.Item ||
				e.Item.ItemType == ListItemType.SelectedItem )
			{
				// Find all the controls needed
				
				
				Label lblUserName			= ( Label ) e.Item.FindControl( "lblUserName" );
				Label lblViewedDate			= ( Label ) e.Item.FindControl( "lblViewedDate" );
				Label lblReportLocation		= ( Label ) e.Item.FindControl( "lblReportLocation" );
				Label lblReportName			= ( Label ) e.Item.FindControl( "lblReportName" );

								
				System.Data.DataRowView drv = ( DataRowView )e.Item.DataItem;


				lblUserName.Text		= drv.Row.ItemArray[0].ToString();
				lblViewedDate.Text		= drv.Row.ItemArray[1].ToString();
				lblReportLocation.Text	= drv.Row.ItemArray[2].ToString();
				lblReportName.Text		= drv.Row.ItemArray[1].ToString();

				
			}
		}
		
			
		private void LoadData()
		{
					
			DateTime startDate	= System.Convert.ToDateTime( this.txtStartDate.Text );
			DateTime endDate	= System.Convert.ToDateTime( this.txtEndDate.Text );

            Repeater1.DataSource = new Reports(AppValues.MyReportsDB).GetViewedReports(startDate, endDate, "", "");

			Repeater1.DataBind();
		}

		protected void Button1_Click(object sender, System.EventArgs e)
		{
			LoadData();
		}
	}
}
