using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ReportDistribution_Administration_Administration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected string GetTabStripSrc
    {
        get
        {
            return "../../Common/TabStrip/TabStrip.aspx?TabContext=RDAdminTabs&" + Request.QueryString.ToString();
            //return "";
        }

    }
}
