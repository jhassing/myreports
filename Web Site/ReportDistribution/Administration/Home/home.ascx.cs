

namespace DistributionPortal.ReportDistribution_Administration.Home
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	//using ReportDistribution.RemotingObject;
	using MyReports.Common;

	/// <summary>
	///		Summary description for home.
	/// </summary>
	public partial class home : System.Web.UI.UserControl
	{

		private string m_sRemotingConnectionString = "";

		protected void Page_Load(object sender, System.EventArgs e)
		{

			if (Visible)
			{
				string sMachineName = AppValues.ReportServerName;
				string sMachinePort = AppValues.ReportServerPort;

				lblCurrentDomainName.Text = "";
				lblFullyQualifiedDomainName.Text = AppValues.FullyQualifiedDomain;
				lblBackEndServer.Text = sMachineName;
				lblReportServerURL.Text = sMachineName;
				lblRemotingPort.Text = sMachinePort;

				m_sRemotingConnectionString = "tcp://" + sMachineName + ":" + sMachinePort + "/RemotingService";
			
				getBurstProcessStatistics();
			}

		}

		private void getBurstProcessStatistics()
		{
			try
			{
				//RemotingService oRemoting = (RemotingService)Activator.GetObject(typeof(ReportDistribution.RemotingObject.RemotingService), m_sRemotingConnectionString);						

				string sSAPMessage = "";
			
				//if (oRemoting.IsSAPFilePickupMonitorRunning)
				//	sSAPMessage = "up and running";
				//else
				//	sSAPMessage = "stopped.";

				//if (oRemoting.IsFolderImportMonitorRunning)
				//{
					// Folder Import pickup is running

				//}
				//else
				//{

				//}

				//this.lblNumberOfActuateJobs.Text = oRemoting.RunningActuateReports().ToString();
				//this.lblNumberOfReportBurst.Text = oRemoting.RunningThreads().ToString();

				//lblSAPFilePickupStatus.Text = "SAP File Pickup Status is " + sSAPMessage;				 

			}
			catch(Exception ex)
			{
				string sMess = ex.Message;
				//lblErrors.Text = sMess;

			}
				


		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		protected void btnThrowError_Click(object sender, System.EventArgs e)
		{
			//try
			//{
				throw new Exception(" This is a test exception. Please ignore!!");
			//}
			//catch (Exception ex)
			//{

			//}
		}
	}
}
