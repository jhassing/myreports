<%@ Control Language="c#" Inherits="DistributionPortal.ReportDistribution_Administration.Home.home" CodeFile="home.ascx.cs" %>
<P>
	<asp:Label id="Label1" runat="server" Font-Bold="True" Font-Size="22pt">Welcome to the MyReports admin console</asp:Label></P>
<TABLE id="Table1" style="WIDTH: 544px; HEIGHT: 188px" cellSpacing="1" cellPadding="1"
	width="544" border="1">
	<TR>
		<TD style="WIDTH: 293px"><STRONG><FONT face="Verdana">Current Domain Name:</FONT></STRONG></TD>
		<TD align="right">
			<asp:Label id="lblCurrentDomainName" Font-Size="X-Small" runat="server" Font-Names="Verdana">Label</asp:Label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 293px"><STRONG><FONT face="Verdana">Fully Qualified Domain Name:</FONT></STRONG></TD>
		<TD align="right">
			<asp:Label id="lblFullyQualifiedDomainName" Font-Size="X-Small" runat="server" Font-Names="Verdana">Label</asp:Label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 293px"><STRONG><FONT face="Verdana">Backend Server Name:</FONT></STRONG></TD>
		<TD align="right">
			<asp:Label id="lblBackEndServer" Font-Size="X-Small" runat="server" Font-Names="Verdana">Label</asp:Label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 293px"><STRONG><FONT face="Verdana">Report Server URL:</FONT></STRONG></TD>
		<TD align="right">
			<asp:Label id="lblReportServerURL" Font-Size="X-Small" runat="server" Font-Names="Verdana">Label</asp:Label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 293px"><STRONG><FONT face="Verdana">Remoting Port:</FONT></STRONG></TD>
		<TD align="right">
			<asp:Label id="lblRemotingPort" Font-Size="X-Small" runat="server" Font-Names="Verdana">Label</asp:Label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 293px"><STRONG><FONT face="Verdana">Number of report bursts:</FONT></STRONG></TD>
		<TD align="right">
			<asp:Label id="lblNumberOfReportBurst" Font-Size="X-Small" runat="server" Font-Names="Verdana">Label</asp:Label></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 293px"><STRONG><FONT face="Verdana">Number of Actuate Jobs running:</FONT></STRONG></TD>
		<TD align="right">
			<asp:Label id="lblNumberOfActuateJobs" Font-Size="X-Small" runat="server" Font-Names="Verdana">Label</asp:Label></TD>
	</TR>
</TABLE>
<P>
	<asp:Button id="btnThrowError" runat="server" Text="Throw Error" onclick="btnThrowError_Click"></asp:Button></P>
