namespace DistributionPortal
{
    using System;
    using System.Data;
    using System.Drawing;
    using System.Web;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using System.Web.Services.Protocols;


    using System.Collections;

    //  Using sub name spaces
    using activeportal.classes;
    using activeportal.classes.api.proxy;
    using activeportal.classes.api;
    using actuate_i18n;
    using actuate_i18n.classes;
    using activeportal.classes.configuration;
    using activeportal.classes.functionality;

    using DistributionPortal;
    using Microsoft.ApplicationBlocks.ExceptionManagement;

	/// <summary>
	///		Summary description for BurstHistory.
	/// </summary>
	public partial class BurstHistory : System.Web.UI.UserControl
	{
        protected void btnSubmitTop_Click(object sender, System.EventArgs e)
        {
            ExecuteReport();
        }

        private void btnSubmitBottom_Click(object sender, System.EventArgs e)
        {
            ExecuteReport();
        }

        private void ExecuteReport()
        {
            AcUrlBuilder link = new AcUrlBuilder();

            try
            {
                // Set the base URL
                link.SetBaseUrl(Context.Request.ApplicationPath + "/newrequest/do_executereport.aspx", true);


                link.AddKeyValue("as_BegDate", "8/21/2006");
                link.AddKeyValue("as_EndDate", "9/25/2006");
                link.AddKeyValue("as_UserName", "ReportDistribution_HostedUser");
                link.AddKeyValue("as_Password", "r3p0rtdu");
                link.AddKeyValue("as_ServerName", @"CGUSCHD310\DEV");

                

                link.AddKeyValue(AcParams.Name.__executableName, "/Administration/SubsidyInvoiceListing.rox");
                link.AddKeyValue(AcParams.Name.__SourceOfRequest__, "ReportListing.aspx");

                string sJobName = "Subsidy Invoice Listing";


                // Addd the __wait parameter 
                string sWait = AcParams.Get(Page, AcParams.Name.__wait);

                if (sWait != null &&
                    sWait.Trim() != String.Empty)
                {
                    link.AddKeyValue(AcParams.Name.__wait, sWait);
                }

                link.AddKeyValue(AcParams.Name.__scheduleType, "immediate");


            }
            catch (Exception ex)
            {
                //DisplayError(ex);
            }

            //Server.Transfer(link.ToString());
            Response.Redirect(link.ToString(), false);
        }
}
}
