<%@ Control Language="c#" Inherits="DistributionPortal.BurstHistory" CodeFile="BurstHistory.ascx.cs" %>
<style type="text/css">BODY {
	BACKGROUND-COLOR: white
}
.label {
	COLOR: black
}
.filterlabel {
	COLOR: black
}
.toolbartext {
	COLOR: black
}
.toolbarboldtext {
	COLOR: black
}
.boldLabel {
	COLOR: black
}
.columnHeader {
	COLOR: black
}
.panel {
	BACKGROUND-COLOR: #efebde
}
.panel {
	BORDER-LEFT-COLOR: #b5b29c; BORDER-BOTTOM-COLOR: #b5b29c; BORDER-TOP-COLOR: #b5b29c; BORDER-RIGHT-COLOR: #b5b29c
}
TABLE.breadcrumbs {
	BACKGROUND-COLOR: white
}
.alertText {
	COLOR: red
}
.disabledhyperlink {
	COLOR: lightgrey
}
.hyperlink {
	COLOR: #0000a0
}
.treeviewhyperlink {
	COLOR: #0000a0
}
.breadcrumb {
	COLOR: #0000a0
}
.toolbarlink {
	COLOR: #0000a0
}
.titlebar {
	COLOR: white
}
.titlebar {
	BACKGROUND-COLOR: #31659c
}
A.hyperlink:hover {
	COLOR: red
}
.rowStyle1 {
	BACKGROUND-COLOR: white
}
.rowStyle2 {
	BACKGROUND-COLOR: #efebde
}
.bannerLabel {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.bannerValue {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.bannerLink {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.sidebarSelected {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.sidebarLink {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
A.sidebarLink:hover {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.sidebarLinkSelected {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
A.sidebarLinkSelected:hover {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.sidebarUnselected {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.label {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.disabledhyperlink {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.hyperlink {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
A.hyperlink:hover {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.titlebar {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.filterLabel {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.popupMenu {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.popupMenuItem {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.popupMenuItemSelected {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
TD {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.breadcrumb {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.treeviewhyperlink {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
INPUT {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
SELECT {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.toolbarlink {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.toolbartext {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.toolbarboldtext {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.boldLabel {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.columnHeader {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.alertText {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.searchresultlink {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.LinkOff {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.LinkOver {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.LinkDisable {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.searchHeader {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.searchContent {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.banner {
	BACKGROUND-COLOR: #333399
}
.bannerLink {
	COLOR: #ffcc66
}
A.bannerLink:hover {
	COLOR: #ffcf00
}
.treeviewbanner {
	BACKGROUND-COLOR: #89a7ff
}
.bannerLabel {
	COLOR: #9ccfff
}
.bannerValue {
	COLOR: white
}
.footer {
	BACKGROUND-COLOR: #333399
}
.sidebar {
	BACKGROUND-COLOR: white
}
.newCellActiveTab {
	BACKGROUND-COLOR: #efebde
}
.newCellInactiveTab {
	BACKGROUND-COLOR: #b5b29c
}
A.tabLink:link {
	COLOR: white
}
A.tabLink:hover {
	COLOR: #0000a0
}
.tabLinkSelected {
	COLOR: #0000a0
}
A.tabLinkSelected:hover {
	COLOR: #0000a0
}
.popupMenu {
	BACKGROUND-COLOR: #f0f3f9
}
.popupMenuItem {
	COLOR: #333399
}
.popupMenuItemSelected {
	BACKGROUND-COLOR: #333399
}
.popupMenuItemSelected {
	COLOR: #f0f3f9
}
.popupMenuSeparator {
	BACKGROUND-COLOR: #333399
}
.popupMenuSelection {
	BACKGROUND-COLOR: #ffffcc
}
.navbar {
	BACKGROUND-COLOR: #31699c
}
.LinkOff {
	COLOR: white
}
.LinkOver {
	COLOR: red
}
.LinkDisable {
	COLOR: #99cccc
}
.title {
	COLOR: #ffffff
}
.title {
	BACKGROUND-COLOR: #31699c
}
.searchresultlink {
	COLOR: blue
}
.searchPanel {
	BACKGROUND-COLOR: #006699
}
.searchHeader {
	COLOR: white
}
.searchContent {
	COLOR: white
}
.parameterInputBox {
	Z-INDEX: 2; POSITION: relative; TOP: -1px
}
.parameterDropdown {
	CLIP: rect(auto auto auto 211pt); POSITION: absolute
}
</style>
<br>
<asp:Label id="Label1" runat="server" Font-Bold="True" Font-Size="22pt">Subsidy Invoice Burst History</asp:Label>
<br />
<br />
<div style="text-align: left">
    
    <table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr vAlign="top">
		<td>&nbsp;
			<table style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-COLLAPSE: collapse; BORDER-RIGHT-WIDTH: 0px"
				cellSpacing="0" cellPadding="0" border="0">
				<tr style="CURSOR: default">
					<td class="newCellActiveTab" noWrap><label class="tabLinkSelected">Parameters</label></td>
				</tr>
			</table>
		</td>
		<td align="right"><asp:Button CssClass="MyReportsAdminConsoleButton" id="btnSubmitTop" runat="server" Text="Submit" onclick="btnSubmitTop_Click"/>&nbsp;
		</td>
	</tr>
	<tr>
		<td colSpan="2">
			<table class="panel" width="100%">
			    <tr>
	                <td style="width: 476px">
                        <asp:Label Visible=False ID="lblMessage" ForeColor="Black" runat="server" CssClass="XX-Small" Text="[Message]" Width="141%" Font-Bold="True" Font-Size="XX-Small"></asp:Label>										            
                    </td>
                </tr>
				<tr vAlign="top">
					<td>
                        <table id="MainTable" cellSpacing="10" cellPadding="0" border="0" width="100%">
						    <tr>
							    <td vAlign="top" colSpan="4" style="width: 344px">
								    <table id="ParamTable" cellSpacing="3" cellPadding="3" border="0">
									    <tr>
            <td style="width: 100px">
                <asp:Label ID="lblStartDate" runat="server" BackColor="Transparent" BorderColor="Transparent"
                    CssClass="X-Small">&nbsp;Start Date</asp:Label></td>
            <td style="width: 100px">
                <asp:Label ID="lblEndDate" runat="server" BackColor="Transparent" BorderColor="Transparent"
                    CssClass="X-Small">&nbsp;End Date</asp:Label></td>
									    </tr>
								    </table>
                                    
							    </td>
						    </tr>
					    </table>    				
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="right"><br /><asp:Button CssClass="MyReportsAdminConsoleButton" id="Button1" runat="server" Text="Submit" onclick="btnSubmitTop_Click"/>&nbsp;
		</td>
	</tr>
</table>
    
    
</div>
