namespace DistributionPortal
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using MyReports.Common;
	using ReportDistribution.Common;
	using ReportDistribution.SQLServerDAL;

	/// <summary>
	///		Summary description for SingleValidReport.
	/// </summary>
	public partial class SingleValidReportControl : ClientSideControl
	{

		#region Properties

		public bool AddNewReportCode
		{
			get
			{
				if (ViewState["AddNewReportCode"] != null)
					return System.Convert.ToBoolean( ViewState["AddNewReportCode"] );
				else
					return false;
			}
			set
			{
				ViewState["AddNewReportCode"] = value;
			}
		}


		public string ReportCode
		{
			get
			{
				return this.txtReportCode.Text;
			}
			set
			{
				this.txtReportCode.Text = value;
			}
		}


		public int ArchiveLength
		{
			get
			{
				return this.drpArchiveLength.SelectedIndex;
			}
			set
			{
				this.drpArchiveLength.SelectedIndex = value;
			}
		}

		
		public int ReportType
		{
			get
			{
				return this.drpReportTypeID.SelectedIndex;
			}
			set
			{
				this.drpReportTypeID.SelectedIndex = value;
			}
		}


		public int OverwriteValue
		{
			get
			{
				return System.Convert.ToInt32( this.drpOverwrite.SelectedItem.Value );
			}
			set
			{
				this.drpOverwrite.SelectedIndex = value -1;
			}
		}


		public int ArchiveValue
		{
			get
			{
				return System.Convert.ToInt32( this.drpArchiveValue.SelectedItem.Value );
			}
			set
			{
				this.drpArchiveValue.SelectedIndex = value - 1;
			}
		}

		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PreRender += new System.EventHandler(this.SingleValidReportControl_PreRender);

		}
		#endregion

		protected void SingleValidReportControl_PreRender(object sender, EventArgs e)
		{
			if (Visible)
			{
				if (!this.AddNewReportCode)
				{
					btnDelete.Attributes.Add("OnClick", "javascript:Delete_Click();");
					btnDelete.Visible = true;	
				
					#region Write Delete button client script

					if ( !Page.IsClientScriptBlockRegistered("DeleteButtonClickScript") )
					{
						string hh = "\r\n";
			
						hh += base.getOpenScriptTag();

						hh += "\t function Delete_Click() \r\n";

						hh += "\t{\r\n";

						hh += QuestionDialogScript(	"Are you sure you want to delete report \" + document.forms[0]." + this.txtReportCode.ClientID + ".value + \" from the ACL?",
							"If the report is removed from the ACL it will no longer be a valid MyReports report",
							"Delete",
							"Cancel");
			
						hh += "\r\n \r\n";
						hh += "\t }\r\n ";
				
						hh += base.getCloseScriptTag();

						Page.RegisterClientScriptBlock("DeleteButtonClickScript", hh);

					}

					#endregion
				}
				else
					btnDelete.Visible = false;

				btnCancel.Attributes.Add("OnClick", "javascript:Cancel();");
				Button2.Attributes.Add("OnClick", "javascript:Save();");
			

				string s1 = base.getOpenScriptTag();
				s1 += "\t document.forms[0]." + this.txtReportCode.ClientID + ".readOnly=" + (!this.AddNewReportCode).ToString().ToLower() + ";";
				s1 += base.getCloseScriptTag();

				if ( !Page.IsClientScriptBlockRegistered("EnableDisableReportCode") )
					Page.RegisterClientScriptBlock("EnableDisableReportCode", s1);				

				WriteCancelScript();
				WriteSaveScript();

				Page.RegisterHiddenField("retValueFromQuestion", "");

				MonitorChanges(txtReportCode);
				MonitorChanges(drpReportTypeID);
				MonitorChanges(drpArchiveLength);
				MonitorChanges(drpArchiveValue);
				MonitorChanges(drpOverwrite);	

			}
		}

		protected override void WriteCancelScript()
		{
			if (!Page.IsClientScriptBlockRegistered("Cancel-Script Function") )
			{
				string hh = getOpenScriptTag();
			
				hh += "\t function Cancel() { \r\n";
				hh += "\t\t if (isDirty) { \r\n";

				hh += this.QuestionDialogScript("Are you sure you want to cancel?", 
					" If you select yes then all changes that you have made to the report archive values for \" + document.forms[0]." + this.txtReportCode.ClientID + ".value + \" will be lost", 
					"Save", 
					"Cancel");
		
				hh += "\r\n";
					
				hh += "\t\t } \r\n";

				hh += "\t\t window.returnValue = null; \r\n";
				
				hh += "\t\t } \r\n";

				hh += getCloseScriptTag();
				
				Page.RegisterClientScriptBlock("Cancel-Script Function", hh);
				
			}
		}


		protected void btnDelete_Click(object sender, System.EventArgs e)
		{
			if (base.WasButtonOneClicked)
				new Reports(AppValues.MyReportsDB).DeleteReportFromAcl(txtReportCode.Text.Trim());					
		}

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
		
		}

		protected void Button2_Click(object sender, System.EventArgs e)
		{
			SaveReportCodeData();
		}

		private void SaveReportCodeData()
		{

			try
			{
				// Get the values from the form
				string	sReportCode			=	this.ReportCode;
				string	UpdateInsert		=	(this.AddNewReportCode == true ? "I" : "U" );
				int		nArchiveLength		=	this.ArchiveLength;
				int		nReportTypeID		=	this.ReportType;
				int		nArchiveValue		=	this.ArchiveValue;
				int		nOverwriteValue		=	this.OverwriteValue;

			
				// Pass values back into the BLL for validation
				new Reports(AppValues.MyReportsDB).InsertOrUpdateReportInAcl(UpdateInsert, sReportCode, nArchiveLength, nReportTypeID, nArchiveValue, nOverwriteValue);			
			}
			catch( Exception ex )
			{
				throw ex;
			}
		}
	}
}
