using MyReports.Common;

namespace DistributionPortal
{
	using System;
	using System.Data;
	using System.Web.UI.WebControls;
	using ReportDistribution.Common;
	using ReportDistribution.SQLServerDAL;

	/// <summary>
	///		Summary description for ValidReports.
	/// </summary>
	public partial class ValidReports : System.Web.UI.UserControl
	{
		protected System.Web.UI.WebControls.RadioButton RadioButton1;
		protected System.Web.UI.WebControls.RadioButton Radiobutton2;
		protected System.Web.UI.WebControls.RadioButton Radiobutton3;
		protected System.Web.UI.WebControls.RadioButton Radiobutton4;
		protected System.Web.UI.WebControls.RadioButton Radiobutton5;
		protected System.Web.UI.WebControls.RadioButton Radiobutton6;
		protected System.Web.UI.WebControls.RadioButton Radiobutton7;
		protected System.Web.UI.WebControls.RadioButton Radiobutton8;
		protected System.Web.UI.WebControls.RadioButton Radiobutton9;
		protected System.Web.UI.WebControls.DropDownList DropDownList1;
		protected System.Web.UI.WebControls.LinkButton LinkButton1;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.TextBox ArchiveLength;

		private bool _clickedSingleReport;

		protected void ValidReports_PreRender(object sender, EventArgs e)
		{
			if (Visible)
			{
	
				if (!Page.IsPostBack)
				{
					
					// Populate the grid
					LoadData();
				}
				else
				{
					if (_clickedSingleReport)
					{
						SingleValidReportControl1.Visible = true;
						this.panValidReportsGrid.Visible = false;							
					}
					else
					{
						// Populate the grid
						LoadData();

					}

				}				
			}
		}


		protected void Repeater1_OnItemDataBound( Object sender, RepeaterItemEventArgs e )
		{
			if (	e.Item.ItemType == ListItemType.AlternatingItem || 
					e.Item.ItemType == ListItemType.Item ||
					e.Item.ItemType == ListItemType.SelectedItem )
			{
				// Find all the controls needed
				
				LinkButton lnkReportCode		= ( LinkButton ) e.Item.FindControl( "lnkReportCode" );
				
				Label lblArchiveLength			= ( Label ) e.Item.FindControl( "lblArchiveLength" );
				Label lblReportType				= ( Label ) e.Item.FindControl( "lblReportType" );
				//Label lblOverwriteValue			= ( Label ) e.Item.FindControl( "lblOverwriteValue" );
				Label lblOverwriteDescription	= ( Label ) e.Item.FindControl( "lblOverwriteDescription" );
				Label lblArchiveValue			= ( Label ) e.Item.FindControl( "lblArchiveValue" );
				
				System.Data.DataRowView drv = ( DataRowView )e.Item.DataItem;

				lnkReportCode.Text				= drv.Row.ItemArray[0].ToString();
				lblArchiveLength.Text			= drv.Row.ItemArray[1].ToString();
				lblReportType.Text				= drv.Row.ItemArray[2].ToString();
				//lblOverwriteValue.Text			= drv.Row.ItemArray[3].ToString();
				lblOverwriteDescription.Text	= drv.Row.ItemArray[3].ToString();
				lblArchiveValue.Text			= drv.Row.ItemArray[4].ToString();

				lnkReportCode.Command+=new CommandEventHandler(LinkButton2_Command);
				lnkReportCode.CommandName="ReportCode";

				lnkReportCode.CommandArgument =		lnkReportCode.Text.Trim() + "|" + 
													drv.Row.ItemArray[1] + "|" +
													drv.Row.ItemArray[6] + "|" + 
													drv.Row.ItemArray[5] + "|" + 
													drv.Row.ItemArray[7];
				
			}
		}
		
			
		private void LoadData()
		{
			SingleValidReportControl1.Visible = false;
			this.panValidReportsGrid.Visible = true;

		    Repeater1.DataSource = ViewState["ReportType"] == null
		                               ? new Reports(AppValues.MyReportsDB).GetValidReports()
		                               : new Reports(AppValues.MyReportsDB).GetValidReports((int) ViewState["ReportType"]);

			Repeater1.DataBind();
			_clickedSingleReport = false;
		}


		protected void LinkButton2_Command(object sender, System.Web.UI.WebControls.CommandEventArgs e)
		{
			string[] args = e.CommandArgument.ToString().Split('|');

			SingleValidReportControl1.ReportCode		= args[0];
			
			SingleValidReportControl1.ArchiveLength		= System.Convert.ToInt32(args[1]);
			SingleValidReportControl1.ReportType		= System.Convert.ToInt32(args[2]);
			SingleValidReportControl1.OverwriteValue	= System.Convert.ToInt32(args[3]);
			SingleValidReportControl1.ArchiveValue		= System.Convert.ToInt32(args[4]);

			SingleValidReportControl1.AddNewReportCode	= false;

			_clickedSingleReport = true;
		}		

		
		protected void rdoFinancial_CheckedChanged(object sender, System.EventArgs e)
		{
			ViewState["ReportType"] = (int)Enum_ReportTypes.Financial;
		}

		
		protected void rdoPayroll_CheckedChanged(object sender, System.EventArgs e)
		{
			ViewState["ReportType"] = (int)Enum_ReportTypes.Payroll;
		
		}

		
		protected void rdoBoth_CheckedChanged(object sender, System.EventArgs e)
		{
			ViewState["ReportType"] = null;
		}

		
		protected void rdoProfile_CheckedChanged(object sender, System.EventArgs e)
		{
			ViewState["ReportType"] = (int)Enum_ReportTypes.Profiles;		
		}

		
		protected void lnkAddNew_Click(object sender, System.EventArgs e)
		{
			SingleValidReportControl1.ReportCode = "";
			SingleValidReportControl1.ArchiveLength = 0;
			SingleValidReportControl1.ReportType = 0;
			SingleValidReportControl1.OverwriteValue = 0;
			SingleValidReportControl1.ArchiveValue = 0;

			SingleValidReportControl1.AddNewReportCode = true;
			_clickedSingleReport = true;			
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PreRender += new System.EventHandler(this.ValidReports_PreRender);

		}
		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
		
		}
	}
}
