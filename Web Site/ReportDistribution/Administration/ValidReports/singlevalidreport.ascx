<%@ Control Language="c#" Inherits="DistributionPortal.SingleValidReportControl" CodeFile="SingleValidReport.ascx.cs" %>
<TABLE class="MyReportsAdminConsoleBorder" height="100%" cellSpacing="0" cellPadding="0"
	width="100%" border="0">
	<tr>
		<td colspan="3">
			<LABEL id="MainTest" style="FONT-WEIGHT: bold; FONT-SIZE: 14pt; FONT-FAMILY: Verdana" runat="server">
				Report archive values.</LABEL> <INPUT type="hidden" name="UpdateInsert">
			<BR>
			<br>
		</td>
	</tr>
	<TR>
		<TD style="WIDTH: 128px; HEIGHT: 14px"><asp:label id="Label1" runat="server" BackColor="Transparent" BorderColor="Transparent" CssClass="X-Small">&nbsp;Report Code</asp:label></TD>
		<TD style="WIDTH: 168px; HEIGHT: 14px"><asp:textbox id="txtReportCode" CssClass="X-Small" Runat="server" Width="152px" MaxLength="10"></asp:textbox></TD>
		<TD><LABEL class="X-Small">Name of the SAP report that is being distributed to 
				MyReports.</LABEL></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 128px; HEIGHT: 17px"><asp:label id="Label3" runat="server" BackColor="Transparent" BorderColor="Transparent" CssClass="X-Small">&nbsp;Report Type</asp:label></TD>
		<TD style="WIDTH: 168px; HEIGHT: 17px"><asp:dropdownlist id="drpReportTypeID" runat="server" CssClass="X-Small" Width="152px">
				<asp:ListItem Value="0">Financial</asp:ListItem>
				<asp:ListItem Value="1">Payroll</asp:ListItem>
				<asp:ListItem Value="2">Profiles</asp:ListItem>
				<asp:ListItem Value="3">Off Cycle</asp:ListItem>
				<asp:ListItem Value="Subsid4y Invoice">Subsidy Invoice</asp:ListItem>
			</asp:dropdownlist></TD>
		<TD><LABEL class="X-Small">Payroll or Financial report.</LABEL></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 128px; HEIGHT: 10px"><asp:label id="Label4" runat="server" BackColor="Transparent" BorderColor="Transparent" CssClass="X-Small">&nbsp;Archive Value</asp:label></TD>
		<TD style="WIDTH: 168px; HEIGHT: 10px"><asp:dropdownlist id="drpArchiveValue" runat="server" CssClass="X-Small" Width="152px">
				<asp:ListItem Value="1">Monthly</asp:ListItem>
				<asp:ListItem Value="2">Weekly</asp:ListItem>
				<asp:ListItem Value="3">Daily</asp:ListItem>
			</asp:dropdownlist></TD>
		<TD style="HEIGHT: 10px"><LABEL class="X-Small">This will be used along with the 
				Archive length in order to determine how long the report will be retained by 
				the system.</LABEL></TD>
	</TR>
	<TR>
		<TD valign="top" style="HEIGHT: 26px"><asp:label id="Label2" runat="server" BackColor="Transparent" BorderColor="Transparent" CssClass="X-Small">&nbsp;Archive Length</asp:label></TD>
		<TD valign="top" style="HEIGHT: 26px"><asp:dropdownlist id="drpArchiveLength" runat="server" CssClass="X-Small" Width="152px">
				<asp:ListItem Value="0">0</asp:ListItem>
				<asp:ListItem Value="1">1</asp:ListItem>
				<asp:ListItem Value="2">2</asp:ListItem>
				<asp:ListItem Value="3">3</asp:ListItem>
				<asp:ListItem Value="4">4</asp:ListItem>
				<asp:ListItem Value="5">5</asp:ListItem>
				<asp:ListItem Value="6">6</asp:ListItem>
				<asp:ListItem Value="7">7</asp:ListItem>
				<asp:ListItem Value="8">8</asp:ListItem>
				<asp:ListItem Value="9">9</asp:ListItem>
				<asp:ListItem Value="10">10</asp:ListItem>
				<asp:ListItem Value="11">11</asp:ListItem>
				<asp:ListItem Value="12">12</asp:ListItem>
				<asp:ListItem Value="13">13</asp:ListItem>
				<asp:ListItem Value="14">14</asp:ListItem>
				<asp:ListItem Value="15">15</asp:ListItem>
				<asp:ListItem Value="16">16</asp:ListItem>
				<asp:ListItem Value="17">17</asp:ListItem>
				<asp:ListItem Value="18">18</asp:ListItem>
				<asp:ListItem Value="19">19</asp:ListItem>
			</asp:dropdownlist></TD>
		<TD valign="middle" style="HEIGHT: 26px"><LABEL class="X-Small">How long the report 
				will be retained by the system.</LABEL></TD>
	</TR>
	<TR>
		<TD valign="top"><asp:label id="Label5" runat="server" BackColor="Transparent" BorderColor="Transparent" CssClass="X-Small">&nbsp;Overwrite Value</asp:label></TD>
		<TD valign="top"><asp:dropdownlist id="drpOverwrite" runat="server" CssClass="X-Small" Width="152px">
				<asp:ListItem Value="1">Archive All</asp:ListItem>
				<asp:ListItem Value="2">Archive Month</asp:ListItem>
				<asp:ListItem Value="3">Force Overwrite</asp:ListItem>
			</asp:dropdownlist></TD>
		<TD><LABEL class="X-Small">Determines if a new report that is received will be 
				overwritten or not based on the Archive Value and Archive length.
				<BR>
				(<B>Archive All</B> = Always archive each report.)<BR>
				(<B>Archive Month</B> = Overwrite all reports that are sent in the same month. 
				Archive if month changes.)<BR>
				(<B>Force</B> = Always overwrite (Never archive) ). </LABEL>
		</TD>
	</TR>
	<TR>
		<TD style="WIDTH: 128px">&nbsp;</TD>
		<TD style="WIDTH: 168px">&nbsp;</TD>
		<TD><LABEL class="X-Small">&nbsp;</LABEL></TD>
	</TR>
	<TR>
		<TD style="WIDTH: 128px"></TD>
		<TD style="WIDTH: 168px" align="left"><asp:button id="btnDelete" runat="server" CssClass="MyReportsAdminConsoleButton" Width="110px"
				Text="Delete from ACL" onclick="btnDelete_Click"></asp:button>&nbsp;&nbsp;</TD>
		<TD align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<asp:button id="btnCancel" runat="server" CssClass="MyReportsAdminConsoleButton" Width="110px"
				Text="Cancel" onclick="btnCancel_Click"></asp:button>&nbsp;&nbsp;
			<asp:button id="Button2" runat="server" CssClass="MyReportsAdminConsoleButton" Width="110px"
				Text="Save Changes" onclick="Button2_Click"></asp:button>&nbsp;&nbsp;
		</TD>
	</TR>
	<tr height="100%">
		<td height="100%">&nbsp;</td>
	</tr>
</TABLE>
