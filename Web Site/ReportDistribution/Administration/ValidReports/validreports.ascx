<%@ Register TagPrefix="uc1" TagName="SingleValidReport" Src="SingleValidReport.ascx" %>
<%@ Control Language="c#" Inherits="DistributionPortal.ValidReports" CodeFile="ValidReports.ascx.cs" %>
<asp:Panel ID="panValidReportsGrid" Runat="server" Visible="True">
	<TABLE class="MyReportsAdminConsoleBorder" height="100%" cellSpacing="0" cellPadding="0"
		width="100%" border="0">
		<TR style="PADDING-BOTTOM: 6px; PADDING-TOP: 6px">
			<TD style="HEIGHT: 22px" noWrap colSpan="4">&nbsp;&nbsp;
				<asp:linkbutton id="lnkAddNew" CssClass="ReportCodeStyle" runat="server" onclick="lnkAddNew_Click">Add New</asp:linkbutton></TD>
			<TD style="HEIGHT: 22px" noWrap colSpan="2">
				<asp:radiobutton id="rdoFinancial" runat="server" GroupName="ReportType" Text="Financial" AutoPostBack="true" oncheckedchanged="rdoFinancial_CheckedChanged"></asp:radiobutton>&nbsp;
				<asp:radiobutton id="rdoPayroll" runat="server" GroupName="ReportType" Text="Payroll" AutoPostBack="true" oncheckedchanged="rdoPayroll_CheckedChanged"></asp:radiobutton>&nbsp;
				<asp:radiobutton id="rdoProfile" runat="server" GroupName="ReportType" Text="Profile" AutoPostBack="true" oncheckedchanged="rdoProfile_CheckedChanged"></asp:radiobutton>&nbsp;
				<asp:radiobutton id="rdoBoth" runat="server" GroupName="ReportType" Text="Both" AutoPostBack="true"
					Checked="True" oncheckedchanged="rdoBoth_CheckedChanged"></asp:radiobutton>&nbsp;
			</TD>
		</TR>
		<TR>
			<TD class="TableHeaderRow" colSpan="1" height="1"><SPAN class="XX-SmallBold_White">Report 
					Code</SPAN>
			</TD>
			<TD class="TableHeaderRow" colSpan="1" height="1"><SPAN class="XX-SmallBold_White">Time</SPAN>
			</TD>
			<TD class="TableHeaderRow" colSpan="1" height="1"><SPAN class="XX-SmallBold_White">Report 
					Type</SPAN>
			</TD>
			<TD class="TableHeaderRow" colSpan="" height="1"><SPAN class="XX-SmallBold_White">Overwrite</SPAN>
			</TD>
			<TD class="TableHeaderRow" colSpan="1" height="1"><SPAN class="XX-SmallBold_White">A/R 
					Length</SPAN>
			</TD>
			</TD></TR>
		<TR width="100%">
			<TD width="100%" colSpan="6"><IMG height="4" src="../images/vspace.gif" width="100%" border="0"></TD>
		</TR>
		<asp:repeater id="Repeater1" runat="server" OnItemDataBound="Repeater1_OnItemDataBound">
			<AlternatingItemTemplate>
				<tr runat="server" class="AltTableRow" height="1">
					<td nowrap>
						<asp:LinkButton id="lnkReportCode" CssClass="hyperlink" OnCommand="LinkButton2_Command" runat="server"></asp:LinkButton>
					</td>
					<td nowrap>
						<asp:Label class="XX-Small" ID="lblArchiveLength" Runat="server"></asp:Label>
					</td>
					<td nowrap>
						<asp:Label class="XX-Small" ID="lblReportType" Runat="server"></asp:Label>
					</td>
					<td nowrap>
						<asp:Label class="XX-Small" ID="lblOverwriteDescription" Runat="server"></asp:Label>
					</td>
					<td nowrap>
						<asp:Label class="XX-Small" ID="lblArchiveValue" Runat="server"></asp:Label>
					</td>
				</tr>
			</AlternatingItemTemplate>
			<ItemTemplate>
				<tr runat="server" class="TableRow" height="1">
					<td nowrap>
						<asp:LinkButton id="lnkReportCode" CssClass="hyperlink" OnCommand="LinkButton2_Command" runat="server"></asp:LinkButton>
					</td>
					<td nowrap>
						<asp:Label class="XX-Small" ID="lblArchiveLength" Runat="server"></asp:Label>
					</td>
					<td nowrap>
						<asp:Label class="XX-Small" ID="lblReportType" Runat="server"></asp:Label>
					</td>
					<td nowrap>
						<asp:Label class="XX-Small" ID="lblOverwriteDescription" Runat="server"></asp:Label>
					</td>
					<td nowrap>
						<asp:Label class="XX-Small" ID="lblArchiveValue" Runat="server"></asp:Label>
					</td>
				</tr>
			</ItemTemplate>
		</asp:repeater></TABLE>
</asp:Panel>
<uc1:SingleValidReport id="SingleValidReportControl1" runat="server"></uc1:SingleValidReport>
