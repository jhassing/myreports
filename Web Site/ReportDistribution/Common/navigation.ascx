<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Navigation.ascx.cs" Inherits="MySelectedFiles_Common_Navigation" %>
<table class="breadcrumbs" cellSpacing="0" cellPadding="3" width="100%" border="0">
	<tr>
		<td align="left" width="100%">
			<!-- User Management -->
			<asp:Label id="lblUserManagement" CssClass="breadcrumb" runat="server">MySelected Files > </asp:Label>
			<!-- Home -->
			<asp:LinkButton id="HomeLink" CommandName="showstartscreen" Visible="True" CssClass="breadcrumb" runat="server" CausesValidation="False" OnClick="HomeLink_Click">Home</asp:LinkButton>
			<asp:Label id="HomeLink_Sep" Visible="True" CssClass="breadcrumb" runat="server"> > </asp:Label>			
			<!-- Download -->
			<asp:LinkButton id="DownloadLink" CommandName="ShowDownloadFiles" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False" OnClick="DownloadLink_Click" >Download</asp:LinkButton>
			<asp:Label id="DownloadLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>			
			<!-- Email -->
			<asp:LinkButton id="EmailLink" CommandName="ShowEmailScreen" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False" OnClick="EmailLink_Click" >Email</asp:LinkButton>
			<asp:Label id="EmailLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
		    <!-- Email Search -->
			<asp:LinkButton id="ClientEmailSearchLink" CommandName="" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False" OnClick="ClientEmailSearchLink_Click">Client Email Search</asp:LinkButton>
			<asp:Label id="ClientEmailSearchLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
		    <!-- Edit Client Email -->
			<asp:LinkButton id="EditClientEmailLink" CommandName="" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False" OnClick="ClientEmailSearchLink_Click">Edit Client Email</asp:LinkButton>
			<asp:Label id="EditClientEmailLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>			
		    <!-- Add New Client Email Address-->
			<asp:LinkButton id="AddNewClientEmailLink" CommandName="addnewclientemailaddress" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False" OnClick="AddNewClientEmailLink_Click">Add New</asp:LinkButton>
			<asp:Label id="AddNewClientEmailLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>			
		    <!-- Search From Email Address-->
			<asp:LinkButton id="SearchFromEmailLink" CommandName="" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False" >Search From Email</asp:LinkButton>
			<asp:Label id="SearchFromEmailLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>						
		    <!-- Edit From Email -->
			<asp:LinkButton id="EditFromEmailLink" CommandName="" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False" OnClick="ClientEmailSearchLink_Click">Edit From Email</asp:LinkButton>
			<asp:Label id="EditFromEmailLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>						
		    <!-- Add New From Email -->
			<asp:LinkButton id="AddNewFromEmail" CommandName="addnewclientemailfromaddress" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False" OnClick="AddNewClientEmailLink_Click">Add New</asp:LinkButton>
			<asp:Label id="AddNewFromEmail_sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
			<!-- Email Message Search -->
			<asp:LinkButton ID="lnbEmailMessageSearch" CommandName="editexistingemailmessage" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False" >Email Message Search</asp:LinkButton>
			<asp:Label id="lblEmailMessageSearch_sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
			<!-- Email Message -->
            <asp:LinkButton ID="lnbEditEmailMessage" CommandName="" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False">Edit Email Message</asp:LinkButton>
            <asp:Label ID=lblEditEmailMessage_sep Visible="false" CssClass="breadcrumb" runat="server"> > </asp:Label>	
            <!-- Attach Report -->
			<asp:LinkButton id="lnbAttachReport" CommandName="attachreport" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False" OnClick="AttachReport_Click">Attach Report</asp:LinkButton>
			<asp:Label id="lblAttachReport_sep" Visible="false" CssClass="breadcrumb" runat="server"> > </asp:Label>
		</td>
	</tr>
</table>

