using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

public partial class MySelectedFiles_Common_Navigation : System.Web.UI.UserControl
{

    #region Properties

    public bool Display_DownloadLink
    {
        set
        {
            this.DownloadLink.Visible = value;
            this.DownloadLink_Sep.Visible = value;
        }
    }

    public bool Display_EmailLink
    {
        set
        {
            this.EmailLink.Visible = value;
            this.EmailLink_Sep.Visible = value;
        }
    }

    public bool Display_ClientEmailSearchLink
    {
        set
        {
            this.ClientEmailSearchLink.Visible = value;
            this.ClientEmailSearchLink_Sep.Visible = value;
        }
    }

    public bool Display_ClientEmailSearchLink_FromHome
    {
        set
        {
            this.ClientEmailSearchLink.Visible = value;
            this.ClientEmailSearchLink_Sep.Visible = value;
        }
    }

    public bool Display_AddNewClientEmailLink_FromSearch
    {
        set
        {
            this.AddNewClientEmailLink.Visible = value;
            this.AddNewClientEmailLink_Sep.Visible = value;
        }
    }

    public bool Display_AddNewClientEmailLink_FromHome
    {
        set
        {
            this.AddNewClientEmailLink.Visible = value;
            this.AddNewClientEmailLink_Sep.Visible = value;
        }
    }

    public bool Display_SearchFromEmailLink
    {
        set
        {
            this.SearchFromEmailLink.Visible = value;
            this.SearchFromEmailLink_Sep.Visible = value;
        }
    }

    public bool Display_EditClientEmailLink
    {
        set
        {
            this.EditClientEmailLink.Visible = value;
            this.EditClientEmailLink.Visible = value;
        }
    }

    public bool Display_EditFromEmailLink
    {
        set
        {
            this.EditFromEmailLink.Visible = value;
            this.EditFromEmailLink_Sep.Visible = value;
        }
    }

    public bool Display_AddNewFromEmail
    {
        set
        {
            this.AddNewFromEmail.Visible = value;
            this.AddNewFromEmail_sep.Visible = value;
        }
    }

    public bool Display_EmailMessageSearch
    {
        set
        {
            this.lnbEmailMessageSearch.Visible = value;
            this.lblEmailMessageSearch_sep.Visible = value;
        }
    }

    public bool Display_EditEmailMessage
    {
        set
        {
            this.lnbEditEmailMessage.Visible = value;
            this.lblEditEmailMessage_sep.Visible = value;
        }
    }

    public bool Display_AttachReports
    {
        set
        {
            this.lnbAttachReport.Visible = value;
            this.lblAttachReport_sep.Visible = value;
        }
    }

    
    public string EmailSearchCommandName
    {
        set
        {
            this.ClientEmailSearchLink.CommandName = value;
        }
    }    
    #endregion

    #region Hide Items
    public void HideItems()
    {
        this.Display_DownloadLink = false;
        this.Display_EmailLink = false;
        this.Display_ClientEmailSearchLink = false;
        this.Display_AddNewClientEmailLink_FromHome = false;
        this.Display_AddNewClientEmailLink_FromSearch = false;
        this.Display_SearchFromEmailLink = false;
        this.Display_EditClientEmailLink = false;
        this.Display_EditFromEmailLink = false;
        this.Display_AddNewFromEmail = false;
        this.Display_EmailMessageSearch = false;
        this.Display_EditEmailMessage = false;
        this.Display_AttachReports = false;
    }
    #endregion

    protected void DownloadLink_Click(object sender, EventArgs e)
    {
        this.Display_DownloadLink = true;

        RaiseBubbleEvent(sender, e);

    }
    protected void EmailLink_Click(object sender, EventArgs e)
    {
        this.Display_EmailLink = true;

        RaiseBubbleEvent(sender, e);
    }
    protected void ClientEmailSearchLink_Click(object sender, EventArgs e)
    {
        this.Display_ClientEmailSearchLink = true;

        RaiseBubbleEvent(sender, e);
    }
    protected void HomeLink_Click(object sender, EventArgs e)
    {
        HideItems();
        RaiseBubbleEvent(sender, e);
    }
    protected void AddNewClientEmailLink_Click(object sender, EventArgs e)
    {
        //Display_AddNewClientEmailLink = true;
        RaiseBubbleEvent(sender, e);
    }
    protected void AttachReport_Click(object sender, EventArgs e)
    {
        RaiseBubbleEvent(sender, e);
    }
}