/*
	@author 	John Schiavarelli
				Copyright (C) 2004 Compass-USA. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace DistributionPortal
{

	using System;
	using System.Collections;
	using System.ComponentModel;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.SessionState;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Collections.Specialized;
	using System.Reflection;
	using System.Text;
	using System.Configuration;

	//  Using sub name spaces
	using activeportal.classes;
	using activeportal.classes.api.proxy;
	using activeportal.classes.api;
	using actuate_i18n;
	using actuate_i18n.classes;

	using DistributionPortal.Components;

	using Compass.Reporting.Actuate;
	using MyReports.Common;

	using activeportal.usercontrols.common;

	using activeportal.usercontrols.filesfolders;
	/// <summary>
	///		Summary description for WebUserControl1.
	/// </summary>
	/// 
	public partial class RDList : System.Web.UI.Page
	{	
		protected System.Web.UI.WebControls.Panel panFiles;
					
		protected void Page_Load(object sender, System.EventArgs e)		
		{					
			if (Visible)
			{

				switch(QueryStringValues.App)
				{
					case enumApp.rd:
						//navigation1.Visible = true;
						Categoriesview1.FolderTarget = "../ReportDistribution/RDList.aspx?app=rd&subpage=_list";
						Categoriesview1.DetailTarget = "../index/index.aspx?app=rd&subpage=_detail";
						break;

					case enumApp.bbd:
						//navigation1.Visible = false;
						Categoriesview1.FolderTarget = "../ReportDistribution/RDList.aspx?app=bbd&subpage=_list";
						Categoriesview1.DetailTarget = "../index/index.aspx?app=bbd&subpage=_detail";
						break;
				}

                Categoriesview1.ShowFilter = false;
               // Categoriesview1.FileSearchMode = true;
				Categoriesview1.ViewingTarget = "../viewer/viewFrameset.aspx";		
				Categoriesview1.ContentTarget = "../viewer/view3partyreport.aspx";
				Categoriesview1.DownloadTarget = "../viewer/view3partyreport.aspx";		
				Categoriesview1.ExecuteTarget = "../ReportDistribution/Benefits/BenefitReports.aspx";
				Categoriesview1.SubmitJobTarget = "../viewer/view3partyreport.aspx";
			}


		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();

			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

	}
}
