<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserManagementDefault.aspx.cs" Inherits="UserManagement_UserManagementDefault" %>


<%@ Register TagPrefix="uc1" TagName="Navigation" Src="Common/Navigation.ascx" %>
<%@ Register Src="Home/home.ascx" TagName="home" TagPrefix="uc2" %>
<%@ Register Src="ObjectListing/objectlisting.ascx" TagName="objectlisting" TagPrefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
		<LINK href="../css/allstyles.css" type="text/css" rel="stylesheet">
		<script language=javascript src="../js/General.js"></script>
	</head>
	<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" scroll="auto">
		<form id="Form1" method="post" runat="server">		    
			<uc1:Navigation Visible="true" id="Navigation1" runat="server"></uc1:Navigation>
            <uc2:home Visible="false" ID="Home1" runat="server" />
            <uc3:objectlisting Visible="false" ID="Objectlisting1" runat="server" />
			
		</form>
    </body>
</html>
