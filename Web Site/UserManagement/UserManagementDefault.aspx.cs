using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class UserManagement_UserManagementDefault : System.Web.UI.Page
{
    #region HideControls

    private void HideControls()
    {
        this.Home1.Visible = false;
        this.Objectlisting1.Visible = false;


    }

    #endregion

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        if (!Page.IsPostBack)
        {
            #region Show Start Screen
            this.Navigation1.HideItems();
            HideControls();
            this.Home1.Visible = true;


            #endregion
        }
    }

    protected override bool OnBubbleEvent(object source, EventArgs args)
    {

        

        bool handled = false;
        if (args is CommandEventArgs)
        {
            CommandEventArgs ce = (CommandEventArgs)args;
            switch (ce.CommandName)
            {
                case "UM_HomeClicked":
                    #region Show Start Screen
                    this.Navigation1.HideItems();
                    HideControls();
                    this.Home1.Visible = true;

                    break;
                    #endregion

                case "UM_ShowGroupSearchScreen":
                    #region Show the Group Listing display

                    this.Navigation1.HideItems();
                    HideControls();

                    this.Objectlisting1.Visible = true;
                    this.Objectlisting1.DisplayGroupListing = true;
                    this.Objectlisting1.DisplayUserListing = false;

                    break;
                    #endregion

                case "UM_ShowUserSearchScreen":
                    #region Show the User Listing display

                    this.Navigation1.HideItems();
                    HideControls();

                    this.Objectlisting1.Visible = true;
                    this.Objectlisting1.DisplayGroupListing = false;
                    this.Objectlisting1.DisplayUserListing = true;

                    break;
                    #endregion

                case "UM_ShowAddNewUserScreen":
                    #region Show the add new user screen

                    HideControls();
                    this.Navigation1.HideItems();
                    this.Navigation1.Display_RequestNewUserLink = true;                   

                    break;
                    #endregion

            }
        }

        return true;
    }
}