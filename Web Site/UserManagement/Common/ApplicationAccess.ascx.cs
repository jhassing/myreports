using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using DistributionPortal;

public partial class UserManagement_Common_ApplicationAccess : System.Web.UI.UserControl
{

    #region Properties

    public System.Web.UI.WebControls.CheckBox CheckBox_MyReports
    {
        get
        {
            return chkMyReports;
        }
        set
        {
            chkMyReports = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_BudgetDistribution
    {
        get
        {
            return chkBudgetDistribution;
        }
        set
        {
            chkBudgetDistribution = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_Passport
    {
        get
        {
            return this.chkPassport;
        }
        set
        {
            this.chkPassport = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_BlueBookDistribution
    {
        get
        {
            return this.chkBlueBookDistribution;
        }
        set
        {
            this.chkBlueBookDistribution = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_MupFinAdmin
    {
        get
        {
            return this.chkMupFinAdmin;
        }
        set
        {
            this.chkMupFinAdmin = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_MupPayAdmin
    {
        get
        {
            return this.chkMupPayAdmin;
        }
        set
        {
            this.chkMupPayAdmin = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_MyFacts
    {
        get
        {
            return this.chkMyFacts;
        }
        set
        {
            this.chkMyFacts = value;
        }
    }

    #endregion

    #region PreRender

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin") || 
            MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin"))
        {
            // enable the MyReports and BudgetDistribution check boxes
            this.chkMyReports.Enabled = true;
            this.chkBudgetDistribution.Enabled = true;
        }
        else
        {
            // disable the MyReports and BudgetDistribution check boxes
            this.chkMyReports.Enabled = false;
            this.chkBudgetDistribution.Enabled = false;
        }

        this.chkPassport.Enabled = MyReports.Common.AppValues.IsInRole("PassportAdmins");
        this.chkBlueBookDistribution.Enabled = MyReports.Common.AppValues.IsInRole("BlueBookDistributionAdmins");
        this.chkMyFacts.Enabled = MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin");

        this.chkMupFinAdmin.Enabled = MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin");
        this.chkMupPayAdmin.Enabled = MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin");

    }

    #endregion


}