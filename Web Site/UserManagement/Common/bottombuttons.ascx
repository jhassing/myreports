<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BottomButtons.ascx.cs" Inherits="UserManagement_Common_BottomButtons" %>
<div>
	<DIV style="HEIGHT: 2px">
		<HR>
	</DIV>
	&nbsp;
	<asp:button Width="80px" id="btnBack" runat="server" CssClass="MyReportsAdminConsoleButton "
		Text="Back" CausesValidation="False" onclick="btnBack_Click"></asp:button>&nbsp;
	<asp:button Width="80px" id="btnOk" runat="server" CssClass="MyReportsAdminConsoleButton" Text="Ok" onclick="btnOk_Click"></asp:button>&nbsp;
	<asp:button id="btnApply" runat="server" Text="Apply" CssClass="MyReportsAdminConsoleButton"
		Width="80px" onclick="btnApply_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
	<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="MyReportsAdminConsoleButton"
		Width="80px" onclick="btnDeleteUser_Click"></asp:button>&nbsp;
</div>
