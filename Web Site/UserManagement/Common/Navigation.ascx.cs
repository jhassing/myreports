using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

public partial class UserManagement_Common_Navigation : System.Web.UI.UserControl
{

    #region Properties

    public bool Display_GroupListingMenu
    {
        set
        {
            this.GroupListingLink.Visible = value;
            this.GroupListingLink_Sep.Visible = value;
        }
    }

    public bool Display_UserListingMenu
    {
        set
        {
            this.UserListingLink.Visible = value;
            this.UserListingLink_Sep.Visible = value;
        }
    }

    public bool Display_ObjectValueLink
    {
        set
        {
            this.ObjectValueLink.Visible = value;
            this.ObjectValueLink_Sep.Visible = value;
        }
    }

    public string Set_ObjectValueLinkText
    {
        set
        {
            this.ObjectValueLink.Text = value;
        }
    }

    public bool Display_RequestNewUserLink
    {
        set
        {
            this.RequestNewUserLink.Visible = value;
            this.RequestNewUserLink_Sep.Visible = value;
        }
    }

    #endregion

    public void HideItems()
    {
        this.Display_GroupListingMenu = false;
        this.Display_UserListingMenu = false;
        this.Display_ObjectValueLink = false;
        this.Display_RequestNewUserLink = false;

    }
}