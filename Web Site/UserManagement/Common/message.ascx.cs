using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using Microsoft.ApplicationBlocks.ExceptionManagement;

public partial class UserManagement_Common_Message : System.Web.UI.UserControl
{

    public void ShowMessage(string message, System.Drawing.Color color)
    {
        this.divMessage.Style.Add("DISPLAY", "inline");
        this.divMessage.Style.Add("VISIBILITY", "visible");
        this.lblMessage.Text = message;
        this.lblMessage.ForeColor = color;
        this.lblMessage.Visible = true;
    }

    public void HideMessage()
    {
        this.divMessage.Style.Add("DISPLAY", "inline");
        this.divMessage.Style.Add("VISIBILITY", "hidden");
        this.lblMessage.Text = "";
        this.lblMessage.Visible = false;
    }

    public void DisplayError(Exception ex)
    {
        DisplayError(ex, false);
    }

    public void DisplayError(Exception ex, bool suppressLog)
    {
        this.lblMessage.Visible = true;
        string mess = ex.Message;
        mess += "\r\n";

        if (ex.InnerException != null)
            mess += ex.InnerException.Message;

        this.lblMessage.Text = mess;

        if (!suppressLog)
            ExceptionManager.Publish(ex);
    }
}