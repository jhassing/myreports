<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Navigation.ascx.cs" Inherits="UserManagement_Common_Navigation" %>
<table class="breadcrumbs" cellSpacing="0" cellPadding="3" width="100%" border="0">
	<tr>
		<td align="left" width="100%">
			<!-- User Management -->
			<asp:Label id="lblUserManagement" CssClass="breadcrumb" runat="server">User Management > </asp:Label>
			<!-- Home Landing -->
			<asp:LinkButton id="HomeLink" CommandName="UM_HomeClicked" Visible="True" CssClass="breadcrumb" runat="server" CausesValidation="False">Home</asp:LinkButton>
			<asp:Label id="HomeLink_Sep" Visible="True" CssClass="breadcrumb" runat="server"> > </asp:Label>			
			<!-- Group Listing -->
			<asp:LinkButton id="GroupListingLink" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False">Group Listing</asp:LinkButton>
			<asp:Label id="GroupListingLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
			<!-- User Listing -->
			<asp:LinkButton id="UserListingLink" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False">User Listing</asp:LinkButton>
			<asp:Label id="UserListingLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
			<!-- Object Value -->
			<asp:LinkButton id="ObjectValueLink" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False">CHB18</asp:LinkButton>
			<asp:Label id="ObjectValueLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>
			<!-- Request New User  -->
			<asp:LinkButton id="RequestNewUserLink" Visible="False" CssClass="breadcrumb" runat="server" CausesValidation="False">Request New User</asp:LinkButton>
			<asp:Label id="RequestNewUserLink_Sep" Visible="False" CssClass="breadcrumb" runat="server"> > </asp:Label>			
		</td>
	</tr>
</table>

