using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Text;

public partial class UserManagement_Common_BottomButtons : System.Web.UI.UserControl
{

    public event Delegates.BackButtonClicked BackButton_OnClick;
    public event Delegates.OKButtonClicked OKButton_OnClick;
    public event Delegates.ApplyButtonClicked ApplyButton_OnClick;
    public event Delegates.DeleteButtonClicked DeleteButton_OnClick;

    #region Properties

    //		public bool Display_DeleteButton
    //		{
    public bool Display_DeleteButton
    {
        get
        {
            return ViewState["Display_DeleteButton"] != null ? System.Convert.ToBoolean(ViewState["Display_DeleteButton"].ToString()) : true;
        }
        set
        {
            ViewState["Display_DeleteButton"] = value;
            this.btnDelete.Visible = value;
        }

    }

    //		public bool Display_ApplyButton
    //		{
    public bool Display_ApplyButton
    {
        get
        {
            return ViewState["Display_ApplyButton"] != null ? System.Convert.ToBoolean(ViewState["Display_ApplyButton"].ToString()) : true;
        }
        set
        {
            ViewState["Display_ApplyButton"] = value;
            this.btnApply.Visible = value;
        }
    }

    //		public bool Display_OKButton
    //		{
    public bool Display_OKButton
    {
        get
        {
            return ViewState["Display_OKButton"] != null ? System.Convert.ToBoolean(ViewState["Display_OKButton"].ToString()) : true;
        }
        set
        {
            this.btnOk.Visible = value;
            ViewState["Display_OKButton"] = value;
        }
    }


    #endregion

    #region PreRender

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Visible)
        {
            #region Build Client side Java Script to disable the buttons

            StringBuilder sb = new StringBuilder();

            if (this.Display_ApplyButton)
                sb.Append("document.all." + btnApply.ClientID + ".disabled = true;");

            if (this.Display_DeleteButton)
                sb.Append("document.all." + btnDelete.ClientID + ".disabled = true;");

            if (this.Display_OKButton)
                sb.Append("document.all." + btnOk.ClientID + ".disabled = true;");

            sb.Append("document.all." + btnBack.ClientID + ".disabled = true;");

            // Search button java script to disable it					
            if (this.Display_ApplyButton)
                btnApply.Attributes.Add("onclick", sb.ToString() + this.Page.GetPostBackEventReference(btnApply) + ";");

            if (this.Display_DeleteButton)
                btnDelete.Attributes.Add("onclick", sb.ToString() + this.Page.GetPostBackEventReference(btnDelete) + ";");

            if (this.Display_OKButton)
                btnOk.Attributes.Add("onclick", sb.ToString() + this.Page.GetPostBackEventReference(btnOk) + ";");

            btnBack.Attributes.Add("onclick", sb.ToString() + this.Page.GetPostBackEventReference(btnBack) + ";");

            #endregion

        }
    }

    #endregion

    #region OnLoad

    protected void Page_Load(object sender, System.EventArgs e)
    {
        btnBack.Attributes.Add("onClick", "history.back()");


    }

    #endregion

    protected void btnBack_Click(object sender, System.EventArgs e)
    {
        if (BackButton_OnClick != null)
            BackButton_OnClick();
    }

    protected void btnOk_Click(object sender, System.EventArgs e)
    {
        if (OKButton_OnClick != null)
            OKButton_OnClick();
    }

    protected void btnApply_Click(object sender, System.EventArgs e)
    {
        if (ApplyButton_OnClick != null)
            ApplyButton_OnClick();
    }

    protected void btnDeleteUser_Click(object sender, System.EventArgs e)
    {
        if (DeleteButton_OnClick != null)
            DeleteButton_OnClick();
    }

  

}