<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HomeFolders.ascx.cs" Inherits="UserManagement_Common_HomeFolders" %>
<table width="100%">
	<tr>
		<td colspan="5">
			<asp:RadioButton CssClass="XX-Small" id="rdoSpecific" runat="server" Text="To assign a specific home folder, select from the following available listing"
				AutoPostBack="True" GroupName="Listing" Checked="True" oncheckedchanged="rdoSpecific_CheckedChanged"></asp:RadioButton>
		</td>
	</tr>
	<tr>
		<td width="2" style="WIDTH: 2px"></td>
		<td colspan="4">
			<asp:ListBox id="lstGroups" Height="120px" Width="100%" CssClass="XX-Small" runat="server"></asp:ListBox>
		</td>
	</tr>
	<tr>
		<td colspan="5"><asp:RadioButton id="rdoGeneric" runat="server" CssClass="XX-Small" Text="To assign a generic home folder, select the appropriate level"
				AutoPostBack="True" GroupName="Listing" oncheckedchanged="rdoGeneric_CheckedChanged"></asp:RadioButton></td>
	</tr>
	<tr>
		<td style="WIDTH: 20px"></td>
		<td>
			<asp:RadioButton CssClass="XX-Small" id="rdoAccounting" Text="/(1) Accounting/" runat="server" GroupName="Operations"
				Enabled="False"></asp:RadioButton></td>
		<td>
			<asp:RadioButton CssClass="XX-Small" id="rdoDistrict" Text="/(2) District/" runat="server" GroupName="Operations"
				Enabled="False"></asp:RadioButton></td>
		<td>
			<asp:RadioButton CssClass="XX-Small" id="rdoOperations" Text="/(3) Operations/" runat="server" GroupName="Operations"
				Enabled="False"></asp:RadioButton></td>
		<td>
			<asp:RadioButton CssClass="XX-Small" id="rdoPayroll" Text="/(4) Payroll/" runat="server" GroupName="Operations"
				Enabled="False"></asp:RadioButton></td>
	</tr>
</table>
