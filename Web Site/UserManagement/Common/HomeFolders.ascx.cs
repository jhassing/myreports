using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using Compass.Security.ActiveDirectory;

using UserManagement.BLL;
using MyReports.Common;

public partial class UserManagement_Common_HomeFolders : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (Visible)
        {
            if (rdoSpecific.Checked)
                EnableSpecific();
            else
                EnableGeneric();

            //				if (!Page.IsPostBack)
            //				{
            //					if ( DistributionPortal.NewUserFields.rdNewUsersGroups != null)
            //					{
            //						if (DistributionPortal.NewUserFields.rdNewUsersGroups.Count > 0)
            //						{
            //							HomeFolderListing = (DistributionPortal.NewUserFields.rdNewUsersGroups);
            //								
            //						}
            //					}
            //				}
        }
    }

    public ArrayList HomeFolderListing
    {
        set
        {
            lstGroups.DataSource = value;
            lstGroups.DataBind();
        }
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {

    }
    #endregion

    protected void rdoSpecific_CheckedChanged(object sender, System.EventArgs e)
    {
        EnableSpecific();
    }

    private void EnableSpecific()
    {
        this.rdoAccounting.Enabled = false;
        this.rdoDistrict.Enabled = false;
        this.rdoOperations.Enabled = false;
        this.rdoPayroll.Enabled = false;

        this.lstGroups.Enabled = true;
    }


    protected void rdoGeneric_CheckedChanged(object sender, System.EventArgs e)
    {
        EnableGeneric();
    }

    private void EnableGeneric()
    {
        this.rdoAccounting.Enabled = true;
        this.rdoDistrict.Enabled = true;
        this.rdoOperations.Enabled = true;
        this.rdoPayroll.Enabled = true;

        this.lstGroups.Enabled = false;
    }

    public string SubSelection
    {
        get
        {
            if (rdoAccounting.Checked)
                return ("/(1) Accounting/");
            else if (rdoDistrict.Checked)
                return ("/(2) District/");
            else if (rdoOperations.Checked)
                return ("/(3) Operations/");
            else if (rdoPayroll.Checked)
                return ("/(4) Payroll/");
            else
                return "";
        }
        set
        {
            switch (value)
            {
                case "/(1) Accounting/":
                    rdoAccounting.Checked = true;
                    break;
                case "/(2) District/":
                    rdoDistrict.Checked = true;
                    break;
                case "/(3) Operations/":
                    rdoOperations.Checked = true;
                    break;
                case "/(4) Payroll/":
                    rdoPayroll.Checked = true;
                    break;
            }
        }
    }

    public string MainSelection
    {
        get
        {
            if (rdoGeneric.Checked)
                return ("Generic");
            else
                return ("Specific");
        }
        set
        {
            if (value == "Generic")
            {
                rdoGeneric.Checked = true;
                rdoSpecific.Checked = false;
                EnableGeneric();
            }
            else
            {
                EnableSpecific();
                this.rdoSpecific.Checked = true;
                rdoGeneric.Checked = false;
            }

        }
    }

    public string HomeFolder
    {
        get
        {
            return (this.lstGroups.SelectedValue.ToString());
        }
    }
}