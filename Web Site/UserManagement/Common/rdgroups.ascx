<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RDGroups.ascx.cs" Inherits="UserManagement_Common_RDGroups" %>
<%@ Register TagPrefix="uc1" TagName="MyReportsGroupType" Src="../../Common/MyReportsGroupType.ascx" %>
<table border="0" cellpadding="0" cellspacing="1">
	<tr>
		<td colSpan="2"><asp:label id="lblAvailable" runat="server" CssClass="XX-SmallBold">Available:</asp:label></td>
		<td colSpan="1"><asp:label id="lblSelected" runat="server" CssClass="XX-SmallBold">Selected:</asp:label></td>
	</tr>
	<tr>
		<td><asp:listbox id="lstAvailable" runat="server" CssClass="XX-Small" SelectionMode="Multiple" Height="200px"
				Width="290px"></asp:listbox></td>
		<td align="center"><asp:button id="btnAdd" runat="server" CssClass="MyReportsAdminConsoleButton" Width="80px" Text="Add >>" onclick="btnAdd_Click"></asp:button><br>
			<br>
			<asp:button id="btnRemove" runat="server" CssClass="MyReportsAdminConsoleButton" Width="80px"
				Text="<< Remove" onclick="btnRemove_Click"></asp:button></td>
		<td><asp:listbox id="lstSelected" runat="server" CssClass="XX-Small" SelectionMode="Multiple" Height="200px"
				Width="290px"></asp:listbox></td>
	</tr>
	<tr>
		<td colspan="3" noWrap style="height: 26px"><asp:label id="Label1" runat="server" CssClass="X-SmallBold">Filter:&nbsp;</asp:label><asp:textbox autocomplete="off"  id="txtFilter" runat="server" CssClass="X-Small" Width="127px" MaxLength="25"></asp:textbox>

&nbsp;&nbsp;
<asp:button id="btnSearch" runat="server" CssClass="MyReportsAdminConsoleButton" Text="Search" onclick="btnSearch_Click"></asp:button>&nbsp;
			<INPUT class="MyReportsAdminConsoleButton" id="btnClear" type="reset" value="Clear">
			&nbsp;&nbsp;
		<uc1:myreportsgrouptype id="MyReportsGroupType1" runat="server"></uc1:myreportsgrouptype>
           &nbsp
		
		</td>
	</tr>
	<tr>
		<td align="center" colSpan="4">
			<div id="divMessageBar" style="DISPLAY: none" align="center"><span id="ff"><FONT face="Verdana" size="2"><STRONG>Retrieving 
							group information. Please wait....</STRONG></FONT></span>
			</div>
			<div id="div2" align="center"><asp:label id="lblMessage" runat="server" CssClass="XX-Small" Font-Bold="True" Visible="False"></asp:label></div>
		</td>
	</tr>
</table>
