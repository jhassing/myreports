using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text;

using Compass.Security.ActiveDirectory;
using MyReports.Common;

using DistributionPortal;

public partial class UserManagement_Common_RDGroups : ClientSideControl
{

    #region Events

    public event Delegates.AddClicked Add_OnClick;
    public event Delegates.RemoveClicked Remove_OnClick;
    public event Delegates.SearchButtonClicked ListSearch_OnClick;

    #endregion

    #region Properties

    /// <summary>
    /// Gets and Sets the Selected listbox contents
    /// </summary>
    public ArrayList SelectedList
    {
        get
        {
            return ListBoxContents(this.lstSelected);
        }
        set
        {
            SortAndBindListBox(this.lstSelected, value);


            // Save the original Selected List

            ArrayList aryOriginalList = new ArrayList();

            foreach (ListItem li in value)
            {
                aryOriginalList.Add(li.Value);
            }

            this.OriginalSelected = aryOriginalList;

            this.ObjectsAdded = new ArrayList();
            this.ObjectsRemoved = new ArrayList();
        }
    }


    /// <summary>
    /// Gets and Sets the Available Listbox contents
    /// </summary>
    public ArrayList AvailableList
    {
        get
        {
            return ListBoxContents(this.lstAvailable);
        }
        set
        {
            try
            {
                ArrayList al = (ArrayList)value;
                ArrayList arySelectedGroups = new ArrayList();
                bool allowAvailable = false;

                // Loop through the "available" groups that were just found with the search	 .
                // remove any groups that already exist in the selected boxes
                for (int x = 0; x < al.Count; x++)
                {
                    string groupName = ((System.Web.UI.WebControls.ListItem)al[x]).Text.Trim();

                    allowAvailable = true;

                    foreach (System.Web.UI.WebControls.ListItem selected in SelectedList)
                    {
                        if (selected.Text.ToLower().Equals(groupName.ToLower()))
                        {
                            allowAvailable = false;
                            break;
                        }
                    }

                    if (allowAvailable)
                        arySelectedGroups.Add(al[x]);

                }

                SortAndBindListBox(this.lstAvailable, arySelectedGroups);

                if (al.Count == 0)
                {
                    // no groups match
                    //lblMessage.Text = "No matching groups...";
                    //lblMessage.ForeColor = System.Drawing.Color.Red;
                    //lblMessage.Visible = true;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }


    /// <summary>
    /// Gets or Sets the filer text box
    /// </summary>
    public string Filter
    {
        get
        {
            return this.txtFilter.Text;
        }
        set
        {
            this.txtFilter.Text = value;
        }
    }


    /// <summary>
    /// Display the Financial radio option buton
    /// </summary>
    public bool Display_Financial_Option
    {
        set
        {
            this.MyReportsGroupType1.Display_Financial_Option = value;
        }
    }


    /// <summary>
    /// Display the Hourly radio option buton
    /// </summary>
    public bool Display_Hourly_Option
    {
        set
        {
            this.MyReportsGroupType1.Display_Hourly_Option = value;
        }
    }


    /// <summary>
    /// Display the Salary radio option buton
    /// </summary>
    public bool Display_Salary_Option
    {
        set
        {
            this.MyReportsGroupType1.Display_Salary_Option = value;
        }
    }


    /// <summary>
    /// Display the all radio option buton
    /// </summary>
    public bool Display_BlueBook_Option
    {
        set
        {
            this.MyReportsGroupType1.Display_BlueBook_Option = value;
        }
    }



    public bool Financial_Option_Checked
    {
        get
        {
            return this.MyReportsGroupType1.Financial_Option_Checked;

        }
        set
        {
            this.MyReportsGroupType1.Financial_Option_Checked = value;
        }
    }


    public bool Salary_Option_Checked
    {
        get
        {
            return this.MyReportsGroupType1.Salary_Option_Checked;
        }
        set
        {
            this.MyReportsGroupType1.Salary_Option_Checked = value;

        }
    }


    public bool Hourly_Option_Checked
    {
        get
        {
            return this.MyReportsGroupType1.Hourly_Option_Checked;
        }
        set
        {
            this.MyReportsGroupType1.Hourly_Option_Checked = value;
        }
    }


    public bool BlueBook_Option_Checked
    {
        get
        {
            return this.MyReportsGroupType1.BlueBook_Option_Checked;
        }
        set
        {
            this.MyReportsGroupType1.BlueBook_Option_Checked = value;
        }
    }


    public ArrayList ObjectsAdded
    {
        get
        {
            if (ViewState["ObjectsAdded"] != null)
                return (ArrayList)ViewState["ObjectsAdded"];
            else
                return new ArrayList();
        }
        set
        {
            ViewState["ObjectsAdded"] = value;
        }
    }

    public ArrayList ObjectsRemoved
    {
        get
        {
            if (ViewState["ObjectsRemoved"] != null)
                return (ArrayList)ViewState["ObjectsRemoved"];
            else
                return new ArrayList();
        }
        set
        {
            ViewState["ObjectsRemoved"] = value;
        }
    }

    public ArrayList OriginalSelected
    {
        get
        {
            if (ViewState["OriginalSelected"] != null)
                return (ArrayList)ViewState["OriginalSelected"];
            else
                return new ArrayList();
        }
        set
        {
            ViewState["OriginalSelected"] = value;
        }
    }


    #endregion

    #region Search Button Clicked

    protected void btnSearch_Click(object sender, System.EventArgs e)
    {
        // Fire Search event
        if (ListSearch_OnClick != null)
            ListSearch_OnClick();

    }


    #endregion

    #region PreRender Page

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Visible)
        {

            StringBuilder sbSave = new StringBuilder();

            if (!Page.IsClientScriptBlockRegistered("Disable-DisableGroupButtons Function"))
            {
                sbSave.Append(getOpenScriptTag());

                sbSave.Append("\t function DisableGroupButtons() { \r\n \r\n");

                sbSave.Append("\t\t document.all." + this.btnSearch.ClientID + ".disabled = true; \r\n ");
                sbSave.Append("\t\t document.all." + this.btnRemove.ClientID + ".disabled = true; \r\n ");
                sbSave.Append("\t\t document.all." + this.btnAdd.ClientID + ".disabled = true; \r\n ");
                sbSave.Append("\t\t document.all.btnClear.disabled = true; \r\n ");

                //sbSave.Append("\t\t document.all." + this.rdoFinancial.ClientID + ".disabled = true; \r\n ");
                //sbSave.Append("\t\t document.all." + this.rdoSalary.ClientID + ".disabled = true; \r\n ");
                //sbSave.Append("\t\t document.all." + this.rdoHourly.ClientID + ".disabled = true; \r\n ");

                sbSave.Append("\t\t } \r\n");
                sbSave.Append(getCloseScriptTag());

                Page.RegisterClientScriptBlock("Disable-DisableGroupButtons Function", sbSave.ToString());

            }

            btnSearch.Attributes.Add("onClick", "DisableGroupButtons();" + this.Page.GetPostBackEventReference(this.btnSearch) + ";");
            btnRemove.Attributes.Add("onClick", "DisableGroupButtons();" + this.Page.GetPostBackEventReference(this.btnRemove) + ";");
            btnAdd.Attributes.Add("onClick", "DisableGroupButtons();" + this.Page.GetPostBackEventReference(this.btnAdd) + ";");
        }
    }


    #endregion

    #region Sort And Bind List Box

    private void SortAndBindListBox(ListBox lb, ArrayList al)
    {
        try
        {
            lb.Items.Clear();

            for (int x = 0; x < al.Count; x++)
            {
                lb.Items.Add((System.Web.UI.WebControls.ListItem)al[x]);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region List Box Contents

    private ArrayList ListBoxContents(ListBox lb)
    {
        ArrayList aryExisting = new ArrayList();

        foreach (ListItem item in lb.Items)
            aryExisting.Add(item);

        return (aryExisting);
    }


    #endregion

    #region Add Button Clicked

    protected void btnAdd_Click(object sender, System.EventArgs e)
    {
        ArrayList arySelected = ListBoxContents(this.lstSelected);
        ArrayList aryAvailable = ListBoxContents(this.lstAvailable);

        foreach (ListItem item in this.lstAvailable.Items)
        {
            if (item.Selected)
            {
                if (!arySelected.Contains(item))
                {
                    arySelected.Add(item);
                    aryAvailable.Remove(item);

                    // Keep a running total of what was added.
                    ArrayList aryObjectsAdded;
                    bool doAdd = false;

                    // Was this item an original selected item? If so, do not add it		
                    doAdd = !this.OriginalSelected.Contains(item.Value);

                    if (doAdd)
                    {

                        aryObjectsAdded = this.ObjectsAdded;

                        // If the object is not already in the list, add it
                        if (!aryObjectsAdded.Contains(item.Value))
                            aryObjectsAdded.Add(item.Value);

                        this.ObjectsAdded = aryObjectsAdded;
                    }
                    else
                    {
                        // Remove the object from the ObjectsRemoved List
                        if (this.ObjectsRemoved.Contains(item.Value))
                            this.ObjectsRemoved.Remove(item.Value);
                    }
                }
            }
        }
        SortAndBindListBox(this.lstSelected, arySelected);
        SortAndBindListBox(this.lstAvailable, aryAvailable);

        // Fire Add event
        if (Add_OnClick != null)
            Add_OnClick();
    }


    #endregion

    #region Remove Button Clicked

    protected void btnRemove_Click(object sender, System.EventArgs e)
    {
        ArrayList arySelected = ListBoxContents(this.lstSelected);
        ArrayList aryGroups = ListBoxContents(this.lstAvailable);

        foreach (ListItem item in this.lstSelected.Items)
        {
            if (item.Selected)
            {
                if (!aryGroups.Contains(item))
                {
                    arySelected.Remove(item);
                    aryGroups.Add(item);
                }
                else
                {
                    arySelected.Remove(item);
                }

                // Keep a running total of what was added.
                ArrayList aryObjectsRemoved;
                bool doRemove = false;

                // Was this item an original selected item? If so, do not add it		
                doRemove = this.OriginalSelected.Contains(item.Value);

                if (doRemove)
                {
                    aryObjectsRemoved = this.ObjectsRemoved;

                    // If the object is not already in the list, add it
                    if (!aryObjectsRemoved.Contains(item.Value))
                        aryObjectsRemoved.Add(item.Value);

                    this.ObjectsRemoved = aryObjectsRemoved;
                }
                else
                {
                    // Remove the object from the ObjectsAdded List
                    if (this.ObjectsAdded.Contains(item.Value))
                        this.ObjectsAdded.Remove(item.Value);
                }
            }
        }

        SortAndBindListBox(this.lstSelected, arySelected);
        SortAndBindListBox(this.lstAvailable, aryGroups);

        // Fire remove event
        if (Remove_OnClick != null)
            Remove_OnClick();
    }


    #endregion
}