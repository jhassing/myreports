<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationAccess.ascx.cs" Inherits="UserManagement_Common_ApplicationAccess" %>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
	<tr>
		<td width="100%" valign="top" align="left">&nbsp;&nbsp;
			<asp:Label id="Label2" runat="server" CssClass="XX-SmallBold">Applications to assign to this user.
				</asp:Label><br>
			<br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox CssClass="XX-Small" id="chkMyReports" runat="server" Text="MyReports"></asp:CheckBox><br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox CssClass="XX-Small" id="chkBudgetDistribution" runat="server" Text="Budget Distribution"></asp:CheckBox><br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox CssClass="XX-Small" id="chkPassport" runat="server" Text="Passport"></asp:CheckBox><br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox CssClass="XX-Small" id="chkBlueBookDistribution" runat="server" Text="Blue Book Distribution"></asp:CheckBox><br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox CssClass="XX-Small" id="chkMyFacts" runat="server" Text="MyFACTS"></asp:CheckBox><br>
			<br />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox CssClass="XX-Small" id="chkMupFinAdmin" runat="server" Text="MUP Fin Admin"></asp:CheckBox><br>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox CssClass="XX-Small" id="chkMupPayAdmin" runat="server" Text="MUP Pay Admin"></asp:CheckBox><br>
			<br />
		</td>
	</tr>
</table>
