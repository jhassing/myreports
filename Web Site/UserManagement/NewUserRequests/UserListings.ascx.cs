using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

using UserManagement.BLL;
using MyReports.Common;

public partial class UserManagement_NewUserRequests_UserListings : System.Web.UI.UserControl
{
    #region PreRender

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Page.Visible)
        {
            if (!Page.IsPostBack)
            {
                ViewState["sortASC"] = true;
            }

            LoadData();
        }
    }

    #endregion

    #region getFromDate

    private DateTime getFromDate
    {
        get
        {
            int days = 0;

            if (this.rdoFourWeeks.Checked)
                days = -28;
            if (this.rdoOneWeek.Checked)
                days = -7;
            if (this.rdoThreeWeeks.Checked)
                days = -21;
            if (this.rdoTwoWeeks.Checked)
                days = -14;

            return System.DateTime.Now.AddDays(days);
        }
    }

    #endregion

    #region LoadData

    private void LoadData()
    {

        string sortColumn = "";

        LinkButton lblDate = lblImportedRejectedDate;

        int done = System.Convert.ToInt32(NewUserFunctions.done);

        string userName = "";

        if (!MyReports.Common.AppValues.IsInRole("Domain Admins"))
            if (MyReports.Common.AppValues.UserName.ToLower() != "schiaj01")
                userName = MyReports.Common.AppValues.UserName;

        ViewState["ds"] = NewUserFunctions.oNewUserBLL.GetMasterRecordListing(done, userName, getFromDate, System.DateTime.Now);

        switch (done)
        {
            case 0:	// Outstanding
                lblDate.Visible = false;
                sortColumn = "RequestedDate";
                break;

            case 1:	// Imported
                lblDate.Visible = true;
                lblDate.Text = "Imported Date";
                sortColumn = "ImportedDate";
                break;

            case 2:	// Rejected
                lblDate.Visible = true;
                lblDate.Text = "Rejected Date";
                sortColumn = "ImportedDate";
                break;

        }

        BindGrid(sortColumn);

    }

    #endregion

    #region Bind Grid

    private void BindGrid(string sortColumn)
    {

        IComparer myComparer = null;

        switch (sortColumn)
        {
            case "samAccountName":
                myComparer = new NewUser_SORT_BY_SamAccountName();
                break;

            case "ImportedDate":
                myComparer = new NewUser_SORT_BY_ImportedDate();
                break;

            case "RequestedDate":
                myComparer = new NewUser_SORT_BY_RequestedDate();
                break;

            case "RequestorsEmail":
                myComparer = new NewUser_SORT_BY_RequestorsEmail();
                break;

            case "RequestorsPhone":
                myComparer = new NewUser_SORT_BY_RequestorsPhone();
                break;
        }

        ((ArrayList)ViewState["ds"]).Sort(myComparer);

        if (!(bool)ViewState["sortASC"])
            ((ArrayList)ViewState["ds"]).Reverse();

        Listing1.DataSource = (ArrayList)ViewState["ds"];
        Listing1.DataBind();
    }

    #endregion

    #region Listing1 ItemDataBound

    private void Listing1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem ||
            e.Item.ItemType == ListItemType.SelectedItem)
        {

            StructNewUsers dr = (StructNewUsers)e.Item.DataItem;

            Label lbReqDate = (Label)e.Item.FindControl("RequestedDate");
            Label lblRequestorsEmail = (Label)e.Item.FindControl("RequestorsEmail");
            Label lbReqDalblRequestorsPhone = (Label)e.Item.FindControl("RequestorsPhone");
            Label lblID = (Label)e.Item.FindControl("PK_ID");
            LinkButton lnkSamAccountName = (LinkButton)e.Item.FindControl("lnkSamAccountName");
            Label txtDate = (Label)e.Item.FindControl("txtImportedRejectedDate");
            System.Web.UI.WebControls.Image imgIcon = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgIcon");

            int done = System.Convert.ToInt32(NewUserFunctions.done);

            switch (done)
            {
                case 0:	// Show not impoted yet
                    txtDate.Visible = false;
                    imgIcon.ImageUrl = "../../images/detailicon.gif";
                    break;

                case 1:	// Impoted already
                    txtDate.Visible = true;
                    txtDate.Text = dr.ImportedDate.ToString("g");
                    imgIcon.ImageUrl = "../../images/succeeded.gif";
                    break;

                case 2:	// Rejected User
                    txtDate.Visible = true;
                    txtDate.Text = dr.ImportedDate.ToString("g");
                    imgIcon.ImageUrl = "../../images/cancelled.gif";
                    break;

            }

            lblID.Text = dr.MasterID.ToString();
            lblID.Visible = false;

            // Get the encoded PKID GUID
            string encodedPKID = getEncodedID(lblID.Text);

            // Get the Parent (td) and the parent (tr) row for the double click event hook
            HtmlTableRow myRow = (HtmlTableRow)e.Item.FindControl("RequestedDate").Parent.Parent;

            string retUrl = "../index/index.aspx?app=user_management&individualview=0&done=" + done + "&id=" + encodedPKID;

            myRow.Attributes.Add("OnDblClick", "ShowDetails('" + retUrl + "');");

            lnkSamAccountName.Text = dr.samAccountName;
            lnkSamAccountName.CommandName = "individual";
            lnkSamAccountName.CommandArgument = dr.MasterID.ToString();

            lbReqDate.Text = dr.RequestedDate.ToString("g");
            lblRequestorsEmail.Text = dr.RequestorsEmail;
            lbReqDalblRequestorsPhone.Text = dr.RequestorsPhone;


        }
    }

    #endregion

    #region Listing1_ItemCommand

    private void Listing1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "individual")
        {
            string sID = getEncodedID(e.CommandArgument.ToString());

            string done = NewUserFunctions.done;

            Response.Redirect("../index/index.aspx?app=user_management&individualview=0&done=" + done + "&id=" + sID, false);
        }
    }

    #endregion

    #region getEncodedID

    private string getEncodedID(string PKID)
    {
        return Server.UrlEncode(Functions.Encrypt(PKID));
    }

    #endregion

    #region lnkSortPersonnel_Click

    protected void lnkSortPersonnel_Click(object sender, System.EventArgs e)
    {
        SortData("samAccountName");
    }

    #endregion

    #region lblImportedRejectedDate_Click

    protected void lblImportedRejectedDate_Click(object sender, System.EventArgs e)
    {
        SortData("ImportedDate");
    }

    #endregion

    #region lnkSortRequestedDate_Click

    protected void lnkSortRequestedDate_Click(object sender, System.EventArgs e)
    {
        SortData("RequestedDate");
    }

    #endregion

    #region Linkbutton1_Click

    protected void Linkbutton1_Click(object sender, System.EventArgs e)
    {
        SortData("RequestorsEmail");
    }

    #endregion

    #region Linkbutton2_Click

    protected void Linkbutton2_Click(object sender, System.EventArgs e)
    {
        SortData("RequestorsPhone");
    }

    #endregion

    #region SortData

    private void SortData(string sortColumn)
    {
        ViewState["sortASC"] = !(bool)ViewState["sortASC"];
        BindGrid(sortColumn);
    }

    #endregion

}