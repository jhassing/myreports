<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserListings.ascx.cs" Inherits="UserManagement_NewUserRequests_UserListings" %>	

<script language="javascript">
<!--
	function ShowDetails(url)
	{

		window.navigate(url);
	}
//-->
</script>
<table>
	<tr>
		<td>
			<asp:RadioButton id="rdoOneWeek" runat="server" Text="One Week" GroupName="Weeks" Checked="false" AutoPostBack="true"></asp:RadioButton></td>
		<td>
			<asp:RadioButton id="rdoTwoWeeks" runat="server" Text="Two Weeks" GroupName="Weeks" Checked="true" AutoPostBack="true"></asp:RadioButton></td>
		<td>
			<asp:RadioButton id="rdoThreeWeeks" runat="server" Text="Three Weeks" GroupName="Weeks" Checked="false" AutoPostBack="true"></asp:RadioButton></td>
		<td>
			<asp:RadioButton id="rdoFourWeeks" runat="server" Text="Four Weeks" Checked="false" GroupName="Weeks" AutoPostBack="true"></asp:RadioButton></td>
	</tr>
</table>
<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<TR >
		<td nowrap class="TableHeaderRow"></td>
		<td nowrap class="TableHeaderRow">
			<asp:LinkButton CssClass="X-SmallBold_White" id="lnkSortPersonnel" runat="server" onclick="lnkSortPersonnel_Click">Personnel #</asp:LinkButton>
		</td>
		<td nowrap class="TableHeaderRow">
			<asp:LinkButton CssClass="X-SmallBold_White" id="lblImportedRejectedDate" runat="server" onclick="lblImportedRejectedDate_Click"></asp:LinkButton>
		</td>
		<td nowrap class="TableHeaderRow">
			<asp:LinkButton CssClass="X-SmallBold_White" id="lnkSortRequestedDate" runat="server" onclick="lnkSortRequestedDate_Click">Requested Date</asp:LinkButton>
		</TD>
		<td nowrap class="TableHeaderRow">
			<asp:LinkButton CssClass="X-SmallBold_White" id="Linkbutton1" runat="server" onclick="Linkbutton1_Click">Requestors Email</asp:LinkButton>
		</TD>
		<td nowrap class="TableHeaderRow">
			<asp:LinkButton CssClass="X-SmallBold_White" id="Linkbutton2" runat="server" onclick="Linkbutton2_Click">Requestors Phone</asp:LinkButton>
		</TD>

	</TR>
	<tr width="100%">
		<td colspan="7" width="100%"><img src="../../images/vspace.gif" width="100%" height="4" border="0"></td>
	</tr>
	<asp:repeater id="Listing1" runat="server">
		<HeaderTemplate>
		</HeaderTemplate>
		<AlternatingItemTemplate>
			<tr runat="server" class="AltTableRow" ID="Tr1">
				<td nowrap width="1" align="right">
					<asp:Image id="imgIcon" runat="server" />
				</td>
				<td nowrap>
					<asp:LinkButton id="lnkSamAccountName" CssClass="hyperlink" runat="server"></asp:LinkButton>
				</td>
				<td nowrap>
					<asp:Label id="txtImportedRejectedDate" CssClass="XX-Small" runat="server"></asp:Label></td>
				<td nowrap>
					<asp:Label id="RequestedDate" CssClass="XX-Small" runat="server"></asp:Label></td>
				<td nowrap>
					<asp:Label id="RequestorsEmail" CssClass="XX-Small" runat="server"></asp:Label>
				</td>
				<td nowrap>
					<asp:Label id="RequestorsPhone" CssClass="XX-Small" runat="server"></asp:Label>
				</td>
				<td nowrap>
					<asp:Label id="PK_ID" CssClass="XX-Small" runat="server"></asp:Label></td>
		
			</tr>
		</AlternatingItemTemplate>
		<ItemTemplate>
			<tr runat="server" class="TableRow">
				<td nowrap width="1" align="right">
					<asp:Image id="imgIcon" runat="server" />
				</td>
				<td nowrap>
					<asp:LinkButton id="lnkSamAccountName" CssClass="hyperlink" runat="server"></asp:LinkButton>
				</td>
				<td nowrap>
					<asp:Label id="txtImportedRejectedDate" CssClass="XX-Small" runat="server"></asp:Label></td>
				<td nowrap>
					<asp:Label id="RequestedDate" CssClass="XX-Small" runat="server"></asp:Label></td>
				<td nowrap>
					<asp:Label id="RequestorsEmail" CssClass="XX-Small" runat="server"></asp:Label>
				</td>
				<td nowrap>
					<asp:Label id="RequestorsPhone" CssClass="XX-Small" runat="server"></asp:Label>
				</td>
				<td nowrap>
					<asp:Label id="PK_ID" CssClass="XX-Small" runat="server"></asp:Label></td>
				
			</tr>
		</ItemTemplate>
	</asp:repeater></TR></table>
