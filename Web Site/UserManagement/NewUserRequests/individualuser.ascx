<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IndividualUser.ascx.cs" Inherits="UserManagement_NewUserRequests_IndividualUser" %>

<%@ Register TagPrefix="uc1" TagName="PersonnelNumber" Src="../Common/PersonnelNumber.ascx" %>

<table cellSpacing="0" cellPadding="0" width="90%" border="0">
	<TR>
		<TD><br>
			<div class="ThinBorder">
				<table>
					<tr>
						<td noWrap><asp:label id="Label8" CssClass="XX-SmallBold" runat="server">Requestor's Network ID: </asp:label>&nbsp;
							<asp:label id="lblRequestorsNetworkID" CssClass="XX-Small" runat="server"></asp:label>&nbsp;</td>
						<td noWrap><asp:label id="Label10" CssClass="XX-SmallBold" runat="server">Requestor's Email Address: </asp:label>&nbsp;
							<asp:label id="lblRequestorsEmailAddress" CssClass="XX-Small" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
					<tr>
						<td noWrap><asp:label id="Label7" CssClass="XX-SmallBold" runat="server">Requestor's First Name: </asp:label>&nbsp;
							<asp:label id="lblRequestorsFirstName" CssClass="XX-Small" runat="server"></asp:label>&nbsp;
						</td>
						<td noWrap><asp:label id="Label71" CssClass="XX-SmallBold" runat="server">Requestor's Last Name: </asp:label>&nbsp;
							<asp:label id="lblRequestorsLastName" CssClass="XX-Small" runat="server"></asp:label>&nbsp;
							<asp:label id="Label5" CssClass="XX-SmallBold" runat="server">Requested Date: </asp:label>&nbsp;
							<asp:label id="lblRequestedDate" CssClass="XX-Small" runat="server"></asp:label>&nbsp;
						</td>
					</tr>
				</table>
			</div>
			<br>
		</TD>
	</TR>
	<asp:panel id="panLastModifiedBy" CssClass="ThinBorder" runat="server">
		<TR>
			<TD noWrap><BR>
				<DIV class="ThinBorder" align="left"><IMG class="menuSelection" onclick="Image_Click(this)" src="../images/plus.gif" align="middle">
					&nbsp;
					<asp:Label id="Label1" onclick="Image_Click(this)" CssClass="menuSelectionText" Runat="server">Import Options</asp:Label></DIV>
				<DIV class="ThinBorder_MissingTop" style="DISPLAY: none">
					<TABLE style="MARGIN-LEFT: 82px">
						<TR>
							<TD noWrap>
								<asp:label id="lblLastModifiedNetworkIDLabel" runat="server" CssClass="XX-SmallBold">Network ID:</asp:label>&nbsp;
								<asp:label id="lblLastModifiedNetworkID" runat="server" CssClass="XX-Small"></asp:label>&nbsp;</TD>
							<TD noWrap>
								<asp:label id="lblLastModifiedEmailAddressLabel" runat="server" CssClass="XX-Small"></asp:label>&nbsp;
								<asp:label id="lblLastModifiedEmailAddress" runat="server" CssClass="XX-Small"></asp:label>&nbsp;
							</TD>
						</TR>
						<TR>
							<TD noWrap>
								<asp:label id="lblLastModifiedFirstNameLabel" runat="server" CssClass="XX-SmallBold">First Name: </asp:label>&nbsp;
								<asp:label id="lblLastModifiedFirstName" runat="server" CssClass="XX-Small"></asp:label>&nbsp;
							</TD>
							<TD noWrap>
								<asp:label id="lblLastModifiedLastNameLabel" runat="server" CssClass="XX-SmallBold">Last Name: </asp:label>&nbsp;
								<asp:label id="lblLastModifiedLastName" runat="server" CssClass="XX-Small"></asp:label>&nbsp;
								<asp:label id="lblLastModifiedDateLabel" runat="server" CssClass="XX-SmallBold">Date: </asp:label>&nbsp;
								<asp:label id="lblLastModifiedDate" runat="server" CssClass="XX-Small"></asp:label><BR>
							</TD>
						</TR>
					</TABLE>
				</DIV>
			</TD>
		</TR>
	</asp:panel>
	<TR>
		<td><br>
			<uc1:personnelnumber id="PersonnelNumber1" runat="server"></uc1:personnelnumber><br>
			<asp:label id="lblRejectedNotice" runat="server" ForeColor="Red" Visible="False">This user request has been rejected.</asp:label>
			<P></P>
		</td>
	</TR>
	<tr>
		<td noWrap colSpan="1"><asp:datagrid id="DataGrid1" runat="server" ForeColor="Black" GridLines="Vertical" BackColor="White"
				BorderWidth="1px" BorderStyle="Solid" BorderColor="#999999" CellPadding="3" AutoGenerateColumns="False" Width="100%" onselectedindexchanged="DataGrid1_SelectedIndexChanged">
				<FooterStyle BackColor="#CCCCCC"></FooterStyle>
				<SelectedItemStyle BackColor="#000099"></SelectedItemStyle>
				<AlternatingItemStyle CssClass="AltTableRow"></AlternatingItemStyle>
				<ItemStyle CssClass="TableRow"></ItemStyle>
				<HeaderStyle CssClass="TableHeaderRow"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Attribute Name">
						<HeaderStyle CssClass="X-SmallBold_White"></HeaderStyle>
						<ItemTemplate>
							<asp:Label id=AttributeName runat="server" CssClass="XX-SmallBold" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeName") %>' Font-Bold="True">
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Attribute Value">
						<HeaderStyle CssClass="X-SmallBold_White"></HeaderStyle>
						<ItemTemplate>
							<asp:Label id=AttributeValue runat="server" CssClass="XX-Small" Text='<%# DataBinder.Eval(Container, "DataItem.AttributeValue") %>'>
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
				<PagerStyle HorizontalAlign="Center" ForeColor="Black" BackColor="#999999"></PagerStyle>
			</asp:datagrid></td>
	</tr>
	<!-- Import Options --><asp:panel id="panImportOptions" CssClass="ThinBorder" runat="server" Visible="false">
		<TR>
			<TD noWrap><BR>
				<DIV class="ThinBorder" align="left"><IMG class="menuSelection" onclick="Image_Click(this)" src="../images/plus.gif" align="middle">
					&nbsp;
					<asp:Label id="Label2" onclick="Image_Click(this)" CssClass="menuSelectionText" Runat="server">Import Options</asp:Label></DIV>
				<DIV class="ThinBorder_MissingTop" style="DISPLAY: none">
					<TABLE style="MARGIN-LEFT: 82px">
						<TR>
							<TD noWrap>
								<asp:CheckBox id="chkEnableUserAccount" runat="server" CssClass="XX-Small" Checked="True" Text="Enable user account."></asp:CheckBox></TD>
							<TD noWrap>&nbsp;&nbsp;
								<asp:CheckBox id="chkForcePasswordReset" runat="server" CssClass="XX-Small" Checked="False" Text="Force user password reset at next login."></asp:CheckBox></TD>
						</TR>
						<TR>
							<TD noWrap>
								<asp:CheckBox id="chkSetPasswordToNeverExpire" runat="server" CssClass="XX-Small" Checked="True"
									Text="Set password to never expire."></asp:CheckBox></TD>
							<TD noWrap>&nbsp;&nbsp;
								<asp:CheckBox id="chkOverrideExistingUserInfo" runat="server" CssClass="XX-Small" Checked="False"
									Text="Override user information if user exists."></asp:CheckBox></TD>
						</TR>
						<TR>
							<TD noWrap colSpan="2">
								<asp:Label id="Label4" CssClass="XX-Small" Runat="server">Import into:</asp:Label>&nbsp;&nbsp;
								<asp:DropDownList id="drpOUListing" runat="server" Width="80%"></asp:DropDownList></TD>
						</TR>
					</TABLE>
				</DIV>
			</TD>
		</TR>
	</asp:panel>
	<tr>
		<td><br>
			<asp:button id="btnGoBack" CssClass="MyReportsAdminConsoleButton" runat="server" Width="155px"
				Text="Go Back" onclick="btnGoBack_Click"></asp:button>&nbsp;<asp:button id="btnImportUser" CssClass="MyReportsAdminConsoleButton" runat="server" Visible="False"
				Width="155px" Text="Import this user" onclick="btnImportUser_Click"></asp:button>&nbsp;<asp:button id="btnRejectUser" CssClass="MyReportsAdminConsoleButton" runat="server" Width="155px"
				Text="Reject this user" onclick="btnRejectUser_Click"></asp:button></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<!-- Reject Reasons --><asp:panel id="RejectPanel" Runat="server">
		<TR>
			<TD>
				<DIV class="ThinBorder" align="left"><IMG class="menuSelection" onclick="Image_Click(this)" src="../images/plus.gif" align="middle">
					&nbsp;
					<asp:Label id="Label13" onclick="Image_Click(this)" CssClass="menuSelectionText" Runat="server">Reject Reason</asp:Label></DIV>
				<DIV class="ThinBorder_MissingTop" style="DISPLAY: none">
					<asp:DataList id="ListRejectReasons" runat="server" RepeatColumns="1" onselectedindexchanged="ListRejectReasons_SelectedIndexChanged">
						<ItemTemplate>
							&nbsp;&nbsp;&nbsp;
							<asp:label id="Label14" runat="server" CssClass="XX-SmallBold">Rejecter:&nbsp;</asp:label>
							<asp:label id="Label41" runat="server" CssClass="XX-Small"><%# DataBinder.Eval(Container.DataItem, "Rejecter" ) %>&nbsp;&nbsp;&nbsp;</asp:label>
							<asp:label id="Label15" runat="server" CssClass="XX-SmallBold">Reject Date:&nbsp;</asp:label>
							<asp:label id="Label51" runat="server" CssClass="XX-Small">
								<%#  System.Convert.ToDateTime( DataBinder.Eval(Container.DataItem, "RejectDateTime") ).ToString("g")  %>
							</asp:label>
							<TR>
								<TD>&nbsp;&nbsp;&nbsp;
									<asp:label id="Label16" runat="server" CssClass="XX-SmallBold">Reject Reason:&nbsp;</asp:label>
									<asp:label id="Label17" runat="server" CssClass="XX-Small">
										<%#  DataBinder.Eval(Container.DataItem, "RejectReason")  %>
									</asp:label><BR>
									<BR>
								</TD>
							</TR>
						</ItemTemplate>
					</asp:DataList></DIV>
				<BR>
			</TD>
		</TR>
	</asp:panel></TR> 
	<!-- Group Information -->
	<tr>
		<td>
			<div class="ThinBorder" align="left"><IMG class="menuSelection" onclick="Image_Click(this)" src="../images/plus.gif" align="middle">
				&nbsp;<asp:label id="Label12" onclick="Image_Click(this)" CssClass="menuSelectionText" Runat="server">Group Information</asp:label>
			</div>
			<div class="ThinBorder_MissingTop" style="DISPLAY: none"><asp:datalist id="ListGroups" runat="server" RepeatColumns="4">
					<HeaderTemplate>
						<asp:label id="Label11" runat="server" CssClass="XX-SmallBold">Groups Added</asp:label>
					</HeaderTemplate>
					<ItemTemplate>
						<%# MyReports.Common.Functions.ConvertFromBytes( DataBinder.Eval(Container.DataItem, "GroupName") ) %>
						&nbsp;
					</ItemTemplate>
				</asp:datalist></div>
		</td>
	</tr>
</table>
<script language="javascript">
<!--
function Image_Click(focusControl)
{			 				
	imageControl = focusControl.parentElement.firstChild;
	
	dataDiv = imageControl.parentElement.nextSibling;				
	
	if (dataDiv.style.display == "none")
	{
		dataDiv.style.display="block";
		imageControl.src="../images/minus.gif";
	}
	else
	{
		dataDiv.style.display="none";
		imageControl.src="../images/plus.gif";
	}				
}
//-->
</script>
