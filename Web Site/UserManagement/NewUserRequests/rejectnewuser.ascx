<%@ Register TagPrefix="uc1" TagName="PersonnelNumber" Src="../Common/PersonnelNumber.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RejectNewUser.ascx.cs" Inherits="UserManagement_NewUserRequests_RejectNewUser" %>
<table cellSpacing="0" cellPadding="0" width="90%" border="0">
	<tr>
		<td colspan="2">
			<asp:Label id="Label4" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small">Rejecter's ID: </asp:Label>&nbsp;
			<asp:Label id="lblRejectersID" runat="server" Font-Names="Verdana" Font-Size="X-Small">Schiaj01</asp:Label>&nbsp;&nbsp;&nbsp;
			<asp:Label id="Label3" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small">Date: </asp:Label>&nbsp;
			<asp:Label id="lblRejectedDate" runat="server" Font-Names="Verdana" Font-Size="X-Small">12/32/1977</asp:Label>
		</td>
	</tr>
	<tr>
		<td colspan="2"><br>
			<uc1:PersonnelNumber id="PersonnelNumber1" runat="server"></uc1:PersonnelNumber><br>
		</td>
	</tr>
	<tr>
		<td nowrap vAlign="top" align="left">
			<asp:Label id="lblRejectReason" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Size="X-Small">Reject Reason: </asp:Label>&nbsp;
		</td>
		<td width="100%">
			<asp:TextBox id="txtRejectReason" runat="server" Width="100%" Height="232px" MaxLength="900"
				TextMode="MultiLine"></asp:TextBox>
		</td>
	</tr>
</table>
<P>
	<asp:Button id="btnCancel" runat="server" Width="155px" CssClass="MyReportsAdminConsoleButton"
		Text="Cancel" onclick="btnCancel_Click"></asp:Button>
	<asp:Button id="btnSubmit" runat="server" Width="155px" CssClass="MyReportsAdminConsoleButton"
		Text="Submit" onclick="btnSubmit_Click"></asp:Button></P>
