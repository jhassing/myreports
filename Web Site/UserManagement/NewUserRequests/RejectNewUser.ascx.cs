using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class UserManagement_NewUserRequests_RejectNewUser : System.Web.UI.UserControl
{
    #region PageLoad

    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (Visible)
        {
            if (!Page.IsPostBack)
            {
                lblRejectersID.Text = MyReports.Common.AppValues.UserName;
                lblRejectedDate.Text = System.DateTime.Now.ToString("g");
            }
        }
    }

    #endregion

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        Return();
    }

    protected void btnSubmit_Click(object sender, System.EventArgs e)
    {
        NewUserFunctions.oNewUserBLL.InsertRejectReason(NewUserFunctions.PK_ID__URL_Decrypted, MyReports.Common.AppValues.UserName, txtRejectReason.Text);
        Return();
    }

    private void Return()
    {
        Response.Redirect("../index/index.aspx?app=user_management&individualview=0&id=" + NewUserFunctions.PK_ID__URL_Encrypted + "&done=" + NewUserFunctions.done, false);
    }
}