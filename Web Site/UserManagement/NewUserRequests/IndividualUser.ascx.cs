using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DistributionPortal.Components;

using Novell.Directory.Ldap;

using Compass.Utilities.Email;
using Compass.Security.ActiveDirectory;
using MyReports.Common;

public partial class UserManagement_NewUserRequests_IndividualUser : System.Web.UI.UserControl
{

    #region PageLoad

    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (Visible)
        {
            if (!Page.IsPostBack)
            {

                #region GetDetailRecords
                ArrayList ary = NewUserFunctions.oNewUserBLL.GetDetailRecords(NewUserFunctions.PK_ID__URL_Decrypted);
                DataGrid1.DataSource = ary;
                DataGrid1.DataBind();
                #endregion

                #region Get Group Membership
                DataSet dr = NewUserFunctions.oNewUserBLL.GetNewUserGroupMembership(NewUserFunctions.PK_ID__URL_Decrypted);
                ListGroups.DataSource = dr;
                ListGroups.DataBind();
                #endregion

                #region Get Personnel Number
                PersonnelNumber1.Personnel_Number = NewUserFunctions.oNewUserBLL.GetSamAccountName(NewUserFunctions.PK_ID__URL_Decrypted);
                #endregion

                drpOUListing.Items.Add(AppValues.NewUser_OU_Container);

                // Populate the Requestors Information
                GetRequestorsInformation();

                // Disable Reject reasons
                RejectPanel.Visible = false;
                lblRejectedNotice.Visible = false;

                // Disable the buttons
                btnImportUser.Visible = false;
                btnRejectUser.Visible = false;

                panLastModifiedBy.Visible = false;

                // Get Sam account name
                string samAccountName = NewUserFunctions.oNewUserBLL.GetSamAccountName(NewUserFunctions.PK_ID__URL_Decrypted);

                switch (QueryStringValues.getQueryStringValue("done"))
                {
                    case "0":
                        // PENDING IMPORT 

                        // Normal, non completed user import					

                        ShowImportSelections();
                        btnRejectUser.Visible = true;


                        break;

                    case "1":
                        // Display last user info
                        DisplayLastUserInfo();

                        // Display any reject history, if available
                        GetRejectHistory();
                        Label1.Text = "Last Modified By:";

                        break;

                    case "2":
                        // This user has been rejected.
                        GetRejectHistory();

                        // Display last user info
                        DisplayLastUserInfo();

                        lblRejectedNotice.Visible = true;

                        ShowImportSelections();
                        btnRejectUser.Visible = true;

                        Label1.Text = "Rejected By: ";

                        break;

                }
            }
        }


    }

    #endregion

    private void ShowImportSelections()
    {
        if (MyReports.Common.AppValues.IsInRole("Domain Admins") || ((MyReports.Common.AppValues.UserName.ToLower() == "schiaj01") || (MyReports.Common.AppValues.UserName.ToLower() == "schiaj02")))
        {
            btnImportUser.Visible = true;
            panImportOptions.Visible = true;
        }
    }

    private void GetRequestorsInformation()
    {
        string requestorsID = "";
        string requestorsEmail = "";
        string requestorsFirstName = "";
        string requestorsLastName = "";
        DateTime requestedDate;

        NewUserFunctions.oNewUserBLL.GetRequestorsInformation_From_Master(NewUserFunctions.PK_ID__URL_Decrypted,
                                                                                        out requestorsID,
                                                                                        out requestorsEmail,
                                                                                        out requestorsFirstName,
                                                                                        out requestorsLastName,
                                                                                        out requestedDate);

        lblRequestorsNetworkID.Text = requestorsID;
        lblRequestorsEmailAddress.Text = requestorsEmail;
        lblRequestorsFirstName.Text = requestorsFirstName;
        lblRequestorsLastName.Text = requestorsLastName;
        lblRequestedDate.Text = requestedDate.ToString();

    }

    private void GetRejectHistory()
    {
        // Display reject history
        SqlDataReader drRejectReasons = NewUserFunctions.oNewUserBLL.GetRejectReasons(NewUserFunctions.PK_ID__URL_Decrypted);

        if (drRejectReasons.HasRows)
        {
            RejectPanel.Visible = true;
            ListRejectReasons.DataSource = drRejectReasons;
            ListRejectReasons.DataBind();
        }
        else
        {
            RejectPanel.Visible = false;
        }
    }

    private void DisplayLastUserInfo()
    {

        panLastModifiedBy.Visible = true;

        IUserObject adHelper = null;
        LDAPConnection ldapConn;

        try
        {

            string networkID = "";
            string firstName = "";
            string lastName = "";
            string emailAddress = "";
            DateTime dtImportedDate;

            NewUserFunctions.oNewUserBLL.GetLastModifiedBy(NewUserFunctions.PK_ID__URL_Decrypted, out networkID, out dtImportedDate);
            ldapConn = new LDAPConnection(AppValues.ServiceAccountUser, AppValues.ServiceAccountPassword, AppValues.FullyQualifiedDomain, AppValues.LdapPort);

            ADUserObject oUser = new ADUserObject(ldapConn);

            // Go to AD, find this samAccountName
            if (adHelper.DoesUserExist(networkID, AppValues.LdapBaseOU))
            {
                ArrayList htName = new ArrayList();
                htName.Add("sn");
                htName.Add("mail");
                htName.Add("givenname");

                // get the first, last and email address of this user
                LdapAttributeSet ldapAttribSet = adHelper.GetUserProperties(networkID, htName);

                firstName = Functions.GetAttributeValue(ldapAttribSet, "givenname");
                lastName = Functions.GetAttributeValue(ldapAttribSet, "sn");
                emailAddress = Functions.GetAttributeValue(ldapAttribSet, "mail");
            }

            lblLastModifiedNetworkID.Text = networkID;
            lblLastModifiedDate.Text = dtImportedDate.ToString("g");
            lblLastModifiedEmailAddress.Text = emailAddress;
            lblLastModifiedFirstName.Text = firstName;
            lblLastModifiedLastName.Text = lastName;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            adHelper = null;
        }
    }

    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    /// <summary>
    ///		Required method for Designer support - do not modify
    ///		the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.DataGrid1.ItemDataBound += new System.Web.UI.WebControls.DataGridItemEventHandler(this.DataGrid1_ItemDataBound);

    }
    #endregion

    protected void btnImportUser_Click(object sender, System.EventArgs e)
    {

        // user does not exist. Add the user to the domain
        NewUserFunctions.oNewUserBLL.AddUserToActiveDirectory(
            NewUserFunctions.PK_ID__URL_Decrypted,
            AppValues.LdapBaseOU,
            this.drpOUListing.SelectedValue,
            this.chkEnableUserAccount.Checked,
            this.chkForcePasswordReset.Checked,
            this.chkSetPasswordToNeverExpire.Checked,
            this.chkOverrideExistingUserInfo.Checked);

        #region Email Requestor, letting him/her know the user was Imported.

        string from = "MyReports@Compass-USA.com";

        string requestorsID;
        string requestorsEmail;
        string requestorsFirstName;
        string requestorsLastName;
        DateTime requestedDate;

        NewUserFunctions.oNewUserBLL.GetRequestorsInformation_From_Master(NewUserFunctions.PK_ID__URL_Decrypted,
            out requestorsID,
            out requestorsEmail,
            out requestorsFirstName,
            out requestorsLastName,
            out requestedDate);

        string samAccountName = NewUserFunctions.oNewUserBLL.GetSamAccountName(NewUserFunctions.PK_ID__URL_Decrypted);

        string subject = Global.ResourceValue("NewUserAdded_Subject") + " Personnel Number: " + samAccountName;
        string message = Global.ResourceValue("NewUserAdded_Message");

        System.Net.Mail.MailAddressCollection macTO = new System.Net.Mail.MailAddressCollection();

        macTO.Add(requestorsEmail);

        EmailHelper.SendMail(   MyReports.Common.AppValues.SMTP,
                                subject,
                                message,
                                macTO,
                                new System.Net.Mail.MailAddress(from),
                                null,
                                null,
                                null,
                                System.Net.Mail.MailPriority.Normal,
                                true);

        #endregion

        Return();
    }

    protected void btnGoBack_Click(object sender, System.EventArgs e)
    {
        Return();
    }

    private void Return()
    {
        Response.Redirect("../index/index.aspx?app=user_management&userlisting=0&done=" + NewUserFunctions.done, false);
    }

    protected void btnRejectUser_Click(object sender, System.EventArgs e)
    {
        //Point user to another page, let them explain why we are rejecting this user

        Response.Redirect("../index/index.aspx?app=user_management&rejectuser=0&id=" + NewUserFunctions.PK_ID__URL_Encrypted + "&done=" + NewUserFunctions.done, false);
    }

    protected void DataGrid1_SelectedIndexChanged(object sender, System.EventArgs e)
    {

    }

    private void DataGrid1_ItemDataBound(object sender, System.Web.UI.WebControls.DataGridItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.AlternatingItem ||
                e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.SelectedItem)
        {
            Label lblAttributeName = (Label)e.Item.FindControl("AttributeName");
            Label lblAttributeValue = (Label)e.Item.FindControl("AttributeValue");

            switch (lblAttributeName.Text.ToLower())
            {

                case "description":
                    lblAttributeName.Text = "Description";
                    break;

                case "actuaterdhomefolder":
                    lblAttributeName.Text = "MyReports Home Folder";
                    break;

                case "givenname":
                    lblAttributeName.Text = "First Name";
                    break;

                case "department":
                    lblAttributeName.Text = "Department";
                    break;

                case "telephonenumber":
                    lblAttributeName.Text = "Telephone Number";
                    break;

                case "title":
                    lblAttributeName.Text = "Title";
                    break;

                case "sn":
                    lblAttributeName.Text = "Last Name";
                    break;

                case "company":
                    lblAttributeName.Text = "Company";
                    break;

                case "actuateuser":
                    lblAttributeName.Text = "Actuate User";
                    break;

            }
        }

    }

    protected void ListRejectReasons_SelectedIndexChanged(object sender, System.EventArgs e)
    {

    }
}