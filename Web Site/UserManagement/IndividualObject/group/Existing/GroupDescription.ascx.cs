using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserManagement_IndividualObject_group_Existing_GroupDescription : System.Web.UI.UserControl
{
    public string Description
    {
        get { return this.txtDescription.Text; }
        set { this.txtDescription.Text = value; }

    }

    public string GroupName
    {
        get { return this.txtGroupName.Text; }
        set { this.txtGroupName.Text = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
