<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GroupSource.aspx.cs" Inherits="UserManagement_IndividualObject_group_Existing_GroupSource" %>

<%@ Register Src="../../../Common/message.ascx" TagName="message" TagPrefix="uc2" %>

<%@ Register Src="../../../Common/rdgroups.ascx" TagName="rdgroups" TagPrefix="uc3" %>

<%@ Register Src="GroupDescription.ascx" TagName="GroupDescription" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <LINK href="../../../../css/allstyles.css" type="text/css" rel="stylesheet">	
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:GroupDescription ID="GroupDescription1" Visible="false" runat="server" />
        <uc3:rdgroups ID="Rdgroups1" runat="server" />
    
    <table style="PADDING-TOP: 5px" cellSpacing="0" cellPadding="0" width="100%" border="0" >
		<TR vAlign="bottom" height="100%">
		<TD vAlign="bottom" align="left" colSpan="1">
            <div>
	            <DIV style="HEIGHT: 2px">
		            <HR>
	            </DIV>
	            &nbsp;
	            <asp:button Width="80px" id="btnBack" runat="server" CssClass="MyReportsAdminConsoleButton "
		            Text="Back" CausesValidation="False"></asp:button>&nbsp;
	            <asp:button Width="80px" id="btnOk" runat="server" CssClass="MyReportsAdminConsoleButton" Text="Ok" onclick="btnOk_Click"></asp:button>&nbsp;
	            <asp:button id="btnApply" runat="server" Text="Apply" CssClass="MyReportsAdminConsoleButton"
		            Width="80px" OnClick="btnApply_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>                   
		</TD>
	</TR>
	</table>
    <uc2:message Visible="true" ID="Message1" runat="server" />
    </div>
    </form>
</body>
</html>
