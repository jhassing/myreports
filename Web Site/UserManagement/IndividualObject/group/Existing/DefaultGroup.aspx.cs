using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class UserManagement_IndividualObject_group_Existing_DefaultGroup_PAGE : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }

    protected string GetTabStripSrc
    {
        get
        {
            return "../../../../Common/TabStrip/TabStrip.aspx?TabContext=GroupObject&" + Request.QueryString.ToString();
        }

    }
}