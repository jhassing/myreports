<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupDescription.ascx.cs" Inherits="UserManagement_IndividualObject_group_Existing_GroupDescription" %>
<%@ Register Src="../../../../Common/myreportsgrouptype.ascx" TagName="myreportsgrouptype"TagPrefix="uc1" %>

<table border="0" cellpadding="0" cellspacing="1">
	<TR>
		<TD vAlign="top" noWrap align="right" height="1">
			<asp:label id="Label6" runat="server" CssClass="X-SmallBold">Group Name:</asp:label></TD>
		<TD style="WIDTH: 14px" vAlign="top" noWrap align="left" width="14">
			<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" EnableClientScript="true" ControlToValidate="txtGroupName"
				ErrorMessage="*"></asp:requiredfieldvalidator></TD>
		<TD vAlign="top" align="left" width="100%">
			<asp:TextBox id="txtGroupName" runat="server" CssClass="X-SmallTextBox" Width="40%"
				MaxLength="30"></asp:TextBox></TD>
	</TR>
	<TR>
		<TD vAlign="top" align= "right" height="1">
			<asp:label id="Label1" runat="server" CssClass="X-SmallBold">Description:</asp:label></TD>
		<TD style="WIDTH: 14px" vAlign="top" noWrap align="left" width="14"></TD>
		<TD vAlign="top" align="left" width="100%">
			<asp:TextBox id="txtDescription" runat="server" CssClass="X-SmallTextBox" Width="40%" 
				MaxLength="30"></asp:TextBox></TD>
	</TR>
	<TR>
		<TD vAlign="top" noWrap align="left" height="100%">
			<asp:label id="Label2" runat="server" CssClass="X-Small">Report Type:</asp:label></TD>
		<TD style="WIDTH: 14px" vAlign="top" noWrap align="left" width="14"></TD>
		<TD vAlign="top" align="left" width="100%">&nbsp;&nbsp;		
            <uc1:myreportsgrouptype ID="Myreportsgrouptype2" runat="server" />
        </TD>
	</TR>
</TABLE>
