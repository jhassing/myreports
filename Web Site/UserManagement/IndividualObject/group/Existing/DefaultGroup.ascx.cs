using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Compass.Security.ActiveDirectory;	   
using MyReports.Common;		
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using DistributionPortal.Components;

using Novell.Directory.Ldap;

using MyReports.UserManagementUI.Common;
using MyReports.Components;

using UserManagement.BLL;
using DistributionPortal;

public partial class UserManagement_IndividualObject_group_Existing_DefaultGroup : IndividualObjectBaseClass
{

    #region Properties

    public string getTabID
    {
        get
        {
            string tabID = "";

            if (Request.QueryString["tabID"] != null)
                tabID = Request.QueryString["tabID"].ToString();
            else
                tabID = "groupdescription";

            return tabID;
        }
    }

    #region CurrentGroupMembership

    public ArrayList CurrentGroupMembership
    {
        get
        {
            Groups oGroups = null;
            ArrayList ary = new ArrayList();

            oGroups = new Groups(UserManagement.BLL.AuthObject.GetAuthObject);

            // Get the current groups members
            ArrayList obList = oGroups.GetGroupMembership(this.DistinguishedName);

            foreach (LdapAttributeSet ldapAttribSet in obList)
            {
                ListItem at = new ListItem(Functions.GetAttributeValue(ldapAttribSet, "cn"),
                    Functions.GetAttributeValue(ldapAttribSet, "distinguishedName"));
                ary.Add(at);
            }

            return ary;
        }

    }

    #endregion

    #endregion

    #region PreRender

    protected override void OnPreRender(EventArgs e)
    {
 	    base.OnPreRender(e);

        try
        {
            if (Visible)
            {
                if (!Page.IsPostBack)
                {
                    // Show the options on the first tab
                    this.MyReportsGroupType1.Display_BlueBook_Option = MyReports.Common.AppValues.IsInRole("BlueBookDistributionAdmins");
                    this.MyReportsGroupType1.Display_Financial_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin");
                    this.MyReportsGroupType1.Display_Hourly_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin");
                    this.MyReportsGroupType1.Display_Salary_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin");

                    // Hide all permission group based options on the 2nd tab
                    this.RDGroups1.Display_Financial_Option = false;
                    this.RDGroups1.Display_BlueBook_Option = false;
                    this.RDGroups1.Display_Hourly_Option = false;
                    this.RDGroups1.Display_Salary_Option = false;

                    btnBack.Attributes.Add("onClick", "Return();");

                    Page.RegisterHiddenField("retValueFromQuestion", "");

                    // Save button javascript
                    //StringBuilder sbSave = new StringBuilder();

                    //sbSave.Append("if (typeof(Page_ClientValidate) == 'function') { ");
                    //sbSave.Append("if (Page_ClientValidate() == false) { return false; }} ");

                    //sbSave.Append("document.all." + this.btnSave.ClientID + ".disabled = true;");
                    //sbSave.Append("document.all." + this.btnBack.ClientID + ".disabled = true;");
                    //sbSave.Append("javascript:Save();");
                    //sbSave.Append(this.Page.GetPostBackEventReference(this.btnSave) + ";");

                    //btnSave.Attributes.Add("OnClick", sbSave.ToString() );				

                    // Back button javascript
                    //StringBuilder sbBack = new StringBuilder();
                    //sbBack.Append("document.all." + this.btnSave.ClientID + ".disabled = true;");
                    //sbBack.Append("document.all." + this.btnBack.ClientID + ".disabled = true;");
                    //sbBack.Append(this.Page.GetPostBackEventReference(this.btnBack) + ";");

                    //btnBack.Attributes.Add("OnClick", sbBack.ToString() );	

                    WriteSaveScript();

                    // Depending on which tab is clicked, show the proper panel
                    //int selectedTab = ViewState["SelectedTab"] != null ? System.Convert.ToInt32( ViewState["SelectedTab"].ToString() ) : 0;

                    switch (this.getTabID)
                    {
                        case "groupdescription":		// Group edit page
                            this.panGroupDescription.Visible = true;
                            this.panMembershipListing.Visible = false;
                            break;

                        case "membership":		// Group membership page
                            this.panGroupDescription.Visible = false;
                            this.panMembershipListing.Visible = true;                            
                            break;
                    }

                    LoadData();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region Page Load

    protected override void OnLoad(System.EventArgs e)
    {

        base.OnLoad(e);

        if (!Page.IsPostBack)
        {            
            this.MyReportsGroupType1.Visible = false;            

            this.Message1.HideMessage();

            if (Request.QueryString.Get("addnew") == null)
            {
                this.txtGroupName.Enabled = false;
            }
            else
            {
                this.txtGroupName.Enabled = (System.Convert.ToBoolean(Request.QueryString.Get("addnew").ToString()));
            }
        }

        this.RDGroups1.ListSearch_OnClick += new Delegates.SearchButtonClicked(RDGroups1_ListSearch_OnClick);
    }

    #endregion  

    #region WriteSaveScript

    protected override void WriteSaveScript()
    {
        if (!Page.IsClientScriptBlockRegistered("Save-Script Function"))
        {
            string hh = getOpenScriptTag();

            hh += "\t function Save() { \r\n";

            // Verify the Group name field has some text in it
            // Ask the user if they are SURE they want to save this group into AD.
            hh += QuestionDialogScript("Header Text", "Are you sure you wish to add this group into AD?", "Save", "Cancel");
            hh += "\r\n";
            hh += "\t\t } \r\n";

            hh += getCloseScriptTag();

            Page.RegisterClientScriptBlock("Save-Script Function", hh);

        }
    }


    #endregion

    #region Back Button clicked

    private void btnBack_Click(object sender, System.EventArgs e)
    {
        DoReturn();
    }


    #endregion

    #region Save Data

    protected override bool SaveData()
    {
        Groups oGroups = null;

        try
        {
            // Ask user to confirm this addition, then save to AD..
            this.Message1.HideMessage();

            switch (this.getTabID)
            {
                case "groupdescription":	// save the group information to AD
                    #region Modify existing group

                    try
                    {
                        oGroups = new Groups(UserManagement.BLL.AuthObject.GetAuthObject);

                        oGroups.ModifyGroup(this.DistinguishedName,
                            this.txtDescription.Text.Trim());

                        return true;
                    }
                    catch (Exception ex)
                    {
                        // Trap adding a new group error 
                        Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex);
                        this.Message1.ShowMessage(Global.ResourceValue("GroupModificationFailed") + " " + ex.Message, System.Drawing.Color.Red);

                        return false;

                    }

                    #endregion
                    break;

                case "membership":	// Edit the group membership, and save to AD
                    return SaveGroupMembershipListing();
                    break;
            }

            return false;

        }
        catch (Exception ex)
        {
            Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex);
            this.Message1.ShowMessage(Global.ResourceValue("GroupModificationFailed") + " " + ex.Message, System.Drawing.Color.Red);

            return false;
        }
        finally
        {
            oGroups = null;
        }

    }

    #endregion

    #region Save Group Membership Listing

    private bool SaveGroupMembershipListing()
    {

        Groups oGroups = null;

        try
        {
            this.Message1.HideMessage();

            oGroups = new Groups(UserManagement.BLL.AuthObject.GetAuthObject);

            oGroups.SaveGroupMembership(this.DistinguishedName, this.RDGroups1.ObjectsAdded, this.RDGroups1.ObjectsRemoved);

            this.Message1.ShowMessage(Global.ResourceValue("GroupMembershipUpdatedSuccessfully"), System.Drawing.Color.Green);

            return true;
        }
        catch (Exception ex)
        {
            Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex);
            this.Message1.ShowMessage(Global.ResourceValue("GroupMembershipUpdateFailed") + " " + ex.Message, System.Drawing.Color.Red);

            return false;
        }
        finally
        {
            oGroups = null;
        }
    }


    #endregion

    #region Load Group Information

    protected override void LoadData()
    {
        Groups oGroups = null;
        ArrayList ary = new ArrayList();

        try
        {
            oGroups = new Groups(UserManagement.BLL.AuthObject.GetAuthObject);

            switch (this.getTabID)
            {
                case "groupdescription":

                    // Name and Description
                    string dn = Request.QueryString.Get("distinguishedName").ToString();

                    string des = "";
                    string name = "";

                    oGroups.GetGroupDescription(dn, out name, out des);
                    this.txtDescription.Text = des;
                    this.txtGroupName.Text = name;
                    
                    break;

                case "membership":
                    // Populate the selected membership 
                    this.RDGroups1.SelectedList = oGroups.GetGroupMembership(this.DistinguishedName);

                    break;
            }
        }
        catch (Exception ex)
        {
            Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex);
            this.Message1.ShowMessage(ex.Message, System.Drawing.Color.Red);
        }
        finally
        {
            oGroups = null;
        }
    }

    #endregion

    #region Membership search button clicked

    private void RDGroups1_ListSearch_OnClick()
    {
        // Load the Available list with users.			

        Users oUsers = null;

        try
        {
            this.Message1.HideMessage();

            oUsers = new Users();

            //List<tUserListing> retUserListing = oUsers.GetUsersBasedOnANR(this.RDGroups1.Filter + "*", AppValues.LdapBaseOU);

            this.RDGroups1.AvailableList = oUsers.GetUserListingForSearch(this.RDGroups1.Filter + "*", AppValues.LdapBaseOU, (int)UserManagement.BLL.BaseClass.enumSearchScope.SCOPE_SUB, AppValues.MaxLDAPsearchResuts);

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oUsers = null;
        }
    }


    #endregion

    #region Tab Clicked

    protected override void Tab_OnClick(int tabClicked)
    {
        this.Message1.HideMessage();

        switch (tabClicked)
        {
            case 0:
                break;

            case 1:
                this.RDGroups1.AvailableList = new ArrayList();
                break;
        }
    }


    #endregion

    #region Delete the group

    private void BottomButtons1_DeleteButton_OnClick()
    {
        // Delete the group

        Groups oGroups = null;


        try
        {
            // Delete the group from AD.
            this.Message1.HideMessage();

            oGroups = new Groups(UserManagement.BLL.AuthObject.GetAuthObject);

            oGroups.DeleteGroup(this.txtGroupName.Text);

            DoReturn();

        }
        catch (Exception ex)
        {
            Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex);
            this.Message1.ShowMessage(Global.ResourceValue("ErrorDeletingGroup") + " " + ex.Message, System.Drawing.Color.Red);

        }
        finally
        {
            oGroups = null;
        }
    }

    #endregion

    protected void btnOk_Click(object sender, EventArgs e)
    {
        SaveData();
    }
    protected void btnApply_Click(object sender, EventArgs e)
    {
        SaveData();
    }
}