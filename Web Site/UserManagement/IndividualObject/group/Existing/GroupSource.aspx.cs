using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Compass.Security.ActiveDirectory;
using MyReports.Common;
using System.IO;
using System.Collections;
using System.Text;

using DistributionPortal.Components;

using Novell.Directory.Ldap;

using MyReports.UserManagementUI.Common;
using MyReports.Components;

using UserManagement.BLL;
using DistributionPortal;

public partial class UserManagement_IndividualObject_group_Existing_GroupSource : baseIndividualObject
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Message1.HideMessage();
        this.Rdgroups1.ListSearch_OnClick += new Delegates.SearchButtonClicked(RDGroups1_ListSearch_OnClick);
        
    }

    #region PreRender



    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        
        try
        {
            if (Visible)
            {
                if (!Page.IsPostBack)
                {

                    //LoadData();

                    //string personnelNumber = this.PersonnelNumber;
                    //string homeFolder = this.HomeFolder;

                    GroupDescription1.Visible = false;
                    Rdgroups1.Visible = false;

                    //btnBack.Attributes.Add("onClick", "Return();");

                    switch (this.getTabID)
                    {
                        case "groupdescription":
                            #region Show Locate User Control

                            this.GroupDescription1.Visible = true;

                            break;
                            #endregion

                        case "membership":
                            #region Show Edit User Membership Control

                            Rdgroups1.Visible = true;

                             break;
                            #endregion                          

                    }

                    LoadData();
                }
            }
        }
        catch (Exception ex)
        {
            Message1.DisplayError(ex);
        }
    }

    #endregion

    #region Load Group Information

    protected void LoadData()
    {
        Groups oGroups = null;
        ArrayList ary = new ArrayList();

        try
        {
            oGroups = new Groups(UserManagement.BLL.AuthObject.GetAuthObject);

            switch (this.getTabID)
            {
                case "groupdescription":

                    // Name and Description                   
                    string des = "";
                    string name = "";

                    oGroups.GetGroupDescription(this.DistinguishedName, out name, out des);

                    this.GroupDescription1.Description = des;
                    this.GroupDescription1.GroupName = name;

                    break;

                case "membership":
                    // Populate the selected membership 
                    this.Rdgroups1.SelectedList = oGroups.GetGroupMembership(this.DistinguishedName);

                    break;
            }
        }
        catch (Exception ex)
        {
            Message1.DisplayError(ex);
        }
        finally
        {
            oGroups = null;
        }
    }

    #endregion

    #region Membership search button clicked

    private void RDGroups1_ListSearch_OnClick()
    {
        // Load the Available list with users.			

        Users oUsers = null;

        try
        {
            //this.Message1.HideMessage();
            oUsers = new Users();

            this.Rdgroups1.AvailableList = oUsers.GetUserListingForSearch(this.Rdgroups1.Filter + "*", AppValues.LdapBaseOU, (int)UserManagement.BLL.BaseClass.enumSearchScope.SCOPE_SUB, AppValues.MaxLDAPsearchResuts);

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oUsers = null;
        }
    }


    #endregion

    protected void btnOk_Click(object sender, EventArgs e)
    {
        SaveData();
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        SaveData();
    }

    #region Save Data

    protected bool SaveData()
    {
        Groups oGroups = null;

        try
        {
            // Ask user to confirm this addition, then save to AD..
            //this.Message1.HideMessage();

            switch (this.getTabID)
            {
                case "groupdescription":	// save the group information to AD
                    #region Modify existing group

                    try
                    {
                        oGroups = new Groups(UserManagement.BLL.AuthObject.GetAuthObject);

                        oGroups.ModifyGroup(this.DistinguishedName, this.GroupDescription1.Description.Trim());

                        return true;
                    }
                    catch (Exception ex)
                    {
                        // Trap adding a new group error 
                        //Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex);
                        //this.Message1.ShowMessage(Global.ResourceValue("GroupModificationFailed") + " " + ex.Message, System.Drawing.Color.Red);
                        Message1.DisplayError(ex);

                        return false;

                    }

                    #endregion
                    break;

                case "membership":	// Edit the group membership, and save to AD
                    return SaveGroupMembershipListing();
                    break;
            }

            return false;

        }
        catch (Exception ex)
        {
            Message1.DisplayError(ex);

            return false;
        }
        finally
        {
            oGroups = null;
        }

    }

    #endregion

    #region Save Group Membership Listing

    private bool SaveGroupMembershipListing()
    {

        Groups oGroups = null;

        try
        {
            //this.Message1.HideMessage();

            oGroups = new Groups(UserManagement.BLL.AuthObject.GetAuthObject);

            oGroups.SaveGroupMembership(this.DistinguishedName, this.Rdgroups1.ObjectsAdded, this.Rdgroups1.ObjectsRemoved);

            //this.Message1.ShowMessage(Global.ResourceValue("GroupMembershipUpdatedSuccessfully"), System.Drawing.Color.Green);

            return true;
        }
        catch (Exception ex)
        {
            //Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex);
            //this.Message1.ShowMessage(Global.ResourceValue("GroupMembershipUpdateFailed") + " " + ex.Message, System.Drawing.Color.Red);
            Message1.DisplayError(ex);

            return false;
        }
        finally
        {
            oGroups = null;
        }
    }


    #endregion
}
