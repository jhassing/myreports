<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefaultGroup.ascx.cs" Inherits="UserManagement_IndividualObject_group_Existing_DefaultGroup" %>
<%@ Register TagPrefix="uc1" TagName="Message" Src="../../../Common/Message.ascx" %>
<%@ Register TagPrefix="uc1" TagName="BottomButtons" Src="../../../Common/BottomButtons.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="AegisControls" Assembly="AegisControls" %>
<%@ Register TagPrefix="uc2" TagName="MyReportsGroupType" Src="../../../../Common/MyReportsGroupType.ascx" %>
<%@ Register TagPrefix="uc1" TagName="RDGroups" Src="../../../Common/RDGroups.ascx" %>

<table style="PADDING-TOP: 5px" height="100%" cellSpacing="0" cellPadding="0" width="100%"
	align="right" border="0" >
	<tr>
		<td class="" style="WIDTH: 100%" vAlign="top" height="100%">
			<!-- Start display of group and description information -->
			<asp:panel id="panGroupDescription" Visible="False" Runat="server">
				<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" align="right" border="0">
					<TR>
						<TD vAlign="top" noWrap align="right" height="1">
							<asp:label id="Label6" runat="server" CssClass="X-SmallBold">Group Name:</asp:label></TD>
						<TD style="WIDTH: 14px" vAlign="top" noWrap align="left" width="14">
							<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" EnableClientScript="true" ControlToValidate="txtGroupName"
								ErrorMessage="*"></asp:requiredfieldvalidator></TD>
						<TD vAlign="top" align="left" width="100%">
							<cc1:maskedtextbox id="txtGroupName" runat="server" CssClass="X-SmallTextBox" Width="40%" Format="Text"
								MaxLength="30"></cc1:maskedtextbox></TD>
					</TR>
					<TR>
						<TD vAlign="top" align= "right" height="1">
							<asp:label id="Label1" runat="server" CssClass="X-SmallBold">Description:</asp:label></TD>
						<TD style="WIDTH: 14px" vAlign="top" noWrap align="left" width="14"></TD>
						<TD vAlign="top" align="left" width="100%">
							<cc1:maskedtextbox id="txtDescription" runat="server" CssClass="X-SmallTextBox" Width="40%" Format="Text"
								MaxLength="30"></cc1:maskedtextbox></TD>
					</TR>
					<TR>
						<TD vAlign="top" noWrap align="left" height="100%">
							<asp:label id="Label2" runat="server" CssClass="X-Small">Report Type:</asp:label></TD>
						<TD style="WIDTH: 14px" vAlign="top" noWrap align="left" width="14"></TD>
						<TD vAlign="top" align="left" width="100%">&nbsp;&nbsp;
							<uc2:MyReportsGroupType id="MyReportsGroupType1" runat="server"></uc2:MyReportsGroupType></TD>
					</TR>
				</TABLE>
			</asp:panel>
			<!-- End display of group and description information -->
			<!-- Start display of group membership --><asp:panel id="panMembershipListing" Visible="False" Runat="server">
				<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" align="right" border="0"
					>
					<TR>
						<TD vAlign="top">
							<uc1:RDGroups id="RDGroups1" runat="server"></uc1:RDGroups></TD>
					</TR>
				</TABLE>
			</asp:panel>
			<!-- End display of group membership --></td>
	</tr>
	<TR vAlign="bottom" height="100%">
		<TD vAlign="bottom" align="left" colSpan="1">
            <div>
	            <DIV style="HEIGHT: 2px">
		            <HR>
	            </DIV>
	            &nbsp;
	            <asp:button Width="80px" id="btnBack" runat="server" CssClass="MyReportsAdminConsoleButton "
		            Text="Back" CausesValidation="False"></asp:button>&nbsp;
	            <asp:button Width="80px" id="btnOk" runat="server" CssClass="MyReportsAdminConsoleButton" Text="Ok" onclick="btnOk_Click"></asp:button>&nbsp;
	            <asp:button id="btnApply" runat="server" Text="Apply" CssClass="MyReportsAdminConsoleButton"
		            Width="80px"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>                   
		</TD>
	</TR>
	<tr>
		<td>&nbsp;<uc1:Message id="Message1" runat="server"></uc1:Message></td>
	</tr>
</table>

<script language=javascript>

    function Return()
    {         
        document.parentWindow.parent.location = "../../../UserManagementDefault.aspx?app=user_management&ListGroups=0";      
    }
</script>   