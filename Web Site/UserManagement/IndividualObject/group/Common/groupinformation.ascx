<%@ Register TagPrefix="cc1" Namespace="AegisControls" Assembly="AegisControls" %>
<%@ Control Language="c#" Inherits="MyReports.UserManagementUI.IndividualObject.Group.Common.GroupInformation" CodeFile="GroupInformation.ascx.cs" %>
<table cellSpacing="1" cellPadding="1" width="90%" border="0">
	<TR>
		<TD style="PADDING-LEFT: 5px" vAlign="top" noWrap align="left" height="1">
			<asp:label id="Label6" runat="server" CssClass="XX-SmallBold">Group Name:</asp:label></TD>
		<TD style="WIDTH: 14px" vAlign="top" noWrap align="left" width="14">
			<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" EnableClientScript="true" ControlToValidate="txtGroupName"
				ErrorMessage="*"></asp:requiredfieldvalidator></TD>
		<TD vAlign="top" align="left" width="100%">
			<cc1:maskedtextbox id="txtGroupName" runat="server" CssClass="XX-SmallTextBox" Width="40%" Format="Text"
				MaxLength="30"></cc1:maskedtextbox></TD>
	</TR>
	<TR>
		<TD style="PADDING-LEFT: 5px" vAlign="top" noWrap align="left" height="1">
			<asp:label id="Label1" runat="server" CssClass="XX-SmallBold">Description:</asp:label></TD>
		<TD style="WIDTH: 14px" vAlign="top" noWrap align="left" width="14"></TD>
		<TD vAlign="top" align="left" width="100%">
			<cc1:maskedtextbox id="txtDescription" runat="server" CssClass="XX-SmallTextBox" Width="40%" Format="Text"
				MaxLength="30"></cc1:maskedtextbox></TD>
	</TR>
	<TR>
		<TD colSpan="3"><BR>
		</TD>
	</TR>
	<TR>
		<TD style="PADDING-LEFT: 5px" vAlign="top" noWrap align="left" height="1">
			<asp:label id="Label2" runat="server" CssClass="XX-SmallBold">Group scope:</asp:label></TD>
		<TD style="WIDTH: 14px" vAlign="top" noWrap align="left" width="14"></TD>
		<TD vAlign="top" noWrap align="left" height="1">
			<asp:label id="Label3" runat="server" CssClass="XX-SmallBold">Group type:</asp:label></TD>
	</TR>
	<TR>
		<TD style="PADDING-LEFT: 5px" noWrap>
			<asp:RadioButton id="rdoDomainLocal" runat="server" CssClass="XX-Small" Text="Domain Local" GroupName="GroupScope"></asp:RadioButton><BR>
			<asp:RadioButton id="rdoGlobal" runat="server" CssClass="XX-Small" Text="Global" GroupName="GroupScope"
				Checked="True"></asp:RadioButton><BR>
			<asp:RadioButton id="rdoUniversal" runat="server" CssClass="XX-Small" Text="Universal" GroupName="GroupScope"></asp:RadioButton><BR>
		</TD>
		<TD style="WIDTH: 14px" vAlign="top" noWrap align="left" width="14"></TD>
		<TD vAlign="top" noWrap align="left">
			<asp:RadioButton id="rdoSecurity" runat="server" CssClass="XX-Small" Text="Security" GroupName="GroupType"
				Checked="True"></asp:RadioButton><BR>
			<asp:RadioButton id="rdoDistribution" runat="server" CssClass="XX-Small" Text="Distribution" GroupName="GroupType"></asp:RadioButton><BR>
		</TD>
	</TR>
</table>
