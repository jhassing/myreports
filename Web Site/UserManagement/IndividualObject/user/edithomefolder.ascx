<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditHomeFolder.ascx.cs" Inherits="UserManagement_IndividualObject_user_EditHomeFolder" %>

<%@ Register TagPrefix="uc1" TagName="HomeFolders" Src="../../Common/HomeFolders.ascx" %>

<table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
	<tr>
		<td noWrap>&nbsp;&nbsp;
			<asp:Label id="Label1" runat="server" CssClass="XX-SmallBold">Current Home Folder:</asp:Label>
			<asp:textbox CssClass="XX-Small" id="txtCurrentHomeFolders" runat="server" Width="99%" ReadOnly="True"></asp:textbox>
			<br>
			<hr style="MARGIN-LEFT: 18px; MARGIN-RIGHT: 28px">
		</td>
	</tr>
	<tr>
		<td>
			<uc1:HomeFolders id="HomeFolders1" runat="server"></uc1:HomeFolders>
		</td>
	</tr>
</table>
