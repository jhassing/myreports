using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

public partial class UserManagement_IndividualObject_user_EditHomeFolder : System.Web.UI.UserControl
{

    #region Properties


    public ArrayList HomeFolderListing
    {
        set
        {
            HomeFolders1.HomeFolderListing = value;
        }
    }

    public string HomeFolder
    {
        set
        {
            this.txtCurrentHomeFolders.Text = value;
        }
        get
        {
            if (HomeFolders1.MainSelection.ToLower() == "specific")
                return HomeFolders1.HomeFolder.Trim();
            else
                return HomeFolders1.SubSelection.Trim();
        }
    }


    #endregion

}