using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;

using MyReports.Common;
using UserManagement.BLL;

public partial class UserManagement_IndividualObject_user_GroupMembershipTab : System.Web.UI.UserControl
{
    #region Properties

    public bool BudgetDistributionUser
    {
        get
        {
            return lblBudgetDistributionUser.Visible;
        }
        set
        {
            lblBudgetDistributionUser.Visible = value;
        }
    }

    public bool MyReportsUser
    {
        get
        {
            return lblMyReportsUser.Visible;
        }
        set
        {
            lblMyReportsUser.Visible = value;
        }
    }

    public bool PassportUser
    {
        get
        {
            return this.lblPassportUser.Visible;
        }
        set
        {
            lblPassportUser.Visible = value;
        }
    }

    public bool BlueBookDistributionUser
    {
        get
        {
            return this.lblBlueBookDistributionUser.Visible;
        }
        set
        {
            lblBlueBookDistributionUser.Visible = value;
        }
    }

    public string PersonnelNumber
    {
        get
        {
            return this.txtPernaNumber.Text;
        }
        set
        {
            this.txtPernaNumber.Text = value;
        }
    }

    public ArrayList UserMembership
    {
        get
        {
            return (ArrayList)lstGroups.DataSource;
        }
        set
        {
            lstGroups.DataSource = value;
            lstGroups.DataBind();
        }
    }

    public string HomeFolder
    {
        get
        {
            return txtHomeFolders.Text;
        }
        set
        {
            txtHomeFolders.Text = value;
        }
    }

    #endregion

}