using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections;
using System.Text;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;

using UserManagement.BLL;

using MyReports.UserManagementUI.Common;
using MyReports.Components;
using MyReports.Common;

using Compass.Security.ActiveDirectory;
using Novell.Directory.Ldap;

using Microsoft.ApplicationBlocks.ExceptionManagement;

public partial class UserManagement_IndividualObject_user_UserSource : baseIndividualObject
{
    #region Properties

    #region GroupListing

    public ArrayList GroupListing
    {
        get
        {
            if (Session["UserMembership"] != null)
            {
                return (ArrayList)Session["UserMembership"];
            }
            else
                return new ArrayList();
        }
        set
        {
            Session["UserMembership"] = value;
        }
    }

    #endregion

    #region Personnel Number

    public string PersonnelNumber
    {
        get
        {
            return ViewState["personnelNumber"] != null ? ViewState["personnelNumber"].ToString() : "";
        }
        set
        {
            ViewState["personnelNumber"] = value;
        }

    }

    #endregion

    #region Home Folder

    public string HomeFolder
    {
        get
        {
            return ViewState["HomeFolder"] != null ? ViewState["HomeFolder"].ToString() : "";
        }
        set
        {
            ViewState["HomeFolder"] = value;
        }
    }


    #endregion

    #region MyReports User

    public bool MyReportsUser
    {
        get
        {
            return ViewState["MyReportsUser"] != null ? System.Convert.ToBoolean(ViewState["MyReportsUser"].ToString()) : false;
        }
        set
        {
            ViewState["MyReportsUser"] = value;
        }
    }


    #endregion

    #region BudgetDIstributionUser

    public bool BudgetDistributionUser
    {
        get
        {
            return ViewState["BudgetDistributionUser"] != null ? System.Convert.ToBoolean(ViewState["BudgetDistributionUser"].ToString()) : false;
        }
        set
        {
            ViewState["BudgetDistributionUser"] = value;
        }
    }


    #endregion

    #region Passport User

    public bool PassportUser
    {
        get
        {
            return ViewState["PassportUser"] != null ? System.Convert.ToBoolean(ViewState["PassportUser"].ToString()) : false;
        }
        set
        {
            ViewState["PassportUser"] = value;
        }
    }


    #endregion

    #region BlueBookDistributionUser

    public bool BlueBookDistributionUser
    {
        get
        {
            return ViewState["BlueBookDistributionUser"] != null ? System.Convert.ToBoolean(ViewState["BlueBookDistributionUser"].ToString()) : false;
        }
        set
        {
            ViewState["BlueBookDistributionUser"] = value;
        }
    }


    #endregion

    #region MyFactsUser

    public bool MyFactsUser
    {
        get
        {
            return ViewState["MyFactsUser"] != null ? System.Convert.ToBoolean(ViewState["MyFactsUser"].ToString()) : false;
        }
        set
        {
            ViewState["MyFactsUser"] = value;
        }
    }
 

    #endregion

    #region MupFinAdmin

    public bool MupFinAdmin
    {
        get
        {
            return ViewState["MupFinAdmin"] != null ? System.Convert.ToBoolean(ViewState["MupFinAdmin"].ToString()) : false;
        }
        set
        {
            ViewState["MupFinAdmin"] = value;
        }
    }


    #endregion

    #region MupPayAdmin

    public bool MupPayAdmin
    {
        get
        {
            return ViewState["MupPayAdmin"] != null ? System.Convert.ToBoolean(ViewState["MupPayAdmin"].ToString()) : false;
        }
        set
        {
            ViewState["MupPayAdmin"] = value;
        }
    }


    #endregion

    #endregion

    #region OnLoad

    protected override void OnLoad(System.EventArgs e)
    {
        base.OnLoad(e);

        this.Message1.HideMessage();
        QueryStringValue.Value = Request.QueryString.ToString().Trim() + "&ListUsers=0";

        // Custom Events	
        this.EditGroupMembership1.ListSearch_OnClick += new Delegates.SearchButtonClicked(EditGroupMembership1_ListSearch_OnClick);
    }

    #endregion

    #region PreRender

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        StringBuilder sbA = new StringBuilder();

        try
        {
            if (Visible)
            {
                if (!Page.IsPostBack)
                {

                    LoadData();

                    string personnelNumber = this.PersonnelNumber;
                    string homeFolder = this.HomeFolder;

                    EditHomeFolder1.Visible = false;
                    GroupMembership1.Visible = false;
                    EditGroupMembership1.Visible = false;
                    EditApplicationAccess1.Visible = false;

                    btnBack.Attributes.Add("onClick", "Return();");

                    switch (this.getTabID)
                    {
                        case "membership":
                            #region Show Locate User Control

                            this.GroupMembership1.Visible = true;

                            this.GroupMembership1.PersonnelNumber = personnelNumber;
                            this.GroupMembership1.HomeFolder = homeFolder;

                            this.GroupMembership1.MyReportsUser = this.MyReportsUser;
                            this.GroupMembership1.BudgetDistributionUser = this.BudgetDistributionUser;
                            this.GroupMembership1.PassportUser = this.PassportUser;
                            this.GroupMembership1.BlueBookDistributionUser = this.BlueBookDistributionUser;

                            this.GroupMembership1.UserMembership = this.GroupListing;

                            break;

                            #endregion

                        case "editmembership":
                            #region Show Edit User Membership Control

                            EditGroupMembership1.Visible = true;

                            // Load the group membership
                            EditGroupMembership1.SelectedList = this.GroupListing;

                            #region Button client side script

                            // javascript event click for the save button
                            // disable all the buttons on the screen
                            sbA.Append("javascript:DisableGroupButtons();");

                            // Hide the message for the next few seconds
                            sbA.Append("javascript:ShowHideMessage();");

                            // disable all the tabs.
                            sbA.Append("javascript:DisableTabs();");

                            // disable this button
                            sbA.Append("this.disabled=true;");


                            #endregion

                            break;

                            #endregion

                        case "edithomefolder":
                            #region Show Edit Home Folder Control

                            try
                            {
                                EditHomeFolder1.Visible = true;

                                EditHomeFolder1.HomeFolder = homeFolder;

                                EditHomeFolder1.HomeFolderListing = new GroupMembershipBLL(AppValues.MyReports_DB_ConnectionString).getHomeFolderListing(GetDisplayGroupsBasedOnPermissions(this.GroupListing));

                            }
                            catch (Exception ex)
                            {
                                System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();
                                nv.Add("Personnel Number", this.PersonnelNumber);

                                ExceptionManager.Publish(ex, nv);
                                Message1.ShowMessage(ex.Message, System.Drawing.Color.Red);
                            }

                            #region Button client side script

                            // javascript event click for the save button

                            // Hide the message for the next few seconds
                            sbA.Append("javascript:ShowHideMessage();");

                            // disable all the tabs.
                            sbA.Append("javascript:DisableTabs();");

                            // disable this button
                            sbA.Append("this.disabled=true;");


                            #endregion

                            break;
                            #endregion

                        case "editapplicationaccess":
                            #region Show Edit User Program Access Control

                            this.EditApplicationAccess1.Visible = true;

                            this.EditApplicationAccess1.CheckBox_BudgetDistribution.Checked = this.BudgetDistributionUser;
                            this.EditApplicationAccess1.CheckBox_MyReports.Checked = this.MyReportsUser;
                            this.EditApplicationAccess1.CheckBox_Passport.Checked = this.PassportUser;
                            this.EditApplicationAccess1.CheckBox_BlueBookDistribution.Checked = this.BlueBookDistributionUser;
                            this.EditApplicationAccess1.CheckBox_MyFacts.Checked = this.MyFactsUser;
                            this.EditApplicationAccess1.CheckBox_MupFinAdmin.Checked = this.MupFinAdmin;
                            this.EditApplicationAccess1.CheckBox_MupPayAdmin.Checked = this.MupPayAdmin;

                            #region Button client side script

                            // javascript event click for the save button

                            // Hide the message for the next few seconds
                            sbA.Append("javascript:ShowHideMessage();");

                            // disable all the tabs.
                            sbA.Append("javascript:DisableTabs();");

                            // disable this button
                            sbA.Append("this.disabled=true;");


                            #endregion

                            break;

                            #endregion

                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region Load User Information from AD

    protected void LoadData()
    {

        ArrayList myAL1 = new ArrayList();

        Message1.HideMessage();

        Users oUsers = null;

        try
        {
            oUsers = new Users(UserManagement.BLL.AuthObject.GetAuthObject);

            UserManagement.BLL.tUser userObj = oUsers.GetUserProperties(this.DistinguishedName);

            this.PersonnelNumber = userObj.PersonnelNumber;
            this.BudgetDistributionUser = userObj.BudgetDistributionUser;
            this.MyReportsUser = userObj.ActuateUser;
            this.HomeFolder = userObj.HomeFolder;
            this.PassportUser = userObj.PassportUser;
            this.BlueBookDistributionUser = userObj.BlueBookDistributionUser;
            this.MyFactsUser = userObj.MyFactsUser;
            this.MupFinAdmin = userObj.MupFinAdmin;
            this.MupPayAdmin = userObj.MupPayAdmin;

            switch (this.getTabID)
            {
                case "membership":
                    #region Show Locate User Control

                    this.GroupListing = oUsers.UsersGroupMembership(userObj.UserMembership);
 
                    break;
                    #endregion

                case "editmembership":
                    #region Show Edit User Membership Control

                    this.GroupListing = oUsers.UsersGroupMembership(userObj.UserMembership);

                    break;
                    #endregion

                case "edithomefolder":
                    #region Show Edit Home Folder Control

                    break;
                    #endregion

                case "editapplicationaccess":
                    #region Show Edit User Program Access Control


                    break;
                    #endregion

            }
        }
        catch (Exception ex)
        {
            this.Message1.DisplayError(ex);
        }
        finally
        {
            oUsers = null;
        }
    }


    #endregion

    #region Save group membership

    private bool ModifyUserMembership()
    {
        Users oUsers = null;

        try
        {
            oUsers = new Users(UserManagement.BLL.AuthObject.GetAuthObject);

            this.GroupListing = oUsers.EditUserMembership(base.DistinguishedName, EditGroupMembership1.ObjectsAdded, EditGroupMembership1.ObjectsRemoved);

            EditGroupMembership1.SelectedList = this.GroupListing;

            // Clear the Objects Added and Objects Removed
            EditGroupMembership1.ObjectsAdded = null;
            EditGroupMembership1.ObjectsRemoved = null;

            Message1.ShowMessage(Global.ResourceValue("GroupMembershipUpdatedSuccessfully"), System.Drawing.Color.Green);

            return true;
        }
        catch (Exception ex)
        {
            System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();
            nv.Add("Personnel Number", this.PersonnelNumber);

            ExceptionManager.Publish(ex, nv);

            Message1.ShowMessage(Global.ResourceValue("GroupMembershipUpdateFailed") + " " + ex.Message, System.Drawing.Color.Red);

            return false;
        }
        finally
        {
            oUsers = null;

            // Clear the search text box
            this.EditGroupMembership1.Filter = "";

            // clear the left group listing.
            //EditGroupMembership1.SearchGroups = new ArrayList();

        }
    }


    #endregion

    #region Save Home Folder

    private bool ModifyHomeFolders()
    {
        Users oUsers = null;

        string HomeFolder = EditHomeFolder1.HomeFolder;

        try
        {
            oUsers = new Users(UserManagement.BLL.AuthObject.GetAuthObject);

            oUsers.SetNewHomeFolder(this.DistinguishedName, HomeFolder);

            // Display the home folder at the top of the screen
            GroupMembership1.HomeFolder = HomeFolder;
            this.HomeFolder = HomeFolder;

            this.Message1.ShowMessage(Global.ResourceValue("HomeFolderUpdatedSuccessfully"), System.Drawing.Color.Green);

            return true;

        }
        catch (Exception ex)
        {
            System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();
            nv.Add("Distinguished Name", this.DistinguishedName);

            ExceptionManager.Publish(ex, nv);

            this.Message1.ShowMessage(Global.ResourceValue("HomeFolderUpdateFailed") + " " + ex.Message, System.Drawing.Color.Red);

            return false;
        }
        finally
        {
            oUsers = null;
        }
    }

    #endregion

    #region Save Application Access

    private bool SaveApplicationAccessData()
    {

        Users oUsers = null;
        DirectoryEntry deUser = null;

        try
        {
            string dn = this.DistinguishedName;

            deUser = new DirectoryEntry("LDAP://" + AppValues.FullyQualifiedDomain + "/" + dn, 
                                                    AppValues.ServiceAccountUser, 
                                                    AppValues.ServiceAccountPassword, 
                                                    AuthenticationTypes.Secure);


            // Has myreports access changed
            if (this.MyReportsUser != this.EditApplicationAccess1.CheckBox_MyReports.Checked)
            {
                // Access has changed, update
                this.MyReportsUser = this.EditApplicationAccess1.CheckBox_MyReports.Checked;

                string myReportsAccess = this.MyReportsUser.ToString().ToUpper() == "TRUE" ? "TRUE" : "FALSE";

                #region Enable MyReports access

                if (deUser.Properties["ActuateUser"].Count == 0)
                {
                    deUser.Properties["ActuateUser"].Add(myReportsAccess);
                }
                else
                {
                    deUser.Properties["ActuateUser"][0] = myReportsAccess;
                }

                #endregion
            }



            if (this.BudgetDistributionUser != this.EditApplicationAccess1.CheckBox_BudgetDistribution.Checked)
            {
                if (oUsers == null)
                    oUsers = new Users(UserManagement.BLL.AuthObject.GetAuthObject);

                // Access has changed
                this.BudgetDistributionUser = this.EditApplicationAccess1.CheckBox_BudgetDistribution.Checked;
                oUsers.SetBudgetDistributionAccess(this.DistinguishedName, AppValues.LdapBaseOU, this.BudgetDistributionUser);

            }

            if (this.PassportUser != this.EditApplicationAccess1.CheckBox_Passport.Checked)
            {
                if (oUsers == null)
                    oUsers = new Users(UserManagement.BLL.AuthObject.GetAuthObject);

                // Access has changed
                this.PassportUser = this.EditApplicationAccess1.CheckBox_Passport.Checked;
                oUsers.SetPassportAccess(this.DistinguishedName, AppValues.LdapBaseOU, this.PassportUser);
            }

            if (this.BlueBookDistributionUser != this.EditApplicationAccess1.CheckBox_BlueBookDistribution.Checked)
            {
                if (oUsers == null)
                    oUsers = new Users(UserManagement.BLL.AuthObject.GetAuthObject);

                // Access has changed
                this.BlueBookDistributionUser = this.EditApplicationAccess1.CheckBox_BlueBookDistribution.Checked;
                oUsers.SetBlueBookDistributionAccess(this.DistinguishedName, AppValues.LdapBaseOU, this.BlueBookDistributionUser);
            }

            if (this.MyFactsUser != EditApplicationAccess1.CheckBox_MyFacts.Checked)
            {
                if (oUsers == null)
                    oUsers = new Users(UserManagement.BLL.AuthObject.GetAuthObject);

                // Access has changed
                this.MyFactsUser = this.EditApplicationAccess1.CheckBox_MyFacts.Checked;
                oUsers.SetMyFactsAccess(this.DistinguishedName, AppValues.LdapBaseOU, this.MyFactsUser);
            }

            if (this.MupFinAdmin != this.EditApplicationAccess1.CheckBox_MupFinAdmin.Checked)
            {
                if (oUsers == null)
                    oUsers = new Users(UserManagement.BLL.AuthObject.GetAuthObject);

                // Access has changed
                this.MupFinAdmin = this.EditApplicationAccess1.CheckBox_MupFinAdmin.Checked;
                oUsers.SetMupFinAdmin(this.DistinguishedName, AppValues.LdapBaseOU, this.MupFinAdmin);
            }

            if (this.MupPayAdmin != this.EditApplicationAccess1.CheckBox_MupPayAdmin.Checked)
            {
                if (oUsers == null)
                    oUsers = new Users(UserManagement.BLL.AuthObject.GetAuthObject);

                // Access has changed
                this.MupPayAdmin = this.EditApplicationAccess1.CheckBox_MupPayAdmin.Checked;
                oUsers.SetMupPayAdmin(this.DistinguishedName, AppValues.LdapBaseOU, this.MupPayAdmin);
            }

            // Commit the changes
            deUser.CommitChanges();

            // Display message
            this.Message1.ShowMessage("Application Access Updated Successfully", System.Drawing.Color.Green);

            return true;
        }
        catch (Exception ex)
        {
            System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();
            nv.Add("Distinguished Name", this.DistinguishedName);

            ExceptionManager.Publish(ex, nv);

            this.Message1.ShowMessage( "Application Access Update Failed" + " " + ex.Message, System.Drawing.Color.Red);

            return false;

        }
        finally
        {
            oUsers = null;

            if (deUser != null)
            {
                deUser.Close();
                deUser.Dispose();
                deUser = null;
            }
        }

    }
    #endregion

    #region Save Data

    protected bool SaveData()
    {
        Message1.HideMessage();

        try
        {
            // Depending on which tab is selected, save the needed data.
            switch (this.getTabID)
            {
                case "membership":	// Group Membership tab.. Do nothing
                    return true;

                case "editmembership":	// Edit Membership
                    return ModifyUserMembership();

                case "edithomefolder":	// Edit Home Folder
                    return ModifyHomeFolders();

                case "editapplicationaccess":	// Edit Application Access
                    return SaveApplicationAccessData();

            }

            return false;
        }
        catch (Exception ex)
        {
            System.Collections.Specialized.NameValueCollection nv = new System.Collections.Specialized.NameValueCollection();
            nv.Add("Personnel Number", this.PersonnelNumber);

            ExceptionManager.Publish(ex, nv);

            Message1.ShowMessage(ex.Message, System.Drawing.Color.Red);

            return false;
        }
    }

    #endregion

    #region Edit group membership search button click

    /// <summary>
    /// Function will get the current selected users group membership, popluate the selected box and allow for editing.
    /// </summary>
    private void EditGroupMembership1_ListSearch_OnClick()
    {
        Groups oGroups = null;

        try
        {
            oGroups = new Groups(UserManagement.BLL.AuthObject.GetAuthObject);

            string filter = "(|";

            if (this.EditGroupMembership1.Financial_Option_Checked)
                filter += "(samAccountName=" + this.EditGroupMembership1.Filter + "*" + AppValues.FinancialGroups + ")";

            if (this.EditGroupMembership1.Salary_Option_Checked)
                filter += "(samAccountName=" + this.EditGroupMembership1.Filter + "*" + AppValues.PayrollSalaryGroups + ")";

            if (this.EditGroupMembership1.Hourly_Option_Checked)
                filter += "(samAccountName=" + this.EditGroupMembership1.Filter + "*" + AppValues.PayrollHourlyGroups + ")";

            if (this.EditGroupMembership1.BlueBook_Option_Checked)
                filter += "(samAccountName=" + this.EditGroupMembership1.Filter + "*" + AppValues.BlueBookGroups + ")";

            filter += ")";

            // Get the list of members in the particulat group
            this.EditGroupMembership1.AvailableList = oGroups.GetGroupListing(filter, AppValues.LdapBaseOU, (int)UserManagement.BLL.BaseClass.enumSearchScope.SCOPE_SUB, AppValues.MaxLDAPsearchResuts);

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oGroups = null;
        }
    }

    #endregion

    private ArrayList GetDisplayGroupsBasedOnPermissions(ArrayList tCurrentListing)
    {
        ArrayList ary = new ArrayList();
        ArrayList aryRemovedGroups = new ArrayList();

        foreach (System.Web.UI.WebControls.ListItem tGroup in tCurrentListing)
        {

            string dn = tGroup.Value;
            string group = tGroup.Text;

            System.Web.UI.WebControls.ListItem at = new System.Web.UI.WebControls.ListItem(group, dn);

            if ((group.EndsWith(AppValues.PayrollHourlyGroups.ToLower())) ||
                (group.EndsWith(AppValues.PayrollSalaryGroups.ToLower())))
            {
                if (MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin"))
                    ary.Add(at);
                else
                    aryRemovedGroups.Add(at);
            }
            else if (group.EndsWith(AppValues.FinancialGroups.ToLower()))
            {
                if (MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin"))
                    ary.Add(at);
                else
                    aryRemovedGroups.Add(at);
            }
            else
                ary.Add(at);

        }

        return ary;
    }

    private ArrayList GetRemovedGroupsBasedOnPermissions(ArrayList tCurrentListing)
    {
        ArrayList aryRemovedGroups = new ArrayList();

        foreach (LdapAttributeSet tGroup in tCurrentListing)
        {

            string dn = Functions.GetAttributeValue(tGroup, "distinguishedName");
            string group = Functions.GetAttributeValue(tGroup, "cn");

            System.Web.UI.WebControls.ListItem at = new System.Web.UI.WebControls.ListItem(group, dn);

            if ((group.EndsWith(AppValues.PayrollHourlyGroups.ToLower())) ||
                (group.EndsWith(AppValues.PayrollSalaryGroups.ToLower())))
            {
                if (!MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin"))
                    aryRemovedGroups.Add(at);
            }
            else if (group.EndsWith(AppValues.FinancialGroups.ToLower()))
            {
                if (!MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin"))
                    aryRemovedGroups.Add(at);
            }

        }

        return aryRemovedGroups;
    }

    protected void btnOk_Click(object sender, EventArgs e)
    {
        SaveData();
    }
    protected void btnApply_Click(object sender, EventArgs e)
    {
        SaveData();
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {

    }
}
