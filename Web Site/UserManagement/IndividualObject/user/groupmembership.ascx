<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupMembership.ascx.cs" Inherits="UserManagement_IndividualObject_user_GroupMembershipTab" %>
<table height="90" cellSpacing="3" cellPadding="3" width="90%" border="0">
	<tr>
		<td noWrap>
			<asp:Label id="Label10" runat="server" CssClass="XX-SmallBold">Personnel Number (8 Digits):</asp:Label>
		</td>
		<td vAlign="top" align="left">
			<asp:textbox CssClass="XX-Small" id="txtPernaNumber" runat="server" MaxLength="8" Width="80px"
				ReadOnly="True"></asp:textbox></td>
		<td noWrap>
			<asp:Label id="Label1" runat="server" CssClass="XX-SmallBold">Home Folder:</asp:Label>
		</td>
		<td vAlign="top" align="left" width="100%">&nbsp;
			<asp:textbox CssClass="XX-Small" id="txtHomeFolders" runat="server" Width="99%" ReadOnly="True"></asp:textbox></td>
	</tr>
	<tr>
		<td nowrap>
			<asp:Label id="Label3" runat="server" CssClass="XX-SmallBold">Applications:</asp:Label>
		</td>
		<td colspan="3" noWrap>
			<asp:Label id="Label2" runat="server" CssClass="XX-SmallBold">User Membership:</asp:Label>
		</td>
	</tr>
	<tr>
		<td nowrap vAlign="top" align="left">
			<asp:Label ID="lblMyReportsUser" Runat="server">My Reports User</asp:Label><br>
			<asp:Label ID="lblBudgetDistributionUser" Runat="server">Budget Distribution User</asp:Label><br>
			<asp:Label ID="lblPassportUser" Runat="server">Passport User</asp:Label><br>
			<asp:Label ID="lblBlueBookDistributionUser" Runat="server">Blue Book Distribution User</asp:Label><br>
		</td>
		<td colspan="3" nowrap width="100%">
			<asp:listbox id="lstGroups" runat="server" Width="100%" Rows="12"></asp:listbox></td>
	</tr>
	<tr>
		<td align="right" colspan="4">
		</td>
	</tr>
</table>
