using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class UserManagement_IndividualObject_user_EditApplicationAccess : System.Web.UI.UserControl
{
    #region Properties

    public System.Web.UI.WebControls.CheckBox CheckBox_MyReports
    {
        get
        {
            return ApplicationAccess1.CheckBox_MyReports;
        }
        set
        {
            ApplicationAccess1.CheckBox_MyReports = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_BudgetDistribution
    {
        get
        {
            return ApplicationAccess1.CheckBox_BudgetDistribution;
        }
        set
        {
            ApplicationAccess1.CheckBox_BudgetDistribution = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_Passport
    {
        get
        {
            return ApplicationAccess1.CheckBox_Passport;
        }
        set
        {
            ApplicationAccess1.CheckBox_Passport = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_BlueBookDistribution
    {
        get
        {
            return ApplicationAccess1.CheckBox_BlueBookDistribution;
        }
        set
        {
            ApplicationAccess1.CheckBox_BlueBookDistribution = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_MupFinAdmin
    {
        get
        {
            return ApplicationAccess1.CheckBox_MupFinAdmin;
        }
        set
        {
            ApplicationAccess1.CheckBox_MupFinAdmin = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_MupPayAdmin
    {
        get
        {
            return ApplicationAccess1.CheckBox_MupPayAdmin;
        }
        set
        {
            ApplicationAccess1.CheckBox_MupPayAdmin = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_MyFacts
    {
        get 
        {
            return ApplicationAccess1.CheckBox_MyFacts;
        }
        set
        {
            ApplicationAccess1.CheckBox_MyFacts = value;
        }
    }
    #endregion
}
