using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text;

public partial class UserManagement_IndividualObject_user_EditGroupMembership : System.Web.UI.UserControl
{

    public event Delegates.SearchButtonClicked ListSearch_OnClick;

    #region Properties


    // The search filter text entered by the end user
    #region Filter
    public string Filter
    {
        get
        {
            return this.RDGroups1.Filter.Trim();
        }
        set
        {
            this.RDGroups1.Filter = value;
        }
    }

    #endregion

    // Gets and Sets the Available Listbox contents
    #region AvailableList
    public ArrayList AvailableList
    {
        get
        {
            return this.RDGroups1.AvailableList;
        }
        set
        {
            this.RDGroups1.AvailableList = value;
        }
    }

    #endregion

    // Gets and Sets the Selected listbox contents
    #region SelectedList
    public ArrayList SelectedList
    {
        get
        {
            return this.RDGroups1.SelectedList;
        }
        set
        {
            this.RDGroups1.SelectedList = value;
        }
    }

    #endregion

    // Objects Added
    #region ObjectsAdded
    public ArrayList ObjectsAdded
    {
        get
        {
            return this.RDGroups1.ObjectsAdded;
        }
        set
        {
            this.RDGroups1.ObjectsAdded = value;
        }
    }

    #endregion

    // Objects Added
    #region ObjectsRemoved
    public ArrayList ObjectsRemoved
    {
        get
        {
            return this.RDGroups1.ObjectsRemoved;
        }
        set
        {
            this.RDGroups1.ObjectsRemoved = value;
        }
    }

    #endregion

    public bool Financial_Option_Checked
    {
        get
        {
            return this.RDGroups1.Financial_Option_Checked;

        }
        set
        {
            this.RDGroups1.Financial_Option_Checked = value;
        }
    }


    public bool Salary_Option_Checked
    {
        get
        {
            return this.RDGroups1.Salary_Option_Checked;
        }
        set
        {
            this.RDGroups1.Salary_Option_Checked = value;

        }
    }


    public bool Hourly_Option_Checked
    {
        get
        {
            return this.RDGroups1.Hourly_Option_Checked;
        }
        set
        {
            this.RDGroups1.Hourly_Option_Checked = value;
        }
    }


    public bool BlueBook_Option_Checked
    {
        get
        {
            return this.RDGroups1.BlueBook_Option_Checked;
        }
        set
        {
            this.RDGroups1.BlueBook_Option_Checked = value;
        }
    }




    #endregion

    #region PreRender

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Visible)
        {
            this.RDGroups1.Display_BlueBook_Option = MyReports.Common.AppValues.IsInRole("BlueBookDistributionAdmins");
            this.RDGroups1.Display_Financial_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin");
            this.RDGroups1.Display_Hourly_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin");
            this.RDGroups1.Display_Salary_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin");
        }
    }
    #endregion

    #region RDGroups1_ListSearch_OnClick

    protected void RDGroups1_ListSearch_OnClick()
    {
        if (ListSearch_OnClick != null)
            ListSearch_OnClick();
    }

    #endregion
    
}