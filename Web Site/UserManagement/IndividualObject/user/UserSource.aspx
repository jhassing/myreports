<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserSource.aspx.cs" Inherits="UserManagement_IndividualObject_user_UserSource" %>

<%@ Reference Control="~/usermanagement/common/personnelnumber.ascx" %>
<%@ Register TagPrefix="uc1" TagName="EditGroupMembership" Src="EditGroupMembership.ascx" %>
<%@ Register TagPrefix="uc1" TagName="EditHomeFolder" Src="EditHomeFolder.ascx" %>
<%@ Register TagPrefix="uc1" TagName="GroupMembership" Src="GroupMembership.ascx" %>
<%@ Register TagPrefix="uc1" TagName="EditApplicationAccess" Src="EditApplicationAccess.ascx" %>
<%@ Register TagPrefix="uc1" TagName="tabStip" Src="../../../Common/tabStip.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Message" Src="../../Common/Message.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <LINK href="../../../css/allstyles.css" type="text/css" rel="stylesheet">	
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%"  align="right" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td style="WIDTH: 100%" height="100%" class="">
			<TABLE height="100%" cellSpacing="0" cellPadding="1" width="100%" align="right" border="0">
				<tr vAlign="top">
					<td vAlign="top">
						<uc1:GroupMembership id="GroupMembership1" runat="server" Visible="False"></uc1:GroupMembership>
						<uc1:EditHomeFolder id="EditHomeFolder1" runat="server" Visible="False"></uc1:EditHomeFolder>
						<uc1:EditGroupMembership id="EditGroupMembership1" runat="server" Visible="False"></uc1:EditGroupMembership>
						<uc1:EditApplicationAccess id="EditApplicationAccess1" runat="server" Visible="False"></uc1:EditApplicationAccess>
					</td>
				</tr>
	            <TR vAlign="top" height="100%">
		            <TD vAlign="bottom" align="left" colSpan="1">
                        <div>
	                        <DIV style="HEIGHT: 2px">
		                        <HR>
	                        </DIV>
	                        &nbsp;<br />
	                        <asp:button Width="80px" id="btnBack" runat="server" CssClass="MyReportsAdminConsoleButton "
		                        Text="Back" CausesValidation="False" OnClick="btnBack_Click"></asp:button>&nbsp;
	                        <asp:button Width="80px" id="btnOk" runat="server" CssClass="MyReportsAdminConsoleButton" Text="Ok" onclick="btnOk_Click"></asp:button>&nbsp;
	                        <asp:button id="btnApply" runat="server" Text="Apply" CssClass="MyReportsAdminConsoleButton"
		                        Width="80px" OnClick="btnApply_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>                   
		            </TD>
	            </TR>
				<tr>
					<td>&nbsp;
						<uc1:Message id="Message1" runat="server"></uc1:Message>
					</td>
				</tr>
			</TABLE>
		</td>
	</tr>
</table>

 <asp:HiddenField ID="QueryStringValue" runat="server" Value="" />
 
<script language=javascript>

    function Return()
    {         
        document.parentWindow.parent.location = "../../UserManagementDefault.aspx?" + document.form1.Defaultuser1_QueryStringValue.value;      
    }
</script>      
    </div>
    </form>
</body>
</html>