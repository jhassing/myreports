using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class UserManagement_NewMyReportsUser_RDAddNewUser_Step3 : NewUserBaseClass
{
    #region Properties

    public System.Web.UI.WebControls.CheckBox CheckBox_MyReports
    {
        get
        {
            return this.ApplicationAccess1.CheckBox_MyReports;
        }
        set
        {
            this.ApplicationAccess1.CheckBox_MyReports = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_BudgetDistribution
    {
        get
        {
            return this.ApplicationAccess1.CheckBox_BudgetDistribution;
        }
        set
        {
            this.ApplicationAccess1.CheckBox_BudgetDistribution = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_Passport
    {
        get
        {
            return this.ApplicationAccess1.CheckBox_Passport;
        }
        set
        {
            this.ApplicationAccess1.CheckBox_Passport = value;
        }
    }

    public System.Web.UI.WebControls.CheckBox CheckBox_BlueBookDistribution
    {
        get
        {
            return this.ApplicationAccess1.CheckBox_BlueBookDistribution;
        }
        set
        {
            this.ApplicationAccess1.CheckBox_BlueBookDistribution = value;
        }
    }

    #endregion
}