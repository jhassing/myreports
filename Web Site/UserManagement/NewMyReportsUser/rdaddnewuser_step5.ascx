<%@ Register TagPrefix="uc1" TagName="HomeFolders" Src="../Common/HomeFolders.ascx" %>

<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RDAddNewUser_Step5.ascx.cs" Inherits="UserManagement_NewMyReportsUser_RDAddNewUser_Step5" %>

<table class="MyReportsAdminConsoleBorder" width="100%" height="100%" valign="top" align="right">
	<tr>
		<td valign="top" align="center" colspan="2">
			<asp:Label id="Label2" runat="server" CssClass="X-SmallBold">Home Folder&nbsp;Information 
				</asp:Label></td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="middle"><hr>
		</td>
	</tr>
	<tr>
		<td width="100%" valign="top" align="left"><uc1:HomeFolders id="HomeFolders1" runat="server"></uc1:HomeFolders>
			</td>
	</tr>
	<tr align="right" valign="bottom" height="100%">
		<td colspan="2">
		    <asp:button CommandName="Step" CommandArgument="4" id="btnNextStep" runat="server" Text="<< Step 4" CssClass="MyReportsAdminConsoleButton"></asp:button>
			<asp:Button CommandName="Step" CommandArgument="6" id="btnSubmit" runat="server" Text="Review" CssClass="MyReportsAdminConsoleButton"></asp:Button>
		</td>
	</tr>
</table>
