<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RDAddNewUser_Step4.ascx.cs" Inherits="UserManagement_NewMyReportsUser_RDAddNewUser_Step4" %>

<%@ Register TagPrefix="uc1" TagName="RDGroups" Src="../Common/RDGroups.ascx" %>
<table class="MyReportsAdminConsoleBorder" width="100%" height="100%" align="right">
	<tr>
		<td valign="top" align="center" colspan="2">
			<asp:Label id="Label2" runat="server" CssClass="X-SmallBold">Group 
				Information</asp:Label></td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="middle"><hr>
		</td>
	</tr>
	<tr>
		<td width="100%" valign="top" align="left"><uc1:RDGroups id="RDGroups1" runat="server"></uc1:RDGroups>
			</td>
	</tr>
	<tr height="100%" valign="bottom" align="right">
		<td>
			<asp:button CommandName="Step" CommandArgument="3" id="btnNextStep" runat="server" Text="<< Step 3" CssClass="MyReportsAdminConsoleButton" ></asp:button>
			<asp:Button CommandName="Step" CommandArgument="5" id="btnStep4" runat="server" Text="Step 5 >>" CssClass="MyReportsAdminConsoleButton" ></asp:Button>
		</td>
	</tr>
</table>
