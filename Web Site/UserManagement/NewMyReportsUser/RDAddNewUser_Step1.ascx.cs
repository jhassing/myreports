using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;


using Novell.Directory.Ldap;
using Compass.Security.ActiveDirectory;

using UserManagement.BLL;
using MyReports.Common;

public partial class UserManagement_NewMyReportsUser_RDAddNewUser_Step1 : NewUserBaseClass
{

    #region LoadUsersInfo

    public void LoadUsersInfo()
    {
        LDAPConnection ldapConn = new LDAPConnection(AppValues.ServiceAccountUser,
                                                        AppValues.ServiceAccountPassword,
                                                        AppValues.FullyQualifiedDomain,
                                                        AppValues.LdapPort);


        IUserObject oADUtilities = new ADUserObject(ldapConn);

        ArrayList ht = new ArrayList();
        ht.Add("givenname");
        ht.Add("sn");
        ht.Add("mail");
        ht.Add("TelephoneNumber");
        ht.Add("Department");

        LdapAttributeSet ldapAttribSet = oADUtilities.GetUserProperties(AppValues.distinguishedUserName, ht);

        this.txtRequestorsFirstName.Text = Functions.GetAttributeValue(ldapAttribSet, "givenname");
        this.txtRequestorsLastName.Text = Functions.GetAttributeValue(ldapAttribSet, "sn");
        this.txtRequestorsEmail.Text = Functions.GetAttributeValue(ldapAttribSet, "mail");
        this.txtRequestorsPhone.Text = Functions.GetAttributeValue(ldapAttribSet, "TelephoneNumber");
        this.txtRequestorsDepartment.Text = Functions.GetAttributeValue(ldapAttribSet, "Department");
    }

    #endregion

    #region Properties

    public string RequestorsFirstName
    {
        get { return this.txtRequestorsFirstName.Text; }
        set { this.txtRequestorsFirstName.Text = value; }
    }

    public string RequestorsLastName
    {
        get { return this.txtRequestorsLastName.Text; }
        set { this.txtRequestorsLastName.Text = value; }
    }

    public string RequestorsEmail
    {
        get { return this.txtRequestorsEmail.Text; }
        set { this.txtRequestorsEmail.Text = value; }
    }

    public string RequestorsPhone
    {
        get { return this.txtRequestorsPhone.Text; }
        set { this.txtRequestorsPhone.Text = value; }
    }

    public string RequestorsDepartment
    {
        get { return this.txtRequestorsDepartment.Text; }
        set { this.txtRequestorsDepartment.Text = value; }
    }

    #endregion

}