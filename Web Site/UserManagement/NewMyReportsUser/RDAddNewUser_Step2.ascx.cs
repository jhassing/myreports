using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UserManagement.BLL;
using MyReports.Common;

public partial class UserManagement_NewMyReportsUser_RDAddNewUser_Step2 : NewUserBaseClass
{
    #region Properties

    public string AssociatesPernaNumber
    {
        get { return this.txtAssociatesPernaNumber.Text; }
        set { this.txtAssociatesPernaNumber.Text = value; }
    }

    public string AssociatesFirstName
    {
        get { return this.txtAssociatesFirstName.Text; }
        set { this.txtAssociatesFirstName.Text = value; }
    }

    public string AssociatesLastName
    {
        get { return this.txtAssociatesLastName.Text; }
        set { this.txtAssociatesLastName.Text = value; }
    }

    public string AssociatesLocation
    {
        get { return this.drpAssociatesLocation.SelectedValue; }
    }

    public string AssociatesDepartment
    {
        get { return this.txtAssociatesDepartment.Text; }
        set { this.txtAssociatesDepartment.Text = value; }
    }

    public string AssociatesCompany
    {
        get { return this.drpAssociatesCompany.SelectedValue; }
    }

    public string AssociatesPhone
    {
        get { return this.txtAssociatesPhone.Text; }
        set { this.txtAssociatesPhone.Text = value; }
    }

    public string AssociatesTitle
    {
        get { return this.txtAssociatesTitle.Text; }
        set { this.txtAssociatesTitle.Text = value; }
    }

    #endregion
}