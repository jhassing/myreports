using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class UserManagement_NewMyReportsUser_SideBar : System.Web.UI.UserControl
{
 
    #region PageLoad

    protected void Page_Load(object sender, System.EventArgs e)
    {
        string sStep = Request.QueryString["step"];

        switch (sStep)
        {

            case "1":
                this.panStep1.CssClass = "SelectedStep";
                break;

            case "2":
                panStep2.CssClass = "SelectedStep";
                break;

            case "3":
                this.panStep3.CssClass = "SelectedStep";
                break;

            case "4":
                this.panStep4.CssClass = "SelectedStep";
                break;

            case "5":
                this.panStep5.CssClass = "SelectedStep";
                break;

            case "6":
                this.panStep6.CssClass = "SelectedStep";
                break;

            case "7":
                this.panStep7.CssClass = "SelectedStep";
                break;

            default:
                this.panStep1.CssClass = "SelectedStep";
                break;


        }

    }

    #endregion

}