using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;

using Compass.Security.ActiveDirectory;
using Compass.Utilities.Email;

using MyReports.Common;

using Microsoft.ApplicationBlocks.ExceptionManagement;

public partial class UserManagement_NewMyReportsUser_NewMyReportsUser : System.Web.UI.UserControl
{

    #region OnLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.lblMessage.Visible = false;
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }

    #endregion

    #region DisplayError

    private void DisplayError(Exception ex)
    {
        this.lblMessage.Visible = true;
        string mess = ex.Message;
        mess += "\r\n";

        if (ex.InnerException != null)
            mess += ex.InnerException.Message;

        this.lblMessage.Text = mess;

        ExceptionManager.Publish(ex);
    }

    #endregion

    #region Start

    public void Start()
    {
        HideScreens();
        this.rdaddnewuser_step1.Visible = true;
        this.rdaddnewuser_step1.LoadUsersInfo();
        ClearScreens();
    }

    #endregion

    #region ClearScreens

    private void ClearScreens()
    {
        // Clear Step 2
        this.rdaddnewuser_step2.AssociatesPernaNumber = "";
        this.rdaddnewuser_step2.AssociatesFirstName = "";
        this.rdaddnewuser_step2.AssociatesLastName = "";
        //this.rdaddnewuser_step2.AssociatesLocation = "";
        this.rdaddnewuser_step2.AssociatesDepartment = "";
        //this.rdaddnewuser_step2.AssociatesCompany = "";
        this.rdaddnewuser_step2.AssociatesPhone = "";
        this.rdaddnewuser_step2.AssociatesTitle = "";

        // Clear Step 3
        this.rdaddnewuser_step3.CheckBox_MyReports.Checked = false;
        this.rdaddnewuser_step3.CheckBox_BudgetDistribution.Checked = false;
        this.rdaddnewuser_step3.CheckBox_Passport.Checked = false;
        this.rdaddnewuser_step3.CheckBox_BlueBookDistribution.Checked = false;

        // Clear Step 4
        this.rdaddnewuser_step4.SelectedList = new ArrayList();
        this.rdaddnewuser_step4.AvailableList = new ArrayList();
        this.rdaddnewuser_step4.Filter = "";

        // Clear Step 5
        this.rdaddnewuser_step5.AssociatesHomeFolder = "";
        this.rdaddnewuser_step5.SubSelection = "";
        this.rdaddnewuser_step5.AssociatesHomeFolder = "";

        // Clear Step 6
    }

    #endregion

    #region HideScreens

    private void HideScreens()
    {
        this.rdaddnewuser_step1.Visible = false;
        this.rdaddnewuser_step2.Visible = false;
        this.rdaddnewuser_step3.Visible = false;
        this.rdaddnewuser_step4.Visible = false;
        this.rdaddnewuser_step5.Visible = false;
        this.rdaddnewuser_step6.Visible = false;
        this.rdaddnewuser_step7.Visible = false;

    }

    #endregion

    #region ShowStep

    private void ShowStep(int step)
    {
        HideScreens();

        switch (step)
        {
            case 1:
                this.rdaddnewuser_step1.Visible = true;
                break;

            case 2:
                this.rdaddnewuser_step2.Visible = true;
                break;

            case 3:
                this.rdaddnewuser_step3.Visible = true;
                break;

            case 4:
                this.rdaddnewuser_step4.Visible = true;
                break;

            case 5:
                this.rdaddnewuser_step5.Visible = true;
                this.rdaddnewuser_step5.LoadHomeFolders(this.rdaddnewuser_step4.SelectedList);
                break;

            case 6:
                this.rdaddnewuser_step6.Visible = true;
                #region Display Data

                // Step 1
                this.rdaddnewuser_step6.RequestorsFirstName = this.rdaddnewuser_step1.RequestorsFirstName;
                this.rdaddnewuser_step6.RequestorsLastName = this.rdaddnewuser_step1.RequestorsLastName;
                this.rdaddnewuser_step6.RequestorsPhone = this.rdaddnewuser_step1.RequestorsPhone;
                this.rdaddnewuser_step6.RequestorsDepartment = this.rdaddnewuser_step1.RequestorsDepartment;
                this.rdaddnewuser_step6.RequestorsEmail = this.rdaddnewuser_step1.RequestorsEmail;

                // Step 2
                this.rdaddnewuser_step6.AssociatesPernaNumber = this.rdaddnewuser_step2.AssociatesPernaNumber;
                this.rdaddnewuser_step6.AssociatesFirstName = this.rdaddnewuser_step2.AssociatesFirstName;
                this.rdaddnewuser_step6.AssociatesLastName = this.rdaddnewuser_step2.AssociatesLastName;
                this.rdaddnewuser_step6.AssociatesLocation = this.rdaddnewuser_step2.AssociatesLocation;
                this.rdaddnewuser_step6.AssociatesDepartment = this.rdaddnewuser_step2.AssociatesDepartment;
                this.rdaddnewuser_step6.AssociatesCompany = this.rdaddnewuser_step2.AssociatesCompany;
                this.rdaddnewuser_step6.AssociatesPhone = this.rdaddnewuser_step2.AssociatesPhone;
                this.rdaddnewuser_step6.AssociatesTitle = this.rdaddnewuser_step2.AssociatesTitle;

                // Step 3
                this.rdaddnewuser_step3.CheckBox_BudgetDistribution.Checked = false;
                this.rdaddnewuser_step3.CheckBox_BlueBookDistribution.Checked = false;
                this.rdaddnewuser_step3.CheckBox_MyReports.Checked = false;
                this.rdaddnewuser_step3.CheckBox_Passport.Checked = false;

                // Step 4
                this.rdaddnewuser_step6.SelectedGroups = this.rdaddnewuser_step4.SelectedList;

                // Step 5
                this.rdaddnewuser_step6.HomeFolder = this.rdaddnewuser_step5.AssociatesHomeFolder;


                #endregion

                break;

            case 7:
                this.rdaddnewuser_step7.Visible = true;
                break;

        }
    }

    #endregion

    #region SaveData

    private void SaveData()
    {

        try
        {
            Hashtable htValue = new Hashtable();

            #region format Email

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append("Please keep the following information for your records. <br><br>");

            sb.Append("<SPAN style=\"FONT-SIZE: 12pt\"><b><u>Requestor's Information</u></b><br></span>");

            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Requestor's First Name:</b> " + DistributionPortal.NewUserFields.RequestorsFirstName + "</span><br>");
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Requestor's Last Name:</b> " + DistributionPortal.NewUserFields.RequestorsLastName + "</span><br>");
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Requestor's Phone:</b> " + DistributionPortal.NewUserFields.RequestorsPhone + "</span><br>");
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Requestor's Email Address:</b> " + DistributionPortal.NewUserFields.RequestorsEmail + "</span><br>");
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Requestor's Department:</b> " + DistributionPortal.NewUserFields.RequestorsDepartment + "</span><br>");
            sb.Append("<br><br>");

            sb.Append("<SPAN style=\"FONT-SIZE: 12pt\"><b><u>New User Information:</u></b></span><br>");
            // First Name
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Associate's First Name:</b> " + DistributionPortal.NewUserFields.AssociatesFirstName + "</span><br>");
            htValue.Add("givenname", DistributionPortal.NewUserFields.AssociatesFirstName);

            // Last Name
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Associate's Last Name:</b> " + DistributionPortal.NewUserFields.AssociatesLastName + "</span><br>");
            htValue.Add("sn", DistributionPortal.NewUserFields.AssociatesLastName);

            // Location
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Associate's Location:</b> " + DistributionPortal.NewUserFields.AssociatesLocation + "</span><br>");
            htValue.Add("description", DistributionPortal.NewUserFields.AssociatesLocation);

            // Perna Number
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Associate's Personnel Number:</b> " + DistributionPortal.NewUserFields.AssociatesPernaNumber + "</span><br>");
            // Do not add this value, we will use it later.

            // Department
            sb.Append("<b>Associate's Department:</b> " + DistributionPortal.NewUserFields.AssociatesDepartment + "</span><br>");
            htValue.Add("Department", DistributionPortal.NewUserFields.AssociatesDepartment);

            // Company
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Associate's Company:</b> " + DistributionPortal.NewUserFields.AssociatesCompany + "</span><br>");
            htValue.Add("Company", DistributionPortal.NewUserFields.AssociatesCompany);

            // Phone Number
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Associate's Phone:</b> " + DistributionPortal.NewUserFields.AssociatesPhone + "</span><br>");
            htValue.Add("Telephonenumber", DistributionPortal.NewUserFields.AssociatesPhone);

            // Title
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Associate's Title:</b> " + DistributionPortal.NewUserFields.AssociatesTitle + "</span><br>");
            htValue.Add("Title", DistributionPortal.NewUserFields.AssociatesTitle);

            // Actuate Home Folder
            sb.Append("<SPAN style=\"FONT-SIZE: 11pt\"><b>Associate's Home Folder:</b> " + DistributionPortal.NewUserFields.AssociatesHomeFolder + "</span><br><br>");
            htValue.Add("actuateRDHomeFolder", DistributionPortal.NewUserFields.AssociatesHomeFolder);

            sb.Append("<SPAN style=\"FONT-SIZE: 12pt\"><b><u>Add the following groups to the New user:</u></b></span><br>");

            #endregion

            // Actuate User
            htValue.Add("actuateUser", this.rdaddnewuser_step3.CheckBox_MyReports.Checked.ToString().ToUpper());

            // add the groups to the hashtable
            #region Add Groups
            string sGroups = "";
            int yy = 0;

            // Get this users group membership
            if (DistributionPortal.NewUserFields.rdNewUsersGroups != null)
            {
                sb.Append("<table><tr>");
                foreach (System.Web.UI.WebControls.ListItem item in DistributionPortal.NewUserFields.rdNewUsersGroups)
                {
                    string sItem = item.Text;

                    yy++;

                    if (yy == 1)
                    {
                        sb.Append("<td><SPAN style=\"FONT-SIZE: 11pt\">" + item + "</span></td>");
                    }
                    else if (yy == 2)
                    {
                        sb.Append("<td><SPAN style=\"FONT-SIZE: 11pt\"> |" + item + "</span></td>");
                    }
                    else if (yy == 3)
                    {
                        sb.Append("<td><SPAN style=\"FONT-SIZE: 11pt\"> |" + item + "</span></td></tr><tr>");
                        yy = 0;
                    }

                    sGroups += sItem + "|";


                }
                sb.Append("</tr></table>");
            }
            #endregion

            // Budget Distribution User
            if (this.rdaddnewuser_step3.CheckBox_BudgetDistribution.Checked)
                sGroups += "BudgetDistributionUsers" + "|";

            // Passport User
            if (this.rdaddnewuser_step3.CheckBox_Passport.Checked)
                sGroups += "PassportUsers" + "|";

            // Blue Book Distribution User
            if (this.rdaddnewuser_step3.CheckBox_BlueBookDistribution.Checked)
                sGroups += "BlueBookDistributionUsers" + "|";

            if (sGroups.EndsWith("|"))
                sGroups = sGroups.Substring(0, sGroups.Length - 1);

            // Insert the values into the database			
            new UserManagement.BLL.NewUserBLL(AppValues.UM_DB_ConnectionString, UserManagement.BLL.AuthObject.GetAuthObject).InsertUserInfoInoSQL(DistributionPortal.NewUserFields.AssociatesPernaNumber, "test,1234", sGroups, DistributionPortal.NewUserFields.RequestorsFirstName, DistributionPortal.NewUserFields.RequestorsLastName, DistributionPortal.NewUserFields.RequestorsEmail, DistributionPortal.NewUserFields.RequestorsPhone, DistributionPortal.NewUserFields.RequestorsDepartment, htValue);

            System.Net.Mail.MailAddressCollection macTO = new System.Net.Mail.MailAddressCollection();
            System.Net.Mail.MailAddressCollection macCC = new System.Net.Mail.MailAddressCollection();

            macTO.Add(AppValues.NewUserEmailConfirmationAddress);

            macCC.Add(DistributionPortal.NewUserFields.RequestorsEmail);

            EmailHelper.SendMail(AppValues.SMTP,	// SMTP Server
                                    "Report Distribution, Request for new user.",	// Subject
                                    sb.ToString(),		// Message body
                                    macTO,
                                    new System.Net.Mail.MailAddress(DistributionPortal.NewUserFields.RequestorsEmail),		// From Email Address, Email originator
                                    macCC,
                                    null,
                                    null,
                                    System.Net.Mail.MailPriority.High, // Mail priority
                                    true);




            // When finished, Clear the screens
            ClearScreens();
        }
        catch (Exception ex)
        {
            DisplayError(ex);
        }
    }

    #endregion

    protected override bool OnBubbleEvent(object source, EventArgs args)
    {
        try
        {
            if (args is CommandEventArgs)
            {
                CommandEventArgs ce = (CommandEventArgs)args;
                switch (ce.CommandName)
                {
                    case "Step":
                        ShowStep(System.Convert.ToInt32(ce.CommandArgument));
                        break;

                    case "SaveNewUser":
                        SaveData();
                        ShowStep(7);
                        break;
                }                
            }

            return true;
        }
        catch (Exception ex)
        {
            DisplayError(ex);
            return false;
        }
    }        
}