<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RDAddNewUser_Step6.ascx.cs" Inherits="UserManagement_NewMyReportsUser_RDAddNewUser_Step6" %>

<%@ Register TagPrefix="uc1" TagName="SideBar" Src="SideBar.ascx" %>
<table width="100%" border="0">
	<tr>
		<td width="100%" valign="top" align="right">
			<table class="MyReportsAdminConsoleBorder">
				<TBODY>
					<TR>
						<TD vAlign="top" align="center" colSpan="4">
							<asp:label id="Label16" runat="server" CssClass="X-SmallBold">
						Requestor's Information</asp:label>
						</TD>
					</TR>
					<TR>
						<TD vAlign="middle" align="left" colSpan="4">
							<HR>
						</TD>
					</TR>
					<TR>
						<TD nowrap>
							<asp:Label id="Label10" runat="server" CssClass="XX-Small">Requestor's First Name:</asp:Label></TD>
						<TD nowrap Width="50%">
							<asp:TextBox id="txtRequestorsFirstName" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:TextBox>
						</TD>
						<TD nowrap>
							<asp:Label id="Label1" runat="server" CssClass="XX-Small">Requestor's Last Name:</asp:Label>
						</TD>
						<TD nowrap Width="50%">
							<asp:TextBox id="txtRequestorsLastName" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD nowrap>
							<asp:Label id="Label4" runat="server" CssClass="XX-Small">Requestor's Phone:</asp:Label></TD>
						<TD nowrap Width="50%">
							<asp:TextBox id="txtRequestorsPhone" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:TextBox></TD>
						<TD nowrap>
							<asp:Label id="Label3" runat="server" CssClass="XX-Small">Requestor's Department:</asp:Label></TD>
						<TD nowrap Width="50%">
							<asp:TextBox id="txtRequestorsDepartment" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD>
							<asp:Label id="Label7" runat="server" CssClass="XX-Small">Requestor's Email:</asp:Label></TD>
						<TD colspan="3">
							<asp:TextBox id="txtRequestorsEmail" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:TextBox></TD>
					</TR>
					<TR>
						<TD vAlign="top" align="center" colSpan="4">
							<asp:label id="Label17" runat="server" CssClass="X-SmallBold">
						Users Information</asp:label>
						</TD>
					</TR>
					<TR>
						<TD vAlign="middle" align="left" colSpan="4">
							<HR>
						</TD>
					</TR>
					<TR>
						<TD noWrap>
							<asp:label id="Label6" runat="server" CssClass="XX-Small">Associate's Personnel Number:</asp:label></TD>
						<TD nowrap colspan="3">
							<asp:textbox id="txtAssociatesPernaNumber" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:textbox></TD>
					</TR>
					<TR>
						<TD noWrap>
							<asp:label id="Label2" runat="server" CssClass="XX-Small">Associate's First Name:</asp:label></TD>
						<TD nowrap Width="50%">
							<asp:textbox id="txtAssociatesFirstName" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:textbox></TD>
						<TD nowrap>
							<asp:label id="Label8" runat="server" CssClass="XX-Small">Associate's Last Name:</asp:label></TD>
						<TD nowrap Width="50%">
							<asp:textbox id="txtAssociatesLastName" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:textbox></TD>
					</TR>
					<TR>
						<TD noWrap>
							<asp:label id="Label11" runat="server" CssClass="XX-Small">Associate's Location:</asp:label></TD>
						<TD nowrap Width="50%">
							<asp:textbox id="txtAssociatesLocation" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:textbox></TD>
						<TD noWrap>
							<asp:label id="Label12" runat="server" CssClass="XX-Small">Associate's Cost Center:</asp:label></TD>
						<TD nowrap Width="50%">
							<asp:textbox id="txtAssociatesDepartment" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:textbox></TD>
					</TR>
					<TR>
						<TD noWrap>
							<asp:label id="Label13" runat="server" CssClass="XX-Small">Associate's Company</asp:label></TD>
						<TD nowrap Width="50%">
							<asp:textbox id="txtAssociatesCompany" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:textbox></TD>
						<TD noWrap>
							<asp:label id="Label14" runat="server" CssClass="XX-Small">Associate's Phone:</asp:label></TD>
						<TD nowrap Width="50%">
							<asp:textbox id="txtAssociatesPhone" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:textbox></TD>
					</TR>
					<TR>
						<TD noWrap>
							<asp:label id="Label15" runat="server" CssClass="XX-Small">Associate's Title:</asp:label></TD>
						<TD nowrap colspan="3">
							<asp:textbox id="txtAssociatesTitle" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:textbox></TD>
					</TR>
					<tr>
						<TD noWrap>
							<asp:label id="Label9" runat="server" CssClass="XX-Small">Associate's Home Folder:</asp:label></TD>
						<TD nowrap colspan="3">
							<asp:textbox id="txtHomeFolder" runat="server" CssClass="XX-Small" Width="100%" ReadOnly="True"></asp:textbox></TD>
					</tr>
					<tr>
						<td nowrap vAlign="top" align="left"><asp:label id="Label5" runat="server" CssClass="XX-Small">Selected Groups:</asp:label></td>
						<TD nowrap colspan="3">
							<asp:ListBox id="lstAddGroups" runat="server" CssClass="XX-Small" Width="100%" Height="120px"
								SelectionMode="Multiple"></asp:ListBox></TD>
					</tr>
					<tr>
						<td valign="bottom" align="right" colspan="4">
							<asp:button CommandName="Step" CommandArgument="5" id="btnNextStep" runat="server" Text="<< Step 5" CssClass="MyReportsAdminConsoleButton"></asp:button>
							<asp:Button CommandName="SaveNewUser" id="btnSubmit" CssClass="MyReportsAdminConsoleButton" runat="server" Text="Submit" ></asp:Button>
						</td>
					</tr>
				</TBODY>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="bottom" align="right" colspan="2">
		</td>
	</tr>
</table>
