using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using MyReports.UserManagementUI.Common;

using Compass.Security.ActiveDirectory;
using UserManagement.BLL;
using System.Collections;

public partial class UserManagement_NewMyReportsUser_RDAddNewUser_Step5 : NewUserBaseClass
{

    #region LoadHomeFolders

    public void LoadHomeFolders(ArrayList arySelectedGroups)
    {
        Users oUsers = null;
        ArrayList ary = new ArrayList();

        try
        {
            this.HomeFolders1.HomeFolderListing = new GroupMembershipBLL(MyReports.Common.AppValues.MyReports_DB_ConnectionString).getHomeFolderListing(arySelectedGroups);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oUsers = null;
        }

    }

    #endregion

    public string MainSelection
    {
        get { return this.HomeFolders1.MainSelection; }
        set { this.HomeFolders1.MainSelection = value; }
    }

    public string SubSelection
    {
        get { return this.HomeFolders1.SubSelection; }
        set { this.HomeFolders1.SubSelection = value; }
    }

    public string AssociatesHomeFolder
    {
        get
        {
            if (this.MainSelection != null)
            {
                if (this.MainSelection == "Generic")
                    return this.HomeFolders1.SubSelection;
                else
                    return this.HomeFolders1.HomeFolder;
            }
            else
                return "";

        }

        set { this.HomeFolders1.SubSelection = value; }
    }

}