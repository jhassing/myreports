<%@ Register TagPrefix="uc1" TagName="SideBar" Src="SideBar.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewMyReportsUser.ascx.cs" Inherits="UserManagement_NewMyReportsUser_NewMyReportsUser" %>
<%@ Register Src="rdaddnewuser_step5.ascx" TagName="rdaddnewuser_step5" TagPrefix="uc6" %>
<%@ Register Src="rdaddnewuser_step6.ascx" TagName="rdaddnewuser_step6" TagPrefix="uc7" %>
<%@ Register Src="rdaddnewuser_step7.ascx" TagName="rdaddnewuser_step7" TagPrefix="uc8" %>
<%@ Register Src="rdaddnewuser_step3.ascx" TagName="rdaddnewuser_step3" TagPrefix="uc4" %>
<%@ Register Src="rdaddnewuser_step4.ascx" TagName="rdaddnewuser_step4" TagPrefix="uc5" %>
<%@ Register Src="rdaddnewuser_step2.ascx" TagName="rdaddnewuser_step2" TagPrefix="uc3" %>
<%@ Register Src="rdaddnewuser_step1.ascx" TagName="rdaddnewuser_step1" TagPrefix="uc2" %>
<table width="100%">
	<tr>
		<td vAlign="top" align="left">
			<uc1:sidebar id="SideBar1" runat="server"></uc1:sidebar></td>
		<td width="10" valign="top" align="right">&nbsp;</td>
		<td vAlign="top" noWrap align="left" width="100%" height="100%">		
            <uc2:rdaddnewuser_step1 Visible="false" ID="rdaddnewuser_step1" runat="server" />
            <uc3:rdaddnewuser_step2 Visible="false" ID="rdaddnewuser_step2" runat="server" />
            <uc4:rdaddnewuser_step3 Visible="false" ID="rdaddnewuser_step3" runat="server" />
            <uc5:rdaddnewuser_step4 Visible="false" ID="rdaddnewuser_step4" runat="server" />
            <uc6:rdaddnewuser_step5 Visible="false" ID="rdaddnewuser_step5" runat="server" />
            <uc7:rdaddnewuser_step6 Visible="false" ID="rdaddnewuser_step6" runat="server" />
            <uc8:rdaddnewuser_step7 Visible="false" ID="rdaddnewuser_step7" runat="server" />
            <asp:Label Visible=False ID="lblMessage" ForeColor="Red" runat="server" CssClass="X-Small" Text="[Message]" Height="52px" Width="608px"></asp:Label>
		</td>
	</tr>
</table>

