<%@ Register TagPrefix="uc1" TagName="ApplicationAccess" Src="../Common/ApplicationAccess.ascx" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RDAddNewUser_Step3.ascx.cs" Inherits="UserManagement_NewMyReportsUser_RDAddNewUser_Step3" %>
<table class="MyReportsAdminConsoleBorder" width="100%" height="100%">
	<tr>
		<td valign="top" align="center" colspan="2">
			<asp:Label id="Label1" runat="server" CssClass="X-SmallBold">Application Assignment
				</asp:Label>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left" valign="middle"><hr>
		</td>
	</tr>
	<tr>
		<td>
			<uc1:ApplicationAccess id="ApplicationAccess1" runat="server"></uc1:ApplicationAccess>
			</td>
	</tr>
	<tr height="100%" valign="bottom" align="right">
		<td>
			<asp:button CommandName="Step" CommandArgument="2" id="btnStep2" runat="server" Text="<< Step 2" CssClass="MyReportsAdminConsoleButton"></asp:button>
			<asp:Button CommandName="Step" CommandArgument="4" id="btnStep4" runat="server" Text="Step 4 >>" CssClass="MyReportsAdminConsoleButton"></asp:Button>
		</td>
	</tr>
</table>
