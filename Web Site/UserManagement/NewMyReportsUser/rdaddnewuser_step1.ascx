<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RDAddNewUser_Step1.ascx.cs" Inherits="UserManagement_NewMyReportsUser_RDAddNewUser_Step1" %>

<table class="MyReportsAdminConsoleBorder" width="100%" height="100%" valign="top">
	<tr>
		<td valign="top" align="center" colspan="3">
			<asp:Label id="Label2" runat="server" CssClass="X-SmallBold">Requestor's 
				Information</asp:Label></td>
	</tr>
	<tr>
		<td colspan="3" align="left" valign="middle"><hr>
		</td>
	</tr>
	<TR>
		<TD nowrap height="26">
			<asp:Label id="Label10" runat="server" Width="160px" CssClass="XX-Small">Requestor's First Name:</asp:Label></TD>
		<td nowrap>
		</td>
		<TD nowrap width="100%" height="26">
			<asp:TextBox id="txtRequestorsFirstName" runat="server" CssClass="XX-Small" Width="98%" ReadOnly="True"></asp:TextBox></TD>
	</TR>
	<TR>
		<TD nowrap >
			<asp:Label id="Label1" runat="server" CssClass="XX-Small" Width="160px" Height="16px">Requestor's Last Name:</asp:Label></TD>
		<td nowrap>
		</td>
		<TD nowrap >
			<asp:TextBox id="txtRequestorsLastName" runat="server" CssClass="XX-Small" Width="98%" ReadOnly="True"></asp:TextBox></TD>
	</TR>
	<TR>
		<TD nowrap height="27">
			<asp:Label id="Label7" runat="server" CssClass="XX-Small" Width="120px">Requestor's Email:</asp:Label></TD>
		<td nowrap>
		</td>
		<TD nowrap height="27">
			<asp:TextBox id="txtRequestorsEmail" runat="server" CssClass="XX-Small" Width="98%" ReadOnly="True"></asp:TextBox></TD>
	</TR>
	<TR>
		<TD nowrap >
			<asp:Label id="Label4" runat="server" CssClass="XX-Small" Width="128px">Requestor's Phone:</asp:Label></TD>
		<td nowrap></td>
		<TD nowrap >
			<asp:TextBox id="txtRequestorsPhone" runat="server" CssClass="XX-Small" Width="98%" ReadOnly="True"></asp:TextBox></TD>
	</TR>
	<TR>
		<TD nowrap >
			<asp:Label id="Label3" runat="server" CssClass="XX-Small" Width="168px">Requestor's Department:</asp:Label></TD>
		<td nowrap></td>
		<TD nowrap >
			<asp:TextBox id="txtRequestorsDepartment" runat="server" CssClass="XX-Small" Width="98%" ReadOnly="True"></asp:TextBox></TD>
	</TR>
	<tr align="right" valign="bottom" height="100%">
		<td valign="bottom" align="right" colspan="3" height="100%">
			<asp:button id="btnNextStep" runat="server" Text="Step 2>>" CssClass="MyReportsAdminConsoleButton" CommandArgument="2" CommandName="Step"></asp:button>
		</td>
	</tr>
</table>
