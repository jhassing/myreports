<%@ Register TagPrefix="cc1" Namespace="AegisControls" Assembly="AegisControls" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RDAddNewUser_Step2.ascx.cs" Inherits="UserManagement_NewMyReportsUser_RDAddNewUser_Step2" %>
<table class="MyReportsAdminConsoleBorder" height="100%" width="100%"valign="top">
	<tr>
		<td vAlign="top" align="center" colSpan="3"><asp:label id="Label1" CssClass="X-SmallBold" runat="server">
						New Users Information</asp:label></td>
	</tr>
	<tr>
		<td vAlign="middle" align="left" colSpan="3">
			<hr>
		</td>
	</tr>
	<TR>
		<TD style="HEIGHT: 34px" noWrap align="left"><asp:label id="Label6" CssClass="XX-Small" runat="server" Width="176px">Associate's Personnel Number:</asp:label></TD>
		<td style="HEIGHT: 34px" noWrap><asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" EnableClientScript="true" ControlToValidate="txtAssociatesPernaNumber"
				ErrorMessage="*"></asp:requiredfieldvalidator></td>
		<TD style="HEIGHT: 34px" width="100%"><cc1:MaskedTextBox autocomplete="off"  id="txtAssociatesPernaNumber" CssClass="XX-SmallTextBox" runat="server" Width="98%"
				MaxLength="8" Format="Text"></cc1:MaskedTextBox><asp:customvalidator id="CustomValidator2" CssClass="XX-Small" runat="server" EnableClientScript="False"
				ControlToValidate="txtAssociatesPernaNumber" ErrorMessage="Personnel number must be 8 digits." Display="Dynamic"></asp:customvalidator><asp:customvalidator id="valPernaAlreadyExists" CssClass="XX-Small" runat="server" EnableClientScript="False"
				ControlToValidate="txtAssociatesPernaNumber" ErrorMessage="Personnel number must be 8 digits." Display="Dynamic"></asp:customvalidator></TD>
	</TR>
	<tr>
		<TD noWrap><asp:label id="Label2" CssClass="XX-Small" runat="server">Associate's First Name:</asp:label></TD>
		<td noWrap><asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" EnableClientScript="true" ControlToValidate="txtAssociatesFirstName"
				ErrorMessage="*"></asp:requiredfieldvalidator></td>
		<TD><cc1:MaskedTextBox autocomplete="off"  id="txtAssociatesFirstName" CssClass="XX-SmallTextBox" runat="server" Width="98%"
				MaxLength="30" Format="Text"></cc1:MaskedTextBox></TD>
	</tr>
	<TR>
		<TD><asp:label id="Label8" CssClass="XX-Small" runat="server">Associate's Last Name:</asp:label></TD>
		<td noWrap><asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" ControlToValidate="txtAssociatesLastName"
				ErrorMessage="*"></asp:requiredfieldvalidator></td>
		<TD><cc1:MaskedTextBox autocomplete="off"  id="txtAssociatesLastName" CssClass="XX-SmallTextBox" runat="server" Width="98%"
				MaxLength="60" Format="Text"></cc1:MaskedTextBox></TD>
	</TR>
	<TR>
		<TD noWrap><asp:label id="Label11" CssClass="XX-Small" runat="server">Associate's Location:</asp:label></TD>
		<td noWrap></td>
		<TD><asp:dropdownlist id="drpAssociatesLocation" CssClass="XX-Small" runat="server" Width="98%">
				<asp:ListItem Value="Corporate" Selected="True">Corporate</asp:ListItem>
				<asp:ListItem Value="Remote">Remote</asp:ListItem>
			</asp:dropdownlist></TD>
	</TR>
	<TR>
		<TD noWrap><asp:label id="Label12" CssClass="XX-Small" runat="server">Associate's Cost Center:</asp:label></TD>
		<td noWrap><asp:requiredfieldvalidator id="Requiredfieldvalidator5" runat="server" ControlToValidate="txtAssociatesDepartment"
				ErrorMessage="*"></asp:requiredfieldvalidator></td>
		<TD><cc1:MaskedTextBox autocomplete="off"  id="txtAssociatesDepartment" CssClass="XX-SmallTextBox" runat="server" Width="98%"
				MaxLength="50" Format="Text"></cc1:MaskedTextBox></TD>
	</TR>
	<TR>
		<TD noWrap><asp:label id="Label13" CssClass="XX-Small" runat="server">Associate's Company</asp:label></TD>
		<td noWrap></td>
		<TD><asp:dropdownlist id="drpAssociatesCompany" CssClass="XX-Small" runat="server" Width="98%">
				<asp:ListItem Value="Bateman">Bateman</asp:ListItem>
				<asp:ListItem Value="Best Vendors">Best Vendors</asp:ListItem>
				<asp:ListItem Value="Canteen Vending Services">Canteen Vending Services</asp:ListItem>
				<asp:ListItem Value="Chartwells">Chartwells</asp:ListItem>
				<asp:ListItem Value="Compass Canada">Compass Canada</asp:ListItem>
				<asp:ListItem Value="Corporate Income">Corporate Income</asp:ListItem>
				<asp:ListItem Value="Daka">Daka</asp:ListItem>
				<asp:ListItem Value="Eurest Dining Services">Eurest Dining Services</asp:ListItem>
				<asp:ListItem Value="Flik">Flik</asp:ListItem>
				<asp:ListItem Value="Franchise &amp; International">Franchise &amp; International</asp:ListItem>
				<asp:ListItem Value="Indirect G&amp;A">Indirect G&amp;A</asp:ListItem>
				<asp:ListItem Value="Military">Military</asp:ListItem>
				<asp:ListItem Value="Morrison">Morrison</asp:ListItem>
				<asp:ListItem Value="R&amp;A B&amp;I Business">R&amp;A B&amp;I Business</asp:ListItem>
				<asp:ListItem Value="R&amp;A Restaurants &amp; Entertainment">R&amp;A Restaurants &amp; Entertainment</asp:ListItem>
				<asp:ListItem Value="Retail Sector">Retail Sector</asp:ListItem>
				<asp:ListItem Value="Service America">Service America</asp:ListItem>
				<asp:ListItem Value="Specialty Market">Specialty Market</asp:ListItem>
			</asp:dropdownlist></TD>
	</TR>
	<TR>
		<TD noWrap><asp:label id="Label14" CssClass="XX-Small" runat="server">Associate's Phone:</asp:label></TD>
		<td noWrap><asp:requiredfieldvalidator id="Requiredfieldvalidator7" runat="server" ControlToValidate="txtAssociatesPhone"
				ErrorMessage="*"></asp:requiredfieldvalidator></td>
		<TD><cc1:MaskedTextBox autocomplete="off"  id="txtAssociatesPhone" CssClass="XX-SmallTextBox" runat="server" Width="98%" MaxLength="13"
				Format="Text"></cc1:MaskedTextBox></TD>
	</TR>
	<TR>
		<TD noWrap><asp:label id="Label15" CssClass="XX-Small" runat="server">Associate's Title:</asp:label></TD>
		<td noWrap><asp:requiredfieldvalidator id="Requiredfieldvalidator8" runat="server" ControlToValidate="txtAssociatesTitle"
				ErrorMessage="*"></asp:requiredfieldvalidator></td>
		<TD><cc1:MaskedTextBox autocomplete="off"  id="txtAssociatesTitle" CssClass="XX-SmallTextBox" runat="server" Width="98%" MaxLength="30"
				Format="Text"></cc1:MaskedTextBox></TD>
	</TR>
	<tr vAlign="bottom" height="100%">
		<td vAlign="bottom" align="right" colSpan="3">
			<asp:button CommandName="Step" CommandArgument="1" CausesValidation="false" id="Button1" CssClass="MyReportsAdminConsoleButton " runat="server" Text="<< Step 1"></asp:button>&nbsp;
			<asp:button CommandName="Step" CommandArgument="3" id="btnNextStep" CssClass="MyReportsAdminConsoleButton" runat="server" Text="Step 3 >>"></asp:button></td>
	</tr>
</table>
