using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using MyReports.UserManagementUI.Common;

using UserManagement.BLL;
using MyReports.Common;

using Compass.Security.ActiveDirectory;

using System.Collections;

public partial class UserManagement_NewMyReportsUser_RDAddNewUser_Step4 : NewUserBaseClass
{
    #region Properties

    public ArrayList SelectedList
    {
        get
        {
            return this.RDGroups1.SelectedList;
        }
        set
        {
            this.RDGroups1.SelectedList = value;
        }
    }

    public ArrayList AvailableList
    {
        get
        {
            return this.RDGroups1.AvailableList;
        }
        set
        {
            this.RDGroups1.AvailableList = value;
        }
    }

    public string Filter
    {
        get
        {
            return this.RDGroups1.Filter;
        }
        set
        {
            this.RDGroups1.Filter = value;
        }
    }

    #endregion

    #region PreRender

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Visible)
        {
            this.RDGroups1.Display_BlueBook_Option = MyReports.Common.AppValues.IsInRole("BlueBookDistributionAdmins");
            this.RDGroups1.Display_Financial_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin");
            this.RDGroups1.Display_Hourly_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin");
            this.RDGroups1.Display_Salary_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin");
        }
    }

    #endregion

    #region PageLoad

    protected void Page_Load(object sender, System.EventArgs e)
    {
        this.RDGroups1.ListSearch_OnClick += new Delegates.SearchButtonClicked(RDGroups1_ListSearch_OnClick);
    }

    #endregion

    #region Edit group membership search button click

    /// <summary>
    /// Function will get the current selected users group membership, popluate the selected box and allow for editing.
    /// </summary>

    private void RDGroups1_ListSearch_OnClick()
    {
        Groups oGroups = null;

        try
        {

            oGroups = new Groups(UserManagement.BLL.AuthObject.GetAuthObject);

            // Build the search filter
            string filter = "(&(|";

            if (this.RDGroups1.Financial_Option_Checked)
                filter += "(samAccountName=" + this.RDGroups1.Filter + "*" + AppValues.FinancialGroups + ")";

            if (this.RDGroups1.Salary_Option_Checked)
                filter += "(samAccountName=" + this.RDGroups1.Filter + "*" + AppValues.PayrollSalaryGroups + ")";

            if (this.RDGroups1.Hourly_Option_Checked)
                filter += "(samAccountName=" + this.RDGroups1.Filter + "*" + AppValues.PayrollHourlyGroups + ")";

            if (this.RDGroups1.BlueBook_Option_Checked)
                filter += "(samAccountName=" + this.RDGroups1.Filter + "*" + AppValues.BlueBookGroups + ")";

            filter += "))";

            // Get the list of members in the particulat group
            ArrayList ary = oGroups.GetGroupListing(filter, AppValues.LdapBaseOU, (int)UserManagement.BLL.BaseClass.enumSearchScope.SCOPE_SUB, AppValues.MaxLDAPsearchResuts);

            // Get the list of members in the particulat group
            this.RDGroups1.AvailableList = ary;

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oGroups = null;
        }
    }

    #endregion
 
}