<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SideBar.ascx.cs" Inherits="UserManagement_NewMyReportsUser_SideBar" %>

<DIV style="WIDTH: 125px; POSITION: relative; HEIGHT: 384px; BACKGROUND-COLOR: #edecd1">
	<br>
	<asp:Panel id="panStep1" runat="server" CssClass="NonSelectedStep">
		<asp:Label id="Label1" CssClass="AddNewUserSideLinkTest" Runat="server">
			<b>Step 1</b><br>
			<i>&nbsp;(Requestor's Info)</i></asp:Label>
	</asp:Panel>
	<asp:Panel id="panStep2" runat="server" CssClass="NonSelectedStep">
		<asp:Label id="Label2" CssClass="AddNewUserSideLinkTest" Runat="server">
			<b>Step 2</b><br>
			<i>&nbsp;(New Users Info)</i></asp:Label>
	</asp:Panel>
	<asp:Panel id="panStep3" runat="server" CssClass="NonSelectedStep">
		<asp:Label id="Label6" CssClass="AddNewUserSideLinkTest" Runat="server">
			<b>Step 3</b><br>
			<i>&nbsp;(Application)</i></asp:Label>
	</asp:Panel>
	<asp:Panel id="panStep4" runat="server" CssClass="NonSelectedStep">
		<asp:Label id="Label3" CssClass="AddNewUserSideLinkTest" Runat="server">
			<b>Step 4</b><br>
			<i>&nbsp;(Group Info)</i></asp:Label>
	</asp:Panel>
	<asp:Panel id="panStep5" runat="server" CssClass="NonSelectedStep">
		<asp:Label id="Label4" CssClass="AddNewUserSideLinkTest" Runat="server">
			<b>Step 5</b><br>
			<i>&nbsp;(Home Folder)</i></asp:Label>
	</asp:Panel>
	<asp:Panel id="panStep6" runat="server" CssClass="NonSelectedStep">
		<asp:Label id="Label5" CssClass="AddNewUserSideLinkTest" Runat="server">
			<b>Step 6</b><br>
			<i>&nbsp;(Review)</i></asp:Label>
	</asp:Panel>
	<asp:Panel id="panStep7" runat="server" CssClass="NonSelectedStep">
		<asp:Label id="Label7" CssClass="AddNewUserSideLinkTest" Runat="server">
			<b>Step 7</b><br>
			<i>&nbsp;(Completed)</i></asp:Label>
	</asp:Panel>
</DIV>
