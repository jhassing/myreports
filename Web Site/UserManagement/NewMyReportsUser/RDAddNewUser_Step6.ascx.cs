using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Text;
using System.Xml;

using Compass.Security.ActiveDirectory;	
using Compass.Utilities.Email;

using MyReports.Common;

public partial class UserManagement_NewMyReportsUser_RDAddNewUser_Step6 : System.Web.UI.UserControl
{

    #region Properties

    public string AssociatesPernaNumber
    {
        set { this.txtAssociatesPernaNumber.Text = value; }
    }

    public string AssociatesFirstName
    {
        set { this.txtAssociatesFirstName.Text = value; }
    }

    public string AssociatesLastName
    {
        set { this.txtAssociatesLastName.Text = value; }
    }

    public string AssociatesLocation
    {
        set { this.txtAssociatesLocation.Text = value; }
    }

    public string AssociatesDepartment
    {
        set { this.txtAssociatesDepartment.Text = value; }
    }

    public string AssociatesCompany
    {
        set { this.txtAssociatesCompany.Text = value; }
    }

    public string AssociatesPhone
    {
        set { this.txtAssociatesPhone.Text = value; }
    }

    public string AssociatesTitle
    {
        set { this.txtAssociatesTitle.Text = value; }
    }

    public string RequestorsFirstName
    {
        set { this.txtRequestorsFirstName.Text = value; }
    }

    public string RequestorsLastName
    {
        set { this.txtRequestorsLastName.Text = value; }
    }

    public string RequestorsEmail
    {
        set { this.txtRequestorsEmail.Text = value; }
    }

    public string RequestorsPhone
    {
        set { this.txtRequestorsPhone.Text = value; }
    }

    public string RequestorsDepartment
    {
        set { this.txtRequestorsDepartment.Text = value; }
    }

    public string HomeFolder
    {
        get { return this.txtHomeFolder.Text; }

        set { txtHomeFolder.Text = (value != "" ? value : "[None Selected]"); }
    }

    public ArrayList SelectedGroups
    {

        set
        {
            if (value == null)
            {
                value.Add("[None Selected]");
                lstAddGroups.DataSource = value;
                lstAddGroups.DataBind();
            }
            else
            {
                if (value.Count == 0)
                {
                    value.Add("[None Selected]");
                    lstAddGroups.DataSource = value;
                    lstAddGroups.DataBind();
                }
                else
                {
                    lstAddGroups.DataSource = value;
                    lstAddGroups.DataBind();
                }
            }
        }
    }

    #endregion

}