using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class UserManagement_PersonnelNumber : System.Web.UI.UserControl
{
    public string Personnel_Number
    {
        get
        {
            return lblPersonnelNumber.Text;
        }
        set
        {
            lblPersonnelNumber.Text = value;
        }
    }
}