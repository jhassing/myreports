using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;	  	
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using DistributionPortal.Common;

using MyReports.Common;

using Compass.Security.ActiveDirectory;	 
using Novell.Directory.Ldap;

using UserManagement.BLL;


public partial class UserManagement_ObjectListing_ObjectListing : UserManagement_ObjectListing_BASE
{

    #region Page PreRender

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (Visible)
        {
            this.MyReportsGroupType1.Display_BlueBook_Option = MyReports.Common.AppValues.IsInRole("BlueBookDistributionAdmins");
            this.MyReportsGroupType1.Display_Financial_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin");
            this.MyReportsGroupType1.Display_Hourly_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin");
            this.MyReportsGroupType1.Display_Salary_Option = MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin");


            //if (Page.IsPostBack)
            //{
            //    ShowHidePanels();
            //    LoadData();
           // }
            //else
            //{
                // First time page visiter
                ShowHidePanels();

                Message1.HideMessage();

                StringBuilder sb = new StringBuilder();
                sb.Append("document.all." + GridFilter1.SearchButtonClientID + ".disabled = true;");

                if (this.GridFilter1.DisplayAddButton)
                    sb.Append("document.all." + GridFilter1.AddButtonClientID + ".disabled = true;");

                // clear button java to disable it
                sb.Append("document.all.btnClear.disabled = true;");

                // Search button java script to disable it
                GridFilter1.SearchButton.Attributes.Add("onclick", sb.ToString() + this.Page.GetPostBackEventReference(GridFilter1.SearchButton) + ";");

                // Add new button java script to disable it. Only generate this script if the button is visible
                if (this.GridFilter1.DisplayAddButton)
                    GridFilter1.AddButton.Attributes.Add("onclick", sb.ToString() + this.Page.GetPostBackEventReference(GridFilter1.AddButton) + ";");

                if (Request.QueryString["ListObjects"] != null)
                {
                    GridFilter1.FilterText = Request.QueryString["Filter"].ToString().Replace("*","");
                    LoadData();
                }
            //}
        }
    }

    #endregion

    #region Page Load

    protected void Page_Load(object sender, System.EventArgs e)
    {
        #region Set Control Events

        this.GridFilter1.Search_OnClick += new SearchClicked(GridFilter1_Search_OnClick);
        #endregion
    }

    #endregion

    #region Properties

    public bool IndividualDataLoaded
    {
        get
        {
            return ViewState["IndividualDataLoaded"] != null ? System.Convert.ToBoolean(ViewState["IndividualDataLoaded"].ToString()) : false;
        }
        set
        {
            ViewState["IndividualDataLoaded"] = value;
        }
    }

    public bool DisplayGroupListing
    {
        get
        {
            return ViewState["DisplayGroupListing"] != null ? System.Convert.ToBoolean(ViewState["DisplayGroupListing"].ToString()) : false;
        }
        set
        {
            ViewState["DisplayGroupListing"] = value;
        }
    }

    public bool DisplayUserListing
    {
        get
        {
            return ViewState["DisplayUserListing"] != null ? System.Convert.ToBoolean(ViewState["DisplayUserListing"].ToString()) : false;
        }
        set
        {
            ViewState["DisplayUserListing"] = value;
        }
    }


    public string FilterValue
    {
        get
        {
            return ViewState["FilterValue"] != null ? ViewState["FilterValue"].ToString() : "";
        }
        set
        {
            ViewState["FilterValue"] = value;
        }
    }

    #endregion

    #region ShowHidePanels

    private void ShowHidePanels()
    {
        this.panObjectListing.Visible = this.DisplayGroupListing;
        this.MyReportsGroupType1.Visible = this.DisplayGroupListing;
        this.GridFilter1.DisplayAddButton = this.DisplayGroupListing;
        this.panUserListing.Visible = this.DisplayUserListing;
        this.panListingPanel.Visible = true;
    }

    #endregion

    #region Load the grid data, depending on if we are searching for Groups or Users

    private void LoadData()
    {
        Users oUsers = null;
        Groups oGroups = null;

        try
        {
            ArrayList retObjects = null;
            int count = 0;

            this.FilterValue = GridFilter1.FilterText.Trim();

            this.FilterValue = this.FilterValue.Replace("*","");

            Message1.HideMessage();

            #region Load and Display the group listing

            if (this.DisplayGroupListing)
            {
                bool oneSelected = false;

                string filter = "(&(|";

                if (this.MyReportsGroupType1.Financial_Option_Checked)
                {
                    oneSelected = true;
                    filter += "(samAccountName=" + this.FilterValue + AppValues.FinancialGroups + ")";
                }

                if (this.MyReportsGroupType1.Salary_Option_Checked)
                {
                    oneSelected = true;
                    filter += "(samAccountName=" + this.FilterValue + AppValues.PayrollSalaryGroups + ")";
                }

                if (this.MyReportsGroupType1.Hourly_Option_Checked)
                {
                    oneSelected = true;
                    filter += "(samAccountName=" + this.FilterValue + AppValues.PayrollHourlyGroups + ")";
                }

                if (this.MyReportsGroupType1.BlueBook_Option_Checked)
                {
                    oneSelected = true;
                    filter += "(samAccountName=" + this.FilterValue + AppValues.BlueBookGroups + ")";
                }

                filter += "))";

                if (!oneSelected)
                {
                    this.GroupList.DataSource = null;
                    this.GroupList.DataBind();
                    this.GroupList.Dispose();
                    this.lblNoGroups.Visible = true;
                    this.lblNoGroups.Text = Global.ResourceValue("USER_MANAGEMENT_MUST_SELECT_GROUP");
                    return;
                }

                oGroups = new Groups(UserManagement.BLL.AuthObject.GetAuthObject);

                retObjects = oGroups.GetGroupDescriptionListing(filter, AppValues.LdapBaseOU, (int)UserManagement.BLL.BaseClass.enumSearchScope.SCOPE_SUB, AppValues.MaxLDAPsearchResuts);

                count = retObjects != null ? retObjects.Count : 0;

                if (count != 0)
                {
                    this.GroupList.DataSource = retObjects;
                    this.GroupList.DataBind();
                    this.lblNoGroups.Visible = false;
                }
                else
                {
                    this.GroupList.DataSource = null;
                    this.GroupList.DataBind();
                    this.GroupList.Dispose();
                    this.lblNoGroups.Visible = true;
                    this.lblNoGroups.Text = Global.ResourceValue("USER_MANAGEMENT_NO_MATCHING_GROUPS");
                }

            }

            #endregion

            #region Load and Display the User listing

            else if (this.DisplayUserListing)
            {
                oUsers = new Users();

                // Users Object
                List<tUserListing> retUserListing = oUsers.GetUsersBasedOnANR(this.FilterValue, AppValues.CurrentOU);

                if (retUserListing.Count != 0)
                {
                    this.UserList.DataSource = retUserListing;
                    this.UserList.DataBind();
                    this.lblNoUsers.Visible = false;
                }
                else
                {
                    this.UserList.DataSource = null;
                    this.UserList.DataBind();
                    this.UserList.Dispose();
                    this.lblNoUsers.Visible = true;
                }
            }

            #endregion

        }
        catch (Exception ex)
        {
            throw new Exception("Error loading AD information", ex);
        }
        finally
        {
            oUsers = null;
            oGroups = null;
        }
    }

    #endregion

    #region Link Button Clicked

    protected void lblName_Command(object sender, CommandEventArgs e )
    {

        string distinguishedName = e.CommandName.ToString().Trim();

        string queryString = "?distinguishedName=" + distinguishedName;

        queryString += "&addnew=false";


        queryString += "&HeaderLabel=Personnel Number: " + e.CommandArgument.ToString();
        queryString += "&Filter=" + this.FilterValue;
        queryString += "&ListObjects=true";


        if (this.DisplayGroupListing)
        {
            queryString += "&ObjectClass=group";
            Response.Redirect(Context.Request.ApplicationPath + "/UserManagement/IndividualObject/group/Existing/DefaultGroup.aspx" + queryString);
        }
        else
        {
            queryString += "&ObjectClass=user";
            Response.Redirect(Context.Request.ApplicationPath + "/UserManagement/IndividualObject/user/DefaultUser.aspx" + queryString);
        }
    }

    #endregion

    #region Search button on the filter was clicked

    private void GridFilter1_Search_OnClick()
    {
        LoadData();
    }


    #endregion

    #region Group List control Item Data Bound

    protected void GroupList_ItemDataBound1(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem ||
            e.Item.ItemType == ListItemType.SelectedItem)
        {

            LdapAttributeSet row = (LdapAttributeSet)e.Item.DataItem;

            string sam = Functions.GetAttributeValue(row, "cn");
            string des = Functions.GetAttributeValue(row, "description");
            string distinguishedName = Functions.GetAttributeValue(row, "distinguishedName");


            LinkButton lblName = (LinkButton)e.Item.FindControl("lnkGroupName");
            Label lbldesc = (Label)e.Item.FindControl("lblDescription");
            System.Web.UI.WebControls.Image ItemPopZone = (System.Web.UI.WebControls.Image)e.Item.FindControl("ItemPopZone");

            lblName.Text = sam;
            lbldesc.Text = des;

            string longDesc = "&DistinguishedName=" + distinguishedName;
            longDesc += "&ObjectClass=group";

            longDesc += "&DBM=USRMEM,USEGPM,USRHOM,USRAPP,USRPRS,USRDEL,GRPDEL";

            // If the user is NOT allowed to edit this
            //longDesc += ",GRPMEM";
           
            ItemPopZone.Attributes.Add("LongDesc", longDesc);

            lblName.CausesValidation = false;

            lblName.CommandName = distinguishedName;
            lblName.CommandArgument = sam + "|" + des;

        }
    }
    #endregion

    #region User List control Item Data Bound

    protected void UserList_ItemDataBound1(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem ||
                e.Item.ItemType == ListItemType.SelectedItem)
        {

            tUserListing row = (tUserListing)e.Item.DataItem;

            string DN = row.DN;
            string samAccountName = row.SamAccountName;

            System.Web.UI.WebControls.Image ItemPopZone = (System.Web.UI.WebControls.Image)e.Item.FindControl("ItemPopZone");
            LinkButton lnkSamAccountName = (LinkButton)e.Item.FindControl("lnkSamAccountName");
            Label lblPersonnelNumber = (Label)e.Item.FindControl("lblPersonnelNumber");            
            Label lblGivenName = (Label)e.Item.FindControl("lblGivenName");
            Label lblSN = (Label)e.Item.FindControl("lblSN");

            lnkSamAccountName.Text = samAccountName;
            lnkSamAccountName.CausesValidation = false;
            lnkSamAccountName.CommandName = DN;
            lnkSamAccountName.CommandArgument = samAccountName;

            lblPersonnelNumber.Text = row.PersonnelNumber;
            lblSN.Text = row.LastName;
            lblGivenName.Text = row.FirstName;

            string longDesc = "&DistinguishedName=" + DN;

            longDesc += "&HeaderLabel=Personnel Number: " + samAccountName;
            longDesc += "&Filter=" + this.FilterValue.Replace("*", "");
            longDesc += "&ListObjects=true";

            longDesc += "&ObjectClass=user";

            longDesc += "&DBM=GRPPRP,GRPMEM,USRPRS,USRDEL,GRPDEL";

            //longDesc = Server.UrlEncode(longDesc);

            ItemPopZone.Attributes.Add("LongDesc", longDesc);
        }
    }


    #endregion


    protected void GridFilter1_Load(object sender, EventArgs e)
    {

    }
}
