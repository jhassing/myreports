<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjectListing.ascx.cs" Inherits="UserManagement_ObjectListing_ObjectListing" %>

<%@ Register TagPrefix="uc1" TagName="Message" Src="../Common/Message.ascx" %>
<%@ Register TagPrefix="uc1" TagName="MyReportsGroupType" Src="../../Common/MyReportsGroupType.ascx" %>
<%@ Register TagPrefix="uc1" TagName="GridFilter" Src="../../Common/GridFilter.ascx" %>

<asp:panel id="panListingPanel" Runat="server" Visible="True">
	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
		<TR style="PADDING-BOTTOM: 6px; PADDING-TOP: 3px">
			<TD style="HEIGHT: 22px" noWrap colSpan="1">
				<uc1:GridFilter id="GridFilter1" runat="server"></uc1:GridFilter>&nbsp;&nbsp;
				<uc1:MyReportsGroupType id="MyReportsGroupType1" runat="server"></uc1:MyReportsGroupType></TD>
		</TR>
		<TR>
			<TD vAlign="top" noWrap align="left">
				<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
					<asp:panel id="panObjectListing" Runat="server" Visible="True">
						<TBODY>
							<TR>
								<TD class="TableHeaderRow" noWrap width="1">&nbsp;&nbsp;&nbsp;&nbsp;</TD>
								<TD class="TableHeaderRow" noWrap align="left">
									<asp:linkbutton id="lnkName" runat="server" CssClass="X-SmallBold_White">Name</asp:linkbutton></TD>
								<TD class="TableHeaderRow" noWrap align="left" width="100%">
									<asp:linkbutton id="lnkDescription" runat="server" CssClass="X-SmallBold_White">Description</asp:linkbutton></TD>
							</TR>
							<asp:repeater id="GroupList" runat="server" OnItemDataBound="GroupList_ItemDataBound1">
								<HeaderTemplate>
								</HeaderTemplate>
								<AlternatingItemTemplate>
									<tr runat="server" class="AltTableRow" ID="Tr1" height="1">
										<td nowrap width="1" align="right">
											<asp:Image onmouseover="javascript:activatePopup(this)" onmouseout="javascript:deActivatePopup(this)"
												onclick="javascript:activatePopupImmediately()" border="0" id="ItemPopZone" Runat="server"
												CssClass="popupMenuSelection" ImageUrl="../../images/popupmenu.gif" />
										</td>
										<td nowrap>
											<asp:LinkButton id="lnkGroupName" CssClass="hyperlink" runat="server" OnCommand="lblName_Command"></asp:LinkButton>
										</td>
										<td nowrap width="100%">
											<asp:Label id="lblDescription" CssClass="X-Small" runat="server"></asp:Label>
										</td>
									</tr>
								</AlternatingItemTemplate>
								<ItemTemplate>
									<tr runat="server" class="TableRow" ID="Tr2" height="1">
										<td nowrap width="1" align="right">
											<asp:Image onmouseover="javascript:activatePopup(this)" onmouseout="javascript:deActivatePopup(this)"
												onclick="javascript:activatePopupImmediately()" border="0" id="ItemPopZone" Runat="server"
												CssClass="popupMenuSelection" ImageUrl="../../images/popupmenu.gif" />
										</td>
										<td nowrap>
											<asp:LinkButton id="lnkGroupName" CssClass="hyperlink" runat="server" OnCommand="lblName_Command"></asp:LinkButton>
										</td>
										<td nowrap width="100%">
											<asp:Label id="lblDescription" CssClass="X-Small" runat="server"></asp:Label>
										</td>
									</tr>
								</ItemTemplate>
							</asp:repeater>
							<TR height="100%">
								<TD vAlign="top" colSpan="3">&nbsp;
									<asp:Label id="lblNoGroups" Runat="server" Visible="False" CssClass="X-Small">No groups to display.</asp:Label></TD>
							</TR>
					</asp:panel>
					<asp:panel id="panUserListing" Runat="server" Visible="True">
						<TR>
							<TD class="TableHeaderRow" noWrap width="1">&nbsp;&nbsp;&nbsp;&nbsp;</TD>
							<TD class="TableHeaderRow" noWrap align="left">
								<asp:linkbutton id="Linkbutton1" runat="server" CssClass="X-SmallBold_White">Login ID</asp:linkbutton></TD>
							<TD class="TableHeaderRow" noWrap align="left">
								<asp:linkbutton id="Linkbutton4" runat="server" CssClass="X-SmallBold_White">Personnel #</asp:linkbutton></TD>								
							<TD class="TableHeaderRow" noWrap align="left">
								<asp:linkbutton id="Linkbutton3" runat="server" CssClass="X-SmallBold_White">First Name</asp:linkbutton></TD>
							<TD class="TableHeaderRow" noWrap align="left" width="100%">
								<asp:linkbutton id="Linkbutton2" runat="server" CssClass="X-SmallBold_White">Last Name</asp:linkbutton></TD>
						</TR>
						<asp:repeater id="UserList" runat="server" OnItemDataBound="UserList_ItemDataBound1">
							<HeaderTemplate>
							</HeaderTemplate>
							<AlternatingItemTemplate>
								<tr runat="server" class="AltTableRow" ID="Tr7" height="1">
									<td nowrap width="1" align="right">
										<asp:Image onmouseover="javascript:activatePopup(this)" onmouseout="javascript:deActivatePopup(this)"
											onclick="javascript:activatePopupImmediately()" border="0" id="ItemPopZone" Runat="server"
											CssClass="popupMenuSelection" ImageUrl="../../images/popupmenu.gif" />
									</td>
									<td nowrap>
										<asp:LinkButton id="lnkSamAccountName" CssClass="hyperlink" runat="server" OnCommand="lblName_Command"></asp:LinkButton>
									</td>
					                <td nowrap>
										<asp:Label id="lblPersonnelNumber" CssClass="X-Small" runat="server"></asp:Label>&nbsp;
									</td>									
									<td nowrap>
										<asp:Label id="lblGivenName" CssClass="X-Small" runat="server"></asp:Label>&nbsp;
									</td>
									<td nowrap width="100%">
										<asp:Label id="lblSN" CssClass="XX-Small" runat="server"></asp:Label>
									</td>
								</tr>
							</AlternatingItemTemplate>
							<ItemTemplate>
								<tr runat="server" class="TableRow" ID="Tr8" height="1">
									<td nowrap width="1" align="right">
										<asp:Image  onmouseover="javascript:activatePopup(this)" onmouseout="javascript:deActivatePopup(this)"
											onclick="javascript:activatePopupImmediately()" border="0" id="ItemPopZone" Runat="server"
											CssClass="popupMenuSelection" ImageUrl="../../images/popupmenu.gif" />
									</td>
									<td nowrap>
										<asp:LinkButton id="lnkSamAccountName" CssClass="hyperlink" runat="server" OnCommand="lblName_Command"></asp:LinkButton>
									</td>
                                    <td nowrap>
										<asp:Label id="lblPersonnelNumber" CssClass="X-Small" runat="server"></asp:Label>&nbsp;
									</td>									
									<td nowrap>
										<asp:Label id="lblGivenName" CssClass="X-Small" runat="server"></asp:Label>&nbsp;
									</td>
									<td nowrap width="100%">
										<asp:Label id="lblSN" CssClass="X-Small" runat="server"></asp:Label>
									</td>
								</tr>
							</ItemTemplate>
						</asp:repeater>
						<TR height="100%">
							<TD vAlign="top" colSpan="3">&nbsp;
								<asp:Label id="lblNoUsers" Runat="server" Visible="False" CssClass="X-Small">No users to display.</asp:Label></TD>
						</TR>
					</asp:panel></TABLE>
			</TD>
		</TR>
		<TR height="100%">
			<TD vAlign="top" colSpan="5">&nbsp;
				<uc1:Message id="Message1" runat="server"></uc1:Message></TD>
		</TR>
		</TABLE>
</asp:panel><iframe id="ItemPopUpMenuFrame" border="0" marginWidth="0" marginHeight="0" src="popup.htm"
	frameBorder="0" scrolling="no"></iframe>
<script language=JavaScript 
src="<%=Context.Request.ApplicationPath%>/js/itempopupmenuparent.js"></script>
