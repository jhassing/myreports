<%@ Control Language="c#" Inherits="DistributionPortal.UserManagementUI.HomePage" CodeFile="Home.ascx.cs" %>
<table border="0" cellpadding="0" cellspacing="3" width="100%">
	<tr>
		<td colspan="3" Class="titlebar" width="100%">
			<asp:Label runat="server" Text="USER_MANAGEMENT_HOME_HEADER" id="USER_MANAGEMENT_HOME_HEADER"
				class="X-SmallBold_White" meta:resourcekey="USER_MANAGEMENT_HOME_HEADERResource1" />
		</td>
	</tr>
	<tr>
		<td nowrap style="height: 19px">&nbsp;&nbsp;</td>
		<td nowrap width="153" style="WIDTH: 153px; height: 19px;">
			<asp:LinkButton CommandName="UM_ShowGroupSearchScreen" Text="USER_MANAGEMENT_GROUP_LISTING_LABEL" CssClass="hyperlink" runat="server" id="LocalizedLinkButton1" meta:resourcekey="LocalizedLinkButton1Resource1" />
		</td>
		<td nowrap width="100%" style="height: 19px">
			<asp:Label Text="USER_MANAGEMENT_GROUP_LISTING_DESCRIPTION" CssClass="X-Small" runat="server"
				id="LocalizedLabel1" meta:resourcekey="LocalizedLabel1Resource1" />
		</td>
	</tr>
	<tr>
		<td nowrap>&nbsp;&nbsp;</td>
		<td nowrap width="153" style="WIDTH: 153px">
			<asp:LinkButton Text="USER_MANAGEMENT_USER_LISTING_LABEL" CssClass="hyperlink" runat="server" id="LocalizedLinkButton2" meta:resourcekey="LocalizedLinkButton2Resource1" CommandName="UM_ShowUserSearchScreen" />
		</td>
		<td nowrap width="400">
			<asp:Label Text="USER_MANAGEMENT_USER_LISTING_DESCRIPTION" CssClass="X-Small" runat="server"
				id="LocalizedLabel2" meta:resourcekey="LocalizedLabel2Resource1" />
		</td>
	</tr>
	<tr>
		<td nowrap>&nbsp;&nbsp;</td>
		<td nowrap width="153" style="WIDTH: 153px">
			<asp:LinkButton Visible=false CommandName="UM_ShowAddNewUserScreen" Text="USER_MANAGEMENT_ADD_NEW_USER_LABEL" CssClass="hyperlink" runat="server" id="LocalizedLinkButton3" meta:resourcekey="LocalizedLinkButton3Resource1" />
		</td>
		<td nowrap width="400">
			<asp:Label Visible=false Text="USER_MANAGEMENT_ADD_NEW_USER_DESCRIPTION" CssClass="X-Small" runat="server"
				id="LocalizedLabel3" meta:resourcekey="LocalizedLabel3Resource1" />
		</td>
	</tr>
</table>
