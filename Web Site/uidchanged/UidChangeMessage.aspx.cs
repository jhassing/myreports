using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UidChangeMessage : UidChangeRequest_BasePage
{
    private string _message = "";

    protected string NewNetworkID
    {
        get
        {
            return ViewState["NewNetworkID"].ToString();
        }
        set
        {
            ViewState["NewNetworkID"] = value;
        }
    }

    protected void writeTextHere()
    {
        //Response.Write("<b>Hello</b><i>Me and you </i>");
        Response.Write(_message);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // Get the user ID from the MyReports Common classes.
            string userID = MyReports.Common.AppValues.UserName;

            // Message string
            string message = "";
            string networkID = "";

            // Search the DB (cache) for the userID. We want to search the Perno field and see if the UserID and perno 
            // are a match.. If they are, we need to then find out if they already have another account to use.
            DataTable dt = base.LoadData();
            string searchStr = "Perno = '" + userID + "'";

            DataRow[] dr = (DataRow[])dt.Select(searchStr);

            if (dr.Length > 0)
            {
                // Found a match. Get the data from the DataRow
                string firstName = dr[0]["FirstName"].ToString().Trim();
                string lastName = dr[0]["lastName"].ToString().Trim();
                networkID = dr[0]["NetworkID"].ToString().Trim();
                this.NewNetworkID = networkID;

                bool exists = System.Convert.ToBoolean(System.Convert.ToInt32(dr[0]["Exists"].ToString().Trim()));

             //   bool exists = false;

                // Display message based on the "Exists" field
                if (exists)
                {
                    //// User already has an account, so basically tell them the account login ID is about to change
                    //message = "<b><font size=large>New Login ID Infomation</font></b>";
                    //message += "<br />";
                    //message += "<br />"; 
                    //message += firstName + " " + lastName + ",";
                    //message += "<br />";
                    //message += "<br />";
                    //message += "You have logged into the MyReports website with your Personnel Login ID  <b>" + userID + "</b>.";
                    //message += "<br />";
                    //message += "<br />";
                    //message += "<b><i>As of Wednesday 12/17/08, we began transitioning Personnel Login ID�s to Network Login ID�s when logging onto MyReports.</i></b>";
                    //message += "<b><i> Our records indicate you have both.  Please begin using your Network Login ID  <b><font color=blue size=4>" + networkID + "</font></b>";
                    //message += " immediately when logging into MyReports as your Personnel Login ID will no longer work for accessing MyReports after Monday 1/12/09. </i></b>";
                    //message += "<br />";
                    //message += "<br />";
                    //message += "Also, when logging in with the Network Login ID <b><font color=blue size=4>" + networkID + "</font></b>, you will be required to use the current password associated with this ID.";
                    //message += "<br />";
                    //message += "<br />";
                    //message += "If you do not know your Network Login password or experience difficulties logging in, ";
                    //message += "<br />";
                    //message += "please contact the PSG Help Desk at 1-888-295-7206 option 2.";
                    //message += "<br />";
                    //message += "<br />";
                    //message += "Please click the Continue button below continue into MyReports.";
                    //message += "<br />";
                    //message += "<br />";
                    //message += "Thank You,";
                    //message += "<br/>";
                    //message += "MyReports Project Team"; 

                    // Final Notice:  Tell them they are locked out
                    message = "<b><font size=large>New Login ID Infomation</font></b>";
                    message += "<br />";
                    message += "<br />";
                    message += firstName + " " + lastName + ",";
                    message += "<br />";
                    message += "<br />";
                    message += "<b>As of January 13, 2008, you can no longer use your Personnel Login ID <font color=blue>" + userID;
                    message += " </font>for accessing MyReports.";
                    message += "  You must now use your Network Login ID <font color=blue>" + networkID + " </font></b> and the current password associated with this ID to access MyReports.</b>";
                    message += "<br />";
                    message += "<br />";
                    message += "Please click the <b>Continue button</b> below to return to the login screen and use your  <b> Network Login ID <font color=blue>" + networkID + ".</font></b>";
                    message += "<br />";
                    message += "<br />";
                    message += "If you do not know your Network Login ID password or experience difficulties logging in, ";
                    message += "<br />";
                    message += "please contact the PSG Help Desk at 1-888-295-7206 option 2.";
                    message += "<br />";
                    message += "<br />";
                    message += "Thank You,";
                    message += "<br/>";
                    message += "MyReports Project Team"; 
                }
                else
                {
                    //string firstName = (string)Request.QueryString["FirstName"];
                    //string lastName = (string)Request.QueryString["LastName"];
                    //string networkID = (string)Request.QueryString["networkID"];
                    //string pernoID = MyReports.Common.AppValues.UserName;

                    // User does not have an account.. Tell them the account is going to be renamed..
                    //message = "<b><font size=4>New Login ID Infomation</font></b>";
                    //message += "<br />";
                    //message += "<br />";
                    //message += firstName + " " + lastName + ",";
                    //message += "<br />";  
                    //message += "<br />";
                    //message += "You have logged into the MyReports web site with your Personnel Login ID <b>" + userID + "</b>.";
                    //message += "<br />";
                    //message += "<br />";
                    //message += "<b><i>As of Wednesday 12/17/08, we began transitioning Personnel Login ID�s to Network Login ID�s when logging onto MyReports.</i></b>";
                    //message += "<b><i> Our records indicate you do not have a Network Login ID, so one has been created for you <b><font color=blue size=4>" + networkID + "</font></b>.";
                    //message += "<br />";
                    //message += "<br />";
                    //message += "Your password will change with the new Network Login ID <b><font color=blue size=4>" + networkID + "</font></b>";
                    //message += ". Therefore, you will need to contact the PSG Help Desk at 1-888-295-7206 option 2, to receive your new password.";
                    //message += " You may begin using your Network Login ID <b><font color=blue size=4>" + networkID + "</font></b>" + " to log into MyReports once you have obtained your new password.";
                    //message += " Your Personnel Login ID will no longer work for accessing MyReports after Monday 1/12/09.</i></b>";
                    //message += "<br />";
                    //message += "<br />";
                    //message += "If you have any difficulties logging in, please contact the PSG Help Desk at 1-888-295-7206 option 2.";
                    //message += "<br />";
                    //message += "<br />";
                    //message += "Please click the Continue button below continue into MyReports.";
                    //message += "<br />";
                    //message += "<br />";
                    //message += "Thank You,";
                    //message += "<br/>";
                    //message += "MyReports Project Team";                   

                    // Final Notice:  Tell them they are locked out
                    message = "<b><font size=large>New Login ID Infomation</font></b>";
                    message += "<br />";
                    message += "<br />";
                    message += firstName + " " + lastName + ",";
                    message += "<br />";
                    message += "<br />";
                    message += "<b>As of January 13, 2008, you can no longer use your Personnel Login ID <font color=blue>" + userID;
                    message += " </font>for accessing MyReports.";
                    message += "  You must now use your Network Login ID <font color=blue>" + networkID + " </font></b> and the current password associated with this ID to access MyReports.</b>";
                    message += "<br />";
                    message += "<br />";
                    message += "Please click the <b>Continue button</b> below to return to the login screen and use your  <b> Network Login ID <font color=blue>" + networkID + ".</font></b>";
                    message += "<br />";
                    message += "<br />";
                    message += "If you do not know your Network Login ID password or experience difficulties logging in, ";
                    message += "<br />";
                    message += "please contact the PSG Help Desk at 1-888-295-7206 option 2.";
                    message += "<br />";
                    message += "<br />";
                    message += "Thank You,";
                    message += "<br/>";
                    message += "MyReports Project Team"; 
                }

                //this.lblMessage.Text = message;
                this._message = message;

                this.lblHeader.Text = "&nbsp;New Login ID Information";
                // wait for button accept to proceed to the index.aspx page
            }
            else
            {
                // Did not find a match, proceed into the MyReports web site

                // Redirect here to the index.aspx page
                RedirectToIndexPage();
            }
        }

    }

    private void RedirectToIndexPage()
    {
        // Enter redirect string here
        Response.Redirect("../Index/Index.aspx", false);
    }

    protected void btnCont_Click(object sender, ImageClickEventArgs e)
    {
        // Go back to the login page
        Response.Redirect("../Login.aspx?newId=" + this.NewNetworkID, false);
      
        //  RedirectToIndexPage();
    }
}
