<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UidChangeMessage.aspx.cs" Inherits="UidChangeMessage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>User Notice</title>
    <link href="../css/loginstyles.css" type="text/css" rel="stylesheet">
</head>
<body>    
    <form id="form2" runat="server" ms_positioning="gridlayout">
        <table id=table1 cellspacing="0" cellpadding="0" width="170" border="0">
            <tr class="banner">
                <td width="170">
                    <img style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px;
                        border-right-width: 0px" alt="" src="../images/MyReportsLogo.gif" align="middle"></td>
            </tr>
            <tr>
                <td>
                    <table id=table2 style="width: 600px; border-right: #261f9c 2px solid; margin-top: 4px; border-left: #261f9c 2px solid;
                        border-bottom: #261f9c 2px solid" cellspacing="0" cellpadding="0" bgcolor="#d7e4f2"
                        border="0">
                        <tr id=tr1 valign="top" style="background: #261f9c">
                            <td id=td1 class="banner" valign="middle" nowrap height="20" style="width: 600px">
                                <asp:Label CssClass="css0002" runat="server" Text="UID_CHANGED_HEADER" ID="lblHeader" />
                            </td>
                        </tr>
                        <tr id=tr2 >
                            <td id=td2 valign="middle" class="css0063" style="width: 600px; height: 199px;" runat="server">
                                <span runat=server id=sdds></span>
                                <% writeTextHere(); %>
                                <!--<asp:Label Font-Size="Small" runat="server" Text="UID_CHANGED_BODY" ID="lblMessage" />-->
                            </td>
                        </tr>
                        <tr id=tr3 >
                            <td id=td3 valign="middle" nowrap style="height: 33px; width: 600px;"
                                align=center>
                                <asp:ImageButton ID="btnCont" runat="server" OnClick="btnCont_Click" ImageUrl="~/images/continue.gif" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>    
</body>
</html>
