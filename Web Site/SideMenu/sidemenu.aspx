<%@ Register TagPrefix="cc1" Namespace="ComputerBuff.Controls.XMenu" Assembly="XMenu" %>
<%@ Page Language="c#" Inherits="DistributionPortal.SideMenu" CodeFile="SideMenu.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<META http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta content="True" name="vs_showGrid">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="<%=Context.Request.ApplicationPath%>/css/allstyles.css" type="text/css" rel="stylesheet">
		<script language=javascript src="<%=Context.Request.ApplicationPath%>/js/XMenu.js"></script>
</HEAD>
	<body style="MARGIN-LEFT: 7px" leftmargin="0" rightmargin="0" topmargin="0" scroll="no">
		<form id="Form1" method="post" runat="server">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<asp:panel id="panMyReports" Visible="False" Runat="server">
<cc1:xmenu id=xmReportDistribution runat="server" Visible="true" MenuItemState="ExpandAll" State="Expanded" HeaderForeColor="Black" HeaderForeColor-IsSystemColor="False" HeaderForeColor-R="0" HeaderForeColor-G="0" HeaderForeColor-Name="Black" HeaderForeColor-IsKnownColor="True" HeaderForeColor-IsNamedColor="True" HeaderForeColor-B="0" HeaderForeColor-A="255" HeaderForeColor-IsEmpty="False" HeaderTextCssClass="XMHeaderTest" HeaderTextCssClass-Length="12" HeaderImageUrl-Length="0" SubmenuExpandImageUrl="../images/plus.gif" SubmenuExpandImageUrl-Length="29" SubmenuCollapseImageUrl="../images/minus.gif" SubmenuCollapseImageUrl-Length="30" Width-Value="160" Width-IsEmpty="False" Width-Type="Pixel" Width="160px" HeaderFont-Size="10pt" HeaderFont-Names="Verdana" BorderWidth="1px" BorderWidth-Value="1" BorderWidth-IsEmpty="False" BorderWidth-Type="Pixel" BorderStyle="Solid" HeaderText="My Reports" HeaderText-Length="10" ScriptPath="/MyReports/js/XMenu.js" ScriptPath-Length="25" MenuExpandImageUrl-Length="0" MenuCollapseImageUrl-Length="0">
<cc1:MenuItem Visible="True" Text="My Home Folder" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="6f136e491a44f298304f653e24eae82" ForeColor="Black" NavigateUrl="" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
<cc1:MenuItem Visible="True" Text="MySelected Files" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="6g136e491a44f298304f653e24eae82" ForeColor="Black" NavigateUrl="../index/index.aspx?doframe=false&amp;app=rd_SelectedFiles" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
<cc1:MenuItem Visible="True" Text="(0) Archives" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="7fb2c1953694a7f89881791dce1ac71" ForeColor="Black" NavigateUrl="../index/index.aspx?doframe=false&amp;app=rd&amp;amp;&amp;folder=%2f(0)+Archives" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
<cc1:MenuItem Visible="True" Text="(1) Accounting" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="a8333f2a3264e93b310ecdf87de313c" ForeColor="Black" NavigateUrl="../index/index.aspx?doframe=false&amp;app=rd&amp;amp;&amp;folder=%2f(1)+Accounting" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
<cc1:MenuItem Visible="True" Text="(2) District" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="4f2b8b35621426da322644af46850be" ForeColor="Black" NavigateUrl="../index/index.aspx?doframe=false&amp;app=rd&amp;amp;&amp;folder=%2f(2)+District" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
<cc1:MenuItem Visible="True" Text="(3) Operations" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="497e63dbed14f00bdd9288797ddea9b" ForeColor="Black" NavigateUrl="../index/index.aspx?doframe=false&amp;app=rd&amp;amp;&amp;folder=%2f(3)+Operations" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
<cc1:MenuItem Visible="True" Text="(4) Payroll" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="497f83dbed14f00bdd9288797ddea9b" ForeColor="Black" NavigateUrl="../index/index.aspx?doframe=false&amp;app=rd&amp;amp;&amp;folder=%2f(4)+Payroll" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
<cc1:MenuItem Visible="True" Text="(5) Benefits" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="497f83dbed14f00bdd9288797ddea9b" ForeColor="Black" NavigateUrl="../index/index.aspx?doframe=false&amp;app=rd&amp;amp;&amp;folder=%2f(5)+Benefits" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
<cc1:MenuItem Visible="True" Text="(8) Viewed Reports" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="497f83dbed14f00bdd9288797ddea9b" ForeColor="Black" NavigateUrl="../index/index.aspx?app=viewed_reports" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
</cc1:xmenu>
						</asp:panel>
					</td>
				</tr>
				<tr style="PADDING-TOP: 6px">
					<td>
						<asp:panel id="panBudgetDistribution" Visible="False" Runat="server">
<cc1:xmenu id=xmBudgetDistribution runat="server" MenuItemState="ExpandAll" State="Expanded" HeaderForeColor="Black" HeaderForeColor-IsSystemColor="False" HeaderForeColor-R="0" HeaderForeColor-G="0" HeaderForeColor-Name="Black" HeaderForeColor-IsKnownColor="True" HeaderForeColor-IsNamedColor="True" HeaderForeColor-B="0" HeaderForeColor-A="255" HeaderForeColor-IsEmpty="False" HeaderTextCssClass="XMHeaderTest" HeaderTextCssClass-Length="12" SubmenuExpandImageUrl="../images/plus.gif" SubmenuExpandImageUrl-Length="29" SubmenuCollapseImageUrl="images/minus.gif" SubmenuCollapseImageUrl-Length="16" Width-Value="160" Width-IsEmpty="False" Width-Type="Pixel" Width="160px" HeaderFont-Size="10pt" HeaderFont-Names="Verdana" BorderWidth="1px" BorderWidth-Value="1" BorderWidth-IsEmpty="False" BorderWidth-Type="Pixel" BorderStyle="Solid" HeaderText="Budget Distribution" HeaderText-Length="19" ScriptPath="/MyReports/js/XMenu.js" ScriptPath-Length="25" MenuExpandImageUrl-Length="0" MenuCollapseImageUrl-Length="0">
<cc1:MenuItem Visible="True" Text="Budget Distribution" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="83b40272551497296ebcc863261f989" ForeColor="Black" NavigateUrl="../index/index.aspx?app=bd" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
<cc1:MenuItem  Visible="False" Text="YEA Budget Trim" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="83b40272551497296ebcc863261f989" ForeColor="Black" NavigateUrl="../index/index.aspx?app=yea" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>

</cc1:xmenu>
<%-- <asp:Button ID="btnYEA" text = "YEA" runat=server OnClick="btnYEA_Click" /> --%>
						</asp:panel>
					</td>
				</tr>
				<tr style="PADDING-TOP: 6px">
					<td>
						<asp:panel id="panBlueBook" Visible="False" Runat="server">
<cc1:xmenu id=xmBlueBook runat="server" MenuItemState="ExpandAll" State="Expanded" HeaderForeColor="Black" HeaderForeColor-IsSystemColor="False" HeaderForeColor-R="0" HeaderForeColor-G="0" HeaderForeColor-Name="Black" HeaderForeColor-IsKnownColor="True" HeaderForeColor-IsNamedColor="True" HeaderForeColor-B="0" HeaderForeColor-A="255" HeaderForeColor-IsEmpty="False" HeaderTextCssClass="XMHeaderTest" HeaderTextCssClass-Length="12" SubmenuExpandImageUrl="../images/plus.gif" SubmenuExpandImageUrl-Length="18" SubmenuCollapseImageUrl="images/minus.gif" SubmenuCollapseImageUrl-Length="16" Width-Value="160" Width-IsEmpty="False" Width-Type="Pixel" Width="160px" HeaderFont-Size="10pt" HeaderFont-Names="Verdana" BorderWidth="1px" BorderWidth-Value="1" BorderWidth-IsEmpty="False" BorderWidth-Type="Pixel" BorderStyle="Solid" HeaderText="Blue Book Files" HeaderText-Length="15" ScriptPath="/MyReports/js/XMenu.js" ScriptPath-Length="25" MenuExpandImageUrl-Length="0" MenuCollapseImageUrl-Length="0">
<cc1:MenuItem Visible="True" Text="Blue Book Distribution" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="a006499e9d74183b76b90d1f3eed654" ForeColor="Black" NavigateUrl="../index/index.aspx?app=bbd&amp;subpage=_list&amp;folder=%2fBlue+Book+Distribution" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
</cc1:xmenu>
						</asp:panel>
					</td>
				</tr>
				<tr style="PADDING-TOP: 6px">
					<td>
						<asp:panel id="panUserManagement" Visible="False" Runat="server">
<cc1:xmenu id=xmNewUserRequests runat="server" MenuItemState="ExpandAll" State="Expanded" HeaderForeColor="Black" HeaderForeColor-IsSystemColor="False" HeaderForeColor-R="0" HeaderForeColor-G="0" HeaderForeColor-Name="Black" HeaderForeColor-IsKnownColor="True" HeaderForeColor-IsNamedColor="True" HeaderForeColor-B="0" HeaderForeColor-A="255" HeaderForeColor-IsEmpty="False" HeaderTextCssClass="XMHeaderTest" HeaderTextCssClass-Length="12" Width-Value="160" Width-IsEmpty="False" Width-Type="Pixel" Width="160px" HeaderFont-Size="10pt" HeaderFont-Names="Verdana" BorderWidth="1px" BorderWidth-Value="1" BorderWidth-IsEmpty="False" BorderWidth-Type="Pixel" BorderStyle="Solid" HeaderText="User Management" HeaderText-Length="15" ScriptPath="/MyReports/js/XMenu.js" ScriptPath-Length="25" MenuExpandImageUrl-Length="0" MenuCollapseImageUrl-Length="0">
<cc1:MenuItem Visible="True" Text="Home" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="e671d00e5a04102b1e43e20ff56099f" ForeColor="Black" NavigateUrl="../index/index.aspx?app=user_management&amp;Home=0" CssClass="XMenuItem" State="Collapsed" BackColor="White" Target="Top"></cc1:MenuItem>
</cc1:xmenu>
						</asp:panel>
					</td>
				</tr>
				<tr style="PADDING-TOP: 6px">
					<td>
						<asp:panel id="panPassport" Visible="False" Runat="server">
<cc1:xmenu id=xmPassport runat="server" MenuItemState="ExpandAll" State="Expanded" HeaderForeColor="Black" HeaderForeColor-IsSystemColor="False" HeaderForeColor-R="0" HeaderForeColor-G="0" HeaderForeColor-Name="Black" HeaderForeColor-IsKnownColor="True" HeaderForeColor-IsNamedColor="True" HeaderForeColor-B="0" HeaderForeColor-A="255" HeaderForeColor-IsEmpty="False" HeaderTextCssClass="XMHeaderTest" HeaderTextCssClass-Length="12" Width-Value="160" Width-IsEmpty="False" Width-Type="Pixel" Width="160px" HeaderFont-Size="10pt" HeaderFont-Names="Verdana" BorderWidth="1px" BorderWidth-Value="1" BorderWidth-IsEmpty="False" BorderWidth-Type="Pixel" BorderStyle="Solid" HeaderText="Passport" HeaderText-Length="15" ScriptPath="/MyReports/js/XMenu.js" ScriptPath-Length="25" MenuExpandImageUrl-Length="0" MenuCollapseImageUrl-Length="0">
<cc1:MenuItem Visible="True" Text="Passport" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="d614cd57aec4602a87fcab681eafwd9" ForeColor="Black" NavigateUrl="../index/index.aspx?app=passport" CssClass="XMenuItem" State="Expanded" BackColor="White" Target="Top"></cc1:MenuItem>
</cc1:xmenu>
						</asp:panel>
					</td>
				</tr>
				<tr style="PADDING-TOP: 6px">
					<td>
						<asp:panel id="panForms" Visible="True" Runat="server">
<cc1:xmenu id=xmForms runat="server" MenuItemState="ExpandAll" State="Expanded" HeaderForeColor="Black" HeaderForeColor-IsSystemColor="False" HeaderForeColor-R="0" HeaderForeColor-G="0" HeaderForeColor-Name="Black" HeaderForeColor-IsKnownColor="True" HeaderForeColor-IsNamedColor="True" HeaderForeColor-B="0" HeaderForeColor-A="255" HeaderForeColor-IsEmpty="False" HeaderTextCssClass="XMHeaderTest" HeaderTextCssClass-Length="12" Width-Value="160" Width-IsEmpty="False" Width-Type="Pixel" Width="160px" HeaderFont-Size="10pt" HeaderFont-Names="Verdana" BorderWidth="1px" BorderWidth-Value="1" BorderWidth-IsEmpty="False" BorderWidth-Type="Pixel" BorderStyle="Solid" HeaderText="Field References" HeaderText-Length="15" ScriptPath="/MyReports/js/XMenu.js" ScriptPath-Length="25" MenuExpandImageUrl-Length="0" MenuCollapseImageUrl-Length="0">
<cc1:MenuItem Visible="True" Text="Policies & Forms" ImageUrl="../images/closedfoldericon.gif" ShowNewImage="False" MenuId="d614cd57aec4602a87fcab681eafwd9" ForeColor="Black" NavigateUrl="https://mycompass.compass-usa.com/mc3/libraries/corporate/gallery.asp?categoryid=168" CssClass="XMenuItem" State="Expanded" BackColor="White" Target="Blank"></cc1:MenuItem>
</cc1:xmenu>
						</asp:panel>
					</td>
				</tr>				
			</table>
		</form>
	</body>
</HTML>
