namespace DistributionPortal
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using ComputerBuff.Controls.XMenu;
	using System.Web.UI;

	using activeportal.classes;
	using activeportal.classes.api.proxy;
	using activeportal.classes.api;
	using actuate_i18n;

	//[PartialCaching(300)]
	public partial class SideMenu : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.WebControls.Panel panNationalAccounts;
		//protected ComputerBuff.Controls.XMenu.XMenu xmReportDistribution;
		protected ComputerBuff.Controls.XMenu.XMenu xmNationalAccounts;
		//rotected ComputerBuff.Controls.XMenu.XMenu xmBlueBook;
		//protected ComputerBuff.Controls.XMenu.XMenu xmNewUserRequests;
		//protected ComputerBuff.Controls.XMenu.XMenu xmBudgetDistribution;
		protected System.Web.UI.WebControls.Label lblTimeSidebar;
		//protected ComputerBuff.Controls.XMenu.XMenu xmNationalAccounts;

		
		protected void Page_Load(object sender, System.EventArgs e)
		{
            

			if (Visible)
			{	
				
				#region My Reports

				// Home Folder
				string HomeFolderTarget = "../index/index.aspx?app=rd&subpage=_list" + "&folder=";
				HomeFolderTarget += HttpUtility.UrlEncode( AcParams.Get( Page, AcParams.Name.homeFolder, "/" ) );

				
				if ( new DistributionPortal.GroupMembership().MyReportsUser  )
				{				
					this.panMyReports.Visible = true;
					this.xmReportDistribution.Visible = true;
					xmReportDistribution.CssClass="XMenu_Out";

					xmReportDistribution.MenuItems[0].NavigateUrl = HomeFolderTarget;

                    if(!MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin"))
                        xmReportDistribution.MenuItems.RemoveAt(1);

                    if (MyReports.Common.AppValues.IsInRole("MyReportsAdministrators"))
						xmReportDistribution.MenuItems.Add( GetMenuItem("(9) Administration", "../index/index.aspx?app=rd_admin") );
				}
				else
				{
					this.xmReportDistribution.Visible = false;
					this.panMyReports.Visible = false;
				}		

				#endregion												
			
				#region Budget Distribution

                if  (MyReports.Common.AppValues.IsInRole("BudgetDistributionAdmins"))
				    {}
				else
				    {
                        this.xmBudgetDistribution.MenuItems.RemoveAt(1);
                    }
            
                

                if (MyReports.Common.AppValues.IsInRole("BudgetDistributionUsers"))
				{
					this.panBudgetDistribution.Visible = true;
                 	this.xmBudgetDistribution.Visible = true;
                                        
					xmBudgetDistribution.CssClass="XMenu_Out";
				}
				else
				{
					this.panBudgetDistribution.Visible = false;
					this.xmBudgetDistribution.Visible = false;
				}

				#endregion
				
				#region National Accounts

				/*
				if (Global.IsInRole( "NationalAccountsUser"))
				{
					// National Accounts Menu Items
					
							xmNationalAccounts.CssClass="XMenu_Out";

							xmNationalAccounts.AddMenuItem("Commission","",ComputerBuff.Controls.XMenu.TargetType.Top);
							xmNationalAccounts.MenuItems[0].ImageUrl="../images/closedfoldericon.gif";
							xmNationalAccounts.MenuItems[0].CssClass="XMenuItem";
			
							xmNationalAccounts.AddMenuItem("Management","",ComputerBuff.Controls.XMenu.TargetType.Top);
							xmNationalAccounts.MenuItems[1].ImageUrl="../images/closedfoldericon.gif";
							xmNationalAccounts.MenuItems[1].CssClass="XMenuItem";

							xmNationalAccounts.AddMenuItem("Profitability","",ComputerBuff.Controls.XMenu.TargetType.Top);
							xmNationalAccounts.MenuItems[2].ImageUrl="../images/closedfoldericon.gif";
							xmNationalAccounts.MenuItems[2].CssClass="XMenuItem";

							xmNationalAccounts.State = State.Expanded;

							xmNationalAccounts.Visible = true;
							
				}
				*/
				#endregion

				#region Blue Book Distribution
				if ( new DistributionPortal.GroupMembership().BlueBookDistributionUser )
				{
					this.panBlueBook.Visible = true;
					this.xmBlueBook.Visible = true;
					xmBlueBook.CssClass="XMenu_Out";
				
				}
				else
				{
					this.panBlueBook.Visible = false;
					this.xmBlueBook.Visible = false;
				}
				#endregion
									
				#region User Management

                if ((MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin")) ||
                        (MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin")) ||
                        (MyReports.Common.AppValues.IsInRole("PassportAdmins")) ||
                        (MyReports.Common.AppValues.IsInRole("BlueBookDistributionAdmins")) ||
                        (MyReports.Common.AppValues.IsInRole("MyUnitPortalUserMembership")))
				
				{
					xmNewUserRequests.CssClass="XMenu_Out";

					xmNewUserRequests.Visible = true;
					panUserManagement.Visible = true;	
				}				
				else
				{
					//xmNewUserRequests.Visible = false;
					panUserManagement.Visible =false;
				}
			
				#endregion

				#region Passport

                if ((MyReports.Common.AppValues.IsInRole("PassportAdmins")))
				{
					xmPassport.CssClass="XMenu_Out";
					// enable all options
					xmPassport.MenuItems[0].Visible = true;
						
					xmPassport.MenuItems.Add( GetMenuItem("Reports", "../index/index.aspx?app=passport_admin&folder=%2fPassport%2fReports") );
				
					xmPassport.Visible = true;
					panPassport.Visible = true;	
				}
				//else if ( (Global.IsInRole("PassportUsers")) )
				else if ( new DistributionPortal.GroupMembership().PassportUser )
				{
					xmPassport.CssClass="XMenu_Out";
					// Just show the user membership sidemenu item under User Management
					xmPassport.MenuItems[0].Visible = true;
					xmPassport.Visible = true;
					panPassport.Visible = true;	
				}
				else
					panPassport.Visible =false;
			
				#endregion

                #region Forms

                if (MyReports.Common.AppValues.IsInRole("MyReportsAdministrators"))
                {
                    xmForms.CssClass = "XMenu_Out";
                    // enable all options
                    xmForms.MenuItems[0].Visible = true;

                    xmForms.Visible = true;
                    panForms.Visible = true;
                }
                else
                {
                    xmForms.MenuItems[0].Visible = false;

                    xmForms.Visible = false;
                    panForms.Visible = false;
                }
                #endregion

                // DELETE THIS
				//lblTimeSidebar.Text = System.DateTime.Now.ToString();			
			}
		}

        private ComputerBuff.Controls.XMenu.MenuItem GetMenuItem(string text, string url)
		{
            ComputerBuff.Controls.XMenu.MenuItem mt = new ComputerBuff.Controls.XMenu.MenuItem();

			
			mt.Visible = true;
			mt.Text = text;
			mt.ImageUrl ="../images/closedfoldericon.gif" ;
			mt.ShowNewImage = false;
			mt.ForeColor = System.Drawing.Color.Black;
			mt.NavigateUrl = url;
			mt.CssClass="XMenuItem";
			mt.State = State.Collapsed;
			mt.Target = TargetType.Top;
			mt.BackColor = System.Drawing.Color.White;
        
			return mt;
		}
		
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
//			InitializeComponent();
			base.OnInit(e);
		}
        //protected void btnYEA_Click(object sender, EventArgs e)
        //{
        //    Response.Redirect("/BudgetDistribution/YEA.aspx");
        //}
}
}
