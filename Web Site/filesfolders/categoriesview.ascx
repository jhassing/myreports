<%@ Reference Control="~/filesfolders/baseview.ascx" %>
<%@ Register TagPrefix="acwc" Namespace="actuate.classes.webcontrols" Assembly="activeportal"%>
<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.filesfolders.categoriesview" CodeFile="categoriesview.ascx.cs" %>
<%@ Register TagPrefix="actuate" TagName="filter" Src="../common/filter.ascx" %>
<%@ Register TagPrefix="actuate" TagName="separator" Src="../common/separator.ascx" %>
<%--	
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<script language=javascript 
src="<%=Context.Request.ApplicationPath%>/js/general.js"></script>
<actuate:filter id="AcFilter" runat="server" Visible="false" Filter="*" ShowLatest="false" ShowDocuments="true" ShowExecutables="true"
	ShowFolders="true"></actuate:filter>
<%-- Folders --%>
<localized:aclabelcontrol id="AcLblNoFolders" runat="server" Visible="false" EnableViewState="false" Key="LBL_NO_SUBFOLDERS"
	CssClass="label">
	<argument id="AcArgNoFolders" />
</localized:aclabelcontrol><localized:aclabelcontrol id="AcLblEmptyFolder" runat="Server" Visible="false" EnableViewState="false" Key="MSG_EMPTY_FOLDER"
	CssClass="label"></localized:aclabelcontrol><asp:label id="AcNumSearchElements" runat="server" CssClass="label"></asp:label><asp:literal id="AcSearchSeparator" runat="server" Text="<br>"></asp:literal>
<asp:datalist id="FolderList" runat="server" EnableViewState="false" cellpadding="3" cellspacing="0"
	Width="100%" border="0" RepeatColumns="3" RepeatDirection="Vertical">
	<HeaderStyle width="100%" CssClass="titlebar"></HeaderStyle>
	<HeaderTemplate>
		<localized:AcLabelControl CssClass="X-SmallBold_White" Key="MSGT_FOLDERS" runat="server" />
	</HeaderTemplate>
	<ItemStyle Width="33%" verticalalign="middle"></ItemStyle>
	<ItemTemplate>
		<a href="<%#GetFolderNavigateURL(Container.DataItem)%>" >
			<asp:image id="AcImgFolderIcon" align="top" runat="server" imageurl="../images/foldericon.gif" /></a>
		<a class="hyperlink" href="<%#GetFolderNavigateURL(Container.DataItem)%>">
			<%# HttpUtility.HtmlEncode(((activeportal.classes.api.proxy.File)Container.DataItem).Name)%>
		</a>
	</ItemTemplate>
	<FooterTemplate>
		&nbsp;
	</FooterTemplate>
</asp:datalist>
<%-- Reports --%>
<asp:panel id="panReports" Runat="server">
	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
		<TR width="100%">
			<TD width="100%" colSpan="4"><IMG height="4" src="<%=Context.Request.ApplicationPath%>/images/vspace.gif" width="100%" border="0"></TD>
		</TR>
		<asp:repeater id="ReportList" runat="server" EnableViewState="false" OnItemDataBound="ReportList_OnItemDataBound">
			<HeaderTemplate>
				<tr class="titlebar">
					<td colspan="7">
						<table border="0" width="100%" cellspacing="0" cellpadding="3">
							<tr class="titlebar">
								<td width="100%" class="titlebar"">
									&nbsp;
									<localized:AcLabelControl Key="MSGT_EXES" runat="server" ID="Aclabelcontrol1" NAME="Aclabelcontrol1" />
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr width="100%">
					<td colspan="7" width="100%"><img src="<%=Context.Request.ApplicationPath%>/images/vspace.gif" width="100%" height="4" border="0"></td>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td width="40%" valign="middle" colspan="2" nowrap>
						<asp:Image id="AcImgReportIcon" Width="16" Height="16" ImageAlign="top" runat="server" />
						<acwc:AcSafeHyperLink id="AcLnkReport" CssClass="hyperlink" runat="server" />
					</td>
					<td width="20%" valign="middle" nowrap>
						<a class="hyperlink" href="<%#GetExecuteReportURL( Container.DataItem )%>">
							<%# HttpUtility.HtmlEncode(((activeportal.classes.api.proxy.File)Container.DataItem).VersionName)%>
						</a>
					</td>
					<td width="35%" valign="middle" colspan="3" nowrap>&nbsp;
						<acwc:acsafelabel ID="AcTimeStampLbl2" CssClass="label" RunAt="server" /></td>
				</tr>
			</ItemTemplate>
			<SeparatorTemplate>
				<actuate:separator id="Separator1" runat="server" />
			</SeparatorTemplate>
			<FooterTemplate>
				<tr>
					<td colspan="7">
						<img src="<%=Context.Request.ApplicationPath%>/images/vspace.gif" width="100%" height="4" border="0">
					</td>
				</tr>
			</FooterTemplate>
		</asp:repeater></TABLE>
</asp:panel>
<asp:panel id="panDocuments" Runat="server">
	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
		<TR>
			<TD class="TableHeaderRow" id="tdReports" vAlign="middle" noWrap width="45%" runat="server">
				<asp:LinkButton id="lblReports" runat="server" CssClass="X-SmallBold_White" onclick="lblReports_Click">Reports</asp:LinkButton>&nbsp;
				<asp:Label id="ReportCount" CssClass="X-SmallBold_White" Runat="server"></asp:Label></TD>
			<TD class="TableHeaderRow" id="tdVersions" vAlign="middle" noWrap width="20%" runat="server">
				<asp:Label id="lblVersions" CssClass="X-SmallBold_White" Runat="server">Viewed</asp:Label></TD>
			<TD class="TableHeaderRow" id="tdTimeStamp" vAlign="middle" noWrap width="17%" runat="server">
				<asp:LinkButton id="lnkTimeStamp" runat="server" CssClass="X-SmallBold_White" onclick="lnkTimeStamp_Click">Time Stamp</asp:LinkButton></TD>
			<TD class="TableHeaderRow" id="tdPageCount" noWrap align="right" width="18%" runat="server">
				<asp:LinkButton id="lnkPageCount" runat="server" CssClass="X-SmallBold_White" onclick="lnkPageCount_Click">Page Count</asp:LinkButton></TD>
		</TR>
		<TR width="100%">
			<TD width="100%" colSpan="4"><IMG height="4" src="<%=Context.Request.ApplicationPath%>/images/vspace.gif" width="100%" border="0"></TD>
		</TR>
		<%-- Documents --%>
		<asp:repeater id="DocumentList" runat="server" EnableViewState="true" OnItemDataBound="DocumentList_OnItemDataBound">
			<ItemTemplate>
				<%#ComputeFirstDocInList((activeportal.classes.api.proxy.File)Container.DataItem)%>
				<tr id="lll" runat="server" style="PADDING-TOP: 6px">
					<td valign="middle" nowrap>
                        <asp:CheckBox OnCheckedChanged="chkSelected_CheckedChanged" ID="chkSelected" runat="server" AutoPostBack="true" />
						<%# ShowIcon( Container.DataItem ) %>
						<acwc:AcSafeHyperLink id="AcLnkDocumentName" CssClass="hyperlink" runat="server" />
					</td>
					<td valign="middle" nowrap>
						<asp:Label ID="lblViewed" CssClass="label" Runat="server" />
					</td>
					<td valign="middle" nowrap>
						<acwc:AcSafeHyperLink id="AcTimeStampLbl1" CssClass="hyperlink" runat="server" /></td>
					<td align="right" nowrap>
						<acwc:acsafelabel ID="AcLblFileSize" CssClass="label" RunAt="server" /></td>
				</tr>
			</ItemTemplate>
			<SeparatorTemplate>
				<actuate:separator id="AcSeparator" runat="server" />
			</SeparatorTemplate>
			<FooterTemplate>
				<tr>
					<td colspan="7">
						<img src="<%=Context.Request.ApplicationPath%>/images/vspace.gif" width="100%" height="4" border="0">
					</td>
				</tr>
			</FooterTemplate>
		</asp:repeater></TABLE>
</asp:panel>

