/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols.filesfolders
{
	using System;
	using System.Collections;
	using System.Collections.Specialized;
	using System.ComponentModel;
	using System.Configuration;
	using System.Data;
	using System.Drawing;
	using System.Reflection;
	using System.Text;
	using System.Web;
	using System.Web.SessionState;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	//  Using sub name spaces
	using activeportal.classes;
	using activeportal.classes.api.proxy;
	using activeportal.classes.api;
	using actuate_i18n;
	using actuate_i18n.classes;
	using activeportal.classes.configuration;
	using activeportal.classes.functionality;

	/// <summary>
	///		User control from which all other user controls for displaying the files
	///		folders list will inherit.
	///		This contain properties to hold data which will be used by other inherited
	///		user controls to, display the actual output
	/// </summary>
	public partial class ActuateBaseView : System.Web.UI.UserControl
	{


		protected UserControl AcFilter;
		protected AcLabelControl AcLblNoFolders;
		protected AcLabelControl AcLblEmptyFolder;
		protected AcKeyArgument AcArgNoFolders;
		protected Label AcNumSearchElements;
		protected Literal AcSearchSeparator;
		
		protected string m_viewingTarget = "../viewer/viewFrameset.aspx";
		protected string m_executeTarget = null;
		protected string m_detailTarget = null;
		protected string m_downloadTarget = null;
		protected string m_submitJobTarget =  null;
		protected string m_folderTarget = null;
		protected string m_formatViewTarget = null;
		protected string m_createQueryTarget = null;
		protected string m_executeQueryTarget = null;
		protected string m_submitQueryTarget = null;
		protected string m_searchReportTarget = null;
		protected bool m_viewInNewBrowser = false;
		protected string m_cubeViewingTarget = null;
		
		protected bool m_showFilter = true;
		protected bool m_showFilterSpecified = false;
		protected bool m_showFolders=true;
		protected bool m_showExecutables=true;
		protected bool m_showDocuments=true;
		
		protected bool m_latestOnly= false;
	
		protected string m_filter="*";
		protected bool m_isStateLess = false;
		protected ArrayList m_documentList, m_reportList, m_folderList, m_queryList, m_infoObjectList, m_cubeProfileList, m_dataCubeList, m_fileList;
		public bool m_moreVersions = false;
		protected String m_homeFolder = "/";
		protected string m_targetFolder = null;
		
		protected int m_maxSize;
	


		// This is to be set to true if you want Large Icons
		// for the File Types.
		protected bool m_largeIconView = false; 

		// Path to the page containing the view user control.
		public string ViewingTarget
		{
			get	{	return m_viewingTarget;	}
			set	{	m_viewingTarget = value;}
		}
		public string ContentTarget
		{
			get	{	return m_formatViewTarget;	}
			set	{	m_formatViewTarget = value;}
        
		}
		public string ExecuteTarget
		{
			get {	return m_executeTarget;}
			set {   m_executeTarget= value;}
		}
		public string DetailTarget
		{
			get {	return m_detailTarget;}
			set {   m_detailTarget= value;}
		}
		public string DownloadTarget
		{
			get {	return m_downloadTarget;}
			set {   m_downloadTarget= value;}
		}

		public string SubmitJobTarget
		{
			get {	return m_submitJobTarget;}
			set {   m_submitJobTarget= value;}
		}

		public string FolderTarget
		{
			get {	return m_folderTarget;}
			set {	m_folderTarget = value;}
		}

		public string CreateQueryTarget
		{
			get {	return m_createQueryTarget;}
			set {	m_createQueryTarget = value;}
		}

		public string SubmitQueryTarget
		{
			get {	return m_submitQueryTarget;}
			set {	m_submitQueryTarget = value;}
		}

		public string ExecuteQueryTarget
		{
			get {	return m_executeQueryTarget;}
			set {	m_executeQueryTarget = value;}
		}

		public string SearchReportTarget
		{
			get {	return m_searchReportTarget;}
			set {	m_searchReportTarget = value;}
		}

		public string CubeViewingTarget
		{
			get{ return m_cubeViewingTarget; }
			set{ m_cubeViewingTarget  = value; }
		}

		public bool ShowFilter
		{
			get { return m_showFilter; }
			set 
			{
				m_showFilter = value;
				m_showFilterSpecified = true;}
		}

		public bool LargeIconView
		{
			get{ return m_largeIconView; }
			set{ m_largeIconView = value; }
		}

		public bool IsStateLess
		{
			set { m_isStateLess = value; }
			get { return m_isStateLess; }
		}
		public ArrayList DocumentDataList
		{
			get{ return m_documentList; }
			set{ m_documentList = value; }
		}

		public ArrayList ReportDataList
		{
			get{ return m_reportList; }
			set{ m_reportList = value; }
		}

		public ArrayList FolderDataList
		{
			get{ return m_folderList;}
			set{ m_folderList = value; }
		}

		public ArrayList QueryDataList
		{
			get{ return m_queryList; }
			set{ m_queryList = value; }
		}

		public ArrayList InfoObjecDatatList
		{
			get{ return m_infoObjectList; }
			set{ m_infoObjectList = value; }
		}

		public ArrayList CubeProfileDataList
		{
			get{ return m_cubeProfileList; }
			set{ m_cubeProfileList = value; }
		}

		public ArrayList DataCubedList
		{
			get{ return m_dataCubeList; }
			set{ m_dataCubeList = value; }
		}

		public ArrayList FileDataList
		{
			get{ return m_fileList; }
			set{ m_fileList = value; }
		}

		// Indicates if the response from 
		protected bool m_categorizeLists = false;
		
		public bool CategorizeLists
		{
			get{ return m_categorizeLists; }
			set{ m_categorizeLists = value; }
		}

		protected int m_IconsPerRow = 4;
		public int IconsPerRow
		{
			get{ return m_IconsPerRow; }
			set{ m_IconsPerRow = value; }
		}


		
		// These variables are used when the viewmanager is used in the Search page
		// for recursive searching
		protected bool m_fileSearchMode = false;
		protected string m_baseSearchFolder =  "/";
		protected string m_fileSearchCriterion = "*";
		protected long m_totalCount = 0; // Number of elements selected


		public bool FileSearchMode
		{
			get{ return m_fileSearchMode; }
			set{ m_fileSearchMode = value; }
		}

		public string BaseSearchFolder
		{
			get{ return m_baseSearchFolder; }
			set{ m_baseSearchFolder = value; }
		}

		public string FileSearchCriterion
		{
			get{ return m_fileSearchCriterion; }
			set{ m_fileSearchCriterion = value; }
		}




		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Set the help topic for the banner

			//if ( Visible )
			//{
				//if ( m_fileSearchMode ) 
				//{
				//	ActuUtil.RegisterHelpTopic( Page, "Search_list" );
				//}
				//else
				//{
				//	ActuUtil.RegisterHelpTopic( Page, "Document_list" );
				//}
				//if ( !IsPostBack)
				//{
				//	string startupMessage = AcParams.Get( Page, AcParams.Name.startupMessage, AcParams.From.Url );
				//	if ( startupMessage != null )
				//	{
				//		((HtmlGenericControl)ActuUtil.FindControlRecursive( Page, "AcBody" )).Attributes.Add( "OnLoad", "alert('" + ActuUtil.jsEncode( startupMessage) + "')" );
				//	}

				//}
				//else
				//{
				//	((HtmlGenericControl)ActuUtil.FindControlRecursive( Page, "AcBody" )).Attributes.Remove( "OnLoad" );
				//}
			//}

			//if ( Visible && !m_fileSearchMode )
			//{
			//	Context.Items["DisplayFolderDetailIcon"] = true;
			//}
			//else
			//{
			//	Context.Items["DisplayFolderDetailIcon"] = false;
			//}

			//Context.Items["subpage"] = "list";
		}

		protected void Page_UnLoad(object sender, System.EventArgs e)
		{
		
			Context.Items["DisplayFolderDetailIcon"] = false;
			Context.Items["subpage"] = "null";
		}

		// Use onprerender to let the filter usercontrol's pageload function get the filter parameters

		protected override void OnPreRender( EventArgs e )
		{
			// FIXME not enough, it will still load the subcontrols
			if ( ! Visible ) return ;


			Hashtable userProfile = null;
			//Hashtable userProfile = AcProfileManager.GetUserProfile( Page );
			if ( userProfile != null )
			{
				string ViewInNewBrowser = (string) userProfile["viewInNewBrowserWindow"];

				if ( ViewInNewBrowser == null || ViewInNewBrowser.Trim() == String.Empty )
				{
					m_viewInNewBrowser = false;
				}
				else
				{
					m_viewInNewBrowser = ViewInNewBrowser.ToLower() == "true" ;
				}
			}
			else
			{
				m_viewInNewBrowser = AcParams.urlCheckboxState( Page, AcParams.Name.viewNewBrowserWindow, m_viewInNewBrowser, AcParams.From.Url  );
			}
	
			if ( new DistributionPortal.GroupMembership().MyReportsUser  )
				m_homeFolder = AcParams.Get( Page, AcParams.Name.homeFolder, m_homeFolder );   
			else if (new DistributionPortal.GroupMembership().BlueBookDistributionUser)
				m_homeFolder = "/Blue Book Distribution";


	
			
			if ( ! m_isStateLess )
			{
				// Save currentFolder to session for next request.

				m_targetFolder = AcParams.Get( Page, AcParams.Name.folder, m_homeFolder, AcParams.From.Url | AcParams.From.Session, AcParams.SetTo.Session );
			}
			else
			{
				m_targetFolder = AcParams.Get( Page, AcParams.Name.folder, m_homeFolder, AcParams.From.Url | AcParams.From.Session);
			}


			m_targetFolder = ActuUtil.ResolveUpFolderPath( m_targetFolder );

			m_maxSize = ActuUtil.ReadIntFromConfig( "MAX_LIST_SIZE", Constants.MAX_LIST_SIZE_DEFAULT );

			
			// When used in the File search mode
			// we do not have to consider the Filter
			
			if ( ! m_fileSearchMode )
			{
				bool ShowFilters = false;
				//Hashtable UserProfile = null;   //AcProfileManager.GetUserProfile( Page );
				//if ( UserProfile != null )
				//{
				//	ShowFilters = ActuUtil.ConvertStringToBool( (string) UserProfile["showFilters"]);
				//}
				//AcFilter.Visible = m_showFilterSpecified ? m_showFilter : AcParams.urlCheckboxState( Page, AcParams.Name.enableFilters, ShowFilters );
				//RetrieveFilterValues();
			}

	

			if ( m_fileSearchMode )
			{
				SelectFiles();
			}
			else
			{
				GetFolderItems();
			}
						
			// do not show the Filter if used as a File Search
			if ( m_fileSearchMode )
			{
				AcFilter.Visible = false;

				// If used in a serach mode display the 
				// Number of elements retrieved
				AcNumSearchElements.Visible = true;
								
				if ( m_categorizeLists && 
					 (m_documentList.Count > 0 ||	 m_reportList.Count > 0 || m_queryList.Count > 0 
					  || m_infoObjectList.Count > 0 || m_cubeProfileList.Count > 0 ||	 m_dataCubeList.Count > 0 ||  m_folderList.Count > 0 ))
				{
					string NumElementsText = AcResourceManager.GetString( "search.label.resultCount",  new AcKeyArgument[] { new AcKeyArgument( null, m_totalCount.ToString()  ) }, Session ) + "<br>" ;
					AcNumSearchElements.Text =  NumElementsText;
				}
				else if ( m_fileList != null && m_fileList.Count > 0 )
				{
					string NumElementsText = AcResourceManager.GetString( "search.label.resultCount",  new AcKeyArgument[] { new AcKeyArgument( null, m_totalCount.ToString()  ) }, Session ) + "<br>" ;
					AcNumSearchElements.Text =  NumElementsText;
				}

				AcSearchSeparator.Visible = true;

			}
			else
			{
				//AcNumSearchElements.Visible = false;
				//AcSearchSeparator.Visible = false;
				
			}
	
		}
		protected string GetFileTypeIconURL( File file )
		{
			if ( m_largeIconView )
			{
				// Large Icon
				return  ( AcFileTypeManager.GetDefaultLargeImageURL( file.FileType, Page ) );
			}
			else
			{
				// Small Icon 

				return  ( AcFileTypeManager.GetDefaultSmallImageURL( file.FileType, Page ) );
			}
		}


		protected string GetFolderIcon()
		{
			// TODO 
			// Depending on the m_largeIconView , return the appropriate icon
			if ( m_largeIconView )
			{
				return( "~/images/foldericon32x32.gif" );
			}
			else
			{
				return( "~/images/foldericon.gif" );
			}
		}

		// add the targetName to the current folder or /
		protected string ConstructTargetPath(  string targetName )
		{


			// When this control is used in the file serach mode , then 
			// the resulting file names to be displayed, are returned as Fully qualified file names
			// by the server. Hence we do not have to add any Folder Name to the existing file names

			

			if ( ! m_fileSearchMode )
			{
				string targetPath = m_targetFolder;	
				if (!targetPath.EndsWith("/"))
				{
					targetPath += "/";
				}
				
				
				targetPath += ( targetName );
				return targetPath ;
			}
			else
			{
				return targetName;
			}
			

		}

	

		protected virtual string ShowFileName( File f )
		{
			if ( f.FileType == "Directory" )
			{
				return f.Name;
			}
			else
			{
				return ActuUtil.stripFileExtension( f.Name ) ;
			}
		}

		
		protected string GetFolderNavigateURL(Object file)
		{				
			AcUrlBuilder 	link = new AcUrlBuilder( this.m_folderTarget );
			link.AddKeyValue( "folder", ConstructTargetPath(((File)file).Name) ) ;
			
			return link.ToString( );
		}
		protected string GetExecuteReportURL( Object file, bool addVersion)
		{
			return GetReportURL( file, m_executeTarget, "__executableName", addVersion );
		}

		protected string GetExecuteReportURL( Object file)
		{
			return GetExecuteReportURL( file, true );
		}

		protected string GetSubmitJobURL( Object file)
		{
			return GetReportURL( file, m_submitJobTarget, "__executableName" );
		}

		protected string GetViewDetailURL( Object file)
		{
			return GetReportURL( file, m_detailTarget, "name" );
		}
		protected string GetDownloadDocumentURL( Object file, bool returnContent )
		{
			return GetDownloadDocumentURL( file, returnContent, true );
		}

		protected string GetDownloadDocumentURL( Object file, bool returnContent, bool addVersion )
		{
			return GetReportURL( file, m_downloadTarget + "?returnContents=" + returnContent.ToString( ), "name", addVersion );
		}

		protected string GetViewDocumentURL( Object file )
		{
			return GetViewDocumentURL( file, true );
		}
		protected string GetViewDocumentURL( Object file, bool addVersion )
		{
			return GetReportURL( file,m_viewingTarget, "name", addVersion );
		}
		protected string GetFormattedViewDocumentURL( Object file )
		{
			return GetReportURL( file, m_formatViewTarget, "name" );
		}
		protected string GetCubeViewingURL( Object file )
		{
			return GetCubeViewingURL( file, true);
		}
		protected string GetCubeViewingURL( Object file, bool addVersion )
		{
			return GetReportURL( file, m_cubeViewingTarget, "name", addVersion );
		}
		protected string GetReportURL( Object file, String pageName, String paramName )
		{
			return GetReportURL( file, pageName, paramName, true );
		}
		protected string GetReportURL( Object file, String pageName, String paramName, bool addVersion )
		{
			return GetReportURL( file, pageName, paramName, addVersion, false );
		}
		protected string GetReportURL( Object file, String pageName, String paramName, bool addVersion, bool addQueryDescription )
		{
			AcUrlBuilder link = new AcUrlBuilder( pageName );
			
			link.AddKeyValue( paramName, ConstructTargetPath(((File)file).Name) );
			
			if ( addVersion )
			{
				link.Append( ";" );
				link.Append( ((File)file).Version.ToString( ) );
			}
			if ( addQueryDescription )
			{
				link.AddKeyValue( AcParams.Name.queryDescription, ((File)file).Description );
			}
			return link.ToString( );
		}

		protected string GetSubmitQueryURL( Object file )
		{
			return GetReportURL( file, m_submitQueryTarget, AcParams.Name.__executableName.ToString() ) ;
		}
		protected string GetRunQueryURL( Object file )
		{
			
			return GetReportURL( file, m_executeQueryTarget, AcParams.Name.__executableName.ToString() ) ;
		}

		protected string GetSearchReportURL( Object file )
		{
			return GetReportURL( file, m_searchReportTarget, "name" );
		}

		protected string GetWizardURL( Object file )
		{
			return GetWizardURL( file, false );
		}

		protected string GetWizardURL( Object file, bool addQueryDescription )
		{
			
			return GetReportURL( file, m_createQueryTarget, AcParams.Name.__executableName.ToString(), false, addQueryDescription );
		}

	
		protected string GetPopupScript( string link, bool isViewWindow, bool showMenu )
		{
			string width = isViewWindow ? "830" : "710";
			string window = isViewWindow ? "" : "AcWizardWindow" ;
			string properties = showMenu ? "'status, scrollbars,resizable,menubar'" : "'status, scrollbars, resizable'";
			if ( isViewWindow )
			{
				return "openPopupWindow( '" + ActuUtil.jsEncode( link ) + "','" + window + "','',''," + properties + ");";
			}
			else
			{
				return "openPopupWindow( '" + ActuUtil.jsEncode( link ) + "','" + window + "'," + width + ",540," + properties + ");";
			}
		}

		
		protected void SetUrlLink ( HyperLink link, string url, bool openInNewWindow, bool isViewWindow, bool showMenu )
		{
			if ( openInNewWindow )
			{
				link.NavigateUrl = "javascript:;";
				link.Attributes.Add( "Onclick", GetPopupScript( url, isViewWindow, showMenu ) );
			}
			else
			{
				link.NavigateUrl = url;
			}

		}
		protected void SetUrlLink( HyperLink link, string url, bool openInNewWindow, bool isViewWindow )
		{
			SetUrlLink( link, url, openInNewWindow, isViewWindow, false );
					
		}

		protected void RetrieveFilterValues( )
		{
			
			Type filterType = AcFilter.GetType( );

			PropertyInfo showExecutables = filterType.GetProperty( "ShowExecutables" );
			m_showExecutables = ( bool )showExecutables.GetValue( AcFilter, null );

			PropertyInfo showDocuments = filterType.GetProperty( "ShowDocuments" );
			m_showDocuments = ( bool )showDocuments.GetValue( AcFilter, null );
			
			PropertyInfo showFolders = filterType.GetProperty( "ShowFolders" );
			m_showFolders = ( bool )showFolders.GetValue( AcFilter, null );

			PropertyInfo latestOnly = filterType.GetProperty( "OnlyLatest" );
			m_latestOnly = ( bool )latestOnly.GetValue( AcFilter, null );

			PropertyInfo filter = filterType.GetProperty( "Filter" );
			m_filter = ( String ) filter.GetValue( AcFilter, null );
			

		}

		protected void ShowPopupMenu( File file, FileType fileType, WebControl icon  )
		{
			String FileName = file.Name;
			if ( ! FileName.StartsWith("/") )
			{
				FileName = "/" + FileName;
			}

			if ( file.FileType.ToLower() != "directory" )
			{
				FileName = FileName + ";" + file.Version;
			}

			if ( ! m_fileSearchMode )
			{
				string BaseFolder = m_targetFolder;
				if ( BaseFolder == null || BaseFolder.Trim() == String.Empty )
				{
					BaseFolder = "/";
				}
				else
				{
					if ( ! BaseFolder.StartsWith("/"))
					{
						BaseFolder = "/" + BaseFolder;
					}
				}

				if ( BaseFolder != "/" )
				{
					FileName = BaseFolder +  FileName;
				}
			}

			
			
			icon.Attributes["onmouseover"]= ActuUtil.getShowMenuJavascript( Page, file, FileName );
			
			icon.Attributes["onmouseout"] = "javascript:scheduleHideMenu();";
		}

		protected void DisplayNoFolderMsg( Control ctl )
		{

			if ( m_fileList == null || m_fileList.Count == 0 )
			{
				if ( m_fileSearchMode )
				{
					if ( m_fileSearchCriterion == null || m_fileSearchCriterion.Trim() == String.Empty )
					{
						AcNumSearchElements.Text  = AcResourceManager.GetString("search.label.entersearch", Session );
					}
					else
					{
						AcNumSearchElements.Text  = AcResourceManager.GetString("search.label.noresults", Session );
					}
				}
				else if ( AcFilter.Visible && ( m_filter != "*" || ! m_showFolders || ! m_showExecutables || ! m_showDocuments ) )
				{
					AcLblEmptyFolder.Key = "MSG_EMPTY_FOLDER_MATCHING_CRITERIA";
					AcLblEmptyFolder.Visible = true;
				}
				else
				{
					if (  m_showFolders && 
						! m_showDocuments &&
						! m_showExecutables && m_fileList.Count == 0 )
					{

						AcLblNoFolders.Visible = true;
						AcArgNoFolders.Text = m_targetFolder;
					}
					else
					{
						AcLblEmptyFolder.Visible = true;
					}
				}

				ctl.Visible = false;
			}
		}

		private void GetFolderItems()
		{
			AcGetFolderItems getFolders = new AcGetFolderItems( m_latestOnly, m_filter, m_targetFolder, m_maxSize, m_categorizeLists ,
																true, true, true );
			try
			{
				getFolders.Execute( Page );
			}
			catch ( System.Web.Services.Protocols.SoapException ex )
			{
				if ( ex.Detail["ErrorCode"].InnerText != "3033" )
				{
					throw;
				}

			}
			
			m_documentList = getFolders.DocumentList;
			m_reportList = getFolders.ReportList;
			m_folderList = getFolders.FolderList;
			
			m_queryList = getFolders.QueryList;
			m_infoObjectList = getFolders.InfoObjectList;
			m_cubeProfileList = getFolders.CubeProfileList;
			m_dataCubeList = getFolders.DataCubeList;
			m_fileList = getFolders.ObjectList;

			
		}

		private void SelectFiles()
		{
			// Retrieve the necessary files
			string[] ResultDef = new String[] { "Id", "Name", "FileType", "Description", "PageCount", 
												  "Size", "TimeStamp", "Version", "VersionName", 
												  "Owner", "UserPermissions", "AccessType" };


			FileSearch fileSearch = new FileSearch();

			
			FileCondition fileCondition = new FileCondition();
			fileCondition.Field = FileField.Name;
			fileCondition.Match = m_fileSearchCriterion;

			fileSearch.Item = fileCondition;
			fileSearch.FetchSize = ActuUtil.ReadIntFromConfig( "MAX_LIST_SIZE", Constants.MAX_LIST_SIZE_DEFAULT );
			fileSearch.FetchSizeSpecified = true;
			AcSearchFiles SearchFiles = new AcSearchFiles( ResultDef, fileSearch,true, m_baseSearchFolder, false, m_categorizeLists );
			
			SearchFiles.Execute(Page);
			
			m_documentList = SearchFiles.DocumentList;
			m_reportList = SearchFiles.ReportList;
			m_folderList = SearchFiles.FolderList;
			m_queryList = SearchFiles.QueryList;
			m_infoObjectList = SearchFiles.InfoObjectList;
			m_cubeProfileList = SearchFiles.CubeProfileList;
			m_dataCubeList = SearchFiles.DataCubeList;
			m_fileList = SearchFiles.ObjectList;

			if ( m_categorizeLists )
			{
				m_totalCount = m_documentList.Count + m_reportList.Count + m_folderList.Count + m_queryList.Count
					+ m_infoObjectList.Count +  m_cubeProfileList.Count +  m_dataCubeList.Count;
			}
			else
			{
				m_totalCount = m_fileList.Count;
			}

		}

		protected string GetTableRowId( string id)
		{
			return ( "item" + id );
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Unload += new System.EventHandler(this.Page_UnLoad);
		}
		#endregion
	}
}
