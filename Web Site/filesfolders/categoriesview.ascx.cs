
namespace activeportal.usercontrols.filesfolders
{


	using System;
	using System.Collections;
	using System.ComponentModel;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.SessionState;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Collections.Specialized;
	using System.Reflection;
	using System.Text;
	using System.Configuration;

	//  Using sub name spaces
	using activeportal.classes;
	using activeportal.classes.api.proxy;
	using activeportal.classes.api;
	using actuate_i18n;
	using actuate_i18n.classes;
	using ReportDistribution.SQLServerDAL;
	using MyReports.Common;
	using DistributionPortal.Components;

	using DistributionPortal;
	
	public partial  class categoriesview : ActuateBaseView
	{
		
		//protected System.Web.UI.WebControls.Repeater ReportList;
		protected System.Web.UI.WebControls.Label LblFolders;
		protected Label AcTimeStampLbl1;
		protected Label AcTimeStampLbl2;
		private string m_prevDocName = "";
		private string m_prevReportName = "";
		
		public bool m_firstInList = true;
		private int m_documentCount = 0;
		private int m_reportCount = 0;
		protected System.Web.UI.WebControls.Image Image1;
		protected System.Web.UI.HtmlControls.HtmlTableCell testTD;
		protected System.Web.UI.HtmlControls.HtmlTableCell testTD2;
		protected System.Web.UI.HtmlControls.HtmlTableCell testTD3;
		
		private Reports _oRD;

		public bool ShowHeaders
		{
			
			set
			{
				if ( ! value )
				{
					FolderList.HeaderTemplate = null;
					DocumentList.HeaderTemplate = null;
					ReportList.HeaderTemplate = null;
				}
			}
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (Session["SelectedReports"] == null)
                Session["SelectedReports"] = new ArrayList();
        }

        #region PreRender

        protected override void OnPreRender( EventArgs e )
		{

			if (Visible)
			{
                if (!Page.IsPostBack)
                {
                    try
                    {
                        // Set the sort ASC or DESC 
                        SetSort();

                        m_categorizeLists = true;

                        try
                        {
                            base.OnPreRender(e);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Error in base", ex);
                        }

                        FolderList.Visible = true && m_folderList.Count > 0;
                        ReportList.Visible = true && m_reportList.Count > 0;
                        DocumentList.Visible = true && m_documentList.Count > 0;
                        panDocuments.Visible = DocumentList.Visible;

                        if (FolderList.Visible)
                        {
                            try
                            {
                                FolderList.DataSource = m_folderList;
                                FolderList.DataBind();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Error binding folder listing", ex);
                            }

                        }
                        if (ReportList.Visible)
                        {
                            try
                            {
                                ReportList.DataSource = m_reportList;
                                ReportList.DataBind();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Error binding folder listing", ex);
                            }


                        }
                        if (DocumentList.Visible)
                        {
                            // Build the header row first

                            try
                            {
                                // Set the header columns and text depending on if this is a blue book, report distribution
                                switch (QueryStringValues.App)
                                {
                                    case enumApp.rd:
                                        break;

                                    case enumApp.bbd:

                                        // Verions TD tag
                                        lblVersions.Visible = false;
                                        tdVersions.Width = "0";
                                        tdVersions.Attributes.Add("class", "TableHeaderRowEndRow");

                                        // Page count
                                        lnkPageCount.Visible = false;
                                        tdPageCount.Width = "0";
                                        tdPageCount.Attributes.Add("class", "TableHeaderRowNonVisible");

                                        // Time stamp
                                        tdTimeStamp.Attributes.Add("class", "TableHeaderRowEndRow");

                                        // Reportd
                                        tdReports.Width = "100%";

                                        break;

                                }
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Error building document listing header row.", ex);
                            }

                            try
                            {
                                // get a reference to the object
                                _oRD = new Reports(AppValues.MyReportsDB);
                                DocumentList.DataSource = SortDocumentListing();
                                DocumentList.DataBind();
                                ReportCount.Text = m_documentList.Count.ToString();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Error binding document listing.", ex);
                            }
                        }


                        // If no subfolders exist, used in folderbrowser
                        if (m_fileSearchMode && m_documentList.Count == 0 && m_reportList.Count == 0 &&
                            m_queryList.Count == 0 && m_infoObjectList.Count == 0 && m_cubeProfileList.Count == 0 && m_dataCubeList.Count == 0 && m_folderList.Count == 0)
                        {
                            DisplayNoFolderMsg(FolderList);
                        }
                        else
                        {
                            if (m_showFolders &&
                                !m_showDocuments &&
                                !m_showExecutables && m_folderList.Count == 0 && !AcFilter.Visible && m_filter == "*")
                            {
                                AcLblNoFolders.Visible = true;
                                AcArgNoFolders.Text = m_targetFolder;
                            }

                                // If nothing is visible

                            else if (!FolderList.Visible &&
                                !DocumentList.Visible &&
                                !ReportList.Visible)
                            {
                                if (AcFilter.Visible && (m_filter != "*" || !m_showFolders || !m_showExecutables || !m_showDocuments))
                                {
                                    AcLblEmptyFolder.Key = "MSG_EMPTY_FOLDER_MATCHING_CRITERIA";
                                }

                                AcLblEmptyFolder.Visible = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex);
                    }
                }
			}
        }

        #endregion

        #region ReportList OnItemDataBound.

        protected void ReportList_OnItemDataBound( Object sender, RepeaterItemEventArgs e )
		{
			
			
			if ( e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item )
			{
				

				Label timeStampLbl = (Label) e.Item.FindControl( "AcTimeStampLbl2" );
				timeStampLbl.Text = AcTimeZoneManager.ConvertToUserTime(Page, ((File) e.Item.DataItem).TimeStamp );
				File currentFile = (File) e.Item.DataItem;
				ComputeFirstReportInList( currentFile );

				System.Web.UI.WebControls.Image AcImgReportIcon = (System.Web.UI.WebControls.Image)e.Item.FindControl( "AcImgReportIcon" );
				HyperLink AcLnkReport = ( HyperLink ) e.Item.FindControl( "AcLnkReport" );
				
				if ( m_firstInList )
				{
					AcImgReportIcon.ImageUrl = GetFileTypeIconURL( currentFile ) ;
					AcImgReportIcon.ToolTip = AcFileTypeManager.GetFileType( currentFile.FileType, Page).ShortDescription;

					
					AcLnkReport.Text = ShowName( currentFile );
					AcLnkReport.NavigateUrl = GetExecuteReportURL( currentFile, false );
				}
				else
				{
					AcImgReportIcon.Visible = false;
					AcLnkReport.Visible = false;
				}

				


				AcHyperLinkControl AcImgViewDetail = ( AcHyperLinkControl ) e.Item.FindControl( "AcImgViewDetail" );

				if ( AcImgViewDetail != null )
					AcImgViewDetail.NavigateUrl = GetViewDetailURL( currentFile ) ;

				m_moreVersions = false;

				if ( m_reportList.Count > m_reportCount + 1  )
					if (((File)m_reportList[m_reportCount+1]).Name.Equals(((File)m_reportList[m_reportCount]).Name))
						m_moreVersions = true;
				
				m_reportCount++;
			}
			
			DisplaySeparator( e );

        }

        #endregion

        #region DocumentList_OnItemDataBound

        protected void DocumentList_OnItemDataBound( Object sender, RepeaterItemEventArgs e )
		{

			if (	e.Item.ItemType == ListItemType.AlternatingItem || 
				e.Item.ItemType == ListItemType.Item ||
				e.Item.ItemType == ListItemType.SelectedItem )
			{
			
				File currentFile = (File) e.Item.DataItem;

                HyperLink AcLnkDocumentName = (HyperLink)e.Item.FindControl("AcLnkDocumentName");
				HyperLink timeStampLbl = ( HyperLink ) e.Item.FindControl( "AcTimeStampLbl1" );
				Label pageCount = (Label) e.Item.FindControl( "AcLblFileSize" );
				Label lblViewed = (Label) e.Item.FindControl( "lblViewed" );
                CheckBox chkSelected = (CheckBox)e.Item.FindControl("chkSelected");                               

				DateTime downloadedTime = System.DateTime.Now;

				timeStampLbl.Text = AcTimeZoneManager.ConvertToUserTime(Page, ((File) e.Item.DataItem).TimeStamp );

                // Version
                //HyperLink AcLnkDocumentVersion = ( HyperLink ) e.Item.FindControl( "AcLnkDocumentVersion" );
                //AcLnkDocumentVersion.Text = currentFile.VersionName;

                m_moreVersions = false;

                if (m_documentList.Count > m_documentCount + 1)
                    if (String.Compare(((File)m_documentList[m_documentCount + 1]).Name,
                        ((File)m_documentList[m_documentCount]).Name,
                        true) == 0)
                    {
                        m_moreVersions = true;
                    }

                string fileTypeExtension = currentFile.FileType.ToLower();
                string viewUrl = GetViewDocumentURL(currentFile);
                AcLnkDocumentName.Text = ShowName(currentFile);

                if (fileTypeExtension == "roi")
                {
                    SetUrlLink(timeStampLbl, viewUrl, m_viewInNewBrowser, true);
                    SetUrlLink(AcLnkDocumentName, GetViewDocumentURL(currentFile, false), m_viewInNewBrowser, true);

                }
                if (fileTypeExtension == "rox")
                {
                    SetUrlLink(timeStampLbl, viewUrl, m_viewInNewBrowser, true);
                    SetUrlLink(AcLnkDocumentName, GetViewDocumentURL(currentFile, false), m_viewInNewBrowser, true);

                }
                else if (fileTypeExtension == "pdf")
                {
                    string downloadUrl = GetDownloadDocumentURL(currentFile, false);

                    SetUrlLink(timeStampLbl, downloadUrl, m_viewInNewBrowser, true);
                    SetUrlLink(AcLnkDocumentName, GetDownloadDocumentURL(currentFile, false), m_viewInNewBrowser, true);
                }

                m_documentCount++;

				switch(QueryStringValues.App)
				{
					case enumApp.rd:
                        // Make the checkbox visible

                        string entireReport = ConstructTargetPath(currentFile.Name).ToLower();

                        chkSelected.Visible = MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin");
                        chkSelected.InputAttributes.Add("doc", entireReport);

                        ArrayList ary = (ArrayList)Session["SelectedReports"];

                        if (ary.Contains(entireReport))
                            chkSelected.Checked = true;
                        else
                            chkSelected.Checked = false;

                        // Page count
                        pageCount.Text = ((File)e.Item.DataItem).PageCount.ToString().PadLeft(5, '0');
						pageCount.Visible = true;

						// Has report ever been viewed
						lblViewed.Visible = true;

						bool hasReportBeenViewed = false;

						try
						{
							hasReportBeenViewed = _oRD.HasReportEverBeenViewed(AppValues.UserName, getReportPathName( ActuUtil.stripVersionNumber(GetViewDocumentURL( currentFile ) ) ), ((File) e.Item.DataItem).TimeStamp.ToString(), out downloadedTime);
						}
						catch (Exception ex)
						{

							string dsfg = "";
						}

						if ( hasReportBeenViewed )
						{					
							lblViewed.Text = downloadedTime.ToString("g");	

							// Change the "row" style of the downloaded file									
							HtmlTableRow row = (HtmlTableRow)e.Item.FindControl("lll");	
							row.BgColor = "gainsboro";
						}

						break;

					case enumApp.bbd:
                        // Hide the check box
                        chkSelected.Visible = false;

						pageCount.Visible = false;
						lblViewed.Visible = false;
						break;

				}														


			}

			DisplaySeparator( e );
        }

        #endregion

        #region chkSelected_CheckedChanged

        protected void chkSelected_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;

            string entireReport = chk.InputAttributes["doc"].ToLower();

            ArrayList ary = (ArrayList)Session["SelectedReports"];

            if (chk.Checked)
            {
                if (!ary.Contains(entireReport))
                    ary.Add(entireReport);
            }
            else
            {
                if (ary.Contains(entireReport))
                    ary.Remove(entireReport);
            }
        }

        #endregion

        protected string ComputeFirstDocInList( File f )
		{
			m_firstInList = ( String.Compare( f.Name, m_prevDocName, true ) != 0 ) ;
			m_prevDocName = f.Name;
			return "";
		}
		protected string ComputeFirstReportInList( File f )
		{
			m_firstInList = ( String.Compare( f.Name, m_prevReportName, true ) != 0 ) ;
			m_prevReportName = f.Name;
			return "";
		}

		protected string ShowIcon( Object item )
		{
			File f = (File)item;
			if ( m_firstInList )
			{
				
				
				return "<img src='" + GetFileTypeIconURL( f ) + "' title='" + AcFileTypeManager.GetFileType( f.FileType, Page).ShortDescription + "'  width='16' height='16' align='top'>" ;
			}
			else
				return "";
		}

		protected string ShowName( File f )
		{		
			if ( m_firstInList )
			{
				return ActuUtil.stripFileExtension( f.Name );
			}
			else
			{
				return "";
			}
		}

		protected bool ShowCheckBox( File f )
		{		
			if ( m_firstInList )
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		protected string DisplayPageCount( Object item )
		{
			File f = (File)item;
			if (f.PageCount != 0 )
				return f.PageCount.ToString();
			else
				return "";
		}
		private void DisplaySeparator( RepeaterItemEventArgs e )
		{
			if ( e.Item.ItemType == ListItemType.Separator )
			{
				UserControl AcSeparator = (UserControl) e.Item.FindControl( "AcSeparator" );
				if ( m_moreVersions )
				{
					AcSeparator.Visible = false;
				}
			}
        }

        #region Header Columns

        protected void lnkTimeStamp_Click(object sender, System.EventArgs e)
		{			
			SortColumns("TimeStamp");
		}

		protected void lblReports_Click(object sender, System.EventArgs e)
		{
			SortColumns("ReportName");
		}

		protected void lnkPageCount_Click(object sender, System.EventArgs e)
		{			
			SortColumns("PageCount");
        }

        #endregion

        #region SetSort

        private void SetSort()
		{
			// depending on the applications, blue book, report distribution, budget distribution

			try
			{
				switch(QueryStringValues.App)
				{
					case enumApp.rd:
						if (ViewState["sortASC"] == null)
							ViewState["sortASC"] = true;
			
						if (ViewState["SortColumn"] == null)
							ViewState["SortColumn"] = "TimeStamp";
						break;

					case enumApp.bbd:
						if (ViewState["sortASC"] == null)
							ViewState["sortASC"] = false;
			
						if (ViewState["SortColumn"] == null)
							ViewState["SortColumn"] = "ReportName";
						break;

						
				}

			}
			catch (Exception ex)
			{
				throw new Exception("Error setting the sort order", ex);
			}
        }

        #endregion

        #region Sort Columns

        private void SortColumns(string column)
		{
			SetSort();

			ViewState["sortASC"] = !(bool)ViewState["sortASC"];
			ViewState["SortColumn"] = column;
        }

        #endregion

        #region Sort Document Listing

        private ArrayList SortDocumentListing()
		{
			ArrayList ary = this.m_documentList;

			string column	= (string)ViewState["SortColumn"];
			bool sort		= ViewState["sortASC"] != null ? (bool)ViewState["sortASC"] : true;

			IComparer myComparer = null;

			switch (column.ToLower())
			{
				case "reportname":
					myComparer = new SORT_BY_FileName();
					break;

				case "timestamp":
					myComparer = new SORT_BY_TimeStamp();
					break;

				case "pagecount":
					myComparer = new SORT_BY_PageCount();
					break;
			}		

			ary.Sort(myComparer);

			if (!sort)
				ary.Reverse();

			
			return ary;
        }

        #endregion

        #region Sort Classes

        public class SORT_BY_FileName : IComparer  
		{						
			int IComparer.Compare( Object x, Object y )  
			{			
				return( (new CaseInsensitiveComparer()).Compare( ((activeportal.classes.api.proxy.File)y).Name, ((activeportal.classes.api.proxy.File)x).Name ) );
			}
		}

		public class SORT_BY_TimeStamp : IComparer  
		{						
			int IComparer.Compare( Object x, Object y )  
			{			
				return( (new CaseInsensitiveComparer()).Compare( ((activeportal.classes.api.proxy.File)y).TimeStamp, ((activeportal.classes.api.proxy.File)x).TimeStamp ) );
			}
		}
																				
		public class SORT_BY_PageCount : IComparer  
		{						
			int IComparer.Compare( Object x, Object y )  
			{			
				return( (new CaseInsensitiveComparer()).Compare( ((activeportal.classes.api.proxy.File)y).PageCount, ((activeportal.classes.api.proxy.File)x).PageCount ) );
			}
        }

        #endregion 

        #region Acsafehyperlink1_Command

        protected void Acsafehyperlink1_Command(object sender, CommandEventArgs e)
		{
			// Log the report visit to SQL server
			string name = getReportPathName(e.CommandName);

			new Reports(AppValues.MyReportsDB).InsertReportHasBeenViewed(AppValues.UserName, name, System.Convert.ToDateTime(e.CommandArgument) );

			// Redirect the user to the report.
			Response.Redirect(e.CommandName, false);

        }

        #endregion

        #region Get Report Path Name

        private string getReportPathName(string report)
		{
			string name = Server.UrlDecode(report);
			return name.Substring(33);
        }

        #endregion

    }


}
