<%@ Control Language="c#" Inherits="activeportal.usercontrols.filesfolders.popupmenu" CodeFile="popupmenu.ascx.cs" %>
<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<script language="javascript" src="<%=Context.Request.ApplicationPath%>/js/popupmenu.js"></script>
<script language="javascript" src="<%=Context.Request.ApplicationPath%>/js/browsertype.js"></script>
<script language="javascript" src="<%=Context.Request.ApplicationPath%>/js/converter.js"></script>
<Script language="javascript">
		function GetPopupScript( IsViewWindow, ShowMenu, link)
		{
			var window;
			var width;
			var properties;
			link = "<%=Request.ApplicationPath%>" + link;
			if ( IsViewWindow )
			{
				width = "830";
				window = "AcViewWindow";
			}
			else
			{
				width = "710";
				window = "AcWizardWindow";
			}
			
			if ( ShowMenu )
			{
				properties = "status, scrollbars,resizable,menubar";
			}
			else
			{
				properties = "status, scrollbars, resizable";
			}
			
			if ( IsViewWindow )
			{
				openPopupWindow(  link , window , '', '',  properties );
			}
			else
			{
				openPopupWindow(  link , window , width, "540",  properties );
			}
			
		}
		function navigateTo( url )
		{
			url = "<%=Request.ApplicationPath%>" + url;
			if ( top.frames.main != null && top.frames.main != self)
			{
				top.frames.main.location = url;
			}
			else
			{ 
				window.location.href = url;
			}
		}
		function doAction(itemId, itemName, selectedMenuItemId, description)
		{
			if(selectedMenuItemId == "openFolder")
			{
				navigateTo( "/filesfolders/index.aspx?subpage=_list&folder=" +  encode(itemName) );
			}
			else if(selectedMenuItemId == "openDocument")
			{
			    var Extension = getFileExtension( itemName );
			    var link;
			    
			    if ( Extension == "doi" || Extension == "roi" )
			    {
					link = "/viewer/viewframeset.aspx?name=" + encode(itemName);
			    }
			    else if ( Extension == "cb4" || Extension == "cvw" || Extension == "odp")
			    {
					link = "/cubeviewer/viewcube.aspx?name=" + encode(itemName);
				}
			    else
			    {
					var returnContents = "false";
					if ( Extension == "row" || Extension == "rpw" || Extension == "sqw" )
					{
						returnContents = "true";
					}

					link = "/viewer/view3partyreport.aspx?name=" + encode(itemName) + "&returnContents=" + returnContents;
			    }
				if ( '<%= m_viewInNewBrowser %>' == 'false' )
				{
					navigateTo( link ); 
				}
				else
				{
					GetPopupScript( true, false, link ) ;
				}
			}
			else if(selectedMenuItemId == "viewXLS")
			{
			
				var link = "/viewer/getreportdata.aspx?componentID=0&format=ExcelDataDump&name=" + encode(itemName);
				if ( <%= m_viewInNewBrowser %> == false )
				{
					navigateTo( link );
				}
				else
				{
					GetPopupScript( true, true, link ) ;
				}
			}
			else if(selectedMenuItemId == "viewPDF")
			{
				var link = "/viewer/getreportdata.aspx?componentID=0&format=PDF&name=" + encode(itemName);
				if ( <%= m_viewInNewBrowser %> == false )
				{
					navigateTo( link );
				}
				else
				{
					GetPopupScript( true, true, link ) ;
				}
			}
			else if(selectedMenuItemId == "viewAnalysis")
			{
				var link = "/viewer/searchframe.aspx?format=analysis&name=" + encode(itemName);
				if ( <%= m_viewInNewBrowser %> == false )
				{
					navigateTo( link );
				}
				else
				{
					GetPopupScript( true, false, link ) ;
				}
				
			}
			else if(selectedMenuItemId == "createQuery" || selectedMenuItemId == "editQuery")
			{
				var link = "/query/create.aspx?__executableName=" + encode(itemName) + "&querydescription=" + encode(description);
				GetPopupScript( false, false, link );
			}
			else if(selectedMenuItemId == "executeQuery")
			{
				var link = "/query/execute.aspx?__requesttype=immediate&__executableName="+ encode(itemName);
				GetPopupScript( false, false, link ) ;
			}
			else if(selectedMenuItemId == "scheduleQuery")
			{
				var link = "/query/submit.aspx?__requesttype=scheduled&__executableName="+ encode(itemName);
				GetPopupScript( false, false, link ) ;
			}
			else if(selectedMenuItemId == "newBackgroundJob")
			{
				var link = "/newrequest/index.aspx?__requesttype=scheduled&__executableName="+ encode(itemName);
				navigateTo( link );
			}
			else if(selectedMenuItemId == "newImmediateJob")
			{
				var link = "/newrequest/index.aspx?__requesttype=immediate&__executableName="+ encode(itemName);
				navigateTo( link );
			}
			else if(selectedMenuItemId == "share")
			{
				var link = "/filesfolders/privilege.aspx?subpage=share&name=" + encode(itemName);
				navigateTo( link );
			}
			else if(selectedMenuItemId == "delete")
			{
				var DeletionMsg = null;
				if ( FileType.toLowerCase() == "directory" )
				{
					DeletionMsg = '<%=m_folderDeletionMsg%>';
				}
				else
				{
					DeletionMsg = '<%=m_fileDeletionMsg%>';
				}
				
				DeletionMsg = messageFormat( DeletionMsg, new Array(itemName));
				if ( confirm( DeletionMsg ) )
				{
					var link = "/filesfolders/do_drop.aspx?name=" + encode(itemName);
					navigateTo( link );
				}
			}
			else if(selectedMenuItemId == "details")
			{
				var link = "/filesfolders/index.aspx?subpage=_detail&name=" + encode(itemName);
				navigateTo( link );	
			}
		}
</SCRIPT>
<DIV id="popup" class="popupMenu" onMouseOver="enterMenu()" onMouseOut="leaveMenu()">
	<Table style="PADDING-RIGHT:120px" cellspacing="0" cellpadding="0">
		<TR id="openFolder" class="popupMenuItem" onMouseOver="highlightItem(this, true)" onMouseOut="highlightItem(this, false)"
			onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl key="files.popupmenu.label.openFolder" runat="server" id="AcLabelControl1" />
			</TD>
		</TR>
		<TR id="openDocument" class="popupMenuItem" onMouseOver="highlightItem(this, true)"
			onMouseOut="highlightItem(this, false)" onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl key="files.popupmenu.label.viewDocument" runat="server" id="AcLabelControl2" />
			</TD>
		</TR>
		<TR id="viewXLS" class="popupMenuItem" onMouseOver="highlightItem(this, true)" onMouseOut="highlightItem(this, false)"
			onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl key="files.popupmenu.label.viewExcel" runat="server" id="AcLabelControl3" />
			</TD>
		</TR>
		<TR id="viewPDF" class="popupMenuItem" onMouseOver="highlightItem(this, true)" onMouseOut="highlightItem(this, false)"
			onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl Key="files.popupmenu.label.viewPDF" runat="server" id="AcLabelControl4" />
			</TD>
		</TR>
		<TR id="viewAnalysis" class="popupMenuItem" onMouseOver="highlightItem(this, true)"
			onMouseOut="highlightItem(this, false)" onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl Key="files.popupmenu.label.viewAnalysis" runat="server" id="AcLabelControl5" />
			</TD>
		</TR>
		<TR id="popup__separator" class="popupMenuSeparator">
			<TD>
			</TD>
		</TR>
		<TR id="createQuery" class="popupMenuItem" onMouseOver="highlightItem(this, true)" onMouseOut="highlightItem(this, false)"
			onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl Key="files.popupmenu.label.createQuery" runat="server" id="AcLabelControl6" />
			</TD>
		</TR>
		<TR id="editQuery" class="popupMenuItem" onMouseOver="highlightItem(this, true)" onMouseOut="highlightItem(this, false)"
			onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl Key="files.popupmenu.label.editQuery" runat="server" id="AcLabelControl7" />
			</TD>
		</TR>
		<TR id="executeQuery" class="popupMenuItem" onMouseOver="highlightItem(this, true)"
			onMouseOut="highlightItem(this, false)" onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl Key="files.popupmenu.label.executeQuery" runat="server" id="AcLabelControl8" />
			</TD>
		</TR>
		<TR id="scheduleQuery" class="popupMenuItem" onMouseOver="highlightItem(this, true)"
			onMouseOut="highlightItem(this, false)" onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl Key="files.popupmenu.label.scheduleQuery" runat="server" id="AcLabelControl9" />
			</TD>
		</TR>
		<TR id="popup__separator" class="popupMenuSeparator">
			<TD cellpadding="2">
			</TD>
		</TR>
		<TR id="newBackgroundJob" class="popupMenuItem" onMouseOver="highlightItem(this, true)"
			onMouseOut="highlightItem(this, false)" onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl Key="files.popupmenu.label.newBackgroundJob" runat="server" id="AcLabelControl10" />
			</TD>
		</TR>
		<TR id="newImmediateJob" class="popupMenuItem" onMouseOver="highlightItem(this, true)"
			onMouseOut="highlightItem(this, false)" onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl Key="files.popupmenu.label.newImmediateJob" runat="server" id="AcLabelControl11" />
			</TD>
		</TR>
		<TR id="popup__separator" class="popupMenuSeparator">
			<TD>
			</TD>
		</TR>
		<TR id="share" class="popupMenuItem" onMouseOver="highlightItem(this, true)" onMouseOut="highlightItem(this, false)"
			onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl Key="files.popupmenu.label.share" runat="server" id="AcLabelControl12" />
			</TD>
		</TR>
		<TR id="delete" class="popupMenuItem" onMouseOver="highlightItem(this, true)" onMouseOut="highlightItem(this, false)"
			onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl Key="files.popupmenu.label.delete" runat="server" id="AcLabelControl13" />
			</TD>
		</TR>
		<TR id="popup__separator" class="popupMenuSeparator">
			<TD>
			</TD>
		</TR>
		<TR id="details" class="popupMenuItem" onMouseOver="highlightItem(this, true)" onMouseOut="highlightItem(this, false)"
			onClick="performAction(this)">
			<TD>
				<localized:AcLabelControl Key="files.popupmenu.label.details" runat="server" id="AcLabelControl14" />
			</TD>
		</TR>
	</Table>
</DIV>
