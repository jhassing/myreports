/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols.filesfolders
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes;
	using actuate_i18n.classes;
	using actuate_i18n;
	using System.Collections;
	using activeportal.classes.configuration;

	/// <summary>
	///		Summary description for popupmenu.
	/// </summary>
	public partial  class popupmenu : System.Web.UI.UserControl
	{
		// Make sure that the Ids returned here match those mentioned in the 
		// page

		protected string 	m_viewInNewBrowser = "false";
		protected string    m_fileDeletionMsg;
		protected string    m_folderDeletionMsg;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			bool viewInNewBrowser = false;

			Hashtable userProfile = null;
			//Hashtable userProfile = AcProfileManager.GetUserProfile( Page );
			if ( userProfile != null )
			{
				m_viewInNewBrowser = (string) userProfile["viewInNewBrowserWindow"];
				
				if ( m_viewInNewBrowser != null && m_viewInNewBrowser.Trim() != String.Empty )
				{
					m_viewInNewBrowser = m_viewInNewBrowser.ToLower() ;
				}
			}
			else
			{
				viewInNewBrowser =  AcParams.urlCheckboxState( Page, AcParams.Name.viewNewBrowserWindow, viewInNewBrowser , AcParams.From.Url  );
				m_viewInNewBrowser = viewInNewBrowser.ToString().ToLower();
			}
	

			m_folderDeletionMsg  = ActuUtil.jsEncode( AcResourceManager.GetString( "QUESTION_DELETE_FOLDER", Session ) )  ;
			m_fileDeletionMsg =    ActuUtil.jsEncode( AcResourceManager.GetString( "QUESTION_DELETE_FILE",  Session ) ) ;
		}

	
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion
	}
}
