/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using activeportal.classes;
	using actuate_i18n;

	/// <summary>
	///		Summary description for saveas.
	/// </summary>
	public partial  class saveas : System.Web.UI.UserControl
	{
		protected String sTitleDocName;
		protected String sId;
		protected String sName;
		protected String sConnectionHandle;
		protected String sVersionId;
		protected String sType;
		protected String sFormat;
		protected String sPage;
		protected String sTotalPages;
		protected String sCalledFrom;

		protected String sServerURL;
		protected String sVolume;
		protected String sLoginID;
		protected String sAuthID;
		protected String acLocale;

		protected String sFwdURL;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			sId = AcParams.Get(Page, AcParams.Name.id);
			sName = AcParams.Get(Page, AcParams.Name.name);
			bool bExistConnHandle = AcParams.Exists(Page, AcParams.Name.connectionhandle);
			sConnectionHandle = AcParams.Get(Page, AcParams.Name.connectionhandle);

			sVersionId = ActuUtil.getVersionFromFilename(sName);
			sName = ActuUtil.stripVersionNumber(sName);
			sType = ActuUtil.getFileExtension(sName);

			sFormat = AcParams.Get(Page, AcParams.Name.format);
			sPage = AcParams.Get(Page, AcParams.Name.page);

			try
			{
				Convert.ToInt16(sPage.Trim());
			}
			catch(Exception)
			{
				sPage = "1";
			}


			sFwdURL = AcParams.createQueryString(Page, true);
			sFwdURL = sFwdURL.Replace("'", "%27");

			sServerURL = AcParams.Get(Page, AcParams.Name.serverURL);
			sVolume = AcParams.Get(Page, AcParams.Name.volume);
			sLoginID = AcParams.Get(Page, AcParams.Name.userID);
			sAuthID = AcParams.Get(Page, AcParams.Name.password);
			acLocale = AcParams.Get(Page, AcParams.Name.locale);

			sTotalPages = AcParams.Get(Page, AcParams.Name.pagecount);
			sCalledFrom = AcParams.Get(Page, AcParams.Name.calledfrom);

			sTitleDocName = ActuUtil.GetDocNameForViewerTitle(bExistConnHandle, Page);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
