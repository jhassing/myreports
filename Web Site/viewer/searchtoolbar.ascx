<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.viewing.viewer.searchtoolbar" CodeFile="searchtoolbar.ascx.cs" %>
<%-- Import name spaces --%>
<%@ Import namespace="activeportal.classes" %>
<%@ Import namespace="actuate_i18n" %>
<%@ Register TagPrefix="template" namespace="actuate.classes.webcontrols.masterpages" assembly="activeportal" %>
<%-- Gather authentication credentials at the top of all level 0 pages --%>

<%-- RESTORE PAGE CONTENT TO UNICODE/UTF8 --%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Expires" content="-1">
		<link rel="stylesheet" type="text/css" href="../css/allstyles.css">
		<template:css runat="server"/>
	</head>
	<script language="JavaScript" src='../js/browsertype.js'></script>
	<script language="JavaScript">
	var ns4 = (document.layers) ? true : false;

	var buttonsEnabled = false;
	var searchToolbarLoaded = false;

	var g_imageDir = "../images/viewer/";
	var classLinkOff = "LinkOff";
	var classLinkOver = "LinkOver";
	var classLinkDisable = "LinkDisable";

	var str = "";
	var searchHelpURL = eval( 'parent.frames.toolbarframe.g_searchTipURL' );
	</script>
	<script language="JavaScript" SRC="../js/searchtoolbar.js"></script>
	<script language="JavaScript">
	
	g_MsgSearchNotRun = "<%= ActuUtil.jsEncode( AcResourceManager.GetString( "ERRMSG_SEARCH_NOT_RUN", Session )) %>" ;
	g_MsgNoSearchCriteria = "<%= ActuUtil.jsEncode(AcResourceManager.GetString( "ERRMSG_NO_SEARCH_CRITERIA_SPECIFIED", Session )) %>" ;
	g_MsgSpecifySearch = "<%= ActuUtil.jsEncode(AcResourceManager.GetString( "ERRMSG_SPECIFY_SEARCH_CRITERIA", Session ))  %>";

	// Use the string to consturct the entire HTML for the toolbar and do a single document.write()
	str += "<BODY marginwidth='0' marginheight='4' topmargin='4' rightmargin='0' class='navbar' onload='STOnLoad()'>";
	str += "<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH='100%' ALIGN=RIGHT>";
	str += "<TR>";

	if (ns4)
	{
		str += "<TD ALIGN=LEFT>";
		str += "<A HREF='w' style='TEXT-DECORATION: none'>";
		str += "</A>";
		str += "</TD>";
	}
	// New Search button
	str += "<TD NOWRAP ALIGN=LEFT>";
	str += "<B><A Title='<%= AcResourceManager.GetString("TT_NEW_SEARCH", Session) %>' id='newsearchButton' class='LinkDisable' style='TEXT-DECORATION: none' HREF='javascript:clickNewsearch();' onMouseOver='setNewsearch(\"" + classLinkOver + "\");return false;'";
	str += " onMouseOut='setNewsearch(\"" + classLinkOff + "\");return false;'>";
	str += "<%= AcResourceManager.GetString("BTN_NEW_SEARCH", Session) %>";
	str += "</A></B>&nbsp&nbsp";
	str += "</TD>";


	// Search Now Button
	str += "<TD NOWRAP ALIGN=LEFT>";
	str += "<B><A Title='<%= AcResourceManager.GetString("TT_SEARCH_NOW", Session) %>' id='searchnowButton' class='LinkDisable' style='TEXT-DECORATION: none' HREF='javascript:myVoid()' onMouseOver='if (isInRequestMode()) setSearchnow(\"" + classLinkOver + "\"); return false;'";
	str += " onMouseOut='if (isInRequestMode()) setSearchnow(\"" + classLinkOff + "\"); return false;'";
	str += " onClick='if (isInRequestMode()) clickSearchnow(); return false;'>";
	str += "<%= AcResourceManager.GetString("BTN_SEARCH_NOW", Session) %>";
	str += "</A></B>";

	str += "</TD>";

	// Help Button
	str += "<TD NOWRAP ALIGN=RIGHT WIDTH='90%'>";
	str += "<B><A Title='<%= AcResourceManager.GetString("TT_HELP", Session) %>' id='helpButton' class='LinkOff' style='TEXT-DECORATION: none' HREF='javascript:myVoid()' onClick='openNewDefaultWindow(\"" + searchHelpURL + "\", \"blank\"); return false'";
	str += " onMouseOver='setHelp(\"" + classLinkOver + "\");return false;'";
	str += " onMouseOut='setHelp(\"" + classLinkOff + "\");return false;'>";
	str += "<%= AcResourceManager.GetString("BTN_HELP", Session) %>";
	str += "</A></B>";
	str += "</TD>";


	// Exit Button
	str += "<TD NOWRAP ALIGN=RIGHT WIDTH='10%'>";
	str += "<B><A Title='<%= AcResourceManager.GetString("TT_CLOSE_SEARCH", Session) %>' id='closeButton' class='LinkDisable' style='TEXT-DECORATION: none' HREF='javascript:clickClose();' onMouseOver='setClose(\"" + classLinkOver + "\");return false;'";
	str += " onMouseOut='setClose(\"" + classLinkOff + "\");return false;'>";
	str += "<%= AcResourceManager.GetString("BTN_X", Session) %>";
	str += "</A></B>";
	str += "</TD>";

	str += "</TR>";
	str += "</TABLE>";
	str += "</BODY>";

	document.write(str);
	</script>
	<script>
	forceOnLoad();
	</script>
</html>
