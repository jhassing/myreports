/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Collections;

	using activeportal.classes;
	using activeportal.classes.api;
	using activeportal.classes.api.proxy;

	/// <summary>
	///		Summary description for requestsearch.
	/// </summary>
	public partial  class requestsearch : System.Web.UI.UserControl
	{
		//	Parameters used in ASP page
		protected String sId				= "";
		protected String sName				= "";
		protected String sRosFileId			= "";
		protected ArrayList arryROSFiles	= null;
		protected String searchJSCommand	= "";
		protected String connectionHandle	= "";

		protected String executableId		= "";
		protected String executableName		= "";
		
		private bool isBoundledReport		= false;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			//	For transcient report, we need executablename instead of id.
			executableName = AcParams.Get(Page, AcParams.Name.__executableName, "");
			executableId = AcParams.Get(Page, AcParams.Name.__executableId, "");
			sName = AcParams.Get(Page, AcParams.Name.name, "");
			sId = AcParams.Get(Page, AcParams.Name.id, "");
			sRosFileId = AcParams.Get(Page, AcParams.Name.rosFileId, "");
			connectionHandle = AcParams.Get(Page, AcParams.Name.connectionhandle, "");

			//	Need js commands
			searchJSCommand = AcParams.Get(Page, AcParams.Name.searchJSCommand, "");
			AcParams.SetToSession(Session, AcParams.Name.searchJSCommand, null);

			//	If there's no id/name specified.
			if(( sId == null || sId.Trim().Length <= 0) &&
				( sName == null || sName.Trim().Length <= 0))
			{
				sName = AcParams.Get(Page, AcParams.Name.basedOnFileName, "");
				sId = AcParams.Get(Page, AcParams.Name.basedOnFileId, "");
			}

			if (executableId == null || executableId.Trim().Length <= 0)
			{
				//	Get executable id
				if (executableName == null || executableName.Trim().Length <= 0)
				{
					executableId = getDependentFileId(sName, sId);
					if (executableId == null || executableId.Trim().Length <= 0)
					{
						isBoundledReport = true;
					}
				}
				else
				{
					//	A dov file.
					String l_extension = ActuUtil.getFileExtension(executableName);
					if (l_extension != null &&
						("dov".Equals(AcString.ToLower(l_extension)) || "rov".Equals(AcString.ToLower(l_extension))))
					{
						executableId = getDependentFileId(executableName, null);
					}
				}
			}

			//	Get ros file list
			if (executableId != null && executableId.Trim().Length > 0 ||
				executableName != null && executableName.Trim().Length > 0)
				
			{
				arryROSFiles = getROSFiles(executableName, executableId);
			}
			else if (isBoundledReport)
			{
				arryROSFiles = getROSFiles(sName, sId);
			}
		}

		/**
		 *	Get dependent executable file id
		 */
		private String getDependentFileId(String roiName, String roiId)
		{
			String[] resultDef = new String[] {"Id", "Name", "FileType", "Version"};
			
			//	Prepare the condition
			FileSearch fileSearch = new FileSearch();
			FileCondition fileCondition = new FileCondition();
			fileCondition.Field = FileField.FileType;
			fileCondition.Match = "rox,dox";
			
			fileSearch.Item = fileCondition;

			if (roiName != null && roiName.Trim().Length > 0)
			{
				fileSearch.Item1 = roiName;
				fileSearch.Item1ElementName = Item1ChoiceType10.DependentFileName;
			}
			else if (roiId != null && roiId.Trim().Length > 0)
			{
				fileSearch.Item1 = roiId;
				fileSearch.Item1ElementName = Item1ChoiceType10.DependentFileId;
			}
			
			fileSearch.FetchSize = ActuUtil.ReadIntFromConfig( "MAX_LIST_SIZE", Constants.MAX_LIST_SIZE_DEFAULT );
			fileSearch.FetchSizeSpecified = true;
			
			fileSearch.IncludeHiddenObject = true;
			fileSearch.IncludeHiddenObjectSpecified = true;

			AcSearchFiles SearchFiles = new AcSearchFiles(resultDef, fileSearch, true, "/", false);
			
			SearchFiles.Execute(Page);
			
			//	Get executable file
			File executableFile = null;
			if (SearchFiles.ObjectList.Count > 0)
			{
				executableFile = (File) SearchFiles.ObjectList[0];
			}
			
			if (executableFile != null)
			{
				return executableFile.Id;
			}
			else
			{
				return null;
			}
		}

		/**
		 *	Get dependent ros files
		 */
		private ArrayList getROSFiles(String executableName, String executableId)
		{
			String[] resultDef = new String[] {"Id", "Name", "FileType", "Version"};
			
			//	Prepare the condition
			FileSearch fileSearch = new FileSearch();
			FileCondition fileCondition = new FileCondition();
			fileCondition.Field = FileField.FileType;
			fileCondition.Match = "ros";
			
			fileSearch.Item = fileCondition;

			if (executableId != null && executableId.Trim().Length > 0)
			{
				fileSearch.Item1 = executableId;
				fileSearch.Item1ElementName = Item1ChoiceType10.RequiredFileId;
			}
			else if (executableName != null && executableName.Trim().Length > 0)
			{
				fileSearch.Item1 = executableName;
				fileSearch.Item1ElementName = Item1ChoiceType10.RequiredFileName;
			}
			
			fileSearch.FetchSize = ActuUtil.ReadIntFromConfig( "MAX_LIST_SIZE", Constants.MAX_LIST_SIZE_DEFAULT );
			fileSearch.FetchSizeSpecified = true;
			
			AcSearchFiles SearchFiles = new AcSearchFiles(resultDef, fileSearch, true, "/", false);
			
			SearchFiles.Execute(Page);
			
			//	Get ros files
			if (SearchFiles.ObjectList.Count > 0)
			{
				return SearchFiles.ObjectList;
			}
			else
			{
				return null;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
