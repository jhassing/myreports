/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Text;
	using System.Collections;
	using System.Xml;

	using activeportal.classes;
	using activeportal.classes.api;
	using activeportal.classes.api.proxy;

	/// <summary>
	///		Summary description for searchframe.
	/// </summary>
	public partial  class searchframe : System.Web.UI.UserControl
	{
		protected bool bSelectAbsent;
		protected bool isAbsent;

		protected String sFwdURL;
		protected String sId;
		protected String sName;
		protected String strConnectionHandle;
		protected String sVersionId;
		protected String sOutputName;
		protected String sType;
		protected String sFormatType;
		protected String sHits;
		protected String sStartingPoint;
		protected String sEnablecolumnheaders;
		protected String sUsequotedelimiter;

		protected String sServerURL;
		protected String sVolume;
		protected String sLoginID;
		protected String sAuthID;
		protected String acLocale;

		protected int iStartHitNumber;
		protected int iNumberOfHitsOnPage;
		protected int iEndHitNumber;
		protected int iTotalNumberOfHits;
		protected int iEndCurrentHit;
		protected String CommandName = "searchReport";

		protected String sHelpBase;

		protected ArrayList parameterList = new ArrayList();

		protected String[] llFormats;

		protected AcSearchResult searchResult;
		protected bool needSave = true;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			sServerURL = AcParams.Get(Page, AcParams.Name.serverURL);
			sVolume = AcParams.Get(Page, AcParams.Name.volume);
			sLoginID = AcParams.Get(Page, AcParams.Name.userID);
			sAuthID = AcParams.Get(Page, AcParams.Name.password);
			acLocale = AcParams.Get(Page, AcParams.Name.locale);

			StringBuilder sFwdURLBuf = new StringBuilder();
			sFwdURLBuf.Append(AcParams.createQueryString(Page, "id", false));
			if (AcParams.Exists(Page, "id"))
			{
				sFwdURLBuf.Append(AcParams.createQueryString(Page, "name", true));
			}
			else
			{
				sFwdURLBuf.Append(AcParams.createQueryString(Page, "name", false));
			}
			sFwdURLBuf.Append(AcParams.createQueryString(Page, "__executablename", true));
			sFwdURLBuf.Append(AcParams.createQueryString(Page, "__executableid", true));
			sFwdURLBuf.Append(AcParams.createQueryString(Page, "connectionhandle", true));
			sFwdURL = sFwdURLBuf.ToString();

			sId = AcViewerParams.Get(Page, "id");
			sName = AcViewerParams.Get(Page, "name");
			sVersionId = AcViewerParams.Get(Page, AcParams.Name.version, null);
			sType = AcViewerParams.Get(Page, AcParams.Name.type, null);
			strConnectionHandle = AcParams.Get(Page, AcParams.Name.connectionhandle);

			if (sVersionId == null || sVersionId.Trim().Length <= 0)
			{
				sVersionId = ActuUtil.getVersionFromFilename(sName);
			}
			
			sName = ActuUtil.stripVersionNumber(AcViewerParams.Get(Page, "name"));
			sOutputName = AcViewerParams.Get(Page, "outputname");
			
			if (sType == null || sType.Trim().Length <= 0)
			{
				sType = ActuUtil.getFileExtension(sName);
			}
			sFormatType = AcViewerParams.Get(Page, "format");
			if (sFormatType == null)
			{
				sFormatType = "XMLDisplay";
			}

			sHits = AcViewerParams.Get(Page, "hits");
			sStartingPoint = AcViewerParams.Get(Page, "startingpoint");

			sEnablecolumnheaders	= AcViewerParams.Get(Page, "enablecolumnheaders");
			if ( sEnablecolumnheaders == null )
			{
				sEnablecolumnheaders = System.Configuration.ConfigurationSettings.AppSettings[ "SEARCH_ENABLE_COLUMN_HEADERS" ];
			}

			sUsequotedelimiter	= AcViewerParams.Get(Page, "usequotedelimiter");
			if ( sUsequotedelimiter == null )
			{
				sUsequotedelimiter = System.Configuration.ConfigurationSettings.AppSettings[ "SEARCH_USE_QUOTE_DELIMITER" ];
			}
			Hashtable htSearchSelectParam = AcViewerParams.GetSearchParameters(Page, false);			

			ArrayList llSelect = (ArrayList) htSearchSelectParam["llselect"];
			ArrayList llSearch = (ArrayList) htSearchSelectParam["llsearch"];

	
			bool bSelectAbsent = (llSelect.Count == 0 );
			bool isAbsent = (llSearch.Count == 0 );

			//	Search report
			if(true)
			{
				AcSearchReport searchReport = new AcSearchReport(	sId,
											sName,
											sType,
											sVersionId,
											sFormatType,
											sStartingPoint,
											sHits,
											sEnablecolumnheaders,
											sUsequotedelimiter,
											llSelect,
											llSearch,
											Request.UserAgent );
				searchReport.Execute( Page, strConnectionHandle );

				needSave = !(searchReport.ReportType == SearchReportResponseReportType.InfomationObject);

				if (sFormatType != null && !AcString.ToLower(sFormatType).Equals("xmldisplay"))
				{
					// analysisResult - we have streamed the attachment from the server to the page, no further action 
					// required
					this.Visible = false;
					return;
				}

				searchResult = new AcSearchResult((XmlDocument) searchReport.SearchResult);

				// Check format information to see whether the e.Analysis is available
				// Used in ascx file
				
				AcGetFormats getFormats = new AcGetFormats(sId, sName);
				getFormats.Execute( Page, strConnectionHandle );
				llFormats = getFormats.Response.FormatList;

			

				iStartHitNumber = 0;
				iNumberOfHitsOnPage = 0;

				try
				{
					iStartHitNumber = Convert.ToInt32(sStartingPoint);
				}
				catch (Exception)
				{
					iStartHitNumber = 0;
				}

				try
				{
					iNumberOfHitsOnPage = Convert.ToInt32(sHits);
				}
				catch (Exception)
				{
					iNumberOfHitsOnPage = 20;
					if (sHits == null)
					{
						iStartHitNumber = 0;
					}
				}

				iEndHitNumber = Convert.ToInt32(searchResult.GetHits());
				iTotalNumberOfHits = Convert.ToInt32(searchResult.GetNumberOfEntries());
				iEndCurrentHit = iStartHitNumber + iTotalNumberOfHits;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
