﻿<%@ Register TagPrefix="template" namespace="actuate.classes.webcontrols.masterpages" assembly="activeportal" %>
<%@ Import namespace="actuate_i18n" %>
<%@ Import namespace="activeportal.classes" %>
<%@ Import namespace="System.Text" %>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.viewing.viewer.viewnavigation" CodeFile="viewnavigation.ascx.cs" %>
<HTML>
	<HEAD>
		<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
		<%-- Import name spaces --%>
		<%-- Gather authentication credentials at the top of all level 0 pages --%>
		<script LANGUAGE="JavaScript" src="../js/viewer.js"></script>
		<script>
    var g_helpBase = "<%= sHelpBase %>";
    var sDefParams = "serverURL=<%= HttpUtility.UrlEncode(sServerURL) %>&volume=<%= HttpUtility.UrlEncode(sVolume) %>";

	/**
		This function has the following parameters
		sDescription - A hardcoded unique string used for identifying the button.
		sToolTip	- The tool tip for this button
		sCaption	- The String to be displayed on this button
		sState		- The current state of the button.
	 */
    function addButton(sDescription, sToolTip, sCaption, sState)
    {
        // CREATE the BUTTON AND ASSIGN the properties
        i = btnArray.length;
        btn = new Button();
        btn.Id = i + 1;
        btn.bName = "button" + (i + 1);
        btn.bDescription = sDescription;
        btn.bState = sState;
        btn.sToolTip = (sToolTip != null) ? "title='" + sToolTip + "' " : "";
        btn.aCaptions = sCaption;
        btnArray[i] = btn;
    }
    
    function getNavigationBarForm()
    {
		btnArray = new Array();
        var str = ""; // HOLDS THE HTML FOR THE FORM CONTAINING THE ENTIRE NAVIGATION BAR

        if (!ns4)
        {
            // Position the form to the top of the container for MSIE on MacOS
            if (g_browserType.name == "IE" && g_browserType.platform == "Mac")
            {
                str += "<form style='position:absolute;top:4;margin-bottom:0; margin-top:0;' name='form1' onSubmit='handlePageEdit(event); return false'>";
            }
            else
            {
                str += "<form style='margin-bottom:0;' name='form1' onSubmit='handlePageEdit(event); return false'>";
            }
        	str += "<table valign=center border=0 cellspacing=0 cellpadding=0>";
        }

        // THE START OF THE TABLE ROW HOLDING ALL NAV BUTTONS
        
        
        // SPECIAL DEFAULT STYLE FOR THE 'TOC' BUTTON
        if (!js_IsReportCompleted)
        {
            tocClass = classLinkDisable;
        }
        else if (parent.g_searchOpen)
        {
            tocClass = classLinkOff;
        }
        else if (parent.tocOpen)
        {
            tocClass = classLinkOver;
        }
        else
        {
            tocClass = classLinkOff;
        }

        // FIRST SET UP ALL THE BUTTONS, STYLES, ETC IN AN ARRAY OF BUTTON OBJECTS
		addButton("toc", "", "", null);
		addButton("firstpage", "<%= AcResourceManager.GetString("TT_FIRST", Session) %>", "<%= AcResourceManager.GetString("BTN_FIRST", Session) %>", "dis");
		addButton("previouspage", "<%= AcResourceManager.GetString("TT_PREV", Session) %>", "<%= AcResourceManager.GetString("BTN_PREV", Session) %>", "dis");
		addButton("nextpage", "<%= AcResourceManager.GetString("TT_NEXT", Session) %>", "<%= AcResourceManager.GetString("BTN_NEXT", Session) %>", "dis");
		addButton("lastpage", "<%= AcResourceManager.GetString("TT_LAST", Session) %>", "<%= AcResourceManager.GetString("BTN_LAST", Session) %>", "dis");
		addButton("goto", "<%= AcResourceManager.GetString("TT_GOTO", Session) %>", "<%= AcResourceManager.GetString("BTN_GOTO", Session) %>", "dis");
		addButton("pagelabel", null, "<%= AcResourceManager.GetString("TXT_RETRIEVING", Session) %>", "dis");
		addButton("cancel", "<%= AcResourceManager.GetString("TT_CANCEL", Session) %>","<%= AcResourceManager.GetString("BTN_CANCEL", Session) %>", "dis");
		addButton("search", "", "", "dis");
		addButton("download", "<%= AcResourceManager.GetString("TT_D_P", Session) %>", "<%= AcResourceManager.GetString("BTN_D_P", Session) %>", "dis");
		addButton("print", "<%= AcResourceManager.GetString("TT_PRINT", Session) %>", "<%= AcResourceManager.GetString("BTN_PRINT", Session) %>", "dis");
		// addButton("help", "<%= AcResourceManager.GetString("TT_HELP", Session) %>", "<%= AcResourceManager.GetString("BTN_HELP", Session) %>", "off");
		addButton("help", "", "", "dis");
		addButton("exit", "<%= AcResourceManager.GetString("TT_EXIT_REPORT", Session) %>", "<%= AcResourceManager.GetString("BTN_X", Session) %>", "off");
					
        var btnLocation = getBtnLocation(btnArray);

        nButtons = btnArray.length;

        // HARDCODED PROPERTIES IN THE BUTTON ARRAY DATA STRUCTURE
        if (!js_IsReportCompleted)
        {
            btnArray[0].fState = "disable"
        }
        else if (parent.tocOpen)
        {
            btnArray[0].fState = "on";
        }
        else
        {
	    btnArray[0].fState = "off";
        }
        btnArray[12].bDestroy = <%= ActuUtil.jsBooleanEncode(bCloseOnX) %>;
        searchClass = classLinkDisable;

        // THE SECOND LOOP CREATES THE DHTML TO PRESENT THE BUTTONS WITH APPROPRIATE STYLES, ETC
        for (i = 0; i < nButtons; i++)
        {
        	var localBtn = btnArray[i];
        	
        	localBtn.left = btnLocation[i];
            // SETUP 'CLASS' FOR ALL BUTTONS
            localBtn.sClassName = "LinkDisable"; // DEFAULT STYLE FOR ALL OTHER BUTTONS
            if (i == 0) // STYLE FOR 'TOC'
                localBtn.sClassName = "tocClass";
            else if (i == 6 || i== 7 || i >= 11) // STYLE FOR 'RETRIEVING', 'CANCEL', 'HELP', 'X' ARE DIFFERENT
                localBtn.sClassName = "LinkOff";

            // SETUP ALIGNMENT FOR THE 'HELP' BUTTON
            localBtn.sAlign = "";
            if (i == 11)
                localBtn.sAlign = " align=right width=90%";
            else if (i == 12)
				localBtn.sAlign = " align=right";

	        localBtn.bTwoState = (i == 0 || i == 8) ? true : false;
	        if (i != 6)
	        {
	            localBtn.bClassOn = localBtn.bClassDown = localBtn.bClassOver = classLinkOver;
	            localBtn.bClassOff = classLinkOff;
	            localBtn.bClassDis = classLinkDisable;
	        }
	        else
	        {
	            localBtn.bClassOn = localBtn.bClassOff = classLinkOff;
	        }
            
            localBtn.btnType = (localBtn.Id == 7) ? BTN_HAS_NOACTION : BTN_HAS_ACTION;// DEFINE EVENT HANDLING FOR ALL BUTTONS EXCEPT 'RETRIEVING' (WHICH IS A LABEL ONLY)
            TXT_OF = "<%= AcResourceManager.GetString("TXT_OF", Session) %>";
            localBtn.editValue  = editValue;
            
            var TOP_ALIGN = (localBtn.btnType == BTN_HAS_ACTION) ? "TOP=\"4\""  : "TOP=\"-1\"";
            
            if ( !js_IsReportCompleted || i!=7 )
	    {
	        if (ns4)
	        {
	            str += "<layer class=\"navbar\" name=\""+localBtn.bName+"\" onMouseOut=\"javascript:layerOnMouseOut(this, event, "+(i+1)+")\"  "+ TOP_ALIGN +" LEFT=\""+localBtn.left+"\">";
	        }
		str += localBtn.convertBtnToHtml();
		if (ns4)
	        {
	           str += "</layer>";
	        }
	    }
        }
        
        if (!ns4) // SPECIAL HANDLING FOR NS4
        {
			str += "</table>";
			str += "</form>"; // END OF THE FORM HOLDING THE NAVIGATION BAR
		}
        return str;
    }
		</script>
		<%
            if (!isReportCompleted)
            {
       %>
		<script>
            function progressiveReload()
            {
                parent.g_refreshCount++;
                location.reload();
            }
            var timer = <%= System.Configuration.ConfigurationSettings.AppSettings[ "PROGRESSIVE_REFRESH" ] %>;
            if ( parent.g_refreshCount == 0 )
            {
                timer = 15;
            }
            else if ( parent.g_refreshCount == 1 )
            {
                timer = 60;
            }
            else if ( timer < 60 )
            {
                timer = 60;
            }
            setTimeout("progressiveReload()", timer * 1000);
		</script>
		<%
            }
       %>
		<%-- SET THE PAGE ENCODING --%>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Expires" content="-1">
		<link rel="stylesheet" type="text/css" href="../css/allstyles.css">
		<script language="javascript" src='../js/encoder.js'></script>
		<script language="javascript" src="../js/cookie.js"></script>
		<script language="javascript" src="../js/browsertype.js"></script>
		<script language="javascript">
			var js_CurrentPage = <%= sPage %>;
			var js_ObjectURL = "<%= sFwdURL %>";
			var isProgressive = <%= ActuUtil.jsBooleanEncode(isProgressive) %>;

			parent.sConnectionHandle = "<%= ActuUtil.jsEncode(sConnectionHandle) %>";
			<% if (sConnectionHandle != null){ %>
				js_ObjectURL = replaceDuplicateParams(js_ObjectURL , "connectionHandle" , "<%= HttpUtility.UrlEncode(sConnectionHandle) %>");
			<% } %>

			var js_ObjURL = removeParams(js_ObjectURL, "searchcriteria");
			js_ObjURL = removeParams(js_ObjURL, "componentid");

			var js_TotalPages = <%= pageCount %>;
			if (js_TotalPages == null) js_TotalPages = "ERR";
			var js_IsReportCompleted = <%= ActuUtil.jsBooleanEncode(isReportCompleted) %>;
			if (js_IsReportCompleted == null) js_IsReportCompleted = false;
			var js_IsPersistent = <%= ActuUtil.jsBooleanEncode(AcParams.Get(Page, AcParams.Name.__saveOutput)) %>;
			if (js_IsPersistent == null) js_IsPersistent = false;

			var g_Page = "<%= AcResourceManager.GetString("TXT_PAGE", Session) %>";
			var g_Retrieving = "<%= AcResourceManager.GetString("TXT_RETRIEVING", Session) %>";
			var g_searchTipURL = "../help/" + "wwhelp.htm?single=true&context=jsp_viewer&topic=ViewingFramework_search";
			if ( parent.reportframe  )
			{
				if(parent.reportframe.location.href.substr(parent.reportframe.location.href.length - 13)  == "blanknav.html" && parent.sConnectionHandle != "" && parent.sConnectionHandle != null )
				{
					parent.reportframe.location.replace(replaceDuplicateParams(parent.g_reportFrameURL,"&connectionHandle",encode(parent.sConnectionHandle)));
				}
			}
		</script>
		<script language="javascript" src="../js/viewnav.js"></script>
		<script language="javascript">
        	ERRMSG_INVALID_PAGE_NUMBER = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("ERRMSG_INVALID_PAGE_NUMBER", Session)) %>";
		</script>
		<style type="text/css">
            #parLayer { LEFT: 0px; POSITION: absolute; TOP: 0px }
            #tip { POSITION: absolute }
            #tipBox { BORDER-RIGHT: black 1pt solid; BORDER-TOP: black 1pt solid; Z-INDEX: 100; BACKGROUND: white; VISIBILITY: hidden; BORDER-LEFT: black 1pt solid; WIDTH: 150px; BORDER-BOTTOM: black 1pt solid; POSITION: absolute }
            #toolbar { POSITION: absolute }
            #report { LEFT: 0px; POSITION: absolute; TOP: 35px }
            #text0 { POSITION: absolute }
            #text1 { POSITION: absolute }
            TD { FONT-SIZE: 10pt; COLOR: white; FONT-FAMILY: "Arial" }
		</style>
		
	</HEAD>
	<body OnMouseUp='swapLinkClass(this, event, currentButton)' OnLoad='onLoad()' OnUnLoad='onUnload()'
		OnResize='onResize()'>
		<div class="navbar" id="toolbar" name="toolbar"></div>
	</body>
</HTML>
