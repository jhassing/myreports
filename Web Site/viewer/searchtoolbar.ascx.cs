/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes;

	/// <summary>
	///		Summary description for searchtoolbar.
	/// </summary>
	public partial  class searchtoolbar : System.Web.UI.UserControl
	{

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// get locale from url and set it in session so that it shows in the right locale,
			// even the session has timed out.
			string locale = AcParams.Get( Page, AcParams.Name.locale, AcParams.From.Url );

			if ( locale != null )
			{
				AcParams.Get( Page, AcParams.Name.locale, AcParams.From.Url, AcParams.SetTo.Session );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
