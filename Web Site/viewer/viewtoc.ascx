<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.viewing.viewer.viewtoc" CodeFile="viewtoc.ascx.cs" %>
<%-- Import name spaces --%>
<%@ Import namespace="activeportal.classes" %>
<%@ Import namespace="activeportal.classes.api" %>
<%@ Import namespace="actuate_i18n" %>
<%-- Gather authentication credentials at the top of all level 0 pages --%>
<script>
	var js_ObjectURL = "<%= sFwdURL %>";
</script>
<script src="../js/viewer.js"></script>
<HTML>
	<HEAD>
		<%-- SET THE PAGE ENCODING --%>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Expires" content="-1">
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/encoder.js'></SCRIPT>
		<script src="../js/toctree.js"></script>
		<script src='../js/cookie.js'></script>
		<script src='../js/browsertype.js'></script>
		<SCRIPT LANGUAGE="JavaScript">

			initGlobals();
			g_vp = "viewpage.aspx" + replaceDuplicateParams(js_ObjectURL , "format" , "DHTML") ;
			g_vt = "viewtoc.aspx" + replaceDuplicateParams(replaceDuplicateParams(js_ObjectURL , "format" , "XMLDisplay") , "depth" , "1");
			g_n  = g_nodesArray;

			// This string is used in place of empty TOC element.
			var Empty_TOC_Description = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

			function emptyString( str )
			{
    			var len = str.length;
    			var i;
    			var newStr;
    			for(i = 0; i < len; i++)
    			{
        			newStr = str.charAt(i).replace(/\s/g, "");
        			if (newStr.length == 0)
            			continue;

        			return false;
    			}
    			return true;
			}

			function xf( p1, p2, p3, p4, p5 )
			{
        			if( emptyString( p2 ) )
	        			return createFolderObject( p1, Empty_TOC_Description, p3, p4, p5 );
				else
	        			return createFolderObject( p1, p2, p3, p4, p5 );
			}

			function xif( p1, p2 )
			{
				return insertFolder( p1, p2 );
			}

			<%
				String CurrentTOCParentID  = m_componentId;
				if (!CurrentTOCParentID.Equals("0"))
				{
			%>
					restoreNodes()
			<%
				}
				String InFirst = "TRUE";
				AcTocNode tnNode;
				while((tnNode = TOC.Next()) != null)
				{
    				if (InFirst.Equals( "TRUE"))
    				{
            			if( CurrentTOCParentID.Equals("0"))
            			{
			%>
            				g_ft = g_Tree = xf("<%= tnNode.GetObjId() %>", "<%= ActuUtil.jsEncode(ActuUtil.convertUTF8toUCS2(tnNode.GetText())) %>", "<%= tnNode.GetLinkTo() %>");
            <%
            			}
            			InFirst = "FALSE";
    				}
    				else
    				{
        				if (CurrentTOCParentID.Equals("0"))
            			{
			%>
							g_n["var<%= tnNode.GetObjId() %>"] = xif(g_ft,
			<%
                		}
                    	else
                		{
			%>
							g_n["var<%= tnNode.GetObjId() %>"] = xif(g_n["var<%= tnNode.GetParentId() %>"],
			<%
                		}
			%>

			xf("<%= tnNode.GetObjId() %>",
        		"<%= ActuUtil.jsEncode(ActuUtil.convertUTF8toUCS2(tnNode.GetText())) %>",
        		"<%= tnNode.GetLinkTo() %>",
        		"<%= tnNode.GetObjId() %>",
			<%
        				if ( tnNode.GetExpandable().Equals("1"))
        				{
			%>
							1));
			<%
        				}
        				else
        				{
			%>
							0));
			<%
        				}
    				}
				}
			%>
			ns4 = (document.layers)? true:false

			if ( ns4 )
			{
				initDocument();
			}

		</SCRIPT>
	</HEAD>
	<SCRIPT LANGUAGE="JavaScript">
	ns4 = (document.layers)? true:false
	if ( ns4 )
	{
		document.write("<body bgcolor=white onload='initTocScrollBarPositionInfo()'>");
	}
	else
	{
		document.write("<body bgcolor=white onload='initDocument()'>");
		document.write("<div id='toclayer'></div>");
		document.write("</body>");
	}
	</SCRIPT>
</HTML>
