<%@ Reference Control="~/viewer/saveas.ascx" %>
<%@ Page language="c#" Inherits="activeportal.usercontrols.viewing.viewer.pages.saveas" contentType="text/html; charset=utf-8" CodeFile="saveas.aspx.cs" %>
<%@ Register TagPrefix="actuate" TagName="saveas" Src="saveas.ascx" %>
<%@ Register TagPrefix="actuate" TagName="authenticate" Src="../authenticate.ascx" %>
<%--
	This page represents the outermost shell that holds
	all components of the ASP.NET Active Portal User Interface. This page
	forms the root for the "Files & Folders" feature.

	PATH     : index.aspx
	LEVEL    : 0 (root page)
	LOCATION : N/A

	PARAMETERS :
	homeFolder = <string> ; required for random access to 'My Documents'
	userID = The username as specified in the login page or returned by the security manager
	volume = The volume name to which the user is connected to
	authID = An authentication ID that is internally obtained
	serverURL = <string>
	locale = <string>
	timezone = <string>
	
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<actuate:authenticate id="AcAuthenticate" runat="server"></actuate:authenticate>
<actuate:saveas runat="server" />
