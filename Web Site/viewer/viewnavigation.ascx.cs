/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
using ReportDistribution.SQLServerDAL;

namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using MyReports.Common;

	using activeportal.classes;
	using activeportal.classes.api;

	/// <summary>
	///		Summary description for viewnavigation.
	/// </summary>
	public partial  class viewnavigation : System.Web.UI.UserControl
	{
		//	Variables used in the asp pages
		protected String sId;
		protected String sName;
		protected String sConnectionHandle;
		protected String sVersionId;
		protected String sType;
		protected String sPage;
		protected String sFileName;
		protected bool isReportCompleted;
		protected String pageCount;
		
		protected String sHelpBase;
		protected String sFwdURL;

		protected bool bCloseOnX;
		protected bool isProgressive;
		
		protected String sServerURL;
		protected String sVolume;
		protected String sLoginID;
		protected String sAuthID;
		protected actuate.classes.webcontrols.masterpages.Css Css1;
		protected String acLocale;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			sId = AcParams.Get(Page, AcParams.Name.id);
			sName = AcParams.Get(Page, AcParams.Name.name);
			sConnectionHandle = AcParams.Get(Page, AcParams.Name.connectionhandle);
			bCloseOnX = AcParams.urlCheckboxState(Page, AcParams.Name.closex);
			isProgressive = AcParams.urlCheckboxState(Page, AcParams.Name.isProgressive);

			sVersionId = ActuUtil.getVersionFromFilename(sName);
			sName = ActuUtil.stripVersionNumber(sName);

			sType = ActuUtil.getFileExtension(sName);

			sPage = AcParams.Get(Page, AcParams.Name.page);
			try
			{
				Convert.ToInt16(sPage.Trim());
			}
			catch (Exception)
			{
				sPage = "1";
			}

			sServerURL = AcParams.Get(Page, AcParams.Name.serverURL);
			sVolume = AcParams.Get(Page, AcParams.Name.volume);
			sLoginID = AcParams.Get(Page, AcParams.Name.userID);
			sAuthID = AcParams.Get(Page, AcParams.Name.password);
			acLocale = AcParams.Get(Page, AcParams.Name.locale);
			
			sHelpBase = "../help/";
			
			sFwdURL = AcParams.createQueryString(Page, true);

			sFileName = AcParams.Get( Page, AcParams.Name.name);

			String version = AcParams.Get( Page, AcParams.Name.version, AcParams.From.Url );

			AcGetPageCount getPageCount = new AcGetPageCount( sFileName, sId, version );
			
			getPageCount.Execute( Page, sConnectionHandle );

			isReportCompleted = getPageCount.Response.IsReportCompleted;
			pageCount = getPageCount.Response.PageCount;
			sConnectionHandle = getPageCount.Response.ConnectionHandle;
			
			try
			{
				string[] sParms = new string[1];
				sParms[0] = "TimeStamp";
				AcGetFileDetails ddd = new AcGetFileDetails( sFileName, sId, sParms);

				ddd.Execute( Page, sConnectionHandle );

				// Log this report to the SQL Server database
				if ( ddd.Response != null )			
					new Reports(AppValues.MyReportsDB).InsertReportHasBeenViewed(AppValues.UserName, sFileName, ddd.Response.File.TimeStamp);

			}
			catch ( Exception ex )
			{

			}
	
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion
	}
}
