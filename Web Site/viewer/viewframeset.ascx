<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.viewing.viewer.viewframeset" CodeFile="viewframeset.ascx.cs" %>
<%-- Import name spaces --%>
<%@ Import namespace="activeportal.classes" %>
<%@ Import namespace="actuate_i18n" %>

<HTML>
	<HEAD>
		<%-- SET THE PAGE ENCODING --%>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Expires" content="-1">
		<TITLE>
			<%= ActuUtil.htmlEncodeBreakOnSpace(sTitle) %>
		</TITLE>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/encoder.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/viewframeset.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/cookie.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/viewframesetfuncs.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/browsertype.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/viewer.js'></SCRIPT>
	</HEAD>
	<SCRIPT>
		var sConnectionHandle = (<%= ActuUtil.jsBooleanEncode(bExistConnHandle) %>) ? "<%= ActuUtil.jsEncode(AcParams.Get(Page, AcParams.Name.connectionhandle)) %>" : null;
		var sSearchable = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TXT_SEARCHABLE", Session)) %>";
		var sRepField = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TXT_REP_FIELD", Session)) %>";
		var sRepValue = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TXT_REP_VALUE", Session)) %>";
		var sRepDisplay = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TXT_REP_DISPLAY", Session)) %>";
		var sTTRepField = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TT_REP_FIELD", Session)) %>";
		var sTTRepValue = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TT_REP_VALUE", Session)) %>";
		var sTTRepDisplay = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TT_REP_DISPLAY", Session)) %>";
        //used to store whether internal or external hyperlink has been clicked. set in handleLinkClick ().
        var bIsLinkInternal = false;

		var sID = (<%= ActuUtil.jsBooleanEncode(AcParams.Exists(Page, AcParams.Name.id)) %>) ? "<%= ActuUtil.jsEncode(AcParams.Get(Page, AcParams.Name.id)) %>" : null;
		var sServerURL = "<%= ActuUtil.jsEncode(sServerURL) %>";
		var sVolume = "<%= ActuUtil.jsEncode(sVolume) %>";
		var g_objectURL = "<%= ActuUtil.htmlEncode(sFwdURL) %>";
		var title = "<%= ActuUtil.jsEncode(sTitle) %>";
		var g_userName = "<%= ActuUtil.jsEncode(sLoginID) %>";
		var g_currentPageNumber = (<%= ActuUtil.jsBooleanEncode((sPage == null)) %>) ? null : "<%= ActuUtil.jsEncode(sPage) %>";

		var str = "";
		var g_platform = navigator.platform;
		var g_browserVersion = parseFloat(window.navigator.appVersion);
		var g_widthCheck = window.innerWidth;
		var g_heightCheck = window.innerHeight;
		var g_curURL = window.document.location.href;
		var g_stopRefresh = true;
		var g_clickActionRefresh = true;
		var isNav4 = (document.layers) ? true : false;
		var g_refreshCount = 0;

		// The resize handler should be set only for Netscape on MacOS.
		// This because when the browser is resized the event is only caught here, not in the
		// toolbar or in the report frame.
		if (isNav4 && g_platform.indexOf("Mac") != -1)
		{
			window.onResize = resizeFix;
		}

		// Fix to allow Navigation within a parent frame in Netscape 4
		if (isNav4)
		{
			if (parent.setActuateMainFrameURL)
				parent.setActuateMainFrameURL(window.document.location.pathname);
		}

		// ViewFrameset&CheckPermission is forced not to be cached,
		// so the resize problem would occur for this particular URL.
		// We need to replace the window with a URL without CheckPermission.
		if( isNav4 )
		{
			var pos = g_curURL.lastIndexOf("CheckPermission");
			if( pos != -1 )
			{
				var newURL = g_curURL.replace(/&CheckPermission/i, "");
				window.location.replace( newURL );
			}
		}

/////////////////////////////////////////////////////////////////////////////////////////////////////
//							 Global variables
/////////////////////////////////////////////////////////////////////////////////////////////////////

		// This variable will be used by methods in different frames to check if they are in the view frameset.
		var g_inViewFrameset = true;
		var g_commandName = "CommandName";
		var g_beforeHistoryLength = window.history.length;
		var tocOpen = false;
		var g_searchOpen	= false;
		var g_searchResults = false;
		var g_searchInitiated = false;

		// URL for different frames
		var g_toolbarFrameURL  = "";
		var g_reportFrameURL   = "";
		var g_searchFrameURL   = "";
		var g_tocFrameURL      = "";
		var g_searchTBFrameURL = "";

		// Global flags that synchronize among frames
		var g_inSearchRequestForm = false;
		var g_bReportFrameLoaded  = false;
		var g_bToolBarFrameLoaded = false;
		var g_bToolbarPageNumberSet = false;
		var g_bSearchFrameLoaded  = false;
		var g_bTocFrameLoaded  = false;

		// Global values used for the search operations
		var g_amountSearchHitsPerPage = 20;

		// The window name used in storing the location store object
		var g_windowName;

		// No special characters in location store object name.
		var escapedUserName = encode(g_userName);
		escapedUserName = escapedUserName.replace(/[^\w]/g, "pt");

		// For MSIE, use a fixed name for the cookie. Storing the unique cookie name
		// (see NS specific code below) in the parent.name will run into problems with
		// embedded frameset architecture. Each browser window open will have its own
		// cookie alive for the duration of the browser session.

		if (!isNav4)
		{
			g_windowName = escapedUserName + "_LocationStore";
		}

		// For Netscape since browser windows open at the same time overwrite cookies
		// with the same name, we try to find a unique name for the cookie, and store
		// it as the window name.

		if (isNav4)
		{
			g_windowName = parent.name;

			if( g_windowName == null || g_windowName == "" || (g_windowName.indexOf(escapedUserName + "_LocationStore_") == -1))
			{

				var currentDate = new Date();
				var unique_num = (Math.abs(currentDate.getTime()));
				g_windowName = escapedUserName + "_LocationStore_" + unique_num.toString();
				parent.name = g_windowName;
			}
		}

		// The location store object
		var g_locationStoreObject = new LocationStore(g_windowName);

		// The action that resulted in this frameset loading
		var g_loadAction = "UNKNOWN";

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Location store - get the action that caused this frameset to load. If it was an external
//search hyperlink, then follow the page number set by the web agent via the template variable.
//Otherwise, find out what page was last viewed in that report for the current browser session.

		if (<%= ActuUtil.jsBooleanEncode(AcParams.Exists(Page, AcParams.Name.page)) %>)
		{
			if ((g_curURL.indexOf("?Search") == -1) && (g_commandName != "searchReport"))
			{
				if (g_locationStoreObject.getAction() != "EHL_CLICK")
				{
					var index = g_locationStoreObject.findAssociation(g_objectURL);
					if (index != -1)
					{
						g_currentPageNumber = g_locationStoreObject.getAssociationPage(index);
					}
				}
			}
		}

		// IF 'page' IS NOT SPECIFIED, HARDCODE 'page' TO '1'
		if (!g_currentPageNumber)
		{
			g_currentPageNumber = "1";
		}

		////////////////////////////////////////////////////////////////////////////////////////
		//Scaling factor stuff
		////////////////////////////////////////////////////////////////////////////////////////

		//ScalingFactor - Read from the URL first. If it exists, read it
		//from there and set the cookie value, and pass it on along with
		//the ?ViewPage URL. If it doesn't exist in the URL, use the one
		//stored in the cookie.

		var scalingFactorValue = "";
		var writeScalingFactorToCookie = false;

		if ( location.href.lastIndexOf( "scalingFactor=" ) != -1 )
		{
			<% if(arrScalingFactor != null && arrScalingFactor.Length > 0){ %>
				scalingFactorValue = "<%= arrScalingFactor[arrScalingFactor.Length-1] %>";
				writeScalingFactorToCookie = true;
			<% }else{ %>
				scalingFactorValue = getValueFromUrl("scalingFactor=", location.href);
			<% } %>
			
		}
		else
		{
			scalingFactorValue = readCookie("scalingFactor");
		}

		if ( scalingFactorValue == "" )
		{
			scalingFactorValue = "100";
			writeScalingFactorToCookie = true;
		}

		if ( writeScalingFactorToCookie )
		{
			var nextYear = new Date();
			nextYear.setFullYear(nextYear.getFullYear() + 1);
			var expirationDate = nextYear.toGMTString();
			writeCookie("scalingFactor", scalingFactorValue, expirationDate, "/");
		}


		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Building the frameset
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		g_reportFrameURL = "viewpage.aspx" +  replaceDuplicateParams(replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "format", "DHTML") , "page" , g_currentPageNumber ) , "scalingfactor" , scalingFactorValue );
		g_reportFrameURL = removeParams(g_reportFrameURL, "range");

		g_toolbarFrameURL = "viewnavigation.aspx" +  replaceDuplicateParams(g_objectURL, "page", g_currentPageNumber);

		g_tocFrameURL = "viewtoc.aspx" + replaceDuplicateParams(replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "format", "XMLDisplay" ) , "componentid", "0" ) , "depth", "1" );

		g_searchFrameURL = "requestsearch.aspx" + g_objectURL + "&locale=<%=HttpUtility.UrlEncode((String)Session["locale"])%>" ;

		g_searchTBFrameURL = "searchtoolbar.aspx?locale=<%=HttpUtility.UrlEncode((String)Session["locale"])%>";

		// Frameset structure
		<%--  Boolean.valueOf Returns a Boolean with a value represented by the specified String.
			  The Boolean returned represents the value true if the string
			  argument is not null and is equal, ignoring case, to the string "true". --%>
		<% if(arrShowTOC != null && arrShowTOC.Length > 0){ %>
			tocOpen = <%= Boolean.Parse(arrShowTOC[arrShowTOC.Length-1]) %>;
		<% }else{ %>
			tocOpen      = (getValueFromUrl("&ShowTOC=",    location.href) == "TRUE") ? true : false;
		<% } %>
		<% if(arrShowSearch != null && arrShowSearch.Length > 0){ %>
			g_searchOpen = <%= Boolean.Parse(arrShowSearch[arrShowSearch.Length-1]) %>;
		<% }else{ %>
			g_searchOpen = (getValueFromUrl("&ShowSearch=", location.href) == "TRUE") ? true : false;
		<% } %>


		str += "<HTML>";

		// For Netscape
		if ( isNav4 )
		{

			//toolbar size needs to be increased for Netscape on SunOS
			if ( g_platform.indexOf("SunOS") == -1 )
			{
				str += "<FRAMESET NAME='mainframeset' ROWS='34, *' >";
			}
			else
			{
				str += "<FRAMESET NAME='mainframeset' ROWS='50, *' >";
			}

			str += "<FRAME NAME='toolbarframe' SRC='" + g_toolbarFrameURL + "' SCROLLING=no NORESIZE >";

			if ( tocOpen )
			{
				str += "<FRAMESET NAME='reportframeset' COLS='245,*' >";
					str += "<FRAME NAME='tocframe' SRC='blanknav.html' SCROLLING=auto>";
					str += "<FRAME NAME='reportframe' SRC='" + g_reportFrameURL + "' >";
				str += "</FRAMESET>";
			}
			else if ( g_searchOpen )
			{
				str += "<FRAMESET NAME='reportframeset' COLS='350,*'>";
		    		str += "<FRAMESET NAME='searchtoolbarframeset' ROWS='34,*' BORDER=0 FRAMEBORDER='no'>";
			    		str += "<FRAME NAME='searchtoolbar' SRC='blanknav.html' SCROLLING=no BORDER=0 FRAMEBORDER='no'>";
						str += "<FRAME NAME='searchframe' SRC='blanknav.html' SCROLLING=auto BORDER=0 FRAMEBORDER='no'>";
					str += "</FRAMESET>";
					str += "<FRAME NAME='reportframe' SRC='" + g_reportFrameURL + "' >";
				str += "</FRAMESET>";
			}
			else
			{
	    		str += "<FRAME NAME='reportframe' SRC='" + g_reportFrameURL + "' >";
			}
    		str += "</FRAMESET>";
		}
		else
		{
			if (g_browserType.cannotDynamicallyAdjustFrameset)
			{
				var dummyPage = "SRC='blanknav.html'";
				if (g_browserType.replaceFrameBreaksHistoryList)
					dummyPage = "";

				str += "<FRAMESET NAME='mainframeset' ROWS='28, *' >";

					str += "<FRAME NAME='toolbarframe' SRC='" + g_toolbarFrameURL + "' SCROLLING=no NORESIZE >";

				if ( tocOpen )
				{
					str += "<FRAMESET NAME='reportframeset' COLS='245,*' >";
						str += "<FRAME NAME='tocframe' " + dummyPage + " SCROLLING=auto>";
						str += "<FRAME NAME='reportframe' " + dummyPage + ">";
					str += "</FRAMESET>";
				}
				else if ( g_searchOpen )
				{
					str += "<FRAMESET NAME='reportframeset' COLS='350,*'>";
			    		str += "<FRAMESET NAME='searchtoolbarframeset' ROWS='34,*' BORDER=0 FRAMEBORDER='no'>";
				    		str += "<FRAME NAME='searchtoolbar' " + dummyPage + " SCROLLING=no BORDER=0 FRAMEBORDER='no'>";
							str += "<FRAME NAME='searchframe' " + dummyPage + " SCROLLING=auto BORDER=0 FRAMEBORDER='no'>";
						str += "</FRAMESET>";
						str += "<FRAME NAME='reportframe' " + dummyPage + ">";
					str += "</FRAMESET>";
				}
				else
				{
		    		str += "<FRAME NAME='reportframe' " + dummyPage + ">";
				}
				str += "</FRAMESET>";
			}

			else
			{
				// Need IDs for the framesets and the frames, so that scripts running MSIE on MacOS can refer to the different frames.
				// For MSIE on Windows the NAME is good enough.
				str += "<FRAMESET NAME='mainframeset' ID='mainframeset' ROWS='28, *'>";
					str += "<FRAME NAME='toolbarframe' ID='toolbarframe' SRC='" + g_toolbarFrameURL + "' SCROLLING=no NORESIZE >";

				if (tocOpen && g_searchOpen)
				{
					str += "<FRAMESET NAME='reportframeset' ID='reportframeset' COLS='245,*'>";
				}
				else if (g_searchOpen)
				{
					str += "<FRAMESET NAME='reportframeset' ID='reportframeset' COLS='350,*'>";
				}
				else
				{
					str += "<FRAMESET NAME='reportframeset' ID='reportframeset' COLS='"+(tocOpen ? "245,*" : "0,*")+"'>";
				}
						str += "<FRAMESET NAME='tocAndSearch' ID='tocAndSearch' ROWS='"+(tocOpen ? "*,0" : "0,*")+"' BORDER=0 FRAMEBORDER='no'>";
							str += "<FRAME NAME='tocframe' ID='tocframe' SRC='"+(tocOpen ? g_tocFrameURL : "blanknav.html")+"' SCROLLING=auto BORDER=0 FRAMEBORDER='no'>";
							str += "<FRAMESET NAME='searchtoolbarframeset' ID='searchtoolbarframeset' ROWS='28,*' BORDER=0 FRAMEBORDER='no'>";
					    		str += "<FRAME NAME='searchtoolbar' ID='searchtoolbar' SRC='"+(g_searchOpen ? g_searchTBFrameURL: "blanknav.html")+"' SCROLLING=no BORDER=0 FRAMEBORDER='no'>";
								str += "<FRAME NAME='searchframe' ID='searchframe' SRC='"+(g_searchOpen ? g_searchFrameURL : "blanknav.html")+"' SCROLLING=auto BORDER=0 FRAMEBORDER='no' NORESIZE>";
							str += "</FRAMESET>";
						str += "</FRAMESET>";
					str += "<FRAME NAME='reportframe' ID='reportframe' SRC='blanknav.html'>";
				
					str += "</FRAMESET>";

			}
		}

		str += "</HTML>";

		document.open();
		document.write( str );
		document.close();
		focus();

	</SCRIPT>
</HTML>
