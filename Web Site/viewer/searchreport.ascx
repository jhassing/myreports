<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.viewing.viewer.searchreport" CodeFile="searchreport.ascx.cs" %>
<%-- Import name spaces --%>
<%@ Import namespace="activeportal.classes" %>
<%@ Import namespace="actuate_i18n" %>
<html>
	<head>
		<%-- SET THE PAGE ENCODING --%>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Expires" content="-1">
		<title>
			<%= ActuUtil.htmlEncodeBreakOnSpace(sTitle) %>
		</title>
		<script language="javascript" src='../js/encoder.js'></script>
		<script language="javascript" src='../js/viewframeset.js'></script>
		<script language="javascript" src='../js/cookie.js'></script>
		<script language="javascript" src='../js/viewframesetfuncs.js'></script>
		<script language="javascript" src='../js/browsertype.js'></script>
		<script language="javascript" src='../js/viewer.js'></script>
	</head>
	<script>
		var str = "";
		var title = "<%= sTitle %>";
		var tocOpen = false;

		var sConnectionHandle = (<%= ActuUtil.jsBooleanEncode(bExistConnHandle) %>) ? "<%= HttpUtility.UrlEncode(ActuUtil.jsEncode(AcParams.Get(Page, AcParams.Name.connectionhandle))) %>" : null;
		var sSearchable = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TXT_SEARCHABLE", Session)) %>";
		var sRepField = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TXT_REP_FIELD", Session)) %>";
		var sRepValue = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TXT_REP_VALUE", Session)) %>";
		var sRepDisplay = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TXT_REP_DISPLAY", Session)) %>";
		var sTTRepField = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TXT_REP_FIELD", Session)) %>";
		var sTTRepValue = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TXT_REP_VALUE", Session)) %>";
		var sTTRepDisplay = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("TXT_REP_DISPLAY", Session)) %>";

		var g_objectURL = "<%= ActuUtil.htmlEncode(sFwdURL) %>";
		var g_currentPageNumber = "<%= sPage %>";
		var g_userName = "<%= ActuUtil.jsEncode(sLoginID) %>";
		var g_platform = navigator.platform;
		var g_browserVersion = parseFloat(window.navigator.appVersion);
		var g_widthCheck = window.innerWidth;
		var g_heightCheck = window.innerHeight;
		var g_curURL = window.document.location.href;

		var g_stopRefresh = true;
		var g_clickActionRefresh = true;

		var isNav4 = (document.layers) ? true : false;

		// The resize handler should be set only for Netscape on MacOS.
		// This because when the browser is resized the event is only caught here, not in the
		// toolbar or in the report frame.
		if (isNav4 && g_platform.indexOf("Mac") != -1)
		{
			window.onResize = resizeFix;
		}

		if (isNav4)
		{
			// Fix to allow Navigation within a parent frame in Netscape 4
			if (parent.setActuateMainFrameURL)
				parent.setActuateMainFrameURL(window.document.location.pathname);

			// ViewFrameset&CheckPermission is forced not to be cached,
			// so the resize problem would occur for this particular URL.
			// We need to replace the window with a URL without CheckPermission.
			var pos = g_curURL.lastIndexOf("CheckPermission");
			if (pos != -1)
			{
				var newURL = g_curURL.replace(/&CheckPermission/i, "");
				window.location.replace( newURL );
			}
		}

		var g_inViewFrameset = true; // Used by methods in different frames to check if they are in the view frameset.
		var g_commandName = "searchReport";
		var g_beforeHistoryLength = window.history.length;
		var g_searchOpen	= false;
		var g_searchResults = false;
		var g_searchInitiated = false;

		// URL for different frames
		var g_toolbarFrameURL  = "";
		var g_reportFrameURL   = "";
		var g_searchFrameURL   = "";
		var g_tocFrameURL      = "";
		var g_searchTBFrameURL = "";

		// Global flags that synchronize among frames
		var g_inSearchRequestForm = false;
		var g_bReportFrameLoaded  = false;
		var g_bToolBarFrameLoaded = false;
		var g_bToolbarPageNumberSet = false;
		var g_bSearchFrameLoaded  = false;
		var g_bTocFrameLoaded  = false;

		// Global values used for the search operations
		var g_amountSearchHitsPerPage = "<%= iNumberOfHitsOnPage %>";

		// The window name used in storing the location store object
		var g_windowName;

		// No special characters in location store object name.
		var escapedUserName = encode(g_userName);
		escapedUserName = escapedUserName.replace(/[^\w]/g, "pt");

		// For MSIE, use a fixed name for the cookie. Storing the unique cookie name
		// (see NS specific code below) in the parent.name will run into problems with
		// embedded frameset architecture. Each browser window open will have its own
		// cookie alive for the duration of the browser session.

		if (!isNav4)
		{
			g_windowName = escapedUserName + "_LocationStore";
		}

		// For Netscape since browser windows open at the same time overwrite cookies
		// with the same name, we try to find a unique name for the cookie, and store
		// it as the window name.

		else
		{
			g_windowName = parent.name;

			if (g_windowName == null || g_windowName == "" || (g_windowName.indexOf(escapedUserName + "_LocationStore_") == -1))
			{

				var currentDate = new Date();
				var unique_num = (Math.abs(currentDate.getTime()));
				g_windowName = escapedUserName + "_LocationStore_" + unique_num.toString();
				parent.name = g_windowName;
			}
		}

		// The location store object
		var g_locationStoreObject = new LocationStore(g_windowName);

		// The action that resulted in this frameset loading
		var g_loadAction = "UNKNOWN";

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Location store - get the action that caused this frameset to load. If it was an external
		// search hyperlink, then follow the page number set by the web agent via the template variable.
		// Otherwise, find out what page was last viewed in that report for the current browser session.
		if (location.href.lastIndexOf("&page=") == -1)
		{
			if ((g_curURL.indexOf("?Search") == -1) && (g_commandName != "searchReport"))
			{
				if (g_locationStoreObject.getAction() != "EHL_CLICK")
				{
					var index = g_locationStoreObject.findAssociation(g_objectURL);
					if (index != -1)
					{
						g_currentPageNumber = g_locationStoreObject.getAssociationPage(index);
					}
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////////////////
		// Scaling factor stuff
		////////////////////////////////////////////////////////////////////////////////////////
		// ScalingFactor - Read from the URL first. If it exists, read it
		// from there and set the cookie value, and pass it on along with
		// the ?ViewPage URL. If it doesn't exist in the URL, use the one
		// stored in the cookie.
		var scalingFactorValue = "";
		var writeScalingFactorToCookie = false;
		if (location.href.lastIndexOf("&scalingFactor=") != -1)
		{
			scalingFactorValue = getValueFromUrl("&scalingFactor=", location.href);
			writeScalingFactorToCookie = true;
		}
		else
		{
			scalingFactorValue = readCookie("scalingFactor");
		}

		if (scalingFactorValue == "")
		{
			scalingFactorValue = "100";
			writeScalingFactorToCookie = true;
		}

		if (writeScalingFactorToCookie)
		{
			var nextYear = new Date();
			nextYear.setFullYear(nextYear.getFullYear() + 1);
			var expirationDate = nextYear.toGMTString();
			writeCookie("scalingFactor", scalingFactorValue, expirationDate, "/");
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Building the frameset
		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		g_reportFrameURL = "viewpage.aspx" +  replaceDuplicateParams(replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "format", "DHTML") , "page" , g_currentPageNumber ) , "scalingfactor" , scalingFactorValue );

		g_toolbarFrameURL = "viewnavigation.aspx" +  replaceDuplicateParams(g_objectURL, "page", g_currentPageNumber);

		g_tocFrameURL = "viewtoc.aspx" + replaceDuplicateParams(replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "format", "XMLDisplay" ) , "componentid", "0" ) , "depth", "1" );

		g_searchFrameURL = "searchframe.aspx" + g_objectURL ;
		g_searchTBFrameURL = "searchtoolbar.aspx?serverURL=<%= HttpUtility.UrlEncode(sServerURL) %>&volume=<%= HttpUtility.UrlEncode(sVolume) %>";

		// Frameset structure
		tocOpen = (getValueFromUrl("&ShowTOC=", location.href) == "TRUE") ? true : false;
		g_searchOpen = true;

		str += "<html>";

		// For Netscape
		if (isNav4)
		{

			// Toolbar size needs to be increased for Netscape on SunOS
			if (g_platform.indexOf("SunOS") == -1)
			{
				str += "<frameset name='mainframeset' rows='34, *' >";
			}
			else
			{
				str += "<frameset name='mainframeset' rows='50, *' >";
			}

			str += "<frame name='toolbarframe' src='" + g_toolbarFrameURL + "' scrolling=no NORESIZE >";

			if (tocOpen)
			{
				str += "<frameset name='reportframeset' cols='245,*' >";
					str += "<frame name='tocframe' src='blanknav.html' scrolling=auto>";
					str += "<frame name='reportframe' src='" + g_reportFrameURL + "' >";
				str += "</frameset>";
			}
			else if (g_searchOpen)
			{
				str += "<frameset name='reportframeset' cols='350,*'>";
		    		str += "<frameset name='searchtoolbarframeset' rows='34,*' border=0 frameborder='no'>";
			    		str += "<frame name='searchtoolbar' src='blanknav.html' scrolling=no border=0 frameborder='no'>";
						str += "<frame name='searchframe' src='blanknav.html' scrolling=auto border=0 frameborder='no'>";
					str += "</frameset>";
					str += "<frame name='reportframe' src='" + g_reportFrameURL + "' >";
				str += "</frameset>";
			}
			else
			{
	    		str += "<frame name='reportframe' src='" + g_reportFrameURL + "' >";
			}
    		str += "</frameset>";
		}
		else
		{
			if (g_browserType.cannotDynamicallyAdjustFrameset)
			{
				var dummyPage = "src='blanknav.html'";
				if (g_browserType.replaceFrameBreaksHistoryList)
					dummyPage = "";

				str += "<frameset name='mainframeset' rows='28, *' >";
					str += "<frame name='toolbarframe' src='" + g_toolbarFrameURL + "' scrolling=no noresize>";

				if (tocOpen)
				{
					str += "<frameset name='reportframeset' cols='245,*' >";
						str += "<frame name='tocframe' " + dummyPage + " scrolling=auto>";
						str += "<frame name='reportframe' " + dummyPage + ">";
					str += "</frameset>";
				}
				else if (g_searchOpen)
				{
					str += "<frameset name='reportframeset' cols='350,*'>";
			    		str += "<frameset name='searchtoolbarframeset' rows='34,*' border=0 frameborder='no'>";
				    		str += "<frame name='searchtoolbar' " + dummyPage + " scrolling=no border=0 frameborder='no'>";
							str += "<frame name='searchframe' " + dummyPage + " scrolling=auto border=0 frameborder='no'>";
						str += "</frameset>";
						str += "<frame name='reportframe' " + dummyPage + ">";
					str += "</frameset>";
				}
				else
				{
		    		str += "<frame name='reportframe' " + dummyPage + ">";
				}
				str += "</frameset>";
			}

			else
			{
				// Need IDs for the framesets and the frames, so that scripts running MSIE on MacOS can refer to the different frames.
				// For MSIE on Windows the NAME is good enough.
				str += "<frameset name='mainframeset' ID='mainframeset' rows='28, *'>";
					str += "<frame name='toolbarframe' ID='toolbarframe' src='" + g_toolbarFrameURL + "' scrolling=no NORESIZE >";

				if (g_searchOpen)
				{
					str += "<frameset name='reportframeset' ID='reportframeset' cols='350,*'>";
				}
				else
				{
					str += "<frameset name='reportframeset' ID='reportframeset' cols='0,*'>";
				}
						str += "<frameset name='tocAndSearch' ID='tocAndSearch' rows='0,*' border=0 frameborder='no'>";
							str += "<frame name='tocframe' ID='tocframe' src='blanknav.html' scrolling=auto border=0 frameborder='no'>";
				    		str += "<frameset name='searchtoolbarframeset' ID='searchtoolbarframeset' rows='28,*' border=0 frameborder='no'>";
					    		str += "<frame name='searchtoolbar' ID='searchtoolbar' src='blanknav.html' scrolling=no border=0 frameborder='no'>";
								str += "<frame name='searchframe' ID='searchframe' src='blanknav.html' scrolling=auto border=0 frameborder='no'>";
							str += "</frameset>";
						str += "</frameset>";
						str += "<frame name='reportframe' ID='reportframe' src='" + g_reportFrameURL + "'>";
					str += "</frameset>";

			}
		}
		str += "</html>";

		document.open();
		document.write(str);
		document.close();
	</script>
</html>
