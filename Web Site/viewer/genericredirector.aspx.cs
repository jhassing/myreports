using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using activeportal.classes;
using activeportal.classes.api.proxy;
using activeportal.classes.api;

namespace activeportal.usercontrols.viewing.viewer.pages
{
	/// <summary>
	/// Summary description for genericredirector.
	/// </summary>
	public partial class genericredirector : System.Web.UI.Page
	{
		static private Hashtable commandList;

		static genericredirector()
		{
			commandList = new Hashtable();

			commandList.Add("submit", "/newrequest/do_executereport.aspx");
			commandList.Add("request", "/newrequest/index.aspx");

			commandList.Add("viewpage","/viewer/viewpage.aspx");
			commandList.Add("viewframeset", "/viewer/viewframeset.aspx");
			commandList.Add("viewdefault", "/viewer/viewdefault.aspx");
			commandList.Add("viewnavigation", "/viewer/viewnavigation.aspx");
			commandList.Add("viewtoc", "/viewer/viewtoc.aspx");
			commandList.Add("searchreport", "/viewer/searchreport.aspx");
			commandList.Add("getstylesheet", "/viewer/viewembeddedobject.aspx?operation=GetStyleSheet");
			commandList.Add("getstaticdata", "/viewer/viewembeddedobject.aspx?operation=GetStaticData");
			commandList.Add("getdynamicdata", "/viewer/viewembeddedobject.aspx?operation=GetDynamicData");
			commandList.Add("getembeddeddata", "/viewer/viewembeddedobject.aspx");

			commandList.Add("getreportdata", "/viewer/getreportdata.aspx");
			commandList.Add("saveas", "/viewer/downloadfile.aspx");
			commandList.Add("filefolderlist","/filesfolders/index.aspx");

			commandList.Add("createquery", "/query/create.aspx");
			commandList.Add("submitquery", "/query/submit.aspx");
			commandList.Add("executequery", "/query/execute.aspx");
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			String commandNm = AcParams.Get(Page, "command");
			String redirectPath = Request.ApplicationPath;
			
			String sFwdURL = AcParams.createQueryString(Page);
			
			if (commandNm != null)
			{
				commandNm = AcString.ToLower(commandNm.Trim());
				if ( commandNm == "saveas" )
				{
					String fileFolderPath  = AcParams.Get( Page, AcParams.Name.name, AcParams.From.Url );
					AcGetFileDetails getFileDetails = new AcGetFileDetails( fileFolderPath, null );
										
					getFileDetails.Execute( Page );
					GetFileDetailsResponse fileDetail = getFileDetails.Response;

					if ( fileDetail != null )
					{
						if ( String.Compare( fileDetail.File.FileType, "directory", true ) == 0 )	
						{
							commandNm="filefolderlist";
							sFwdURL = "?folder=" + HttpUtility.UrlEncode( fileFolderPath );
						}
					}
				}
				redirectPath += (String) commandList[commandNm];
			}
			else
			{
				redirectPath += (String) commandList["viewpage"];
			}

			

			if (sFwdURL != null && sFwdURL.Length > 0)
			{
				redirectPath += sFwdURL;	
			}
			
			Response.Redirect(redirectPath);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion
	}
}
