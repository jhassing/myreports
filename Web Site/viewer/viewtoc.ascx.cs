/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	4.0
	$Revision:: $
*/

namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Xml;

	using activeportal.classes;
	using activeportal.classes.api;

	public partial  class viewtoc : System.Web.UI.UserControl
	{
		protected String m_id;
		protected String m_name;
		protected String m_format;
		protected String m_componentId;
		protected String m_connectionHandle;
		protected String m_depth;
		protected String m_embsrvrequester;
		protected String sFwdURL;

		private AcTocNodeList m_tocNodeList;
		public AcTocNodeList TOC
		{
			get { return  m_tocNodeList; }
		}


		protected void Page_Load(object sender, System.EventArgs e)
		{
			m_id = AcParams.Get(Page, AcParams.Name.id);
			m_name = AcParams.Get(Page, AcParams.Name.name);
			String m_versionId = ActuUtil.getVersionFromFilename(m_name);
			m_name = ActuUtil.stripVersionNumber(m_name);
			String m_type = ActuUtil.getFileExtension(m_name);

			m_componentId = AcParams.Get(Page, AcParams.Name.componentid);
			if (m_componentId == null)
			{
				m_componentId = "0";
			}

			m_connectionHandle = AcParams.Get(Page, AcParams.Name.connectionhandle);
			m_depth = AcParams.Get(Page, AcParams.Name.depth);
			m_format = AcParams.Get(Page, AcParams.Name.format, "XMLDisplay");
			m_embsrvrequester = AcParams.Get(Page, AcParams.Name.embsrvrequester);

			try
			{
				Convert.ToInt16(m_depth);
			}
			catch (Exception)
			{
				m_depth = "1";
			}

			sFwdURL = AcParams.createQueryString(Page, true);
			if (m_embsrvrequester != null)
			{
				sFwdURL += "&embsrvrequester=" + m_embsrvrequester;
			}

			AcGetTOC getTOC = new AcGetTOC( m_id, m_name, m_type, m_versionId, m_format, m_depth, m_componentId, Request.UserAgent );
			getTOC.Execute( Page, m_connectionHandle );
			m_tocNodeList = getTOC.TOC;
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
