/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using activeportal.classes;
	using actuate_i18n;

	/// <summary>
	///		Summary description for viewframeset.
	/// </summary>
	public partial  class viewframeset : System.Web.UI.UserControl
	{

		//	Variables used in the Asp pages
		protected String[] arrScalingFactor;
		protected String[] arrShowTOC;
		protected String[] arrShowSearch;

		protected String sTitle;
		protected String sPage;
		protected String sFwdURL;
		protected bool bExistConnHandle;

		protected String sServerURL;
		protected String sVolume;
		protected String sLoginID;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			arrScalingFactor = AcParams.GetArray(Page, AcParams.Name.scalingFactor);
			arrShowTOC = AcParams.GetArray(Page, AcParams.Name.ShowTOC);
			arrShowSearch = AcParams.GetArray(Page, AcParams.Name.ShowSearch);

			bExistConnHandle = AcParams.Exists(Page, AcParams.Name.connectionhandle);

			String sTitleDocName = ActuUtil.GetDocNameForViewerTitle(bExistConnHandle, Page);
			sTitle = String.Format( AcResourceManager.GetString("MSGT_BROWSER", Session), sTitleDocName);

			sPage = AcParams.Get(Page, AcParams.Name.page);
			if (sPage != null)
			{
				try
				{
					sPage = sPage.Trim();
					Convert.ToInt16(sPage);
				}
				catch (Exception)
				{
					sPage = null;
				}
			}

			sServerURL = AcParams.Get(Page, AcParams.Name.serverURL);
			sVolume = AcParams.Get(Page, AcParams.Name.volume);
			sLoginID = AcParams.Get(Page, AcParams.Name.userID);

			sFwdURL = AcParams.createQueryString(Page, true);
			if (sFwdURL == null || sFwdURL.Trim().Length == 0)
			{
				sFwdURL = "?";
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
