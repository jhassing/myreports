/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Collections;

	using actuate_i18n;
	using activeportal.classes;

	public partial  class searchreport : System.Web.UI.UserControl
	{
		protected String sServerURL;
		protected String sVolume;
		protected String sLoginID;
		protected String sAuthID;
		protected String acLocale;

		protected String sFormat;
		protected String sHits;
		protected int iNumberOfHitsOnPage = 0;
		protected String sTitle = null;
		protected bool bExistConnHandle;
		protected String sPage;
		protected String sFwdURL;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			sServerURL = AcParams.Get(Page, AcParams.Name.serverURL);
			sVolume = AcParams.Get(Page, AcParams.Name.volume);
			sLoginID = AcParams.Get(Page, AcParams.Name.userID);
			sAuthID = AcParams.Get(Page, AcParams.Name.password);
			acLocale = AcParams.Get(Page, AcParams.Name.locale);

			sFormat = AcParams.Get(Page, AcParams.Name.format);

			if (sFormat != null && (AcString.ToLower(sFormat) == "csv"
								|| AcString.ToLower(sFormat) == "tsv"
								|| AcString.ToLower(sFormat) =="excel"
								|| AcString.ToLower(sFormat) == "analysis"
								|| AcString.ToLower(sFormat) == "xmldisplay"))
			{
				Response.Redirect(Request.ApplicationPath + "/viewer/downloadsearchresult.aspx" + AcParams.createQueryString(Page, true));
				return;
			}

			// PROCESS PARAMETER: NUMBER OF HITS IN SEARCH RESULTS
			sHits = AcParams.Get(Page, AcParams.Name.hits);
			if (sHits == null || sHits.Trim().Length <= 0)
			{
				iNumberOfHitsOnPage = 20;
			}
			else
			{
				try
				{
					iNumberOfHitsOnPage = Convert.ToInt32(sHits);
				}
				catch
				{
					iNumberOfHitsOnPage = 20;
				}
			}

			// PROCESS PARAMETER(S): TITLE, CONNECTION HANDLE
			bExistConnHandle = AcParams.Exists(Page, AcParams.Name.connectionhandle);
			String sTitleDocName = ActuUtil.GetDocNameForViewerTitle(bExistConnHandle, Page);
			sTitle = String.Format( AcResourceManager.GetString("MSGT_BROWSER", Session), sTitleDocName);

			// PROCESS PARAMETER: PAGE
			sPage = AcParams.Get(Page, AcParams.Name.page);
			if (sPage == null)
			{
				sPage = "1";
			}

			try
			{
				sPage = sPage.Trim();
				Convert.ToInt32(sPage);
			}
			catch
			{
				sPage = "1";
			}

			sFwdURL = AcParams.createQueryString(Page, true);
			if (sFwdURL == null || sFwdURL.Trim().Length == 0)
			{
				sFwdURL = "?";
			}

			bool bFrameset = true;
			if (AcParams.Exists(Page, AcParams.Name.frameset))
			{
				bFrameset = AcParams.urlCheckboxState(Page, AcParams.Name.frameset);
			}

			if (!bFrameset)
			{
				Response.Redirect(Request.ApplicationPath + "/viewer/searchframe.aspx" + sFwdURL);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
