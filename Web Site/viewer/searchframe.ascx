<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.viewing.viewer.searchframe" CodeFile="searchframe.ascx.cs" %>
<%-- Import name spaces --%>
<%@ Import namespace="System.Collections" %>
<%@ Import namespace="System.Web" %>
<%@ Import namespace="activeportal.classes" %>
<%@ Import namespace="activeportal.classes.api" %>
<%@ Import namespace="actuate_i18n" %>
<%@ Register TagPrefix="template" namespace="actuate.classes.webcontrols.masterpages" assembly="activeportal" %>
<%-- Gather authentication credentials at the top of all level 0 pages --%>

<%	
	if (!bSelectAbsent)
	{
%>
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<meta http-equiv="Pragma" content="no-cache">
				<meta http-equiv="Cache-Control" content="no-cache">
				<meta http-equiv="Expires" content="-1">

				<link rel="stylesheet" type="text/css" href="../css/allstyles.css">

				<script language="javascript" src='../js/encoder.js'></script>
				<script language="javascript" src='../js/search.js'></script>
				<script language="javascript" src='../js/array.js'></script>
				<script language="javascript" src='../js/layer.js'></script>
				<script language="javascript" src='../js/cookie.js'></script>
				<script language="javascript" src='../js/browsertype.js'></script>
				
				<script>
					var js_ObjectURL = "<%= ActuUtil.htmlEncode(sFwdURL) %>";
					var hiddenFormStr ="";
				</script>
				
				<script language="javascript">
					ns4 = (document.layers)? true:false;

					var sServerURL = "<%= ActuUtil.jsEncode(sServerURL) %>";
					var sVolume = "<%= ActuUtil.jsEncode(sVolume) %>";
					var complParams = "serverURL="+encode(sServerURL)+"&volume="+encode(sVolume);

					var g_documentString	= "";
					var g_startHitNumber	= <%= iStartHitNumber %>;
					var g_endHitNumber		= g_startHitNumber + <%= iTotalNumberOfHits %>;
					var g_totalNumberOfHits = <%= iEndHitNumber %>;
					var g_searchTipURL      = "../help/" + "wwhelp.htm?single=true&context=jsp_viewer&topic=ViewingFramework_search";

					// g_amountHitsPerPage is the number of search results which get display per page.
					// If the total number of matches exceeds this the user will need to use
					// the previous/next links on the results page to move through the results.
					// The value is set from viewframeset.jsp and can be changed if needed.

					var g_amountHitsPerPage = (eval('parent.g_amountSearchHitsPerPage'))
						? parent.g_amountSearchHitsPerPage
						: <%= iNumberOfHitsOnPage %>;

					var g_selectedDownloadFormat =  "CSV";
					var g_resultSetNavigationLine = "";
					var g_changeSearchButtonLine = "";
					var g_downloadAsButtonLine = ""
					var blankPage = "blanknav.html";

					var searchToolbarPage = "searchtoolbar.aspx?serverURL=<%= HttpUtility.UrlEncodeUnicode(sServerURL) %>&volume=<%= HttpUtility.UrlEncodeUnicode(sVolume) %>&locale=<%=HttpUtility.UrlEncodeUnicode(acLocale) %>";
					var inViewFrameset = (eval('parent.g_inViewFrameset') || parent.g_inViewFrameset == true) ? true : false
					
					g_MsgSearchNotRun = "<%= ActuUtil.jsEncode( AcResourceManager.GetString( "ERRMSG_SEARCH_NOT_RUN", Session )) %>" ;
					g_MsgNoSearchCriteria = "<%= ActuUtil.jsEncode(AcResourceManager.GetString( "ERRMSG_NO_SEARCH_CRITERIA_SPECIFIED", Session )) %>" ;
					g_MsgSpecifySearch = "<%= ActuUtil.jsEncode(AcResourceManager.GetString( "ERRMSG_SPECIFY_SEARCH_CRITERIA", Session ))  %>";

					var searchForm  = "hiddenSearchForm";
					<%
					if (CommandName.ToLower().Equals("submitsearch"))
					{
						%>
						var extractForm = "hiddenExtractResultsForm";
						<%
					}
					else if (CommandName.ToLower().Equals("searchreport"))
					{
						%>
						var extractForm = "hiddenExtractResultsForm";
						<%
					}
					else
					{
						%>
						<%
							parameterList.Clear();
							parameterList.Add(CommandName);
						%>
						alert("a" + "<%= ActuUtil.MessageFormat(AcResourceManager.GetString("TXT_UNEXPECTED_DIRECTIVE", Session), parameterList) %>");
						<%
					}
					%>

					var saveSearchForm = "savesearchform";
					var isAnalysisSupported = false;
					var showAnalysisButton = true;
					var theAnalysisWindow = null;
					var analysisFormat    = "ANALYSIS";
					var winName = "";

					/**
					 *
					 */
					function addToDocument(str)
					{
						g_documentString += str + "\n";
					}

					/**
					 *
					 */
					function writeDocument()
					{
						document.write(g_documentString);
					}

					/**
					 *
					 */
					function submitExtractForm(formPointer)
					{
						if (isAnalysisSupported && !showAnalysisButton && g_selectedDownloadFormat == analysisFormat)
						{
							submitAnalysisForm(formPointer);
							return;
						}
						else
						{
							<%
							if (CommandName.ToLower().Equals("submitsearch"))
							{
								%>
								formPointer.searchFormat.value = g_selectedDownloadFormat;
								<%
							}
							else if (CommandName.ToLower().Equals("searchreport"))
							{
								%>
								formPointer.format.value = g_selectedDownloadFormat;
								<%
							}
							else
							{
								%>
								<%
									parameterList.Clear();
									parameterList.Add(CommandName);
								%>
								alert("<%= ActuUtil.MessageFormat(AcResourceManager.GetString("TXT_UNEXPECTED_DIRECTIVE", Session), parameterList) %>");
								return;
								<%
							}
							%>

							var browser = navigator.userAgent;
							if( browser.lastIndexOf("IE 4") != -1 )
							{
								formPointer.target = "_blank";
							}
							formPointer.target = "_top";
							formPointer.submit();
							return;
						}
					}

					/**
					 *
					 */
					function submitAnalysisForm(formPointer)
					{
						<%
						if (CommandName.ToLower().Equals("submitsearch"))
						{
							%>
							formPointer.searchFormat.value = analysisFormat;
							<%
						}
						else if (CommandName.ToLower().Equals("searchreport"))
						{
							%>
							formPointer.format.value = analysisFormat;
							<%
						}
						else
						{
							%>
							<%
								parameterList.Clear();
								parameterList.Add(CommandName);
							%>
							alert("<%= ActuUtil.MessageFormat(AcResourceManager.GetString("TXT_UNEXPECTED_DIRECTIVE", Session), parameterList) %>");
							<%
						}
						%>

						var todayDate = new Date();
						var unique_num = (Math.abs(todayDate.getTime()));
						g_windowName = "ANALYSIS" + unique_num.toString();
						theAnalysisWindow = window.open( blankPage , g_windowName, "toolbar=no,resizable=yes,height=580,width=780, scrollbars=yes" );
						theAnalysisWindow.opener = this;
						theAnalysisWindow.focus();
						formPointer.target = g_windowName;
						formPointer.submit();
						return;
					}

					/**
					 *
					 */
					function gotoPreviousResults()
					{
						var newStartPoint = g_startHitNumber - g_amountHitsPerPage;
						if ( newStartPoint < 0 )
						{
							newStartPoint = 0;
						}

						if ( newStartPoint != g_startHitNumber )
							getChunkOfSearchResults(newStartPoint);
					}

					/**
					 *
					 */
					function gotoNextResults()
					{
						if (g_endHitNumber <= g_totalNumberOfHits) getChunkOfSearchResults(g_endHitNumber);
					}

					/**
					 *
					 */
					function gotoStartOfResults()
					{
						var newStartPoint = 0;
						if ( newStartPoint != g_startHitNumber ) getChunkOfSearchResults(newStartPoint);
					}

					/**
					 *
					 */
					function onLoad()
					{
						if (!inViewFrameset) return;
						parent.g_searchResults	= true;
						parent.g_searchInitiated = false;

						var reportFrame	= eval( 'parent.frames.reportframe' );
						parent.g_inSearchRequestForm = false;
						if (parent.frames.searchtoolbar && parent.frames.searchtoolbar.searchToolbarLoaded)
						{
							parent.frames.searchtoolbar.enableSearchToolbar();
						}
					}

					/**
					 *
					 */
					function onUnload()
					{
						if (!inViewFrameset)
							return;

						parent.g_searchOpen		= true;
						parent.g_searchResults	= false;
					}

					/**
					 *
					 */
					function handleLinkClick(objectUrl, componentID, connectionHandle, outputName)
					{
						if (!inViewFrameset) return;
						var scalingFactorValue = readCookie("scalingFactor");
						if (scalingFactorValue == "")
						{
							scalingFactorValue = "100";
						}

						var url = "viewpage.aspx?"+ complParams +"&"+ objectUrl + "&format=DHTML&componentid=" + componentID + "&scalingFactor=" + scalingFactorValue;
						if (connectionHandle != null) url += "&connectionHandle=" + connectionHandle;
						if (outputName != null) url += "&outputName=" + encode(outputName);
						var reportFrame	= eval('parent.frames.reportframe');
						var toolbar = eval('parent.frames.toolbarframe');
						if (!ns4) parent.frames.searchtoolbar.disableSearchToolbar();

						if (reportFrame)
						{
							if (toolbar && !ns4)
							{
								toolbar.disableToolBarButtons();
							}
							reportFrame.location.replace(url);
						}
					}

					/**
					 *
					 */
					function handleFormatChange(form)
					{
						g_selectedDownloadFormat = form.format.options[form.format.selectedIndex].value;
					}

					/**
					 *
					 */
					function showSearchRequestForm()
					{
						if (!inViewFrameset) return;
						parent.g_searchResults = false;
						var url = "requestsearch.aspx?"+ js_ObjectURL;
						location.replace(url);
						parent.frames.searchtoolbar.location.replace( searchToolbarPage );
					}

					/**
					 *	For save search
					 */
					function createSaveSearchForm(formName)
					{
						var str = "<form name='" + formName + "' method='post' action='savesearch.aspx'>";

						<%
						String[] searchParameters = Request.QueryString.AllKeys;
						for (int i=0; i < searchParameters.Length; i++)
						{
							String sParameterName = searchParameters[i];
							if (sParameterName.ToLower().Equals("format") ||
								sParameterName.ToLower().Equals("startingpoint") ||
								sParameterName.ToLower().Equals("hits"))
							{
								continue;
							}
							String sParameterValue = AcViewerParams.Get(Page, sParameterName);
							%>
							str += "<input TYPE='hidden' NAME='<%= ActuUtil.jsEncode(sParameterName) %>' VALUE='<%= ActuUtil.jsEncode(sParameterValue) %>'>";
							<%
						}
						%>
						<%
						searchParameters = Request.Form.AllKeys;
						for (int i=0; i < searchParameters.Length; i++)
						{
							String sParameterName = searchParameters[i];
							if (sParameterName.ToLower().Equals("format") ||
								sParameterName.ToLower().Equals("startingpoint") ||
								sParameterName.ToLower().Equals("hits"))
							{
								continue;
							}
							String sParameterValue = AcViewerParams.Get(Page, sParameterName);
							%>
							str += "<input TYPE='hidden' NAME='<%= ActuUtil.jsEncode(sParameterName) %>' VALUE='<%= ActuUtil.jsEncode(sParameterValue) %>'>";
							<%
						}
						%>
						
						str += "</form>";
						addToDocument(str);
					}

					/**
					 *	Show save smart search interface.
					 */
					function showSaveSearchForm(formPointer)
					{
						parent.frames.searchtoolbar.location.replace( searchToolbarPage );
						formPointer.submit();
					}

					/**
					 *
					 */
					function getChunkOfSearchResults(newStartPoint)
					{
						var formPointer = document.hiddenSearchForm;
						formPointer.startingpoint.value = newStartPoint;
						formPointer.hits.value = g_amountHitsPerPage;
						<%
						if (CommandName.ToLower().Equals("searchreport"))
						{
							%>
							formPointer.format.value = 'XMLDisplay';
							formPointer.target="";
							<%
						}
						%>
						formPointer.submit();
					}

					<%
					if (CommandName.ToLower().Equals("submitsearch"))
					{
						%>

						/**
						 *
						 */
						function createSearchForm(formName, bExtractForm)
						{
							var actionStr = "";//"$(ObjectURL)";
							var varFields;

							if (bExtractForm == true)
							{
								actionStr + ="downloadsearchresult.aspx";
								varFields = "<input TYPE='hidden' NAME='searchFormat' VALUE=''>";
							}
							else
							{
								actionStr += "?submitSearch" + parent.g_escapedObjectID;
								varFields  = "<input TYPE='hidden' NAME='startingpoint' VALUE=''>";
								varFields += "<input TYPE='hidden' NAME='amount' VALUE='" + g_amountHitsPerPage + "'>";
							}

							var str = "<form name='" + formName + "' ";

							if (bExtractForm && g_browserType.postedDataLostGettingCSV) {
								str += "method='get' ";
							}
							else {
								str += "method='post' ";
							}
							str		+= "action='" + actionStr + "'>";

							if (bExtractForm && g_browserType.postedDataLostGettingCSV) {
								str += "<input type='hidden' name='extractSearchResults' value=''>";
							}
							str     += varFields;

							var value;
							str += "<input type='hidden' name='" + $(ComponentID) + "' ";
							value = "controlname=$(ControlName)";
							value += "&searchvalue=$(SearchValue)";
							value += "&selected=$(Selected)";
							//if (bExtractForm && g_browserType.postedDataLostGettingCSV) value = encode(value);
							str += "value=\"" + value + "\">";
							str += "</form>";
							addToDocument(str);
						}
						<%
					}
					else if (CommandName.ToLower().Equals("searchreport"))
					{
						%>

						/**
						 *
						 */
						function createSearchReportForm(formName, bExtractForm)
						{
							var actionStr = "";
							if (bExtractForm)
							{
								actionStr += "downloadsearchresult.aspx";
							}
							else
							{
								actionStr += "searchframe.aspx";
							}

							var str = "<form name='" + formName + "' ";

							if (bExtractForm && g_browserType.postedDataLostGettingCSV)
							{
								str += "method='get' ";
							}
							else
							{
								str += "method='post' ";
							}
							if (bExtractForm)
							{
								str +="target='_top' ";
							}
							str += " action='" + actionStr + "'>";

							/******************************END *************************/
							if (bExtractForm)
							{
								str += "<input type='hidden' name='startingpoint' value='0'>";
								str += "<input TYPE='hidden' name='hits' value='" + g_totalNumberOfHits + "'>";
							}
							else
							{
								str += "<input type='hidden' name='startingpoint' value='<%= ActuUtil.jsEncode(AcViewerParams.Get(Page, "startingpoint")) %>'>";
								str += "<input TYPE='hidden' name='hits' value='<%= ActuUtil.jsEncode(AcViewerParams.Get(Page, "hits")) %>'>";
							}
							str += "<input TYPE='hidden' name='format' value='<%= ActuUtil.jsEncode(AcViewerParams.Get(Page, "format")) %>'>";
							<%
							String[] eParameters = Request.Form.AllKeys;
							for (int i=0; i < eParameters.Length; i++)
							{
								String sParameterName = eParameters[i];
								if (sParameterName.ToLower().Equals("format") || sParameterName.ToLower().Equals("startingpoint") || sParameterName.ToLower().Equals("hits"))
								{
									 continue;
								}
								String sParameterValue = AcViewerParams.Get(Page, sParameterName, AcParams.From.Post);
								if (sParameterValue == null)
								{
									sParameterValue = "";
								}
								%>
								str += "<input TYPE='hidden' NAME='<%= ActuUtil.jsEncode(sParameterName) %>' VALUE='<%= ActuUtil.jsEncode(sParameterValue) %>'>";
								<%
							}
							%>
							<%
							eParameters = Request.QueryString.AllKeys;
							String[] formParameters = Request.Form.AllKeys;
							for (int i=0; i < eParameters.Length; i++)
							{
								String sParameterName = eParameters[i];
								bool isDuplicate = false;
								
								if (sParameterName.ToLower().Equals("format") || sParameterName.ToLower().Equals("startingpoint") || sParameterName.ToLower().Equals("hits"))
								{
									continue;
								}


								for (int j = 0; j < formParameters.Length; j++)
								{
									if (sParameterName.ToLower().Equals(formParameters[j].ToLower()))
									{
										isDuplicate = true;
										break;
									}
								}
								
								if (isDuplicate)
								{
									continue;
								}

								String sParameterValue = AcViewerParams.Get(Page, sParameterName, AcParams.From.Url);
								if (sParameterValue == null)
								{
									sParameterValue = "";
								}

								%>
								str += "<input TYPE='hidden' NAME='<%= ActuUtil.jsEncode(sParameterName) %>' VALUE='<%= ActuUtil.jsEncode(sParameterValue) %>'>";
								<%
							}
							%>
							str += "</FORM>";

							addToDocument(str);
							hiddenFormStr = str;
						}
						<%
					}
					else
					{
					%>
						<%
							parameterList.Clear();
							parameterList.Add(CommandName);
						%>
						alert("<%= ActuUtil.MessageFormat(AcResourceManager.GetString("TXT_UNEXPECTED_DIRECTIVE", Session), parameterList) %>");
						<%
					}
					%>

				</script>
				<template:css runat="server"/>
			</head>
			
			
			<body bgcolor='white' onLoad='onLoad()' class='searchresultlink' onUnload='onUnload()'>
				<script>

					addToDocument("<div class='label'>");
					<%
						parameterList.Clear();
						parameterList.Add(searchResult.GetHits());
					%>
					addToDocument("<p class='label'><%= ActuUtil.MessageFormat(AcResourceManager.GetString("TXT_SMART_SEARCH", Session), parameterList) %></P>");
					addToDocument("<p></p>");

					if (g_totalNumberOfHits > 0)
					{
						if (g_startHitNumber > g_totalNumberOfHits)
						{
							addToDocument("<p>Go to the <a class='searchresultlink' href='javascript:gotoStartOfResults()'><B>Start</B></A> of the result set.</P>");
						}
						g_resultSetNavigationLine += "<table><tr><td nowrap>";

						if (g_startHitNumber != 0)
						{
							g_resultSetNavigationLine += "<a class='searchresultlink' href='javascript:gotoPreviousResults()'><B><%= AcResourceManager.GetString("BTN_SEARCH_PREV", Session) %></B></A>";
						}
						else
						{
							g_resultSetNavigationLine += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						}
						g_resultSetNavigationLine += "</td>";

						if (g_startHitNumber < g_endHitNumber)
						{
							<%
								parameterList.Clear();
								parameterList.Add(Convert.ToString(iStartHitNumber + 1));
								parameterList.Add(Convert.ToString(iEndCurrentHit));
							%>
							g_resultSetNavigationLine +="<TD NOWRAP class='label'><%= ActuUtil.MessageFormat(AcResourceManager.GetString("TXT_SEARCH_RESULT2", Session), parameterList) %>";
						}
						else if (g_startHitNumber == g_endHitNumber)
						{
							<%
								parameterList.Clear();
								parameterList.Add(Convert.ToString(iStartHitNumber + 1));
							%>
							g_resultSetNavigationLine += "<TD NOWRAP class='label'><%= ActuUtil.MessageFormat(AcResourceManager.GetString("TXT_SEARCH_RESULT1", Session), parameterList) %>";
						}
						g_resultSetNavigationLine += "&nbsp;&nbsp</TD>";

						g_resultSetNavigationLine += "<TD NOWRAP>";

						if (g_startHitNumber <= g_endHitNumber  &&  g_endHitNumber < g_totalNumberOfHits)
						{
							g_resultSetNavigationLine += "<A class='searchresultlink' HREF='javascript:gotoNextResults()'><B><%= AcResourceManager.GetString("BTN_SEARCH_NEXT", Session) %></B></A>";
						}
						else
						{
							g_resultSetNavigationLine += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						}
						g_resultSetNavigationLine += "</TD></TR></TABLE>";

						addToDocument(g_resultSetNavigationLine);
					}

					addToDocument("<TABLE BORDER='0' CELLPADDING='0' CELLSPACING='5'>");

					<%
					ArrayList llTHResult = searchResult.GetTHResult();
					for (int i = 0; i < llTHResult.Count; i++)
					{
						//Properties pr = (Properties)llTHResult[i];
						if (i == 0)
						{
							%>
							addToDocument("<TR>");
							addToDocument("<TD WIDTH='8'>&nbsp;</TD>");
							<%
						}
						%>
						addToDocument("<TD nowrap class='boldlabel'><%= ActuUtil.jsEncode(ActuUtil.convertUTF8toUCS2((String) llTHResult[i])) %>&nbsp;&nbsp;&nbsp;</TD>");
						<%
						if( i==llTHResult.Count - 1)
						{
							%>
							addToDocument("<TD WIDTH='8'>&nbsp;</TD>");
							addToDocument("</TR>");
							<%
						}
					}
					%>

					////////////////////////////////////////////// End of selected Result Fields //////////////////////////////////////////////

					addToDocument("<TR>");
					addToDocument("<TD>&nbsp;</TD>");
					addToDocument("</TR>");

					////////////////////////////////////////////// Result Hits //////////////////////////////////////////////
					// The search result URL  (objURL) may contain a multibyte character whose second byte character is a
					// backslash.  The backslash will be unescaped by the IE browser when it is passed into a JavaScript
					// function. Detect the backslash before going into the function and add another backslash in front of it
					// to escape it twice.  We only do this for IE browsers since that's where the problem exists.
					<%
					ArrayList llResult = searchResult.GetSearchResult();
					int ctrResult = 0;
					String sCH = (strConnectionHandle != null && !strConnectionHandle.Equals("")) ? ", \"" + HttpUtility.UrlEncodeUnicode(strConnectionHandle) + "\"" : ", \"\"";
					String sON = (sName != null) ? ", \"" + ActuUtil.htmlEncode(sName) + "\"" : null;
					sON = (sON == null && sOutputName != null) ? ", \"" + ActuUtil.htmlEncode(sOutputName) + "\"" : "";

					while(ctrResult < llResult.Count)
					{
						for(int j = 0; j < llTHResult.Count; j++)
						{
							if (j == 0)
							{
								%>
								addToDocument("<TR>");
								addToDocument("<TD>&nbsp;</TD>");
								<%
							}

							AcSearchResultItem pr = (AcSearchResultItem)llResult[ctrResult];
							String sValue = pr.Value;
							if (sValue == null || sValue.Length == 0)
							{
								%>
								var displayStr = " ";
								<%
							}
							else
							{
								%>
								var displayStr = "<%= ActuUtil.jsEncode(ActuUtil.convertUTF8toUCS2(sValue)) %>";
								<%
							}
							%>

							var objURL = js_ObjectURL;
							var compID =  "<%= pr.Id %>";
							var linkClickStr = "<A class='searchresultlink' onClick='handleLinkClick(";
							linkClickStr += '"' + objURL + '", "' + compID + '"<%= sCH %><%= sON %>); return false';
							linkClickStr += "' HREF='$(hrefLinkError)'>";

							addToDocument("<TD nowrap class='label'>");
							if (compID != "0")
							{
								addToDocument(linkClickStr + htmlEncode(displayStr) + "</A>");
							}
							else
							{
								addToDocument(htmlEncode(displayStr));
							}

							addToDocument("</TD>");
							<%
							if (j == llTHResult.Count - 1)
							{
								%>
								addToDocument("<TD>&nbsp;</TD>");
								addToDocument("</TR>");
								<%
							}
							else
							{
								ctrResult++;
							}
						}
						ctrResult ++;
					}
					%>

					addToDocument("<TR>");
					addToDocument("<TD WIDTH='8'>&nbsp;</TD>");
					addToDocument("</TR>");
					addToDocument("</TABLE>");
					addToDocument("<BR>");

					<%
					for (int i = 0; i < llFormats.Length; i ++)
					{
						String sValue = (String) llFormats[i];
						if (sValue.ToLower().Equals("analysis"))
						{
							%>
							isAnalysisSupported = true;
							<%
							break;
						}
					}
					%>

					if (g_totalNumberOfHits > 0)
					{
						addToDocument(g_resultSetNavigationLine);
					}

					g_changeSearchButtonLine = "<FORM NAME='newSearchForm'><TABLE><TR><TD NOWRAP>";
					g_changeSearchButtonLine += "<input TYPE='button' STYLE='width:100%' VALUE='"+"<%= AcResourceManager.GetString("BTN_NEW_SEARCH", Session) %>"+"' BORDER=0 ONCLICK='showSearchRequestForm()'></TD>";

					// Changes for save smart search feature
					g_changeSearchButtonLine += "<TD NOWRAP>";
					<%
					if (needSave)
					{
					%>
					g_changeSearchButtonLine += "<input TYPE='button' STYLE='width:100%' VALUE='" + "<%= AcResourceManager.GetString("BTN_SAVE_SEARCH", Session) %>" + "' BORDER=0 ONCLICK='showSaveSearchForm(eval(saveSearchForm))'>";
					<%
					}
					%>
					g_changeSearchButtonLine += "<TD NOWRAP>";
					"</TD>";
					g_changeSearchButtonLine += "</TR>";

					if (isAnalysisSupported && showAnalysisButton && g_totalNumberOfHits > 0)
					{
						g_changeSearchButtonLine += "<TR>";
						g_changeSearchButtonLine += "<TD NOWRAP COLSPAN='1'><input TYPE='button' STYLE='width:100%' VALUE='"+"<%= AcResourceManager.GetString("BTN_ANALYSIS", Session) %>"+"' BORDER=0 ONCLICK='submitAnalysisForm(eval(extractForm))'></TD>";
						g_changeSearchButtonLine += "</TR>";
					}
					g_changeSearchButtonLine += "</TABLE></FORM>";

					addToDocument(g_changeSearchButtonLine);
					addToDocument("</div>");

					if ( g_totalNumberOfHits )
					{
						g_downloadAsButtonLine =  "<FORM NAME='downloadSearchForm'>";
						g_downloadAsButtonLine += "<TABLE><TR>";
						g_downloadAsButtonLine += "<TD NOWRAP class='label'><%= AcResourceManager.GetString("TXT_DOWNLOAD_SEARCH_RES", Session) %></TD>";
						g_downloadAsButtonLine += "<TD>&nbsp;</TD>";
						g_downloadAsButtonLine += "<TR><TD NOWRAP>";
						g_downloadAsButtonLine += "<SELECT NAME='format' onChange ='handleFormatChange(document.downloadSearchForm)'>";
						g_downloadAsButtonLine += "<OPTION VALUE='CSV' SELECTED><%= AcResourceManager.GetString("OPTION_CSV", Session) %>";
						g_downloadAsButtonLine += "<OPTION VALUE='UNCSV'><%= AcResourceManager.GetString("OPTION_UCSV", Session) %>";
						g_downloadAsButtonLine += "<OPTION VALUE='TSV'><%= AcResourceManager.GetString("OPTION_TSV", Session) %>";
						g_downloadAsButtonLine += "<OPTION VALUE='UNTSV'><%= AcResourceManager.GetString("OPTION_UTSV", Session) %>";
						g_downloadAsButtonLine += "<OPTION VALUE='EXCEL'><%= AcResourceManager.GetString("OPTION_EXCEL", Session) %>";
						if (isAnalysisSupported && !showAnalysisButton)
						{
							g_downloadAsButtonLine += "<OPTION VALUE='ANALYSIS'>Analysis";
						}
						g_downloadAsButtonLine += "</SELECT>";
						g_downloadAsButtonLine += "<input TYPE='button' VALUE='"+"<%= AcResourceManager.GetString("BTN_GO", Session) %>"+"' BORDER=0 ONCLICK='submitExtractForm(eval(extractForm))'>";
						g_downloadAsButtonLine += "</TD>";
						g_downloadAsButtonLine += "<TD>&nbsp;</TD></TR></TABLE>";
						g_downloadAsButtonLine += "</FORM>";
						addToDocument(g_downloadAsButtonLine);
					}

					// Form that will be used for the next/prev link click actions and the download action.
					<%
					if (CommandName.ToLower().Equals("submitsearch"))
					{
						%>
						createSearchForm("hiddenSearchForm", false);		// Used for Prev/Next link
						createSearchForm("hiddenExtractResultsForm", true); // Used for CSV, TAB and ANALYSIS formats.
						<%
					}
					else if (CommandName.ToLower().Equals("searchreport"))
					{
						%>
						createSearchReportForm("hiddenSearchForm",false);
						createSearchReportForm("hiddenExtractResultsForm",true);
						<%
					}
					else
					{
						%>
						<%
							parameterList.Clear();
							parameterList.Add(CommandName);
						%>
						alert("<%= ActuUtil.MessageFormat(AcResourceManager.GetString("TXT_UNEXPECTED_DIRECTIVE", Session), parameterList) %>");
						<%
					}
					%>

					//	Create form for save search
					createSaveSearchForm(saveSearchForm);

					writeDocument();

				</script>
			</body>
			
		</html>
	<%
	}
	else
	{
	%>
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<meta http-equiv="Pragma" content="no-cache">
				<meta http-equiv="Cache-Control" content="no-cache">
				<meta http-equiv="Expires" content="-1">
				
				<link rel="stylesheet" type="text/css" href="../css/allstyles.css">
				
				
				
				<script language="javascript" src='../js/encoder.js'></script>
				<script language="javascript" src='../js/search.js'></script>
				<script language="javascript" src='../js/array.js'></script>
				<script language="javascript" src='../js/layer.js'></script>
				<script language="javascript" src='../js/cookie.js'></script>
				<script language="javascript" src='../js/browsertype.js'></script>

				<script language="javascript">

					ns4 = (document.layers)? true:false;
					var js_ObjectURL = "<%= sFwdURL %>";
					var hiddenFormStr ="";
					var g_documentString	= "";
					var inViewFrameset = (eval('parent.g_inViewFrameset') || parent.g_inViewFrameset == true) ? true : false
					var g_changeSearchButtonLine = "";
					var searchToolbarPage = "searchtoolbar.aspx?serverURL=<%= HttpUtility.UrlEncodeUnicode(sServerURL) %>&volume=<%= HttpUtility.UrlEncodeUnicode(sVolume) %>";

					/**
					 *
					 */
					function addToDocument(str)
					{
						g_documentString += str + "\n";
					}

					/**
					 *
					 */
					function writeDocument()
					{
						document.write(g_documentString);
					}

					/**
					 *
					 */
					function onLoad()
					{
						// Check to see if we are in the frameset

						if (!inViewFrameset)
							return;

						parent.g_searchOpen		= true;
						parent.g_searchResults	= true;
						parent.g_searchInitiated = false;

						var reportFrame	= eval( 'parent.frames.reportframe' );

						parent.g_inSearchRequestForm = false;
						if (parent.frames.searchtoolbar && parent.frames.searchtoolbar.searchToolbarLoaded)
						{
							parent.frames.searchtoolbar.enableSearchToolbar();
						}
					}

					/**
					 *
					 */
					function onUnload()
					{
						if (!inViewFrameset) return;
						parent.g_searchOpen = true;
						parent.g_searchResults= false;
					}

					/**
					 *
					 */
					function showSearchRequestForm()
					{
						if (!inViewFrameset) return;
						parent.g_searchResults = false;
						var url = "requestsearch.aspx?" + js_ObjectURL;
						location.replace(url);
						parent.frames.searchtoolbar.location.replace( searchToolbarPage );
					}

				</script>

		
			</head>
			<body bgColor='white' class='searchresultlink' onLoad='onLoad()' onUnload='onUnload()'>
				<script>
					addToDocument("<div class='label'");
					addToDocument("<P class='label'><%= AcResourceManager.GetString("TXT_SMART_SEARCH1", Session) %></P>");
					addToDocument("<P></P>");
					g_changeSearchButtonLine = "<FORM NAME='newSearchForm'><TABLE><TR><TD NOWRAP>";
					g_changeSearchButtonLine += "<input TYPE='button' VALUE='"+"<%= AcResourceManager.GetString("BTN_NEW_SEARCH", Session) %>"+"' BORDER=0 ONCLICK='showSearchRequestForm()'>";
					g_changeSearchButtonLine += "</FORM>";
					addToDocument(g_changeSearchButtonLine);
					addToDocument("</div>");
					writeDocument();
				</script>
			</body>
			
		</html>
<% } %>
