/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using activeportal.classes;
	using activeportal.classes.api;

	/// <summary>
	///		Summary description for print.
	/// </summary>
	public partial  class print : System.Web.UI.UserControl
	{
		protected String sID;
		protected String sName;
		protected String sURL;
		protected String sTitleDocName;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			sID = AcParams.Get(Page, AcParams.Name.id);
			sName = AcParams.Get(Page, AcParams.Name.name);
			String sPage = AcParams.Get(Page, AcParams.Name.page);
			bool bExistConnHandle = AcParams.Exists(Page, AcParams.Name.connectionhandle);
			String sConnectionHandle = AcParams.Get(Page, AcParams.Name.connectionhandle);
			String sRange = AcParams.Get(Page, AcParams.Name.range);
			String sVersion = AcParams.Get( Page, AcParams.Name.version );

			String sPageRangeToPrint = "1";
			if(sRange != null)
			{
				sPageRangeToPrint = sRange;
			}
			else if(sPage != null)
			{
				sPageRangeToPrint = sPage;
			}
			else
			{
				String sType = null;
				
				if(sName != null)
				{
					sType = ActuUtil.getFileExtension(sName);
					if ( sVersion == null )
					{
						sVersion = ActuUtil.getVersionFromFilename(sName);
					}
				}

				AcGetPageCount getPageCount = new AcGetPageCount( sName, sID, sVersion );
				getPageCount.Execute( Page, sConnectionHandle );

				String pageCount = getPageCount.Response.PageCount;
				sConnectionHandle = getPageCount.Response.ConnectionHandle;

				sPageRangeToPrint = "1-" + pageCount;
			}

			sURL = ActuUtil.jsEncode( "../viewer/viewpage.aspx?" +
					((sID != null && !sID.Equals("null"))?("id=" + sID):("name=" + HttpUtility.UrlEncode(sName))) +
					((sConnectionHandle != null?("&connectionHandle=" + HttpUtility.UrlEncode(sConnectionHandle)):"")) +
					"&range=" + sPageRangeToPrint +
					"&format=PDF&operation=print");

			sTitleDocName = ActuUtil.GetDocNameForViewerTitle(bExistConnHandle, Page);
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
