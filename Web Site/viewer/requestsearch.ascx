<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.viewing.viewer.requestsearch" CodeFile="requestsearch.ascx.cs" %>
<%-- Import name spaces --%>
<%@ Import namespace="activeportal.classes" %>
<%@ Import namespace="activeportal.classes.api.proxy" %>
<%@ Import namespace="actuate_i18n" %>
<%@ Register TagPrefix="template" namespace="actuate.classes.webcontrols.masterpages" assembly="activeportal" %>
<%-- Gather authentication credentials at the top of all level 0 pages --%>

<%-- RESTORE PAGE CONTENT TO UNICODE/UTF8 --%>
<HTML>
	<HEAD>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Expires" content="-1">
		<STYLE TYPE="text/css">
			TH { color:white;
				 font-family:"Arial";
				 font-weight:bold;
				 font-size:10pt;
				 text-decoration: underline;
			   }

			TD { color:white;
				 font-family:"Arial";
				 font-size:11pt; }
		</STYLE>
		<SCRIPT LANGUAGE="JavaScript">
			var g_objectURL = parent.g_objectURL;
			var g_submitSearchURL = "searchframe.aspx" + parent.removeParams(parent.removeParams(parent.removeParams(g_objectURL, "format"), "startingPoint"), "hits");
			var BTN_SEARCH_NOW = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("BTN_SEARCH_NOW", Session)) %>";
			
			g_MsgSearchNotRun = "<%= ActuUtil.jsEncode( AcResourceManager.GetString( "ERRMSG_SEARCH_NOT_RUN", Session )) %>" ;
			g_MsgNoSearchCriteria = "<%= ActuUtil.jsEncode(AcResourceManager.GetString( "ERRMSG_NO_SEARCH_CRITERIA_SPECIFIED", Session )) %>" ;
			g_MsgSpecifySearch = "<%= ActuUtil.jsEncode(AcResourceManager.GetString( "ERRMSG_SPECIFY_SEARCH_CRITERIA", Session ))  %>";

			//	Call get saved search action
			function getSavedSearch()
			{
				var searchFileId = document.forms[0].savedSearch.value;

				//	Clear prevous settings
				parent.frames.searchframe.clearButton();

				if (searchFileId == null || searchFileId.length <= 0)
				{
					return;
				}
				
				var url = "<%=Request.ApplicationPath%>" + "/viewer/getsavedsearch.aspx?rosfileid=" + searchFileId;
				
				<%
				if (sId != null && sId.Trim().Length > 0)
				{
				%>
					url += "&basedonfileid=" + "<%= sId %>";
				<%
				}
				else
				{
				%>
					url += "&basedonfilename=" + "<%= HttpUtility.UrlEncode(sName) %>";
				<%
				}
				%>

				url += "&action=requestsearch";

				<%
				if (connectionHandle != null && connectionHandle.Trim().Length > 0)
				{
				%>
					url += "&connectionhandle=" + "<%= HttpUtility.UrlEncode(connectionHandle) %>";
				<%
				}
				%>

				<%
				if (executableName != null && executableName.Trim().Length > 0)
				{
				%>
					url += "&__executablename=" + "<%= HttpUtility.UrlEncode(executableName) %>";
				<%
				}
				%>

				<%
				if (AcParams.Get(Page, AcParams.Name.__executableId, "") != null &&
					AcParams.Get(Page, AcParams.Name.__executableId, "").Trim().Length > 0)
				{
				%>
					url += "&__executableid=" + "<%= AcParams.Get(Page, AcParams.Name.__executableId, "") %>";
				<%
				}
				%>

				g_searchFrame.location.replace(url);
			}
			
			//	Initialize the saved search UI
			function InitsavedSearch()
			{
				if (parent.frames.reportframe)
				{
					<%
					if (searchJSCommand != null && searchJSCommand.Trim().Length > 0)
					{
					%>
						<%= searchJSCommand %>
					<%
					}
					%>
				}
			}
		</SCRIPT>
		
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/encoder.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC="../js/requestsearch.js"></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/search.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/array.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/layer.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/cookie.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/browsertype.js'></SCRIPT>
		<link rel="stylesheet" type="text/css" href="../css/allstyles.css">
		<template:css runat="server"/>
						
	</HEAD>
	<BODY ONLOAD='onLoad(); InitsavedSearch(); return false;' ONUNLOAD='onUnload(); return false;' STYLE="font-size:10pt;" marginheight="5" topmargin="5">

		<!- Changes for save smart search feature -->
		<DIV STYLE="position:absolute;left:10px;top:10px;text-align:left;width:250px;height: 12px;" class="label">
			<NOBR>
				<%= AcResourceManager.GetString("TXT_SELECT_SAVED_SEARCH", Session) %>
			</NOBR>
			<BR><BR>
			<FORM >
				<SELECT NAME="savedSearch" onchange="javascript:getSavedSearch();">
					<OPTION VALUE=""> <%= AcResourceManager.GetString("TXT_SELECT_NONE_SELECTED", Session) %>
					<%
					if (arryROSFiles != null)
					{
						for (int i = 0; i < arryROSFiles.Count; i++)
						{
							File fileObj = null;
							String rosFileId = "";
							String rosFileName = "";
							String displayName = "";

							fileObj = (File) arryROSFiles[i];
							rosFileId = fileObj.Id;
							rosFileName = fileObj.Name;
							displayName = ActuUtil.stripFileExtension( ActuUtil.stripVersionNumber( ActuUtil.stripFolderName(rosFileName, "/") ) );
							if (displayName != null && displayName.Trim().Length > 0)
							{
								if (rosFileId.Equals(sRosFileId))
								{
					%>
									<OPTION VALUE="<%= rosFileId %>" selected> <%= displayName %>
					<%
								}
								else
								{
					%>
									<OPTION VALUE="<%= rosFileId %>"> <%= displayName %>
					<%
								}
							}
						}
					}
					%>
				</SELECT>
			<FORM>
		</DIV>

		<SCRIPT language="JavaScript">
			
			var g_searchTipURL= "../help/" + "wwhelp.htm?single=true&context=jsp_viewer&topic=ViewingFramework_search";

			var str = "";
			var ns4 = (document.layers)? true:false

			if ( ns4 )
			{
				// The width of the layer is important.  In Netscape 4, the report frame displays only
				// as much as the width of the layer specifies, regardless of how much the layer contains.
				// So we set the width to 900 pixels to allow a lot of writing within the layer and still
				// have the report frame display all of it.
				// The height is also important. Netscape does not show scrollbars in the search request frame
				// even if the scrollbar is set to auto on the frame. When search items are added to the search
				// display list, and content overflows the height, the scrollbars still don't appear. To workaround
				// this problem we increase the height of the search request page when initially loaded, by
				// setting the height of the layer to a large value. Thus the scrollbars will alwys show.
				str = "<LAYER ID='searchFields' LEFT='10px' TOP='100px'  HEIGHT='6000px' WIDTH='900px'>";
				str += "</LAYER>";
			}
			else
			{
				str = "<DIV ID='searchFields' STYLE=\"left:10px; top:100px; height:20px; width:80px;\">";
				str += "</DIV>";
			}

			document.write(str);

			document.close();
		</SCRIPT>
		<DIV STYLE="position:absolute;
			left:10px;
			top:80px;
			text-align:left;
			width:250px;
			height: 12px;" class="label">
			<NOBR>
				<%= AcResourceManager.GetString("TXT_CLICK_ON_RP_FIELD", Session) %>
			</NOBR>
		</DIV>
	</BODY>
</HTML>
