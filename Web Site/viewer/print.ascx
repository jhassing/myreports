
<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.viewing.viewer.print" CodeFile="print.ascx.cs" %>
<%-- Import name spaces --%>
<%@ Import namespace="activeportal.classes" %>
<%@ Import namespace="actuate_i18n" %>

<HTML>
	<HEAD>
		<title><%= AcResourceManager.GetString("BTN_PRINT", Session) %> - <%= sTitleDocName %></title>
		<script language="javascript" src="<%=Context.Request.ApplicationPath%>/js/browsertype.js"></script>
		<%-- SET THE PAGE ENCODING --%>
	<SCRIPT language="JavaScript">
		function printDocument()
		{
			var isNS4 = (navigator.appName == "Netscape" && parseInt(navigator.appVersion) == 4);
			isIE5 = ( g_browserType.name == "IE" && g_browserType.version < 5.5 )
			if( isNS4 ||  isIE5 )
			{
				//If this is Netscape 4 ( or IE version < 5.5 ) then set the location of the window to the URL.
				//This is being done because Netscape 4 can't populate the print dialog with the range
				//of pages and as a result when you click on print an error is displayed.
				//In Netscape 4, the user will need to explicitly click the print button on the Acrobat plugin toolbar.
				window.location.href="<%= sURL %>";
			}
			else
			{
				window.setTimeout("printNow()", 500);
			}
		}
		function printNow()
		{
			window.frames["printableFrame"].print();
		}

	</SCRIPT>
	</HEAD>
	<FRAMESET ROWS="*" onLoad="printDocument()">
	
	<FRAME NAME="printableFrame"  SRC="<%= sURL %>">
	<NOFRAMES>
		<BODY>
			Your browser doesn't support frames
		</BODY>
	</NOFRAMES>
	</FRAMESET>
</HTML>