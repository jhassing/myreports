namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Collections;

	using activeportal.classes;
	using activeportal.classes.api;

	/// <summary>
	///		Summary description for DownloadSearchResult.
	/// </summary>
	public partial  class downloadsearchresult : System.Web.UI.UserControl
	{

		protected void Page_Load(object sender, System.EventArgs e)
		{
			String sName = AcViewerParams.Get(Page, "name");
			String sVersionId = ActuUtil.getVersionFromFilename(sName);
			sName = ActuUtil.stripVersionNumber(AcViewerParams.Get(Page, "name"));
			String sType = ActuUtil.getFileExtension(sName);

			String sId = AcViewerParams.Get(Page, "id");
			String strConnectionHandle = AcParams.Get(Page, AcParams.Name.connectionhandle);
			String sOutputName = AcViewerParams.Get(Page, "outputname");
			String sFormatType = AcViewerParams.Get(Page, "format");
			String sHits = AcViewerParams.Get(Page, "hits");
			String sStartingPoint = AcViewerParams.Get(Page, "startingpoint");
			string sEnablecolumnheaders	= AcViewerParams.Get(Page, "enablecolumnheaders");
			if ( sEnablecolumnheaders == null )
			{
				sEnablecolumnheaders = System.Configuration.ConfigurationSettings.AppSettings[ "SEARCH_ENABLE_COLUMN_HEADERS" ];
			}

			string sUsequotedelimiter	= AcViewerParams.Get(Page, "usequotedelimiter");
			if ( sUsequotedelimiter == null )
			{
				sUsequotedelimiter = System.Configuration.ConfigurationSettings.AppSettings[ "SEARCH_USE_QUOTE_DELIMITER" ];
			}

			Hashtable htSearchSelectParam = AcViewerParams.GetSearchParameters(Page, false);			

			ArrayList llSelect = (ArrayList) htSearchSelectParam["llselect"];
			ArrayList llSearch = (ArrayList) htSearchSelectParam["llsearch"];
			
			bool bSelectAbsent = (llSelect.Count == 0 );

			//	prepare the http headers
			if ( sFormatType != null )
			{
				if (AcString.ToLower(sFormatType).Equals("csv") ||
					AcString.ToLower(sFormatType).Equals("uncsv"))
				{
					Page.Response.ContentType = "application/csv";
					//Page.Response.AddHeader("Content-Disposition", "inline; filename=searchresult.csv");
					Page.Response.AddHeader("Content-Disposition", "attachment; filename=searchresult.csv");
				}
				else if (AcString.ToLower(sFormatType).StartsWith("tsv") || 
					AcString.ToLower(sFormatType).StartsWith("untsv"))
				{
					Page.Response.ContentType = "application/txt";
					//Page.Response.AddHeader("Content-Disposition", "inline; filename=searchresult.txt");
					Page.Response.AddHeader("Content-Disposition", "attachment; filename=searchresult.txt");
				}
				else if (AcString.ToLower(sFormatType).StartsWith("excel"))
				{
					Page.Response.ContentType = "application/x-msexcel";
					//Page.Response.AddHeader("Content-Disposition", "inline; filename=searchresult.xls");
					Page.Response.AddHeader("Content-Disposition", "attachment; filename=searchresult.xls");
				}
			}
			//	Search report
			if(!bSelectAbsent)
			{
				AcSearchReport searchReport = new AcSearchReport(	sId,
																	sName,
																	sType,
																	sVersionId,
																	sFormatType,
																	sStartingPoint,
																	sHits,
																	sEnablecolumnheaders,
																	sUsequotedelimiter,
																	llSelect,
																	llSearch,
																	Request.UserAgent );
				searchReport.Execute( Page, strConnectionHandle );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
