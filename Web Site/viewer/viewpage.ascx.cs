/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using activeportal.classes;
	using activeportal.classes.api;
	using activeportal.classes.api.proxy;

public partial  class viewpage : System.Web.UI.UserControl
	{
		protected String m_fileName;
		protected String m_id;
		protected String m_format = "DHTML";
		protected String m_action = "view";
		protected String m_page = "1";
		protected String m_range;
		protected String m_scalingFactor = "100";
		protected String m_componentId;
		protected String m_componentName;
		protected String m_componentValue;
		protected String m_searchCriteria;
		protected String m_mode;
		protected String m_version;
		protected String m_type;
		protected String m_reportletmaxheight;
		protected String m_reportletcustominputpara;
		protected String m_userAgent;
		protected String m_acceptEncoding;
		protected String m_embsrvrequester;
		protected String m_splitOversizePages;
		protected String m_pageHeight;
		protected String m_pageWidth;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			m_fileName = AcParams.Get( Page, AcParams.Name.name );
			m_id = AcParams.Get( Page, AcParams.Name.id );
			m_action = AcParams.Get( Page, AcParams.Name.action, "view" );
			m_format = AcParams.Get( Page, AcParams.Name.format, "dhtml" );
			m_page = AcParams.Get( Page, AcParams.Name.page, "1" );
			m_range = AcParams.Get( Page, AcParams.Name.range );
			m_scalingFactor = AcParams.Get( Page, AcParams.Name.scalingFactor );
			m_componentId = AcParams.Get( Page, AcParams.Name.componentid );
			m_componentName = AcParams.Get( Page, AcParams.Name.componentname, null );
			m_componentValue = AcParams.Get( Page, AcParams.Name.componentvalue, null );
			m_searchCriteria = AcParams.Get( Page, AcParams.Name.searchcriteria );
			m_mode = AcParams.Get( Page, AcParams.Name.mode, null );
			m_version = AcParams.Get( Page, AcParams.Name.version, null );
			m_type = AcParams.Get( Page, AcParams.Name.type, null );
			m_reportletmaxheight = AcParams.Get( Page, AcParams.Name.reportletmaxheight, null );
			m_reportletcustominputpara = AcParams.Get( Page, AcParams.Name.reportletcustominputpara, null );
			m_userAgent = AcParams.Get( Page, AcParams.Name.useragent, null );
			if (m_userAgent == null || m_userAgent.Trim().Length <= 0)
			{
				m_userAgent = Request.UserAgent;
			}
			m_acceptEncoding = AcParams.Get( Page, AcParams.Name.acceptencoding, null );
			if (m_acceptEncoding == null || m_acceptEncoding.Trim().Length <= 0)
			{
				m_acceptEncoding = Request.Headers["Accept-Encoding"];
			}
			m_embsrvrequester = AcParams.Get( Page, AcParams.Name.embsrvrequester, "operation=" );

			m_splitOversizePages = AcParams.Get( Page, AcParams.Name.splitOversizePages, null );
			m_pageWidth = AcParams.Get( Page, AcParams.Name.pageWidth, null );
			m_pageHeight = AcParams.Get( Page, AcParams.Name.pageHeight, null );

			String l_connectionHandle = AcParams.Get(Page, AcParams.Name.connectionhandle);

			string pdfQuality = AcParams.Get( Page, AcParams.Name.pdfquality, AcParams.From.Url );
			string operation = AcParams.Get( Page, AcParams.Name.operation, "view", AcParams.From.Url  );
			AcSelectPage selectPage = new AcSelectPage(m_fileName,
														m_id,
														m_format,
														m_action,
														m_page,
														m_range,
														m_scalingFactor,
														m_componentId,
														m_componentName,
														m_componentValue,
														m_searchCriteria,
														m_mode,
														m_version,
														m_type,
														m_reportletmaxheight,
														m_reportletcustominputpara,
														HttpUtility.UrlEncode(l_connectionHandle),
														m_userAgent,
														m_acceptEncoding,
														m_embsrvrequester,
														m_splitOversizePages,
														m_pageWidth,
														m_pageHeight,
														pdfQuality,
														operation);

			selectPage.Execute( Page, l_connectionHandle );
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
