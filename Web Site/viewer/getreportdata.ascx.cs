namespace activeportal.usercontrols.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using activeportal.classes;
	using activeportal.classes.api;

	/// <summary>
	///		Summary description for getreportdata.
	/// </summary>
	public partial  class getreportdata : System.Web.UI.UserControl
	{

		private string m_connectionHandle;
		private string m_name;
		private string m_objectId;
		private string m_componentId;
		private string m_componentName;
		private string m_format;
		private string m_scalingFactor;
		private long m_maxHeight = 0;
		private long m_version = 0;
		private string m_embeddedPath;
		private string m_componentValue;
		private bool m_doStream = true;

		public string ConnectionHandle
		{
			set { m_connectionHandle = value; }
			get { return m_connectionHandle ; }
		}

		public string Name
		{
			set { m_name = value; }
			get { return m_name ; }
		}

		public string ObjectId
		{
			set { m_objectId = value; }
			get { return m_objectId ; }
		}

		public string ComponentId
		{
			set { m_componentId = value; }
			get { return m_componentId ; }
		}

		public string ComponentName
		{
			set { m_componentName = value; }
			get { return m_componentName ; }
		}

		public string ComponentValue
		{
			set { m_componentValue = value; }
			get { return m_componentValue ; }
		}

		public string Format
		{
			set { m_format = value; }
			get { return m_format ; }
		}
		public string ScalingFactor
		{
			set { m_scalingFactor = value; }
			get { return m_scalingFactor ; }
		}

		public long MaxHeight
		{
			set { m_maxHeight = value; }
			get { return m_maxHeight ;}
		}

		public long Version
		{
			set { m_version = value; }
			get { return m_version ;}
		}

		public string EmbeddedPath
		{
			set { m_embeddedPath = value; }
			get { return m_embeddedPath ;}
		}


		protected void Page_Load(object sender, System.EventArgs e)
		{
			m_connectionHandle = AcParams.Get(Page, AcParams.Name.connectionhandle, m_connectionHandle);
			m_name = AcParams.Get( Page, AcParams.Name.name, m_name);
			m_objectId = AcParams.Get( Page, AcParams.Name.objectid, m_objectId);
			if ( m_objectId == null )
			{
				m_objectId = AcParams.Get( Page, AcParams.Name.id );
			}
			
			m_componentId = AcParams.Get( Page, AcParams.Name.componentid, m_componentId);
			m_componentName = AcParams.Get( Page, AcParams.Name.componentname, m_componentName );
			m_componentValue = AcParams.Get( Page, AcParams.Name.componentvalue, m_componentValue );
			m_embeddedPath = AcParams.Get( Page, AcParams.Name.embsrvrequester, m_embeddedPath );
			m_maxHeight = System.Int32.Parse( AcParams.Get( Page, AcParams.Name.reportletmaxheight, m_maxHeight.ToString() ) ); 
			m_format = AcParams.Get( Page, AcParams.Name.format, m_format);
			m_scalingFactor = AcParams.Get( Page, AcParams.Name.scalingFactor, m_scalingFactor);
			m_version = System.Int32.Parse( AcParams.Get( Page, AcParams.Name.version, m_version.ToString( ) ) );

		

			// If not reportlet, we need to set http header at page load time
			if ( m_format != null )
			{
				m_format = AcString.ToLower(m_format);
				if ( m_format != "dhtml" && m_format != "reportlet" )
				{
					StreamContent();
					m_doStream = false;
				}
			}
			
		}

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
		
			// If it is a reportlet, we don't set any http header, we just stream the content at
			// render time to allow surrounding html to render.
			if ( m_doStream )
			{
				StreamContent();
			}
		}

		private void StreamContent()
		{
			string operation = AcParams.Get( Page, AcParams.Name.operation );
			string encoding = AcParams.Get( Page, AcParams.Name.encoding );
			string type = AcParams.Get( Page, AcParams.Name.type );
			string converterparam = AcParams.Get( Page, AcParams.Name.converterparam );
			string userAgent = AcParams.Get( Page, AcParams.Name.useragent, Request.UserAgent );
			string pdfQuality = AcParams.Get( Page, AcParams.Name.pdfquality, AcParams.From.Url );

			AcGetContent getContent = new AcGetContent( m_name,
				m_version,
				m_maxHeight,
				m_embeddedPath,
				m_componentId,
				m_componentName,
				m_componentValue,
				m_objectId,
				m_format,
				m_scalingFactor,
				operation,
				encoding,
				type,
				converterparam,
				userAgent,
				m_connectionHandle,
				pdfQuality); 
			
			getContent.Execute( Page, m_connectionHandle );
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
