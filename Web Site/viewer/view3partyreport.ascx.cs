using ReportDistribution.SQLServerDAL;

namespace activeportal.viewer
{
	using System;
	using System.Web;
	using activeportal.classes;
	using activeportal.classes.api;
	using MyReports.Common;

	/// <summary>
	///		Summary description for view3partyreport.
	/// </summary>
	public partial  class view3partyreport : System.Web.UI.UserControl
	{

		private bool m_isTransient = false;

		private void DownloadFile( string sPersistent, string sName, string sFileID, string sConnectionHandle, string sOutputFileName, string tempDir, bool deCompose ) 
		{
			// Call the correct API depending on if this is a persistent 
			// or a transient report
			
			// if no connection handle we assume it's persistent
			if ( string.IsNullOrEmpty(sConnectionHandle))
			{
				sPersistent = "true";
			}
			if ( sPersistent != null && AcString.ToLower(sPersistent) == "true" )
			{
				m_isTransient = false;
				AcDownloadFile downloadFile = new AcDownloadFile( sName, sFileID, sOutputFileName, deCompose );
				downloadFile.Execute( tempDir, Page, sConnectionHandle  );
			}
			else
			{
				m_isTransient = true;
				AcDownloadTransientFile downloadFile = new AcDownloadTransientFile( sFileID, deCompose );
				downloadFile.Execute( tempDir, Page, sConnectionHandle  );
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			String sConnectionHandle = AcParams.Get(Page, AcParams.Name.connectionhandle, AcParams.From.Url);
			String sFileID = AcParams.Get(Page, AcParams.Name.id, AcParams.From.Url);
			String sName = AcParams.Get(Page, AcParams.Name.name, AcParams.From.Url);
			String sOutputFileType = AcParams.Get(Page, AcParams.Name.outputfiletype, AcParams.From.Url);
			String sOutputFileName = AcParams.Get(Page, AcParams.Name.outputfilename, AcParams.From.Url);
			String sDecomposeCompoundDoc = AcParams.Get(Page, AcParams.Name.returncontents, AcParams.From.Url);
			String sPersistent = AcParams.Get(Page, AcParams.Name.persistent, AcParams.From.Url);

			try
			{
				string[] sParms = new string[1];
				sParms[0] = "TimeStamp";
				AcGetFileDetails ddd = new AcGetFileDetails( sName, sFileID, sParms);

				ddd.Execute( Page, sConnectionHandle );

				// Log this report to the SQL Server database
				if ( ddd.Response != null )			
					new Reports(AppValues.MyReportsDB).InsertReportHasBeenViewed(AppValues.UserName, ActuUtil.stripVersionNumber( sName ), ddd.Response.File.TimeStamp);

			}
			catch ( Exception ex )
			{

			}

			bool deCompose = false;
			try
			{
				deCompose = Convert.ToBoolean(sDecomposeCompoundDoc);
			}
			catch (Exception)
			{
				deCompose = false;
			}
			
			if(deCompose)
			{
				bool retry = true;
				while ( retry )
				{
					retry = false;

					String sGlobalFileId = AcCachedFileManager.GetFileIdentifier(Page);
					CachedFile l_cachedFile = AcCachedFileManager.GetCachedFile(sGlobalFileId);

					lock(l_cachedFile)
					{
						if( l_cachedFile.Deleted )
						{
							// Race condition, the sweep has just deleted the file, while we were
							// waiting for the lock. Just redownload it again

							retry = true;
							continue;
						}

						if(!l_cachedFile.FinishedCaching)
						{
						
							DownloadFile( sPersistent, sName, sFileID, sConnectionHandle, sOutputFileName, l_cachedFile.Name, deCompose );
							l_cachedFile.FinishedCaching = true;
						}
						
						// compute the virtual path and remove it

						string path = System.Configuration.ConfigurationSettings.AppSettings[ "TEMP_FOLDER_LOCATION" ].ToLower( );
						string physPath = Page.Server.MapPath( ".." ).ToLower( );

						if ( path.StartsWith( physPath ) )
						{
							path = path.Substring( physPath.Length + 1 ); 
						}

						path = HttpUtility.UrlEncode( path );

						if ( ! path.StartsWith( "/" ) )
						{
							path = "~/" + path;
						}
						else
						{
							path = "~" + path;
						}
						if ( ! path.EndsWith( "/" ) )
						{
							path = path + "/";
						}
						
						// We don't cache transient reports
						
						l_cachedFile.IsTransient = m_isTransient;

						// Here we already have the Url encoded Name
						string fileName = path + HttpUtility.UrlEncode( l_cachedFile.Name );
						
						// replace  + by %20. HttpUtility.UrlEncode function seems to be 
						// encoding " " into + , giving problems

						fileName = fileName.Replace( "+","%20");
					
						Page.Response.Redirect( fileName  + "/main.html"  );
					}
				}
			}
			else
			{
				DownloadFile( sPersistent, sName, sFileID, sConnectionHandle, null, null, deCompose );
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
