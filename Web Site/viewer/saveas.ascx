<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.viewing.viewer.saveas" CodeFile="saveas.ascx.cs" %>
<%-- Import name spaces --%>
<%@ Import namespace="activeportal.classes" %>
<%@ Import namespace="actuate_i18n" %>
<%@ Register TagPrefix="template" namespace="actuate.classes.webcontrols.masterpages" assembly="activeportal" %>

<%-- Gather authentication credentials at the top of all level 0 pages --%>

<%-- RESTORE PAGE CONTENT TO UNICODE/UTF8 --%>
<script src="../js/viewer.js"></script>
<%
	if (sCalledFrom == null || !sCalledFrom.ToLower().Equals("nav"))
	{
		// TODO need to get the total no. of pages if this page is called directly
%>

		<!--
		<viewer:getPageCount authID="<%=sAuthID%>" volume="<%=sVolume%>" serverURL="<%=sServerURL%>" locale="<%= acLocale %>"
			connectionHandle="<%=sConnectionHandle%>" id="<%=sId%>" name="<%=sName%>" type="<%=sType%>" version="<%=sVersionId%>" />
		-->
<%
	}
%>



<HTML>
<head>
		<%-- SET THE PAGE ENCODING --%>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Expires" content="-1">
		<title><%= AcResourceManager.GetString("TXT_TITLE", Session) %> - <%= sTitleDocName %></title>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/encoder.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/saveas.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/browsertype.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/cookie.js'></SCRIPT>
		<SCRIPT LANGUAGE="JavaScript" SRC='../js/allscripts.js'></SCRIPT>

		<link rel="stylesheet" type="text/css" href="../css/allstyles.css">

		<SCRIPT LANGUAGE="JavaScript">
			ERRMSG_INVALID_PAGE_RANGE = "<%= ActuUtil.jsEncode(AcResourceManager.GetString("ERRMSG_INVALID_PAGE_RANGE", Session)) %>";
			var g_objectURL = "<%= ActuUtil.jsEncode(sFwdURL) %>";
			var g_Format = "<%= sFormat %>";
			var TotalPages =<%=sTotalPages%>;
			var currentPage = <%=sPage%>;
			var applyURL = "viewpage.aspx" + replaceDuplicateParams(replaceDuplicateParams(g_objectURL , "operation", "print") , "page" , currentPage);
			var g_saveAsForm;

			var ns4 = (document.layers)? true:false;

			var g_pageRange	= "";
		</SCRIPT>
		
</head>
<BODY onLoad="selectCurrent()" >

<form name='frmFileFormat' method='POST'>

<DIV STYLE="margin-left: 10;DISPLAY: none; VISIBILITY: hidden">
	<BR>
	<TABLE WIDTH=98% BORDER="0" CELLSPACING="0" CELLPADDING="0">
		<TR>
			<TD colspan=3 CLASS="title">			
				<%= AcResourceManager.GetString("TXT_FILE_FORMAT", Session) %>
			</TD>
			<TD width=12 ROWSPAN=2>
				&nbsp;
			</TD>
		</TR>
		<TR>
			<TD Width=5%>
				&nbsp; 
			</TD>
			<TD Width=25% class="label">
				<INPUT name="fileformat" type="radio" value='1' CHECKED><%= AcResourceManager.GetString("FMT_PDF", Session) %>
			</TD>
			<TD class="label">
				<BR>
				<%= AcResourceManager.GetString("TXT_PDF_QUALITY", Session) %>
				<select id="pdf_quality" name="pdf_quality" >
					<option value="100">100</option>
					<option value="150">150</option>
					<option value="200">200</option>
					<option value="250">250</option>
					<option value="300">300</option>
				</select>
				<BR>
				<%= AcResourceManager.GetString("TXT_SPLIT_LARGE_PAGES", Session) %>
				<select id="split_large_pages" name="split_large_pages">
					<option value="default">
						<%= AcResourceManager.GetString("TXT_SPLIT_LARGE_PAGES_DEFAULT", Session) %>
					</option>
					<option value="yes">
						<%= AcResourceManager.GetString("TXT_SPLIT_LARGE_PAGES_YES", Session) %>
					</option>
					<option value="no">
						<%= AcResourceManager.GetString("TXT_SPLIT_LARGE_PAGES_NO", Session) %>
					</option>
				</select>
				<BR>

				<%= AcResourceManager.GetString("TXT_PAGE_WIDTH", Session) %>
				<INPUT name="page_width" type="text" size="8" maxlength="8">
				&nbsp;&nbsp;
				<%= AcResourceManager.GetString("TXT_PAGE_HEIGHT", Session) %>
				<INPUT name="page_height" type="text" size="8" maxlength="8">
				<BR><BR>
			</TD>
		</TR>
		
		<TR>
			<TD Width=5%>
				&nbsp; 
			</TD>
			<TD Width=25% class="label">
					<BR>
					<INPUT name="fileformat" type="radio" value='2' ><%= AcResourceManager.GetString("FMT_EXCEL_TABLE", Session) %><BR>
					<INPUT name="fileformat" type="radio" value='3' ><%= AcResourceManager.GetString("FMT_EXCEL_DISPLAY", Session) %><BR>
					<INPUT name="fileformat" type="radio" value='4' ><%= AcResourceManager.GetString("FMT_RTF_FRAME", Session) %><BR>
					<INPUT name="fileformat" type="radio" value='5' ><%= AcResourceManager.GetString("FMT_RTF_TEXTBOX", Session) %>
			</TD>
			<TD class="label">
				<%= AcResourceManager.GetString("TXT_TIPS", Session) %><BR>
				<%= AcResourceManager.GetString("TXT_TIP_PDF", Session) %><BR>
				<%= AcResourceManager.GetString("TXT_TIP_EXCEL", Session) %><BR>
				<%= AcResourceManager.GetString("TXT_TIP_RTF", Session) %><BR>
				<%= AcResourceManager.GetString("TXT_TIP_PDF1", Session) %><BR>
			</TD>
		</TR>
	</TABLE>
</DIV>

</form>
<form name='frmPageRange' method='POST'>

<DIV STYLE="margin-left: 10;">
<TABLE  WIDTH=98% BORDER="0" CELLSPACING="0" CELLPADDING="0">
<TR>
	<TD><BR>
	&nbsp;
	</TD>
</TR>
<TR  >
	<TD COLSPAN=2 CLASS="title">
		<%= AcResourceManager.GetString("TXT_PAGE_RANGE", Session) %>
	</TD>
	<TD width=12 ROWSPAN=2>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD width=5%>
		&nbsp;
	</TD>
	<TD>
		<p dir="ltr" class="label">
		<BR>
		<INPUT name="pageRange" type="radio" value='a' ><%= AcResourceManager.GetString("RB_ALL", Session) %><BR>
		<INPUT name="pageRange" type="radio" value='c' CHECKED><%= AcResourceManager.GetString("RB_CUR_PAGE", Session) %><BR>
		<INPUT name="pageRange" type="radio" value='r' ><%= AcResourceManager.GetString("RB_PAGES", Session) %>
		<INPUT name='CustomRange' value='' onblur='changeRange(1)'  onKeyDown='g_saveAsForm = document.forms[1];' size="20"> <br><br>
		<%= AcResourceManager.GetString("TXT_ENTER_PG_NOS", Session) %>
		<BR>




	</TD>
</TR>
</TABLE>
</DIV>

<TABLE WIDTH=96.5% BORDER="0" CELLSPACING="0" CELLPADDING="0" STYLE="margin-left: 10;">
<BR>
<TR CLASS="filterGray" >
	<TD><Center>
		<DIV STYLE="margin-left: 10;">
	   		<INPUT type=button value="<%= AcResourceManager.GetString("BTN_VIEW_REPORT", Session) %>" class="button" onclick="applyLinkClick(0, 1)">
		</DIV>
	</Center></TD>
	<TD><Center>
		<DIV STYLE="margin-left: 10;">
	    	<INPUT type=button value="<%= AcResourceManager.GetString("BTN_SAVE_REPORT", Session) %>" class="button"  onclick="applyLinkClick(1, 1)">

		</DIV>
	</Center></TD>
</TR>

</TABLE>
<BR>

</form>
</BODY>
</HTML>
