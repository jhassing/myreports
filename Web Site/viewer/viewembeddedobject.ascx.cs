/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols.viewing.viewer
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using activeportal.classes;
	using activeportal.classes.api;

	/// <summary>
	///		Summary description for viewembeddedobject.
	/// </summary>
	public partial  class viewembeddedobject : System.Web.UI.UserControl
	{

		String m_operation = "GetStyleSheet";
		public String Operation
		{
			get{ return m_operation; }
			set{ m_operation = value; }
		}

		String m_name;
		public String Name
		{
			get{ return m_name; }
			set{ m_name = value; }
		}

		String m_version;
		public String Version
		{
			get{ return m_version; }
			set{ m_version = value; }
		}

		String m_type;
		public String Type
		{
			get{ return m_type; }
			set{ m_type = value; }
		}

		String m_objectId;
		public String ObjectId
		{
			get{ return m_objectId; }
			set{ m_objectId = value; }
		}

		String m_componentId;
		public String ComponentId
		{
			get{ return m_componentId; }
			set{ m_componentId = value; }
		}

		String m_format;
		public String Format
		{
			get{ return m_format; }
			set{ m_format = value; }
		}

		String m_streamName;
		public String StreamName
		{
			get{ return m_streamName; }
			set{ m_streamName = value; }
		}

		String m_embed;
		public String Embed
		{
			get{ return m_embed; }
			set{ m_embed = value; }
		}

		String m_scalingFactor;
		public String ScalingFactor
		{
			get{ return m_scalingFactor; }
			set{ m_scalingFactor = value; }
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			String l_connectionHandle = AcParams.Get(Page, AcParams.Name.connectionhandle);
			m_operation = AcParams.Get( Page, AcParams.Name.operation);
			m_name = AcParams.Get( Page, AcParams.Name.name);
			m_version = AcParams.Get( Page, AcParams.Name.version, null);
			m_type = AcParams.Get( Page, AcParams.Name.type, null);
			m_objectId = AcParams.Get( Page, AcParams.Name.objectid);
			if ( m_objectId == null )
			{
				m_objectId = AcParams.Get( Page, AcParams.Name.id );
			}
			m_componentId = AcParams.Get( Page, AcParams.Name.componentid);
			m_format = AcParams.Get( Page, AcParams.Name.format, "CSS");
			m_streamName = AcParams.Get( Page, AcParams.Name.streamName);
			m_embed = AcParams.Get( Page, AcParams.Name.embed);
			m_scalingFactor = AcParams.Get( Page, AcParams.Name.scalingFactor);

			string userAgent = Request.UserAgent;
			String acceptEncoding = AcParams.Get( Page, AcParams.Name.acceptencoding, null);
			if (acceptEncoding == null || acceptEncoding.Trim().Length <= 0)
			{
				acceptEncoding = Request.Headers["Accept-Encoding"];
			}
			

			if(m_operation.Equals("GetDynamicData"))
			{
				AcGetDynamicData getDynamicData = new AcGetDynamicData( m_name, m_version, m_type, m_componentId, m_objectId, m_format, m_scalingFactor,userAgent );
				
				string hyperchart = AcParams.Get( Page, AcParams.Name.hyperChartXY, AcParams.From.Url );
				if ( hyperchart != null && hyperchart != "")
				{
					// remove the ?
					hyperchart = hyperchart.Substring( 1 );

					// Get x and y
					string[] coordinates = hyperchart.Split( ',' );
				
					// Set them in the request

					getDynamicData.Request.CoordinateX = Convert.ToInt64( coordinates[ 0 ] );
					getDynamicData.Request.CoordinateXSpecified = true;

					getDynamicData.Request.CoordinateY = Convert.ToInt64( coordinates[ 1 ] );
					getDynamicData.Request.CoordinateYSpecified = true;

					getDynamicData.Request.ViewParameter.RedirectPath = "genericredirector.aspx";
					getDynamicData.Execute( Page, l_connectionHandle );
					string redirectUrl = getDynamicData.Response.DataLinkingURL;
					if ( redirectUrl == null || redirectUrl.Trim() == "" )
					{
						Page.Response.Write("<html><script>javascript:history.back( -1 )</script><body/></html>");
					}
					else
					{
						Response.Redirect( redirectUrl );	
					}
					
					
				}

				else
				{
					getDynamicData.Execute( Page, l_connectionHandle );
				}
			}
			else if(m_operation.Equals("GetStaticData"))
			{
				AcGetStaticData getStaticData = new AcGetStaticData( m_name, m_version, m_type, m_objectId, m_format, m_scalingFactor, m_streamName, m_embed, userAgent );
				getStaticData.Execute( Page, l_connectionHandle );
			}
			else if(m_operation.Equals("GetStyleSheet"))
			{
				AcGetStyleSheet getStyleSheet = new AcGetStyleSheet( m_name, m_version, m_type, m_objectId, m_format, userAgent, acceptEncoding);
				getStyleSheet.Execute( Page, l_connectionHandle );
			}
		
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
