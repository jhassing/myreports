<%@ Reference Control="~/common/separator.ascx" %>
<%--	
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.common.navigation" CodeFile="navigation.ascx.cs" %>
<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>


<table border="0" width="100%" class="breadcrumbs" cellspacing="0" cellpadding="3">
		<tr>
			<td width=100% align=left>
				<asp:placeholder id="AcNavigationContent" runat="server" />
			</td>
			
			<td  valign="top" align=right>
				<localized:AcHyperLinkControl id="AcImgMyDocuments" runat=server Key="LNK_MYDOCUMENTS" />
			</td>
			<td  valign="top" nowrap>
				<localized:AcHyperLinkControl valign=top id="AcLnkMyDocuments" CssClass="breadcrumb" Key="LNK_MYDOCUMENTS" runat="server"/>
			</td>
		</tr>
		
</table>
