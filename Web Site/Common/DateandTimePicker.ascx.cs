using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class DateandTimePicker : System.Web.UI.UserControl
{
    public System.DateTime selectedDateTime
    {
        get
        {
            return dp1.SelectedDate.Add(tp1.SelectedTime.TimeOfDay);
        }
        set
        {
            dp1.SelectedDate = value;
            tp1.SelectedTime = value;
        }
    }

}
