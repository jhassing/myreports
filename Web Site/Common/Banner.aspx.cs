using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Common_Banner : System.Web.UI.Page
{
    
    protected void lnkLogout_Click(object sender, System.EventArgs e)
    {
        
        Response.Redirect(Context.Request.ApplicationPath + "/logout.aspx");
    }
}