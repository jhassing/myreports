<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Banner.aspx.cs" Inherits="Common_Banner" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@Register TagPrefix="localized" namespace="Compass.Web.WebControls" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
		<LINK href="../css/allstyles.css" type="text/css" rel="stylesheet">
	</HEAD>
	
	<body topmargin="0" bottommargin="0">
		<form id="Form1" method="post" runat="server">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr height="100%" width="100%" class="banner">
				<%-- ACTUATE LOGO --%>
				<td>
					<img 
      style="BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px" 
      alt="" src="<%=Context.Request.ApplicationPath%>/images/MyReportsLogo.gif" 
      align=middle></td>
				<%-- LINKS: THE UPPER RIGHT SECTION DISPLAYING GLOBALLY ACCESSIBLE HYPERLINKS --%>
				<td align="right">
					<%-- LINK: RESET PASSWORD--%>
					<localized:CgLinkButtonControl class="bannerLink" runat="server" visible="false"  Text="LBL_PASSWORD_RESET" ID="lnkResetPassword" />
					<!-- <span class="bannerLink">|&nbsp;</span> -->
					
					<%-- LINK: LOGOUT --%>
					<asp:LinkButton OnClientClick="javascript:window.parent.location='../logout.aspx'" CssClass="bannerLink" runat="server" Text="Logout" ID="lnkLogout" onclick="lnkLogout_Click" />
					&nbsp;&nbsp;
					
				</td>
			</tr>
			<tr>
				<td colspan="2" height="5" class="treeviewbanner"></td>
			</tr>
		</table>
		</form>
	</body>
</HTML>
