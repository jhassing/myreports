<%@ Page language="c#" Inherits="activeportal.common.calendar" CodeFile="calendar.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>calendar</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<asp:Literal id="Literal2" runat="server"></asp:Literal>
	</HEAD>
	<body>
		<form method="post" runat="server">
			<asp:Calendar id="Calendar1" OnSelectionChanged="Calendar1_SelectionChanged" OnDayRender="Calendar1_DayRender" runat="server">
				<TitleStyle BackColor="#000080" ForeColor="#ffffff" />
				<NextPrevStyle BackColor="#00080" ForeColor="#ffffff" />
				<OtherMonthDayStyle ForeColor="#c0c0c0" />
			</asp:Calendar>
		</form>
	</body>
</HTML>
