using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
using activeportal.classes;

namespace activeportal.common
{
	/// <summary>
	/// Summary description for calendar.
	/// </summary>
	public partial class calendar : System.Web.UI.Page
	{

		private CultureInfo m_originalCulture;
		//protected System.Web.UI.WebControls.Literal Literal1;

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
			string locale = AcParams.Get( Page, AcParams.Name.locale, "en-US");
			m_originalCulture = Thread.CurrentThread.CurrentCulture;

			CultureInfo desiredCulture = new CultureInfo( locale );
			

			
			
			// SCR 
			// Store the  the DateTimeFormat of the Culture
			// All other properties , except the Calendar, will be the same as that
			// of the created object
			// Just change the Calendar Type to Gregorian calendar

			DateTimeFormatInfo requiredDatetimeformat = new DateTimeFormatInfo();
			requiredDatetimeformat = desiredCulture.DateTimeFormat;
			requiredDatetimeformat.Calendar  = new GregorianCalendar();
			
			// Assign the New DateTime Format to the Culture of the 
			// current thread
			desiredCulture.DateTimeFormat = requiredDatetimeformat;

			Thread.CurrentThread.CurrentCulture = desiredCulture;

			// Depending on the Value of the date types in the TextBox
			// Display the Date in the Calendar
			
			string ScheduleDate = AcParams.Get( Page, AcParams.Name.ScheduleDate, AcParams.From.Url );

			if ( ScheduleDate != null &&
				ScheduleDate.Trim() != String.Empty )
			{
				try
				{
					Calendar1.SelectedDate = DateTime.Parse( ScheduleDate, desiredCulture );
					Calendar1.TodaysDate = DateTime.Parse( ScheduleDate, desiredCulture );
				}
				catch( Exception  )
				{
					// Do not do anything
				}
			}



		}

		protected void Page_UnLoad(object sender, System.EventArgs e)
		{
		
			 Thread.CurrentThread.CurrentCulture = m_originalCulture;
		}

		public void Calendar1_SelectionChanged( Object sender, System.EventArgs e)
		{
			// NOTE
			// The Value of the Parameter should match those in the script schedule.js

			string formName = HttpContext.Current.Request.QueryString["formName"];
			string txtBoxName = HttpContext.Current.Request.QueryString["TextBoxName"];

			string strjscript = "<script language=\"javascript\">";
			strjscript += "window.opener.document.forms[0].elements['" + txtBoxName + "'].value='" + Calendar1.SelectedDate.ToString("d") + "' ;";
			strjscript += "window.close();";
			strjscript += "</script" + ">";
			Literal2.Text = strjscript;
		}

		public void Calendar1_DayRender( Object sender, DayRenderEventArgs e )
		{
			if ( e.Day.Date.ToString("d") == DateTime.Now.ToString("d"))
			{
				e.Cell.BackColor = System.Drawing.Color.LightGray;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Unload += new System.EventHandler(this.Page_UnLoad);

		}
		#endregion
	}
}

