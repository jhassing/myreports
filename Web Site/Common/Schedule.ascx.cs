namespace activeportal.common
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Threading;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using actuate_i18n;
	using actuate_i18n.classes;
	using activeportal.classes;
	using activeportal.classes.api;
	using activeportal.classes.api.proxy;
	using System.Globalization;
	using System.Text;
	using actuate.classes.webcontrols;
	using System.Reflection;
	using activeportal.classes.functionality;

	/// <summary>
	///		Summary description for Schedule.
	/// </summary>
	public abstract class Schedule : System.Web.UI.UserControl
	{

		protected HtmlTableRow AcRowPriority;
		protected Label JobNameLbl;
		protected TextBox   JobNameTxt;
		protected Label ScheduleRightNowLbl;
		protected HtmlInputRadioButton ScheduleNowRdBtn;
		protected HtmlInputRadioButton ScheduleOnceRdBtn;

		protected TextBox ScheduleOnceTxt;
		protected AcSafeHyperLink CalOnceDateLink;
		protected Label OnceDateFormatLbl;
		protected TextBox OnceTimeTxt;
		protected Label   OnceTimeLbl;

		protected HtmlInputRadioButton ScheduleRecurringRdBtn;
		protected DropDownList WeekDaysList;
		protected TextBox RecurringTimeTxt;
		protected Label RecurringTimeLbl;
		protected HtmlInputCheckBox RecurringStartChkBox;
		
		protected TextBox StartDateTxt;

		protected AcSafeHyperLink StartDateLink;
		

		protected Label StartDateLbl;

		protected HtmlInputCheckBox useEndDateTime;
		protected TextBox endDateTxt;
		protected AcSafeHyperLink endDateLink;
		protected Label EndDateFormatLbl;
		protected HtmlTableRow InstructionRow;

		protected HtmlInputHidden maxJobPriorityHidden;
		protected RadioButtonList priorityRadioList;
		protected TextBox priorityTxt;

		// Variables used
		private string m_localizedPMString;
		private string m_localizedAMString;

		// RADIO BUTTONS
		public enum eScheduleType
		{
			SCHEDTYPE_IMMEDIATE,
			SCHEDTYPE_ONCE,
			SCHEDTYPE_RECURRING,
		}

		
		
		private int iScheduleType = (int) eScheduleType.SCHEDTYPE_IMMEDIATE;
		private string[] saScheduleTypes = {"immediate", "once", "recurring"};
		private string[] saRadioStates = { "", "", "" };
		private string sScheduleType;
		protected HtmlInputHidden scheduleTypeField;
		protected string m_requestType;


		 string[] saRecurringDays = Constants.AC_RECURRING_SCHEDULES;
		 string[] saRecurringDaysOld  = Constants.AC_RECURRING_SCHEDULES_OLD;

		private string m_shortDatePattern;
		private string m_shortTimePattern;
		private CultureInfo m_currentCulture;
		protected string  m_timeOffset ;

		public string ScheduleType
		{
			get
			{
				return sScheduleType;
			}

			set
			{
				sScheduleType = value;
			}
		}

		
		public int ScheduleIndex
		{
			get
			{
				return iScheduleType;
			}

			set
			{
				iScheduleType = value;
			}
		}

		public string JobName
		{
			get
			{
				return JobNameTxt.Text;
			}

			set
			{
				JobNameTxt.Text = value;
			}
		}

		public string OnceDate
		{
			get
			{
				return ScheduleOnceTxt.Text;
			}

			set
			{
				ScheduleOnceTxt.Text = value;
			}
		}

		public string OnceTime
		{
			get
			{
				return OnceTimeTxt.Text;
			}

			set
			{
				OnceTimeTxt.Text = value;
			}
		}

		public string Priority
		{
			get
			{
				bool noRadioDisplayed = (priorityRadioList.Items.Count  == 0);
				bool otherButtonSelected = (priorityRadioList.SelectedIndex == priorityRadioList.Items.Count -1);

				if (noRadioDisplayed || otherButtonSelected)
					return priorityTxt.Text;
				return priorityRadioList.SelectedItem.Value;
			}
		}

		public string SchedulePeriod
		{
			get
			{
				string selectedItem = "";
				if ( WeekDaysList.SelectedIndex >-1 )
				{
					return WeekDaysList.SelectedItem.Value;
				}
				return selectedItem;
			}

			set
			{
				
				if (value  != null) 
				{
		
					for (int i = 0; i < saRecurringDays.Length; i++)
					{
						if ( String.Compare( value, saRecurringDays[i]) == 0 )
						{
							WeekDaysList.SelectedIndex = i;
						}
					}
				}
			}
		}

		public string RecurringTime
		{
			get
			{
				return RecurringTimeTxt.Text;
			}
			set
			{
				RecurringTimeTxt.Text = value;

			}
		}

		public string RecurringStartDate
		{
			get
			{
				return StartDateTxt.Text;
			}

			set
			{
				StartDateTxt.Text = value;
			}
		}

		public string RecurringEndDate
		{
			get
			{
				return endDateTxt.Text;
			}

			set
			{
				endDateTxt.Text = value;
			}
		}

		private bool m_usedForActuateQuery;
		public bool UsedForActuateQuery
		{
			get
			{
				return m_usedForActuateQuery;
			}

			set
			{
				m_usedForActuateQuery = value;
			}
		}

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		
			// Register the Script for handling Events for Schedule Controls
			RegisterEventHandlerScript();

			PopulateScheduleTable();

		}


		private void PopulateScheduleTable()
		{
			string executableName = AcParams.Get( Page, AcParams.Name.__executableName );
			
			if( ! IsPostBack )
			{
				scheduleTypeField.Value = AcParams.Get( Page, AcParams.Name.__scheduleType, "immediate" );
			}
			m_requestType  = AcParams.Get( Page, AcParams.Name.__requesttype, "immediate" );
			if ( !m_usedForActuateQuery )
			{
				InstructionRow.Visible = false;
			}


			if ( ! IsPostBack )
			{
				string jobName = AcParams.Get( Page, AcParams.Name.__jobname, executableName, AcParams.From.Url );
				
				if ( jobName != null )
				{
					jobName = ActuUtil.stripFolderName( jobName );
					jobName = ActuUtil.stripFileExtension( jobName );
					JobNameTxt.Text = jobName;
				}
			}

			
			string locale = AcParams.Get( Page, AcParams.Name.locale );
			m_currentCulture  = new CultureInfo( locale );

			m_localizedPMString = m_currentCulture.DateTimeFormat.PMDesignator;
			m_localizedAMString = m_currentCulture.DateTimeFormat.AMDesignator;

			
			m_shortDatePattern = m_currentCulture.DateTimeFormat.ShortDatePattern.ToString();

			CalOnceDateLink.ImageUrl = "~/images/calendar.gif";
			CalOnceDateLink.NavigateUrl = "javascript:dummy()";


			OnceDateFormatLbl.Text = " (" +  m_shortDatePattern + ") " +  AcResourceManager.GetString( "MSGT_AT", Session);

			m_shortTimePattern = m_currentCulture.DateTimeFormat.ShortTimePattern.ToString();
			OnceTimeLbl.Text = " (" + m_shortTimePattern + ")";

			RecurringTimeLbl.Text = " (" + m_shortTimePattern + ")" ;


			StartDateLink.ImageUrl = "~/images/calendar.gif";
			StartDateLink.NavigateUrl = "javascript:dummy()";
	

			StartDateLbl.Text = " (" + m_shortDatePattern + ")";

			endDateLink.ImageUrl = "~/images/calendar.gif";
			endDateLink.NavigateUrl = "javascript:dummy()";

			EndDateFormatLbl.Text = " (" + m_shortDatePattern + ")";

			if ( ! IsPostBack )
			PopulateRecurringList();

			if( ! IsPostBack )
				PopulatePriorityControls();

			//Depending on the Parameters specified, update the state of the
			// Various controls
			// To be done only initially 

			if ( ! IsPostBack )
				GetScheduleType();
			else
				// Needed because we use javascript to change the state of controls
				// so relying  just on the viewstate  will not be sufficient
				UpdateScheduleControls();



		}

		private void PopulatePriorityControls()
		{
			if ( !AcFunctionalityManager.HasSubFeature( Page, "JobPriority" ) || m_usedForActuateQuery  )
			{
				AcRowPriority.Attributes.Add( "style", "display:none" );
			}

			int maxPriority = (int)AcParams.GetObjectFromSession( Page, AcParams.Name.maxJobPriority );

			priorityTxt.Text = maxPriority.ToString();

			if (maxPriority >= Constants.LOW_PRIORITY_VALUE)
			{
				ListItem low = new ListItem(AcResourceManager.GetString( "RDBTN_LOW", Session) + " ",
											Constants.LOW_PRIORITY_VALUE.ToString());
				if (maxPriority == Constants.LOW_PRIORITY_VALUE)
					low.Selected = true;
				priorityRadioList.Items.Add(low);

				if (maxPriority >= Constants.MEDIUM_PRIORITY_VALUE)
				{
					ListItem med = new ListItem(AcResourceManager.GetString( "RDBTN_MEDIUM", Session) + " ",
												Constants.MEDIUM_PRIORITY_VALUE.ToString());
					if (maxPriority == Constants.MEDIUM_PRIORITY_VALUE)
						med.Selected = true;
					priorityRadioList.Items.Add(med);
				}
				if (maxPriority >= Constants.HIGH_PRIORITY_VALUE)
				{
					ListItem high = new ListItem(AcResourceManager.GetString( "RDBTN_HIGH", Session) + " ",
												 Constants.HIGH_PRIORITY_VALUE.ToString());
					if (maxPriority == Constants.HIGH_PRIORITY_VALUE)
						high.Selected = true;
					priorityRadioList.Items.Add(high);
				}

				AcKeyArgument[] priorityRange = { new AcKeyArgument( null, "1" ) , new AcKeyArgument( null, maxPriority.ToString() )};
				String otherField = AcResourceManager.GetString( "LBL_OTHER_PR", priorityRange, Session);
				priorityRadioList.Items.Add(new ListItem(otherField, "-1"));
			
				// Select OTHER button if no buttons are selected
				if (priorityRadioList.SelectedIndex == -1)
					priorityRadioList.SelectedIndex = priorityRadioList.Items.Count - 1;
			}

			maxJobPriorityHidden.Value = maxPriority.ToString();
		}

		private void PopulateRecurringList()
		{
			string[] saRecurringDays = Constants.AC_RECURRING_SCHEDULES;

			for (int i = 0; i < saRecurringDays.Length; i++ )
			{
				ListItem scheduleItem = new ListItem();
				scheduleItem.Value = saRecurringDays[i];
				scheduleItem.Text = AcResourceManager.GetString( saRecurringDays[i], Session);
				WeekDaysList.Items.Add( scheduleItem );
			}

		}


		

		private string GetCalendar( TextBox txtFieldName, string sDatePatternShort )
		{
		
			
			string[] saMonthNames = Thread.CurrentThread.CurrentCulture.DateTimeFormat.MonthNames;
			StringBuilder calString = new StringBuilder();
			calString.Append( "javascript:getCalendarFor(");
			calString.Append( "event,");
			calString.Append( "\"" + txtFieldName.UniqueID + "\"," );
			calString.Append( "\"" + ActuUtil.jsEncode( sDatePatternShort)+ "\",");

			for( int i=0; i < 12; i++ )
			{
				calString.Append( "\"" + ActuUtil.jsEncode( saMonthNames[i]) + "\"" );
				calString.Append( ",");
			}
			calString.Append( "\" \"");
			calString.Append( " ); ");

			return calString.ToString();
				
				   
		}


		private string StartJavaScript()
		{
			return "<script language=\"javascript\">";
		}

		private string EndJavaScript()
		{
			return "</script>";
		}

		private string ScheduleOnceRdBtnClicked()
		{
			StringBuilder script = new StringBuilder();
			script.Append( " function ScheduleOnceRdBtnClicked( aForm ) ");
			script.Append( " { " );
			script.Append( " } " );

			return script.ToString();
		}

		protected override void OnPreRender( EventArgs e )
		{
			AcTimeZone zone = AcTimeZoneManager.GetUserTimeZone( Page ) ;
			if ( zone.IsDaylightSavingTime( AcTimeZoneManager.Convert( Page, DateTime.Now, AcTimeZoneManager.From.AppServer, AcTimeZoneManager.To.User ) ) )
			{
				m_timeOffset = zone.DstOffsetInMinutes.ToString( ) ;
			}
			else
			{
				m_timeOffset = zone.OffsetInMinutes.ToString( ) ;
			}

	
			ScheduleNowRdBtn.Attributes.Add( "OnClick", "javascript:ScheduleNowRdBtnClicked(" + SchedNowRdBtnClikedArgs() + ");");
			ScheduleOnceRdBtn.Attributes.Add( "OnClick", "javascript:ScheduleOnceRdBtnClicked(" + SchedOnceRdBtnClikedArgs() + ");");
			ScheduleRecurringRdBtn.Attributes.Add( "OnClick", "javascript:ScheduleRecurringRdBtnClicked(" + 
																SchedRecurringRdBtnClikedArgs( ) + ");");
			RecurringStartChkBox.Attributes.Add( "OnClick", "javascript:RecurringStartClicked(" +
															RecurringStartChkBoxClickedArgs( ) + ");");
			useEndDateTime.Attributes.Add( "OnClick", "javascript:RecurringEndClicked(" +
														RecurringEndClickedArgs( ) + ");");

			// Various calendars
			CalOnceDateLink.Attributes.Add( "OnClick", "javascript:GetCalendar(" +
														ActuUtil.GetCalendarArgs( ScheduleOnceTxt.ClientID , CalOnceDateLink.ClientID) + ");");

			StartDateLink.Attributes.Add( "OnClick", "javascript:GetCalendar(" +
										   ActuUtil.GetCalendarArgs(StartDateTxt.ClientID, StartDateLink.ClientID ) + ");");

			endDateLink.Attributes.Add( "OnClick", "javascript:GetCalendar(" +
										ActuUtil.GetCalendarArgs(endDateTxt.ClientID ,endDateLink.ClientID  ) + ");");

			// If the Link to switch between the immediate and scheduled jobs are clicked 
			// then the Url parameter for the requesttype is not to be considered

			if ( IsPostBack )
			{
				Control Tab = Parent.Parent.Parent;
				Type TabType = Tab.GetType();
				PropertyInfo JobType = TabType.GetProperty("JobType");

				if ( JobType != null )
				{
					m_requestType = (string) JobType.GetValue( Tab, null );
				}
			}

		}

		
		private void RegisterEventHandlerScript()
		{
			string Script = "<script language=\"JavaScript\" src={0}></script>";
			string ScriptSrc = "../js/allscripts.js";
			Page.RegisterClientScriptBlock( "AllScripts", String.Format( Script, ScriptSrc ));

			string ScheduleScript = "<script language=\"JavaScript\" src={0}></script>";
			string ScheduleScriptSrc = "../js/schedule.js";
			Page.RegisterClientScriptBlock( "CalendarScript", String.Format( ScheduleScript, ScheduleScriptSrc ));
		}

		protected string SchedNowRdBtnClikedArgs()
		{

			StringBuilder SchedNowRdBtnClickedArgs = new StringBuilder();
	
			SchedNowRdBtnClickedArgs.Append(   "\"" + ScheduleOnceTxt.ClientID +"\"," );
			SchedNowRdBtnClickedArgs.Append(  "\"" + OnceTimeTxt.UniqueID + "\","); 
			SchedNowRdBtnClickedArgs.Append(  "\"" + WeekDaysList.UniqueID + "\"," );
			SchedNowRdBtnClickedArgs.Append(  "\"" + RecurringTimeTxt.UniqueID + "\"," );
			SchedNowRdBtnClickedArgs.Append(  "\"" + RecurringStartChkBox.UniqueID + "\"," );
			SchedNowRdBtnClickedArgs.Append(  "\"" + StartDateTxt.UniqueID + "\"," );
			SchedNowRdBtnClickedArgs.Append(  "\"" + useEndDateTime.UniqueID + "\"," );
			SchedNowRdBtnClickedArgs.Append(  "\"" + endDateTxt.UniqueID + "\","   );
			SchedNowRdBtnClickedArgs.Append(  "\"" + scheduleTypeField.UniqueID + "\","   );

			SchedNowRdBtnClickedArgs.Append(  "\"" + CalOnceDateLink.ClientID + "\","   );
			SchedNowRdBtnClickedArgs.Append(  "\"" + StartDateLink.ClientID + "\","   );
			SchedNowRdBtnClickedArgs.Append(  "\"" + endDateLink.ClientID + "\","   );
			SchedNowRdBtnClickedArgs.Append(  m_timeOffset );

			return SchedNowRdBtnClickedArgs.ToString();
		
		}

		protected string SchedOnceRdBtnClikedArgs(  )
		{
			

			string DateFormat = m_currentCulture.DateTimeFormat.ShortDatePattern;
			string TimeFormat = m_currentCulture.DateTimeFormat.ShortTimePattern;
		
			
			StringBuilder SchedOnceRdBtnClickedArgs = new StringBuilder();
	
			SchedOnceRdBtnClickedArgs.Append(   "\"" + ScheduleOnceTxt.ClientID +"\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + OnceTimeTxt.UniqueID + "\","); 
			SchedOnceRdBtnClickedArgs.Append(  "\"" + DateFormat  + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + TimeFormat + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + ScheduleRecurringRdBtn.UniqueID + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + WeekDaysList.UniqueID + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + RecurringTimeTxt.UniqueID + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + RecurringStartChkBox.UniqueID + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + StartDateTxt.UniqueID + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + useEndDateTime.UniqueID + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + endDateTxt.UniqueID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + scheduleTypeField.UniqueID + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + m_localizedAMString + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + m_localizedPMString + "\"," );

			SchedOnceRdBtnClickedArgs.Append(  "\"" + CalOnceDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + StartDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + endDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(   m_timeOffset );

			
			return SchedOnceRdBtnClickedArgs.ToString();
		
		}


		protected string SchedRecurringRdBtnClikedArgs(  )
		{
			

			string DateFormat = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
			string TimeFormat = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern;

			//TODO, how to decie the Time Patter, so that the
			// client side java script will format in the corredct format with Am/Pm
			
			StringBuilder SchedOnceRdBtnClickedArgs = new StringBuilder();
			SchedOnceRdBtnClickedArgs.Append( "\"" + WeekDaysList.UniqueID + "\"," );
			SchedOnceRdBtnClickedArgs.Append( "\"" + RecurringTimeTxt.UniqueID + "\",");
			SchedOnceRdBtnClickedArgs.Append( "\"" + TimeFormat + "\",");

			SchedOnceRdBtnClickedArgs.Append( "\"" + ScheduleOnceTxt.UniqueID + "\","); 
			SchedOnceRdBtnClickedArgs.Append( "\"" + OnceTimeTxt.UniqueID + "\","); 
			SchedOnceRdBtnClickedArgs.Append( "\"" + RecurringStartChkBox.UniqueID + "\",");
			SchedOnceRdBtnClickedArgs.Append( "\"" + StartDateTxt.UniqueID + "\",");
			SchedOnceRdBtnClickedArgs.Append( "\"" + useEndDateTime.UniqueID + "\",");
			SchedOnceRdBtnClickedArgs.Append( "\"" + endDateTxt.UniqueID + "\",");
			SchedOnceRdBtnClickedArgs.Append( "\"" + scheduleTypeField.UniqueID  + "\",");
			SchedOnceRdBtnClickedArgs.Append(  "\"" + m_localizedAMString + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + m_localizedPMString + "\"," );

			SchedOnceRdBtnClickedArgs.Append(  "\"" + CalOnceDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + StartDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + endDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  m_timeOffset   );

			return SchedOnceRdBtnClickedArgs.ToString();
		
		}

		protected string RecurringStartChkBoxClickedArgs( )
		{
			string DateFormat = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
			string TimeFormat = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern;
			StringBuilder SchedOnceRdBtnClickedArgs = new StringBuilder();
			SchedOnceRdBtnClickedArgs.Append( "\"" + StartDateTxt.UniqueID + "\",");
			SchedOnceRdBtnClickedArgs.Append( "\"" + DateFormat  + "\"," );

			SchedOnceRdBtnClickedArgs.Append(  "\"" + RecurringStartChkBox.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + CalOnceDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + StartDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + endDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  m_timeOffset   );


			return SchedOnceRdBtnClickedArgs.ToString();
		}

		protected string RecurringEndClickedArgs(  ) 
		{

			string DateFormat = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern;
			string TimeFormat = Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern;
			StringBuilder SchedOnceRdBtnClickedArgs = new StringBuilder();
			SchedOnceRdBtnClickedArgs.Append( "\"" + endDateTxt.UniqueID + "\",");
			SchedOnceRdBtnClickedArgs.Append( "\"" + DateFormat  + "\"," );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + useEndDateTime.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + CalOnceDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + StartDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  "\"" + endDateLink.ClientID + "\","   );
			SchedOnceRdBtnClickedArgs.Append(  m_timeOffset   );


			return SchedOnceRdBtnClickedArgs.ToString();

		}

		


		private void GetScheduleType()
		{
			
			ScheduleType = AcParams.Get( Page, AcParams.Name.__scheduleType, "immediate" );

			if ( ScheduleType == saScheduleTypes[ (int) eScheduleType.SCHEDTYPE_IMMEDIATE] )
			{
				ScheduleNowRdBtn.Checked = true;
				iScheduleType = (int)eScheduleType.SCHEDTYPE_IMMEDIATE;

				// Disable Some of the other fields

				ScheduleOnceTxt.Enabled = false;
				OnceTimeTxt.Enabled = false;
				WeekDaysList.Enabled = false;
				RecurringTimeTxt.Enabled = false;
				RecurringStartChkBox.Disabled = true;
				StartDateTxt.Enabled = false;
				useEndDateTime.Disabled = true;
				endDateTxt.Enabled = false;

				CalOnceDateLink.Enabled = false;
				StartDateLink.Enabled = false;
				endDateLink.Enabled  = false;
	

			}
			else if ( ScheduleType == saScheduleTypes[ (int) eScheduleType.SCHEDTYPE_ONCE] )
			{
				ScheduleOnceRdBtn.Checked = true;
				iScheduleType = (int)eScheduleType.SCHEDTYPE_ONCE;

				string sOnceDate = AcParams.Get( Page, AcParams.Name.__onceDate, "");
				string sOnceTime = AcParams.Get( Page, AcParams.Name.__onceTime, "" );

				// Populate the dates in the corresponding text boxes

				if ( OnceDate != "" )
				{
					OnceDate = OnceDate;
				}

				if ( OnceTime != "" )
				{
					OnceTime = OnceTime;
				}

				ScheduleOnceTxt.Enabled = true;
				OnceTimeTxt.Enabled = true;
				WeekDaysList.Enabled = false;
				RecurringTimeTxt.Enabled = false;
				RecurringStartChkBox.Disabled = true;
				StartDateTxt.Enabled = false;
				useEndDateTime.Disabled = true;
				endDateTxt.Enabled = false;
			}
			else if ( ScheduleType == saScheduleTypes[ (int) eScheduleType.SCHEDTYPE_RECURRING] )
			{
				ScheduleRecurringRdBtn.Checked = true;

				ScheduleOnceTxt.Enabled = false;
				OnceTimeTxt.Enabled = false;
				WeekDaysList.Enabled = true;
				RecurringTimeTxt.Enabled = true;
				RecurringStartChkBox.Disabled = true;
				StartDateTxt.Enabled = false;
				useEndDateTime.Disabled = true;
				endDateTxt.Enabled = false;


				iScheduleType = (int)eScheduleType.SCHEDTYPE_RECURRING;

				RecurringTime = AcParams.Get( Page, AcParams.Name.__recurringTime );
				string sRecurringDay =  AcParams.Get( Page, AcParams.Name.__recurringDay );

				if (sRecurringDay == null) // BACKWARD COMPATIBILITY
				{
					sRecurringDay = AcParams.Get( Page, AcParams.Name.__schedulePeriod );
					if (sRecurringDay != null) // MAP OLD VALUE TO NEW i18n VALUE
					{
						sRecurringDay = sRecurringDay.Trim();
		
						for (int i = 0; i < saRecurringDaysOld.Length; i++)
						{
							if ( String.Compare( sRecurringDay, saRecurringDaysOld[i]) == 0 )
							{
								sRecurringDay = saRecurringDays[i];
								WeekDaysList.SelectedIndex = i;
								SchedulePeriod = sRecurringDay;
							}
						}
					}
				
				}

			}

			
			

		}

		// FUnction to be called by the main page to 
		// TO DO
		// The logic of converting the various dates to the XML formats is
		// already in the JobUtils.cs
		// This code can be removed from here, after testing
		public void GetScheduleDetails( AcUrlBuilder link)
		{

			ScheduleType = scheduleTypeField.Value;
			link.AddKeyValue( AcParams.Name.__scheduleType, sScheduleType );

			//  Priority
			link.AddKeyValue( AcParams.Name.__priority, Priority );
					
			if ( ScheduleType == "immediate" )
				return; 

			if ( ScheduleOnceTxt.Text != null &&
				ScheduleOnceTxt.Text.Trim() != String.Empty )
			{
				DateTime onceDate = DateTime.Parse( ScheduleOnceTxt.Text, m_currentCulture);
				// Do not change Case Sensiive patterns
				link.AddKeyValue( AcParams.Name.__onceDate, onceDate.ToString( "yyyy-MM-dd" ) );
			}

			if ( OnceTimeTxt.Text  != null &&
				OnceTimeTxt.Text.Trim() != String.Empty )
			{
				DateTime onceTime = DateTime.Parse( OnceTimeTxt.Text , m_currentCulture );
				link.AddKeyValue( AcParams.Name.__onceTime, onceTime.ToString( "HH:mm:ss" ) );
			}

			if ( RecurringTimeTxt.Text != null &&
				RecurringTimeTxt.Text.Trim() != String.Empty )
			{
				DateTime recurringTime = DateTime.Parse( RecurringTimeTxt.Text,
															  m_currentCulture );
				link.AddKeyValue( AcParams.Name.__recurringTime, recurringTime.ToString("HH:mm:ss") );
			}

				
			link.AddKeyValue( AcParams.Name.__recurringDay, SchedulePeriod );

			DateTime startDate;
			if ( StartDateTxt.Text != null &&
				StartDateTxt.Text.Trim() != String.Empty )
			{
				startDate = DateTime.Parse( StartDateTxt.Text, m_currentCulture );
			}
			else
			{
				startDate = AcTimeZoneManager.Convert(	Page,
														DateTime.Now,
														AcTimeZoneManager.From.AppServer,
														AcTimeZoneManager.To.User );
			}

			link.AddKeyValue( AcParams.Name.__startdate, startDate.ToString("yyyy-MM-dd") );
		

			if( endDateTxt.Text != null &&
				endDateTxt.Text.Trim() != String.Empty )
			{
				DateTime  recurringEndDate = DateTime.Parse( endDateTxt.Text, m_currentCulture);
				link.AddKeyValue( AcParams.Name.__enddate, recurringEndDate.ToString("yyyy-MM-dd" ) );
			}

			

		}


		// Used to restore the state of the scheduled tab
		// during a postback

		private void UpdateScheduleControls()
		{
			if ( ! IsPostBack )
				return;

			if ( scheduleTypeField.Value == "immediate" )
			{
				//Disbale all  other fields

				// Once
				ScheduleOnceTxt.Enabled = false;
				OnceTimeTxt.Enabled  = false;
				CalOnceDateLink.Enabled = false;

				// Recurring

				WeekDaysList.Enabled = false;
				RecurringTimeTxt.Enabled  = false;

				RecurringStartChkBox.Disabled = true;
				StartDateTxt.Enabled = false;
				StartDateLink.Enabled  = false;

				useEndDateTime.Disabled  = true;
				endDateTxt.Enabled = false;
				endDateLink.Enabled = false;

			}
			else if ( scheduleTypeField.Value == "once" )
			{

				// Once
				ScheduleOnceTxt.Enabled = true;
				OnceTimeTxt.Enabled  = true;
				CalOnceDateLink.Enabled = true;

				// Recurring

				WeekDaysList.Enabled = false;
				RecurringTimeTxt.Enabled  = false;

				RecurringStartChkBox.Disabled = true;
				StartDateTxt.Enabled = false;
				StartDateLink.Enabled = false;

				useEndDateTime.Disabled  = true;
				endDateTxt.Enabled = false;
				endDateLink.Enabled = false;

			}
			else if ( scheduleTypeField.Value == "Recurring")
			{
				ScheduleOnceTxt.Enabled = false;
				OnceTimeTxt.Enabled  = false;
				CalOnceDateLink.Enabled  = false;

				// Recurring

				WeekDaysList.Enabled = true;
				RecurringTimeTxt.Enabled  = true;
				RecurringStartChkBox.Disabled = false;
				useEndDateTime.Disabled  = false;

				StartDateTxt.Enabled = RecurringStartChkBox.Checked ;
				StartDateLink.Enabled = RecurringStartChkBox.Checked;

				
				endDateTxt.Enabled = useEndDateTime.Checked;
				endDateLink.Enabled = useEndDateTime.Checked;

		
			}

		}

			
		// Called by the main Tab Control to return a List Of fields
		// to be validated and also the messages which have to be displayed 
		// if the validation fails
		public string GetValidationFields()
		{

			string missingJobName = AcResourceManager.GetString( "ERRMSG_MISSING_JOB_NAME", Session );
			string invalidOnceDateMsg = AcResourceManager.GetString( "ERRMSG_INVALID_ONCE_DATE", Session );
			string invalidOnceTimeMsg = AcResourceManager.GetString( "ERRMSG_INVALID_ONCE_TIME", Session );
			string invalidRecurringDay  = AcResourceManager.GetString( "ERRMSG_INVALID_RECUR_DAY", Session );
			string invalidRecurringTime = AcResourceManager.GetString( "ERRMSG_INVALID_RECURRING_TIME", Session );
			string invalidStartDate = AcResourceManager.GetString( "ERRMSG_INVALID_START_DATE", Session );
			string invalidEndDate = AcResourceManager.GetString( "ERRMSG_INVALID_END_DATE", Session );
			string invalidPriorityValue = AcResourceManager.GetString( "ERRMSG_INVALID_PRIORITY_VALUE", Session ); 
			string invalidPriorityRange = AcResourceManager.GetString( "ERRMSG_INVALID_PRIORITY_RANGE", Session ); 

			StringBuilder args = new StringBuilder();

			ActuUtil.AddJsArgument( args, JobNameTxt.ClientID );
			ActuUtil.AddJsArgument( args, ScheduleOnceTxt.ClientID );
			ActuUtil.AddJsArgument( args, OnceTimeTxt.ClientID );
			ActuUtil.AddJsArgument( args, RecurringTimeTxt.ClientID );
			ActuUtil.AddJsArgument( args, RecurringStartChkBox.ClientID );
			ActuUtil.AddJsArgument( args, StartDateTxt.ClientID );
			ActuUtil.AddJsArgument( args, useEndDateTime.ClientID );
			ActuUtil.AddJsArgument( args, endDateTxt.ClientID );
			ActuUtil.AddJsArgument( args, m_shortDatePattern);
			ActuUtil.AddJsArgument( args, m_shortTimePattern );
			ActuUtil.AddJsArgument( args, scheduleTypeField.ClientID );
			ActuUtil.AddJsArgument( args, priorityRadioList.ClientID );
			ActuUtil.AddJsArgument( args, priorityTxt.ClientID );
			ActuUtil.AddJsArgument( args, maxJobPriorityHidden.ClientID );
			ActuUtil.AddJsArgument( args, missingJobName );
			ActuUtil.AddJsArgument( args, invalidOnceDateMsg );
			ActuUtil.AddJsArgument( args, invalidOnceTimeMsg );
			ActuUtil.AddJsArgument( args, invalidRecurringTime ) ;
			ActuUtil.AddJsArgument( args, invalidStartDate ) ;
			ActuUtil.AddJsArgument( args, invalidEndDate );
			ActuUtil.AddJsArgument( args, invalidPriorityValue ) ;
			ActuUtil.AddJsArgument( args, invalidPriorityRange, true );
			return args.ToString();
		
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion
		
	}
}


