namespace activeportal.usercontrols.newrequest
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Text;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes;
	using activeportal.classes.api;
	using activeportal.classes.api.proxy;
	using actuate_i18n;
	using actuate_i18n.classes;
	using System.Collections;
	using System.Collections.Specialized;
	using System.Globalization;
	using System.Threading;
	

	/// <summary>
	///		Summary description for parameters.
	/// </summary>
	public abstract class parameters : System.Web.UI.UserControl
	{
		protected HtmlTableRow  AcRowFilterTitle;
		protected HtmlTableRow InsertRow;
		protected HtmlTableRow BtnRowTop;
		protected HtmlTableRow BtnRowBottom;
		protected Button AcBtnOkTop;
		protected Button AcBtnOkBottom;
		protected Button AcBtnCancelTop;
		protected Button AcBtnCancelBottom;
		protected Literal AcLtrlTblEditor;
		protected Label AcLblTblParamHeading;
		protected int m_tableRowNumber;
		private FieldValue[]   m_editedRow;
		private Hashtable m_promptStructParamTable = null;
		private Hashtable m_structParamInitValue = null; // Used in Quries with struct param


		
		protected Table ParamTable;
		protected HtmlTable MainTable;
		protected HtmlInputHidden EditableListParamValues;
		protected HtmlTable AcTblInsertedRow;

		private Unit FILTER_VALUE_WIDTH = new Unit( 20, UnitType.Percentage );
		private Unit FILTER_NAME_WIDTH = new Unit( 10, UnitType.Percentage );
		private Unit FILTER_TYPE_WIDTH = new Unit( 10, UnitType.Percentage );
		private Unit FILTER_EMPTY_WIDTH = new Unit( 1, UnitType.Percentage );
		private Unit FILTER_CHECKBOX_WIDTH = new Unit( 1, UnitType.Percentage );
		private Unit FILTER_TEXTBOX_WIDTH = new Unit ( 45, UnitType.Ex );
		private Unit FILTER_DATE_WIDTH  = new Unit( 24, UnitType.Ex );
		private Unit FILTER_TIME_WIDTH = new Unit( 15, UnitType.Ex );
		
		protected ArrayList m_pwdBoxList = new ArrayList( );
		// TODO change this to use a parameter webcontrol instead of relying on cells.

		private string m_Item;

		public string Item
		{
			get
			{
				return m_Item;
			}

			set
			{
				m_Item = value;
			}
		}

		private string m_ItemTagName;
		public string ItemTagName
		{
			get
			{
				return m_ItemTagName ;
			}

			set
			{
				m_ItemTagName = value;
			}
		}

		private ReportParameters m_reportParameters;
		public ReportParameters Parameters
		{
			get
			{
				return m_reportParameters;
			}

			set
			{
				m_reportParameters = value;
			}
		}

		private bool m_usedForActuateQuery;
		public bool UsedForActuateQuery
		{
			get
			{
				return m_usedForActuateQuery ;
			}

			set
			{
				m_usedForActuateQuery = value;
			}
		}

		private ParameterDefinition[] m_parameterList;
		public ParameterDefinition[] ParameterList
		{
			get
			{
				return m_parameterList ;
			}

			set
			{
				m_parameterList = value;
			}
		}

		private ArrayList m_paramGroupList;
		public ArrayList ParamGroupList
		{
			get
			{
				return m_paramGroupList;
			}
		}

		private bool m_isWizard = false;
		public bool IsWizard
		{
			set { m_isWizard = value; }
			get { return m_isWizard; }
		}

		private Hashtable m_paramDefTable;

		 // Stores the Intial Set of Param Values as  returned by the server

		private Hashtable m_initParamValueTable; 
		
		private bool m_structureParameterEditor  = false;
		public bool StructureParameterEditor
		{
			get{ return m_structureParameterEditor;}
			set{ m_structureParameterEditor = value; }
		}
											

		private bool m_tableParameterEditor  = false;
		public bool TableParameterEditor
		{
			get{ return m_tableParameterEditor;}
			set{ m_tableParameterEditor = value; }
		}


		//private bool m_TableParamInsertMode = false;
		//private bool m_TableParamResetMode = false;

		private string m_structParamName = null;
		private string m_tableParamName = null ; // If used as a Editor for a Table Parameter then
												 // the name of the Table Parameter
		private string m_tableParamDisplayMode;
		bool m_displayStructParams = false;

		//private int m_tableParamIndex = 0;		 //  The Index of the Table Param Value which
												 // 


		private void DisplayNoParamsMsg()
		{
			if ( UsedForActuateQuery )
			{
				this.Visible = false;
			}
			else
			{
				// Display the message that no params exist for the report
				TableRow row = new TableRow();

				TableCell cell  = new TableCell();
				cell.ColumnSpan = 3;
				cell.Wrap = false;

				Label lblNoParams = new Label();
				lblNoParams.CssClass = "label";
				lblNoParams.Text = "&nbsp;" + ActuUtil.jsEncode( AcResourceManager.GetString( "MSG_NO_PARAMS", Session ));

				cell.Controls.Add( lblNoParams );
				row.Cells.Add( cell );
				ParamTable.Rows.Add( row );
			}
			
		}


		private void DisplayParametersAndGroups( ParameterDefinition[] paramList, ArrayList groupList )
		{
			DisplayParametersAndGroups( paramList, groupList, false  );
		}


		// NOTE
		// Useful Info for gathering Parameter Values from the Table
		//  There are three type of Rows
		// 1) Searator Rows - Indictaed by the character 'S' in the first cell
		// 2) Cells which show the Parameter Group - Indictaed by a 'G' in the first cell
		// 3) Cells which show the parameter Values - Indicated by a value 'P'/'ST', 'TA' in the first cell
		//
		// For the Rows which indicate the parameter Values the Following are the cells within
		// 1) Group Cell
		// cell[0] = "G"
		// cell[1] = GRoup Name
		// cell[2] =  Prompt Change to Parameter - IN case of Struct Params
		//2) Parameter Cell
		// cell[0] = "P"( For Normal Parameter )    - Invisible
		//           "ST" - Structure Parameters
		//			 "TA" - Table Parameters
		// cell[1] = Parameter Name   - Invisible
		// cell[2] = Paramete Group Name - Invisible
		// cell[3] = Parameter Data Type - Invisible
		// cell[4] = Display Name
		// cell[5] = Parameter Values ( in controls )
		// cell[6] = Empty cell or the Link for PopUp calendar
		// cell[7] = Formatted Type information
		// cell[8] = Prompt for change checkbox



		private void DisplayParametersAndGroups( ParameterDefinition[] paramList, ArrayList groupList, bool StructParam  )
		{
			bool bAnyParametersPresented = false;
			m_displayStructParams = StructParam;

			for ( int i = 0; i < groupList.Count ; i++ )
			{
				string sGroup = (string) groupList[i];


				ArrayList paramForGroup = JobUtil.GetParametersForGroup( sGroup, paramList );

				// In case this is being used as a Table Parameter Editor then we
				// simulate that they belong to the same group
				if ( m_tableParameterEditor )
				{
					AddTableParamsToGroup( paramForGroup );
				}

				bool bAnyParametersForGroup = true;


				ParamTable.EnableViewState  = true;
				
				if (  bAnyParametersForGroup )
				{
					// Display the Group Name
					// Looks like For the Actuate Query  we do not have to display the
					// Parameter Group Names


					// In order to be able to collect the param information
					// needed for submitting a job, we need to distinguish between
					// the row containing the Group Name and those containing the Param Values

					if ( (sGroup != null &&  ! m_tableParameterEditor) ||
						 ( StructParam) ) // Display the Grop name ( struct param name )
					{
						TableRow groupRow = new TableRow();
						groupRow.EnableViewState = true;

						// Dummy Cell to denote that this is a row containing a Parameter Group
						TableCell paramTypeCell = new TableCell();
						paramTypeCell.Visible = false;
						paramTypeCell.Text = "G";
						groupRow.Cells.Add( paramTypeCell );


						TableCell groupCell = new TableCell();
						groupCell.EnableViewState = true;
						//if ( !StructParam )
							groupCell.ColumnSpan = 3;
						//else
						//	groupCell.ColumnSpan = 4;

						Label lblGroup = new Label();
						lblGroup.EnableViewState = true;
						lblGroup.CssClass = "boldlabel";
						lblGroup.Text = sGroup;

						groupCell.Controls.Add( lblGroup );
						groupRow.Cells.Add( groupCell );

						// If Struct Param Display the type next to the Param Name
						if ( StructParam )
						{
							TableCell DataTypeCell = new TableCell();
							DataTypeCell.Text = "(" + DataType.Structure.ToString() + ")";
							groupRow.Cells.Add( DataTypeCell );
						}
						
						// In case of  Representing Struct Params for SAP. we display the
						// Allow to change check box , only once for the entire group
						if ( m_usedForActuateQuery && StructParam )
						{
							PopulatePromptParamForStruct( sGroup, groupRow );
						}


						groupRow.Width = new Unit( 100, UnitType.Percentage );
						ParamTable.Rows.Add( groupRow );
					}

					// Iterate through parameters for each group

					for (int j = 0; j < paramForGroup.Count; j++)
					{
						ParameterDefinition parameterDef = ( ParameterDefinition )paramForGroup[j];

						

						bool AllowToChange = true;
						bool displayParameter = true;

						if  ( UsedForActuateQuery )
						{
							AllowToChange =  AllowedToChangeParam (parameterDef ) ;
							if ( ! AllowToChange )
							{
								if ( ! m_isWizard )
								{
									// For cases such as Execute / Submit a query
									// if the parameter cannot be changed, then 
									// Do not display it
									displayParameter = false;
								}
							}
						}



						if ( UsedForActuateQuery && !m_isWizard )
						{
							if ( displayParameter && !parameterDef.IsHidden  )
							{
								bAnyParametersPresented = true;
							}
						}
						else
						{
							// If at least one parameter is to be Displayed set the flag bAnyParametersPresented

							if ( !bAnyParametersPresented )
							{
								if ( !parameterDef.IsHidden  )
								{
									bAnyParametersPresented = true;
								}
							}
							
						}
										
						String sDisplayName =  parameterDef.DisplayName;

						if ( sDisplayName == null || sDisplayName.Trim() == String.Empty )
						{
							sDisplayName = JobUtil.StripQualifiedParameterName( parameterDef.Name );
						}

						TableRow  dispNameRow = new TableRow();

						// For Actuate Quer Do not display params which cannot be 
						// changed, if it is not a wizard
						if  ( UsedForActuateQuery && ! m_isWizard )
						{
							dispNameRow.Visible = displayParameter;
						}


					
							dispNameRow.Visible  = dispNameRow.Visible && !parameterDef.IsHidden;
						
						

						// Dummy Cell to indicate that this is a Cell containing a Report Parameter
						// And Not a Param Group
						TableCell aCell = new TableCell();
						aCell.Visible = false;




						if ( parameterDef.DataType == DataType.Table )
						{
							aCell.Text = "TA";
						}
						else if ( !StructParam )
						{
							aCell.Text = "P";
						}
						else
						{
							aCell.Text = "ST";
						}


						/*
						if ( !StructParam && ( parameterDef.DataType != DataType.Table) )
						{
							aCell.Text = "P";
						}
						else 
						{
							aCell.Text = "ST";
						} */

						dispNameRow.Cells.Add( aCell );


						// Param Name - Invisible
						TableCell paramNameCell = new TableCell();
						paramNameCell.Visible = false;
						paramNameCell.Text = parameterDef.Name;
						dispNameRow.Cells.Add( paramNameCell );

						// Param Group - Invisible
						TableCell paramGroupCell = new TableCell();
						paramGroupCell.Visible  = false;
						paramGroupCell.Text = parameterDef.Group;
						dispNameRow.Cells.Add( paramGroupCell );

						// Param Datatype
						TableCell paramDataTypeCell = new TableCell();
						paramDataTypeCell.Visible  = false;
						paramDataTypeCell.Text = parameterDef.DataType.ToString();
						dispNameRow.Cells.Add( paramDataTypeCell );

						
						ParamTable.Rows.Add( dispNameRow );
						dispNameRow.EnableViewState = true;

					
						TableCell dispNameCell = new TableCell();
						dispNameCell.EnableViewState = true;

						dispNameCell.Width = FILTER_NAME_WIDTH; 
						dispNameCell.Wrap = true;
						dispNameCell.VerticalAlign = VerticalAlign.Middle;
						

						Label lblDispName = new Label();
						lblDispName.EnableViewState  = true;

						lblDispName.CssClass = "label";
						lblDispName.Text =  sDisplayName;
						

						dispNameCell.Controls.Add( lblDispName );
						dispNameRow.Cells.Add( dispNameCell );

						PopulateContentCell( parameterDef , dispNameRow);
						PopulateTypeInfo( parameterDef, dispNameRow );
						
						// Display the allow to change check box only if used
						// for Actuate Query
						if ( UsedForActuateQuery )
						{
							PopulateAllowChanges( parameterDef, dispNameRow , AllowToChange, StructParam);
						}

					}

					TableRow sepRow = new TableRow();
					sepRow.EnableViewState = true;

					TableCell dummyCell = new TableCell();
					dummyCell.Visible = false;
					dummyCell.Text = "S";

					TableCell sepCell = new TableCell();
					sepCell.EnableViewState = true;
						
					sepCell.ColumnSpan = 5;
					sepCell.Text = "<br>";
					sepRow.Cells.Add( sepCell );
					ParamTable.Rows.Add( sepRow );

					sepRow.Visible =  ( (groupList != null) && (i != groupList.Count -1 ));

			}

		}

			// IF NO PARAMETERS WERE PRESENTED BECAUSE THEY WERE HIDDEN -
			if (!bAnyParametersPresented) 
			{
				DisplayNoParamsMsg();
			}

		}

		
		public void PopulateParametersInContext()
		{
			TableRowCollection rows = ParamTable.Rows;

			ArrayList paramValueList = new ArrayList();

			for ( int i = 0; i < rows.Count ; i++ )
			{
				if ( rows[i].Cells[0].Text  == "P" ) 
				{
					ParameterValue parameter = new ParameterValue();

					string ParamDataType = rows[i].Cells[3].Text;

					if ( ParamDataType.ToLower() == DataType.Structure.ToString().ToLower() ||
						ParamDataType.ToLower() ==  DataType.Table.ToString().ToLower() )
					{
						continue;
					}
					
						 
					string paramterName = rows[i].Cells[1].Text;
					if ( paramterName != null )
					{
						parameter.Name = HttpUtility.HtmlDecode( paramterName );
					}

					// Depending on the type of the control, Call the appropriate property 
					// of the control to get the Parameter Value
					
					if ( rows[i].Cells.Count > 5 && 
						rows[i].Cells[5].Controls.Count > 0 )

					{
						parameter.Value =  GetParameterValue( rows[i].Cells[5] );
					}
					
					
					
					paramValueList.Add( parameter );
				}
			}


			// Now Get the Structure Parameters
			GetStructureParameterValues( paramValueList );

			// Get The Table type parameters
			GetTableParameterValues( paramValueList );
		

			if ( paramValueList.Count > 0 )
			{
				Session["ReportParameterList"] = paramValueList;
			}

		}

		
		

		private void Page_Load(object sender, System.EventArgs e)
		{
			// REVIEW alternative solution needed. Commented out for now
			/*if (((basePageTemplate)Page).SessionTimedOut )
			{
				return;
			}*/

			RegisterScripts();

			GetRequiredInputParameters();

			RenderControl();

			if ( m_tableParameterEditor && 
				m_tableParamDisplayMode.ToLower() == "edit" )
			{
				GetEditedRow();
			}

			GetParameterList();

			
			if ( (ParameterList == null) || ( ParameterList.Length == 0 ))
				DisplayNoParamsMsg();
			else
				DisplayParametersAndGroups( ParameterList, ParamGroupList );

		}

		protected override void OnPreRender( System.EventArgs e )
		{
			// Repopulate passwords
			// REVIEW alternative solution needed. Commented out for now
			/*if (((basePageTemplate)Page).SessionTimedOut )
			{
				return;
			}*/


			foreach ( TextBox txtbox in m_pwdBoxList )
			{
				txtbox.Attributes[ "value" ] = txtbox.Text;
			}
			ValidateDates();

		}

		private void PopulateContentCell( ParameterDefinition parameterDef , TableRow row)
		{
			// Depending on the type of the parameter 
			// Display the correct control

			// Find out the  List of available Values which
			// can be selected 
			
			string[] selectValueList = parameterDef.SelectValueList;

			if ( ( selectValueList != null && selectValueList.Length > 0) && ( parameterDef.ControlTypeSpecified ) )
			{
				switch ( parameterDef.ControlType )
				{
					case ParameterDefinitionControlType.ControlRadioButton:
						CreateRdBtn( selectValueList, row , parameterDef);
						break;

					case ParameterDefinitionControlType.ControlList:
						CreateList( selectValueList, row , parameterDef );
						break;

					case ParameterDefinitionControlType.ControlListAllowNew:
						CreateEditableList( selectValueList, row, parameterDef );
						break;

					case ParameterDefinitionControlType.ControlCheckBox:
						CreateCheckBox( selectValueList, row, parameterDef );
						break;

				}
			}
			else
			{
				// No Values to Select From
				// Following cases
				// 1) Text Box: For standard  string  datatypes
				// 1) Date - Display the calendar control
				// 2) Adhoc Parameters - Display the expression builder

				switch( parameterDef.DataType )
				{
					case DataType.Date:
						CreatePopUpCalendar( row, parameterDef );
						break;

					case DataType.String:
					{
						if ( parameterDef.IsAdHoc )
						{

							// Expression Builder
							CreateExpressionBuilder( row , parameterDef);
						}
						else
						{
							CreateTextField( row, parameterDef );
						}
						break;
					}
					case DataType.Structure:
					{
						DisplayStructParameter( row, parameterDef );
						break;
					}
					case DataType.Table:
					{
						DisplayTableParameter( row, parameterDef );
						break;
					}
					default:
					{
						CreateTextField( row, parameterDef );
						break;
					}
				}
			}
			// Add a hidden field if the parameter is required for validation

			if ( parameterDef.IsRequired )
			{
				HtmlInputHidden hidden = new HtmlInputHidden( );
				hidden.Value = parameterDef.Name;
				row.Cells[ row.Cells.Count - 2 ].Controls.Add( hidden );
			}
		}

		// Creates a List of Radio buttons depending on the type of
		// parameter
		private void CreateRdBtn( string[] list, TableRow row, ParameterDefinition parameterDef )
		{
			TableCell cell  = new TableCell();
			cell.Width = FILTER_VALUE_WIDTH;
			cell.VerticalAlign = VerticalAlign.Top;
			cell.Wrap = false;

			RadioButtonList rdButtonList = new RadioButtonList();
			rdButtonList.RepeatLayout=RepeatLayout.Flow;
			rdButtonList.DataSource = list;
			rdButtonList.DataBind();

			int selectedIndex = 0;
			if ( ! IsPostBack )
			{
				string ParamInitValue = GetInitParamValue(parameterDef );
				if ( ParamInitValue != null )
				{
					ListItem defItem = new ListItem();
					defItem.Value = ParamInitValue ;
					selectedIndex = rdButtonList.Items.IndexOf( defItem );
					rdButtonList.SelectedIndex = selectedIndex;
				}
			}

			cell.Controls.Add( rdButtonList );
			row.Cells.Add( cell );

			CreateEmptyCell( row );

		}

		private void CreateList( string[] list, TableRow row, ParameterDefinition parameterDef )
		{
			TableCell cell  = new TableCell();
			cell.Width = FILTER_VALUE_WIDTH;
			DropDownList  dList = new DropDownList();
			dList.Width= FILTER_TEXTBOX_WIDTH;
			
			
			if ( parameterDef.DefaultValue == null || parameterDef.DefaultValue == "" )
			{
				ListItemCollection dataSource = new ListItemCollection();

				// Add a empty Item to the list
				ListItem emptyItem = new ListItem();
				string defEmptyValue = "";
				emptyItem.Text = defEmptyValue;
				emptyItem.Value = defEmptyValue;
				dataSource.Add( emptyItem );
				for ( int i=0; i < list.Length ; i++ )
				{
					ListItem item  = new ListItem();
					item.Text = list[i];
					item.Value = list[i];
					
					dataSource.Add( item );
				}
				dList.DataSource = dataSource;
				dList.DataBind();
				
			}
			else
			{
				dList.DataSource = list;
				dList.DataBind();
			}
			

			if ( ! IsPostBack )
			{
				int selectedIndex = 0;
				ListItem defItem = new ListItem();
				string ParamInitValue = GetInitParamValue( parameterDef );
				defItem.Value = ParamInitValue ;
				selectedIndex = dList.Items.IndexOf( defItem );
				dList.SelectedIndex = selectedIndex;
			}
			


			cell.Controls.Add( dList );
			row.Cells.Add( cell );

			CreateEmptyCell( row );


		}



		private void CreateEditableList( string[] list, TableRow row, ParameterDefinition parameterDef )
		{
			TableCell cell  = new TableCell();
			cell.Width = FILTER_VALUE_WIDTH;
			DropDownList  dList = new DropDownList();
			dList.Width= FILTER_TEXTBOX_WIDTH;
			dList.CssClass = "parameterDropdown";

			TextBox textBox = new TextBox( );
			textBox.Width = FILTER_TEXTBOX_WIDTH;
			textBox.CssClass = "parameterInputBox";
			
			
			ListItemCollection dataSource = new ListItemCollection();
			
			if ( ! IsPostBack )
			{
				textBox.Text = GetInitParamValue( parameterDef );
				ViewState[ parameterDef.Name ] = textBox.Text;
			}
			else
			{
				textBox.Text = (string) ViewState[ parameterDef.Name ];
			}
			
			for ( int i=0; i < list.Length ; i++ )
			{
				ListItem item  = new ListItem();
				item.Text = list[i];
				item.Value = list[i];
				
				dataSource.Add( item );
			}


			dList.DataTextField = "Text";
			dList.DataValueField = "Value";
			dList.DataSource = dataSource;
			dList.DataBind();

			cell.Controls.Add( dList );
			cell.Controls.Add( textBox );

		


			dList.SelectedIndex = -1;

			for ( int i = 0; i < dList.Items.Count; i++)
			{
				if (dList.Items[i].Text == textBox.Text )
				{
					dList.SelectedIndex = i;
					break;
				}
			}	
					
			

			row.Cells.Add( cell );

			dList.Attributes.Add("onchange", "javascript:updateInput('" + textBox.UniqueID  + "',this);");
			textBox.Attributes.Add("onblur", "javascript:updateDropdown('" + dList.UniqueID + "',this);");																		
			dList.Attributes.Add("onclick", "javascript:updateDropdownBySelf('" + textBox.UniqueID + "',this);");																		
																		

			CreateEmptyCell( row );

		}

		private void CreateCheckBox( string[] list, TableRow row, ParameterDefinition parameterDef )
		{
			TableCell cell  = new TableCell();
			cell.Width = FILTER_VALUE_WIDTH;
			CheckBox  chkbox = new CheckBox();
			

			string paramValue = GetInitParamValue( parameterDef );
			if ( paramValue == null ||
				 paramValue.ToLower() == "false" )
			{
				chkbox.Checked = false;
			}
			else
			{
				chkbox.Checked = true;
			}


			cell.Controls.Add( chkbox );
			row.Cells.Add( cell );

			CreateEmptyCell( row );

		}

		private void CreatePopUpCalendar( TableRow row, ParameterDefinition paramdef )
		{
			TableCell txtCell = new TableCell();
			txtCell.Width = FILTER_VALUE_WIDTH;

			// Add a text box to display the date
			TextBox DateTxt = new TextBox();
			DateTxt.Width = txtCell.Width;
			DateTxt.Width =  FILTER_DATE_WIDTH ;
			


			string paramvalue = GetInitParamValue( paramdef );


			// Split this into the Date and the Time part
			CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
			DateTime paramDate = DateTime.Now;
			bool ValidDate = false;
			string sDate = null;
			string sTime = null;

			

			if ( paramvalue != null &&
				paramvalue.Trim() != String.Empty )
			{
				ValidDate  = true;
			}

			DateTime dateTime = new DateTime();
			bool IsNull = false;
			if ( ValidDate )
			{

						
				// Special Handling for Null values
				// In case of Structure and Parameters the Text Null
				// has a special meaning, and it should be kept as is.
				if ( m_displayStructParams || m_tableParameterEditor )
				{
					IsNull = JobUtil.IsNullValue( paramvalue );
				}
				
				

				if ( IsNull )
				{
					DateTxt.Text = paramvalue;
				}
				else
				{

						
					// There does not seem to be any function which would tell if
					// the Date portion /Time portion is Valid.
					// If Any of the Portion is invalid the Parse Function seems to be Failing
					try
					{
					
						dateTime = DateTime.Parse( paramvalue, currentCulture);// DateTime.ParseExact( paramvalue, currentCulture.DateTimeFormat.LongTimePattern.ToString(), currentCulture );
						sDate = dateTime.ToString( currentCulture.DateTimeFormat.ShortDatePattern.ToString( ) ); 
						sTime = dateTime.ToString( currentCulture.DateTimeFormat.ShortTimePattern.ToString( ) );
					}

					catch
					{
						ValidDate = false;
					}
					DateTxt.Text = sDate; 
				}
				
			}	
		
		
			
			
			if ( m_displayStructParams )
			{
				DateTxt.ID = "ST " + m_structParamName + paramdef.Name.Replace( "::", "_") ;
			}
			else
			{
				DateTxt.ID = paramdef.Name.Replace( "::", "_") ;
			}
			txtCell.Controls.Add( DateTxt );
			row.Cells.Add( txtCell );

			txtCell.Controls.Add( new LiteralControl( "&nbsp;" ) ) ;

			// Add the Link to the calendar control
			
			HyperLink CalendarLink = new HyperLink();
			if ( m_displayStructParams )
			{
				CalendarLink.ID = "ST" + m_structParamName + paramdef.Name + "link";
			}
			else
			{
				CalendarLink.ID = paramdef.Name + "link";
			}
			CalendarLink.ImageUrl = "~/images/calendar.gif";
			
			CalendarLink.NavigateUrl = "javascript:;";

			
			txtCell.Controls.Add( CalendarLink ); 

			CalendarLink.Attributes.Add( "OnClick", 
				"javascript:GetCalendar(" + ActuUtil.GetCalendarArgs( DateTxt.ClientID ,CalendarLink.ClientID ) + ");");


			// Add the time
			
			txtCell.Controls.Add( new LiteralControl( "&nbsp;" ) );

			TextBox TimeTxt = new TextBox( );

			TimeTxt.Width =  FILTER_TIME_WIDTH;
			
			//if ( ValidDate )
			//{
			// Do not Display anything if there is No Time Component of the Date
			if ( ! IsNull )
			{
				if( sTime != null && sTime.Trim() != String.Empty )
				{
					try
					{
						// Try getting the Time part
						if ( dateTime.Hour > 0 || dateTime.Minute > 0 || dateTime.Second > 0 )
						{
							TimeTxt.Text = dateTime.ToString( currentCulture.DateTimeFormat.LongTimePattern.ToString());
						}
					}
					catch
					{
						ValidDate = false;
					}
				}
			}


			if ( paramdef.IsPassword )
			{
				DateTxt.TextMode = TextBoxMode.Password;
				TimeTxt.TextMode = TextBoxMode.Password;

				// To display the "*" for a password field

				m_pwdBoxList.Add( DateTxt );
				m_pwdBoxList.Add( TimeTxt );
			}

			txtCell.Controls.Add( TimeTxt );
			
			CreateEmptyCell( row );

		}

		private void PopulateTypeInfo( ParameterDefinition parameterDef,TableRow row )
		{
			TableCell paramTypeCell = new TableCell();
			paramTypeCell.Width = FILTER_TYPE_WIDTH ;
			paramTypeCell.Wrap = false;
			paramTypeCell.CssClass= "label";
								
			if ( ( parameterDef.SelectValueList == null || parameterDef.SelectValueList.Length == 0 )  )
			{
				if ( parameterDef.DataType != DataType.Date )
				{
					paramTypeCell.Text = JobUtil.GetParameterType( parameterDef.DataType.ToString(), parameterDef.ColumnType.ToString(),
						parameterDef.IsAdHoc, parameterDef.IsRequired, Session );
				}
				else
				{ 
					string typeString = "(";
					if ( parameterDef.IsRequired )
					{
						typeString +=  AcResourceManager.GetString( "PTYPE_REQUIRED", Session);
						typeString += ", ";
					}

					CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
					typeString +=  currentCulture.DateTimeFormat.LongTimePattern.ToString();
					paramTypeCell.Text = typeString + ")";
				}
			}
			else if ( parameterDef.ControlType == ParameterDefinitionControlType.ControlListAllowNew ||
					( parameterDef.ControlType == ParameterDefinitionControlType.ControlList &&
						( parameterDef.DefaultValue == null || parameterDef.DefaultValue == ""  )) )
			{
				string typeString = "";
				if ( parameterDef.IsRequired )
				{
					typeString += "(";
					typeString +=  AcResourceManager.GetString( "PTYPE_REQUIRED", Session);
					typeString += ")";
					paramTypeCell.Text = typeString;
				}
			}
			else
			{
				paramTypeCell.Text = "&nbsp;";
			}
			row.Cells.Add( paramTypeCell );
		}


		private void CreateExpressionBuilder( TableRow row, ParameterDefinition parameterDef )
		{
			TableCell exprCell = new TableCell();
			exprCell.Width = FILTER_VALUE_WIDTH;

			TextBox exprTxt = new TextBox();

			exprTxt.Width = FILTER_TEXTBOX_WIDTH;

			exprTxt.Text = GetInitParamValue( parameterDef );
			
		

			exprCell.Controls.Add( exprTxt );

			row.Cells.Add(  exprCell );
			
			TableCell btnCell = new TableCell();
			ImageButton expBtn = new ImageButton();
			expBtn.ImageUrl = "~/images/arrow_btn.gif";
			expBtn.Attributes["onmousedown"]= "javascript:enableClick(this);";
			expBtn.Attributes["onkeypress"] = "javascript:enableClick(false,event);";
			expBtn.Attributes["onclick"] = "javascript:activatePopup(this, '" + exprTxt.UniqueID + "'); return false;";
			expBtn.Attributes["onmouseout"] = "javascript:deactivatePopup();";
			btnCell.Controls.Add( expBtn );
			row.Cells.Add( btnCell );
			
			// Set password textmode after adding to the cell for viewstate persistence.

			if ( parameterDef.IsPasswordSpecified && parameterDef.IsPassword )
			{
				exprTxt.TextMode = TextBoxMode.Password;
				m_pwdBoxList.Add( exprTxt );
			}
		}

		private void CreateTextField( TableRow row, ParameterDefinition paramdef )
		{
			TableCell cell = new TableCell();
			cell.Width = FILTER_VALUE_WIDTH;

			TextBox valTxt = new TextBox();
			valTxt.Width = FILTER_TEXTBOX_WIDTH;



			valTxt.Text = GetInitParamValue ( paramdef );
			

			

			cell.Controls.Add( valTxt );
			row.Cells.Add( cell );

			// HideText parameter
			// Set password textmode after adding to the cell for viewstate persistence.

			if ( paramdef.IsPasswordSpecified && paramdef.IsPassword )
			{
				valTxt.TextMode = TextBoxMode.Password;
				m_pwdBoxList.Add( valTxt );
			
			}

			CreateEmptyCell( row );
		}

		private void CreateEmptyCell( TableRow row )
		{
			TableCell cell = new TableCell();
			cell.Width  = FILTER_EMPTY_WIDTH;
			cell.Text = "&nbsp;";
			row.Cells.Add( cell );
		}

		
		private void PopulateAllowChanges( ParameterDefinition parameterDef, TableRow row, bool AllowToChange, bool StructParam )
		{
			// The same page is used to also display the struct params
			// as a group. Although STruct Params are displayed as a group. each individual 
			// element cannot have a Prompt element. hence we Hide this for such elements
			if ( m_isWizard )
			{
				TableCell cell = new TableCell();
				cell.Width = FILTER_CHECKBOX_WIDTH;
				CheckBox chkBox = new CheckBox();
			

				chkBox.Checked = AllowToChange;

				cell.Controls.Add( chkBox );
				if ( StructParam )
				{
					cell.Visible = false;
				}
				row.Cells.Add( cell );
			}
		}



		private void PopulatePromptParamForStruct( string ParamName, TableRow row )
		{
			// The same page is used to also display the struct params
			// as a group. Although STruct Params are displayed as a group. each individual 
			// element cannot have a Prompt element. hence we Hide this for such elements
			if ( m_isWizard )
			{
				TableCell cell = new TableCell();
				cell.Width = FILTER_CHECKBOX_WIDTH;
				CheckBox chkBox = new CheckBox();
			

				chkBox.Checked = AllowedToChangeParam( ParamName );

				cell.Controls.Add( chkBox );
				row.Cells.Add( cell );
			}
		}

		private void CreatePromptParamTable()
		{
			m_promptStructParamTable = new Hashtable();
			TableRowCollection rows = ParamTable.Rows;
			if ( rows == null )
				return;

			for ( int i=0; i < rows.Count; i++ )
			{

				// Strcuture Parameters
				if ( rows[i].Cells.Count == 4 && rows[i].Cells[0].Text  == "G")
				{
					Label lblParamName = (Label)(rows[i].Cells[1].Controls[0]);
					string ParamName = lblParamName.Text;
					CheckBox chkPrompt = (CheckBox) rows[i].Cells[3].Controls[0];
					bool PromptParam = chkPrompt.Checked;
					m_promptStructParamTable[ ParamName ] = PromptParam;
				}
				else
				{

					// Table Parameters
					if ( rows[i].Cells.Count == 9 && rows[i].Cells[0].Text  == "TA" )
					{
						string ParamDataType = rows[i].Cells[3].Text;

						if ( ParamDataType == DataType.Table.ToString() )
						{
							string ParamName = rows[i].Cells[1].Text;
							bool promptParameter = ((CheckBox)(rows[i].Cells[8].Controls[0])).Checked;
							m_promptStructParamTable[ ParamName ] = promptParameter;
						}
					}
				}

			}
		}

		private void GetPromptParamValue( ParameterDefinition[] ParamDefList, ParameterValue[] ParamValueList )
		{
			if ( ParamDefList == null || ParamValueList == null )
				return;

			bool AllowToChangeParam = true;
			string ParamName = null;

			for ( int i=0; i < ParamDefList.Length; i++ )
			{
				ParameterDefinition ParamDef = ParamDefList[i];
				ParamName = ParamDef.Name;

				if ( ParamDef.DataType == DataType.Structure ||
					 ParamDef.DataType == DataType.Table )
				{
					AllowToChangeParam = (bool) m_promptStructParamTable[ParamName];

					
					for( int j=0; j< ParamValueList.Length; j++)
					{
						if ( ParamValueList[j].Name == ParamName )
						{
							ParamValueList[j].PromptParameter = AllowToChangeParam;
							ParamValueList[j].PromptParameterSpecified = true;
							break;
						}
					}
				
				}
			}

		}

		private void RegisterScripts()
		{
			
			string CalendarScriptKey = "CalendarScript";
			string CalendarScript = "<script language=\"JavaScript\" src={0}></script>";
			string CalendarScriptSrc = "../js/schedule.js";
			if ( ! Page.IsClientScriptBlockRegistered( CalendarScriptKey ))
			{
				Page.RegisterClientScriptBlock( CalendarScriptKey, String.Format( CalendarScript, CalendarScriptSrc ));
			}

		}

		private void GetParameterList()
		{
			if ( UsedForActuateQuery )
			{
				GetQuery();
			}
			else
			{
				GetReportParameters();
			}

		}

		private void GetReportParameters()
		{
			AcGetReportParameters reportParameters = null;

			if ( ! IsPostBack )
			{

				// The Same Control is being used to display the  normal parameters and also the
				// Table parameters. In case of a Table Parameter the Values have already been stored in the
				// Session. so we do not have to query the server for these
				if ( ! m_tableParameterEditor )
				{
					reportParameters = new AcGetReportParameters( Item, ItemTagName, Page );
					m_parameterList = reportParameters.paramList();


					// NOte:
					// Really not much choice here than to store it in the session
					// storing it in the view state requires that the response be serializable
               
					Session["Parameters"] = reportParameters.Response;
				}
				else
				{
					ParameterDefinition[] paramList = null;
					string UsedForQuery = AcParams.Get( Page, AcParams.Name.UsedForQuery, "false", AcParams.From.Url );
					if ( UsedForQuery.ToLower() == "false" )
					{
						GetReportParametersResponse reportParamsResp = ( GetReportParametersResponse ) Session["Parameters"];
						paramList = ( ParameterDefinition[] ) (reportParamsResp.ParameterList );
					}
					else
					{
						Query  query = (Query)Session[AcParams.Name.queryObject.ToString()];
						paramList = query.ParameterDefinitionList;
					}
					m_parameterList = ConvertTableParamToParamList( paramList );
				}

			}
			else
			{
				if (! m_tableParameterEditor )
				{
					string UsedForQuery = AcParams.Get( Page, AcParams.Name.UsedForQuery, "false", AcParams.From.Url );
					UsedForQuery = UsedForQuery.ToLower();

					if ( UsedForQuery.ToLower() == "false" )
					{
						GetReportParametersResponse reportParametersRes = (GetReportParametersResponse) Session["Parameters"];
						if ( reportParametersRes != null )
						{
							m_parameterList = reportParametersRes.ParameterList;
						}
					}
					else
					{
						Query query = (Query) Session[AcParams.Name.queryObject.ToString()];

						if ( query != null )
						{
							m_parameterList = query.ParameterDefinitionList;
						}
					}

				}
				else
				{
					/*
					GetReportParametersResponse reportParametersRes = (GetReportParametersResponse) Session["EditedParamList"];
					ParameterDefinition[] paramList = ( ParameterDefinition[] ) (reportParametersRes.ParameterList );
					if ( reportParametersRes != null )
					{
						m_parameterList = ConvertTableParamToParamList( paramList );
					}
					*/

					ParameterDefinition TableParamDefinition = ( ParameterDefinition ) Session["EditedParamList"];
					if ( TableParamDefinition != null )
					{
						m_parameterList = ConvertTableParamToParamList( TableParamDefinition );
					}
				}

			}

			GetParamGroups();
			
		}

		private void GetQuery()
		{
			Query query = (Query)AcParams.GetObjectFromSession( Page, AcParams.Name.queryObject );

			if ( query == null ||
				 query.ParameterDefinitionList == null)
				return;

			m_parameterList = query.ParameterDefinitionList;


			PopulateParamDefTable( m_parameterList );

			//m_parameterList = CreateParamDefinition();
			GetParamGroups();

			FillInitialParamValues( query );

		}

		private void  GetParamGroups()
		{
			if ( m_parameterList == null )
				return ;

			if ( m_paramGroupList == null )
				m_paramGroupList = new ArrayList();


			if ( m_tableParameterEditor )
			{
				m_paramGroupList.Add( m_tableParamName );
				return;
			}

			ArrayList paramGroupList = new ArrayList();
			for ( int i = 0; i < m_parameterList.Length ; i++ )
			{
				ParameterDefinition parameter = m_parameterList[i];
				if (! m_paramGroupList.Contains( parameter.Group ) )
				{
					m_paramGroupList.Add( parameter.Group );
				}
			}

		}

		private void GetRequiredInputParameters()
		{
			Item = AcParams.Get( Page, AcParams.Name.__executableName );
			ItemTagName = "ReportFileName";
			//m_structParamName = AcParams.Get( Page, AcParams.Name.StructParamName );
			m_tableParamName =  AcParams.Get( Page, AcParams.Name.TableParamName );
			m_tableParamDisplayMode = AcParams.Get( Page, AcParams.Name.TableParamDisplayMode, "insert" );
			string tableRowNumber = "0";
			if( ! IsPostBack )
			{
				tableRowNumber = AcParams.Get( Page, AcParams.Name.TableParamRowNo, "0");
				ViewState["TableRowNumber"] = tableRowNumber;
			}
			else
			{
				tableRowNumber = (string) ViewState["TableRowNumber"];
			}

			try
			{
				m_tableRowNumber = Convert.ToInt32( tableRowNumber );
			}	
			catch
			{
				m_tableRowNumber = 0;
			}
			
		}

		private void  CreateParamDefinition( GetQueryResponse queryResp)
		{
			if ( queryResp == null )
				return;

			if ( queryResp.Query.ParameterDefinitionList == null )
				return;

			m_parameterList = queryResp.Query.ParameterDefinitionList;

		}

		private string GetParameterValue( TableCell cell )
		{
			if ( cell.Controls.Count == 0 )
			{
				return "";
			}

			Control ctl = cell.Controls[ 0 ];

			Type ctlType = ctl.GetType();
			string paramValue = "";
			
			switch ( ctlType.Name )
			{
				case "TextBox":
				{
					if ( cell.Controls.Count > 4 && cell.Controls[ 4 ].GetType( ).Name == "TextBox"  )
					{						
						// it's a date concatenate date and time
						
						string time = ((TextBox)cell.Controls[4]).Text.Trim( ) ;
						
						if ( time != "" )
						{
							time = " " + time;
						}
						paramValue = ( ( TextBox )ctl ).Text + time;
					}
					else
					{
						paramValue = ((TextBox)ctl).Text;
					}

					// validate if required parameter value is missing
					
					Control lastControl = cell.Controls[ cell.Controls.Count - 1];
					if ( lastControl.GetType( ).Name == "HtmlInputHidden" )
					{
						if ( paramValue == "" || paramValue == null )
						{
							throw new AcException ( "ERRMSG_MISSING_REQUIRED_PARA", new AcKeyArgument[] { new AcKeyArgument( null, ( ( HtmlInputHidden )lastControl ).Value ) }, Session, false ) ;
						}
					}
				}
				break;
			
				case "DropDownList":

				{
					if ( cell.Controls.Count > 1 && cell.Controls[ 1 ].GetType( ).Name == "TextBox"  )
					{
						// editable list
						ctl = cell.Controls[ 1 ];
						goto case "TextBox";
					}
					DropDownList list = ((DropDownList)ctl);

					ListItem item = ((DropDownList)ctl).SelectedItem;
					if ( item != null )
					{
						paramValue = item.Value;
					}
					
					// validate if required parameter value is missing
					
					Control lastControl = cell.Controls[ cell.Controls.Count - 1];
					if ( lastControl.GetType( ).Name == "HtmlInputHidden" )
					{
						if ( paramValue == "" || paramValue == null )
						{
							throw new AcException ( "ERRMSG_MISSING_REQUIRED_PARA", new AcKeyArgument[] { new AcKeyArgument( null, ( ( HtmlInputHidden )lastControl ).Value ) }, Session, false ) ;
						}
					}
				}
				break;
				case "RadioButtonList" :
				{
					ListItem item = ((RadioButtonList)ctl).SelectedItem;
					if ( item != null )
					{
						paramValue = item.Value;
					}
				}
				break;
				
				case "CheckBox":
				{
					if ( (( CheckBox)ctl).Checked )
						paramValue = "true";
					else
						paramValue = "false";

				}
				break;
				
			}
			

			return paramValue;
		}

		
		public void FillQuery( Query query )
		{
			GetParameterDefinition( query );
			GetParameterValues( query );

			if ( m_isWizard )
			{
				CreatePromptParamTable();
				GetPromptParamValue( query.ParameterDefinitionList, query.ReportParameterList );
			}
		}

		private void GetParameterDefinition( Query query )
		{
			if ( query == null )
				return;

			Query getQuery = (Query)AcParams.GetObjectFromSession( Page, AcParams.Name.queryObject );


			if (  getQuery != null )
			{
				query.ParameterDefinitionList = getQuery.ParameterDefinitionList;
			}

		}

		private void GetParameterValues( Query query )
		{
			if ( !UsedForActuateQuery || 
				  query == null )
				return;
			if ( (ParameterList == null) || ( ParameterList.Length == 0 ))
				return;

			TableRowCollection rows = ParamTable.Rows;

			ArrayList paramValueList = new ArrayList();

			for ( int i = 0; i < rows.Count ; i++ )
			{
				if ( rows[i].Cells.Count >= 1 && rows[i].Cells[0].Text  == "P" ) 
				{
					string paramName = rows[i].Cells[1].Text;
					string paramGroup = rows[i].Cells[2].Text;

					string ParamDataType = rows[i].Cells[3].Text;

					if ( ParamDataType.ToLower() == DataType.Structure.ToString().ToLower() ||
						ParamDataType.ToLower() ==  DataType.Table.ToString().ToLower() )
					{
						continue;
					}
					
					
					ParameterValue paramValue = new ParameterValue();
					paramValue.Group = paramGroup;
					paramValue.Name = paramName;
					paramValue.Value = GetParameterValue( rows[i].Cells[5] );

					if ( m_isWizard )
					{
						bool promptParameter = ((CheckBox)(rows[i].Cells[8].Controls[0])).Checked;

						paramValue.PromptParameter = promptParameter;
						paramValue.PromptParameterSpecified = true;
					}

					paramValueList.Add( paramValue );
					
				}
			}

			GetStructureParameterValues( paramValueList );
			GetTableParameterValues( paramValueList );

			query.ReportParameterList = (ParameterValue[]) paramValueList.ToArray( typeof( ParameterValue ) );

		}

		private void GetStructureParameterValues( ArrayList paramValueList )
		{
			TableRowCollection rows = ParamTable.Rows;

			String OldStructParamName = null;
			String NewStructParamName = null;
			int StructParamCount = 0;
			string StructName = null;


			ArrayList StructParamList = null;

			for ( int i = 0; i < rows.Count ; i++ )
			{
				if ( rows[i].Cells.Count >= 1 && rows[i].Cells[0].Text  == "ST" ) 
				{
					
					string ParamName =  rows[i].Cells[1].Text;
					StructName = rows[i].Cells[2].Text;
					string ParamValue = GetParameterValue( rows[i].Cells[5] );

					NewStructParamName = StructName;
					
					if( OldStructParamName != NewStructParamName )
					{
						OldStructParamName = NewStructParamName;
						if ( StructParamCount == 0 )
						{
							StructParamList = new ArrayList();

							FieldValue  fldValue = new FieldValue();
							fldValue.Name  = ParamName;
							fldValue.Value = ParamValue;
					
							StructParamList.Add( fldValue );
						}
						else
						{
							SetStructParamValue( paramValueList, StructParamList, OldStructParamName );
							StructParamList = new ArrayList();
						}

						StructParamCount++;
					}
					else
					{
						FieldValue  fldValue = new FieldValue();
						fldValue.Name  = ParamName;
						fldValue.Value = ParamValue;
					
						StructParamList.Add( fldValue );
					}
				
				}
				else
				{
					if ( StructParamList != null && StructParamList.Count > 0 )
					{
						SetStructParamValue( paramValueList, StructParamList,StructName );
						StructParamCount = 0;
						OldStructParamName = null;
						StructParamList = null;
					}
				}

			}

			if ( StructParamList != null && StructParamList.Count > 0 )
			{
				SetStructParamValue( paramValueList, StructParamList,StructName );
			}
			
		}


		private void SetStructParamValue( ArrayList paramValueList, ArrayList StructParamList,  String StructName )
		{
			ParameterValue paramVal = new ParameterValue();
			paramVal.Name = StructName;
			FieldValue[][] StructParamValue = new FieldValue[1][];
			StructParamValue[0] = new FieldValue[ StructParamList.Count ];

			for ( int i=0; i < StructParamList.Count ; i++ )
			{
				StructParamValue[0][i] = (FieldValue)(StructParamList[i]);
			}


			paramVal.TableValue = StructParamValue;

			paramValueList.Add( paramVal );

		}

		private void GetTableParameterValues( ArrayList paramValueList )
		{
			if ( paramValueList == null )
				return;


			ParameterDefinition[] ParamList = null;

			if ( ! m_usedForActuateQuery )
			{

				GetReportParametersResponse ReportParametersResp = (GetReportParametersResponse) Session["Parameters"];
				if ( ReportParametersResp == null )
					return;
				ParamList = ReportParametersResp.ParameterList;
			}
			else
			{
				Query query = (Query) Session[AcParams.Name.queryObject.ToString()];
				ParamList = query.ParameterDefinitionList;
			}

			
			if ( ParamList == null )
				return;

			for ( int i=0; i < ParamList.Length; i++ )
			{
				if ( ParamList[i].DataType == DataType.Table )
				{
					ParameterDefinition TableParamDefinition = ParamList[i];
					ParameterValue TableParamValue  = new ParameterValue();
					TableParamValue.Name = TableParamDefinition.Name;
					TableParamValue.TableValue = TableParamDefinition.DefaultTableValues;

					//  The server forces that atleast the <Value> or the <TableValue>
					// be present, both cannot be null. Ideally this should be handled in
					// the schema definition

					if ( TableParamDefinition.DefaultTableValues == null )
					{
						TableParamValue.Value = String.Empty;
					}

					paramValueList.Add( TableParamValue );
				}
			}

	}

		private void RenderControl()
		{
			if ( UsedForActuateQuery )
			{
				AcRowFilterTitle.Visible = true;
			}

			/*
			if ( m_tableParamDisplayMode.ToLower() == "insert" )
			{
				AcLblTblParamHeading.Text = AcResourceManager.GetString( "parameters.tables.rows.insert", Session );
			}
			else
			{
				int TableRowNumber = m_tableRowNumber + 1;
				AcLblTblParamHeading.Text = AcResourceManager.GetString( "parameters.tables.rows.edit", new AcKeyArgument[]{ new AcKeyArgument(null, TableRowNumber.ToString()) }, Session );
				
			}*/

			AcLblTblParamHeading.Text = JobUtil.GetTableEditorTitle( Page );


			InsertRow.Visible  = m_tableParameterEditor;
			BtnRowTop.Visible = m_tableParameterEditor;
			BtnRowBottom.Visible = m_tableParameterEditor;

			if ( m_tableParameterEditor )
			{
				ParamTable.CellPadding = 4;
				ParamTable.CellSpacing = 0;

				MainTable.CellPadding = 4;
				MainTable.CellSpacing = 0;
			}
			

			AcLtrlTblEditor.Text = "";

	
		}


		private void FillInitialParamValues( Query query )
		{
			ParameterValue[] ParamValueList = query.ReportParameterList;

			if ( ParamValueList!= null )
			{
				m_initParamValueTable = new Hashtable();

				for ( int i=0; i < ParamValueList.Length; i++ )
				{
					ParameterValue ParamValue = ParamValueList[i];
					string paramName = ParamValue.Name;
					
					ParameterDefinition ParamDef = GetParameterDefinition ( paramName );


					if ( ParamDef.DataType == DataType.Structure )
					{
						FillInitStructParamValue( ParamValue, ParamDef );
					}
					m_initParamValueTable[paramName] = ParamValue;

				}
			}
			else
			{
				// No REportParameters
				// Store the Values from the Parameter Definition 
				ParameterDefinition[] ParamDefList = query.ParameterDefinitionList;

				for ( int k=0 ; k < ParamDefList.Length; k++ )
				{
					ParameterDefinition ParamDef = ParamDefList[k];

					if ( ParamDef.DataType == DataType.Structure )
					{
						FillInitStructParamValue( null , ParamDef );
					}

				}

			}
			
		}


		private string GetInitParamValue( ParameterDefinition paramDef )
		{
			string paramName = paramDef.Name;
			string paramInitValue = paramDef.DefaultValue;

			if ( m_initParamValueTable != null )
			{

				if ( m_initParamValueTable.Contains( paramName ) )
				{
					ParameterValue ParamValue = (ParameterValue) m_initParamValueTable[paramName];
					paramInitValue = (string) ParamValue.Value;
				}
			}

			return paramInitValue;
		}

		 private bool AllowedToChangeParam ( ParameterDefinition parameterDef )
		 {
			bool AllowToChange = true;
			
			 if ( m_initParamValueTable != null )
			 {
				 ParameterValue ParamValue = (ParameterValue)m_initParamValueTable[parameterDef.Name];
				 if ( ParamValue != null )
				 {
					 AllowToChange  = ParamValue.PromptParameter ;
				 }
			 }

			 return AllowToChange;
			
		}


		private bool AllowedToChangeParam ( string ParamName  )
		{
			bool AllowToChange = true;
			
			if ( m_initParamValueTable != null )
			{
				ParameterValue ParamValue = (ParameterValue)m_initParamValueTable[ParamName];
				if ( ParamValue != null )
				{
					AllowToChange  = ParamValue.PromptParameter ;
				}
			}

			return AllowToChange;
			
		}

		void DisplayStructParameter( TableRow row, ParameterDefinition parameterDef )
		{
			row.Visible = false;
			TableCell cell = new TableCell();
			cell.Width = FILTER_VALUE_WIDTH;
			m_structParamName = parameterDef.Name;


			// Prepare a newgroup list with the Struct Param Name as the group Name
			ArrayList groupList = new ArrayList();
			string StructParamDisplayName = ( parameterDef.Name == null || parameterDef.Name.Trim() == String.Empty ) ? parameterDef.DisplayName : parameterDef.Name ;
			groupList.Add( StructParamDisplayName );

			//ParameterDefinition[]  StructParams = ConvertStructParamToParamList( m_parameterList );

			 ParameterDefinition[]  StructParams = ConvertStructParamToParamList( parameterDef , StructParamDisplayName );

			DisplayParametersAndGroups( StructParams, groupList, true );
		}

		void DisplayTableParameter( TableRow row, ParameterDefinition parameterDef )
		{
			TableCell cell = new TableCell();
			cell.Width = FILTER_VALUE_WIDTH;


			AcButtonControl EditButton = new AcButtonControl();
			EditButton.Key = "parameters.tables.button.editTable";
			EditButton.CommandArgument = parameterDef.Name;
			cell.Controls.Add( EditButton );
			
			row.Cells.Add( cell );


			AcUrlBuilder link = new AcUrlBuilder("../newrequest/TableParameterDisplay.aspx");
			link.AddKeyValue( AcParams.Name.TableParamName, parameterDef.Name );
			link.AddKeyValue( AcParams.Name.__executableName, m_Item );
			link.AddKeyValue( AcParams.Name.TableParamDisplayMode, "insert" );
			link.AddKeyValue( AcParams.Name.UsedForQuery, m_usedForActuateQuery.ToString());

			StringBuilder args = new StringBuilder();
			ActuUtil.AddJsArgument( args, link.ToString(), true );

			Session["TableParameterName"] = parameterDef.Name;
			// This part of the code needed because if the user wishes to see the REport
			// in a New browser, then we add the function to open a new window in the OnLoad  Attribute
			// of the Page. In that case , even when the Edit button is clicked, a Post Back is caused
			((HtmlGenericControl)ActuUtil.FindControlRecursive( Page, "AcBody" )).Attributes.Remove("OnLoad");
			EditButton.Attributes["onclick"] = "javascript:DisplayTableParameters(" + args.ToString() + ");";
			//((basePageTemplate)Page).IsPopUpWindow = true;
			//EditButton.Click += EditTableParamButton_Clicked();
			
			CreateEmptyCell( row );
	
		}


		private ParameterDefinition[] ConvertStructParamToParamList( ParameterDefinition parameterList, String StructParamDispName )
		{

			//ParameterDefinition[] ParameterList = null;
			//for ( int i = 0 ; i < parameterList.Length ; i++ )
			//{
			//	if ( ( m_structParamName == parameterList[i].Name ) ||
			//		 ( m_tableParameterEditor && m_tableParamName == parameterList[i].Name ))
			//	{
					FieldDefinition[] StructParameterDefinition = parameterList.RecordDefinition;
					FieldValue[][] StructDefaultValue = parameterList.DefaultTableValues;
					ParameterList = new ParameterDefinition[ StructParameterDefinition.Length ];   


					for ( int j = 0; j < StructParameterDefinition.Length ; j++ )
					{
						ParameterList[j] = new ParameterDefinition();
						ParameterList[j].Name = StructParameterDefinition[j].Name;
						ParameterList[j].DisplayName  = StructParameterDefinition[j].DisplayName;
						ParameterList[j].DataType = ConvertStructDataType( StructParameterDefinition[j].DataType );
						ParameterList[j].DataTypeSpecified = StructParameterDefinition[j].DataTypeSpecified;
						ParameterList[j].IsHidden = StructParameterDefinition[j].IsHidden;
						ParameterList[j].IsHiddenSpecified = StructParameterDefinition[j].IsHiddenSpecified;
						ParameterList[j].IsRequired = StructParameterDefinition[j].IsRequired;
						ParameterList[j].IsRequiredSpecified = StructParameterDefinition[j].IsRequiredSpecified;
						//ParameterList[j].DefaultValue = StructParameterDefinition[j].DefaultValue;
						if ( UsedForActuateQuery )
						{
							ParameterList[j].DefaultValue  = GetStructInitValue(StructParameterDefinition[j].Name);
						}
						else
						{
						if ( StructDefaultValue != null )
						{
							ParameterList[j].DefaultValue = (StructDefaultValue[0][j]).Value;
						}
						}
						ParameterList[j].SelectValueList = StructParameterDefinition[j].SelectValueList;
						ParameterList[j].ControlType = ConvertStructControlType ( StructParameterDefinition[j].FieldControlType );
						ParameterList[j].ControlTypeSpecified = StructParameterDefinition[j].FieldControlTypeSpecified;
                        ParameterList[j].Group = StructParamDispName;

					}
			//		break;

			//	}
			//}
			return ParameterList;
		}




		private ParameterDefinition[] ConvertTableParamToParamList( ParameterDefinition[] parameterList )
		{

			ParameterDefinition[] ParameterList = null;
			for ( int i = 0 ; i < parameterList.Length ; i++ )
			{
				if ( m_tableParamName == parameterList[i].Name ) 
				{
					FieldDefinition[] StructParameterDefinition = parameterList[i].RecordDefinition;
					ParameterList = new ParameterDefinition[ StructParameterDefinition.Length ];   

					for ( int j = 0; j < StructParameterDefinition.Length ; j++ )
					{
						ParameterList[j] = new ParameterDefinition();
						ParameterList[j].Name = StructParameterDefinition[j].Name;
						ParameterList[j].DisplayName  = StructParameterDefinition[j].DisplayName;
						ParameterList[j].DataType = ConvertStructDataType( StructParameterDefinition[j].DataType );
						ParameterList[j].DataTypeSpecified = StructParameterDefinition[j].DataTypeSpecified;
						ParameterList[j].IsHidden = StructParameterDefinition[j].IsHidden;
						ParameterList[j].IsHiddenSpecified = StructParameterDefinition[j].IsHiddenSpecified;
						ParameterList[j].IsRequired = StructParameterDefinition[j].IsRequired;
						ParameterList[j].IsRequiredSpecified = StructParameterDefinition[j].IsRequiredSpecified;
						if ( m_tableParamDisplayMode.ToLower() == "insert" )
						{
							ParameterList[j].DefaultValue = StructParameterDefinition[j].DefaultValue;
						}
						else
						{
							ParameterList[j].DefaultValue = GetFieldValueForParam( j );
						}

						ParameterList[j].SelectValueList = StructParameterDefinition[j].SelectValueList;
						ParameterList[j].ControlType = ConvertStructControlType ( StructParameterDefinition[j].FieldControlType );
						ParameterList[j].ControlTypeSpecified = StructParameterDefinition[j].FieldControlTypeSpecified;
					}
					break;

				}
			}
			return ParameterList;
		}


		private ParameterDefinition[] ConvertTableParamToParamList( ParameterDefinition parameterList )
		{

			FieldDefinition[] StructParameterDefinition = parameterList.RecordDefinition;
			ParameterList = new ParameterDefinition[ StructParameterDefinition.Length ];   

				for ( int j = 0; j < StructParameterDefinition.Length ; j++ )
				{
					ParameterList[j] = new ParameterDefinition();
					ParameterList[j].Name = StructParameterDefinition[j].Name;
					ParameterList[j].DisplayName  = StructParameterDefinition[j].DisplayName;
					ParameterList[j].DataType = ConvertStructDataType( StructParameterDefinition[j].DataType );
					ParameterList[j].DataTypeSpecified = StructParameterDefinition[j].DataTypeSpecified;
					ParameterList[j].IsHidden = StructParameterDefinition[j].IsHidden;
					ParameterList[j].IsHiddenSpecified = StructParameterDefinition[j].IsHiddenSpecified;
					ParameterList[j].IsRequired = StructParameterDefinition[j].IsRequired;
					ParameterList[j].IsRequiredSpecified = StructParameterDefinition[j].IsRequiredSpecified;
					if ( m_tableParamDisplayMode.ToLower() == "insert" )
					{
						ParameterList[j].DefaultValue = StructParameterDefinition[j].DefaultValue;
					}
					else
					{
						ParameterList[j].DefaultValue = GetFieldValueForParam( j );
					}
					ParameterList[j].SelectValueList = StructParameterDefinition[j].SelectValueList;
					ParameterList[j].ControlType = ConvertStructControlType ( StructParameterDefinition[j].FieldControlType );
					ParameterList[j].ControlTypeSpecified = StructParameterDefinition[j].FieldControlTypeSpecified;
				}
			return ParameterList;
		}

		private DataType  ConvertStructDataType( ScalarDataType dataType  )
		{
			DataType ParamDataType = DataType.String;
			switch ( dataType )
			{
				case ScalarDataType.Currency:
					ParamDataType = DataType.Currency;
					break;

				case ScalarDataType.Date:
					ParamDataType =  DataType.Date;
					break;

				case ScalarDataType.Double:
					ParamDataType  = DataType.Double;
					break;

				case ScalarDataType.Integer:
					ParamDataType =  DataType.Integer;
					break;

				case ScalarDataType.String:
					ParamDataType =  DataType.String;
					break;

				case ScalarDataType.Boolean:
					ParamDataType =  DataType.Boolean;
					break;
			}
			return ParamDataType;
		}


		private ParameterDefinitionControlType ConvertStructControlType( FieldDefinitionFieldControlType controlType )
		{
			ParameterDefinitionControlType  ControlType = ParameterDefinitionControlType.ControlList;
			switch ( controlType )
			{
				case FieldDefinitionFieldControlType.ControlList:
					ControlType =  ParameterDefinitionControlType.ControlList;
					break;
        
				case FieldDefinitionFieldControlType.ControlListAllowNew:
					ControlType =  ParameterDefinitionControlType.ControlListAllowNew;
					break;
			}

			return ControlType;
		}

		// Utility fucntion called when the Job is submitted, which does the work of 
		// populating the  structure related parameters in the Session, to be used by the 
		// do_execute and the do_submitjob user controls

		private void PopulateStructParamValues( ArrayList paramValueList )
		{
			if ( paramValueList == null )
				return;


			GetReportParametersResponse  reportParametersResp  = (GetReportParametersResponse) ( Session["Parameters"] );
			ParameterDefinition[] paramDefinition = ( ParameterDefinition[] ) ( reportParametersResp.ParameterList );

			for ( int i = 0; i < paramDefinition.Length; i ++ )
			{
				ParameterDefinition  ParameterDef =  paramDefinition[i];

				if (ParameterDef.DataType == DataType.Structure )
				{
					 
					ParameterValue   ParamValue = new ParameterValue();

					ParamValue.Group = ParameterDef.Group;
					ParamValue.Name = ParameterDef.Name ;
					ParamValue.Value = ParameterDef.DefaultValue;
					FieldDefinition[] StructParam = ParameterDef.RecordDefinition;
					FieldValue[] StructParamValue  = new FieldValue[ StructParam.Length ];


	                for ( int j = 0 ; j < StructParam.Length; j ++ )
					{

						StructParamValue[j] = new FieldValue();
						StructParamValue[j].Name  = StructParam[j].Name;
						StructParamValue[j].Value = StructParam[j].DefaultValue;
					}

					FieldValue[][] TableValue = new FieldValue[1][];

					TableValue[0] = new FieldValue[StructParamValue.Length];

					for ( int k = 0 ; k < StructParamValue.Length; k++ )
					{
							TableValue[0][k] = new FieldValue();
							TableValue[0][k] = StructParamValue[k];
					}

					ParamValue.TableValue = TableValue;

					paramValueList.Add( ParamValue );

				}

			}

		}



		public void OkButton_Clicked(Object sender, EventArgs e) 
		{

			
			//if ( !((basePageTemplate)Page).SessionTimedOut )
			{
			string DisplayMode = m_tableParamDisplayMode.ToLower();
			Session["TableParamDisplayMode"] = DisplayMode;
			if ( DisplayMode == "edit" )
			{
				Session["TableParamRowNo"] = m_tableRowNumber.ToString();
			}

			GetInsertedRecord();
	
			StringBuilder Script = new StringBuilder();
			Script.Append( "<script language=\"javascript\">");
			Script.Append("window.close();");
			Script.Append(" if ( (window.opener != null) && ( window.opener.closed == false ) && (window.opener.document != null) && ( window.opener.document.forms[0] != null) ) { ");
			Script.Append("window.opener.document.forms[0].submit();}");
				Script.Append("</script>");
				AcLtrlTblEditor.Text = Script.ToString();
			}
			/*else
			{
				// If the Session Has timedout
				// Close the Current Window - Table Parameter Editor
				// Close the Opener of the Current Window  - The Table Parameter Display Page
				
				StringBuilder Script = new StringBuilder();
				Script.Append( "<script language=\"javascript\">");
				Script.Append("window.close();");
				Script.Append(" if ( (window.opener != null) && ( window.opener.closed == false ) ) { ");
				Script.Append("window.opener.location.replace(window.opener.location.href);}");
			Script.Append("</script>");
			AcLtrlTblEditor.Text = Script.ToString();
			}*/


			/*
			ArrayList paramValueList = new ArrayList();
			TableRowCollection rows = ParamTable.Rows;

			for ( int i = 0; i < rows.Count ; i++ )
			{

				if ( rows[i].Cells.Count >= 1 && rows[i].Cells[0].Text  == "P" ) 
				{
					string paramName = rows[i].Cells[1].Text;
					string paramValue = GetParameterValue( rows[i].Cells[5] );
					
					FieldValue NewRecord = new FieldValue();
					NewRecord.Name = paramName;
					NewRecord.Value = paramValue;
					paramValueList.Add( NewRecord );
				}
			}


			FieldValue[] InsertRecord = new FieldValue[ paramValueList.Count ];
			for ( int i = 0; i < paramValueList.Count ; i++ )
			{
				InsertRecord[i] = new FieldValue();
				InsertRecord[i] = ( FieldValue ) ( paramValueList[i] );
			}


			// Add this to the List of Default Values stored in the session;
			Session["InsertedTableRecord"] = InsertRecord;
			Session["RecordInserted"] = "true";
			
			*/
	
		}


		public void CancelButton_Clicked(Object sender, EventArgs e) 
		{
			StringBuilder Script = new StringBuilder();
			Script.Append( "<script language=\"javascript\">");
			Script.Append("window.close();");
			Script.Append("</script>");
			AcLtrlTblEditor.Text = Script.ToString();

		}

		private void GetInsertedRecord()
		{

			ArrayList paramValueList = new ArrayList();
			TableRowCollection rows = ParamTable.Rows;

			for ( int i = 0; i < rows.Count ; i++ )
			{

				if ( rows[i].Cells.Count >= 1 && rows[i].Cells[0].Text  == "P" ) 
				{
					string paramName = rows[i].Cells[1].Text;
					string paramValue = GetParameterValue( rows[i].Cells[5] );
					
					FieldValue NewRecord = new FieldValue();
					NewRecord.Name = paramName;
					NewRecord.Value = paramValue;
					paramValueList.Add( NewRecord );
				}
			}


			FieldValue[] InsertRecord = new FieldValue[ paramValueList.Count ];
			for ( int i = 0; i < paramValueList.Count ; i++ )
			{
				InsertRecord[i] = new FieldValue();
				InsertRecord[i] = ( FieldValue ) ( paramValueList[i] );
			}


			// Add this to the List of Default Values stored in the session;
			Session["InsertedTableRecord"] = InsertRecord;
			//Session["RecordInserted"] = "true";

			Session["TableParamDisplayMode"]  = m_tableParamDisplayMode.ToLower();
			Session["TableRowNumber"] = m_tableRowNumber;
		}

		/*
		private void GetUpdatedRecord()
		{

			ArrayList paramValueList = new ArrayList();
			TableRowCollection rows = ParamTable.Rows;

			for ( int i = 0; i < rows.Count ; i++ )
			{

				if ( rows[i].Cells.Count >= 1 && rows[i].Cells[0].Text  == "P" ) 
				{
					string paramName = rows[i].Cells[1].Text;
					string paramValue = GetParameterValue( rows[i].Cells[5] );
					
					FieldValue NewRecord = new FieldValue();
					NewRecord.Name = paramName;
					NewRecord.Value = paramValue;
					paramValueList.Add( NewRecord );
				}
			}


			// Iterate through the existing Parameters in the Session and update the
			// Appropriate record
            

			FieldValue[] UpdatedRecord = new FieldValue[ paramValueList.Count ];
			for ( int i = 0; i < paramValueList.Count ; i++ )
			{
				UpdatedRecord[i] = new FieldValue();
				UpdatedRecord[i] = ( FieldValue ) ( paramValueList[i] );
			}


			// Add this to the List of Default Values stored in the session;
			Session["UpdatedTableRecord"] = InsertRecord;
			Session["TableRowNumber"] = m_tableRowNumber;
		}*/

		private void AddTableParamsToGroup( ArrayList paramForGroup )
		{
			if ( m_parameterList == null )
				return;

			for ( int i=0; i < m_parameterList.Length; i++ )
			{
				paramForGroup.Add( m_parameterList[i] );
			}
		}

/*
		private void GetEditedRow()
		{
			GetReportParametersResponse reportParametersRes = ( GetReportParametersResponse ) Session["EditedParamList"];
			if ( reportParametersRes != null )
			{
				ParameterDefinition[] ParamList = ( ParameterDefinition[]) reportParametersRes.ParameterList;

				FieldValue[][]  TableValues = null;

				 for ( int i=0; i < ParamList.Length; i++ )
				 {
					 ParameterDefinition ParamDef = ParamList[i];

					 if ( ParamDef.Name == m_tableParamName )
					 {
						TableValues = ParamDef.DefaultTableValues;
					 }
				 }

				if ( TableValues != null )
				{
					m_editedRow = TableValues[ m_tableRowNumber ]; 
				}
			}
		}
*/
		private void GetEditedRow()
		{
			ParameterDefinition TableParamDefinition = ( ParameterDefinition ) Session["EditedParamList"];
			if ( TableParamDefinition != null )
			{

				FieldValue[][]  TableValues = null;
				TableValues = TableParamDefinition.DefaultTableValues;

				if ( TableValues != null )
				{
					m_editedRow = TableValues[ m_tableRowNumber ]; 
				}
			}
		}



		private string GetFieldValueForParam( int columnNumber  )
		{
			string ParamValue = null;
			if ( m_editedRow != null )
			{
				FieldValue fldValue = m_editedRow[ columnNumber ];
				ParamValue = fldValue.Value;
			}
			return ParamValue;
		}

		private void EditTableParamButton_Clicked(Object sender, EventArgs e)
		{
			//Button TableEditBtn = ( Button) sender;
			//string TableParamName = TableEditBtn.CommandArgument;
			//((basePageTemplate)Page).Body.Attributes.Add("OnLoad","javascript:DisplayTableParameters(" + args.ToString() + ");");

		}

		public void ResetButton_Clicked(Object sender, EventArgs e)
		{
			//m_TableParamResetMode = true;			
		}


		private void PopulateTableParamValues( ArrayList paramValueList )
		{

		}

		private void ValidateDates()
		{
			// Validate the Dates and time values	
			TableRowCollection rows = ParamTable.Rows;
			if ( rows == null )
				return;

			CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
			DateTime paramDate = DateTime.Now;

			for ( int i = 0; i < rows.Count ; i++ )
			{
				if ( rows[i].Cells.Count > 5 && 
					rows[i].Cells[5].Controls.Count > 0 &&
					rows[i].Cells[0].Text  == "P" )

				{
					string paramName = rows[i].Cells[1].Text;
					string ParamDataType = rows[i].Cells[3].Text;
					if ( ParamDataType.ToLower() == DataType.Date.ToString().ToLower())
					{
						TableCell cell = rows[i].Cells[5];

						if ( cell.Controls.Count == 0 )
						{
							continue;
						}

						Control ctl = cell.Controls[ 0 ];

						Type ctlType = ctl.GetType();

						string Date = null;


						switch ( ctlType.Name )
						{
							case "TextBox":
							{
								TextBox DateTxt  = (TextBox) cell.Controls[ 0 ];
								TextBox TimeTxt = (TextBox) cell.Controls[4];

								bool IsNull = false;
								
								// Special case for  Null date parameters
								// Valid only for strcuture and table parameters
								if ( m_displayStructParams || m_tableParameterEditor )
								{
										IsNull = JobUtil.IsNullValue( DateTxt.Text );
								}

								if ( IsNull )
								{
									TimeTxt.Text = "";
								}
								else
								{
								
									Date = DateTxt.Text.Trim() + " " + TimeTxt.Text.Trim();

									if ( Date == null || Date.Trim() == String.Empty )
										continue;
		
									try
									{
										DateTime.Parse( Date, currentCulture);
									}
									catch
									{
										DateTxt.Text = "";
										TimeTxt.Text = "";
										continue;
									}
								}
							}
							break;
						}
					}
				}
			}
		}

		private void PopulateParamDefTable( ParameterDefinition[] ParamDefList )
		{
			if ( ParamDefList == null )
				return;

			m_paramDefTable = new Hashtable();

			for ( int i=0; i < ParamDefList.Length ; i++ )
			{
				ParameterDefinition ParamDef = ParamDefList[i];
				m_paramDefTable[ ParamDef.Name ] = ParamDef;
			}
		}


		// Make  sure that PopulateParamDefTable() has been called before this
		private ParameterDefinition GetParameterDefinition( string Name )
		{
			return ( ( ParameterDefinition ) m_paramDefTable[ Name ] );
		}

		// Used by query 
		private void FillInitStructParamValue( ParameterValue ParamValue, ParameterDefinition ParamDef )
		{
			if ( m_structParamInitValue == null )
			{
				m_structParamInitValue = new Hashtable();
			}

			
			// For structure parameters 
			// 1) Check the <TableValue> of the ReportParameters
			// 2) Then use the DefaultTableValues of the ParameterDefitnion
			FieldValue[][] StructParamValues = null;
			if ( ParamValue != null )
			{
				StructParamValues = ParamValue.TableValue;
			}
			FieldDefinition[] StructParamDefn = ParamDef.RecordDefinition;

			if ( StructParamValues != null )
			{
				if ( StructParamDefn != null )
				{
					for ( int j=0; j < StructParamDefn.Length; j++ )
					{
						FieldValue Field = StructParamValues[0][j];
						m_structParamInitValue[ Field.Name ] = Field.Value;
					}
				}

			}
			else
			{
				// Pick up the Default from the <ParameterDefinition>
				FieldValue[][] StructDefaultValue = ParamDef.DefaultTableValues;
				if ( StructDefaultValue != null && StructParamDefn != null )
				{
					for ( int k=0; k < StructParamDefn.Length; k++ )
					{
						FieldValue Field = StructDefaultValue[0][k];
						m_structParamInitValue[ Field.Name ] = Field.Value;;					}

				}

			}

		}

		private string GetStructInitValue( string Name )
		{
			string Value = null;
			if( m_structParamInitValue != null && m_structParamInitValue.Contains( Name ) )
			{
				Value = (string ) m_structParamInitValue[ Name ];
			}
			return Value;
		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion
	}
}
