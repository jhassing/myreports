namespace DistributionPortal
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using DistributionPortal.Components;

	/// <summary>
	///		Summary description for Title.
	/// </summary>
	public partial class Title : System.Web.UI.UserControl
	{
	
		private string _newTextToDisplay = "";

		public string Text
		{
			set
			{
				_newTextToDisplay = value;
				lblTitle.Text = _newTextToDisplay;
			}
			get
			{
				return _newTextToDisplay;
			}
		}

		protected void Page_Load(object sender, System.EventArgs e)
		{
			if (!Page.IsPostBack)
			{	
				if (Visible)
				{
					switch (QueryStringValues.App)
					{
						case enumApp.rd_admin:
						switch (QueryStringValues.AdminTabIndex())
						{
							case 0:							
								lblTitle.Text="Server Setup";
								break;

							case 1:
								lblTitle.Text="Burst History";
								break;

							case 2:							
								lblTitle.Text="User Membership";
								break;

							case 3:
								lblTitle.Text="New MyReports User";
								break;

							case 4:
								lblTitle.Text="Valid Report Listing";
								break;

							case 5:
								lblTitle.Text="Folder Status";
								break;

						}
							break;
					}
			
					
				}

			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion
	}
}
