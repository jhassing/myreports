//===========================================================================
// This file was modified as part of an ASP.NET 2.0 Web project conversion.
// The class name was changed and the class modified to inherit from the abstract base class 
// in file 'App_Code\Migrated\common\Stub_tabstip_ascx_cs.cs'.
// During runtime, this allows other classes in your web application to bind and access 
// the code-behind page using the abstract base class.
// The associated content page 'common\tabstip.ascx' was also modified to refer to the new class name.
// For more information on this code pattern, please refer to http://go.microsoft.com/fwlink/?LinkId=46995 
//===========================================================================
namespace DistributionPortal
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Collections;
	using System.Web.Security;
	using System.Text;

	public partial class Migrated_TabStripClass : TabStripClass
	{
		//protected System.Web.UI.WebControls.DataList tabs;
		private ArrayList _tabItems;

		private int _selectedTab;

        public event TabCLicked Tab_OnClick;

		#region Properties

//		public ArrayList TabItems
//		{
		override public ArrayList TabItems
		{
			set
			{
				_tabItems = value;

			}
			get
			{
				return _tabItems;
			}
		}

//		public int SelectedTab
//		{
		override public int SelectedTab
		{
			set
			{
				ViewState["SelectedTab"] = value;
				_selectedTab = value;
				//tabSelected = value;
			}
			get
			{
				//return _selectedTab;
				return System.Convert.ToInt32( ViewState["SelectedTab"] );
				//return tabSelected;
			}
		}

//		public void Enabled(int tab, bool enabled)
		override public void Enabled(int tab, bool enabled)
		{
			int nTabs = (PlaceHolderMain.Controls.Count - 1 );

			if (tab <= nTabs)
				((LinkButton)PlaceHolderMain.Controls[tab]).Enabled = enabled;

		}

		

		#endregion

			
//		public void LinkButton_Command(Object sender, CommandEventArgs e) 
		override public void LinkButton_Command(Object sender, CommandEventArgs e) 
		{
			// Populate the tabs
			int tabClicked = System.Convert.ToInt32(e.CommandArgument) ;
			SelectedTab = tabClicked;

			for (int x=0;x<PlaceHolderMain.Controls.Count;x++)
			{
				((LinkButton)PlaceHolderMain.Controls[x]).CssClass = "tab-inactive";
			}
			((LinkButton)PlaceHolderMain.Controls[tabClicked]).CssClass = "tab-active";
			//LoadTabs();

			// Tab clicked. raise event to client
            
			if ( Tab_OnClick != null)
				Tab_OnClick( tabClicked );
		}	

//		public void LoadTabs()
		override public void LoadTabs()
		{
			PlaceHolderMain.Controls.Clear();
			StringBuilder sb = new StringBuilder();
			StringBuilder sbTab = new StringBuilder();

			for (int x=0;x<_tabItems.Count;x++)
			{

				LinkButton LinkButton1 = new LinkButton();
				LinkButton1.Command+=new CommandEventHandler(LinkButton_Command);
				LinkButton1.CausesValidation = true;	
				LinkButton1.Text= ( (TabItem)_tabItems[x]).Name;
				LinkButton1.CommandName="TabItem";
				//LinkButton1.Enabled = ( (TabItem)_tabItems[x]).Enabled;
				if (SelectedTab == x)
					LinkButton1.CssClass="tab-active";
				else
					LinkButton1.CssClass="tab-inactive";

				LinkButton1.CommandArgument=x.ToString();

				//LinkButton1.ID = "tab" + x.ToString();				

                
                
				PlaceHolderMain.Controls.Add(LinkButton1);
				
				LinkButton1.ID = "Tab" + x.ToString();
				LinkButton1.Attributes.Add("onClick", "javascript:DisableTabs();" + this.Page.GetPostBackEventReference(LinkButton1) );				
				sb.Append( "\t\t" + LinkButton1.ClientID +".disabled  = true; \r\n ");
			}

			// Write javascript to disable the tabs when clicked.
			if ( !Page.IsClientScriptBlockRegistered("Disable-Tabs Function") )
			{
				sbTab.Append( getOpenScriptTag() );

				sbTab.Append( "\t function DisableTabs() { \r\n \r\n" );				

				// Place the tab disable js here
				sbTab.Append( sb.ToString() );

				sbTab.Append( "\t\t } \r\n" );
				sbTab.Append( getCloseScriptTag() );
				
				Page.RegisterClientScriptBlock( "Disable-Tabs Function", sbTab.ToString() );
				
			}
		}

		



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
		
		}


	}
}
