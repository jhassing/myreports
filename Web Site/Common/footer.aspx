<%@ Page Language="C#" AutoEventWireup="true" CodeFile="footer.aspx.cs" Inherits="Common_footer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
		<LINK href="../css/allstyles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body style="MARGIN:0px">
		<form runat="server" ID="Form1">
			<table height="100%" width="100%" border="0" cellspacing="0" cellpadding="1" class="banner">
				<tr style="WIDTH: 100%">
					<td>
						<%-- PROPERTY: USER NAME --%>
						&nbsp;
						<asp:label CssClass="bannerLabel" runat="server" ID="lblUserName" />
						<asp:label id="UserNameValue" CssClass="bannerValue" runat="server" />
						<%-- PROPERTY: SERVER --%>
						&nbsp;&nbsp;&nbsp;
						<asp:label CssClass="bannerLabel" runat="server" ID="lblServer" />
						<asp:label id="ServerValue" CssClass="bannerValue" runat="server" />
						<%-- PROPERTY: VOLUME --%>
						&nbsp;&nbsp;&nbsp;
						<asp:label CssClass="bannerLabel" runat="server" ID="lblDomain" />
						<asp:label id="DomainValue" CssClass="bannerValue" runat="server" />
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
