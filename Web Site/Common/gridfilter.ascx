<%@ Control Language="c#" Inherits="DistributionPortal.Common.GridFilter" CodeFile="GridFilter.ascx.cs" %>
<table class="panel" cellSpacing="0" cellPadding="8" width="100%">
	<tr>
		<td noWrap>
			<asp:label id="lblFilter" CssClass="X-Small" runat="server">Filter:</asp:label>&nbsp;
			<asp:TextBox id="txtFilter" CssClass="X-SmallTextBox" runat="server" MaxLength="20" Width="120px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<asp:button id="btnSearch" CssClass="MyReportsAdminConsoleButton" runat="server" Width="50px"
				CausesValidation="False" Text="Search" onclick="btnSearch_Click"></asp:button>&nbsp;<INPUT class="MyReportsAdminConsoleButton" id="btnClear" style="WIDTH: 50px" type="reset"
				value="Clear">&nbsp;&nbsp;&nbsp;&nbsp;
			<asp:button id="btnAddNewGroup" CssClass="MyReportsAdminConsoleButton" runat="server" Width="50px"
				CausesValidation="False" Text="Add" onclick="btnAddNewGroup_Click"></asp:button>
				</td>
	</tr>
</table>
