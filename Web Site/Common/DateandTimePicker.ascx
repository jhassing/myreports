<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DateandTimePicker.ascx.cs" Inherits="DateandTimePicker" %>
<%@ Register TagPrefix="Club" Namespace="ClubSite" %>

<div class="controlblock">


    <table>
        <tr>
            <td>
                Date:
            </td>
            <td>
                <club:datepicker id="dp1" runat="server" />             
            </td>
            <td>
                Time:
            </td>
            <td>
                <club:timepicker id="tp1" runat="server" />
            </td>
        </tr>
    </table>
</div>