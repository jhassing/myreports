namespace activeportal.common
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes;
	using activeportal.classes.configuration;
	using actuate_i18n;
	using System.Collections;
	using System.Web.UI;

	/// <summary>
	///		Summary description for toolbar.
	/// </summary>
	public partial  class toolbar : System.Web.UI.UserControl, IPostBackEventHandler
	{
		private bool m_showView = true, m_showFilter = true;

		public bool ShowView
		{
			set { m_showView = value; }
		}
		
		public bool ShowFilter
		{
			set 
			{
				m_showFilter = value;
			}
		}
		protected void Page_Load( Object sender, System.EventArgs e )
		{
			if ( !IsPostBack )
			{
				if ( m_showView )
				{
					PopulateFileViewList();
				}
				else
				{
					HideView( );
				}

				if ( m_showFilter )
				{
					Hashtable UserProfile = AcProfileManager.GetUserProfile( Page );
					bool ShowFilters = true;
					if ( UserProfile != null )
					{
						ShowFilters = ActuUtil.ConvertStringToBool( (string) UserProfile["showFilters"]);
					}

					ShowFilters = AcParams.urlCheckboxState( Page, AcParams.Name.enableFilters, ShowFilters );
					SetFilterValue( ShowFilters );
				}
				else
				{
					HideFilter( );
						
				}
			}
			else
			{
				if ( AcLstFilesView.SelectedIndex != -1 )
				{
					AcParams.SetToSession( Session, AcParams.Name.view, AcLstFilesView.Items[AcLstFilesView.SelectedIndex].Value );
				}
				// workaround .NET bug. If this is not called, the javascript __dopostback function is not generated.
				Page.GetPostBackEventReference( this, "true" );
			}
			
		}
		

		private void HideFilter( )
		{
			AcLnkFilterOn.Visible = false;
			AcLnkFilterOff.Visible = false;
			AcLblFilter.Visible = false;
			AcCellSeparator.Visible = false;
		}

		private void HideView( )
		{
			AcLblView.Visible = false;
			AcLstFilesView.Visible = false;
		}
		void IPostBackEventHandler.RaisePostBackEvent(String strEventArgument)
		{
			if (strEventArgument == null)
					return;

			bool showFilters = Boolean.Parse(strEventArgument);
			
			SetFilterValue( showFilters );	
		
		}
		private void SetFilterValue( bool showFilters )
		{
			if ( ! showFilters )
			{
				AcLnkFilterOn.NavigateUrl = "javascript:" +  Page.GetPostBackEventReference( this, "true" );
				AcLnkFilterOff.NavigateUrl = "";
				AcLnkFilterOn.CssClass = "toolbarlink";
				AcLnkFilterOff.CssClass = "toolbarboldtext";

			}
			else
			{
				AcLnkFilterOff.NavigateUrl = "javascript:" +  Page.GetPostBackEventReference( this, "false" );
				AcLnkFilterOn.NavigateUrl = "";
				AcLnkFilterOn.CssClass = "toolbarboldtext";
				AcLnkFilterOff.CssClass = "toolbarlink";
			}
			
			AcParams.SetToSession( Session, AcParams.Name.enableFilters, showFilters.ToString( ) );
		}
		private void PopulateFileViewList()
		{
			string Category  = AcResourceManager.GetString( "options.FilesView.Categories", Session );
			string Detail	 = AcResourceManager.GetString("options.FilesView.Detail", Session );
			string Icon		 = AcResourceManager.GetString( "options.FilesView.Icon", Session );
			string List		 =  AcResourceManager.GetString( "options.FilesView.List", Session );
	
			ArrayList FileViewList = new ArrayList();

			ListItem CategoryList = new ListItem();
			CategoryList.Text = Category;
			CategoryList.Value = Constants.FILE_VIEW_CATEGORY;
			FileViewList.Add( CategoryList );

			ListItem DetailItem = new ListItem();
			DetailItem.Text = Detail;
			DetailItem.Value = Constants.FILE_VIEW_DETAIL;
			FileViewList.Add( DetailItem );


			ListItem IconItem = new ListItem();
			IconItem.Text = Icon;
			IconItem.Value = Constants.FILE_VIEW_ICON;
			FileViewList.Add( IconItem );

			
			ListItem  listItem = new ListItem();
			listItem.Text = List;
			listItem.Value = Constants.FILE_VIEW_LIST;
			FileViewList.Add( listItem );

			AcLstFilesView.DataValueField = "Value";
			AcLstFilesView.DataTextField = "Text";
			AcLstFilesView.DataSource = FileViewList;
			AcLstFilesView.DataBind();

			string view = AcParams.Get( Page, AcParams.Name.view );
			if ( view == null )
			{
				Hashtable userProfile = AcProfileManager.GetUserProfile( Page );
				if ( userProfile != null )
				{
					view = (string)userProfile[ "view" ];
				}
			}

			if ( view != null )
			{
				ListItem currentViewItem = AcLstFilesView.Items.FindByValue( view );
				if ( currentViewItem != null )
				{
					currentViewItem.Selected = true;
				}
			}
		}
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
