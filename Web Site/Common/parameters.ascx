<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="parameters.ascx.cs" Src="parameters.ascx.cs" Inherits="activeportal.usercontrols.newrequest.parameters" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<STYLE>
	.parameterInputBox { z-Index:2;position:relative;top=-1;}
	.parameterDropdown { z-Index:0;position:absolute;clip:rect(auto auto auto 211pt); }
</STYLE>
<table ID="MainTable" cellspacing="10" cellpadding="0" Border="0" RunAt="Server">
	<tbody>
	<tr id="AcRowFilterTitle" Visible="false" runat="server">
		<td valign="bottom">
			<hr width="20" size="1">
		</td>
		<td nowrap>
			<localized:AcLabelControl CssClass="label" Key="query.create.filters.label.parameters"   runat="server"/>
		</td>
		<td width="73%" valign="bottom">
			<hr size="1">
		</td>
		<td valign="bottom">
			&nbsp;
		</td>
	</tr>
  	  
    <tr ID="InsertRow" RunAt="server">
		<th valign="top" rowspan="1" colspan="3" class="tableParameterHeader" align="center">
			<asp:Label  ID="AcLblTblParamHeading"  RunAt="Server"/>
		</th>
    </tr>
    <tr ID="BtnRowTop" RunAt="Server">
		<td  rowspan="1" colspan="3" class="tableParameterButtonPanel" align="center" nowrap>
		   <localized:AcButtonControl ID="AcBtnOkTop" Key="BTN_OK" OnClick="OkButton_Clicked" RunAt="server"/>
		   <localized:AcButtonControl ID="AcBtnCancelTop" Key="BTN_CANCEL"  OnClick="CancelButton_Clicked" RunAt="Server"/>
		</td>
    </tr>
	<tr>
		<td colspan="4" valign="Top">
			<asp:Table ID="ParamTable" EnableViewState="True" cellspacing="3" cellpadding="3" border="0" Runat="server"></asp:Table>
		</td>
	</tr>
    <tr ID="BtnRowBottom" RunAt="Server">
		<td  rowspan="1" colspan="3" class="tableParameterButtonPanel" align="center" nowrap>
		   <localized:AcButtonControl ID="AcBtnOkBottom" Key="BTN_OK" OnClick="OkButton_Clicked" RunAt="server"/>
		   <localized:AcButtonControl ID="AcBtnCancelBottom" Key="BTN_CANCEL" OnClick="CancelButton_Clicked" RunAt="Server"/>
		</td>
    </tr>

 </tbody>
</table>

<%-- --------------------------------------------------------------------------
	AD HOC MENU
-------------------------------------------------------------------------- --%>
<input border=0 id="dropdownInput" style="position:absolute;visibility:hidden;width:250pt" value="" onblur="finishEditingList();">
<select id="dropdownShadow" style="position:absolute;visibility:hidden;width:20"></select>

<table id="AdHocMenu" cellspacing="1" style="position:absolute;left:0px;top:0px;visibility:hidden;backgroundColor:white" ondeactive="javascript:deactivatePopUp();">
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('>');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.gt" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('<');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.lt" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('>=');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.ge" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('<=');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.le" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('|');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.or" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('&');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.and" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('!');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.not" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('_');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.match1char" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('%');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.match0ormore" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('[]');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.match1ormore" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('-');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.rangeofvalues" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
	<tr>
		<td width="4%">&nbsp;</td>
		<td width="92%" nowrap onmouseover="javascript:onItem(this)" onmouseout="javascript:offItem(this)" onclick="javascript:appendText('\\');"> 
			<localized:AcLabelControl key="parameter.expressionbuilder.escape" runat="server"/>
		</td>
		<td width="4%">&nbsp;</td>
	</tr>
</table>
<input type="hidden" id="EditableListParamValues" RunAt="server" />
<asp:Literal ID="AcLtrlTblEditor" RunAt="server"/>
