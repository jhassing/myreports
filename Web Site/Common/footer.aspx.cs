using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


public partial class Common_footer : System.Web.UI.Page
{

    protected void Page_Load(object sender, System.EventArgs e)
    {
        //lblUserName.Text = Global.ResourceValue("PROP_USER");
        lblUserName.Text = "User:";
        UserNameValue.Text = MyReports.Common.AppValues.UserName;

        //lblServer.Text = Global.ResourceValue("PROP_SERVER");
        lblServer.Text = "System:";
        ServerValue.Text = MyReports.Common.AppValues.ActuateServerName;

        //lblDomain.Text = Global.ResourceValue("PROP_DOMAIN");
        lblDomain.Text = "Domain:";
        DomainValue.Text = MyReports.Common.AppValues.FullyQualifiedDomain;

    }
}