
namespace activeportal.usercontrols.common
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using actuate_i18n;
	using actuate_i18n.classes;
	using activeportal.classes;
	using System.Collections;
	using activeportal.classes.functionality;
	using activeportal.classes.configuration;
	using actuate.classes.webcontrols;
	using activeportal.classes.skins;
	using DistributionPortal;
	using DistributionPortal.Components;
	
	public partial  class navigation : System.Web.UI.UserControl
	{

		private string m_targetBase = ""; // "../index/index.aspx?app=rd&subpage=_list";
		public string FolderTarget
		{
			set { m_targetBase = value; }
			get { return m_targetBase ; }
		}
		
		protected void Page_Load(object sender, System.EventArgs e)
		{
	
			if (Visible)
			{
				switch(QueryStringValues.App)
				{
					case enumApp.rd:
						//m_targetBase = "../index/index.aspx?app=rd&subpage=_list";
						m_targetBase = Context.Request.ApplicationPath + "/ReportDistribution/RDList.aspx?doframe=true&subpage=_list&app=rd";
						break;

					case enumApp.bbd:
						m_targetBase = Context.Request.ApplicationPath + "/ReportDistribution/RDList.aspx?doframe=true&subpage=_list&app=bbd";
						//m_targetBase = "../index/index.aspx?app=bbd&subpage=_list";
						break;
				}

				Feature documents = AcFunctionalityManager.GetFeature( Page, "Documents" );
				AcImgMyDocuments.ImageUrl= "../images/homefoldericon.gif";
				if ( documents == null )
				{
					AcLnkMyDocuments.Visible = false;
					AcImgMyDocuments.Visible = false;
				}
			}
		}

		protected override void OnPreRender( EventArgs e )
		{
		
			if (Visible)
			{
				switch(QueryStringValues.App)
				{
					case enumApp.rd:
						// My Documents
						String target = m_targetBase + "&folder=";
						target += HttpUtility.UrlEncode( AcParams.Get( Page, AcParams.Name.homeFolder, "/" ) );
						AcLnkMyDocuments.NavigateUrl = target;
						AcImgMyDocuments.NavigateUrl = target;
						break;

					case enumApp.bbd:
						AcLnkMyDocuments.Visible = false;
						AcImgMyDocuments.Visible = false;
						break;
				}


				string pageLocation = ( string ) Context.Items["Location"];
			
				string execName = AcParams.Get( Page, AcParams.Name.__executableName, AcParams.From.Url );
				string name = AcParams.Get( Page, AcParams.Name.name, AcParams.From.Url );

				string AdditionalText = (string) Context.Items["AdditionalNavText"];

				// If there is a file name to show, set this bool to true

				bool HasFileName = ( name != null || execName != null || AdditionalText != null);

				// Folder Navigation

				String homeFolder = AcParams.Get( Page, AcParams.Name.homeFolder, "/" );   
				
				string workingDir = AcParams.Get( Page, AcParams.Name.folder, homeFolder );
			
				string ActualPageLocation = (string) Context.Items["ActualLocation"];

				workingDir = ActuUtil.ResolveUpFolderPath( workingDir );

				if ( workingDir.StartsWith( "~" ) )
				{
					// for correct processing in navigation only, it does not affect request sent to server
					workingDir = '/' + workingDir;
				}
				workingDir = workingDir.TrimEnd( '/' );
				string [] arrayOfDir = workingDir.Split('/');
				int nbOfDirs = arrayOfDir.Length;
								

				HyperLink volume = new HyperLink();
				volume.Text = AcParams.Get( Page, AcParams.Name.volume );
			
				if ( nbOfDirs > 1 || HasFileName )
				{
					//volume.NavigateUrl = m_targetBase + "&folder=/";
					volume.CssClass = "breadcrumb";
				}
				else
				{
					volume.CssClass = "breadcrumb";
				}
			
			
				AcNavigationContent.Controls.Add( volume );
			
				for (int i = 1; i < nbOfDirs; i++ )
				{
					AddSeparator();
					HyperLink targetFolder = new HyperLink( );
				
					if ( i < nbOfDirs - 1 || HasFileName )
					{
						String folderName = "";
						for (int j =1 ; j <= i; j ++ )
						{
							folderName += "/";
							folderName += arrayOfDir[ j ];
						}
				
				
						targetFolder.NavigateUrl = m_targetBase +"&folder=";
						targetFolder.NavigateUrl += HttpUtility.UrlEncode(folderName); 
						targetFolder.CssClass = "breadcrumb";
					}
					else
					{
						targetFolder.CssClass = "breadcrumb";
					}

					targetFolder.Text = arrayOfDir[ i ];
				

					AcNavigationContent.Controls.Add( targetFolder );								
				}	
			}					
		}

		private void AddSeparator()
		{
			// Link for the Folder Detail
			Label separator = new Label();
			separator.Text = " > ";
			separator.CssClass = "breadcrumb";
			AcNavigationContent.Controls.Add( separator );

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion
	}
}
