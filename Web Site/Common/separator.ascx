<%@ Control Language="c#" Inherits="activeportal.usercontrols.common.separator" CodeFile="separator.ascx.cs" %>
<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<tr width="100%">
	<td colspan="<%=Colspan%>" width="100%"><img src="<%=Context.Request.ApplicationPath%>/images/vspace.gif" width="100%" height="1" border="0"></td>
</tr>
<tr width="100%">
	<td colspan="<%=Colspan%>" width="100%"><img src="<%=Context.Request.ApplicationPath%>/images/horzline.gif" width="100%" height="1" border="0"></td>
</tr>
