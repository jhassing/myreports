<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TabStrip.aspx.cs" Inherits="TabStrip" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">    
    <link href="../../css/allstyles.css" type="text/css" rel="stylesheet" />
    <link href="webAdminStyles.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" src="tabs.js"></script>
    <title>Tab Strip</title>
</head>
<body>
    <form id="frmProps" runat="server">
    <div>
                 <table width="100%" height="64" cellspacing="0" cellpadding="0" class="homePageHeader" border="0">
                <tr>
                    <td valign="bottom" nowrap="nowrap" class="webToolBrand" height="31">
                    &nbsp;
                        <asp:Label ID="HeaderLabel" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="1" valign="bottom" nowrap height="33">       
                       <asp:Repeater id=Repeater1 runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                          <HeaderTemplate>
                              <table id="TabTable" width="100%" cellspacing="0" cellpadding="0" border="0" onselectstart="event.returnValue=false;">
                                <tr>
                          </HeaderTemplate>            
                          <ItemTemplate>           
                                <td  width=4 height=20 nowrap class="spacerTab">&nbsp;</td>                
                                 <td  runat="server" id="left" width="4" height="20" valign="top" class="deSTabLeft" nowrap>
                                    <asp:Image id="leftImage" ImageUrl="selectedTab_leftCorner.gif" runat="server" Width="4" Height="3" BorderStyle="None" /></td>                
                                 <td  runat="server" id="center" Height="20" width="125px" align="center" valign="top" nowrap class="deSTabCenter" >
                                    </td>                                
                                <td  runat="server" id="right" width="4" height="20" valign=top class="deSTabRight" nowrap>
                                    <asp:Image id="Image1" ImageUrl="selectedTab_rightCorner.gif" runat="server" Width="4" Height="3" BorderStyle="None" /></td>                              
                          </ItemTemplate>             
                          <FooterTemplate>
                           <td  width="" height="20"  nowrap class="spacerTab">&nbsp;</td>
                                </tr>
                             </table>
                          </FooterTemplate>             
                       </asp:Repeater>
                    </td>
                </tr>
            </table>

            <input type="hidden" name="sPreviousTab" value="">
            <asp:HiddenField ID="QueryStringValue" runat="server" />
            <asp:HiddenField ID="sTab" runat="server" />
            
            <script language=javascript>
               <%GetDefaultPage(); %>
            </script>                   
        
        </div>
        </form>
    
</body>
</html>
