using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class TabStrip : System.Web.UI.Page
{
    private int pos = 0;

    #region OnLoad

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        QueryStringValue.Value = Request.QueryString.ToString();

        // Show or Hide the navigattion menu as needed
        // Depending on the app and the shownavigation query-string
        if (Request.QueryString["HeaderLabel"] != null)
        {
            this.HeaderLabel.Text = Request.QueryString["HeaderLabel"].ToString();
        }

        Repeater1.DataSource = Tabs.GetTabs(Request.QueryString["TabContext"].ToString());
        Repeater1.DataBind();



    }
    #endregion

    protected void GetDefaultPage()
    {
        Response.Write(" changeSrc(this, 'membership', 'UserSource.aspx');");
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!this.Page.IsClientScriptBlockRegistered("RuntimeScript"))
            this.Page.RegisterClientScriptBlock("RuntimeScript", "<script language=javascript>changeSrc(this, 'membership', 'UserSource.aspx');</script>");
        

    }


    #region Properties

    #region Header

    public string Header
    {
        get
        {
            return HeaderLabel.Text;
        }
        set
        {
            HeaderLabel.Text = value;
        }
    }

    #endregion

    #region TabItems

    public ArrayList TabItems
    {
        set
        {
            ViewState["TabItems"] = value;

        }
        get
        {
            try
            {
                if (ViewState["TabItems"] != null)
                {
                    return (ArrayList)ViewState["TabItems"];
                }
                else
                    return new ArrayList();
            }
            catch
            {
                return new ArrayList();
            }
        }
    }

    #endregion

    #region SelectedTab

    public int SelectedTab
    {
        set
        {
            ViewState["SelectedTab"] = value;
        }
        get
        {
            try
            {
                if (ViewState["SelectedTab"] != null)
                    return System.Convert.ToInt32(ViewState["SelectedTab"]);
                else
                    return 0;
            }
            catch
            {
                return 0;
            }
        }
    }

    #endregion

    #endregion

    #region LinkButton_Command

    public void LinkButton_Command(Object sender, CommandEventArgs e)
    {
        // Populate the tabs
        int tabClicked = System.Convert.ToInt32(e.CommandArgument);
        this.SelectedTab = tabClicked;

        Repeater1.DataBind();

        // Tab clicked. raise event to client
        //if (Tab_OnClick != null)
        //     Tab_OnClick(tabClicked);

    }

    #endregion

    #region Repeater1_ItemDataBound

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem ||
            e.Item.ItemType == ListItemType.SelectedItem)
        {
            TabItem ti = (TabItem)e.Item.DataItem;

            ///HtmlAnchor LinkButton1 = (HtmlAnchor)e.Item.FindControl("LinkButton1"); 
            HtmlTableCell left = (HtmlTableCell)e.Item.FindControl("left");
            HtmlTableCell right = (HtmlTableCell)e.Item.FindControl("right");
            HtmlTableCell center = (HtmlTableCell)e.Item.FindControl("center");

            //LinkButton1.InnerText = ti.Name;
            center.InnerText = ti.Name;

            //string mouseOver = "this.className='hoverTabCenter';";
            //mouseOver += "document.getElementById('" + left.ClientID + "').className='hoverTabLeft';";
            //mouseOver += "document.getElementById('" + right.ClientID + "').className='hoverTabRight';";

            string mouseOver = "javascript:hoverLink(this, '" + ti.Id + "',getTab(), true);";

            //string mouseOut = "this.className='deSTabCenter';";
            //mouseOut += "document.getElementById('" + left.ClientID + "').className='deSTabLeft';";
            //mouseOut += "document.getElementById('" + right.ClientID + "').className='deSTabRight';";

            string mouseOut = "javascript:hoverLink(this, '" + ti.Id + "',getTab(), false);";

            string mouseClick = "javascript:changeSrc(this, '" + ti.Id + "', '" + ti.Url + "')";

            center.Attributes.Add("onmouseover", mouseOver);
            center.Attributes.Add("onmouseout", mouseOut);
            center.Attributes.Add("onclick", mouseClick);
                
            pos++;
        }
    }

    #endregion

}
