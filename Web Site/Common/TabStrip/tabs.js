﻿// JScript File
var areTabsLoaded = true;

     /* called on click of each tab link. */
    function changeSrc(objID, tabID, url)
    {
 
	    // if tab page is not loaded and clicked on hyperlink don't do anything
	    if (!areTabsLoaded) 
	    {
		    return;
	    }

	    //if clicked on same tab, dont do anything...
	    if (document.frmProps.sTab.value == tabID)
	    {   
		    return;
	    }
	    //onInnerTabUnload(); //NO NEED TO CALL FROM ALL CURRESPONDING SET METHOD
	    //setValues(getTab());
	    showTab (tabID, url);
	    resetTabHighlight();
	    setTabHighlight(objID);
	    
    }   

    function showTab(tabID, url)
    {
	    var frmProperties = document.frmProps;
	    var frmDummy = document.frmDummyStorage;// ras
	    var tabUrl;
	    
	    
	    changeFrameSrc(0, tabID, url);
	    setTab(tabID);	    
	
	}
	
	function getTab()
    {
	    return document.frmProps.sTab.value;
    } 
    	
	function setTab(tbName)
    {
	    document.frmProps.sTab.value = tbName;
    }
    
	function getPreviousTab()
    {
	    return document.frmProps.sPreviousTab.value;;
    }

	function setPreviousTab(tbName)
    {
	    document.frmProps.sPreviousTab.value = tbName;
    }       
    	
	function changeFrameSrc(frameIndex, tabID, url)
    {
        var modifiedQueryString;        
    
        // If the tabID was already present, we have to strip it out
        var origQueryString = document.frmProps.QueryStringValue.value.toLowerCase();
        
        //alert(origQueryString);
        
        tabID = tabID.toLowerCase();
        tabID = "tabid=" + tabID;       
        
        var tabIDStartingLoc = origQueryString.indexOf("tabid=");        
        
        //alert(tabIDStartingLoc);
        
        if ( tabIDStartingLoc > -1 )
        {
           var tabIDEndingLoc = origQueryString.indexOf("&", tabIDStartingLoc)
           //alert(tabIDEndingLoc);
           
           var tmp = origQueryString.substr(tabIDStartingLoc, tabIDEndingLoc - tabIDStartingLoc);
           
           origQueryString = origQueryString.replace(tmp, "");
           
           modifiedQueryString = origQueryString;
           
        
        }
        else
        {
            modifiedQueryString = origQueryString;
        }
        
    	var partialParamString = "?" + tabID + "&" + modifiedQueryString;
    	partialParamString = partialParamString.replace("&&", "&");
    
        var parDoc = document.parentWindow.parent.document;

        var mainFrame = parDoc.getElementById("main");
        
        url = url + partialParamString;
        
        //alert( url );
        
        //document.frames
        mainFrame.src = url;

    }
    
    /* called on mouseover, and onmouseout of all tab links. */
    function hoverLink(linkObj, hoverTabName, currentTab, makeActive)
    {
	    // if the tab is disabled, dont create any highlighting/unhighlighting effects.
        if (hoverTabName == currentTab)
        {
            return;
        }
                         
	    if (makeActive)
	    {
		    linkObj.className = "hoverTabCenter";
		    linkObj.previousSibling.className = "hoverTabLeft";
            linkObj.nextSibling.className = "hoverTabRight";
	    }
	    else
	    {
	        linkObj.className = "deSTabCenter";
	        linkObj.previousSibling.className = "deSTabLeft";
            linkObj.nextSibling.className = "deSTabRight";
	    }
	}
	
	function resetTabHighlight()
	{		         
	     var previous = getPreviousTab();	    
	     
	     if (previous.length != 0 )
	     {
                  
             // Reset the styles on the previous tab
             var center =  document.getElementById( previous );
             
             if ( center != null )
             {                            
                 center.className = "deSTabCenter";
                 center.previousSibling.className = "deSTabLeft";
                 center.nextSibling.className = "deSTabRight";
             }
         }	
	}
	    
	function setTabHighlight(obj)
    {
        // Set the Currently clicked link to the proper display

         obj.className = "selTabCenter";
         obj.previousSibling.className = "selTabLeft";
         obj.nextSibling.className = "selTabRight";              
         
         setPreviousTab( obj.id );
    }
