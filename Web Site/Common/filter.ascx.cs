namespace activeportal.usercontrols.common
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes;

	/// <summary>
	///		Summary description for filter.
	/// </summary>
	public partial  class filter : System.Web.UI.UserControl
	{

		
		private bool m_showFolders;
		private bool m_showExecutables;
		private string m_filter;
		private bool m_showDocuments;
		private bool m_onlyLatest;
		private bool m_succeeded;
		private bool m_failed;

		public bool ShowFolders
		{
			get {	return AcFldChkShowFolders.Checked;}
			set {	m_showFolders=value;}
		}
		public bool ShowExecutables
		{
			get {	return AcFldChkShowExecutables.Checked;}
			set {	m_showExecutables=value;}
		}
		public bool ShowDocuments
		{
			get {	return AcFldChkShowDocuments.Checked;}
			set {	m_showDocuments=value;}
		}

		public string Filter
		{
			get 
			{
				if ( AcTxtFilter.Text == "" )
				{
					return "*";
				}
				else
				{
					return AcTxtFilter.Text;
				}
			}
			set {	m_filter = value;}
		}
		public bool OnlyLatest
		{
			get { return AcFldChkOnlyLatest.Checked; }
			set { m_onlyLatest = value; }
		}

		public bool ShowSuccessfulJobs
		{
			get
			{
				return AcJobChkSucceeded.Checked ;
			}

			set
			{
				m_succeeded = value;
			}
		}


		// Properties for displaying for Jobs related filters
		public bool DisplaySuccessfulChkBox
		{
			get
			{
				return AcJobChkSucceeded.Visible ;
			}

			set
			{
				AcJobChkSucceeded.Visible = value;
			}
		}

		public bool DisplayFailedChkBox
		{
			get
			{
				return AcJobChkFailed.Visible;
			}

			set
			{
				AcJobChkFailed.Visible = value;
			}
		}



		public bool ShowFailedJobs
		{
			get
			{
				return AcJobChkFailed.Checked ;
			}

			set
			{
				m_failed = value;
			}
		}

		private string m_pageLocation;
		protected void Page_Load(object sender, System.EventArgs e)
		{
			m_pageLocation = ( string ) Context.Items[ "Location" ];

			if ( !IsPostBack )
			{
				// Get filter text from url (filter) or from session (filefilter, jobfilter, channelfilter,noticefilter)
				// Do not set filter to session, as it is used by channels, filesfolders, jobs,...

				String filterText = AcParams.Get( Page, AcParams.Name.filter, AcParams.From.Url);
				AcBtnReset.Attributes.Add( "OnClick", "document.forms[0].elements['" + AcTxtFilter.ClientID + "'].value='" + ActuUtil.jsEncode( m_filter ) + "';" );
				
				switch ( m_pageLocation )
				{
					case "Documents":
					{
						// Get values from url or session and set them to session

						if ( filterText == null )
						{
							filterText = AcParams.Get( Page, AcParams.Name.fileFilter, "", AcParams.From.Session);
						}
						AcTxtFilter.Text = filterText;

						AcMpgCheckBoxes.SelectedIndex = 0;
						

						AcFldChkOnlyLatest.Checked = AcParams.urlCheckboxState( Page, AcParams.Name.onlyLatest, m_onlyLatest, AcParams.From.Url  | AcParams.From.Session, AcParams.SetTo.Session );
						AcFldChkShowFolders.Checked = AcParams.urlCheckboxState( Page, AcParams.Name.showFolders,m_showFolders, AcParams.From.Url  | AcParams.From.Session, AcParams.SetTo.Session  );
						AcFldChkShowDocuments.Checked = AcParams.urlCheckboxState( Page, AcParams.Name.showDocuments,m_showDocuments, AcParams.From.Url  | AcParams.From.Session, AcParams.SetTo.Session  );
						AcFldChkShowExecutables.Checked = AcParams.urlCheckboxState( Page, AcParams.Name.showExecutables, m_showExecutables, AcParams.From.Url  | AcParams.From.Session, AcParams.SetTo.Session  );
						
					}
						break;
					case "Jobs":
					{
						AcMpgCheckBoxes.SelectedIndex = 1;
												
						//AcJobChkSucceeded.Visible = m_dispSuccessfulChkBox;
						//AcJobChkFailed.Visible = m_dispFailedchkBox;
						if ( filterText == null )
						{
							filterText = AcParams.Get( Page, AcParams.Name.jobFilter, "", AcParams.From.Session );
						}

						AcTxtFilter.Text = filterText;
						
						// Display Successful jobs
						AcJobChkSucceeded.Checked = AcParams.urlCheckboxState( Page, AcParams.Name.cbSuccess, true, AcParams.From.Url  | AcParams.From.Session, AcParams.SetTo.Session );

						// Display failed jobs
						AcJobChkFailed.Checked =  AcParams.urlCheckboxState( Page, AcParams.Name.cbFail, true, AcParams.From.Url  | AcParams.From.Session, AcParams.SetTo.Session );


					}
						break;
					case "Channels":
					{
						if ( filterText == null )
						{
							filterText = AcParams.Get( Page, AcParams.Name.channelFilter, "", AcParams.From.Session);
						}
						AcTxtFilter.Text = filterText;

						AcMpgCheckBoxes.SelectedIndex = 2; // no checkbox for channel
					}
						break;
					case "Options":
					{
						filterText = AcParams.Get( Page, AcParams.Name.channelListFilter );
						AcTxtFilter.Text = filterText;

						AcMpgCheckBoxes.SelectedIndex = 2; // no checkbox for Options
						
					}
						break;
					case "BrowseFile":
					{
						AcMpgCheckBoxes.SelectedIndex = 2; // no checkbox 
						AcFldChkShowFolders.Checked = true;
						AcFldChkShowDocuments.Checked = false;
						AcFldChkShowExecutables.Checked = false;
						AcFldChkOnlyLatest.Checked = false;
						break;
					}
				
				}
			}
		}

		private void AcBtnApply_Command( object sender, CommandEventArgs  e )
		{
			// Set everything to session

			switch ( m_pageLocation )
			{
				case "Documents":
				{
					SetDocumentFilterToSession( );
				}
					break;
				case "Jobs":
				{
					SetJobFilterToSession( );
				}
					break;
				case "Channels":
				{
					SetChannelFilterToSession( );
				}
					break;
				case "Options":
				{
					DisableFilter();
				}
					break;
			}
		}
		private void AcBtnReset_Command( object sender, CommandEventArgs  e )
		{
			AcTxtFilter.Text = m_filter;

			switch ( m_pageLocation )
			{
				case "Documents":
				{
					// Reset values

					
					AcFldChkOnlyLatest.Checked =  m_onlyLatest;
					AcFldChkShowFolders.Checked = m_showFolders;
					AcFldChkShowDocuments.Checked = m_showDocuments;
					AcFldChkShowExecutables.Checked = m_showExecutables;

					// Store them into session

					SetDocumentFilterToSession();
				}
					break;
				case "Channels":
				{
					SetChannelFilterToSession();
				}
					break;

				case "Jobs":
				{
					AcJobChkSucceeded.Checked = m_succeeded;
					AcJobChkFailed.Checked = m_failed;
					SetJobFilterToSession( );
				}
					break;
			}
		}

		private void SetChannelFilterToSession()
		{
			// Store values into sessin
			AcParams.SetToSession( Session, AcParams.Name.channelFilter, AcTxtFilter.Text );
		}
		private void SetDocumentFilterToSession ()
		{
			// Store values into session

			AcParams.SetToSession( Session, AcParams.Name.fileFilter, AcTxtFilter.Text );
			AcParams.SetToSession( Session, AcParams.Name.onlyLatest, AcFldChkOnlyLatest.Checked.ToString( ) );
			AcParams.SetToSession( Session, AcParams.Name.showFolders, AcFldChkShowFolders.Checked.ToString( ) );
			AcParams.SetToSession( Session, AcParams.Name.showDocuments, AcFldChkShowDocuments.Checked.ToString( ) );
			AcParams.SetToSession( Session, AcParams.Name.showExecutables, AcFldChkShowExecutables.Checked.ToString( ) );
		}

		private void SetJobFilterToSession( )
		{
			AcParams.SetToSession( Session, AcParams.Name.jobFilter, AcTxtFilter.Text );
			AcParams.SetToSession( Session, AcParams.Name.cbSuccess, AcJobChkSucceeded.Checked.ToString( ) );
			AcParams.SetToSession( Session, AcParams.Name.cbFail, AcJobChkFailed.Checked.ToString( ) );
		}

		// Used For Disbaling the Filter text , and the buttons
		// Currently , since is being used in the Options Tab 
		// we do not have to disable anything else
		private  void DisableFilter()
		{
			switch ( m_pageLocation )
			{
				case "Options":
				{

								
					AcVolumeProperty volumeProperty = AcVolumeManager.GetVolumeProperties( Page ) ;
					long securityLevel = volumeProperty.SecurityIntegrationLevel;
					string[] externalProperties = volumeProperty.ExternalUserProperties;


					if (	securityLevel != 2 && 
						!ActuUtil.IsPropertyExternal( externalProperties, "ChannelName" ))
					{
						return;
					}
					else
					{
						AcBtnApply.Enabled  = false;			
						AcBtnReset.Enabled  = false;
						AcTxtFilter.Enabled = false;		
					}
				}
					break;
			}



			
		}
			
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.AcBtnApply.Command += new System.Web.UI.WebControls.CommandEventHandler(this.AcBtnApply_Command);
			this.AcBtnReset.Command += new System.Web.UI.WebControls.CommandEventHandler(this.AcBtnReset_Command);
		}
		#endregion
	}
}
