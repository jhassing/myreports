	<%@ Control Language="c#" Inherits="activeportal.common.toolbar" CodeFile="toolbar.ascx.cs" %>
<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<table cellspacing=0 cellpadding=2 border=0>
	<tr>
		<td nowrap>
			<localized:AcLabelControl id="AcLblFilter" CssClass="toolbartext" key="toolbar.label.filter" runat="server"/>
		</td>
		
		<td nowrap>
			<localized:AcHyperLinkControl id="AcLnkFilterOn" key="toolbar.label.filter.on" runat="server"/>
		</td>
		<td class="toolbartext" id="AcCellSeparator" runat="server">|</td>
		<td nowrap>
			<localized:AcHyperLinkControl id="AcLnkFilterOff" key="toolbar.label.filter.off" runat="server"/>
		</td>
		
		<td width="100%" class="toolbartext">&nbsp;</td>
		
		<td nowrap align="right">
			<localized:AcLabelControl id="AcLblView" class="toolbartext" key="toolbar.label.view" runat="server"/>
		</td>
		
		<td nowrap align="right">
			<asp:DropDownList class="toolbartext" ID="AcLstFilesView" AutoPostBack="true"  Runat="server"/>
		</td>
	</tr>
</table>