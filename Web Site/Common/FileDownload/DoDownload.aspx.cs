using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace TextFileDwnldC
{
	/// <summary>
	/// Summary description for DoDownload.
	/// </summary>
	public partial class DoDownload : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.Label MainLabel;

		private string sFullPath;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
				try
			{
                sFullPath = Request.Params.Get("Filename");
                string saveFileName = Request.Params.Get("SaveFileName");

				string sNameOnly;

				sNameOnly = sFullPath.Substring(sFullPath.LastIndexOf('\\') + 1);

				System.IO.FileInfo fi = new System.IO.FileInfo(sFullPath);
				String sFileLength = fi.Length.ToString();
			
				Response.ClearHeaders();
				Response.ClearContent();
				Response.Clear();
				Response.ContentType = "text/plain";
				Response.Charset = "";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + saveFileName);
				Response.AddHeader("Content-Length", sFileLength);
				Response.WriteFile(sFullPath);
				Response.Flush();
				Response.Close();

				// Clean up
				string[] sss = sFullPath.Split('\\');
				string tmp = "";

				for (int x=0;x<sss.Length-1;x++)
				{
					tmp = tmp + sss[x].ToString() + @"\";
				}

				System.IO.Directory.Delete(tmp, true);
			}
			catch (Exception ex)
			{
				throw (ex);
			}
			finally
			{
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
		}
		#endregion

	}
}
