<%@ Page language="c#" Inherits="TextFileDwnldC.ConfigDwnld" CodeFile="ConfigDwnld.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Configure and Download a text file.</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<table align="center">
			<tr>
				<td align="center">
					<h1>
						<asp:label id="MainLabel" runat="server" Height="40px" Width="546px">
								Download the file Test.txt
							</asp:label>
					</h1>
				</td>
			</tr>
			<tr>
				<td align=center>
					<h3>
						C# Version
					</h3>
				</td>
			</tr>
		</table>
		<table width="80%" align="center" style="PADDING-RIGHT: 10%; PADDING-LEFT: 10%">
			<tr>
				<td align="center">
					Call the download page using old HTML
				</td>
				<td align="center">
					Call the download page using new ASP.NET
				</td>
			</tr>
			<tr>
				<td align="center" width="50%" style="PADDING-RIGHT: 10%; PADDING-LEFT: 10%">
					<form id="Form1" action="DoDownload.aspx" method="post">
						<input type=hidden  name="Filename" value="<%=sFullPath%>"> <input type="submit" value="Download" name="OldDownload">
					</form>
				</td>
				<td align="center" width="50%" style="PADDING-RIGHT: 10%; PADDING-LEFT: 10%">
					<form id="Form2" action="DoDownload.aspx" method="post" runat="server">
						<asp:Button id="DoDownload" runat="server" Text="Download" onclick="DoDownload_Click"></asp:Button>
					</form>
				</td>
			</tr>
			<tr>
				<td align=center>
					This displays the double-open-dialog behavior.
				</td>
				<td align=center>
					This works properly.  (Or at least, as desired.)
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<INPUT onclick="javascript:history.back()" type="button" value="Back">
				</td>
			</tr>
		</table>
	</body>
</HTML>
