using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace TextFileDwnldC
{
	/// <summary>
	/// Summary description for CfgDoDwnld.
	/// </summary>
	public partial class CfgDoDwnld : System.Web.UI.Page
	{
		const string sFileName = "Test.txt";


		private string sFullPath;
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			string sDirName = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);
			sFullPath = sDirName + "\\" + sFileName;
			System.IO.StreamWriter strWrite = new System.IO.StreamWriter( sFullPath, false );
			strWrite.WriteLine("This is a test text file for the C# Downloader.  It is only a test.");
			strWrite.WriteLine();
			strWrite.WriteLine("In the event that this is replaced by a real downloaded file, then it will contain real, useful data rather than this pedestrian, pedantic, ponderous, pablum.");
			strWrite.WriteLine();
			strWrite.WriteLine("Except for sort-of-useful information for developers at the very end, this concludes this test.");
			strWrite.WriteLine();
			strWrite.WriteLine("It is located at \"{0}\".", sFullPath);
			strWrite.Close();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    

		}
		#endregion

		protected void DoDownload_Click(object sender, System.EventArgs e)
		{
			string sNameOnly;

			sNameOnly = sFileName.Substring(sFileName.LastIndexOf('\\') + 1);

			System.IO.FileInfo fi = new System.IO.FileInfo(sFullPath);
			String sFileLength = fi.Length.ToString();
			Response.ClearHeaders();
			Response.Clear();
			Response.ContentType = "text/plain";
			Response.Charset = "";
			Response.AddHeader("Content-Disposition", "attachment;filename=" + sFileName);
			Response.AddHeader("Content-Length", sFileLength);
			Response.WriteFile(sFullPath);
			Response.Flush();
			Response.Close();
		
		}

	}
}
