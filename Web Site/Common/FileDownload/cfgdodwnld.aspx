<%@ Page language="c#" Inherits="TextFileDwnldC.CfgDoDwnld" CodeFile="CfgDoDwnld.aspx.cs" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Configure and Download a text file.</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Style.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table align="center">
				<tr>
					<td align="center"><h1>
							<asp:Label id="MainLabel" runat="server" Width="546px" Height="40px">
							Download the file Test.txt
						</asp:Label></h1>
					</td>
				</tr>
				<tr>
					<td align=center>
						<h3>
							C# Version
						</h3>
					</td>
				</tr>
			</table>
			<table align="center" width="80%">
				<tr>
					<td align="center" width="50%">
						<INPUT type="button" value="Back" onclick="javascript:history.back()">
					</td>
					<td align="center" width="50%">
						<asp:Button id="DoDownload" runat="server" Text="Download" onclick="DoDownload_Click"></asp:Button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
