namespace DistributionPortal.Common
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	public delegate void SearchClicked();
	public delegate void AddClicked();

	public partial class GridFilter : System.Web.UI.UserControl
	{

		public event SearchClicked Search_OnClick;
		public event AddClicked Add_OnClick;		
		
		#region Properties

		public bool DisplayAddButton
		{
			get
			{
				return this.btnAddNewGroup.Visible;
			}
			set
			{
				this.btnAddNewGroup.Visible = value;
			}
		}

		public string SearchButtonClientID
		{
			get
			{
				return this.btnSearch.ClientID;
			}
		}

		public string AddButtonClientID
		{
			get
			{
				return this.btnAddNewGroup.ClientID;
			}
		}

		public string FilterText
		{
			get
			{
				return this.txtFilter.Text.Trim();
			}
			set
			{
				this.txtFilter.Text = value;
			}
		}

		public Button SearchButton
		{
			get
			{
				return this.btnSearch;
			}
		}

		public Button AddButton
		{
			get
			{
				return this.btnAddNewGroup;
			}
		}

		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		protected void btnSearch_Click(object sender, System.EventArgs e)
		{
			// Raise the search event to the client page.
			if (Search_OnClick != null)
				Search_OnClick();
		}

		protected void btnAddNewGroup_Click(object sender, System.EventArgs e)
		{
			// Raise the Add event to the client page
			if (Add_OnClick != null)
				Add_OnClick();
		}
	}
}
