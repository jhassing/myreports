<%@ Register TagPrefix="actuate_i18n" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<%@ Register TagPrefix="acwc" Namespace="actuate.classes.webcontrols" Assembly="activeportal"%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.common.filter" CodeFile="filter.ascx.cs" %>
<table class="panel" cellSpacing="0" cellPadding="8" width="100%" >
	<tr>
		<td noWrap>
			<table cellSpacing="5" cellPadding="0" border="0">
				<tr>
					<td nowrap><actuate_i18n:aclabelcontrol id="AcFilterLabelControl" CssClass="filterLabel" Key="FLT_FILTER" runat="server"></actuate_i18n:aclabelcontrol>&nbsp;
					</td>
					<td nowrap>
						<%-- TEXTFIELD: THE TEXTFIELD CONTAINING THE FILTER STRING --%>
						<asp:textbox id="AcTxtFilter" Width="120" runat="server"></asp:textbox>
						&nbsp;&nbsp;
						<%-- invisible input to force the user clicking on the apply button --%>
						<input type="text" style="DISPLAY:none">
					</td>
					<td nowrap>
						
							<%-- CHKBOXES DISPLAY APPROPRIATE CHKBOXES DEPENDING ON THE CURRENT PAGE --%>
							<acwc:AcMultiPage id="AcMpgCheckBoxes"  runat="server">
								<%-- FILEFOLDERS CHKBOXES --%>
								<acwc:PageView>
								<table >
									<tr>
										<td nowrap>
											<%-- CHECKBOX: LATEST VERSION --%>
											<actuate_i18n:AcCheckBoxControl id="AcFldChkOnlyLatest" runat="server" Key="FLT_LATEST_ONLY" CssClass="filterLabel" />
											&nbsp;
										</td>
										<td nowrap>
											<%-- CHECKBOX: ENABLE FOLDERS TO BE LISTED --%>
											<actuate_i18n:AcCheckBoxControl id="AcFldChkShowFolders" runat="server" Key="FLT_SHOW_FOLDERS" CssClass="filterLabel" />
										</td>
									</tr>
									<tr>
										<td nowrap>
											<%-- CHECKBOX: ENABLE DOCUMENTS TO BE LISTED --%>
											<actuate_i18n:AcCheckBoxControl id="AcFldChkShowDocuments" runat="server"  Key="FLT_SHOW_DOCUMENTS" CssClass="filterLabel" />
											&nbsp;
										</td>
										<td nowrap>
											<%-- CHECKBOX: ENABLE EXECUTABLES TO BE LISTED --%>
											<actuate_i18n:AcCheckBoxControl id="AcFldChkShowExecutables" runat="server" Key="FLT_SHOW_EXECUTABLES" CssClass="filterLabel" />
										</td>
									</tr>
									</table>
								</acwc:PageView>
								<%-- JOB CHKBOXES --%>
								<acwc:PageView>
								<table>
									<tr>
										<td nowrap>
											<actuate_i18n:AcCheckBoxControl id="AcJobChkSucceeded" runat="server" Checked="true" Key="CB_SUCCEEDED" CssClass="filterLabel" />
										</td>
										<td nowrap>
											<actuate_i18n:AcCheckBoxControl id="AcJobChkFailed" runat="server" Checked="true" Key="CB_FAILED" CssClass="filterLabel" />
										</td>
									</tr>
									</table>
								</acwc:PageView>
								<%-- CHANNEL CHKBOXES --%>
								<acwc:PageView></acwc:PageView>
							</acwc:AcMultipage>
						
					</td>
					<td noWrap>
						<%-- BUTTON: APPLY SUBMITS THE MAIN FORM --%>
						&nbsp;<actuate_i18n:acbuttoncontrol id="AcBtnApply" CommandName="Apply" Key="BTN_APPLY" runat="server"></actuate_i18n:acbuttoncontrol>
						<%-- BUTTON: CLEAR RESETS UI ELEMENT STATES --%>
						<actuate_i18n:acbuttoncontrol id="AcBtnReset" CommandName="Reset" Key="BTN_RESET" runat="server"></actuate_i18n:acbuttoncontrol></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
