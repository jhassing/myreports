<%@ Control Language="c#" AutoEventWireup="false" Src="Schedule.ascx.cs" Codebehind="Schedule.ascx.cs" Inherits="activeportal.common.Schedule" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<%@ Register TagPrefix="acwc" Namespace="actuate.classes.webcontrols" Assembly="activeportal"%>
<table width="100%" height="100%" cellSpacing="0" cellPadding="5">
	<tr>
		<td valign="top">
			<table Width="100%" border="0" CellPadding="0" CellSpacing="1">
				<tr ID="InstructionRow" nowrap  RunAt="server">
				  <td nowrap colspan="3">
				     <b>
				     <localized:AcLabelControl ID="InstructionLbl" Key="query.run.schedule.label.instruction" CssClass="fntQuery" RunAt="Server" />
				     <b>
				     <br>
				     <br>
				  </td>
				</tr>
				<tr >
					<%-- JobName --%>
					<td ID="JobNameCell" Width="30%" CssClass="label" Runat="server">
					  <localized:AcLabelControl CssClass="label" Key="MSGT_JOB_NAME" RunAt="server" />
					</td>
					<td  colspan="2" Width="70%" Runat="server">
						<asp:TextBox Width="50Ex" ID="JobNameTxt" MaxLength="100" Runat="server"></asp:TextBox>
						<span class="label">&nbsp;*</span>
					</td>
				</tr>
				
				<%-- Vertical spacing --%>
				<TR><TD colspan="4"><br></TD></TR>
				
				<%-- Schedule Details: FIELD --%>
				<tr>
					<td  rowspan="10" valign="top">
					  <localized:AcLabelControl Key="MSGT_SCHEDULE_DETAILS" CssClass="label" RunAt="server"/>
					</td>
					<%-- Run now (Immediately): SECTION --%>
					<td width="15%" nowrap>
						<input type="radio" name="ScheduleTypeRdBtn" id="ScheduleNowRdBtn" Checked="true"  runat="server">
						<localized:AcLabelControl  CssClass="label" Key="MSGT_IMMEDIATELY" RunAt="server" />
					</td>
					<td  colspan="2" width="65%">
					</td>
				</tr>
				
				<%-- Vertical spacing --%>
				<TR><TD colspan="3">&nbsp;</TD></TR>
		
			
				<%-- Once (at specified date/time): SECTION --%>
				<tr>
					<td nowrap>
						<input type="radio" name="ScheduleTypeRdBtn" id ="ScheduleOnceRdBtn" RunAt="Server">
						<localized:AcLabelControl CssClass="label" Key="MSGT_ONCE" RunAt="server"/>
					</td>
					<td colspan="2" nowrap>
						<asp:TextBox ID="ScheduleOnceTxt"  RunAt="server"></asp:TextBox>
						<acwc:AcSafeHyperLink ID="CalOnceDateLink" Runat="server"/>
						<acwc:acsafelabel ID="OnceDateFormatLbl" CssClass="label" Runat="server"></acwc:acsafelabel>
					</td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">
						<asp:TextBox MaxLength="14" ID="OnceTimeTxt"  Runat="server"></asp:TextBox>
						<acwc:acsafelabel ID="OnceTimeLbl" cssclass="label" Runat="server"></acwc:acsafelabel>
					</td>
				</tr>
			
				<%-- Vertical spacing --%>
				<TR><TD colspan="3">&nbsp;</TD></TR>
		
				
				<%-- Recurring (at specified dates/times): SECTION --%>
				<tr>
					<td nowrap>
						<input type="radio" name="ScheduleTypeRdBtn" id="ScheduleRecurringRdBtn"  runat="server"> 
						<localized:AcLabelControl CssClass="label" Key="MSGT_RECURRING" RunAt="server"/>
					</td>
					<td  colspan="2">
						<asp:DropDownList ID="WeekDaysList"  Runat="server"></asp:DropDownList>
						<localized:AcLabelControl CssClass="label" Key="MSGT_AT" RunAt="server" />
					</td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
					<td colspan="2">
						<asp:TextBox ID="RecurringTimeTxt"  Runat="server" MaxLength="14"></asp:TextBox>
						<acwc:acsafelabel ID="RecurringTimeLbl" CssClass="label" Runat="server"></acwc:acsafelabel>
					</td>
				</tr>
				
				<%-- Recurring START (specified date/time): SECTION --%>
				<tr>
					<td>
						<input type="checkbox" id="RecurringStartChkBox" size="14" name="useStartDateTime"  RunAt="server" >
						<localized:AcLabelControl CssClass="label" Key="MSGT_START" RunAt="server"/>
					</td>
					<td colspan="2">
						<asp:TextBox ID="StartDateTxt"  MaxLength="14" RunAt="server"></asp:TextBox>
						<acwc:AcSafeHyperLink id="StartDateLink" runat="Server"></acwc:AcSafeHyperLink>
						<acwc:acsafelabel ID="StartDateLbl" CssClass="label" Runat="server"></acwc:acsafelabel>
					</td>
				</tr>
				<%-- Recurring UNTIL (specified date/time): SECTION --%>
				<tr>
					<td>
						<input type="checkbox"  runat="server" size="14" id="useEndDateTime"  name="useEndDateTime">
						<localized:AcLabelControl CssClass="label" Key="MSGT_UNTIL" RunAt="server" />
					</td>
					
					<td colspan="2">
						<asp:TextBox ID="endDateTxt"  MaxLength="14" Runat="server"></asp:TextBox>
						<acwc:AcSafeHyperLink ID="endDateLink" Runat="server"></acwc:AcSafeHyperLink>
						<acwc:acsafelabel ID="EndDateFormatLbl" CssClass="label" Runat="server"></acwc:acsafelabel>
					</td>
				</tr>
				<%-- Vertical spacing --%>
				<TR><TD colspan="4"><br></TD></TR>
				<%-- Priority: FIELD --%>
				<tr id="AcRowPriority" runat="server">
					<td Width="30%" CssClass="label" Runat="server">
						<localized:AcLabelControl CssClass="label" Key="LBL_PRIORITY" RunAt="server" />
					</td>
					<td colspan="2" width="70%" nowrap>
						<asp:RadioButtonList CssClass="label" ID="priorityRadioList" RepeatDirection="Horizontal" RepeatLayout="Flow" runat="server" />
						<asp:TextBox ID="priorityTxt" Enabled="true" Columns="4" MaxLength="4" Runat="server" />
						<input type="hidden" id="maxJobPriorityHidden" name="maxJobPriorityHidden" Runat="server"/>
					</td>					
				</tr>												
			</table>
		</td>
	</tr>
</table>
<input type="hidden" id="scheduleTypeField" Runat="server"/>
<script language="javascript">
  ScheduleNowRadioButton = document.getElementById('<%=ScheduleNowRdBtn.ClientID%>');
  ScheduleOnceRdBtn = document.getElementById('<%=ScheduleOnceRdBtn.ClientID%>');
  ScheduleOnceTxt = document.getElementById('<%=ScheduleOnceTxt.ClientID%>');
  OnceTimeTxt = document.getElementById('<%=OnceTimeTxt.ClientID%>');
  ScheduleRecurringRdBtn = document.getElementById('<%=ScheduleRecurringRdBtn.ClientID%>');
  WeekDaysList = document.getElementById('<%=WeekDaysList.ClientID%>');
  RecurringTimeTxt =  document.getElementById('<%=RecurringTimeTxt.ClientID%>');
  RecurringStartChkBox = document.getElementById('<%=RecurringStartChkBox.ClientID%>');
  StartDateTxt = document.getElementById('<%=StartDateTxt.ClientID%>');
  endDateTxt = document.getElementById('<%=endDateTxt.ClientID%>');
  useEndDateTime = document.getElementById('<%=useEndDateTime.ClientID%>');
  scheduleTypeField = document.getElementById('<%=scheduleTypeField.ClientID%>');
  
  function UpdateScheduleElements()
  {
	 requestType = '<%=m_requestType%>'

     if ( requestType == null  || requestType == '' ||  requestType == 'immediate'  )
     {
        return;
     }
     if ( ScheduleOnceRdBtn.checked  )
     {
		ScheduleOnceTxt.disabled = false;
		OnceTimeTxt.disabled = false;
		scheduleTypeField.value = "once";
	 }
     else if ( ScheduleRecurringRdBtn.checked )
     {
		WeekDaysList.disabled = false;
		RecurringTimeTxt.disabled = false;
		RecurringStartChkBox.disabled = false;
		useEndDateTime.disabled = false;
		
		if ( RecurringStartChkBox.checked )
		{
			 StartDateTxt.disabled = false;
		}
		
		if ( useEndDateTime.checked )
		{
			endDateTxt.disabled = false;
		}        
		
		scheduleTypeField.value = "Recurring";
     }
     else if ( ScheduleNowRadioButton.checked )
     {
		ScheduleOnceTxt.disabled = true;
		OnceTimeTxt.disabled = true;
		WeekDaysList.disabled = true;
	    RecurringTimeTxt.disabled = true;
	    RecurringStartChkBox.disabled = true;
	    StartDateTxt.disabled = true;
		endDateTxt.disabled = true;
	    useEndDateTime.disabled = true;
	    scheduleTypeField.value = "immediate";
  
     }
  }
  
window.onload=UpdateScheduleElements; 
</script>
