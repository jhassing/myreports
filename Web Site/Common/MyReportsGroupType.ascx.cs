namespace DistributionPortal
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;

	using MyReports.Common;

	public partial class MyReportsGroupType : System.Web.UI.UserControl
	{

		#region Properties

		public bool Display_Financial_Option
		{
			get
			{
				return this.chkFinancial.Visible;
			}
			set
			{
                this.chkFinancial.Visible = (value) && (MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin"));
                SetCheck(this.chkFinancial);
			}
		}

		
		public bool Display_Salary_Option
		{
			get
			{
				return this.chkSalary.Visible;
			}
			set
			{
                this.chkSalary.Visible = (value) && (MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin"));
                SetCheck(this.chkSalary);
			}
		}

		
		public bool Display_Hourly_Option
		{
			get
			{
				return this.chkHourly.Visible;
			}
			set
			{
                this.chkHourly.Visible = (value) && (MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin"));
                SetCheck(this.chkHourly);
			}
		}

		
		public bool Display_BlueBook_Option
		{
			get
			{
				return this.chkBlueBook.Visible;
			}
			set
			{
                this.chkBlueBook.Visible = (value) && (MyReports.Common.AppValues.IsInRole("BlueBookDistributionAdmins"));
                SetCheck(this.chkBlueBook);
			}
		}


		public bool Financial_Option_Checked
		{
			get
			{
                if (MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin"))
					return this.chkFinancial.Checked;
				else
					return false;
			}
			set
			{
                if (MyReports.Common.AppValues.IsInRole("MyUnitPortalFinancialUsersAdmin"))
					this.chkFinancial.Checked = value;
				else
					this.chkFinancial.Checked = false;
			}
		}

		
		public bool Salary_Option_Checked
		{
			get
			{
                if (MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin"))
					return this.chkSalary.Checked;
				else
					return false;
			}
			set
			{
                if (MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin"))
					this.chkSalary.Checked = value;
				else
				   this.chkSalary.Checked = false;
			}
		}

		
		public bool Hourly_Option_Checked
		{
			get
			{
                if (MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin"))
					return this.chkHourly.Checked;
				else
					return false;
			}
			set
			{
                if (MyReports.Common.AppValues.IsInRole("MyUnitPortalPayrollUsersAdmin"))
					this.chkHourly.Checked = value;
				else
					this.chkHourly.Checked = false;
			}
		}

		
		public bool BlueBook_Option_Checked
		{
			get
			{
                if (MyReports.Common.AppValues.IsInRole("BlueBookDistributionAdmins"))
					return this.chkBlueBook.Checked;
				else
					return false;
			}
			set
			{
                if (MyReports.Common.AppValues.IsInRole("BlueBookDistributionAdmins"))
					this.chkBlueBook.Checked = value;
				else
					this.chkBlueBook.Checked = false;
			}
		}



		#endregion


        private void SetCheck(CheckBox chk)
        {
            if (chk.Visible)
                chk.Checked = true;
        }
	}
}
