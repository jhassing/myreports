//===========================================================================
// This file was modified as part of an ASP.NET 2.0 Web project conversion.
// The class name was changed and the class modified to inherit from the abstract base class 
// in file 'App_Code\Migrated\Stub_Login_aspx_cs.cs'.
// During runtime, this allows other classes in your web application to bind and access 
// the code-behind page using the abstract base class.
// The associated content page 'Login.aspx' was also modified to refer to the new class name.
// For more information on this code pattern, please refer to http://go.microsoft.com/fwlink/?LinkId=46995 
//===========================================================================
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Web.Security;
using actuate_i18n.classes;
using activeportal.classes.api;
using activeportal.classes;
using activeportal.classes.configuration;
using activeportal.classes.functionality;

using Compass.Security.ActiveDirectory;
using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Common;
using MyReports.DAL;

namespace DistributionPortal
{
    /// <summary>
    /// Summary description for TEST.
    /// </summary>
    public partial class Migrated_Login : System.Web.UI.Page
    {

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            { 
                this.btnLogin.Attributes.Add("onclick", "return login_clicked()");

                m_timeZone = AcParams.Get(Page, AcParams.Name.timezone, m_timeZone, AcParams.From.All);


                // First, did we come from the CAMS page? If not, redirect to CAMS.
                string CAS_Server = "https://cgunx160:10401/CAMS/";
                string MyServer = "https://cgnad00616/";
                string greeting = "World"; // In case I fail

                string uid = Session.Contents["Netid"] != null ? Session.Contents["Netid"].ToString() : "";
                string ticket = Request.QueryString["ticket"] != null ? Request.QueryString["ticket"].ToString() : "";

                string casTicket = "";
                string responseTextFromCas = "";
                string url = "None";

                //			if ( uid.Length == 0 )
                //			{
                //				greeting = "No User Id";
                //				
                //				if ( ticket.Length == 0 ) 
                //				{
                //
                //					// No session, no ticket, Redirect to CAS Logon page
                //					url = CAS_Server+"login?" + "service="+MyServer+"HelloCas/defaultAIX.asp";
                //
                //					Response.Redirect(url);
                //					
                //					Response.End();
                //
                //				} 
                //				else 
                //				{
                //
                //					MSXML2.XMLHTTP30 oXMLHttp = new MSXML2.XMLHTTP30();
                //					
                //					url =CAS_Server+"serviceValidate?ticket="+ticket+"&" + "service="+MyServer+"HelloCas/defaultAIX.asp";
                //
                //					oXMLHttp.open("GET", url, false, "", "" ); // HTTP transaction to CAS server
                //					oXMLHttp.setRequestHeader("content-Type","Text/xml");
                //					oXMLHttp.send( "" );
                //
                //					responseTextFromCas=oXMLHttp.responseText;				
                //				}
                //			}


                // Check to see if this was a timeout issue
                if (Request.QueryString["SignOut"] != null)
                {
                    FailureText.Text = Global.ResourceValue("TimeoutMessage");
                    FailureText.Visible = true;

                }

                if (Request.QueryString["SignIn"] != null)
                {
                    FailureText.Text = Global.ResourceValue("AuthenticationUserFailed");
                    FailureText.Visible = true;
                }
                if (Request.QueryString["errorMessage"] != null)
                {
                    FailureText.Text = Request.QueryString["errorMessage"];
                    FailureText.Visible = true;
                }

                string onLoad = "";

                // --- User ID
                string m_userID = "";

                #region Read the cookie

                HttpCookie myCookie = new HttpCookie("MyReports");
                myCookie = Request.Cookies["MyReports"];

                // Read the cookie information and display it.
                if (myCookie != null)
                    m_userID = myCookie.Value;

                // ------------------------------------------
                // DELETE THIS CODE HERE
                if (Request.QueryString["newId"] != null)
                {
                    m_userID = Request.QueryString["newId"].ToString();
                }

                #endregion

                this.txtUserName.Text = m_userID;

                // --- TimeZone				
                AcTimeZoneDataSet timeZoneMap = AcTimeZoneManager.Instance().GetTimeZoneMap();
                AcTimeZoneDataSet timeZoneMapWithDuplicates = AcTimeZoneManager.Instance().GetTimeZoneMapWithDuplicates();
                AcLstTimeZone.DataSource = timeZoneMap.TimeZone;
                AcLstTimeZone.DataTextField = timeZoneMap.TimeZone.DisplayNameColumn.ColumnName;
                AcLstTimeZone.DataValueField = timeZoneMap.TimeZone.IDColumn.ColumnName;

                AcLstTimeZone.DataBind();
                string query = timeZoneMapWithDuplicates.TimeZone.IDColumn.ColumnName + "='" + m_timeZone + "'";
                System.Data.DataRow[] rows = timeZoneMapWithDuplicates.TimeZone.Select(query);

                ListItem timeZoneToSelect;

                if (rows != null && rows.Length > 0)
                {
                    activeportal.classes.AcTimeZoneDataSet.TimeZoneRow tzr = (activeportal.classes.AcTimeZoneDataSet.TimeZoneRow)rows[0];
                    string m_disp = tzr.DisplayName;
                    timeZoneToSelect = AcLstTimeZone.Items.FindByText(m_disp);
                }
                else
                {
                    timeZoneToSelect = AcLstTimeZone.Items.FindByValue(m_timeZone);
                }

                if (timeZoneToSelect != null)
                {
                    timeZoneToSelect.Selected = true;
                }
                else
                {
                    ListItem PSTitem = AcLstTimeZone.Items.FindByValue("Pacific Standard Time");
                    if (PSTitem != null)
                    {
                        PSTitem.Selected = true;
                    }
                }
            }

        }

        #region Login Button clicked

        /// <summary>
        /// Handles the Click event of the btnLogin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.ImageClickEventArgs"/> instance containing the event data.</param>
        protected void btnLogin_Click(object sender, ImageClickEventArgs e)
        {

            // Clean anything left, just in case
            Response.Cookies.Clear();
            Response.Clear();
            Session.Clear();
            Server.ClearError();

            IUserObject oADUtilities = null;

            try
            {
                if (Page.IsValid)
                {
                    // Get the user info from the text boxes
                    string userId = CleanString.InputText(txtUserName.Text, 20);
                    string password = CleanString.InputText(txtPassword.Text, 30);
                    LDAPConnection ldapConn = new LDAPConnection(AppValues.ServiceAccountUser,
                        AppValues.ServiceAccountPassword,
                        AppValues.FullyQualifiedDomain,
                        AppValues.LdapPort);

                    oADUtilities = new ADUserObject(ldapConn);

                    string distinguishedName = "";

                    if (Compass.Security.ActiveDirectory.LoginResult.LOGIN_OK != oADUtilities.VerifyUserLogin(userId, password, AppValues.LdapBaseOU, ref distinguishedName))
                    {
                        // The user was not authenticated. Redirect back to signin page, and display error message							
                        Page.Response.Redirect("login.aspx?SignIn=failed", false);

                    }
                    else
                    {
                        // Login was Successful
                        #region Write the cookie

                        HttpCookie myCookie = new HttpCookie("MyReports");
                        DateTime now = DateTime.Now;

                        // Set the cookie value.
                        myCookie.Value = userId;

                        // Set the cookie expiration date.
                        myCookie.Expires = now.AddDays(100);

                        // Add the cookie.
                        Response.Cookies.Add(myCookie);

                        #endregion

                        // Set the distinguished Name
                        MyReports.Common.AppValues.distinguishedUserName = distinguishedName;

                        // User name and password are correct.
                        // Group membership for this particular user
                        #region Get User Group Membership

                        // Get the listing of Groups this user belongs too, and put them in a pipe delimited string.				

                        MyReports.Common.AppValues.UserGroupMembership = oADUtilities.GetUsersMembership(distinguishedName, false);


                        #endregion GroupMembership

                        // Does this user have Access to MyReports?					
                        #region MyReport Access

                        new DistributionPortal.GroupMembership().MyReportsUser = false;
                        new DistributionPortal.GroupMembership().PassportUser = false;
                        new DistributionPortal.GroupMembership().BlueBookDistributionUser = false;

                        string temp1 = Functions.GetAttributeValue(oADUtilities.GetUserProperties(distinguishedName, "actuateUser"));

                        if (temp1.ToLower() == "true")
                        {
                            // Set passport access here
                            new DistributionPortal.GroupMembership().PassportUser = true;
                            new DistributionPortal.GroupMembership().MyReportsUser = true;
                        }

                        if (MyReports.Common.AppValues.IsInRole("PassportUsers"))
                            new DistributionPortal.GroupMembership().PassportUser = true;

                        if (MyReports.Common.AppValues.IsInRole("BlueBookDistributionUsers"))
                            new DistributionPortal.GroupMembership().BlueBookDistributionUser = true;

                        #endregion

                        #region Is user a member of a group allowed to use this site

                        if ((!new DistributionPortal.GroupMembership().BlueBookDistributionUser) &&
                             (!new DistributionPortal.GroupMembership().PassportUser) &&
                             (!new DistributionPortal.GroupMembership().MyReportsUser))
                        {
                            //Now, match the list of groups able to use this site up to the list of
                            // Temporarily, this can only be User with Group Membership of BudgetDistributionUsers
                            string m_sGroupsAllowedToUserSite = AppValues.GroupsAllowedToUseSite;
                            if (!Global.IsUserGroupAuthorized(MyReports.Common.AppValues.UserGroupMembership, m_sGroupsAllowedToUserSite))
                            {
                                FailureText.Text = Global.ResourceValue("AuthenticationUserFailed");
                                FailureText.Visible = true;

                                return;
                            }
                        }

                        #endregion

                        #region Create the authetication ticket

                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, userId, DateTime.Now, DateTime.Now.AddMinutes(60), false, "");

                        // Now encrypt the ticket.
                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        // Create a cookie and add the encrypted ticket to the
                        // cookie as data.
                        HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        // Add the cookie to the outgoing cookies collection.					
                        Response.Cookies.Add(authCookie);

                        #endregion

                        #region Store the password and Domain selected for later user

                        MyReports.Common.AppValues.Password = password;
                        MyReports.Common.AppValues.UserName = userId;

                        #endregion

                        #region Log the user name, browser, ect *any other info to SQL

                        //					System.Net.IPHostEntry    TmpEntry    =
                        //						System.Net.Dns.GetHostByAddress(Request.ServerVariables["REMOTE_ADDR"]);
                        //					string workstationName = TmpEntry.HostName;

                        string workstationName = "";

                        new AuditLogging(AppValues.MyReportsDB).InsertAudit(AppValues.FullyQualifiedDomain,
                            Session.SessionID, userId, Request.Browser.Platform, Request.Browser.Browser,
                            Request.Browser.Version, Request.UserHostAddress, workstationName);

                        #endregion

                        // Redirect the user to the originally requested page
                        if ((new DistributionPortal.GroupMembership().MyReportsUser) || (new DistributionPortal.GroupMembership().BlueBookDistributionUser))
                        {

                            #region Actuate User, Login and Transfer

                            //m_userID = AcParams.Get( Page, AcParams.Name.userID, m_userID, AcParams.From.All );
                            m_serverURL = AcParams.Get(Page, AcParams.Name.serverURL, m_serverURL, AcParams.From.Url);
                            //m_volume = AcParams.Get(Page, AcParams.Name.volume, m_volume, AcParams.From.Url | AcParams.From.Cookie );
                            m_timeZone = AcParams.Get(Page, AcParams.Name.timezone, m_timeZone, AcParams.From.All);
                            m_language = AcParams.Get(Page, AcParams.Name.locale, m_language, AcParams.From.All);
                            m_loginTarget = AcParams.Get(Page, AcParams.Name.targetPage, m_loginTarget, AcParams.From.All);

                            AcParams.SetToSession(Session, AcParams.Name.timezone, AcLstTimeZone.SelectedItem.Value.ToString());
                            AcParams.SetToCookie(Response, AcParams.Name.timezone, AcLstTimeZone.SelectedItem.Value.ToString());
                            AcParams.SetToSession(Session, AcParams.Name.timezoneName, AcLstTimeZone.SelectedItem.Text);

                            //m_password = AcParams.Get(Page, AcParams.Name.password, m_password);

                            AcParams.SetToSession(Session, AcParams.Name.serverURL, m_serverURL);
                            AcParams.SetToSession(Session, AcParams.Name.targetPage, m_loginTarget);

                            ViewState["serverURL"] = m_serverURL;
                            AcParams.SetToSession(Session, AcParams.Name.serverURL, ViewState["serverURL"]);

                            AcGetSystemVolumeNames volumeList = new AcGetSystemVolumeNames(true);

                            AcParams.SetToSession(Session, AcParams.Name.userID, userId);
                     //       AcParams.SetToCookie(Response, AcParams.Name.userID, password);
                            AcParams.SetToSession(Session, AcParams.Name.password, password);
                            AcParams.SetToCookie(Response, "AcSE", "1");

                            Response.Redirect("~/authenticate.aspx?redirectafterlogin=" + HttpUtility.UrlEncode(m_loginTarget), false);

                            #endregion
                        }
                        else
                        {
                            Response.Redirect(m_loginTarget, false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                ExceptionManager.Publish(ex);

                Page.Response.Redirect("login.aspx?errorMessage=" + ex.Message, false);

            }
        }


        #endregion

        #region Actuate Properties
        String m_userID = "";

        /// <summary>
        /// Gets or sets the user ID.
        /// </summary>
        /// <value>The user ID.</value>
        public String UserID
        {
            get { return m_userID; }
            set { m_userID = value; }
        }

        String m_password = "";

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        public String Password
        {
            get { return m_password; }
            set { m_password = value; }
        }

        // Set m_volume to a default value to select a volume as default in the volume list based
        // on the following overriding rule - 
        // 1. The volume parameter value in the URL
        // 2. If not 1, then system default volume as returned by the iServer system
        // 3. If not 1 and 2, then the value set here for m_volume
        String m_volume = null;

        /// <summary>
        /// Gets or sets the volume.
        /// </summary>
        /// <value>The volume.</value>
        public String Volume
        {
            get { return m_volume; }
            set { m_volume = value; }
        }

        String m_serverURL = ConfigurationSettings.AppSettings["SERVER_DEFAULT"];

        /// <summary>
        /// Gets or sets the server URL.
        /// </summary>
        /// <value>The server URL.</value>
        public String ServerURL
        {
            get { return m_serverURL; }
            set { m_serverURL = value; }
        }
        String m_timeZone = ConfigurationSettings.AppSettings["DEFAULT_TIMEZONE"];

        /// <summary>
        /// Gets or sets the time zone.
        /// </summary>
        /// <value>The time zone.</value>
        public String TimeZone
        {
            get { return m_timeZone; }
            set { m_timeZone = value; }
        }
        String m_language = ConfigurationSettings.AppSettings["DEFAULT_LOCALE"];

        /// <summary>
        /// Gets or sets the language.
        /// </summary>
        /// <value>The language.</value>
        public String Language
        {
            get { return m_language; }
            set { m_language = value; }
        }

        String m_loginTarget = "index/index.aspx";
        
       // Used for redirecting to validate perno Id's
       // String m_loginTarget = "uidchanged/UidChangeMessage.aspx";

        /// <summary>
        /// Gets or sets the login target.
        /// </summary>
        /// <value>The login target.</value>
        public String LoginTarget
        {
            get { return m_loginTarget; }
            set { m_loginTarget = value; }
        }

        // Property to get system Name returned from server.

        String m_systemName = null;

        /// <summary>
        /// Handles the SelectedIndexChanged event of the AcLstLocale control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void AcLstLocale_SelectedIndexChanged(object sender, System.EventArgs e)
        {

        }

        /// <summary>
        /// Gets the name of the system.
        /// </summary>
        /// <value>The name of the system.</value>
        public String SystemName
        {
            get { return m_systemName; }
        }
        #endregion
    }
}