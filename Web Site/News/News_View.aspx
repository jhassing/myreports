<%@ Page Language="C#" AutoEventWireup="true" CodeFile="News_View.aspx.cs" Inherits="News_News_View" %>
<%@ Register TagPrefix="Club" Namespace="ClubSite" %>
<%@ Register Src="../Common/DateandTimePicker.ascx" TagName="DateandTimePicker" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <LINK href="../css/allstyles.css" type="text/css" rel="stylesheet">
    <style>
        .date {font-family:verdana,Arial,Helvetica,sans-serif;font-size:0.8em;color:#000000}
        .content {font-family:verdana,Arial,Helvetica,sans-serif;font-size:0.7em;color:#000000}
        .heading {font-family:Arial,Helvetica,sans-serif;font-size:1.0em;color:#FFFFFF}
    </style>	    	 
</head>
<body class="TA">
    <form id="form1" runat="server" class="TA">
    <div class="rightblock">
        <table class="TA" width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#0000cc">
            <tr>
				<td height="21" bgcolor="#0000cc" class="heading">Announcements</td>
			  </tr>
			  <tr>
				  <td style="height: 206px">
					  <table width="100%" border="0" cellspacing="0" cellpadding="10">						  	    
					   <tr>
					     <td>
					        

												       					        

                <asp:FormView ID="FormView1" runat="server" DataSourceID="SqlDataSource1" DataKeyNames="id"
                    Width="444px" >
                    <EmptyDataTemplate>
                                         <asp:Panel runat="server" ID="panel11" CssClass="actionbuttons" Visible='<%# MyReports.Common.AppValues.IsInRole("MyReportsAdministrators") %>'>
                            
                            <Club:RolloverLink ID="btnInsertNew1" runat="server" Text="Insert Article" OnClick="btnInsertNew_Click"  />
                        </asp:Panel>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <h2>
                            <asp:Label Text='<%# Eval("title") %>' runat="server" ID="titleLabel" />
                        </h2>
                        <div class="itemdetails">
                            <p>
                                <asp:Label CssClass="date" Text='<%# Eval("itemdate","{0:D}") %>' runat="server" ID="itemdateLabel" />
                            </p>
                        </div>
                        <p>
                            <asp:Label CssClass="content" Text='<%# ReplaceDatabaseString(Eval("description").ToString()) %>' runat="server" ID="descriptionLabel" />
                        </p>
                        <asp:Panel runat="server" ID="panel1" CssClass="actionbuttons" Visible='<%# MyReports.Common.AppValues.IsInRole("MyReportsAdministrators") %>'>
                            <Club:RolloverLink ID="btnEdit" runat="server" Text="Edit Article" OnClick="btnEdit_Click" />
                            <Club:RolloverLink ID="btnInsertNew" runat="server" Text="Insert Article" OnClick="btnInsertNew_Click"  />
                        </asp:Panel>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <div class="fullwidth">
                            <h3>
                                Edit News Article</h3>
                            <div class="dashedline">
                            </div>
                            <div class="actionbuttons">
                                <Club:RolloverButton ID="btnSaveChangesTop" CommandName="Update" Text="Save Changes" runat="server" />
                                <Club:RolloverLink ID="btnCancelChangesTop" Text="Cancel" runat="server" OnClick="btnCancelChangesTop_Click"  />
                            </div>
                            <div class="dashedline">
                            </div>
                            <table>
                                <tr>
                                    <td class="formlabel">
                                        Article Title:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="titleTextBox" runat="server" Width="500px" Text='<%# Bind("title") %>' ReadOnly="True"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formlabel">
                                        <asp:Label ID="Label1" runat="server" Text="Description:" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox Text='<%# Bind("description") %>' runat="server" ID="descriptionTextBox"
                                            Rows="10" TextMode="MultiLine" Width="500px" Height="166px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formlabel">
                                        Article Date</td>
                                    <td align="left">
                                        <p>
                                            The news article will not be visible to users until after this date.
                                        </p>                                
                                        <uc1:DateandTimePicker ID="dtpicker" runat="server" selectedDateTime='<%#Bind("itemdate") %>' />
                                    </td>
                                </tr>
                            </table>
                            <div class="dashedline">
                            </div>
                            <div class="actionbuttons">
                                <Club:RolloverButton ID="btnSaveChangesBottom" CommandName="Update" Text="Save Changes" runat="server" />
                                <Club:RolloverLink ID="btnCancelChangesBottom" Text="Cancel" runat="server" OnClick="btnCancelChangesBottom_Click" />
                            </div>
                        </div>
                </EditItemTemplate>
                   <InsertItemTemplate>
                        <div class="fullwidth">
                            <h3>
                                New News Article</h3>
                            <div class="dashedline">
                            </div>
                            <div class="actionbuttons">
                                <Club:RolloverButton ID="btnSaveChangesTop" CommandName="Insert" Text="Save Changes" runat="server" />
                                <Club:RolloverLink ID="btnCancelChangesTop" Text="Cancel" runat="server" OnClick="btnCancelChangesTop_Click"  />
                            </div>
                            <div class="dashedline">
                            </div>
                            <table>
                                <tr>
                                    <td class="formlabel">
                                        Article Title:
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="titleTextBox" runat="server" Width="500px" Text='<%# Bind("title") %>'></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formlabel">
                                        <asp:Label ID="Label1" runat="server" Text="Description:" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox Text='<%# Bind("description") %>' runat="server" ID="descriptionTextBox"
                                            Rows="10" TextMode="MultiLine" Width="500px" Height="166px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="formlabel">
                                        Article Date</td>
                                    <td align="left">
                                        <p>
                                            The news article will not be visible to users until after this date.
                                        </p>                                
                                        <uc1:DateandTimePicker ID="dtpicker" runat="server" selectedDateTime='<%#Bind("itemdate") %>' />
                                    </td>
                                </tr>
                            </table>
                            <div class="dashedline">
                            </div>
                            <div class="actionbuttons">
                                <Club:RolloverButton ID="btnSaveChangesBottom" CommandName="Insert" Text="Save Changes" runat="server" />
                                <Club:RolloverLink ID="btnCancelChangesBottom" Text="Cancel" runat="server" OnClick="btnCancelChangesBottom_Click" />
                            </div>
                        </div>
                </InsertItemTemplate>  
             
            </asp:FormView>    
            
                            					     </td>
					   </tr>					   				
				      </table>
				  </td>
			  </tr>		 
		 </table>   
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MyReportsConnectionString %>"
                    SelectCommand="usp_Get_LatestAnnouncement" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSource1_Selecting" UpdateCommand="usp_Update_Announcements" UpdateCommandType="StoredProcedure" OnUpdating="SqlDataSource1_Updating" InsertCommand="usp_Insert_Announcements" InsertCommandType="StoredProcedure" OnInserting="SqlDataSource1_Inserting" >
                        <UpdateParameters>
                            <asp:Parameter Name="ai_id" Type="Int32" />
                            <asp:Parameter Name="ad_itemdate" Type="DateTime" />
                            <asp:Parameter Name="as_description" Type="String" />
                            <asp:Parameter Name="as_ModifiedBy" Type="String" />
                        </UpdateParameters>
                        <InsertParameters>
                            <asp:Parameter Name="ad_itemdate" Type="DateTime" />
                            <asp:Parameter Name="as_title" Type="String" />
                            <asp:Parameter Name="as_description" Type="String" />
                            <asp:Parameter Name="as_CreatedBy" Type="String" />
                        </InsertParameters>
                </asp:SqlDataSource>
                   
        </div>
    </form>
</body>
</html>
