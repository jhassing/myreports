using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class News_News_View : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
               
    }
    protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Connection.ConnectionString = MyReports.Common.AppValues.MyReports_DB_ConnectionString;
    }
    protected void SqlDataSource1_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        e.Command.Connection.ConnectionString = MyReports.Common.AppValues.MyReports_DB_ConnectionString;
        e.Command.Parameters.Clear();

        e.Command.Parameters.Add(new SqlParameter("@ai_id", FormView1.DataKey.Value));
        e.Command.Parameters.Add(new SqlParameter("@ad_itemdate", ((DateandTimePicker)this.FormView1.FindControl("dtpicker")).selectedDateTime));
        e.Command.Parameters.Add(new SqlParameter("@as_description", ((TextBox)this.FormView1.FindControl("descriptionTextBox")).Text));
        e.Command.Parameters.Add(new SqlParameter("@as_ModifiedBy", MyReports.Common.AppValues.UserName));
    }

    protected void SqlDataSource1_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        e.Command.Connection.ConnectionString = MyReports.Common.AppValues.MyReports_DB_ConnectionString;
        e.Command.Parameters.Clear();

        e.Command.Parameters.Add(new SqlParameter("@ad_itemdate", ((DateandTimePicker)this.FormView1.FindControl("dtpicker")).selectedDateTime));
        e.Command.Parameters.Add(new SqlParameter("@as_title", ((TextBox)this.FormView1.FindControl("titleTextBox")).Text));
        e.Command.Parameters.Add(new SqlParameter("@as_description", ((TextBox)this.FormView1.FindControl("descriptionTextBox")).Text));
        e.Command.Parameters.Add(new SqlParameter("@as_CreatedBy", MyReports.Common.AppValues.UserName));
    }

    protected void btnCancelChangesTop_Click(object sender, EventArgs e)
    {
        SwitchBackToEditMode();
    }
    protected void btnCancelChangesBottom_Click(object sender, EventArgs e)
    {
        SwitchBackToEditMode();
    }

    private void SwitchBackToEditMode()
    {
        FormView1.ChangeMode(FormViewMode.ReadOnly);
    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        FormView1.ChangeMode(FormViewMode.Edit);
    }

    protected void btnInsertNew_Click(object sender, EventArgs e)
    {
        FormView1.ChangeMode(FormViewMode.Insert);                
    }

    protected string ReplaceDatabaseString(string old)
    {

        return old.Replace("\r\n", "<br>");

    }


}
