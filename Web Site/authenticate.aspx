<%@ Page language="c#" Inherits="activeportal.Migrated_authenticate" CodeFile="authenticate.aspx.cs" %>
<%@ Register TagPrefix="actuate" TagName="authenticate" Src="authenticate.ascx" %>
<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>authenticate</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body>
		<form method="post" runat="server">
			<actuate:authenticate id="Authenticate1" runat="server"/>
		</form>
	</body>
</HTML>
