using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public enum enumApp
	{
		rd=1,
		bd=2,
		rd_admin=3,
		user_management=4,
		bbd=5,
		bbd_admin=6,
		passport=7,
		passport_admin=8,
        rd_selectedfiles=9,
        viewed_reports=10,
        yea=11
	}

public class QueryStringValues : System.Web.UI.UserControl
{
    public static string GetRedirectString(int step)
    {
        HttpRequest request = HttpContext.Current.Request;

        return "../UserManagement/UserManagementDefault.aspx?app=user_management&addnewuser=0&step=" + step.ToString();
    }

    public static enumApp App
    {
        get
        {
            HttpRequest request = HttpContext.Current.Request;

            string app = "";
            enumApp nApp = 0;

            if (request.QueryString["app"] != null)
                app = request.QueryString["app"].ToString();

            switch (app.ToLower())
            {
                case "rd":
                    nApp = enumApp.rd;
                    break;

                case "bd":
                    nApp = enumApp.bd;
                    break;
                case "yea":
                    nApp = enumApp.yea;
                    break;
                case "rd_admin":
                    nApp = enumApp.rd_admin;
                    break;

                case "rd_selectedfiles":
                    nApp = enumApp.rd_selectedfiles;
                    break;

                case "user_management":
                    nApp = enumApp.user_management;
                    break;

                case "bbd":
                    nApp = enumApp.bbd;
                    break;

                case "bbd_admin":
                    nApp = enumApp.bbd_admin;
                    break;

                case "passport":
                    nApp = enumApp.passport;
                    break;

                case "passport_admin":
                    nApp = enumApp.passport_admin;
                    break;

                case "viewed_reports":
                    nApp = enumApp.viewed_reports;
                    break;
            }

            return (nApp);
        }

    }

    public static int Step(HttpRequest request)
    {
        int step = 0;

        if (request.QueryString["step"] != null)
            step = System.Convert.ToInt32(request.QueryString["step"]);

        return (step);
    }


    public static int AdminTabIndex()
    {
        HttpRequest request = HttpContext.Current.Request;

        int index = 0;

        if (request.QueryString["rdAdmintabIndex"] != null)
            index = System.Convert.ToInt32(request.QueryString["rdAdmintabIndex"]);

        return (index);
    }

    public static string getQueryStringValue(string Value)
    {
        HttpRequest request = HttpContext.Current.Request;

        if (request.QueryString[Value] != null)
            return request.QueryString[Value].ToString().ToLower();
        else
            return null;
    }
}

