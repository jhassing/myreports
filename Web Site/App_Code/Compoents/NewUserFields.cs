using System;
using System.Collections;
using System.Web;

namespace DistributionPortal
{
	public class NewUserFields
	{
		
		#region Requestors Information

		public static string RequestorsFirstName
		{
			set
			{
				HttpContext.Current.Session.Add("RequestorsFirstName", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["RequestorsFirstName"] );
			}
		}

		public static string RequestorsLastName
		{
			set
			{
				HttpContext.Current.Session.Add("RequestorsLastName", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["RequestorsLastName"] );
			}
		}
		public static string RequestorsPhone
		{
			set
			{
				HttpContext.Current.Session.Add("RequestorsPhone", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["RequestorsPhone"] );
			}
		}
		public static string RequestorsEmail
		{
			set
			{
				HttpContext.Current.Session.Add("RequestorsEmail", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["RequestorsEmail"] );
			}
		}

		public static string RequestorsDepartment
		{
			set
			{
				HttpContext.Current.Session.Add("RequestorsDepartment", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["RequestorsDepartment"] );
			}
		}

		#endregion

		#region Associates Information

		public static string AssociatesPernaNumber
		{
			set
			{
				HttpContext.Current.Session.Add("AssociatesPernaNumber", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["AssociatesPernaNumber"] );
			}
		}

		public static string AssociatesFirstName
		{
			set
			{
				HttpContext.Current.Session.Add("AssociatesFirstName", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["AssociatesFirstName"] );
			}
		}

		public static string AssociatesLastName
		{
			set
			{
				HttpContext.Current.Session.Add("AssociatesLastName", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["AssociatesLastName"] );
			}
		}

		public static int AssociatesLocation_Index
		{
			set
			{
				HttpContext.Current.Session.Add("AssociatesLocation_Index", value);
			}
			get
			{
				return System.Convert.ToInt32( HttpContext.Current.Session["AssociatesLocation_Index"] );
			}
		}

		public static string AssociatesLocation
		{
			set
			{
				HttpContext.Current.Session.Add("AssociatesLocation", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["AssociatesLocation"] );
			}
		}

		public static string AssociatesDepartment
		{
			set
			{
				HttpContext.Current.Session.Add("AssociatesDepartment", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["AssociatesDepartment"] );
			}
		}

		public static int AssociatesCompany_Index
		{
			set
			{
				HttpContext.Current.Session.Add("AssociatesCompany_Index", value);
			}
			get
			{
				return System.Convert.ToInt32( HttpContext.Current.Session["AssociatesCompany_Index"] );
			}
		}

		public static string AssociatesCompany
		{
			set
			{
				HttpContext.Current.Session.Add("AssociatesCompany", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["AssociatesCompany"] );
			}
		}

		public static string AssociatesPhone
		{
			set
			{
				HttpContext.Current.Session.Add("AssociatesPhone", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["AssociatesPhone"] );
			}
		}

		public static string AssociatesTitle	
		{
			set
			{
				HttpContext.Current.Session.Add("AssociatesTitle", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["AssociatesTitle"] );
			}
		}

		public static bool MyReportsUser	
		{
			set
			{
				HttpContext.Current.Session.Add("NewMyReportsUser", value);
			}
			get
			{
				if (HttpContext.Current.Session["NewMyReportsUser"] == null)
					return true;

				return (bool)HttpContext.Current.Session["NewMyReportsUser"];
			}
		}

		public static bool BudgetDistributionUser	
		{
			set
			{
				HttpContext.Current.Session.Add("NewBudgetDistributionUser", value);
			}
			get
			{
				if (HttpContext.Current.Session["NewBudgetDistributionUser"] == null)
					return false;

				return (bool)HttpContext.Current.Session["NewBudgetDistributionUser"];
			}
		}

		public static bool PassportUser	
		{
			set
			{
				HttpContext.Current.Session.Add("NewPassportUser", value);
			}
			get
			{
				if (HttpContext.Current.Session["NewPassportUser"] == null)
					return false;

				return (bool)HttpContext.Current.Session["NewPassportUser"];
			}
		}

		public static bool BlueBookDistributionUser	
		{
			set
			{
				HttpContext.Current.Session.Add("NewBlueBookDistributionUser", value);
			}
			get
			{
				if (HttpContext.Current.Session["NewBlueBookDistributionUser"] == null)
					return false;

				return (bool)HttpContext.Current.Session["NewBlueBookDistributionUser"];
			}
		}

		#endregion

		#region Group Information

		public static ArrayList rdNewUsersGroups
		{
			set
			{
				HttpContext.Current.Session.Add("rdNewUsersGroups", value);
			}
			get
			{
				return (ArrayList)HttpContext.Current.Session["rdNewUsersGroups"] ;
			}
		}

		#endregion

		#region Home Folder Information

		public static string AssociatesHomeFolder
		{
			set
			{
				HttpContext.Current.Session.Add("AssociatesHomeFolder", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["AssociatesHomeFolder"] );
			}
		}
		public static string MainSelection
		{
			set
			{
				HttpContext.Current.Session.Add("MainSelection", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["MainSelection"] );
			}
		}
		public static string SubSelection
		{
			set
			{
				HttpContext.Current.Session.Add("SubSelection", value);
			}
			get
			{
				return System.Convert.ToString( HttpContext.Current.Session["SubSelection"] );
			}
		}

		#endregion
		
	}
}
