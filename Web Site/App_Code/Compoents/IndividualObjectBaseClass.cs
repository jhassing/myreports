using System;

using MyReports.UserManagementUI.Common;

using DistributionPortal;


namespace MyReports.Components
{
	/// <summary>
	/// Summary description for IndividualObjectBaseClass.
	/// </summary>
	public class IndividualObjectBaseClass : ClientSideControl
	{

		#region Properties

		/// <summary>
		/// Handles the Distinguished name property
		/// </summary>
		public string DistinguishedName
		{
			get
			{
				return ViewState["DistinguishedName"] != null ? ViewState["DistinguishedName"].ToString().Trim() : "";
			}
			set
			{
				ViewState["DistinguishedName"] = value.ToString().Trim();
			}

		}


		/// <summary>
		/// Handle the return
		/// </summary>
		public bool Return
		{
			get
			{
				//return ViewState["Return"] != null ? System.Convert.ToBoolean( ViewState["Return"].ToString() ) : false;
                return false;
			}
			set
			{
				//ViewState["Return"] = value;
				//this.Tab = 0;
				//this.Message1.HideMessage();
				
			}
		}


		/// <summary>
		/// Which tab is selected
		/// </summary>
		public int Tab
		{
			get
			{
				//return tbs.SelectedTab;
                return 0;
			}
			set
			{
				//tbs.SelectedTab = value;
			}
		}


        public string getTabID
        {
            get
            {
                string tabID = "";

                if (Request.QueryString["tabID"] != null)
                    tabID = Request.QueryString["tabID"].ToString();
                else
                    tabID = "";

                return tabID;
            }
        }


		#endregion

		protected virtual void LoadData()
		{

		}

		protected virtual bool SaveData()
		{
			return false;
		}

		protected virtual void Tab_OnClick(int tabClicked)
		{

		}

		protected virtual void DoReturn()
		{
			string sf = "";
		}
		
		protected override void OnLoad(EventArgs e)
		{
            if (!Page.IsPostBack)
            {
                base.OnLoad(e);

                this.DistinguishedName = Request.QueryString["distinguishedName"].ToString();
            }
		}

		#region Apply Button Clicked

		private void BottomButtons1_ApplyButton_OnClick()
		{
			SaveData();
		}

		#endregion

		#region Save Button Clicked
		
		private void BottomButtons1_OKButton_OnClick()
		{
			if ( SaveData() )
				DoReturn();
		}


		#endregion

		#region Back Button clicked

		private void BottomButtons1_BackButton_OnClick()
		{
			DoReturn();
		}


		#endregion

	}
}