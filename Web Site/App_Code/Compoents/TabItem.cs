using System;

[Serializable]
public struct TabItem
{
    private string _name;
    private string _id;
    private string _url;

    public TabItem(string name, string id, string url)
    {
        _name = name;
        _id = id;
        _url = url;
    }

    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    public string Id
    {
        get { return _id; }
        set { _id = value; }
    }

    public string Url
    {
        get { return _url; }
        set { _url = value; }
    }
}