namespace Compass.Web.WebControls
{
	using System;
	using System.Globalization;
	using System.Reflection;
	using System.Resources;
	using System.Threading;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.WebControls;


	public class ResourceFactory 
	{
		static ResourceManager _rm;

		public static ResourceManager RManager 
		{
			get 
			{
				if(_rm == null) 
				{
					_rm = new ResourceManager( "MyReports",
						Assembly.GetExecutingAssembly(),
						null);
				}
				return _rm;
			}
		}
	}

	public class CgLabelControl : System.Web.UI.WebControls.Label 
	{
		override protected void Render (HtmlTextWriter writer) 
		{
            //Text = ResourceFactory.RManager.GetString(Text);

            Text = Text;
            
            base.Render(writer);
		}
	}

	public class CgLinkButtonControl : System.Web.UI.WebControls.LinkButton 
	{
		override protected void Render (HtmlTextWriter writer) 
		{
			//Text = ResourceFactory.RManager.GetString(Text);

            Text = Text;

			base.Render(writer);
		}
	}

	public class CgButtonControl : System.Web.UI.WebControls.Button 
	{
		override protected void Render (HtmlTextWriter writer) 
		{
			//Text = ResourceFactory.RManager.GetString(Text);

            Text = Text;

			base.Render(writer);
		}
	}


}


