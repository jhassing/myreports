using System;
using MyReports.UserManagementUI.Common;

using DistributionPortal;


namespace MyReports.Components
{
	public class NewObjectBaseClass	: ClientSideControl
	{
		protected System.Web.UI.WebControls.Button btnBack;
		protected System.Web.UI.WebControls.Button btnNext;
		protected System.Web.UI.WebControls.Button btnCancel;

		protected System.Web.UI.WebControls.Panel panTopPan;
		protected System.Web.UI.WebControls.Label lblCreateIn;

		protected Message				Message1;
		
		#region Properties

		public bool AlreadyLoadedExchangeInfo
		{
			get
			{
				return ViewState["AlreadyLoadedExchangeInfo"] != null ? System.Convert.ToBoolean( ViewState["AlreadyLoadedExchangeInfo"] ) : false;
			}
			set
			{
				ViewState["AlreadyLoadedExchangeInfo"] = value;
			}																	

		}

		public int Step
		{

			get
			{
				return ViewState["Step"] != null ? System.Convert.ToInt32( ViewState["Step"].ToString() ) : 1;
			}
			set
			{
				if ( value < 1 )
					value = 1;

				if ( value > 5 )
					value = 5;
					
				ViewState["Step"] = value;
			}
		}

		/// <summary>
		/// Handle the return
		/// </summary>
		public bool Return
		{
			get
			{
				return ViewState["Return"] != null ? System.Convert.ToBoolean( ViewState["Return"].ToString() ) : false;
			}
			set
			{
				ViewState["Return"] = value;
				this.Message1.HideMessage();
			}
		}


		#endregion

		#region DoReturn

		protected void DoReturn()
		{
			// Tell the calling page we are returning to him
			// do not load this page again
			Response.Redirect( "../index/index.aspx?app=user_management&amp;Home=0&amp;userManagementTabIndex=0" );
		}


		#endregion

		#region Cancel Button Clicked

		protected void btnCancel_Click(object sender, System.EventArgs e)
		{
			DoReturn();
		}

		#endregion

		#region Back Button Clicked

		protected void btnBack_Click(object sender, System.EventArgs e)
		{
			this.Step = this.Step - 1;
			this.Message1.HideMessage();
		}

		
		#endregion

	}
}
