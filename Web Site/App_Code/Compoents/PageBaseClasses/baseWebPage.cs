using System;
using System.Collections;
using System.Text;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;

namespace DistributionPortal
{
	/// <summary>
	/// Summary description for baseWebPage.
	/// </summary>
	public class baseWebPage : System.Web.UI.Page
	{
		public void RegisterScript(string name, string script)
		{
			this.RegisterStartupScript(name, script);
		}

		protected void RegisterTimeoutScript()
		{
			StringWriter str = new StringWriter();
			
			str.WriteLine("<script language='javascript'>\r\n");
			str.WriteLine("window.setTimeout(\"window.navigate('../Login.aspx?SignOut=5')\", " + ConfigurationSettings.AppSettings["IdleTimeout"] + " * 60000);");
			str.WriteLine("</script>");
		
			this.RegisterStartupScript("timeoutscript",str.ToString());
		}
	}
}
