using System;
using System.Web.UI.WebControls;

namespace DistributionPortal
{
	/// <summary>
	/// Summary description for ClientSidePage.
	/// </summary>
	public class ClientSidePage	: System.Web.UI.Page
	{

		public string getOpenScriptTag()
		{
			string hh = "\r\n";
			hh += "\t <script language=\"javascript\"> <!-- \r\n";

			return hh;
		}

		public string getCloseScriptTag()
		{
			return "\t //--> </script>";
		}

		public void RegisterOnchangeScript()
		{
			if (!Page.IsClientScriptBlockRegistered("CSP-onchange-function") )
			{
				string hh = getOpenScriptTag();

				hh += "\t var isDirty= false; \r\n";
				hh += getCloseScriptTag();

				Page.RegisterClientScriptBlock("CSP-onchange-function", hh);
			}
		}

		public void MonitorChanges(WebControl c)
		{
			RegisterOnchangeScript();

			if (	c.GetType().ToString() == "System.Web.UI.WebControls.CheckBox" ||
					c.GetType().ToString() == "System.Web.UI.WebControls.CheckBoxList" ||
					c.GetType().ToString() == "System.Web.UI.WebControls.RadioButtonList" )
			{
				c.Attributes.Add("onclick", "isDirty = true");
				
			}
			else
			{
				c.Attributes.Add("onchange", "isDirty = true");
			}					 
		}

		public string QuestionDialogScript(string headerText, string descriptionText, string buttonOneText, string buttonTwoText)
		{
			string hh = "\r\n";

			// write the script to show question dialog
			hh += "\t\t var myObject = new Object(); \r\n\r\n";

			hh += "\t\t myObject.HeaderText = \"" + headerText + "\";\r\n";
			hh += "\t\t myObject.DescriptionText = \"" + descriptionText + "\"; \r\n";
			hh += "\t\t myObject.ButtonOneText = \"" + buttonOneText + "\"; \r\n ";
			hh += "\t\t myObject.ButtonTwoText = \"" + buttonTwoText + "\"; \r\n ";
			hh += " \r\n ";
			hh += "\t\t var retObject = window.showModalDialog(\"../../../Common/question.aspx\", myObject , \"scroll: No; toolbar: No; dialogHeight: 360px; dialogWidth: 525px; dialogTop: px; dialogLeft: px; edge: Raised; center: Yes; help: No; resizable: No; status: No;\"); \r\n ";
			hh += "\r\n ";

			hh += "\t\t if (retObject != null) { \r\n";

			hh += "\t\t var retValue = retObject.ButtonClicked; \r\n";
			hh += "\t\t var theform = document.forms[0]; \r\n ";					
			hh += "\t\t theform.retValueFromQuestion.value = retValue; \r\n";

			hh += "\t\t } \r\n";

			return hh;
			// end script showing question dialog
		}

		public void WriteSaveScript()
		{
			if (!Page.IsClientScriptBlockRegistered("Save-Script Function") )
			{
				string hh = getOpenScriptTag();

				hh += "\t function Save() { \r\n";
				//hh += "\t\t window.returnValue = diag; \r\n";
				hh += "\t\t window.close(); } \r\n";		

				hh += getCloseScriptTag();
				
				Page.RegisterClientScriptBlock("Save-Script Function", hh);
				
			}
		}

		public bool WasButtonOneClicked
		{
			get
			{
				if (Request.Form["retValueFromQuestion"] != null)
				{
					string ret = Request.Form["retValueFromQuestion"];

					if (ret == "1")
					{
						return true;
					}
				}

				return false;
			}
		}

		public void WriteCancelScript()
		{
			if (!Page.IsClientScriptBlockRegistered("Cancel-Script Function") )
			{
				string hh = getOpenScriptTag();
			
				hh += "\t function Cancel() { \r\n";
				hh += "\t\t if (isDirty) { \r\n";
				hh += this.QuestionDialogScript("Are you sure you want to cancel?", 
												" If you select yes then all changes that you have made to the report archive values for \" + Form1.ReportCode.value + \" will be lost", 
												"Save", 
												"Cancel");
		


				hh += "\r\n";
					
				hh += "\t\t } \r\n";

				hh += "\t\t window.returnValue = null; \r\n";
				hh += "\t\t window.close(); \r\n";
				hh += "\t\t } \r\n";

				hh += getCloseScriptTag();
				
				Page.RegisterClientScriptBlock("Cancel-Script Function", hh);
				
			}
		}
	}
}
