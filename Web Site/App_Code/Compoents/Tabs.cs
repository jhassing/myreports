using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Xml;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Summary description for Tabs
/// </summary>
public class Tabs
{
    #region GetTabs

    private static List<TabItem> loadTabs(string tabName)
    {
        // Generate the tabs, and save into memory
        List<TabItem> tReturnTabs = new List<TabItem>();

        XmlDocument doc = new XmlDocument();
        doc.Load(HttpContext.Current.Request.PhysicalApplicationPath + "App_Data\\Tab_Data\\" + tabName + ".xml");

        XmlNodeList nl = doc.GetElementsByTagName("tab");

        for (int x = 0; x < nl.Count; x++)
        {
            XmlNode xmlNode = nl.Item(x);

            string id = xmlNode.Attributes["id"].Value;
            string displayName = xmlNode.Attributes["DisplayName"].Value;
            string url = xmlNode.Attributes["url"].Value;

            if (xmlNode.HasChildNodes)
            {

                XmlNodeList xmlNodeList = xmlNode.ChildNodes;

                if (xmlNodeList != null)
                {
                    if (xmlNodeList.Item(0).HasChildNodes)
                    {
                        XmlNodeList xmlNodeListGroups = xmlNodeList.Item(0).ChildNodes;

                        for (int z = 0; z < xmlNodeListGroups.Count; z++)
                        {
                            XmlNode xmlGroupNode = xmlNodeListGroups.Item(z);

                            string allowedGroupName = xmlGroupNode.FirstChild.Value.ToLower();

                            if (allowedGroupName.Equals("all"))
                            {
                                tReturnTabs.Add(new TabItem(displayName, id, url));
                            }
                            else
                            {
                                // See if the users in inside the group needed
                                if (MyReports.Common.AppValues.IsInRole(allowedGroupName))
                                {
                                    tReturnTabs.Add(new TabItem(displayName, id, url));
                                }
                            }
                        }

                    }
                }
            }
        }

        return tReturnTabs;
    }

    public static List<TabItem> GetTabs(string tabName)
    {
        string cacheName = tabName + "Session:" + HttpContext.Current.Session.SessionID;

        if (HttpContext.Current.Cache[cacheName] != null)
        {
            // Cast and return from memory
            return (List<TabItem>)HttpContext.Current.Cache[cacheName];
        }
        else
        {

            List<TabItem> aryRetTabs = loadTabs(tabName);

            // Store it in the cache for later use
            HttpContext.Current.Cache.Insert(cacheName, aryRetTabs, null, DateTime.Now.AddMinutes(5), TimeSpan.Zero);

            return aryRetTabs;
        }        
    }

    #endregion
}
