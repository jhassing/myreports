using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DistributionPortal
{
	public class GroupMembership
	{


		public bool MyReportsUser
		{
			get
			{
				return (bool)HttpContext.Current.Session["MyReportsUser"];
			}
			set
			{
				HttpContext.Current.Session["MyReportsUser"] = value;
			}
		}

		public bool PassportUser
		{
			get
			{
				return (bool)HttpContext.Current.Session["PassportUser"];
			}
			set
			{
				HttpContext.Current.Session["PassportUser"] = value;
			}
		}

		public bool BlueBookDistributionUser
		{
			get
			{
				return (bool)HttpContext.Current.Session["BlueBookDistributionUser"];
			}
			set
			{
				HttpContext.Current.Session["BlueBookDistributionUser"] = value;
			}
		}


	}
}
