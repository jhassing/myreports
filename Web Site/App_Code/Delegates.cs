using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public class Delegates
{
    // Passport
    public delegate void ExpeditionLinkClicked();
    public delegate void WelcomeLinkClicked();
    public delegate void ActivateAssociateClicked();
    public delegate void UnitExpeditionClicked();
    public delegate void SaveAssociateActiveSettingsClicked();
    public delegate void ExpeditionClicked(int ExpeditionID, string ExpeditionName);

    // User Management 
    public delegate void BackButtonClicked();
    public delegate void OKButtonClicked();
    public delegate void ApplyButtonClicked();
    public delegate void DeleteButtonClicked();
    public delegate void GroupListingLinkClicked();
    public delegate void UserListingLinkClicked();
    public delegate void HomeLinkClicked();
    public delegate void AddClicked();
    public delegate void RemoveClicked();
    public delegate void SearchButtonClicked();
    public delegate void ModifyHomeFoldersClicked(string HomeFolder);
    public delegate void ModifyApplicationAccess();

    public Delegates()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
