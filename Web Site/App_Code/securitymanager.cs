using System;
using System.Web.UI;
using activeportal.classes;


public class securitymanager : AcSecurityManager
{
    private string m_sUser;
    private string m_sPassword;
    private string m_sVolume;
    private byte[] m_bExCredentials;

    public securitymanager(string user, string password)
    {
        m_sUser = user;
        m_sPassword = password;

    }

    public string getUserName() { return m_sUser; }
    public string getPassword() { return m_sPassword; }
    public string getVolume() { return m_sVolume; }
    public byte[] getExtendedCredentials() { return m_bExCredentials; }

    public bool authenticate(Page page)
    {

        m_sVolume = "Report Distribution";
        return true;
    }
}