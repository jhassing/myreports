Imports Microsoft.VisualBasic

Public Class clsZip
    '== ***************************************************************************
    '==
    '==  REQUIRED EARLY BOUND REFERENCES
    '==  --------------------------------------------------------------------------
    '==    none
    '==  --------------------------------------------------------------------------
    '==
    '==  REQUIRED FILES or MODULES
    '==  --------------------------------------------------------------------------
    '==    none
    '==  --------------------------------------------------------------------------
    '==
    '==  REQUIRED SYSTEM DLLs
    '==  --------------------------------------------------------------------------
    '==    none
    '==  --------------------------------------------------------------------------
    '==
    '== History: Eric Knudsen            04/05/2006
    '==
    '== ***************************************************************************

    '== ***************************************************************************
    '== Win32 API Declarations

    '== End Win32 API Declarations
    '== ***************************************************************************

    '== ***************************************************************************
    '== Public Enumerations
    '==  --------------------------------------------------------------------------

    '==  --------------------------------------------------------------------------
    '== End Public Enumerations
    '== ***************************************************************************

    '== ***************************************************************************
    '== Public Constants
    '==  --------------------------------------------------------------------------
    '==  --------------------------------------------------------------------------
    '== End Public Constants
    '== ***************************************************************************

    '== ***************************************************************************
    '== Public Structures
    '==  --------------------------------------------------------------------------

    '==  --------------------------------------------------------------------------
    '== End Public Structures
    '== ***************************************************************************
    '== ***************************************************************************
    '== Module Level Variable declarations
    '==  --------------------------------------------------------------------------

    '== Enumerations

    '== Structures

    '== Objects

    '== Intrinsic Data Types

    '== Constants
    
    '==  --------------------------------------------------------------------------
    '== End Module Level Variable declarations
    '== ***************************************************************************

    '****************************************************************************
    '== Procedure:  Compress
    '==
    '== Purpose:    
    '==
    '== Params:    bytes
    '==             ---------------------------------------------------------------
    '==
    '== Returns:    Boolean
    '==
    '== Note:       none
    '==
    '== *************************************************************************
    Shared Function Compress(ByVal vaBytes() As Byte, _
                            ByRef raBytesOut() As Byte, _
                            ByRef rsErrorXmlOut As String) As Boolean

        Dim lbReturnCode As Boolean
        Dim lsErrorXml As String = ""

        Dim lobjMemory As System.IO.MemoryStream = Nothing
        Dim lobjStream As System.IO.Compression.DeflateStream = Nothing

        Try
            lobjMemory = New System.IO.MemoryStream
            lobjStream = New System.IO.Compression.DeflateStream( _
                                                        stream:=lobjMemory, _
                                                        mode:=System.IO.Compression.CompressionMode.Compress)
            lobjStream.Write(array:=vaBytes, offset:=0, count:=vaBytes.Length)
            lobjStream.Close()
            raBytesOut = lobjMemory.ToArray()
Success:
            '== Success
            lbReturnCode = True

        Catch ex As CompassCommon.Exception
            '== -------------------------------------------------------------------
            '== Format error xml
            lsErrorXml = CompassCommon.clsErrorHandler.sBuildErrorXml( _
                         robjException:=ex, _
                         vsProcedureName:=System.Reflection.MethodInfo.GetCurrentMethod.Name, _
                         vsModuleName:=System.Reflection.MethodBase.GetCurrentMethod.ReflectedType.ToString)
            '== -------------------------------------------------------------------
        Catch ex As System.Exception
            '== -------------------------------------------------------------------
            '== Format error xml
            lsErrorXml = CompassCommon.clsErrorHandler.sBuildErrorXml( _
                         robjException:=ex, _
                         vsProcedureName:=System.Reflection.MethodInfo.GetCurrentMethod.Name, _
                         vsModuleName:=System.Reflection.MethodBase.GetCurrentMethod.ReflectedType.ToString)
            '== -------------------------------------------------------------------
        Finally
            '== Destroy object references
            If Not lobjMemory Is Nothing Then lobjMemory.Dispose()
            lobjMemory = Nothing
            If Not lobjStream Is Nothing Then lobjStream.Dispose()
            lobjStream = Nothing
        End Try

        rsErrorXmlOut = lsErrorXml
        Return lbReturnCode

    End Function
    '****************************************************************************

    '****************************************************************************
    '== Procedure:  Decompress
    '==
    '== Purpose:    
    '==
    '== Params:    bytes
    '==             ---------------------------------------------------------------
    '==
    '== Returns:    Byte
    '==
    '== Note:       none
    '==
    '== *************************************************************************
    Shared Function Decompress(ByVal vaBytes() As Byte, _
                                ByRef raBytesOut() As Byte, _
                                ByRef rsErrorXmlOut As String) As Boolean

        Dim lbReturnCode As Boolean
        Dim lsErrorXml As String = ""

        Dim lobjStream As System.IO.Compression.DeflateStream = Nothing
        Dim lobjMemory As System.IO.MemoryStream = Nothing
        Dim labyteWriteData(4096) As Byte
        Dim liSize As Integer

        Try

            lobjStream = New System.IO.Compression.DeflateStream( _
                                            stream:=New System.IO.MemoryStream(vaBytes), _
                                           mode:=System.IO.Compression.CompressionMode.Decompress)
            lobjMemory = New System.IO.MemoryStream
            
            While True
                liSize = lobjStream.Read(array:=labyteWriteData, _
                                        offset:=0, _
                                        count:=labyteWriteData.Length)
                If liSize > 0 Then
                    lobjMemory.Write(buffer:=labyteWriteData, _
                                    offset:=0, _
                                    count:=liSize)
                Else
                    Exit While
                End If

            End While
            lobjStream.Close()
            raBytesOut = lobjMemory.ToArray()

Success:
            '== Success
            lbReturnCode = True

        Catch ex As CompassCommon.Exception
            '== -------------------------------------------------------------------
            '== Format error xml
            lsErrorXml = CompassCommon.clsErrorHandler.sBuildErrorXml( _
                         robjException:=ex, _
                         vsProcedureName:=System.Reflection.MethodInfo.GetCurrentMethod.Name, _
                         vsModuleName:=System.Reflection.MethodBase.GetCurrentMethod.ReflectedType.ToString)
            '== -------------------------------------------------------------------
        Catch ex As System.Exception
            '== -------------------------------------------------------------------
            '== Format error xml
            lsErrorXml = CompassCommon.clsErrorHandler.sBuildErrorXml( _
                         robjException:=ex, _
                         vsProcedureName:=System.Reflection.MethodInfo.GetCurrentMethod.Name, _
                         vsModuleName:=System.Reflection.MethodBase.GetCurrentMethod.ReflectedType.ToString)
            '== -------------------------------------------------------------------
        Finally
            '== Destroy object references
            If Not lobjStream Is Nothing Then lobjStream.Dispose()
            lobjStream = Nothing
            If Not lobjMemory Is Nothing Then lobjMemory.Dispose()
            lobjMemory = Nothing
            labytewriteData = Nothing
        End Try

        rsErrorXmlOut = lsErrorXml
        Return lbReturnCode

    End Function
    '****************************************************************************
End Class
