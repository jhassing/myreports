using System;

public class NewUserBaseClass : System.Web.UI.UserControl
{
    protected override void OnLoad(EventArgs e)
    {

        base.OnLoad(e);

        if (!Page.IsPostBack)
        {
            if (Visible)
            {
                LoadStateInformation();
            }
        }
    }


    protected virtual void LoadStateInformation()
    {
        // Blank..
    }

    protected virtual void SaveState()
    {
        // blank
    }

    protected void ReDirect(int step)
    {
        SaveState();
        Response.Redirect(QueryStringValues.GetRedirectString(step));
    }
}