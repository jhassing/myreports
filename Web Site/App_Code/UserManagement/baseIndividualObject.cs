using System;
using System.Data;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Collections;
using System.Text;

using UserManagement.BLL;

using MyReports.UserManagementUI.Common;
using MyReports.Components;
using MyReports.Common;

using Compass.Security.ActiveDirectory;
using Novell.Directory.Ldap;

using Microsoft.ApplicationBlocks.ExceptionManagement;

public class baseIndividualObject : System.Web.UI.Page
{
    public string getTabID
    {
        get
        {
            string tabID = "";

            if (Request.QueryString["tabID"] != null)
                tabID = Request.QueryString["tabID"].ToString();
            else
                tabID = "";

            return tabID;
        }
    }

    public string DistinguishedName
    {
        get
        {
            return ViewState["DistinguishedName"] != null ? ViewState["DistinguishedName"].ToString().Trim() : "";
        }
        set
        {
            ViewState["DistinguishedName"] = value.ToString().Trim();
        }

    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        
        this.DistinguishedName = Request.QueryString["distinguishedName"].ToString();
    }
}
