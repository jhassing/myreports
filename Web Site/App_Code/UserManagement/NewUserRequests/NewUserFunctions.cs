using System;
using System.Web;
using UserManagement.BLL;
using MyReports.Common;

public class NewUserFunctions
	{
		public static NewUserBLL oNewUserBLL
		{
			get
			{
				return  new NewUserBLL( AppValues.UM_DB_ConnectionString, UserManagement.BLL.AuthObject.GetAuthObject );
			}
		}

		public static string PK_ID__URL_Encrypted
		{
			get
			{
				return HttpContext.Current.Server.UrlEncode( HttpContext.Current.Request.QueryString["id"].ToString() );
			}
		}

		public static Guid PK_ID__URL_Decrypted
		{
			get
			{
				return new Guid( MyReports.Common.Functions.Decrypt( HttpContext.Current.Request.QueryString["id"].ToString() ));
			}
		}


		public static string done
		{
			get
			{
				string done = "0";

				if (HttpContext.Current.Request.QueryString["done"] != null)
					done = HttpContext.Current.Request.QueryString["done"].ToString();
			
				return done;
			}
		}

	}

