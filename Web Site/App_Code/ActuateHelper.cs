using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Server_Proxy.localhost;
using Compass.Reporting.Actuate;

public sealed class MyReportsActuateHelper : ActuateHelper
{
    public MyReportsActuateHelper(string userName, string password, string url)
    {
        try
        {
            // Login to actuate
            LoginActuate(userName, password, url); //, out l_proxy);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public ArrayList DownloadFiles(ArrayList files, string locationToSave)
    {
        ArrayList ary = new ArrayList();

        try
        {
            foreach (string file in files)
            {
                try
                {
                    string fileName = "";
                    string folderCode = "";

                    string[] tmp = file.Split('/');

                    string tmp1 = System.IO.Path.GetFileNameWithoutExtension(file);
                    string tmp2 = tmp[tmp.Length - 2];

                    int x = tmp1.IndexOf("-");

                    if (x > 0)
                    {
                        fileName = tmp1.Substring(x + 1, tmp1.Length - x - 1);
                    }
                    else
                    {
                        fileName = tmp1;
                    }

                    int y = fileName.LastIndexOf(" ");
                    if (y > 0)
                    {
                        fileName = fileName.Substring(0, y).Trim();
                    }

                    if (tmp2.IndexOf("-") > 0)
                    {
                        folderCode = tmp2.Substring(0, tmp2.IndexOf("-")).Trim();
                    }
                    else
                    {
                        folderCode = tmp2.Trim();
                    }

                    string loc = locationToSave + folderCode + " - " + fileName + ".pdf";

                    switch (System.IO.Path.GetExtension(file).ToLower())
                    {
                        case ".roi":
                            base.GetContents(file, loc);
                            ary.Add(loc);
                            break;

                        case ".pdf":
                            base.DownloadFile(file, loc);
                            ary.Add(loc);
                            break;
                    }                    
                }
                catch (Exception ex)
                {
                    continue;
                }
            }

            return ary;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
