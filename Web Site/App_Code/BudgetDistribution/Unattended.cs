using System.Data;
using System.Data.SqlClient;
using System.IO;
using MyReports.Common;
using YEA_Tools;

/// <summary>
/// Summary description for Unattended
/// </summary>
public class Unattended
{
    private SqlConnection conn;
    private string conStr1;
    private SqlDataAdapter daUnits;
    private DataSet ds;


    public void Run_Silent()
    {
        int i;
        i = 0;

        // conStr1 = ConfigurationManager.AppSettings["CONN_STR1"];
        conStr1 = AppValues.MyReports_DB_ConnectionString;
        conn = new SqlConnection(conStr1);

        daUnits = new SqlDataAdapter("select replace(str(UnitNumber, 5), ' ', '0') Unit FROM tbl_UnitNumbers", conn);
        ds = new DataSet();
        daUnits.Fill(ds, "tbl_UnitNumbers");
        DataTable dTable = ds.Tables[0];

        CodeFile CF = new CodeFile();
        CF.CreateLogFile("U");
        TextWriter outLog = new StreamWriter(CF.YEALog);
        outLog.WriteLine(CF.CurrentTime + "     Starting unhandled processing of files in: " + CF.BudgetLocation);
        outLog.WriteLine(CF.CurrentTime + "     " + dTable.Rows.Count + " unit numbers for processing");
        outLog.WriteLine("");
        foreach (DataRow dRow in dTable.Rows)
        {
            i++;
            //txtStatus.Text = dTable.Rows.Count + " ROWS: " + i.ToString() + " Unit: " + dRow["Unit"].ToString();
            CF.UnitNumber = dRow["Unit"].ToString();
            //   outLog.WriteLine(CF.CurrentTime + "     Starting process for unit " + CF.UnitNumber);
            CF.ProcessUnitsBudget(outLog);
            //   outLog.WriteLine(CF.CurrentTime + "     Process complete for unit " + CF.UnitNumber);
            outLog.WriteLine("");
        }

        outLog.WriteLine("Unhandled Processed: " + dTable.Rows.Count + " units");
        outLog.WriteLine(CF.CurrentTime + "     Process Complete...");
        outLog.Close();
    }
}

//<connectionStrings>
//  <add name="MyReports_CONN" connectionString="Data Source=CGUSCHD310\DEV;Initial Catalog=MyReports_DEV;Integrated Security=True"
//   providerName="System.Data.Sql" />
//  <add name="MyReports_2" connectionString="Data Source=CGUSCHD310\DEV;Initial Catalog=MyReports_DEV;Integrated Security=True"
//   providerName="System.Data.SqlClient" />
//  <add name="National_Accounts_R5ConnectionString" connectionString="Data Source=CGUSCHD310\DEV;Initial Catalog=MyReports_DEV;Integrated Security=True"
//   providerName="System.Data.SqlClient" />

// </connectionStrings>


//<asp:SqlDataSource ID="SqlDataSource1x" runat="server" ConnectionString="<%$ ConnectionStrings:National_Accounts_R5ConnectionString %>"
//    ProviderName="<%$ ConnectionStrings:MyReports_CONN.ProviderName %>" SelectCommand="SELECT [CostCenter], [CostCenterID], [Sector], [Division] FROM [CostCenter]">
//</asp:SqlDataSource>