using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using Compass.Data.MSSQLDataAccess;
using MyReports.Common;

namespace YEA_Tools

{
    
    public class CodeFile
    {
        #region private fields

        string _budgetFile = "";
        string _newBudgetFile = "";
        string _strBudgetLocation = "";
        int _rowCount = 0;
        int _rowIndex = 0;
        string _unitNumber = "";
        string _currentTime = "";
        string _logDest = "";
        string _yeaLog = "";

        #endregion


        #region public properties
        public CodeFile()
        {
            _budgetFile = "";
            _newBudgetFile = "";
            _strBudgetLocation = ""; //ConfigurationManager.AppSettings["BUDGET_PATH"];
            _rowCount = 0;
            _rowIndex = 0;
            _unitNumber = "";
            _currentTime = "";
            _logDest = ConfigurationManager.AppSettings["YEA_LOG_PATH"]; 
            _yeaLog = "";
        }
        public string BudgetFile
        {
            get { return _budgetFile; }
            set { _budgetFile = value; }
        }

        public string NewBudgetFile
        {
            get { return _newBudgetFile; }
            set { _newBudgetFile = value; }
        }

        public string BudgetLocation
        {
            get { return _strBudgetLocation; }
            set { _strBudgetLocation = value; }
        }
        public int RowCount
        {
            get { return _rowCount; }
            set { _rowCount = value; }
        }
        public int RowIndex
        {
            get { return _rowIndex; }
            set { _rowIndex = value; }
        }
        public string UnitNumber
        {
            get { return _unitNumber; }
            set { _unitNumber = value; }
        }
        public string CurrentTime
        {
            get { return DateTime.Now.ToString("yyyyMMdd_HHmmss"); }
            set { _currentTime = DateTime.Now.ToString("yyyyMMdd_HHmmss"); }
        }
        public string LogDestination
        {
            get { return _logDest; }
            set { _logDest = value; }
        }
        public string YEALog
        {
            get { return _yeaLog; }
            set { _yeaLog = value; }
        }

        #endregion


        #region Public Methods

        Imp i = new Imp();

        public void CreateLogFile(string U_A)
        {
            LogDestination = LogDestination + "\\YEALogs";
            DirectoryInfo folder = new DirectoryInfo(LogDestination);

            if (!folder.Exists)
            {
                folder.Create();
            }
            if (U_A == "A")
                YEALog = LogDestination + "\\YEALog_" + CurrentTime + ".txt";
            else
                YEALog = LogDestination + "\\YEALog_" + CurrentTime + "_U.txt";
        }


        public void ProcessUnitsBudget(TextWriter outLog)

        {
                
            CheckUnitNumberLength(UnitNumber);
            BudgetFile = BudgetLocation + "\\budget00000" + UnitNumber + ".txt";
            NewBudgetFile = BudgetLocation + "\\budget00000" + UnitNumber + "_NEW.txt";



           FileInfo budgetFyle = new FileInfo(BudgetFile);
            if (budgetFyle.Exists)
            {
                outLog.WriteLine(CurrentTime + "     Found budget file for unit " + UnitNumber);
                //outLog.WriteLine(CurrentTime + "     Budget File Path is '" + BudgetFile + "'");
                //outLog.WriteLine(CurrentTime + "     Temporary Budget File Path is '" + NewBudgetFile + "'");
                ReadBudgetFile(BudgetFile, outLog);
            }
            else
            {
                outLog.WriteLine(CurrentTime + "     Could not find budget file for unit " + UnitNumber);
                //outLog.WriteLine(CurrentTime + "     Expected budget file path is '" + BudgetFile + "'");
            }
        }


        public void CheckUnitNumberLength(string u)
        {
            // This process will check the unit number passed to see if it is
            // 4 or 5 digits in length.  If it's on 4 digits we must add
            // a 0 to the beginning of it.
            int _length = UnitNumber.Length;
            if (_length < 5)
            {
                UnitNumber = "0" + UnitNumber;
            }
        }


        public void ReadBudgetFile(string t, TextWriter outLog)
        {
            TextReader oReader = new StreamReader(BudgetFile);
            TextWriter oWriter = new StreamWriter(NewBudgetFile);

            string wholeFile = oReader.ReadToEnd();
            string[] wholeFileArray = wholeFile.Split(new char[] { }, StringSplitOptions.RemoveEmptyEntries);
            int r = wholeFileArray.Length;

            outLog.WriteLine(CurrentTime + "     Budget file contains " + r + " lines for review");
            try
            {
                for (int c = 0; c <= wholeFileArray.Length - 1; c++)
                {
                    int z = c + 1;
                    string currentLine = wholeFileArray.GetValue(c).ToString();//oReader.ReadLine();
                    string[] split = currentLine.Split(new char[] { ',' });
                    string newLine = "";

                    // If there are 15 items in the Char Array then this budget file 
                    // includes P13 data, get rid of it (index #14)
                    if (split.Length > 14)
                            {
                                outLog.WriteLine(CurrentTime + "       ...Line " + c + 1 + " contains P13 budget data");
                                for (int i = 0; i <= 13; i++)
                                {
                                    newLine = newLine + split.GetValue(i);
                                    if (i != 13)
                                    {
                                        newLine = newLine + ",";
                                    }
                                }
                            }
                       else
                            {
                                   outLog.WriteLine(CurrentTime + "       ...Line " + z + " does not contain P13 budget data");
                            }

                    if (newLine == "")
                    {
                        oWriter.WriteLine(currentLine);
                    //    outLog.WriteLine(CurrentTime + "       ...Writing original line to temporary buget file");
                    }
                    else
                    {
                        oWriter.WriteLine(newLine);
                    //    outLog.WriteLine(CurrentTime + "       ...Writing new line to temporary budget file");
                    }
                }
                outLog.WriteLine(CurrentTime + "     All budget lines for file have been processed");
                oReader.Close();
                oWriter.Close();

                FileInfo fNewBudgetFile = new FileInfo(NewBudgetFile);
                outLog.WriteLine(CurrentTime + "     Replacing original budget file with temporary file");
                // added to delete original file prior to copy
                //BudgetFile.Delete();
                fNewBudgetFile.CopyTo(BudgetFile, true);
                fNewBudgetFile.Delete();
                outLog.WriteLine(CurrentTime + "     Original budget file has been replaced");
            }
            catch (Exception e)
            {
                outLog.WriteLine("");
                outLog.WriteLine(CurrentTime + "     ***An Error Has Occured While Processing Budget File for Unit " + UnitNumber + "***");
                outLog.WriteLine(CurrentTime + "     Error: " + e);
            }
        }
        

        #endregion

        #region DB Access

        public void updUnitTbl(string uNum, string A_D)
        {
            SqlConnection conn;
            string conStr1;
            conStr1 = AppValues.MyReports_DB_ConnectionString;
            conn = new SqlConnection(conStr1);
            DataSet ds;
            ds = new DataSet();
           
            string strSQL;
                    strSQL = "INSERT INTO [tbl_UnitNumbers] ([UnitNumberID], [UnitNumber]) VALUES ('" + uNum + "','" + uNum + "')";
            if (A_D == "D")
                {
                    strSQL = "delete from [tbl_UnitNumbers] where [UnitNumber]= " + uNum + "";                 
                }

            if (A_D == "T")
            {
                strSQL = "delete  dbo.[tbl_UnitNumbers];";
            }


            if (A_D == "Z")
            {
                strSQL = "INSERT dbo.[tbl_UnitNumbers]select unit,unit " +
                         " from dbo.[zrops] where unit is not null";
            }
           
            // public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText)
            
            SqlHelper.ExecuteDataset(conStr1, CommandType.Text, strSQL);

           
        }


        #endregion
    }

    public class Imp : Compass.Security.Impersonation.Impersonate
    {
        private string _userName = ConfigurationManager.AppSettings["IMP_USERNAME"];    // "BDAdmin";
        private string _passWord = ConfigurationManager.AppSettings["IMP_PW"];          //"crU58wRe";
        private string _domain = ConfigurationManager.AppSettings["IMP_DOMAIN"];        //"compass-usa";
        
        public string UserName
        {
          get { return _userName; }
          set { _userName = value; }
        }

        public string Password
        {
            get { return _passWord; }
            set { _passWord = value; }
         }

        public string Domain
        {
            get { return _domain; }
            set { _domain = value; }
        }
        
        public Imp()
        {
            base.impersonateValidUser(UserName, Domain, Password);
        }

    }


}

