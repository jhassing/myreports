using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Web.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;

using Compass.FileSystem.Permissions;
using Compass.Security.ActiveDirectory;
using Compass.Security.Impersonation;
using BudgetDistribution.BLL;

using Microsoft.ApplicationBlocks.ExceptionManagement;
using MyReports.Common;

namespace DistributionPortal
{
	
	public class baseFileClass
	{
		private string _fileName;
		private DateTime _datTimeStamp;

		public baseFileClass(string FileName, DateTime TimeStamp)
		{
			this._fileName = FileName;					
			this._datTimeStamp = TimeStamp;
		}

		public string FileName 
		{ get { return(this._fileName ); } }

		
		public DateTime TimeStamp 
		{ get { return(this._datTimeStamp ); } }

	}

	public class BudgetDistribution_SORT_BY_FileName : IComparer  
	{						
		int IComparer.Compare( Object x, Object y )  
		{			
			return( (new CaseInsensitiveComparer()).Compare( ((baseFileClass)y).FileName, ((baseFileClass)x).FileName ) );
		}
	}

	public class BudgetDistribution_SORT_BY_CreationDate : IComparer  
	{				
		int IComparer.Compare( Object x, Object y )  
		{			
			return( (new CaseInsensitiveComparer()).Compare( ((baseFileClass)y).TimeStamp, ((baseFileClass)x).TimeStamp ) );
		}
	}

	

	public class BUDGET_DISTRIBUTION_FILES : baseFileClass
	{
		private string _path;		

		public BUDGET_DISTRIBUTION_FILES(string Path, string FileName, DateTime TimeStamp) : base(FileName, TimeStamp)
		{	
			this._path = Path;			
		}

		public string Path 
		{ get { return(this._path); } }

	}			


	public class BudgetDistributionFiles : Impersonate
	{
		#region Private class variables

		private string _impersonatedUser;
		private string _impersonatedPassword;
		private string _impersonatedDomain;
		private string _pathToFiles;
		private string _filter;			
		private IntPtr hToken = IntPtr.Zero;

		#endregion

		#region ctors
		
		public BudgetDistributionFiles(string impersonatedUser, string impersonatedPassword, 
											string impersonatedDomain, string pathToFiles, string filter)
		{
			_impersonatedUser = impersonatedUser;
			_impersonatedPassword = impersonatedPassword;
			_impersonatedDomain = impersonatedDomain;
			_pathToFiles = pathToFiles;
			_filter = filter;
		}

		public BudgetDistributionFiles(string impersonatedUser, string impersonatedPassword, string impersonatedDomain)
		{
			_impersonatedUser = impersonatedUser;
			_impersonatedPassword = impersonatedPassword;
			_impersonatedDomain = impersonatedDomain;
		}

		#endregion

		public void ImpersonateUser()
		{
			// Impersonate the web user first
			if (!base.impersonateValidUser(this._impersonatedUser, this._impersonatedDomain, this._impersonatedPassword, out hToken))
				throw new ApplicationException("Impersonation has failed.");
		}


		public BUDGET_DISTRIBUTION_FILES[] getBudgetDistributionFiles()
		{	
			try
			{
				ArrayList ary;
				// Get the groups this person belongs to and the files they can see
				FilePermissions o = new FilePermissions();
				string sFile = "";
				DateTime sLastAccessTime;


				// get the files
				ary = o.GetFilesUserCanRead(hToken, _pathToFiles,_filter);// true, false, false, false);			

				BUDGET_DISTRIBUTION_FILES[] oBUDGET_DISTRIBUTION_FILES = new BUDGET_DISTRIBUTION_FILES[ary.Count];

				for (int x=0;x<ary.Count;x++)
				{
					sFile = ary[x].ToString();
				
					sLastAccessTime = File.GetLastWriteTime(sFile);
					string[] s = sFile.Split('\\');
					string fileName = s[s.Length-1].ToString();

					oBUDGET_DISTRIBUTION_FILES[x] = new BUDGET_DISTRIBUTION_FILES(sFile, fileName, sLastAccessTime);
					
				}			
		
				// Undo the impersonations
				this.undoImpersonation();

				return(oBUDGET_DISTRIBUTION_FILES);		
			}
			catch (Exception ex)
			{
				throw ex;
			}

					
		} 		

		
		public DateTime GetLastWriteTime(string file)
		{
			try
			{
				return File.GetLastWriteTime(file);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}


		public string CreateZipFilesForDownload(ArrayList aryFiles)
		{
			string tmpPath = "";
			string tmpFile = "";

			try
			{
				BudgetDistributionBLL oBLL = new BudgetDistributionBLL();

				string[] files = new string[aryFiles.Count];
			
				tmpPath = Functions.AddTrailingSlash(oBLL.GetTempDirectoryForThisUser( AppValues.UserName, AppValues.BudgetDistribution_TempFolder_Location) );
				tmpFile = oBLL.GetZipFileName();

				if (Directory.Exists(tmpPath))
					Directory.Delete(tmpPath);

				// Create a new temp directory.
				Directory.CreateDirectory(tmpPath);

				// Go through the array, copy the files to a temp location, also create a string array of the new file locations
				for (int x=0;x<aryFiles.Count;x++)
				{			
					string fileName = aryFiles[x].ToString();

					string sFullPath = AppValues.BudgetDistribution_FileLocation_Folder + fileName;

					string newPath = Functions.AddTrailingSlash(tmpPath) + fileName;

					files[x] = newPath;
					// Copy the selected file to the new temp directory
					File.Copy(sFullPath, newPath, true);				
				}
			
				//base.undoImpersonation();
				// Send the files to get zipped			
				oBLL.ZipSelectedFiles(tmpPath + tmpFile, files);	
			
				return(tmpPath + tmpFile);
			}
			catch(Exception ex)
			{
				NameValueCollection nv = new NameValueCollection();

				nv.Add("Temp Path", tmpPath);
				nv.Add("Temp File", tmpFile);	
			
				for (int xx=0;xx<aryFiles.Count;xx++)
				{
					nv.Add("File (" + xx + ")", aryFiles[xx].ToString());
				}

				ExceptionManager.Publish ( new Exception("Error getting config key values", ex), nv );

				throw ex;
			}
		}


		public void UndoImpersonate()
		{
			try
			{
				this.undoImpersonation();
			}
			catch(Exception ex)
			{
				throw(ex);
			}
		}
	}
}
