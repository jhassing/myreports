


public class basePassportUserControl : System.Web.UI.UserControl
{

    public enum enumScreens
    {
        Welcome = 0,
        Expedition_Listing = 1,
        Ports_Of_Call = 2,
        Associate_Listing = 3,
        Report_Listing = 4,
        Report_Name = 5
    }

    #region Properties

    #region ExpeditionID

    public int ExpeditionID
    {
        get
        {
            return ViewState["ExpeditionID"] != null ? System.Convert.ToInt32(ViewState["ExpeditionID"]) : 0;
        }
        set
        {
            ViewState["ExpeditionID"] = value;
        }
    }


    #endregion

    #region Expedition Name

    public string ExpeditionName
    {
        get
        {
            return ViewState["ExpeditionName"] != null ? ViewState["ExpeditionName"].ToString() : "";
        }
        set
        {
            ViewState["ExpeditionName"] = value;
        }
    }


    #endregion

    #region User Network ID

    public string UserNetworkID
    {
        get
        {
            return ViewState["UserNetworkID"] != null ? ViewState["UserNetworkID"].ToString() : "";
        }
        set
        {
            ViewState["UserNetworkID"] = value;
        }
    }

    #endregion

    #region Screens

    public enumScreens Screen
    {
        get
        {
            return ViewState["SelectedScreen"] != null ? (enumScreens)ViewState["SelectedScreen"] : enumScreens.Welcome;
        }
        set
        {
            ViewState["SelectedScreen"] = value;
        }
    }

    #endregion

    #region Operation Number

    public virtual int OperationNumber
    {
        get
        {
            return ViewState["OperationNumber"] != null ? System.Convert.ToInt32(ViewState["OperationNumber"]) : 0;
        }
        set
        {
            ViewState["OperationNumber"] = value;
        }
    }

    #endregion


    #endregion

}