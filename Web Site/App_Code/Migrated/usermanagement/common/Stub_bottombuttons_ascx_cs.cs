using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;

abstract public class BottomButtons : System.Web.UI.UserControl
{
    public event Delegates.BackButtonClicked BackButton_OnClick;
    public event Delegates.OKButtonClicked OKButton_OnClick;
    public event Delegates.ApplyButtonClicked ApplyButton_OnClick;
    public event Delegates.DeleteButtonClicked DeleteButton_OnClick;

    abstract public bool Display_DeleteButton
    {
        get;
        set;
    }
    abstract public bool Display_ApplyButton
    {
        get;
        set;
    }
    abstract public bool Display_OKButton
    {
        get;
        set;
    }
}
