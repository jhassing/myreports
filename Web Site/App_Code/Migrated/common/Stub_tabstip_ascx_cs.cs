//===========================================================================
// This file was generated as part of an ASP.NET 2.0 Web project conversion.
// This code file 'App_Code\Migrated\common\Stub_tabstip_ascx_cs.cs' was created and contains an abstract class 
// used as a base class for the class 'Migrated_TabStripClass' in file 'common\tabstip.ascx.cs'.
// This allows the the base class to be referenced by all code files in your project.
// For more information on this code pattern, please refer to http://go.microsoft.com/fwlink/?LinkId=46995 
//===========================================================================




namespace DistributionPortal
 {

	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using System.Collections;
	using System.Web.Security;
	using System.Text;

    public delegate void TabCLicked(int tabClicked);

abstract public class TabStripClass :  ClientSideControl
{
    public event TabCLicked Tab_OnClick;

	abstract public ArrayList TabItems
	{
	  get;
	  set;
	}
	abstract public int SelectedTab
	{
	  get;
	  set;
	}
	abstract public void Enabled(int tab, bool enabled);
	abstract public void LinkButton_Command(Object sender, CommandEventArgs e);
	abstract public void LoadTabs();


}



}
