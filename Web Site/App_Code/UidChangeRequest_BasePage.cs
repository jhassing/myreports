using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Caching;

/// <summary>
/// Summary description for UidChangeRequest
/// </summary>
public class UidChangeRequest_BasePage : System.Web.UI.Page
{

    private string _loadedDataFrom = "";
    protected string LoadedDataFrom
    {
        get { return this._loadedDataFrom; }
    }

    public DataTable LoadData()
    {
        // Load the data, from the database, if not already in Cache         
        DataTable dt = (DataTable)Cache.Get("UdiChangedCache");

        if (dt == null)
        {
            dt = loadDataIntoCache();
            this._loadedDataFrom = "Loaded data from Database";
        }
        else
            this._loadedDataFrom = "Loaded data from Cache";

        return dt;
    }

    private DataTable loadDataIntoCache()
    {
        DataTable dt;

        //SqlDependency.Start(MyReports.Common.AppValues.MyReports_DB_ConnectionString);

        using (SqlConnection connection = new SqlConnection(MyReports.Common.AppValues.MyReports_DB_ConnectionString))
        {
            using (SqlCommand command = new SqlCommand("dbo.usp_Get_UserListing", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                //SqlCacheDependency dependency = new SqlCacheDependency(command);
                SqlDataAdapter adapter = new SqlDataAdapter(command);

                DataSet dataSet = new DataSet();

                adapter.Fill(dataSet);

                dt = dataSet.Tables[0];

                //Cache.Insert("UdiChangedCache", dataSet.Tables[0], dependency);
                Cache.Insert("UdiChangedCache", dataSet.Tables[0]);
            }
        }

        return dt;
    }
}
