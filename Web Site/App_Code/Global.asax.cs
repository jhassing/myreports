using System;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Threading;
using System.Globalization;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Security.Principal;
using System.Resources;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.SqlClient;


using Compass.Security.ActiveDirectory;
using MyReports.Common;

using Microsoft.ApplicationBlocks.ExceptionManagement;


public class Global : System.Web.HttpApplication
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    //	private ResourceManager _rm;

    public Global()
    {
        InitializeComponent();
    }

    public override string GetVaryByCustomString(HttpContext context, string custom)
    {
        //return base.GetVaryByCustomString (context, custom);
        // Set cache based on user
        //return AppValues.UserName;

        return "";

    }


    protected void Application_Start(Object sender, EventArgs e)
    {

        //Application["RM"] = new ResourceManager("ResourceTest", Assembly.GetExecutingAssembly() );
        GC.Collect();


        //SqlDependency.Start(MyReports.Common.AppValues.UdiChange_ConnectonString);
        
    }


    protected void Session_Start(Object sender, EventArgs e)
    {
        GC.Collect();

    }


    protected void Application_BeginRequest(Object sender, EventArgs e)
    {
        // For each request initialize the culture values with the
        // user language as specified by the browser.
        try
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Request.UserLanguages[0]);
        }
        catch (Exception)
        {
            // provide fallback for not supported languages. This is really just a safety catch, for 'in-case' scenarios
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
        }

        Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

        //_rm = (ResourceManager) Application["RM"];
    }


    protected void Application_EndRequest(Object sender, EventArgs e)
    {

    }


    protected void Application_AuthenticateRequest(Object sender, EventArgs e)
    {
        // Extract the forms authentication cookie
        string cookieName = FormsAuthentication.FormsCookieName;
        HttpCookie authCookie = Context.Request.Cookies[cookieName];

        if (null == authCookie)
        {
            // There is no authentication cookie.
            return;
        }

        FormsAuthenticationTicket authTicket = null;

        try
        {
            authTicket = FormsAuthentication.Decrypt(authCookie.Value);
        }
        catch (Exception ex)
        {
            // Log exception details (omitted for simplicity)
            return;
        }

        if (null == authTicket)
        {
            // Cookie failed to decrypt.
            return;
        }

        // When the ticket was created, the UserData property was assigned a
        // pipe delimited string of group names.
        String[] groups = new String[1]; //authTicket.UserData.Split(new char[]{'|'});

        // Create an Identity object
        GenericIdentity id = new GenericIdentity(authTicket.Name,
            "LdapAuthentication");

        // This principal will flow throughout the request.
        GenericPrincipal principal = new GenericPrincipal(id, groups);
        // Attach the new principal object to the current HttpContext object
        Context.User = principal;

    }


    protected void Application_Error(Object sender, EventArgs e)
    {
        ExceptionManager.Publish(Server.GetLastError());

        Server.ClearError();

        //System.Web.Security.FormsAuthentication.SignOut();

        //Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(Server.GetLastError());

        //FormsAuthentication.SignOut();
        //Session.Clear();

        //Response.Redirect("../Error.htm", false);
    }


    protected void Session_End(Object sender, EventArgs e)
    {
        Global.SignOut();
        
    }


    protected void Application_End(Object sender, EventArgs e)
    {
        GC.Collect();

    }


    public static void SignOut()
    {
        //FormsAuthentication.SignOut();
        //HttpContext.Current.Session.Clear();
        //HttpContext.Current.Response.Clear();
        //HttpContext.Current.Response.ClearContent();
        //HttpContext.Current.Response.ClearHeaders();

        GC.Collect();
        
        

    }


 


    public static string ResourceValue(string sKeys)
    {
        return sKeys;
        
        ResourceManager rm = new ResourceManager("MyReports", Assembly.GetExecutingAssembly());

        return rm.GetString(sKeys);
    }



    //*********************************************************************
    //
    // GetApplicationPath Method
    //
    // This method returns the correct relative path when installing
    // the portal on a root web site instead of virtual directory
    //
    //*********************************************************************
    public static string GetApplicationPath(HttpRequest request)
    {
        string path = string.Empty;
        try
        {
            if (request.ApplicationPath != "/")
                path = request.ApplicationPath;
        }
        catch (Exception e)
        {
            throw e;
        }
        return path;

    }


    //*************************************************************************
    // Mail Server SMTP Address
    //*************************************************************************


    //***********************************************************************************
    // Does the user belong to any groups authorized to use this site?
    //***********************************************************************************
    public static bool IsUserGroupAuthorized(ArrayList sGroups, string sGroupsAllowedToSite)
    {
        string[] allowedMembership = sGroupsAllowedToSite.Split('|');

        for (int x = 0; x < sGroups.Count; x++)
        {
            for (int y = 0; y < allowedMembership.Length; y++)
            {
                if (sGroups[x].ToString().ToLower() == allowedMembership[y].ToString().ToLower())
                    return true;
            }
        }

        return false;
    }

    #region App Version

    public static string PortalVersion
    {
        get
        {
            try
            {
                string major = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major.ToString();
                string minor = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString();
                string build = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();
                string revision = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Revision.ToString();

                return string.Format( Global.ResourceValue("PORTAL_VERSION"), major, minor, build, revision );


            }
            catch
            {
                return "Error";
            }
        }
    }

    #endregion

    #region Web Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
    }
    #endregion
}