<%@ Control Language="c#" Inherits="activeportal.usercontrols.authenticate" CodeFile="authenticate.ascx.cs" %>
<%--	
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<% if ( EnableJavascript ) {%>
<script language="javascript" src="<%=Context.Request.ApplicationPath%>/js/encoder.js"></script>
<script language="javascript" src="<%=Context.Request.ApplicationPath%>/js/cookie.js"></script>
<script language="javascript">
if ( <%= m_isPopUp %>  == "false" )
{
if ( 1 == readCookie("AcSE"))
{
	var redirect = '<%=Context.Request.ApplicationPath%>/login.aspx';
	var serverURL = readCookie( "serverURL" );
	if ( serverURL != null )
	{
		redirect += "?serverurl=" + encode( serverURL );
	}
	window.location = redirect;
}
}
</script>
<%}%>
