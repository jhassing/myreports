/*
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
*/
namespace activeportal.usercontrols
{
	using System;
	using System.Text;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes.api;
	using activeportal.classes;
	using activeportal.classes.configuration;
	using activeportal.classes.functionality;
	using System.Threading;
	using System.Globalization;
	using System.Configuration;
	using actuate_i18n.classes;
	using System.Collections;
	using DistributionPortal;

    using MyReports.Common;
	
	/// <summary>
	///		Summary description for authenticate.
	/// </summary>
	public partial  class authenticate : System.Web.UI.UserControl
	{
		private String m_securityManager = ConfigurationSettings.AppSettings[ "SECURITY_MANAGER_CLASS" ];
		public String SecurityManager
		{
			get	{	return m_securityManager;	}
			set	{	m_securityManager = value;}
		}
		private String m_loginTarget =  "~/login.aspx";
		public String LoginTarget
		{
			get	{	return m_loginTarget;	}
			set	{	m_loginTarget = value;}
		}

		private bool m_enableJavascript = true;
		public bool EnableJavascript
		{
			get {	return m_enableJavascript; }
			set {	m_enableJavascript = value; }
		}

		protected  string m_isPopUp = "false";
		//bool  m_derivedFromBasePage = false;

		private void AuthenticationError()
		{
			
			
			// Some windows which are opened using javascipt as Pop up
			// will close them selves, by examining the flag SessionTimeout flag
			// of the BasePageTemplate
			/* m_derivedFromBasePage = ( Page is basePageTemplate );
			if ( m_derivedFromBasePage )
			{
				((basePageTemplate)Page).SessionTimedOut = true;
				m_isPopUp = ((basePageTemplate)Page).IsPopUpWindow ? "true" : "false";
			}
			if ( !m_derivedFromBasePage  || !((basePageTemplate)Page).IsPopUpWindow )
			{*/
		

				AcUrlBuilder target = new AcUrlBuilder( m_loginTarget );

				target.AddKeyValue( AcParams.Name.serverURL, AcParams.Get( Page, AcParams.Name.serverURL ) );
				target.AddKeyValue( AcParams.Name.userID, AcParams.Get( Page, AcParams.Name.userID ) );
				target.AddKeyValue( AcParams.Name.timezone, AcParams.Get( Page, AcParams.Name.timezone ) );
				target.AddKeyValue( AcParams.Name.locale, AcParams.Get( Page, AcParams.Name.locale ) );
				target.AddKeyValue( AcParams.Name.volume, AcParams.Get( Page, AcParams.Name.volume ) );
		
				AcUrlBuilder targetPage = new AcUrlBuilder( this.Request.RawUrl, false );
			
				foreach ( string key in this.Request.Form.Keys )
				{
					// Keep all form values except viewstate which is too long
					if ( key != "__VIEWSTATE" && key != "__EVENTTARGET" && key != "__EVENTARGUMENT" )
					{
						targetPage.AddKeyValue( key, this.Request.Form[key] );
					}
				}

				AcParams.SetToSession( Session, AcParams.Name.targetPage, targetPage.ToString( ) );

				Response.Redirect( target.ToString( ) );
			//}

			
		}

		private void doLogin( AcLogin login )
		{
			login.Execute( Page );

			// Store the USer object in the Session
			if ( login.Response != null )
			{
				AcUserManager.SetUserPrivileges( Page, login.Response );
				AcUserManager.SetUser( Page, login.Response );
				// Store the feature options enabled
				AcUserManager.SetUserFeatureOptions( Page, login.Response );
				// Set the feature list into session
				AcFunctionalityManager.SetFeatureList( Page );
			}

		}

		protected void Page_Init(object sender, System.EventArgs e)
		{
			
			try
			{
				
				if ( AcParams.Get( Page, AcParams.Name.authID, AcParams.From.All, AcParams.SetTo.Session ) == null )
				{
					AcLogin login = null;

					//System.Reflection.Assembly assembly = System.Reflection.Assembly.Load( "MyReports" );

					object securityMgr = null;

					//Type T = assembly.GetType( m_securityManager );
					//Type T = assembly.GetType( "DistributionPortal.securitymanager" );
		
					//object[] args = { MyReports.Common.AppValues.UserName, MyReports.Common.AppValues.Password };

					//securityMgr = Activator.CreateInstance( T, args );

                    securityMgr = new securitymanager(AppValues.UserName, AppValues.Password);
				
					if	( ((AcSecurityManager)securityMgr).authenticate( Page ) )
					{
						// External authentication ok
						// Now do Actuate authentication to report server
						
						login = new AcLogin( (AcSecurityManager)securityMgr );
						doLogin( login );
					}
					else
					{
						AuthenticationError( );
					}
				
					// Check if security manager is registered

					if ( null == m_securityManager || "" == m_securityManager.Trim( )  )
					{						

						/*
						String userID = AcParams.Get( Page,AcParams.Name.userID , AcParams.From.Session | AcParams.From.Url | AcParams.From.Post);
						String password = AcParams.Get( Page, AcParams.Name.password);
						String volume = AcParams.Get( Page, AcParams.Name.volume);
						if ( null == password )
						{
							password = "";
						}
						if ( null == volume )
						{
							volume = ConfigurationSettings.AppSettings[ "DEFAULT_VOLUME" ];
						}
						if ( null == userID )
						{
							userID = AcParams.Get( Page, AcParams.Name.userID, AcParams.From.Cookie );
							if ( userID == null || userID.ToLower( ) != "anonymous" )
							{
								AcLog.Debug( "The user session has timed-out. Redirecting to login page" );
								AuthenticationError( );
							}
						}

						
							login = new AcLogin( userID,password,volume );
							doLogin( login );
							*/
					}
					else
					{
						// Get Security Manager
					
						/*
						System.Reflection.Assembly assembly = System.Reflection.Assembly.Load( "MyReports" );

						object securityMgr = null;

						//Type T = assembly.GetType( m_securityManager );
						Type T = assembly.GetType( "DistributionPortal.securitymanager" );
						securityMgr = Activator.CreateInstance( T );
								
						if	( ((AcSecurityManager)securityMgr).authenticate( Page ) )
						{
							// External authentication ok
							// Now do Actuate authentication to report server
						
							login = new AcLogin( (AcSecurityManager)securityMgr );
							doLogin( login );
						}
						else
						{
							AuthenticationError( );
						}
						*/
						
					}
					
					// If the user doesn't go through the login page, we need to get the system name here

					string systemName = AcParams.Get( Page, AcParams.Name.systemName );
					if ( systemName == null )
					{
						AcGetSystemVolumeNames volumeList = new AcGetSystemVolumeNames( true );
						volumeList.Execute( Page );
						if ( volumeList.Response != null )
						{
							AcParams.SetToSession( Session, AcParams.Name.systemName, volumeList.Response.SystemName );
						}
					}

					// we save it to cookie when we authenticate the first time
					// since when we loose the session, we also loose the serverurl
					// but the serverurl is needed in the login page

					string serverURL = AcParams.Get( Page, AcParams.Name.serverURL );
					if ( serverURL != null )
					{
						AcParams.SetToCookie( Response, AcParams.Name.serverURL, serverURL );
					}

					// Get the user profile. Force reload from server.
					//System.Collections.Hashtable userProfile =  AcProfileManager.GetUserProfile( Page, true );
					//AcParams.SetToCookie( Response, "skin", (string) userProfile[ "skin" ] );
					//AcProfileManager.SaveProfileToSessionVariables( Page );
					

				
				}
				
				string location = (string)Context.Items["Location"];
				if ( AcFunctionalityManager.ExistsFeature( location ) && AcFunctionalityManager.GetFeature( Page, location ) == null )
				{
					throw new AcException( "errors.functionality.permissiondenied", new AcKeyArgument[]{ new AcKeyArgument( null, location ) }, Page.Session, false );
				}

				
			}
			catch ( System.Web.Services.Protocols.SoapException soapException)
			{
				string target = m_loginTarget;
				
				target += "?errorMessage=";
				target += HttpUtility.UrlEncode( soapException.Detail["ErrorCode"].InnerText );
				target += HttpUtility.UrlEncode( ": " );
				target += HttpUtility.UrlEncode( soapException.Detail["Description"]["Message"].InnerText );
				
				string serverURL = AcParams.Get( Page, AcParams.Name.serverURL );
				if ( serverURL != null )
				{
					target += "&serverURL=";
					target += HttpUtility.UrlEncode( serverURL );
				}
				string volume = AcParams.Get( Page, AcParams.Name.volume );
				if ( volume != null )
				{
					target += "&volume=";
					target += HttpUtility.UrlEncode( volume );
				}
					
				Response.Redirect( target );
			}

			// Set the SessionExpired to 0. We are now authenticated

			AcParams.SetToCookie( Response, "AcSE", "0" );

			// Store the locale and the Time Zone info in the session
			string defLocale = AcParams.Get( Page, AcParams.Name.locale );

			if ( defLocale == null ||
				defLocale.Trim() == String.Empty )
			{
				defLocale = ConfigurationSettings.AppSettings[ "DEFAULT_LOCALE" ];
			}

			if ( defLocale == null )
			{
				defLocale = "en-US";
			}
			defLocale = defLocale.Trim( );

			try
			{
				Thread.CurrentThread.CurrentUICulture = new CultureInfo( defLocale );
			}
			catch ( Exception )
			{
				Thread.CurrentThread.CurrentUICulture = new CultureInfo( "en-US" );
			}

			Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;

			AcParams.SetToSession( Session, AcParams.Name.locale, defLocale);

			string timezone = AcParams.Get( Page, AcParams.Name.timezone );
			if ( timezone == null ||
				timezone.Trim() == String.Empty )
			{
				timezone = ConfigurationSettings.AppSettings[ "DEFAULT_TIMEZONE" ];
			}
			AcParams.SetToSession( Session, AcParams.Name.timezone, timezone);

			string redirect =  AcParams.Get( Page, AcParams.Name.redirectafterlogin, AcParams.From.Url );
			if ( redirect != null )
			{
				//Response.Redirect(redirect + "?app=rd");
				Response.Redirect(redirect);
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion

		protected void Page_Load(object sender, System.EventArgs e)
		{
		
		}			  


	}
}
