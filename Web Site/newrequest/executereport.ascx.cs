namespace activeportal.usercontrols.newrequest
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using actuate.classes.webcontrols;
	using activeportal.classes;
	using activeportal.classes.api.proxy;
	using actuate_i18n;
	using System.Collections.Specialized;
	using System.Reflection;
	using System.Text;

	/// <summary>
	///		Summary description for executereport.
	/// </summary>
	public partial  class executereport : System.Web.UI.UserControl
	{
		

		protected AcTabSeparator	ScheduleTabLeftSeparator;
		protected AcTabSeparator	ScheduleTabRightSeparator;


		
		protected System.Web.UI.WebControls.Table SaveOutputTbl;

		
		string m_jobType;
		public string JobType
		{
			get
			{
				return m_jobType;
			}

			set
			{
				m_jobType = value;
			}
		}

		private bool m_showSubmitJobLink = true;
		public bool ShowSubmitJobLink
		{
			get
			{
				return m_showSubmitJobLink;
			}

			set
			{
				m_showSubmitJobLink = value;
			}
		}

		private bool m_showExecuteRequestLink = true;

		public bool ShowExecuteRequestLink
		{
			get
			{
				return m_showExecuteRequestLink;
			}

			set
			{
				m_showExecuteRequestLink = value;
			}
		}


		private string m_browseFileTarget = "../common/browsefile.aspx";
		public string BrowseFileTarget
		{
			get
			{
				return m_browseFileTarget;
			}

			set
			{
				m_browseFileTarget = value;
			}
		}

		private string m_executeRequestTarget = "../newrequest/do_executereport.aspx";
		
		public string ExecuteRequestTarget
		{
			get
			{
				return m_executeRequestTarget;
			}

			set
			{
				m_executeRequestTarget = value;
			}
		}

		private string m_submitJobTarget = "../newrequest/do_submitjob.aspx";
		public string SubmitJobTarget
		{
			get
			{
				return m_submitJobTarget;
			}

			set
			{
				m_submitJobTarget = value;
			}
		}

		
		private void ProcessParameters( bool bPostBack )
		{
			
			if ( ! bPostBack )
			{

				m_jobType = AcParams.Get( Page, AcParams.Name.__requesttype, "immediate" );
				m_jobType = AcString.ToLower(m_jobType);

				// For th Navigation Control, storing the Value in the Context
				Context.Items[AcParams.Name.__requesttype.ToString()] = m_jobType;

				
				ViewState["JobType"] = m_jobType;


				if ( m_jobType ==  Constants.JobTypeEnum.JOB_TYPE_SCHEDULED.ToString() )
				{
					AcReportTabs.SelectedIndex = (int)Constants.JobTabEnum.JOB_TAB_SCHEDULED;
				}
				else
				{
					AcReportTabs.SelectedIndex = (int)Constants.JobTabEnum.JOB_TAB_PARAMETERS;
					
				}

				
				// Select the correct Tab depending on the params specified in the url
				string subPage = AcParams.Get( Page, AcParams.Name.__subPage, AcParams.From.Url  );
				if ( subPage == null )
				{
					if ( m_jobType  == "scheduled" )
					{
						subPage = "schedule";
					}
					else
					{
						subPage = "parameters";
					}
				}
				else
				{
					if ( m_jobType   == Constants.JOB_TYPE_IMMEDIATE  )
					{
						if ( AcString.ToLower(subPage) == "schedule" )
							subPage = "parameters";
						else if ( AcString.ToLower(subPage) == "save_as" )
							subPage = "output";
					}
				}
				switch ( AcString.ToLower(subPage) )
				{
					case "schedule":
						AcReportTabs.SelectedIndex = 0;
						break;
					case "parameters":
						AcReportTabs.SelectedIndex = 1;
						break;
					case "output":
						AcReportTabs.SelectedIndex = 2;
						break;
				}

			}
			else
			{
				// Post Back
				m_jobType = (string) ViewState["JobType"];

				// For th Navigation Control, storing the Value in the Context
				Context.Items[AcParams.Name.__requesttype.ToString()] = m_jobType;
			}
		}

		private string m_executableFileName;
		public  string ExecutableFileName
		{
			get
			{
				     m_executableFileName = (string) ViewState["ExecutableFileName"];
				
				if ( m_executableFileName == null )			
				{
					m_executableFileName = AcParams.Get( Page, AcParams.Name.__executableName );
					ViewState["ExecutableFileName"] = m_executableFileName;
				}
					return m_executableFileName;
			}

	
		}

		private void  RenderTabs( bool bPostBack )
		{
			((HtmlGenericControl)ActuUtil.FindControlRecursive( Page, "AcBody" )).Attributes.Remove( "OnLoad" );
			ProcessParameters( bPostBack);

			AcTabGroup ScheduleTab = ( AcTabGroup) AcReportTabs[0];

			ScheduleTab.Visible = true;

			if ( ! IsPostBack )
			{
				ValidJob.Value = "true";
			}


			if ( m_jobType ==  Constants.JOB_TYPE_IMMEDIATE )
			{
				ScheduleTab.Visible = false;

			}
			if (  m_jobType ==  Constants.JOB_TYPE_SCHEDULED )
				OutputTab.Key = "TAB_OUTPUT";
			else
				OutputTab.Key = "TAB_SAVE_AS";
			

			// The Links for the Simple Request and background job
			SimpleRequestLink.Text = AcResourceManager.GetString("LNK_SIMPLE_REQUEST", Session) + "&nbsp;";
			BackgroundJobLnk.Text = AcResourceManager.GetString("LNK_BACKGROUND_JOB", Session) + "&nbsp;" ;

			SubmitBtnTop.Text = " " + AcResourceManager.GetString("BTN_SUBMIT_REQUEST", Session);


			SubmitBtnBtm.Text = " " + AcResourceManager.GetString("BTN_SUBMIT_REQUEST", Session);


			SimpleRequestLink.CommandName = "JobType";
			SimpleRequestLink.CommandArgument = Constants.JOB_TYPE_IMMEDIATE;
				

			SimpleRequestLink.Command += new  System.Web.UI.WebControls.CommandEventHandler(OnLinkClick);


			BackgroundJobLnk.CommandName = "JobType";
			BackgroundJobLnk.CommandArgument = Constants.JOB_TYPE_SCHEDULED;
			BackgroundJobLnk.Command += new  System.Web.UI.WebControls.CommandEventHandler(OnLinkClick);

			if ( m_jobType ==  Constants.JOB_TYPE_SCHEDULED )
			{

				SimpleRequestLink.CssClass = "hyperlink";
				SimpleRequestLink.Enabled  = true;
				BackgroundJobLnk.Enabled = false;
				BackgroundJobLnk.CssClass = "fntDisabledLink" ;
				
		
			}
			else
			{

				SimpleRequestLink.CssClass = "disabledhyperlink" ;
				SimpleRequestLink.Enabled = false;

				BackgroundJobLnk.CssClass = "hyperlink";
				BackgroundJobLnk.Enabled = true;

			}

			BackgroundJobLnk.Visible = m_showSubmitJobLink;
			SimpleRequestLink.Visible = m_showExecuteRequestLink;

			// Make left blank of first tab invisible
			AcTabSeparator separator = AcReportTabs[ AcReportTabs.FirstVisibleIndex ].LeftSeparator;
			if ( separator != null )
			{
				separator.Visible = false;
			}

			// 
			Type SaveOutput = SaveOutputFile.GetType();

			PropertyInfo browseFile = SaveOutput.GetProperty( "BrowseFileTarget" );
			browseFile.SetValue( SaveOutputFile, m_browseFileTarget, null );
	
		}
		

		public void SubmitJob_Clicked(Object sender, EventArgs e) 
		{
			// If the Job Is Not Valid then do not send the request
			if ( ValidJob.Value == null ||
				ValidJob.Value.Trim() == String.Empty ||
				AcString.ToLower(ValidJob.Value.Trim()) == "false" )
			{
				ValidJob.Value = "true";
				return;
			}
			// Gtaher all the Parameters and Submit the Job
			// Depending on the Type of the job either execute the report
			// or submit the job
			
			
			AcUrlBuilder link = new AcUrlBuilder( );


			if ( m_jobType ==  Constants.JOB_TYPE_SCHEDULED )
			{
				link.SetBaseUrl( m_submitJobTarget, true );

				PrepareExecuteReportRequest( Constants.JobTypeEnum.JOB_TYPE_SCHEDULED, ref link );
				PrepareScheduleDetails( ref link );
				PrepareEmailAttachmentDetails( ref link );
				PrepareConversionOptions( ref link );
				Server.Transfer( link.ToString( ) );
			}
			else
			{
				link.SetBaseUrl( m_executeRequestTarget, true );

				PrepareExecuteReportRequest( Constants.JobTypeEnum.JOB_TYPE_IMMEDIATE, ref link );

				bool ViewDocumentInNewBrowser = ActuUtil.ViewDocumentInNewBrowser( Page );
				if ( ViewDocumentInNewBrowser )
 				{
					link.AddKeyValue( "doframe", "false");
					Page.RegisterStartupScript("OpenViewerScript","<script language=javascript>openPopupWindow( '" + ActuUtil.jsEncode( link.ToString( ) ) + "','AcViewWindow',800,540,'status, scrollbars, resizable,menubar,toolbar, location' );</script>");
				}
				else
				{
					Server.Transfer( link.ToString( ) );
				}
			}


		
		}


		private void PrepareExecuteReportRequest( Constants.JobTypeEnum jobType, ref AcUrlBuilder link)
		{

			// THis field would be checked to see the source of the request

			

			string sExecutableName = new String( this.ExecutableFileName.ToCharArray() );

			link.AddKeyValue( AcParams.Name.__executableName, sExecutableName );
			link.AddKeyValue( AcParams.Name.__SourceOfRequest__, "Index.aspx" );

			
			if ( m_jobType == Constants.JOB_TYPE_IMMEDIATE )
			{
				string sJobName =  AcParams.Get( Page, AcParams.Name.__jobname );
				if ( sJobName == null )
				{
					sJobName = ActuUtil.stripFileExtension( ActuUtil.stripFolderName( sExecutableName , "/"));
					link.AddKeyValue( AcParams.Name.__jobname, sJobName );
				}

				// Addd the __wait parameter 

				string sWait = AcParams.Get( Page, AcParams.Name.__wait );

				if ( sWait != null && 
					 sWait.Trim() != String.Empty )
				{
					link.AddKeyValue( AcParams.Name.__wait , sWait);
				}
			}
			else
			{
				// The JOb Name should be retrieved from the schedule Tab

				Type schedule = Schedule.GetType();
				PropertyInfo  jobName = schedule.GetProperty( "JobName");
				link.AddKeyValue( AcParams.Name.__jobname, (string)jobName.GetValue( Schedule, null ) );
				
			}
			
			// For execueReport There is no schedule
			if ( m_jobType == Constants.JOB_TYPE_IMMEDIATE )
			{
				link.AddKeyValue( AcParams.Name.__scheduleType, "immediate" );
			}

					
			// save as
			
			Type _saveOutputFileType = SaveOutputFile.GetType();
			PropertyInfo _saveOutputFileType_SaveOutput = _saveOutputFileType.GetProperty("SaveOutPut");
			bool bSaveOutput = (bool) (_saveOutputFileType_SaveOutput.GetValue( SaveOutputFile, null ));

			if ( bSaveOutput )
			{
				link.AddKeyValue( AcParams.Name.__saveOutput, "checked" );
			}

			string outputFolderType = "";
			string outputFileName = "";
			string outputFolderName = "/";
		
			
			// Output Folder Type
			PropertyInfo _saveOutputFile_OutputFolderType = _saveOutputFileType.GetProperty("OutputFolderType");
			if ( _saveOutputFile_OutputFolderType != null )
			{
				outputFolderType = (string) ( _saveOutputFile_OutputFolderType.GetValue( SaveOutputFile, null));
				link.AddKeyValue( AcParams.Name.__outputfoldertype, outputFolderType );
			
			}

			// Document Name
			PropertyInfo _saveOutputFile_OutputName = _saveOutputFileType.GetProperty("OutputFileName");
			if ( _saveOutputFile_OutputName != null )
			{
				outputFileName = ( string) ( _saveOutputFile_OutputName.GetValue( SaveOutputFile, null ) );
			}


			// OutputFolderName
			PropertyInfo _saveOutputFile_OutputFolderName = _saveOutputFileType.GetProperty("OutputFolderName");
			if ( _saveOutputFile_OutputFolderName != null )
			{
				outputFolderName = ( string) ( _saveOutputFile_OutputFolderName.GetValue( SaveOutputFile, null ) );
			}

			

			if ( outputFolderType == "absolute" )
			{

				if ( outputFolderName == null ||
					outputFolderName == String.Empty )
				{
					outputFolderName = "/";
				}
				else
				{
	
					if ( !outputFolderName.StartsWith("/" ))
					{
						// If the output folder however starts with ~
						// then this means that the Report is to be put in the home folder of
						// the user
						// Do not 
						if ( !outputFolderName.StartsWith("~"))
						outputFolderName = "/" + outputFolderName;
					}
					
					// make sure the outputfoldername ends with /
					
					if ( ! outputFolderName.EndsWith( "/" ) )
					{
						outputFolderName += "/";
					}
					outputFileName = outputFolderName + outputFileName;
				}
			}

			link.AddKeyValue( AcParams.Name.__outputName, outputFileName );
			
			PropertyInfo _saveOutputFile_VersionName = _saveOutputFileType.GetProperty("VersionName");

			if ( _saveOutputFile_VersionName != null )
			{
				link.AddKeyValue( AcParams.Name.__versionName,  ( string) ( _saveOutputFile_VersionName.GetValue( SaveOutputFile, null )) );
			}

			
			PropertyInfo _saveOutputFile_IfExists = _saveOutputFileType.GetProperty("IfExists");

			if ( _saveOutputFile_IfExists != null )
			{
				link.AddKeyValue( AcParams.Name.__ifexists,  (string) ( _saveOutputFile_IfExists.GetValue( SaveOutputFile, null ) ) );
			}

																															 
			//Headline 
			if ( m_jobType == Constants.JOB_TYPE_SCHEDULED )
			{
				PropertyInfo _saveOutputFile_headline = _saveOutputFileType.GetProperty("HeadLine");
				string headline = (string)( _saveOutputFile_headline.GetValue( SaveOutputFile, null ) );
				if ( headline != null &&
					headline.Trim() != String.Empty )
				{
					link.AddKeyValue( AcParams.Name.__headline,  headline  );
				}
                
			}																													 
			
			// Populate  with the Parameters Name and Values
			Type _reportParametersType = ReportParameters.GetType();
			MethodInfo _reportParameters_Method = _reportParametersType.GetMethod( "PopulateParametersInContext");

			if ( _reportParameters_Method != null )
			{
				_reportParameters_Method.Invoke( ReportParameters, null );
			} 


			// Add the RequestType to the Url
			link.AddKeyValue( AcParams.Name.__requesttype, m_jobType );

		
			
		}

		// Method to be called to prepare the schedule
		// related details when using a scheduled job

		private void	PrepareScheduleDetails( ref AcUrlBuilder link )
		{
			if ( m_jobType !=  Constants.JOB_TYPE_SCHEDULED )
			{
				return;
			}

			Type _scheduleType = Schedule.GetType();
			MethodInfo _schedule_Method = _scheduleType.GetMethod("GetScheduleDetails");

			if ( _schedule_Method != null )
			{
				_schedule_Method.Invoke( Schedule, new Object[] { link });
			}
		}

	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			bool bPostBack = IsPostBack;
			RenderTabs( bPostBack );

		}

		protected void Page_Init( object sender, System.EventArgs e )
		{
			Context.Items["ActualLocation"] = "NewRequests";
		}
		
		protected void OnLinkClick(object O, System.Web.UI.WebControls.CommandEventArgs E)
		{
			m_jobType = (string) E.CommandArgument;
			bool bPostBack = IsPostBack;

			ViewState["JobType"] = m_jobType;

			//Set the properties of the SaveOutputTabs
			Type SaveOutput = SaveOutputFile.GetType();

			PropertyInfo JobType = SaveOutput.GetProperty( "JobType" );
            JobType.SetValue( SaveOutputFile, m_jobType, null );

			ProcessParameters( IsPostBack );
			RenderTabs( bPostBack );

			// Reset the value of the hidden field

			// Set the correct Tab Index

			if ( m_jobType == Constants.JOB_TYPE_IMMEDIATE )
			{
				AcReportTabs.SelectedIndex = 1;

			}
			else
			{
				AcReportTabs.SelectedIndex = 0;
			}


			// Set the Value of the Navigation Control
			// to display the correct HeadLine
			Context.Items[ AcParams.Name.__requesttype.ToString() ] = m_jobType;

			
		}

		protected override void OnPreRender( EventArgs e )
		{
			int SelectedIndex = AcReportTabs.SelectedIndex ;
			if ( SelectedIndex == 2 )
			{
				SubmitBtnTop.Attributes["onclick"] = "javascript:validateSaveOutput(" + GetSaveOutputArgs() + ");";
				SubmitBtnBtm.Attributes["onclick"] = "javascript:validateSaveOutput(" + GetSaveOutputArgs() + ");";
				AcReportTabs.SwitchingJavaScript = "validateSaveOutput(" + GetSaveOutputArgs() + ") ";
			}
			else if ( SelectedIndex == 0)
			{

				SubmitBtnTop.Attributes["onclick"] = "javascript:ValidateSchedule(" + GetScheduleArgs() + ");";
				SubmitBtnBtm.Attributes["onclick"] = "javascript:ValidateSchedule(" + GetScheduleArgs() + ");";
				AcReportTabs.SwitchingJavaScript = "ValidateSchedule(" + GetScheduleArgs() + ") ";
			}
			else 
			{
				SubmitBtnTop.Attributes["onclick"] = null;
				SubmitBtnBtm.Attributes["onclick"] = null;

			}

			RegisterHelpTopics();
		}


		private void RegisterHelpTopics()
		{
			// Set help topic

			switch ( AcReportTabs.SelectedIndex)
			{
				case 0:
					
					ActuUtil.RegisterHelpTopic( Page, "NewRequest_schedule");
					break;

				case 1:
					ActuUtil.RegisterHelpTopic( Page, "NewRequest_parameters" );
					break;

				case 2:
					if (  m_jobType ==  Constants.JOB_TYPE_SCHEDULED )
						ActuUtil.RegisterHelpTopic( Page, "NewRequest_output" );
					else
						ActuUtil.RegisterHelpTopic( Page, "NewRequest_save" );
					break;

			}

		}

		private string GetSaveOutputArgs()
		{
			StringBuilder SaveOutputArgs = new StringBuilder();

			Type saveFile = SaveOutputFile.GetType();
			MethodInfo GetValidationFields = saveFile.GetMethod( "GetValidationFields");

			if ( GetValidationFields != null )
			{
				SaveOutputArgs.Append( (string) GetValidationFields.Invoke( SaveOutputFile, null ));
				// Add the Valid Job Hidden Field

				SaveOutputArgs.Append( ",");
				SaveOutputArgs.Append( "'" + ValidJob.ClientID + "'");
				
			}
 
			return SaveOutputArgs.ToString();
		}

		private string GetSubmitJobArgs()
		{
			StringBuilder submitJobArgs = new StringBuilder();

			submitJobArgs.Append( "'" + m_jobType + "'," );
			submitJobArgs.Append( "'" +   AcReportTabs.SelectedIndex  + "'," );
			submitJobArgs.Append( "'" + ValidJob.ClientID + "'" );

			// Get the fields to be validated form each Tab Control
			if ( AcReportTabs.SelectedIndex == 2 )
			{
				Type saveFile = SaveOutputFile.GetType();
				MethodInfo GetValidationFields = saveFile.GetMethod( "GetValidationFields");
				string OutputValidationFields = "";

				if ( GetValidationFields != null )
				{
					OutputValidationFields = (string) GetValidationFields.Invoke( SaveOutputFile, null );
				}
 
				if ( OutputValidationFields != null && 
					OutputValidationFields.Trim() != String.Empty )
				{
					submitJobArgs.Append( "," );
					submitJobArgs.Append( OutputValidationFields );
				}
			}


			return submitJobArgs.ToString();

		}


		private string GetScheduleArgs()
		{

			StringBuilder scheduleArgs = new StringBuilder();

			Type scheduleType = Schedule.GetType();
			MethodInfo GetValidationFields = scheduleType.GetMethod( "GetValidationFields");

			string args = (string) GetValidationFields.Invoke( Schedule, null );

			scheduleArgs.Append( args );
			scheduleArgs.Append( "," );
			scheduleArgs.Append( "'" + ValidJob.ClientID + "'" );

			return scheduleArgs.ToString();

		}

		private void PrepareEmailAttachmentDetails( ref AcUrlBuilder link )
		{

			Type saveFile = SaveOutputFile.GetType();
			PropertyInfo EmailNotification = saveFile.GetProperty( "SendEmailNotification");

			bool SendEmailNotification = (bool)EmailNotification.GetValue( SaveOutputFile , null );

			PropertyInfo AttachmentFormat = saveFile.GetProperty( "AttachmentFormat");

			string format = (string)AttachmentFormat.GetValue( SaveOutputFile, null );

			PropertyInfo EmailNotificationEnabled = saveFile.GetProperty( "SendEmailNotification");
			bool Notify = (bool) EmailNotificationEnabled.GetValue( SaveOutputFile, null );

			if ( Notify )
			{
				link.AddKeyValue( AcParams.Name.notify, SendEmailNotification.ToString() ); 
				link.AddKeyValue( AcParams.Name.notificationAttachment, format );
			}

		}

		private void PrepareConversionOptions( ref AcUrlBuilder link )
		{

			Type saveFile = SaveOutputFile.GetType();
			PropertyInfo DocumentConversionEnabled = saveFile.GetProperty( "DocumentConversionEnabled");

			bool ConvertDocument = (bool)DocumentConversionEnabled.GetValue( SaveOutputFile , null );

			link.AddKeyValue( AcParams.Name.OutputConversionEnabled, ConvertDocument.ToString() );
			if ( ConvertDocument )
			{

				PropertyInfo ConversionFormat = saveFile.GetProperty( "DocumentConversionFormat");
				string format = (string)ConversionFormat.GetValue( SaveOutputFile, null );

				if ( format != null )
				{
					link.AddKeyValue( AcParams.Name.__Format, format ); 
					if ( format.ToLower() != "roi" )
					{
						link.AddKeyValue( AcParams.Name.__KeepROIIfSucceeded, "false" );
						link.AddKeyValue( AcParams.Name.__KeepROIIfFailed, "false" );
					}
				}

			}

		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion
	}
}
