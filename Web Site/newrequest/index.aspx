<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<%@ Register TagPrefix="template" namespace="actuate.classes.webcontrols.masterpages" assembly="activeportal" %>
<%@ Register TagPrefix="actuate" TagName="executereport" Src="executereport.ascx" %>
<%@ Page language="c#" Inherits="DistributionPortal.newrequest.index" CodeFile="index.aspx.cs" %>
<template:contentcontainer Location="Documents" runat="server" id="Contentcontainer1">
	<template:content id="title" runat="server">
		<localized:AcLiteral id="AcLiteral1" runat="server" Key="MSGT_BROWSER">
			<ARGUMENT key="TBAR_NEW_REQUEST" />
		</localized:AcLiteral>
	</template:content>
	<template:content id="content" runat="server">
		<actuate:executereport id="Executereport1" runat="server" BrowseFileTarget="../common/browsefile.aspx"
			ExecuteRequestTarget="../newrequest/do_executereport.aspx" SubmitJobTarget="../newrequest/do_submitjob.aspx"
			ShowExecuteRequestLink="true" ShowSubmitJobLink="true"></actuate:executereport>
	</template:content>
</template:contentcontainer>
