namespace activeportal.usercontrols.newrequest
{
	using System;
	using System.Collections;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes;
	using actuate_i18n;
	using activeportal.classes.api;
	using activeportal.classes.api.proxy;
	using System.Text;
	using System.Reflection;
	using activeportal.classes.functionality;
	/// <summary>
	///		Summary description for SaveFile.
	/// </summary>
	public partial  class SaveFile : System.Web.UI.UserControl
	{
		#region Controls



		protected  TableRow OutputLocationRow;




		


		protected string sDocumentConversionEnabled;
		protected string sEmailNotificationEnabled;


		#endregion

		#region Fields
		private string m_execName;
		private string m_extension;

		// In case of a rov etc, the extension of the executable file , on which it depends
		private string m_actualExeFileExtension;  
		protected string m_defOutputFolder;
		protected bool m_homeFolderExists = false;
		protected string m_jobType;

		protected string m_noAttachmentText;
		#endregion
		
		#region Properties
		public bool SaveOutPut
		{
			get
			{
				if ( m_jobType == Constants.JOB_TYPE_SCHEDULED )
				{
					return true;
				}
				else
				{
					return SaveToFileChkBox.Checked;
				}
			}
			
			set
			{
				SaveToFileChkBox.Checked = value;
			}

		}

		private string m_outputFolderType;
		public  string OutputFolderType
		{
			get
			{
				if ( PersonalFolderRdBtn.Checked )
					m_outputFolderType =  "personal";
				else if ( AbsoluteFolderRdBtn.Checked )
					m_outputFolderType = "absolute";

				return m_outputFolderType;
			}

			set
			{
				m_outputFolderType = value;

				if ( m_outputFolderType == "personal" )
				{
					PersonalFolderRdBtn.Checked = true;
				}
				else if ( m_outputFolderType == "absolute" )
				{
					AbsoluteFolderRdBtn.Checked = true;
				}

			}
		}

		
		public string OutputFileName
		{
			get
			{
				return ActuUtil.stripFileExtension( OutputNameTxt.Value  )+ "." + AcLstFileType.SelectedItem.Value;
			}

		}

		public string OutputFolderName
		{
			get
			{
				return AbsFolderTxt.Text;
			
			}

			set
			{
				AbsFolderTxt.Text = value;
			}

		}

		public string VersionName
		{
			get
			{
				return VersionNameTxt.Text;
			}

			set
			{
				VersionNameTxt.Text = value;
			}
		}

		public string IfExists
		{
			get
			{

				if ( this.NewVersionRdBtn.Checked )
				{
					if ( NumVersionChk.Checked )
					{
						return "create" + NumVersionTxt.Text ;
					}
					else
					{
						return "create";
					}
				}
				else if ( this.ReplaceVersionRdBtn.Checked )
					return "replace";
				else
					return "create";
			}

			set
			{
				if ( value.ToLower().Trim().StartsWith( "create" ) )
				{
					NewVersionRdBtn.Checked = true;
					NumVersionTxt.Text =  value.Substring( 6 );
					NumVersionChk.Checked = true;
					NumVersionTxt.Enabled = true;
				}
				else if ( value.ToLower().Trim().StartsWith( "replace" ) )
				{
					ReplaceVersionRdBtn.Checked = true;
				}
				else
					NewVersionRdBtn.Checked = true;

			}
		}

		public string ExecutableFileName
		{
			get
			{
				return DocumentNameTxt.Text ;
			}
		}

		

		public string JobType
		{
			get
			{
				return m_jobType;
			}

			set
			{
				m_jobType = value;
			}
		}

		
		public string MaxVersionsToRetain
		{
			get
			{
				return NumVersionTxt.Text;
			}

			set
			{
				NumVersionTxt.Text = value;
			}
		}
		public bool LimitVersions 
		{ 
			get { return NumVersionChk.Checked; }
			set { NumVersionChk.Checked = value; }
		}

		public string HeadLine
		{
			get
			{
				return HeadLineTxt.Text;
			}

			set
			{
				HeadLineTxt.Text = value;
			}
		}

		private string m_browseFileTarget = "../common/browsefile.aspx";
		public string BrowseFileTarget
		{
			get
			{
				return m_browseFileTarget;
			}

			set
			{
				m_browseFileTarget = value;
			}
		}

		public bool SendEmailNotification
		{
			get{ return AcChkEmail.Checked ; }
		}

		public string AttachmentFormat 
		{
			get
			{
				string emailFormat = null;
				ListItem selectedItem = AcLstAttachment.SelectedItem;

				switch ( selectedItem.Value )
				{
					case Constants.ATTACHMENT_FORMAT_NO:
						emailFormat = "no";
						break;
				
					case Constants.ATTACHMENT_FORMAT_DEFAULT:
						emailFormat = "";
						break;
				
					case Constants.ATTACHMENT_FORMAT_PDF:
						emailFormat = "PDF";
						break;
				
					case Constants.ATTACHMENT_FORMAT_EXCEL_DISPLAY:
						emailFormat = "ExcelDisplay";
						break;

					case Constants.ATTACHMENT_FORMAT_EXCEL_DATA:
						emailFormat = "ExcelData";
						break;

					case Constants.ATTACHMENT_FORMAT_RTF_TEXTBOX:
						emailFormat = "RTFFullyEditable";
						break;

					case Constants.ATTACHMENT_FORMAT_RTF:
						emailFormat = "RTF";
						break;
				}
				return emailFormat;
			}
		}

		public bool EmailNotificationEnabled
		{
			get{ return AcRowNotification.Visible ; }
		}

		public bool DocumentConversionEnabled
		{
			get
			{
				if ( m_jobType != "scheduled" )
				{
					return false;
				}

				if ( m_actualExeFileExtension != null &&
					( m_actualExeFileExtension.ToLower() == "rox"  ||
					  m_actualExeFileExtension.ToLower() == "rov"  ||
					  m_actualExeFileExtension.ToLower() == "roi" ))
				{
					return true;
				}

				return false;
			}
		}

		public string DocumentConversionFormat
		{
			get { return AcLstDocumentFormat.SelectedItem.Value; }
		}

		#endregion

		#region Public Functions
		// Utility Function which validates  some of the fields entered
		public void ValidateFields()
		{
			// The Number of Versions should be a valid Integer

			if ( NumVersionChk.Checked )
			{
				try
				{
					uint numVersions = Convert.ToUInt32( NumVersionTxt.Text );
				}
				catch
				{
					throw new AcException ( "ERRMSG_KEEP_NUMBER_INVALID", null, Session, false );
				}
			}
		}


		
		// Called by the main Tab Control to return a List Of fields
		// to be validated and also the messages which have to be displayed 
		// if the validation fails
		public string GetValidationFields()
		{
			StringBuilder args = new StringBuilder();

			string SaveDocument = "false";

			// For scheduled jobs always save the output document
				
			SaveDocument = SaveOutPut.ToString();

						
			// Document Name related
			ActuUtil.AddJsArgument( args,SaveDocument );


			string OutputFolderType = AbsoluteFolderRdBtn.Checked ? "absolute" : "personal";
			ActuUtil.AddJsArgument( args, OutputFolderType );
			ActuUtil.AddJsArgument( args, AbsFolderTxt.ClientID );
			ActuUtil.AddJsArgument( args, AcResourceManager.GetString( "ERRMSG_INVALID_OUTPUT_FOLDER", Session ));
			ActuUtil.AddJsArgument( args, AcResourceManager.GetString( "ERRMSG_INVALID_START_CHAR", Session ));
			

			ActuUtil.AddJsArgument( args, DocumentNameTxt.ClientID );
			ActuUtil.AddJsArgument( args, AcResourceManager.GetString("ERRMSG_EMPTY_OUTPUT_DOC", Session ));

			// Number of versions to retain related
			string CreateReplace = NewVersionRdBtn.Checked ? "create" : "replace" ;

			ActuUtil.AddJsArgument( args, NumVersionChk.ClientID );
			ActuUtil.AddJsArgument( args, NumVersionTxt.ClientID );
			ActuUtil.AddJsArgument( args, CreateReplace );
			ActuUtil.AddJsArgument( args, AcResourceManager.GetString( "ERRMSG_KEEP_NUMBER_INVALID" , Session), true);
			return args.ToString();
		}
		#endregion
		
		#region Private Functions

		private void PopulateSaveToFileTable( )
		{
			if ( ! IsPostBack )
			{
				PopulateOutputTypeList( );
				JobUtil.PopulateEmailAttachmentFormat(AcLstAttachment, m_extension, JobUtil.GetDefaultOutputType( m_extension, m_execName, Page ), Page);
				BrowseFolderBtn.Attributes.Add( "OnClick", "openBrowser();" ) ;
				string defOutputFolder= "";
				// No Home Folder
				// Disable the Home Folder option
				// Display The defaultout folder
				if ( ! m_homeFolderExists )
				{
					defOutputFolder = ActuUtil.GetFolderFromFileName(m_execName);
					PersonalFolderRdBtn.Checked = false;
					PersonalFolderRdBtn.Enabled=false;

					AbsoluteFolderRdBtn.Checked = true;
					AbsFolderTxt.Text = m_defOutputFolder;
				}
				else
				{
					// User has a Home Folder
					// Check the  Personal Folder rd button
					// Disbale the Absolute Folder text ( will be enabled only when the 
					m_homeFolderExists = true;
					PersonalFolderRdBtn.Checked = true;

					AbsoluteFolderRdBtn.Checked = false;
					AbsFolderTxt.Enabled = false;
					BrowseFolderBtn.Attributes.Add( "disabled", "disabled" );
				}

				if ( m_jobType == "scheduled" )
				{
					SaveToFileChkBox.Visible = false;
					HeadLineRow.Visible = true;
					HeadLineTxt.Text = AcParams.Get( Page, AcParams.Name.__headline, "", AcParams.From.Url );
				}
				else
				{
					SaveToFileChkBox.Visible = true;
					SaveToFileChkBox.Checked = AcParams.urlCheckboxState( Page, AcParams.Name.__saveOutput, SaveToFileChkBox.Checked );
					HeadLineRow.Visible = false;
				}

				SaveToFileChkBox.Text = AcResourceManager.GetString("CB_SAVE_IN_SERVER", Session);

				AbsFolderTxt.Width = new Unit( 35, UnitType.Ex );
		
				DocumentNameLbl.Text = " " + AcResourceManager.GetString( "MSGT_FILE_NAME", Session );
				DocumentNameTxt.Width = new Unit ( 35, UnitType.Ex );

				DocumentNameTxt.Text = GetDefaultOutputName();
				OutputNameTxt.Value = DocumentNameTxt.Text;
			

				VersionNameLbl.Text = " " + AcResourceManager.GetString( "MSGT_VERSION_NAME", Session);
				VersionNameTxt.Width = new Unit( 35, UnitType.Ex );
				VersionNameTxt.Text = AcParams.Get( Page, AcParams.Name.__versionName, "", AcParams.From.Url );
			
				FileExistsLbl.Text = " " + AcResourceManager.GetString( "MSGT_IF_EXISTS", Session);
				
				IfExists = AcParams.Get( Page, AcParams.Name.__ifexists, "", AcParams.From.Url );
				
				VersionLbl.Text = "  " + AcResourceManager.GetString( "CB_KEEP_LATEST", Session) + " " ;
				NumVersionLbl.Text = " " +  AcResourceManager.GetString("CB_VERSIONS", Session);
			}
			else
			{
				// PostBack
				// Since some of the Fields are being disabled using Javascript
				// they wil not be updated automatically, we need to update them accoridingly

				if ( m_jobType == "scheduled" )
				{
					SaveToFileChkBox.Visible = false;
					HeadLineRow.Visible = true;
				}
				else
				{
					SaveToFileChkBox.Visible = true;
					HeadLineRow.Visible  = false;
				}
				AbsFolderTxt.Enabled = AbsoluteFolderRdBtn.Checked ;
			}

			PopulateDocumentFormat();
		}

		private void PopulateOutputTypeList( )
		{
			String l_extension = m_extension;
			
			if (l_extension != null && l_extension.ToLower() == "rov")
			{
				String l_tmpExtension = GetDependentFileExtension();

				if( l_tmpExtension != null && l_tmpExtension.Trim() != String.Empty )
				{
					l_extension = l_tmpExtension;
				}
			}

			if ( l_extension != null )
			{
				int SelectedIndex = 0;
				String[] outputTypes;
				switch ( l_extension.ToLower( ) )
				{
					case "jod":
					case "jox":
						outputTypes = new String[]{"row","pdf","xls","xml","joi","csv", "rtf"};
						break;

					case "joi":
						outputTypes = new String[]{"row","pdf","xls","xml","csv", "rtf"};
						break;
				
					case "rpx":
						outputTypes = new String[]{"rpt","rpw"};
						// By Default "rpw" is to be selected
						SelectedIndex = 1;
						break;

					case "sqt":
						outputTypes = new String[]{"spf","sqw"};
						break;

					default:
						outputTypes = new String[]{ JobUtil.GetDefaultOutputType( l_extension, m_execName, Page ) };
						break;
				}

				AcLstFileType.DataSource = outputTypes;
				AcLstFileType.DataBind();
				AcLstFileType.SelectedIndex = SelectedIndex;

				// Localize
				foreach ( ListItem item in AcLstFileType.Items )
				{
					item.Text = AcResourceManager.GetString( "output.format." + item.Text, Session );
				}

			
			}
			
			if ( AcLstFileType.Items.Count < 2 )
			{
				AcLstFileType.Visible = false;
				AcLblFileType.Visible = false;
			}
			else
			{
				AcLstFileType.Visible = true;
				AcLblFileType.Visible = true;
			}

		
		}


		protected string GetDefaultOutputName()
		{
			string outputName = AcParams.Get( Page, AcParams.Name.__outputName );
			if ( outputName == null )
			{
				if ( m_execName != null )
				{
					return ActuUtil.stripFileExtension( ActuUtil.stripFolderName( m_execName ));
				}
			}
			return outputName;
		}



		#endregion
		
		#region Control Events
		public void PersonalFolderRdBtn_CheckedChanged( Object sender, EventArgs args )
		{
			// If PErsonal Folder Radio Button is clicked then
			// Disable the Absolute Folder Radio button and also
			// The text box for inputting the Absolute Folder

			if ( PersonalFolderRdBtn.Checked )
			{
				AbsoluteFolderRdBtn.Checked = false;
				AbsoluteFolderRdBtn.Enabled = false;
				AbsFolderTxt.Enabled = false;
			}
		}

		public void AbsoluteFolderRdBtn_CheckedChanged( Object sender, EventArgs args )
		{
			if ( AbsoluteFolderRdBtn.Checked )
			{
				PersonalFolderRdBtn.Checked = false;
			}
		}
		#endregion

		#region Page Events
		
		protected void Page_Init(object sender, System.EventArgs e)
		{
			m_execName = AcParams.Get( Page, AcParams.Name.__executableName );
			m_extension = ActuUtil.getFileExtension(m_execName);
			
		}

		
		protected void Page_Load(object sender, System.EventArgs e)
		{
			
			// Note: In case of a PostBack,  especially caused by the 
			// clicking of the Link BUttons, this property has to be set by the parent

			// First try to get the property from the Parent
			// Else the try to retrieve it from the Url

			Type tab = (Parent.Parent.Parent).GetType( );
			PropertyInfo JobType  = tab.GetProperty( "JobType" );
			m_jobType = (string) JobType.GetValue( Parent.Parent.Parent, null );

			
			string homeFolder = AcParams.Get( Page, AcParams.Name.homeFolder );

			if ( homeFolder == null ||
				homeFolder.Trim() == String.Empty )
			{
				m_homeFolderExists = false;
			}
			else
			{
				m_homeFolderExists = true;
			}

			m_defOutputFolder =  ActuUtil.GetFolderFromFileName(m_execName); 

			if ( !IsPostBack)
			{
				// For document conversion , check if this file depends on a file which is not a "rox"
				// We can have a rov , created from a third party report, and in this case we should not be enabling the 
				// document format conversion

				m_actualExeFileExtension = m_extension;
				if ( m_extension != null && m_extension.ToLower() == "rov" )
				{
					m_actualExeFileExtension = GetDependentFileExtension();

					if( m_actualExeFileExtension == null || m_actualExeFileExtension.Trim() == String.Empty )
					{
						m_actualExeFileExtension = m_extension;
					}
				}

				ViewState["ActualExeFileExtension"] = m_actualExeFileExtension;

			}
			else
			{
				m_actualExeFileExtension = (string) ViewState["ActualExeFileExtension"];
			}

			PopulateSaveToFileTable( );

			// Not to be displayed for transient reports
			
			// The Defualt value for the check box, is taken from the 
			// Users preference for sending email for succeeded jobs


			if ( ! IsPostBack )
			{
				User user = AcUserManager.GetUserInfo( Page );
				if ( user != null )
				{
					AcChkEmail.Checked = user.SendEmailForSuccess;
				}

			}

			AcChkEmail.Text = AcResourceManager.GetString( "output.notification.label.emailnotification", Session );
			string formatString = AcResourceManager.GetString( "output.notification.attachment.attachment", Session );

			AcLstFileType.Attributes.Add( "onchange", "UpdateNotificationList('" +  ActuUtil.jsEncode(formatString) + "','" + AcLstFileType.ClientID + "','" + AcLstAttachment.ClientID + "')" );
			Page.RegisterStartupScript( "updatenotification", "<script language=javascript>UpdateNotificationList('" +  ActuUtil.jsEncode(formatString) + "','" + AcLstFileType.ClientID + "','" + AcLstAttachment.ClientID + "'); </script>");

			m_noAttachmentText = ActuUtil.jsEncode( AcResourceManager.GetString("output.notification.attachment.no", Session ));


		}

		
		protected override void OnPreRender( EventArgs e )
		{
					
			// Note: In case of a PostBack,  especially caused by the 
			// clicking of the Link BUttons, this property has to be set by the parent

			// First try to get the property from the Parent
			// Else the try to retrieve it from the Url

			Type tab = (Parent.Parent.Parent).GetType( );
			PropertyInfo JobType  = tab.GetProperty( "JobType" );
			m_jobType = (string) JobType.GetValue( Parent.Parent.Parent, null );

			ReplaceVersionRdBtn.Attributes.Add( "OnClick", "javascript:ReplaceVersionRdBtnClicked(true);");

			NewVersionRdBtn.Attributes.Add( "OnClick", "javascript:ReplaceVersionRdBtnClicked(false);");

			NumVersionChk.Attributes.Add( "OnClick", "javascript:NumVersionChkClicked();"); 

			PersonalFolderRdBtn.Attributes.Add( "OnClick", "javascript:PersonalFolderRdBtnClicked();");

			AbsoluteFolderRdBtn.Attributes.Add( "OnClick", "javascript:AbsoluteFolderRdBtnClicked(" +
				"'" + ActuUtil.jsEncode( m_defOutputFolder ) + "');") ;

			AcUrlBuilder browseFileTarget  = new AcUrlBuilder( m_browseFileTarget  );
			browseFileTarget.AddKeyValue( "backTarget", AbsFolderTxt.ClientID );
			browseFileTarget.AddKeyValue( "openerWindowName", "AcMainWindow" );
	
			string link = browseFileTarget.ToString();
			

			if ( Visible )
			{
				
				Page.RegisterClientScriptBlock( "openBrowser", @"
				<script language=""javascript"">
				function openBrowser()
				{
					var wrkFld = document.forms[0].elements['" + AbsFolderTxt.ClientID+ @"'].value;
					if ( wrkFld == """" || wrkFld == null )
					{
						wrkFld = ""/"";
					}
					openPopupWindow( '" + ActuUtil.jsEncode( link ) + @"&folder='+ encode(wrkFld),'',500,350,'status, scrollbars, resizable' );  
				}
				</script>
				");
			}

			if ( m_jobType != "scheduled"  || !AcFunctionalityManager.HasSubFeature(Page , "SelfNotificationWithAttachment" ))
			{
				AcRowNotification.Visible = false;

			}
			else
			{
				AcRowNotification.Visible = true;
			}

			if ( !IsPostBack )
			{
				if ( m_jobType == "scheduled" )
				{
					AcLstFileType.Visible = true;
					AcLblFileType.Visible = true;
				}
				else
				{
					AcLstFileType.Visible = false;
					AcLblFileType.Visible = false;
				}
			}
			AcChkEmail.Attributes.Add( "OnClick", "javascript:EmailNotificationClicked();" );


			
			// Populate the hidden fields 

			if ( AbsFolderTxt.Text.Trim() != String.Empty )
			{
				HiddenAbsFolderTxt.Value = AbsFolderTxt.Text;
			}

			if ( DocumentNameTxt.Text.Trim() != String.Empty )
			{
				HiddenDocumentNameTxt.Value = DocumentNameTxt.Text;
			}

			if ( VersionNameTxt.Text.Trim() != String.Empty )
			{
				HiddenVersionNameTxt.Value = VersionNameTxt.Text;
			}


			if ( HiddenNewVersionRdBtn.Value.Trim() == String.Empty ) 
			{
				HiddenNewVersionRdBtn.Value = GetRdBtnCheckedStatus( NewVersionRdBtn );
			}

			if ( HiddenReplaceVersionRdBtn.Value.Trim() == String.Empty  )
			{
				HiddenReplaceVersionRdBtn.Value = GetRdBtnCheckedStatus( ReplaceVersionRdBtn );
			}

			if ( HiddenPersonalFolderRdBtn.Value.Trim() == String.Empty )
			{
				HiddenPersonalFolderRdBtn.Value = GetRdBtnCheckedStatus( PersonalFolderRdBtn );
			}
			if ( HiddenAbsoluteFolderRdBtn.Value.Trim() == String.Empty )
			{
				HiddenAbsoluteFolderRdBtn.Value  = GetRdBtnCheckedStatus ( AbsoluteFolderRdBtn );
			}

			if ( HiddenNumVersionChk.Value.Trim() == String.Empty )
			{
				HiddenNumVersionChk.Value = GetChkBoxCheckedStatus( NumVersionChk );
			}
			if ( HiddenNumVersionTxt.Value.Trim() == String.Empty )
			{
				HiddenNumVersionTxt.Value =  NumVersionTxt.Text ;
			}

			sDocumentConversionEnabled = ActuUtil.jsEncode( DocumentConversionEnabled.ToString() );
			sDocumentConversionEnabled = sDocumentConversionEnabled.ToLower();
			sEmailNotificationEnabled = ActuUtil.jsEncode( EmailNotificationEnabled.ToString().ToLower() );

		
		}


		private string GetRdBtnCheckedStatus( RadioButton ctl )
		{
			if ( ctl.Checked )
			{
				return "true";
			}
			else
			{
				return "false";
			}
		}

		private string GetChkBoxCheckedStatus( CheckBox ctl  )
		{

			if ( ctl.Checked )
			{
				return "true";
			}
			else
			{
				return "false";
			}
		}

		protected void PopulateDocumentFormat()
		{
			// Do not show the Document format for Sync jobs and for
			// executables which are not rox type

			if ( ! DocumentConversionEnabled )
			{
				DocumentFormatRow.Visible = false;
			}
			else
			{
				DocumentFormatRow.Visible = true;

				ArrayList DocumentFormats = new ArrayList () ;
				ListItem RoiItem = new ListItem();
				RoiItem.Value = "ROI";
				RoiItem.Text = AcResourceManager.GetString("output.document.format.ROI", Session );
				DocumentFormats.Add( RoiItem );

				ListItem PdfItem = new ListItem();
				PdfItem.Value = "PDF";
				PdfItem.Text = AcResourceManager.GetString( "output.document.format.PDF", Session );
				DocumentFormats.Add( PdfItem );

				ListItem ExcelDataItem = new ListItem();
				ExcelDataItem.Value = "ExcelData" ;
				ExcelDataItem.Text = AcResourceManager.GetString( "output.document.format.ExcelData", Session );
				DocumentFormats.Add( ExcelDataItem );

				ListItem ExcelDisplayItem = new ListItem();
				ExcelDisplayItem.Value = "ExcelDisplay";
				ExcelDisplayItem.Text = AcResourceManager.GetString( "output.document.format.ExcelDisplay", Session );
				DocumentFormats.Add( ExcelDisplayItem );

				ListItem RftItem = new ListItem();
				RftItem.Value = "RTF";
				RftItem.Text = AcResourceManager.GetString("output.document.format.RTF", Session );
				DocumentFormats.Add( RftItem );

				ListItem EditableRtfItem = new ListItem();
				EditableRtfItem.Value = "RTFFullyEditable";
				EditableRtfItem.Text = AcResourceManager.GetString("output.document.format.RTFFullyEditable", Session );
				DocumentFormats.Add( EditableRtfItem );

				AcLstDocumentFormat.DataSource = DocumentFormats;
				AcLstDocumentFormat.DataValueField = "Value";
				AcLstDocumentFormat.DataTextField = "Text";
				AcLstDocumentFormat.DataBind();

				AcLstDocumentFormat.SelectedIndex = 0;

			}

			if ( IsPostBack )
			{
				if ( HiddenSelectedFormat.Value != null && HiddenSelectedFormat.Value.Trim() != String.Empty )
				{
					int SelectedIndex = 0;

					try
					{
						SelectedIndex = Convert.ToInt32(HiddenSelectedFormat.Value);
					}
					catch
					{
					}

					AcLstDocumentFormat.SelectedIndex = SelectedIndex;
				}
			}

		}

		protected string GetDependentFileExtension()
		{
			string[] ResultDef =  { "Name" };
			string DependentFile = ActuUtil.GetDependentFileByName( m_execName, ResultDef, Page);
			if ( DependentFile != null )
			{
				return ActuUtil.getFileExtension( ActuUtil.stripVersionNumber( DependentFile ));
			}
			return null;
		}


		#endregion

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion
	}
}

