<%@ Import NameSpace="activeportal.classes" %>
<%@ Register TagPrefix="template" namespace="actuate.classes.webcontrols.masterpages" assembly="activeportal" %>
<%@ Register TagPrefix="actuate" TagName="TableParameterEditor" Src="../common/parameters.ascx" %>
<%@ Page language="c#" Inherits="DistributionPortal.newrequest.TableParameterEditor" CodeFile="TableParameterEditor.aspx.cs" %>
<template:contentcontainer templatefile="popuptemplate.ascx" runat="server" id="Contentcontainer1">
	<template:content id="head" runat="server">
		<SCRIPT language=javascript 
src="<%=Context.Request.ApplicationPath%>/js/query.js"></SCRIPT>
	</template:content>
	<template:content id="title" runat="server">
		<%=JobUtil.GetTableEditorTitle( Page )%>
	</template:content>
	<template:content id="content" runat="server">
		<actuate:TableParameterEditor id="Tableparametereditor1" RunAt="Server" TableParameterEditor="true"></actuate:TableParameterEditor>
	</template:content>
</template:contentcontainer>
