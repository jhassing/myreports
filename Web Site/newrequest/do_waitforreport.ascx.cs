namespace activeportal.usercontrols.newrequest
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes.api;
	using activeportal.classes;
	using activeportal.classes.api.proxy;
	using System.Text;
	using actuate_i18n;
	using actuate_i18n.classes;


	/// <summary>
	///		Summary description for do_waitforreport.
	/// </summary>
	public partial  class do_waitforreport : System.Web.UI.UserControl
	{
	
		protected HtmlInputHidden AcInpConnectionHandle;
		protected HtmlInputHidden AcInpId;
		protected HtmlInputHidden AcInpOutputName;
		protected HtmlInputHidden AcInpSaveOutput;
		protected HtmlInputHidden AcInpOutputFileType;
		protected string redirect;
		protected string error;
		protected bool submitForm = true;
		protected bool succeeded = true;

		protected override void OnPreRender(System.EventArgs e)
		{
			bool forcePostBack = AcParams.urlCheckboxState( Page, AcParams.Name.forcePostBack, false, AcParams.From.Url );

			if ( IsPostBack || forcePostBack )
			{
				// used by javascript
				submitForm = false;
				// Wait for report
				string id = AcParams.Get( Page, AcParams.Name.id, AcParams.From.Url );

				AcWaitForExecuteReport waitReport = new AcWaitForExecuteReport( id );
				waitReport.Execute( Page );
	
				string status = waitReport.Response.Status;

				redirect = AcParams.Get( Page, AcParams.Name.redirect, AcParams.From.Url );

				if ( redirect == null )
				{
					throw new AcException( "ERRMSG_MISSING_REQUIRED_PARA", new AcKeyArgument[] { new AcKeyArgument( null, "redirect" ) }, Session, false );
				}
				if ( status != null )
				{
					switch ( AcString.ToLower(status))
					{
						case "firstpage":
                            goto case "done";
						
						case "done":
							// the javascript takes care of redirection.
							break;

						case "failed":
							succeeded = false;
							AcUrlBuilder errorUrl = new AcUrlBuilder( "../errors/error.aspx" );
							errorUrl.AddKeyValue( "errormsg", AcResourceManager.GetString( "MSG_JOB_FAILED", Session ) );
							string closex = AcParams.Get( Page, AcParams.Name.closex, AcParams.From.Url );
							if ( closex != null )
							{
								errorUrl.AddKeyValue( "closex", closex );
							}
							error = errorUrl.ToString( );
							break;
					}
				}
			}
		}
		
		protected void OnCancelButton(object sender, System.EventArgs e)
		{
			// Cancel and close. Transfer to cancel page
			string id = AcParams.Get( Page, AcParams.Name.id, AcParams.From.Url );
			string connectionHandle = AcParams.Get( Page, AcParams.Name.connectionhandle, AcParams.From.Url );

			Server.Transfer( "cancelreport.aspx?fromWhere=wait&id=" + id + "&connectionhandle=" + HttpUtility.UrlEncode( connectionHandle ) );
		}
		
		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{

		}
		#endregion
	}
}
