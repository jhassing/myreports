<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<%@ Register TagPrefix="template" namespace="actuate.classes.webcontrols.masterpages" assembly="activeportal" %>
<%@ Register TagPrefix="actuate" TagName="do_submitjob" Src="do_submitjob.ascx" %>
<%@ Page language="c#" Inherits="DistributionPortal.newrequest.do_submitjob" CodeFile="do_submitjob.aspx.cs" %>
<template:contentcontainer runat="server" id="Contentcontainer1">
	<template:content id="title" runat="server">
		<localized:AcLiteral id="AcLiteral1" runat="server" Key="MSGT_BROWSER">
			<ARGUMENT key="JOBS" />
		</localized:AcLiteral>
	</template:content>
	<template:content id="content" runat="server">
		<actuate:do_submitjob id="Do_submitjob1" runat="server"></actuate:do_submitjob>
	</template:content>
</template:contentcontainer>
