<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Page language="c#" AutoEventWireup="false" %>
<%@ Register TagPrefix="actuate" TagName="authenticate" Src="../authenticate.ascx" %>
<%@ Register TagPrefix="actuate" TagName="do_cancel" Src="do_cancel.ascx" %>

<actuate:authenticate id="AcAuthenticate" runat="server"/>
<actuate:do_cancel runat="server" />