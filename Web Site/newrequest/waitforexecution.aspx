<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<%@ Register TagPrefix="template" namespace="actuate.classes.webcontrols.masterpages" assembly="activeportal" %>
<%@ Register TagPrefix="actuate" TagName="do_waitforreport" Src="do_waitforreport.ascx" %>
<%@ Page language="c#" enableSessionState="ReadOnly" Inherits="DistributionPortal.newrequest.waitforexecution" CodeFile="waitforexecution.aspx.cs" %>
<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<template:contentcontainer templatefile="popuptemplate.ascx" runat="server" id="Contentcontainer1">
	<template:content id="title" runat="server">
		<localized:AcLiteral id="AcLiteral1" runat="server" Key="MSGT_BROWSER">
			<ARGUMENT key="newrequest.wait.title" />
		</localized:AcLiteral>
	</template:content>
	<template:content id="content" runat="server">
		<actuate:do_waitforreport id="Do_waitforreport1" runat="server"></actuate:do_waitforreport>
	</template:content>
</template:contentcontainer>
