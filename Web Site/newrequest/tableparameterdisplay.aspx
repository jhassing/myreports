<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<%@ Register TagPrefix="template" namespace="actuate.classes.webcontrols.masterpages" assembly="activeportal" %>
<%@ Register TagPrefix="actuate" TagName="TableParameterDisplay" Src="TableParameterDisplay.ascx" %>
<%@ Page language="c#" Inherits="DistributionPortal.newrequest.TableParameterDisplay" CodeFile="TableParameterDisplay.aspx.cs" %>
<template:contentcontainer templatefile="popuptemplate.ascx" runat="server" id="Contentcontainer1">
	<template:content id="title" runat="server">
		<localized:AcLiteral id="AcLiteral1" runat="server" Key="parameters.tables.title"></localized:AcLiteral>
	</template:content>
	<template:content id="content" runat="server">
		<actuate:TableParameterDisplay id="TableParameterDisplay1" RunAt="Server"></actuate:TableParameterDisplay>
	</template:content>
</template:contentcontainer>
