namespace activeportal.usercontrols.newrequest
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes;
	using actuate_i18n;
	using System.Text;
	

	/// <summary>
	///		Summary description for submittedjob_status1.
	/// </summary>
	public partial  class submittedjob_status1 : System.Web.UI.UserControl
	{

		

		private string m_jobAction;
	
		private string m_jobId;
	
		private string m_jobName;

		private bool m_showDelete = true;

		public bool ShowDelete
		{
			set { m_showDelete = value;}
			get { return m_showDelete ; }
		}

		private bool m_showStatus = true;

		public bool ShowStatus
		{
			set { m_showStatus = value;}
			get { return m_showStatus ; }
		}

		private string m_cancelJobTarget =  null;
		public string CancelJobTarget
		{
			set { m_cancelJobTarget = value; }
			get { return m_cancelJobTarget ; }
		}

		private string m_deleteJobTarget =  null;
		public string DeleteJobTarget
		{
			set { m_deleteJobTarget = value; }
			get { return m_deleteJobTarget ; }
		}

		private string m_jobDetailTarget =  null;
		public string JobDetailTarget
		{
			set { m_jobDetailTarget = value; }
			get { return m_jobDetailTarget ; }
		}	
	
		protected void Page_Load(object sender, System.EventArgs e)
		{
			
			ViewJobDetailBtn.Visible = m_showStatus;
			CancelOrDeleteBtn.Visible = m_showDelete;

			// Get the parameters

			string sFQExecName = AcParams.Get( Page, AcParams.Name.__executableName );
			m_jobId = AcParams.Get( Page, AcParams.Name.jobID );
			m_jobName = AcParams.Get( Page, AcParams.Name.__jobname );
			m_jobAction = AcParams.Get( Page, AcParams.Name.jobAction );
			string sServerURL = AcParams.Get( Page, AcParams.Name.serverURL );


			if ( m_jobAction == Constants.CancelJob )
			{
				CancelOrDeleteBtn.Key = "BTN_CANCEL_JOB";
			}
			else if (m_jobAction == Constants.DeleteJob )
			{
				CancelOrDeleteBtn.Key = "BTN_DELETE_JOB";
			}



			// Job Status message

			string[] saJobStatusMsg = { "<font class=\"fntNewRequestStatusHighlight\">" + HttpUtility.HtmlEncode(m_jobName) + "</font> ",
										 "<font class=\"fntNewRequestStatusHighlight\">" + HttpUtility.HtmlEncode(sFQExecName) + "</font>",
										 "<font class=\"fntNewRequestStatusHighlight\">" + sServerURL + "</font>",
										 "<font class=\"fntNewRequestStatusHighlight\">" + m_jobId + "</font>" };
			string sStatus = String.Format( ActuUtil.htmlEncode( AcResourceManager.GetString("STATUS_JOB_SUBMISSION", Session)),saJobStatusMsg );

			JobStatusMsgLbl.Text = sStatus;

			// Initialize the Context with the corret request type for the navigator control

			string requesttype = AcParams.Get( Page, AcParams.Name.__requesttype, AcParams.From.Url );
			Context.Items[ AcParams.Name.__requesttype.ToString() ] = requesttype;

		}

		public void CancelOrDeleteBtn_Cilcked( Object sender, System.EventArgs e )
		{
			switch ( m_jobAction )
			{
				case  Constants.CancelJob :
				{
					AcUrlBuilder url = new AcUrlBuilder( m_cancelJobTarget );
					url.AddKeyValue( "JobId", m_jobId );
					url.AddKeyValue( "jobname", m_jobName );
					url.AddKeyValue( "operation", m_jobAction );
					Response.Redirect( url.ToString( ) );
				}
					break;
				case Constants.DeleteJob :
				{
					AcUrlBuilder url = new AcUrlBuilder( m_deleteJobTarget );
					url.AddKeyValue( "JobId", m_jobId);
					url.AddKeyValue( "jobname", m_jobName );
					url.AddKeyValue( "operation", m_jobAction );
					Response.Redirect( url.ToString( ) );
				}
					break;
			}
			
		}

		public void ViewJobDetailBtn_Cilcked( Object sender, System.EventArgs e )
		{
			
				AcUrlBuilder url = new AcUrlBuilder( m_jobDetailTarget );
				url.AddKeyValue( "JobId", m_jobId );
				url.AddKeyValue( "name", m_jobName );
				Response.Redirect( url.ToString( ) );
			
		}



		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
