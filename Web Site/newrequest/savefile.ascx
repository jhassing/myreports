<%@ Control Language="c#" Inherits="activeportal.usercontrols.newrequest.SaveFile" CodeFile="savefile.ascx.cs" %>
<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<%@ Register TagPrefix="acwc" Namespace="actuate.classes.webcontrols" Assembly="activeportal"%>
<Table width="100%" height="100%" cellspacing="0" CellPadding="3" id="Table1" runat="server">
	<tr>
		<td valign="Top">
			<table ID="SaveToFileTable" width="100%" CellSpacing="3" CellPadding="3" Runat="server">
				<%-- Save Document on Server Checkbox --%>
				<tr>
					<td colspan="3">
						<asp:CheckBox ID="SaveToFileChkBox" onclick="updateValues()" CssClass="boldlabel" Runat="server"></asp:CheckBox>
					</td>
				</tr>
				<%-- Headline: FIELD (For scheduled Jobs --%>
				<tr ID="HeadLineRow" Visible="false" RunAt="server">
					<td width="30%">
					    <localized:AcLabelControl Key="MSGT_HEADLINE" CssClass="label" Width="35Ex" ID="HeadlineLbl" RunAt="Server"/>
					</td>
					<td colspan="2" width="70%" RunAt="server">
					    <asp:TextBox ID="HeadLineTxt" Width="35Ex" RunAt="server"></asp:TextBox>
					</td>
				</tr>
				<%-- Space of a line --%>
				<%--
				<tr>
					<td colspan="3">
						<br>
					</td>
				</tr> --%>
				<%-- Output Location label --%>
				<tr>
					<td colspan="3" class="label">
					  <localized:AcLabelControl CssClass="label" Key="MSGT_OUTPUT_LOCATION" RunAt="server"/>
					</td>
				</tr>
				<%-- Personal Folder --%>
				<tr>
					<td colspan="3">
						<localized:AcRadioButton Key="CB_PERSONAL_FOLDER" ID="PersonalFolderRdBtn" CssClass="label"  AutoPostBack="False" GroupName="OuputFolderType" checked="true" Runat="server"/>	
					</td>
				</tr>
				<%-- Other Folder --%>
				<tr>
					<td colspan="1" Width="30%">
						<localized:AcRadioButton Key="CB_ABSOLUTE_FOLDER" ID="AbsoluteFolderRdBtn" AutoPostBack="False" CssClass="label" GroupName="OuputFolderType"   Runat="server"/>
					</td>
					<td colspan="2" Width="70%" nowrap Runat="server">
						<asp:TextBox ID="AbsFolderTxt" Runat="server"></asp:TextBox>
						<localized:AcHtmlInputButton ID="BrowseFolderBtn" Key="BTN_BROWSE_FOLDER" Runat="server"/>
					</td>
				</tr>
				<%-- Space of One line --%>
				<tr>
					<td colspan="3">
						<br>
					</td>
				</tr>
				<tr>
					<td>
						<acwc:acsafelabel CssClass="label" ID="DocumentNameLbl" Runat="server"></acwc:acsafelabel>
					</td>
					<td colspan="2" nowrap>
						<asp:TextBox ID="DocumentNameTxt" onchange="SaveDocumentName()" Runat="server"/>
						<span class="label">&nbsp;*&nbsp;&nbsp;</span>
						<localized:AcLabelControl id="AcLblFileType" CssClass="label" Key="MSGT_FILE_TYPE" runat="server"/>
						<asp:DropDownList id="AcLstFileType"  runat="server"/>
					</td>
				</tr>
				<%-- Document format --%>
				<tr id="DocumentFormatRow" runat="server">
					<td>
						<localized:AcLabelControl CssClass="label" ID="DocumentFormatLbl" Key="output.document.label.format" Runat="server"/>
					</td>
					
					<td colspan="2" nowrap>
				        <asp:DropDownList id="AcLstDocumentFormat" onchange="UpdateEmailFormat()"  runat="server" />
					</td>
				</tr>
				<%-- Version Name --%>
				<tr>
					<td>
					   <acwc:acsafelabel CssClass="label" ID="VersionNameLbl" RunAt="server"/>
					</td>
					<td colspan="2" Runat="server">
						<asp:TextBox ID="VersionNameTxt" Runat="server"></asp:TextBox>
					</td>
				</tr>
				<%-- Space of a line --%>
				<tr>
					<td colspan="3">
						<br>
					</td>
				</tr>
				<%-- File Exists --%>
				<tr>
				  <td>
					<acwc:acsafelabel CssClass="label" ID="FileExistsLbl" RunAt="Server"/>
				  </td>
				</tr>
				<tr>
					<td colspan="3">
						<localized:AcRadioButton key="CB_CREATE_NEW" ID="NewVersionRdBtn" Runat="server" CssClass="label" AutoPostBack="False" GroupName="Version" Checked="True"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" Width="40%">
						<localized:AcRadioButton key="CB_REPLACE_LATEST" ID="ReplaceVersionRdBtn" CssClass="label" AutoPostBack="False" GroupName="Version" Runat="server"/>
					</td>
					<td Width="60%">
						<asp:CheckBox ID="NumVersionChk" CssClass="label" Runat="server"></asp:CheckBox>
						<acwc:acsafelabel  ID="VersionLbl" CssClass="label" Runat="server"></acwc:acsafelabel>
						&nbsp;<asp:TextBox ID="NumVersionTxt"  Runat="server" Width="6Em" MaxLength="10"></asp:TextBox>&nbsp;
						<acwc:acsafelabel ID="NumVersionLbl" CssClass="label" Runat="server"></acwc:acsafelabel>
					</td>
				</tr>
				<tr>
					<td colspan="3"><br></td>
				</tr>
				<tr id="AcRowNotification" nowrap runat="server">
				   <td width="40%" colspan="2" class="label">
						&nbsp;&nbsp;
				       <localized:AcLabelControl id="AcLblNotification" Key="output.notification.label.notification" runat="server"/>
				   </td>
				   <td width="60%" nowrap class="label">
						<asp:CheckBox id="AcChkEmail" runat="server"/>
				        <asp:DropDownList id="AcLstAttachment" runat="server" />
				   </td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<%-- Hidden Field to store the Output Name --%>
<input type="hidden" id="OutputNameTxt" runat="server"/>
<input type="hidden" id="HiddenAbsFolderTxt" runat="server" />
<input type="hidden" id="HiddenDocumentNameTxt" runat="server" />
<input type="hidden" id="HiddenVersionNameTxt" runat="server" />
<input type="hidden" id="HiddenNewVersionRdBtn" runat="server" />
<input type="hidden" id="HiddenReplaceVersionRdBtn" runat="server" />
<input type="hidden" id="HiddenPersonalFolderRdBtn" runat="server" />
<input type="hidden" id="HiddenAbsoluteFolderRdBtn" runat="server" />
<input type="hidden" id="HiddenNumVersionChk" runat="server" />
<input type="hidden" id="HiddenNumVersionTxt" runat="server" />
<input type="hidden" id="HiddenSelectedFormat" runat="server" />


<script language="javascript">

var SaveToFileChkBox = document.getElementById('<%=SaveToFileChkBox.ClientID%>');
var PersonalFolderRdBtn = document.getElementById('<%=PersonalFolderRdBtn.ClientID%>');
var HiddenPersonalFolderRdBtn = document.getElementById( '<%=HiddenPersonalFolderRdBtn.ClientID%>');

var AbsoluteFolderRdBtn = document.getElementById('<%=AbsoluteFolderRdBtn.ClientID%>');
var HiddenAbsoluteFolderRdBtn = document.getElementById('<%=HiddenAbsoluteFolderRdBtn.ClientID%>');

var AbsFolderTxt = document.getElementById('<%=AbsFolderTxt.ClientID%>');
var HiddenAbsFolderTxt = document.getElementById('<%=HiddenAbsFolderTxt.ClientID%>');


var BrowseFolderBtn = document.getElementById('<%=BrowseFolderBtn.ClientID%>');
var DocumentNameTxt = document.getElementById('<%=DocumentNameTxt.ClientID%>');
var HiddenDocumentNameTxt  = document.getElementById('<%=HiddenDocumentNameTxt.ClientID%>');

var VersionNameTxt = document.getElementById('<%=VersionNameTxt.ClientID%>');
var HiddenVersionNameTxt = document.getElementById('<%=HiddenVersionNameTxt.ClientID%>');


var NewVersionRdBtn = document.getElementById('<%=NewVersionRdBtn.ClientID%>');
var HiddenNewVersionRdBtn  = document.getElementById('<%=HiddenNewVersionRdBtn.ClientID%>');


var ReplaceVersionRdBtn = document.getElementById('<%=ReplaceVersionRdBtn.ClientID%>');
var HiddenReplaceVersionRdBtn = document.getElementById('<%=HiddenReplaceVersionRdBtn.ClientID%>');

var NumVersionChk = document.getElementById('<%=NumVersionChk.ClientID%>');
var HiddenNumVersionChk = document.getElementById('<%=HiddenNumVersionChk.ClientID%>');

var NumVersionTxt = document.getElementById('<%=NumVersionTxt.ClientID%>');
var HiddenNumVersionTxt = document.getElementById('<%=HiddenNumVersionTxt.ClientID%>');

var OutputNameTxt = document.getElementById('<%=OutputNameTxt.ClientID%>');
var SaveToFileChecked = false;

var AllNotificationFormats = null;
var HiddenSelectedFormat = document.getElementById('<%=HiddenSelectedFormat.ClientID%>');


function disableAll( )
{


	
	if ( SaveToFileChkBox != undefined || SaveToFileChkBox != null || '<%=m_jobType%>'.toLowerCase() == "scheduled" )
	{
		
		if ( SaveToFileChkBox != undefined || SaveToFileChkBox != null )
		{
			SaveToFileChecked = SaveToFileChkBox.checked;
		}
		else
		{
			SaveToFileChecked = '<%=m_jobType%>'.toLowerCase() == "scheduled";
		}
		
		PersonalFolderRdBtn.disabled = !SaveToFileChecked ;
		AbsoluteFolderRdBtn.disabled = !SaveToFileChecked ;
		AbsFolderTxt.disabled = !SaveToFileChecked ;
		BrowseFolderBtn.disabled = !SaveToFileChecked ;
		DocumentNameTxt.disabled = !SaveToFileChecked ;
		VersionNameTxt.disabled = !SaveToFileChecked ;
		NewVersionRdBtn.disabled = !SaveToFileChecked ;
		ReplaceVersionRdBtn.disabled = !SaveToFileChecked ;
		NumVersionChk.disabled = !SaveToFileChecked ;
		NumVersionTxt.disabled = !SaveToFileChecked ;
		if ( !DocumentNameTxt.disabled && DocumentNameTxt.value == "" )
		{
			DocumentNameTxt.value = '<%=activeportal.classes.ActuUtil.jsEncode(GetDefaultOutputName())%>';
		}
		
		
	}
	if ( OutputNameTxt != undefined || OutputNameTxt != null )
	{
		OutputNameTxt.value = DocumentNameTxt.value;
	}
	

	
}
		
	

function ReplaceVersionRdBtnClicked( Disable)
{

	NumVersionChk.disabled = Disable;
	
	NumVersionTxt.disabled  = NumVersionTxt.disabled || (!NumVersionChk.checked);
	
	HiddenReplaceVersionRdBtn.value = GetCheckedStatus( ReplaceVersionRdBtn );
	
	
		
}

function NumVersionChkClicked( )
{
	NumVersionTxt.disabled = !NumVersionTxt.disabled;
	
	HiddenNumVersionChk.value = GetCheckedStatus( NumVersionChk );
	
}

// Personal Folder Radio Button has been clicked
function PersonalFolderRdBtnClicked( )
{
	AbsFolderTxt.disabled = true;
	if ( !AbsoluteFolderRdBtn.disabled )
	{
		AbsFolderTxt.value = "";
	}
	BrowseFolderBtn.disabled = true;
	
	HiddenPersonalFolderRdBtn.value = GetCheckedStatus( PersonalFolderRdBtn );
}

// Absolute Folder Radio Button has been clicked
function AbsoluteFolderRdBtnClicked( DefOutputFolder )
{
	AbsFolderTxt.disabled = false;
	BrowseFolderBtn.disabled = false;

	// If there was no value in the Absolute Folder  field, then populate it
	// with the Default Output Folder
	if ( AbsFolderTxt.value == "" )
	{
		AbsFolderTxt.value = DefOutputFolder;
	}
	
	HiddenAbsoluteFolderRdBtn.value = GetCheckedStatus( AbsoluteFolderRdBtn );
	
}
function EmailNotificationClicked()
{

    if ( '<%=m_jobType%>'.toLowerCase() != "scheduled" )
       return;
       
	var EmailNotification = document.forms[0].elements[ '<%=AcChkEmail.ClientID%>' ];
	var AttachmentFormat = document.forms[0].elements[ '<%=AcLstAttachment.ClientID%>' ];
	
	if ( EmailNotification == null || EmailNotification == undefined ||
	     AttachmentFormat == null || AttachmentFormat == undefined )
	{
	    return;
	}
	
	if ( EmailNotification != undefined )
	{
	
		if ( EmailNotification.checked )
		{
			AttachmentFormat.disabled = false;
		}
		else
		{
			AttachmentFormat.disabled = true;
		}
	}
	
}

function updateValues()
{

	disableAll( );

	// Update javascript checked values when reloading the page from cache.

	if ( SaveToFileChkBox == undefined || SaveToFileChkBox.checked )
	{

	
		if ( ReplaceVersionRdBtn && NewVersionRdBtn )
		{

			// if both buttons disabled after postback, checked value is lost: recheck the default one
			if ( !ReplaceVersionRdBtn.checked && !NewVersionRdBtn.checked )
			{
				NewVersionRdBtn.checked = true;
			}
			
			if ( ReplaceVersionRdBtn.checked )
			{
				ReplaceVersionRdBtnClicked(true);
			}
			else if ( NewVersionRdBtn.checked && !ReplaceVersionRdBtn.disabled)
			{
				ReplaceVersionRdBtnClicked( false );
			}
		
		}

		
		if ( AbsoluteFolderRdBtn != undefined )
		{
			// if both buttons disabled after postback, checked value is lost:recheck the default one
			if ( !AbsoluteFolderRdBtn.checked && !PersonalFolderRdBtn.checked )
			{
				PersonalFolderExists = '<%=m_homeFolderExists.ToString().ToLower() == "true"%>';
				if ( PersonalFolderExists.toLowerCase() == "true"  )
				{
					PersonalFolderRdBtn.checked = true;
				}
				else
				{
					PersonalFolderRdBtn.checked = false;
				}
				AbsoluteFolderRdBtn.checked = !PersonalFolderRdBtn.checked;

			}

			if ( AbsoluteFolderRdBtn.checked && !AbsoluteFolderRdBtn.disabled)
			{
				AbsoluteFolderRdBtnClicked('<%=activeportal.classes.ActuUtil.jsEncode( m_defOutputFolder )%>');
			}
			else
			{
				PersonalFolderRdBtnClicked( );
			}
		}
	}
	EmailNotificationClicked();

	if ( SaveToFileChecked )
	{
		StoreToHiddenFields();	
	}
	
	if ( ! SaveToFileChecked )
	{
		PopulateFromHiddenFields();
	}
	
}


function StoreToHiddenFields()
{

	
	//if (  SaveToFileChecked )
	//{

		HiddenAbsFolderTxt.value = AbsFolderTxt.value;
		HiddenDocumentNameTxt.value = DocumentNameTxt.value;
		HiddenVersionNameTxt.value = VersionNameTxt.value;
		
		
		HiddenNewVersionRdBtn.value = GetCheckedStatus( NewVersionRdBtn );
		HiddenReplaceVersionRdBtn.value = GetCheckedStatus( ReplaceVersionRdBtn );
		HiddenPersonalFolderRdBtn.value = GetCheckedStatus( PersonalFolderRdBtn );
		HiddenAbsoluteFolderRdBtn.value = GetCheckedStatus( AbsoluteFolderRdBtn );
		HiddenNumVersionChk.value = GetCheckedStatus( NumVersionChk );
		HiddenNumVersionTxt.value = NumVersionTxt.value;

	//}
		
}
function  PopulateFromHiddenFields()
{

	
	//if ( ! SaveToFileChecked )
	//{

		AbsFolderTxt.value = HiddenAbsFolderTxt.value ;
		DocumentNameTxt.value = HiddenDocumentNameTxt.value;
		VersionNameTxt.value = HiddenVersionNameTxt.value;
		NewVersionRdBtn.checked = SetChecked ( HiddenNewVersionRdBtn );
		ReplaceVersionRdBtn.checked = SetChecked ( HiddenReplaceVersionRdBtn );
		PersonalFolderRdBtn.checked = SetChecked ( HiddenPersonalFolderRdBtn );
		AbsoluteFolderRdBtn.checked = SetChecked( HiddenAbsoluteFolderRdBtn );
		NumVersionChk.checked = SetChecked( HiddenNumVersionChk );
		NumVersionTxt.value = 		HiddenNumVersionTxt.value ;
		
	//}
		
}

function GetCheckedStatus( control )
{
	if ( control == null || control == undefined )
	{
		return "false";
	}
	
	if ( control.checked )
	{
		return "true";
	}
	else
	{
		return "false";
	}
}

function SetChecked( StatusString )
{
	if ( StatusString == null || StatusString == '' )
	{
		return false;
	}

	if ( StatusString.value == "true" ) 
	{
		return true;
	}
	else
	{
		return false; 
	}
}

function UpdateEmailFormat()
{
	if ( '<%=sDocumentConversionEnabled%>' == "false" )
	{
		return;
	}
	
	if ( '<%=sEmailNotificationEnabled%>' == "false" )
	{
		return;
	}
		
	// make sure that if the Document format is not ROI then 
	// email notification format is updated accordingly
	
	
	var AcLstDocumentFormat = document.getElementById('<%=AcLstDocumentFormat.ClientID%>');
	var AttachmentFormat = document.getElementById('<%=AcLstAttachment.ClientID%>');
		
	if (AllNotificationFormats == null )
	{
		AllNotificationFormats = new Array(AttachmentFormat.options.length);
		for ( j=0; j < AttachmentFormat.options.length; j++ )
		{
				AllNotificationFormats[j] = AttachmentFormat.options[j];
		}
	}
			
	for (j=0; j < AcLstDocumentFormat.length; j++ )
	{
	 if (  AcLstDocumentFormat.options[j].selected )
	 {
		// Store the selected index in the hidden field
		// this will be used to repopulate the list during a post back
		
		HiddenSelectedFormat.value = j;
		
		OutputFormat = AcLstDocumentFormat.options[j].value;
				
		if ( OutputFormat == "ROI" || OutputFormat == "roi" )
		{
			if ( AttachmentFormat.options.length != AllNotificationFormats.length )
			{
				if ( AllNotificationFormats != null )
				{
					for ( j=0; j < AllNotificationFormats.length; j++ )
					{
						AttachmentFormat.options[j] = AllNotificationFormats[j];
					}	
				}
			} 	
		
			break;		
		}
		else
		{
			// For output format which are not ROI , the only supported email formats are
			// Attachments and Attachment in corresponding Format
			
			orglength = AttachmentFormat.length;
			NewOptions = new Array(2);
			NewOptions[0] = new Option();
			NewOptions[0].text = "<%=m_noAttachmentText%>";
			NewOptions[0].value= "no";
			
			
			
			// The email notification uses rtdtextbox, in the List control as the value
			// for a Fully Editable Rich Text Format , while the Document Format uses RTFFullyEditable
			// They Actually mean the same. But to avoid changes to existing email notification
			// the following code
			
			if ( trim(OutputFormat) == "RTFFullyEditable" )
			{
				OutputFormat = "rtftextbox" ;
			}

			for (j=0; j < AllNotificationFormats.length; j++ )
			{
				if ( trim(AllNotificationFormats[j].value.toLowerCase()) == trim(OutputFormat.toLowerCase()))
				{
					NewOptions[1]  = AllNotificationFormats[j];
					break;
				}
			}
					
			orglength = AttachmentFormat.length;
			for ( j=0; j < orglength; j++ )
			{
				AttachmentFormat.options[0] = null;
			}
			
					
			for ( j=0; j < 2; j++ )
			{
				AttachmentFormat.options[j] = new Option( NewOptions[j].text, NewOptions[j].value, false, false );
			}
					
			break;
		}

	}
  }
}

function SaveDocumentName()
{
	if ( OutputNameTxt != null && OutputNameTxt != undefined )
	{
		OutputNameTxt.value = DocumentNameTxt.value;
	}
}

window.onload=updateValues

UpdateEmailFormat();
</script>
<asp:Literal ID="AcJavaScriptLtrl" RunAt="Server"/>
