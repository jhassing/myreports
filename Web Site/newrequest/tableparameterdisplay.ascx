<%@ Control Language="c#" Inherits="activeportal.newrequest.TableParameterDisplay1" CodeFile="TableParameterDisplay.ascx.cs" %>
<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<table id="TableParamDisplay" cellpadding="4" cellspacing="0" border="0" width="100%" runat="server">
 <tbody>
   <tr>
       <th valign="top" class="tableParameterHeader">
		<asp:Label ID="AcTableParamHeading" RunAt="Server" ForeColor="#ffffff" RunAt="Server"/>
	  </th>
   </tr>
   
   <tr>
      <td valign="top" class="tableParameterButtonPanel" nowrap>
		<localized:AcButtonControl Id="AcInsertBtnTop" Key="parameters.tables.button.insert" AddSpaces="false" RunAt="Server"/>
		&nbsp;
		<localized:AcButtonControl Id="AcOkBtnTop"  Key="BTN_OK" OnClick="OkButton_Clicked" AddSpaces="false" RunAt="Server"/>
		&nbsp;
		<localized:AcButtonControl Id="AcCancelBtnTop" Key="BTN_CANCEL" OnClick="CancelButton_Clicked" AddSpaces="false" RunAt="Server"/>
      </td>
   </tr>
   
   <tr>
     <td>
       <table ID="TableParamData" cellpadding="2" cellspacing="0" border="1" RunAt="server">
       </table>
     </td>
   </tr>
   
   <tr>
      <td valign="top" class="tableParameterButtonPanel"  nowrap>
		<localized:AcButtonControl Id="AcInsertBtnBtm" Key="parameters.tables.button.insert" AddSpaces="false" RunAt="Server"/>
		&nbsp;
		<localized:AcButtonControl Id="AcOkBtnBtm"  Key="BTN_OK" AddSpaces="false" OnClick="OkButton_Clicked" RunAt="Server"/>
		&nbsp;
		<localized:AcButtonControl Id="AcCancelBtnBtm" Key="BTN_CANCEL" AddSpaces="false" OnClick="CancelButton_Clicked" RunAt="Server"/>
     </td>
   </tr>
  </tbody>
</table>
<asp:Literal ID="AcLtrlJScript" RunAt="Server"/>
