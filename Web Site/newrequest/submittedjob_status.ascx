<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.newrequest.submittedjob_status1" CodeFile="submittedjob_status.ascx.cs" %>
<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="5" cellpadding="5" class="filter">
				<tr>
					<td>
						<localized:AcButtonControl ID="ViewJobDetailBtn"  Key="BTN_JOB_STATUS" OnClick="ViewJobDetailBtn_Cilcked" Runat="server"/>
						&nbsp;
						<localized:AcButtonControl  ID="CancelOrDeleteBtn" OnClick="CancelOrDeleteBtn_Cilcked" RunAt="server"/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="5" cellpadding="5">
			<tr>
			
				<td>
				
					<%-- JobName --%>
					<asp:label ID="JobStatusMsgLbl" CssClass="label" Runat="server"/>
									
				</td>
				
			</tr>
			
		</td>
	</tr>
</table>