namespace activeportal.usercontrols.newrequest
{
	using System;
	using System.Collections;
	using System.ComponentModel;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.SessionState;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes;
	using activeportal.classes.api;
	using actuate_i18n;

	/// <summary>
	///		Summary description for do_cancel.
	/// </summary>
	public partial  class do_cancel : System.Web.UI.UserControl
	{

		protected int goBack = -2;

		protected void Page_Load(object sender, System.EventArgs e)
		{
			string fromWhere = AcParams.Get( Page, AcParams.Name.fromWhere, AcParams.From.Url | AcParams.From.Post );
			if ( fromWhere == "wait" )
			{
				goBack = -2;
			}

			string id = AcParams.Get( Page, AcParams.Name.id, AcParams.From.Url | AcParams.From.Post );
			AcCancelReport cancel = new AcCancelReport( id );
			cancel.Execute( Page );

			
			string message = null;
			
			// If persistent report, we will display a warning or error in an alert if any

			if ( AcParams.urlCheckboxState( Page, AcParams.Name.__saveOutput ) )
			{
				if ( cancel.Response != null && cancel.Response.Status != null )
				{
					

					switch ( AcString.ToLower(cancel.Response.Status ))
					{

						case "failed":
							message = AcResourceManager.GetString( "TXT_CANCEL_FAILED", Session ) ;
							break;
		
						case "inactive":
							message = AcResourceManager.GetString( "TXT_CANCEL_INACTIVE", Session ) ;
							break;

					}
				
				}
			
			}

			if ( message != null )
			{
				AcBody.Attributes.Add( "OnLoad", "alert('" + ActuUtil.jsEncode( message ) + "' );GoBack();" );
			}
			else
			{
				AcBody.Attributes.Add( "OnLoad", "GoBack();" );
			}
		
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
