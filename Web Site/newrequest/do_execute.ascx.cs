namespace activeportal.usercontrols.newrequest
{
	using System;
	using System.Data;
	using System.Text;
	using System.Drawing;
	using System.Web;
	using System.Web.UI;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using  activeportal.classes;
	using activeportal.classes.api.proxy;
	using activeportal.classes.api;
	using System.Collections;
	using actuate_i18n;
	using actuate_i18n.classes;
	
	


	/// <summary>
	///		Summary description for do_execute.
	/// </summary>
	public partial  class do_execute : System.Web.UI.UserControl
	{
		
		string sOutputFolderType ;
		bool IsQuery = false;
		string outputFileExtension;
		string sFQExecName ;
		string fileExtension;
		string sOutputVersionName;
		string sFQOutputName;
		
		string sWaitVal;
		bool bProgressive = true;
		
		string sVersionsToRetain = null;
		string sSaveOutput;
		bool bSaveOutput = false;
		
		bool bNewVersion = true;
		bool FolderSpecified = false;


		protected void Page_Load(object sender, System.EventArgs e)
		{
			// This page can be accessed directly, and the  parameters can be specified
			// in the URL or , the parameters can come from the index.aspx page, from where
			// the user has been directed to this page.

			// We check for parameters in the URL first, if present, else we 
			// check from the Context.Items collection where the parameters
			// Have been stored in the  newrequest/index.aspx page

			sFQExecName = GetParameter( AcParams.Name.__executableName, true);
			fileExtension = ActuUtil.getFileExtension( sFQExecName );

			outputFileExtension = JobUtil.GetDefaultOutputType( fileExtension, sFQExecName, Page );

			if ( outputFileExtension == "doi" )
			{
				IsQuery = true;
			}
			sOutputFolderType = GetParameter(AcParams.Name.__outputfoldertype, true);
			
			if ( sOutputFolderType == null || sOutputFolderType.Trim() == String.Empty )
			{
				sOutputFolderType = JobUtil.GetOutputFolderType( Page );
			}

			sSaveOutput = GetParameter(AcParams.Name.__saveOutput, true);
			

			if ( sSaveOutput == null )
			{
				sSaveOutput = "off";
				bSaveOutput = false;
			}
			else if ( AcString.ToLower(sSaveOutput) == "checked" ||
				AcString.ToLower(sSaveOutput) == "on" || AcString.ToLower(sSaveOutput) == "true" )
			{
				bSaveOutput = true;
			}

			sOutputVersionName = GetParameter( AcParams.Name.__versionName, true); // CAN BE NULL



			sFQOutputName = GetParameter( AcParams.Name.__outputName, true);

			if ( sFQOutputName != null && sFQOutputName.Trim() != String.Empty )
			{
				if ( sFQOutputName.StartsWith("/") )
				{
					FolderSpecified = true;
				}
			}


			// TODO why are these params Null

			if (sOutputFolderType == "personal" &&  
				sFQOutputName != null &&
				sFQOutputName.Trim().Length > 0 ) // PREFIX PERSONAL FOLDERNAME AT JOB SUBMISSION TIME
			{
				if ( ! FolderSpecified  )
				{
					string sFQON = AcParams.Get( Page, AcParams.Name.homeFolder );
					if (sFQON == null) sFQON = "/";
					if (!sFQON.EndsWith("/")) sFQON += '/';
					if (!sFQON.StartsWith("/")) sFQON = '/' + sFQON;
					sFQON += ActuUtil.stripFolderName(sFQOutputName, "/");
					sFQOutputName = sFQON;
				}
			}

			if (sFQExecName == null) // VALIDATION FOR EXISTENCE OF '__executableName'
			{
				//sError = "ERRMSG_MISSING_EXEC_NAME";
				sFQExecName = "UNSPECIFIED";
			}
			else if (sFQOutputName == null || sFQOutputName.Trim().Length == 0) // VALIDATION FOR EXISTENCE OF '__outputName' OR BUILD IT IF MISSING
			{
				sFQOutputName = ActuUtil.stripFileExtension(sFQExecName) + "." + outputFileExtension;

				if ( sOutputFolderType == Constants.FOLDER_PERSONAL )
				{

					string HomeFolder = AcParams.Get( Page, AcParams.Name.homeFolder );

					if (HomeFolder == null) HomeFolder = "/";
					if (!HomeFolder.EndsWith("/")) HomeFolder += '/';
					if (!HomeFolder.StartsWith("/")) HomeFolder = '/' + HomeFolder; 

					sFQOutputName = HomeFolder + sFQOutputName;
				}
			}


			

			// Progressive viewing only for dhtml format

			string format = GetParameter( AcParams.Name.__viewFormat, true );
			
			if ( format == null || AcString.ToLower(format) == "dhtml" )
			{

				bProgressive = true; // IF UNSPECIFIED, DEFAULT TO PROGRESSIVE VIEWING

				// ONLY APPLICABLE TO TRANSIENT REPORTS; NOT PERSISTENT ASYNC

				string sProgressive =  AcParams.Get( Page, AcParams.Name.__progressive);
				if ( sProgressive != null )
				{
					sProgressive = AcString.ToLower(sProgressive).Trim( );
					bProgressive = ! ( sProgressive == "false" );
				}
				else
				{
					sWaitVal =  AcParams.Get( Page, AcParams.Name.__wait );
					if ( sWaitVal != null )
					{
						sWaitVal = AcString.ToLower(sWaitVal).Trim( );
						bProgressive = ! ( sWaitVal == "wait" || sWaitVal == "true" );
					}
				}
			}
			else
			{
				bProgressive = false;
			}

			if ( bSaveOutput )
			{
		
				// IfExists = createN | replace | create

				string sIfExists = GetParameter( AcParams.Name.__ifexists, true);
				if (sIfExists != null)
				{
					sIfExists = AcString.ToLower(sIfExists).Trim();
					bNewVersion = sIfExists.StartsWith("create");
					if ( bNewVersion )
					{
						sVersionsToRetain =  sIfExists.Substring( 6 ) ;
					}
				}
		
				// Old reportcast parameters support

				else
				{
					string overwrite = GetParameter( AcParams.Name.__overwrite, true );

					// __overwrite = new | old | true | false

					if ( overwrite != null )
					{
						overwrite = AcString.ToLower(overwrite).Trim( );
						bNewVersion = ! ( overwrite == "old" || overwrite == "true" );
					}						
					
					if( bNewVersion	)
					{
						// __limit = limit|true|false

						string sLimit = GetParameter( AcParams.Name.__limit, true);
						if ( sLimit != null )
						{
							sLimit = AcString.ToLower(sLimit).Trim( );
							if ( sLimit == "true" || sLimit == "limit" )
							{
								// __limitNumber = number
								sVersionsToRetain = GetParameter( AcParams.Name.__limitNumber, true);
							}
						}
					}
				}
			}
			
			
			ArrayList paramValue = new ArrayList();

			if ( !IsQuery )
			{
				//Report Parameters
				 
				string sourceOfRequest = AcParams.Get( Page, AcParams.Name.__SourceOfRequest__, "", Context);
				if ( sourceOfRequest  == "Index.aspx" )
				{
					paramValue = ( ArrayList) ( Session["ReportParameterList"]);
				}
				else
				{
					paramValue = JobUtil.GetReportParametersFromRequestURL( this.Request, JobUtil.ExcludeParamList );
				}

				ExecuteReport( paramValue );
			}
			else
			{
				string sourceOfRequest = AcParams.Get( Page, AcParams.Name.__SourceOfRequest__, "", AcParams.From.Url);
				Query query = null;
				if ( sourceOfRequest == "Wizard" )
				{
					// Comes from the GUI. Uses the query in session

					query = (Query)AcParams.GetObjectFromSession( Page, AcParams.Name.executingQueryObject );
				}
				else
				{
					// Url access

					paramValue = JobUtil.GetReportParametersFromRequestURL( this.Request, JobUtil.ExcludeParamList );
					if ( paramValue.Count == 0 )
					{
						// use query name. Let query object set to null
					}
					else
					{
						// Get the query from the server
						
						AcGetQuery getQuery = new AcGetQuery( sFQExecName );
						getQuery.Execute( Page );
						query = getQuery.Response.Query;
						if ( query == null || query.AvailableColumnList == null)
						{
							// if it's empty display an error

							throw new AcException( "error.query.empty", new AcKeyArgument[] { new AcKeyArgument( null, sFQExecName ) }, Session, false );
						}

						// now replace the parameters values with the ones in the url.

						if ( query.ReportParameterList != null  )
						{

							foreach ( ParameterValue param in paramValue )
							{
								for ( int i = 0 ; i < query.ReportParameterList.Length ; i++ )
								{
									// Parameter names are case insensitive

									if ( String.Compare( query.ReportParameterList[ i ].Name, param.Name, true ) == 0 )
									{
										query.ReportParameterList[i].Value = param.Value.Trim( '"' );
									}
								}
							}
						}

						

					}
				}
				
	
				ExecuteQuery( query, sFQExecName );
			}
		
		}

		private string GetParameter( AcParams.Name paramName, bool searchInContext )
		{
			string sParamValue = AcParams.Get( Page, paramName );

			if ( sParamValue == null && searchInContext)
			{
				// If the Parameter is not found in the URL, then check in the
				// context

				object paramObject = Context.Items[paramName.ToString()];
				if ( paramObject != null )
				{
					sParamValue = paramObject.ToString( );
				}
			}

			return sParamValue;
		}

		
		private void CheckStatus( Object response,
			
			string outputName,
			string executableName,
			bool bSaveOutput,
			bool IsExecuteReport )
		{
			string objectID ;
			string connectionHandle;
			string outputFileType ;
			string errorDescription;
			string executeFileId;

				
			ExecuteReportStatus status ;

			if ( IsExecuteReport )
			{
				status = (( ExecuteReportResponse)response).Status;
				objectID = (( ExecuteReportResponse)response).ObjectId;
				connectionHandle = (( ExecuteReportResponse)response).ConnectionHandle;
				outputFileType = (( ExecuteReportResponse)response).OutputFileType;
				errorDescription = (( ExecuteReportResponse)response).ErrorDescription;
				executeFileId = "";
			}
			else
			{
				status = ((ExecuteQueryResponse)response).Status;
				objectID = (( ExecuteQueryResponse)response).ObjectId;
				connectionHandle = (( ExecuteQueryResponse)response).ConnectionHandle;
				outputFileType = (( ExecuteQueryResponse)response).OutputFileType;
				errorDescription = (( ExecuteQueryResponse)response).ErrorDescription;
				executeFileId = (( ExecuteQueryResponse)response).ExecutableFileId;
			}

			bool bThirdPartyExecutable = ( outputFileType != null && 
				!( AcString.ToLower(outputFileType) == "roi") && 
				
				!(AcString.ToLower(outputFileType) == "doi" ));
				

			// build the url for viewing

			string viewUrl;
			if ( bThirdPartyExecutable )
			{
				viewUrl = BuildView3PartyURL(outputName, objectID, executableName ,connectionHandle, outputFileType, bSaveOutput );
			}
			else
			{
				viewUrl = BuildViewNativeURL( outputName, objectID, executableName, executeFileId, connectionHandle, outputFileType, bSaveOutput, IsExecuteReport );
			}

			bool closex = AcParams.urlCheckboxState( Page, AcParams.Name.viewNewBrowserWindow, false );
			closex = AcParams.urlCheckboxState( Page, AcParams.Name.closex, closex );
			if ( closex )
			{
				// if open in a new window, closex=true 
										
				viewUrl += "&closex=true";
			}

			switch ( status )
			{
				case ExecuteReportStatus.Failed:
			
					// Display an Error message
					
					DisplayErrorMsg( errorDescription );
					break;
			
				case ExecuteReportStatus.Pending:

					// Show Cancel Button. Redirect to waitforexection.aspx

					AcUrlBuilder url = new AcUrlBuilder( "waitforexecution.aspx" );
					url.AddKeyValue( "id", objectID );
					url.AddKeyValue( "connectionHandle", connectionHandle );
					url.AddKeyValue( "outputFileType", outputFileType );
					url.AddKeyValue( "outputName", outputName );
					url.AddKeyValue( "__saveOutput", bSaveOutput.ToString( ) );
					url.AddKeyValue( "redirect", viewUrl );
									
					if ( closex )
					{
						// if open in a new window, closex=true 
						// We set it for both, one for the error page, one for the viewing.
						
						url.AddKeyValue( "closex", "true" );
					}
					Response.Redirect( url.ToString( ) );
					break;

				case ExecuteReportStatus.FirstPage:
					
					// Only one page received, enable progressive viewing
					
					viewUrl += "&isProgressive=true";
					goto case ExecuteReportStatus.Done;
					

				case ExecuteReportStatus.Done:
					
					// Redirect to viewer

					Response.Redirect( viewUrl );
					break;
			}
		}

		private string BuildView3PartyURL( string OutputName,
			string ObjectID, 
			string ExecutableName,
			string ConnectionHandle,
			string outputFileType,
			bool bSaveOutput )
		{
			if ( outputFileType != null )
				outputFileType = AcString.ToLower(outputFileType);

			bool bCompoundDoc = ( outputFileType != null && 
				outputFileType == "row" || 
				outputFileType == "rpw" || 
				outputFileType == "sqw");

			bool bViewCube = ( outputFileType != null && outputFileType == "cb4" );

			AcUrlBuilder Url = null;
			if (bViewCube)
			{
				Url = new AcUrlBuilder("../cubeviewer/viewcube.aspx");
				Url.AddKeyValue("__executableName", ExecutableName);
			}
			else
			{
				Url = new AcUrlBuilder("../viewer/view3partyreport.aspx");
				Url.AddKeyValue("name", OutputName );
			}

			Url.AddKeyValue("id", ObjectID);
			
			if (!bSaveOutput)
			{
				Url.AddKeyValue("connectionHandle", ConnectionHandle );
			}

			Url.AddKeyValue( "outputType", outputFileType);
			if ( bCompoundDoc )
			{
				Url.AddKeyValue( "returnContents", "true");
			}
			if ( bSaveOutput ) 
			{
				Url.AddKeyValue("persistent", "true");
			}
		
			return Url.ToString( ) ;
			
		}

		private string BuildViewNativeURL( string OutputName,
			string ObjectID, 
			string ExecutableName,
			string ExecutableId,
			string ConnectionHandle,
			string outputFileType,
			bool bSaveOutput,
			bool isExecuteReport)
		{
			object objFormat =  GetParameter( AcParams.Name.__viewFormat, true );
			string format;

			if ( objFormat == null )
			{
				format = "dhtml";
			}
			else
			{
				format = AcString.ToLower(( ( string ) objFormat ) );
			}

			AcUrlBuilder sbUrl = new AcUrlBuilder( ) ;

			switch ( format )
			{
				case "dhtml":
				
					// Create the new URL for viewing the report
				
					sbUrl.SetBaseUrl( Context.Request.ApplicationPath + "/viewer/viewframeset.aspx");
					
					string searchCriteria = GetParameter( AcParams.Name.searchcriteria, false );
					if ( searchCriteria != null )
					{
						sbUrl.AddKeyValue( AcParams.Name.searchcriteria, searchCriteria );
					}
					sbUrl.AddKeyValue( "id", ObjectID);
					sbUrl.AddKeyValue( "connectionhandle", ConnectionHandle );
					sbUrl.AddKeyValue( "outputName", OutputName );
					sbUrl.AddKeyValue( AcParams.Name.__saveOutput, bSaveOutput.ToString( ) );
					
					//	For save search
					if (ExecutableId != null && ExecutableId.Trim().Length > 0)
					{
						sbUrl.AddKeyValue( AcParams.Name.__executableId, ExecutableId );
					}
					else
					{
						string ext = ActuUtil.getFileExtension( ExecutableName );
						if (ext == "rov" || ext == "dov")
						{
							//	From a rov/dov file
							String[] resultDef = new String[] {"Id", "Name", "FileType", "Version"};
							
							//	Prepare the condition
							FileSearch fileSearch = new FileSearch();
							FileCondition fileCondition = new FileCondition();
							fileCondition.Field = FileField.FileType;
							fileCondition.Match = "rox,dox";
							
							fileSearch.Item = fileCondition;

							fileSearch.Item1 = ExecutableName;
							fileSearch.Item1ElementName = Item1ChoiceType10.DependentFileName;
							fileSearch.FetchSize = ActuUtil.ReadIntFromConfig( "MAX_LIST_SIZE", Constants.MAX_LIST_SIZE_DEFAULT );
							fileSearch.FetchSizeSpecified = true;
							AcSearchFiles SearchFiles = new AcSearchFiles(resultDef, fileSearch, true, "/", false);
							SearchFiles.Execute(Page);
							
							//	Get executable file
							File executableFile = null;
							if (SearchFiles.ObjectList.Count > 0)
							{
								executableFile = (File) SearchFiles.ObjectList[0];
							}
							if (executableFile != null)
							{
								sbUrl.AddKeyValue( AcParams.Name.__executableId, executableFile.Id );
							}
						}
						else
						{
							//	Get executable id directly
							AcGetFileDetails l_getFileDetails = new AcGetFileDetails(ExecutableName, null);
							l_getFileDetails.Execute(Page);

							if (l_getFileDetails.Response.File != null)
							{
								sbUrl.AddKeyValue( AcParams.Name.__executableId, l_getFileDetails.Response.File.Id );
							}
						}
					}
				
					break;

				case "eanalysis":
					
					sbUrl.SetBaseUrl( "../viewer/searchframe.aspx" );
					sbUrl.AddKeyValue( "id", ObjectID );
					sbUrl.AddKeyValue( "connectionhandle", ConnectionHandle);
					sbUrl.AddKeyValue( "format", "analysis" );

					break;

				case "pdf":

					sbUrl.SetBaseUrl( "../viewer/getreportdata.aspx" );
					sbUrl.AddKeyValue( "objectId", ObjectID );
					sbUrl.AddKeyValue( "connectionhandle", ConnectionHandle);
					sbUrl.AddKeyValue( "componentID", "0" );
					sbUrl.AddKeyValue( "format", "PDF" );
					
					break;

				case "xls":
					
					sbUrl.SetBaseUrl( "../viewer/getreportdata.aspx" );
					sbUrl.AddKeyValue( "objectId", ObjectID );
					sbUrl.AddKeyValue( "connectionhandle", ConnectionHandle);
					sbUrl.AddKeyValue( "componentID", "0" );
					sbUrl.AddKeyValue( "format", "ExcelDataDump" );
										
					break;

				default:
					goto case "dhtml";

			}
			return sbUrl.ToString();


		}

		private void DisplayErrorMsg( string errorDescription )
		{
			//throw new Exception( AcResourceManager.GetString( "MSG_JOB_FAILED", Session ) + "\n" + errorDescription  );

            string target = Request.ApplicationPath + "/Errors/ReportExecutionErrorPage.aspx";

            target += "?errorMessage=" + HttpUtility.UrlEncode(errorDescription);

            Response.Redirect(target, false);
		}

		private void PopulateReportParameters( ExecuteReport executeReportReq , ArrayList paramValueList)
		{
			if ( paramValueList == null )
				return;
			int paramCount = paramValueList.Count;
			if ( paramCount > 0 )
			{
				if ( executeReportReq.Item1 == null )
				{
					executeReportReq.Item1 = new ParameterValue[paramCount];
				}
			}
			for ( int i = 0 ; i < paramCount; i++ )
			{
				ParameterValue param  = ( ParameterValue ) ( paramValueList[i]);
				
				// Trim quotes from parameter values
				if ( param.Value != null )
				{
					param.Value = param.Value.Trim( '"' );
				}
				
				(( ParameterValue[]) executeReportReq.Item1)[i] = param;
			}

		}

		private void ExecuteReport( ArrayList paramValue )
		{
			ExecuteReport executeReportReq = new ExecuteReport();
			executeReportReq.JobName = "TRANSIENT_JOB";
			executeReportReq.IsBundled = false;
			executeReportReq.IsBundledSpecified = true;

			// Get the File extension

			string fileExt = ActuUtil.getFileExtension( sFQExecName );

			// TODO 
			// this part to come from FileType cache
			
			executeReportReq.Item = sFQExecName;
			executeReportReq.ItemElementName = ItemChoiceType42.InputFileName;
			
			
			executeReportReq.SaveOutputFile = bSaveOutput;
			executeReportReq.SaveOutputFileSpecified  = true;

			if ( bSaveOutput )
			{
				executeReportReq.RequestedOutputFile = new NewFile();
				executeReportReq.RequestedOutputFile.Name = sFQOutputName;
				executeReportReq.RequestedOutputFile.VersionName = sOutputVersionName;
				executeReportReq.RequestedOutputFile.ReplaceExisting = ! bNewVersion;
				executeReportReq.RequestedOutputFile.ReplaceExistingSpecified = true;
				if ( sVersionsToRetain != null && sVersionsToRetain.Trim( ) != "")
				{
					executeReportReq.RequestedOutputFile.MaxVersions = Convert.ToInt32( sVersionsToRetain );
					executeReportReq.RequestedOutputFile.MaxVersionsSpecified = true;
				}

				// Handle the Archive Policy
			
			
				JobUtil.SetArchiveRules( Page, sFQExecName, ref executeReportReq.RequestedOutputFile );
			}

			executeReportReq.ProgressiveViewing = bProgressive;
			executeReportReq.ProgressiveViewingSpecified = true;

			// For cancellation, set the timeout

			executeReportReq.WaitTime = Convert.ToInt32( System.Configuration.ConfigurationSettings.AppSettings[ "EXECUTE_REPORT_WAIT_TIME" ] );
			executeReportReq.WaitTimeSpecified = true;

			PopulateReportParameters( executeReportReq, paramValue );

            AcExecuteReport execReport = new AcExecuteReport(executeReportReq);
            bool errorEncountered = false;

            try
            {
                execReport.Execute(Page);

            }
            catch (System.Web.Services.Protocols.SoapException soapException)
            {
                Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(soapException);

                errorEncountered = true;

                string description = HttpUtility.UrlEncode(soapException.Detail["ErrorCode"].InnerText);
                description += HttpUtility.UrlEncode(": ");
                description += HttpUtility.UrlEncode(soapException.Detail["Description"]["Message"].InnerText);

                string serverURL = AcParams.Get(Page, AcParams.Name.serverURL);
                if (serverURL != null)
                {
                    description += "&serverURL=";
                    description += HttpUtility.UrlEncode(serverURL);
                }
                string volume = AcParams.Get(Page, AcParams.Name.volume);
                if (volume != null)
                {
                    description += "&volume=";
                    description += HttpUtility.UrlEncode(volume);
                }

                DisplayErrorMsg(description);
                
            }
            catch (Exception ex)
            {
                errorEncountered = true;
                throw ex;
            }

            if (!errorEncountered)
            {
                ExecuteReportResponse response = execReport.Response;

                // If Successful,  view the report
                CheckStatus(response, sFQOutputName, sFQExecName, bSaveOutput, true);
            }
		}

		private void ExecuteQuery( Query query, string execName )
		{
			
			ExecuteQuery execQuery = new ExecuteQuery();

			execQuery.JobName = "executeQuery";
			execQuery.Item = sFQExecName;
			execQuery.ItemElementName = ItemChoiceType43.InputFileName;
			execQuery.SaveOutputFile = bSaveOutput;
			execQuery.SaveOutputFileSpecified = true;

			NewFile OutputFile = new NewFile();
			OutputFile.Name = sFQOutputName;
			OutputFile.VersionName = sOutputVersionName;
			OutputFile.ReplaceExisting = ! bNewVersion;
			OutputFile.ReplaceExistingSpecified = true;

			execQuery.RequestedOutputFile = OutputFile;
			
			if ( query != null )
			{
				execQuery.Item1 = query;
				execQuery.Item1ElementName = Item1ChoiceType20.Query;
			}
			else
			{				
				execQuery.Item1 = execName;
				execQuery.Item1ElementName = Item1ChoiceType20.QueryFileName;
			}
			execQuery.ProgressiveViewing = bProgressive;
			execQuery.ProgressiveViewingSpecified = true;

			
			execQuery.WaitTime = Convert.ToInt32( System.Configuration.ConfigurationSettings.AppSettings[ "EXECUTE_REPORT_WAIT_TIME" ] );
			execQuery.WaitTimeSpecified = true;

			AcExecuteQuery executeQuery = new AcExecuteQuery( execQuery );
			executeQuery.Execute( Page );
			ExecuteQueryResponse response = executeQuery.Response;
			
			CheckStatus( response, sFQOutputName, sFQExecName, bSaveOutput, false );

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
