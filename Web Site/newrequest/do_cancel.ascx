<%@ Control Language="c#" Inherits="activeportal.usercontrols.newrequest.do_cancel" CodeFile="do_cancel.ascx.cs" %>
<html>
	<head></head>
	<body id="AcBody" runat="server">
			
	</body>
</html>
<script>
	
	function GoBack()
	{
		// We close the window if the WaitForReport was in a separate window
	    
		// Otherwise, we go back to the page the report was submitted from.
		var curHistoryLength = parent.history.length;

		if (curHistoryLength < 2)
		{
			parent.window.close();
		}
		else
		{
			parent.history.go( <%=goBack%> );
		}
	}
</script>