namespace activeportal.usercontrols.newrequest
{
	using System;
	using System.Data;
	using System.Drawing;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes;
	using activeportal.classes.api.proxy;
	using System.Threading;
	using System.Collections;
	using activeportal.classes.api;
	using System.Text;
	using actuate_i18n;

	/// <summary>
	///		Summary description for do_submitjob1.
	/// </summary>
	public partial  class do_submitjob : System.Web.UI.UserControl
	{

		string sOutputFolderType;
		//string sError;
		//int iVersionsToRetain = -1;
		string sJobName;
		string sHeadline;
		string sOutputVersionName;
		string sFQExecName;
		bool   IsQuery = false;
		string sFQOutputName;
		
		string sPriority;
		string sPriorityValue;


		string sVersionsToRetain = null;

		bool bNewVersion = true;
		
		string sOutputFileType;

		// Notification
		string[] saChannelsToNotify = null;
		string[] saGroupsToNotify = null;
		string[] saUsersToNotify = null;


		ArrayList paramValueList = null;
		private ScheduleInformation m_scheduleInfo;
		bool FolderSpecified = false;



		protected void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here

			// remove framecontenturl from session( for treeview)
			AcParams.SetToSession( Session,  AcParams.Name.frameContentUrl, null );

			// Put user code to initialize the page here

			// This page can be accessed directly, and the  parameters can be specified
			// in the URL or , the parameters can come from the index.aspx page, from where
			// the user has been directed to this page.

			// We check for parameters in the URL first, if present, else we 
			// check from the Context.Items collection where the parameters
			// Have been stored in the  newrequest/index.aspx page

			sFQExecName = AcParams.Get( Page, AcParams.Name.__executableName, "", Context );
			if ( sFQExecName == null || sFQExecName == String.Empty )
			{
				sFQExecName = AcParams.Get( Page, AcParams.Name.dataSourceName, "", Context );
			}

			string fileExtension = ActuUtil.getFileExtension( sFQExecName );
			
			if ( AcString.ToLower(fileExtension) == "dox" || 
				AcString.ToLower(fileExtension) == "dov" )
			{
				IsQuery = true;
			}
			

			
			sOutputFolderType = AcParams.Get( Page, AcParams.Name.__outputfoldertype, "", Context);
			
			
			if ( sOutputFolderType == null || sOutputFolderType.Trim() == String.Empty )
			{
				sOutputFolderType = JobUtil.GetOutputFolderType( Page ) ;
			}


			sJobName = AcParams.Get( Page, AcParams.Name.__jobname, "", Context);

			//sScheduleType = AcParams.Get( Page, AcParams.Name.__scheduleType, "immediate", Context );
			//sScheduleType = sScheduleType.Trim().ToLower();

			m_scheduleInfo  = new ScheduleInformation( Page, Context );

			sHeadline = AcParams.Get( Page, AcParams.Name.__headline,"", Context);
			sOutputVersionName = AcParams.Get( Page, AcParams.Name.__versionName, null, Context); 

			
			sFQOutputName = AcParams.Get( Page, AcParams.Name.__outputName, "", Context);

			if ( sFQOutputName != null && sFQOutputName.Trim() != String.Empty )
			{
				if ( sFQOutputName.StartsWith("/") )
				{
					FolderSpecified = true;
				}
			}

			
			FileType ft =  AcFileTypeManager.GetFileType( fileExtension , Page  );
			sOutputFileType = ft.Name;

	

			if (sOutputFolderType == Constants.FOLDER_PERSONAL &&  
				sFQOutputName != null &&
				sFQOutputName.Trim().Length > 0 ) // PREFIX PERSONAL FOLDERNAME AT JOB SUBMISSION TIME
			{

				// Get the Folder Name form the Output Name
				// Check if same as Home Folder . If so then output is in that folder
				string sFQON = AcParams.Get( Page, AcParams.Name.homeFolder );
				string OutputFolder = ActuUtil.GetFolderFromFileName(sFQOutputName);

				if ( ! FolderSpecified  )
				{
					if (sFQON == null) sFQON = "/";
					if (!sFQON.EndsWith("/")) sFQON += '/';
					if (!sFQON.StartsWith("/")) sFQON = '/' + sFQON;
					sFQON += ActuUtil.stripFolderName(sFQOutputName, "/");
					sFQOutputName = sFQON;
				}
			}

			if (sFQExecName == null) // VALIDATION FOR EXISTENCE OF '__executableName'
			{
				//sError = "ERRMSG_MISSING_EXEC_NAME";
				sFQExecName = "UNSPECIFIED";
			}
			else if (sFQOutputName == null || sFQOutputName.Trim().Length == 0) // VALIDATION FOR EXISTENCE OF '__outputName' OR BUILD IT IF MISSING
			{
				string outputExtension = "";
				if ( AcString.ToLower(sOutputFileType) == "rov"  )
					outputExtension = "roi";
				else if ( AcString.ToLower(sOutputFileType) == "dov" )
					outputExtension = "doi";
				else
					outputExtension = ft.OutputType;

				sFQOutputName = ActuUtil.stripFileExtension(sFQExecName)  + "." + outputExtension;

				if ( sOutputFolderType == Constants.FOLDER_PERSONAL )
				{

					string HomeFolder = AcParams.Get( Page, AcParams.Name.homeFolder );

					if (HomeFolder == null) HomeFolder = "/";
					if (!HomeFolder.EndsWith("/")) HomeFolder += '/';
					if (!HomeFolder.StartsWith("/")) HomeFolder = '/' + HomeFolder; 

					sFQOutputName = HomeFolder + sFQOutputName;
				}
				
			}



			bool bThirdPartyExecutable = ( sFQExecName != null && 
				!( AcString.ToLower(ActuUtil.getFileExtension(sFQExecName)) == "rox") && 
				!( AcString.ToLower(ActuUtil.getFileExtension(sFQExecName)) == "rov" ) &&
				!( AcString.ToLower(ActuUtil.getFileExtension(sFQExecName)) == "dox" ) && 
				!( AcString.ToLower(ActuUtil.getFileExtension(sFQExecName)) == "dov" ));

			
			
			// HANDLE URL PARAMETERS ... __priority, __priorityValue
			sPriority = AcString.ToLower(AcParams.Get( Page, AcParams.Name.__priority, "other" ).Trim( ));

			switch ( sPriority )
			{
				case "other" :
				{
					sPriorityValue = AcParams.Get( Page, AcParams.Name.__priorityValue, "500" );
				}
					break;

				case "low":
				{
					sPriorityValue = Constants.LOW_PRIORITY_VALUE.ToString();
				}
					break;

				case "medium" :
				{
					sPriorityValue = Constants.MEDIUM_PRIORITY_VALUE.ToString();
				}
					break;

				case "high":
				{
					sPriorityValue = Constants.HIGH_PRIORITY_VALUE.ToString();
				}
					break;

				default:
				{
					sPriorityValue = sPriority;
				}
					break;
			}
			
					
			// HANDLE URL PARAMETERS ... __channels, __groups, __users
			
			saChannelsToNotify = AcParams.GetListFromUrl( Page, AcParams.Name.__channels );
			saGroupsToNotify = AcParams.GetListFromUrl( Page, AcParams.Name.__groups );
			saUsersToNotify = AcParams.GetListFromUrl( Page, AcParams.Name.__users);
			
			// IfExists = createN | replace | create

			string sIfExists = AcParams.Get( Page, AcParams.Name.__ifexists, null, Context);
			if (sIfExists != null)
			{
				sIfExists = AcString.ToLower(sIfExists).Trim();
				bNewVersion = sIfExists.StartsWith("create");
				if ( bNewVersion )
				{
					sVersionsToRetain =  sIfExists.Substring( 6 ) ;
				}
			}
		
				// Old reportcast parameters support

			else
			{
				string overwrite = AcParams.Get( Page, AcParams.Name.__overwrite, null, Context);

				// __overwrite = new | old | true | false

				if ( overwrite != null )
				{
					overwrite = AcString.ToLower(overwrite).Trim( );
					bNewVersion = ! ( overwrite == "old" || overwrite == "true" );
				}						
					
				if( bNewVersion	)
				{
					// __limit = limit|true|false

					string sLimit = AcParams.Get( Page, AcParams.Name.__limit, null, Context);
					if ( sLimit != null )
					{
						sLimit = AcString.ToLower(sLimit).Trim( );
						if ( sLimit == "true" || sLimit == "limit" )
						{
							// __limitNumber = number
							sVersionsToRetain = AcParams.Get( Page, AcParams.Name.__limitNumber, null, Context);
						}
					}
				}
			}
			
			
			//Report Parameters

			paramValueList = new ArrayList();
			string sourceOfRequest = AcParams.Get( Page, AcParams.Name.__SourceOfRequest__, "", Context);
			if ( sourceOfRequest  == "Index.aspx" )
			{
				paramValueList = ( ArrayList) ( Session["ReportParameterList"]);
			}
			else
			{
				paramValueList = JobUtil.GetReportParametersFromRequestURL( this.Request, JobUtil.ExcludeParamList );
			}

	

			SubmitJob submitJobReq = new activeportal.classes.api.proxy.SubmitJob();
			SetJobGeneralProperties( submitJobReq );
			SetJobInputProperties( submitJobReq );
			SetJobOutputProperties( submitJobReq );
			SetJobParameters( submitJobReq );
			SetJobSchedules( submitJobReq );
			SetJobNotification( submitJobReq );
			SetAttachmentNotification( submitJobReq );
			SetConversionOptions( submitJobReq );
			SetUserPreferences( submitJobReq );

			
			AcSubmitJob submitJob = new AcSubmitJob( submitJobReq );
			submitJob.Execute( Page );
			SubmitJobResponse  response = submitJob.Response;



			// Check the status and direct to the correct page
			CheckStatus(sJobName, sFQExecName, response.JobId );

		}

		private void SetJobGeneralProperties( SubmitJob submitJobReq )
		{
			submitJobReq.Headline = sHeadline;
			submitJobReq.JobName = sJobName;
			submitJobReq.IsBundled = false;
			submitJobReq.IsBundledSpecified = true;
			submitJobReq.Operation = SubmitJobOperation.RunReport;
			submitJobReq.Priority = Convert.ToInt32( sPriorityValue );


		}

		private void SetJobInputProperties( SubmitJob submitJobReq )
		{
			submitJobReq.Item = sFQExecName;
			submitJobReq.ItemElementName = ItemChoiceType45.InputFileName;
		}

		private void SetJobOutputProperties( SubmitJob submitJobReq )
		{
			NewFile OutputFile  = new NewFile();
			OutputFile.Name = sFQOutputName;
			OutputFile.VersionName = sOutputVersionName;
			OutputFile.ReplaceExisting = ( !bNewVersion );
			OutputFile.ReplaceExistingSpecified  = true;

			if ( sVersionsToRetain != null && sVersionsToRetain.Trim( ) != "" )
			{
				long VersionsToRetain = Convert.ToInt64( sVersionsToRetain );
				OutputFile.MaxVersions = VersionsToRetain;
				OutputFile.MaxVersionsSpecified = true;
			}
			
			JobUtil.SetArchiveRules( Page, sFQExecName, ref OutputFile );
			submitJobReq.RequestedOutputFile = OutputFile;
	
		}

		private void SetJobParameters( SubmitJob submitJobReq )
		{
			if ( ! IsQuery )
			{
				if ( paramValueList == null )
					return;
				int paramCount = paramValueList.Count;
				if ( paramCount > 0 )
				{
					if ( submitJobReq.Item1 == null )
					{
						submitJobReq.Item1 = new ParameterValue[paramCount];
					}
				}
				for ( int i = 0 ; i < paramCount; i++ )
				{
					(( ParameterValue[]) submitJobReq.Item1)[i] = ( ParameterValue ) ( paramValueList[i]);
				}
				submitJobReq.Item1ElementName = Item1ChoiceType21.ParameterValues;
			}
			else
			{
				Query query = (Query)AcParams.GetObjectFromSession( Page, AcParams.Name.executingQueryObject );

				if ( query != null )
				{
					submitJobReq.Item1 = query;
					submitJobReq.Item1ElementName = Item1ChoiceType21.Query;
				}
			}
		}

		private void SetJobSchedules( SubmitJob submitJobReq )
		{
			submitJobReq.Schedules = m_scheduleInfo.GetSchedule();
			
		}

		private void SetJobNotification( SubmitJob submitJobReq )
		{
			if ( saUsersToNotify != null && saUsersToNotify.Length > 0 )
			{
				submitJobReq.NotifyUsersByName = saUsersToNotify;
			}

			if ( saGroupsToNotify != null && saGroupsToNotify.Length > 0 )
			{
				submitJobReq.NotifyGroupsByName = saGroupsToNotify;
			}

			if ( saChannelsToNotify != null && saChannelsToNotify.Length > 0 )
			{
				submitJobReq.NotifyChannelsByName = saChannelsToNotify;
			}

		}

		private void 	CheckStatus(string sJobName, string sFQExecName, string jobId )
		{
			string sourceOfRequest = AcParams.Get( Page, AcParams.Name.__SourceOfRequest__);
			
			if ( sourceOfRequest != "Wizard"  )
			{
				CheckJobStatus(  sJobName,  sFQExecName, jobId );
			}
			else
			{
				CheckQueryStatus( sJobName, sFQExecName, jobId);
			}
		}

		private void CheckJobStatus( string sJobName, string sFQExecName, string jobId )
		{
			AcUrlBuilder url = new AcUrlBuilder();
			string target = AcParams.Get( Page, AcParams.Name.__redirect, "submittedjob_status.aspx", AcParams.From.Url );
			url.SetBaseUrl( target );
			url.AddKeyValue( "__executableName", sFQExecName );
			url.AddKeyValue( "__jobName", sJobName );
			url.AddKeyValue( "jobID", jobId );
			url.AddKeyValue("jobAction", m_scheduleInfo.PostJobAction);
			

			string requesttype = AcParams.Get( Page, AcParams.Name.__requesttype, AcParams.From.Url );
			url.AddKeyValue("__requesttype", requesttype);
			
			ActuUtil.Redirect( Response, url.ToString() );
		}

		private void SetAttachmentNotification( SubmitJob submitJobReq )
		{
			string SendEmailNotification = AcParams.Get( Page, AcParams.Name.notify, AcParams.From.Url );
			string AttachmentFormat =  AcParams.Get( Page, AcParams.Name.notificationAttachment, AcParams.From.Url);

			if ( SendEmailNotification != null ) 
				
			{
				SendEmailNotification = AcString.ToLower(SendEmailNotification);

				// See SCR 60111
				/*
					If the �Send email notification� check box is not checked even then the 
					recipient preference should be overridden and the user should not receive any email.
					
					The user should always receive a failure email notification if the �Send me an email notification� checkbox 
					is checked on the Background Job Output tab or the Actuate Query Finish tab.
				*/
						
        		submitJobReq.OverrideRecipientPref = true;
        		submitJobReq.OverrideRecipientPrefSpecified = true;

				if ( AttachmentFormat != "no" && AttachmentFormat != null)
				{
					submitJobReq.AttachReportInEmail = true;
					submitJobReq.AttachReportInEmailSpecified = true;
					submitJobReq.EmailFormat = AttachmentFormat;
				}

				submitJobReq.SendEmailForSuccess = SendEmailNotification == "true";
				submitJobReq.SendEmailForSuccessSpecified = true;
				submitJobReq.SendEmailForFailure = SendEmailNotification == "true";
				submitJobReq.SendEmailForFailureSpecified = true;
        
			}
  
		}

		private void SetConversionOptions( SubmitJob submitJobReq )
		{
			if ( ! OutputConversionEnabled() )
				return;

			string Format = AcParams.Get( Page, AcParams.Name.__Format, AcParams.From.Url );

			if ( Format == null || Format.Trim() == String.Empty )
			{
				Format = "ROI";
			}

			string KeepROIIfSucceeded = AcParams.Get( Page, AcParams.Name.__KeepROIIfSucceeded, AcParams.From.Url );
			string KeepROIIfFailed = AcParams.Get( Page, AcParams.Name.__KeepROIIfFailed, AcParams.From.Url);

			if ( KeepROIIfSucceeded == null || KeepROIIfSucceeded.Trim() == String.Empty )
			{
				KeepROIIfSucceeded = "false" ;
			}

			if ( KeepROIIfFailed == null || KeepROIIfFailed.Trim() == String.Empty )
			{
				KeepROIIfFailed = "false";
			}

			ConversionOptions ConvOptions = new ConversionOptions();
			ConvOptions.Format = Format;
			ConvOptions.KeepROIIfSucceeded = (KeepROIIfSucceeded.ToLower() == "true");
			ConvOptions.KeepROIIfSucceededSpecified = true;
			ConvOptions.KeepROIIfFailed = (KeepROIIfFailed.ToLower() == "true" );
			ConvOptions.KeepROIIfFailedSpecified = true;
			submitJobReq.ConversionOptions = ConvOptions;

		}

		private void CheckQueryStatus(string sJobName, string sFQExecName, string jobId )
		{
			
			string sServerURL = AcParams.Get( Page, AcParams.Name.serverURL );


			//  Status message

			string[] saStatusMsg = { HttpUtility.HtmlEncode(ActuUtil.stripVersionNumber(sFQExecName)) , jobId  };
			string sStatus = String.Format( AcResourceManager.GetString("query.popup.label.submitted", Session),saStatusMsg );

			
			((HtmlGenericControl)ActuUtil.FindControlRecursive( Page, "AcBody" )).Attributes.Add("OnLoad","openPopupWindow( '../query/querystatus.aspx?queryStatusMsg=" + ActuUtil.jsEncode( HttpUtility.UrlEncode( sStatus ) ) + "','AcConfirmation',300,150,'' );javascript:history.back();");

			//Close the current window
		}

		private bool OutputConversionEnabled()
		{

			// First Check the Url 

			string ConversionAllowed =  AcParams.Get( Page, AcParams.Name.OutputConversionEnabled, null, AcParams.From.Url );
			if ( ConversionAllowed != null && ConversionAllowed.Trim() != String.Empty )
			{
				return Convert.ToBoolean(ConversionAllowed);
			}

			if ( sFQExecName == null || sFQExecName.Trim() == String.Empty )
			{
				return false;
			}

			string FileExtension = ActuUtil.getFileExtension( sFQExecName );

			if ( FileExtension == null || FileExtension.Trim() == String.Empty )
			{
				return false;
			}


			if ( FileExtension.ToLower() == "rox" ||
				FileExtension.ToLower() == "roi" )
			{
				return true;
			}
			else if ( FileExtension.ToLower() == "rov" )
			{
				// If rov, check the extension of the actual executable file
				// from which this parameter file was created
				// If it is created from a third party report , then we should not 
				// be allowing the ouptut conversion

				string[] ResultDef =  { "Name" };
				string DependentFile = ActuUtil.GetDependentFileByName( sFQExecName, ResultDef, Page );
				if ( DependentFile != null && DependentFile.Trim() != String.Empty )
				{
					string fileExtension =  ActuUtil.getFileExtension( ActuUtil.stripVersionNumber( DependentFile ));

					if ( fileExtension.ToLower() == "rox" ||
						fileExtension.ToLower() == "rov" ||
						fileExtension.ToLower() == "roi" )
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return true;
				}
			}


			return false;
		}

		private void SetUserPreferences( SubmitJob submitJobReq ) 
		{
			// Get the user object

			User user  = AcUserManager.GetUserInfo(Page);

			if ( user == null )
			{
				return;
			}
			submitJobReq.SendSuccessNotice = user.SendNoticeForSuccess;
			submitJobReq.SendSuccessNoticeSpecified = true;
			submitJobReq.SendFailureNotice = user.SendNoticeForFailure;
			submitJobReq.SendFailureNoticeSpecified = true;

		}


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}

	
}
