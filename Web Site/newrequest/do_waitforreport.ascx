<%--
	@author 	Actuate Corporation
				Copyright (C) 2003 Actuate Corporation. All rights reserved.
	@version	1.0
	$Revision:: $
--%>
<%@ Control Language="c#" Inherits="activeportal.usercontrols.newrequest.do_waitforreport" CodeFile="do_waitforreport.ascx.cs" %>
<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<%@ Import namespace="activeportal.classes" %>
<table height=100% width=100%>
	<tr>
		<td align=center valign=center>
			<table>
				<tr>
					<td align=center style="COLOR: #336699" >
						<b><localized:AcLabelControl Key="TXT_REPORT_EXECUTING" runat="server"/></b>
					</td>
				</tr>
				<tr>
					<td align=center>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td align=center>
						<localized:AcButtonControl id="AcBtnCancel" Key="BTN_CANCEL_EXEC" runat="server" onclick="OnCancelButton" />
						</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<script>
if ('<%=submitForm%>' == 'True')
{

	document.forms[0].submit();
}
else
{
	history.back();
	if ( '<%=succeeded%>' == 'True' )
		document.location.replace('<%=ActuUtil.jsEncode(redirect)%>');
	else
		document.location.replace('<%=ActuUtil.jsEncode(error)%>');
}
</script>
