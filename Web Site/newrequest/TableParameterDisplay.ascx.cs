namespace activeportal.newrequest
{
	using System;
	using System.Collections;
	using System.Data;
	using System.Drawing;
	using System.Text;
	using System.Web;
	using System.Web.UI.WebControls;
	using System.Web.UI.HtmlControls;
	using activeportal.classes.api;
	using activeportal.classes.api.proxy;
	using activeportal.classes;
    using actuate.classes;
	using actuate_i18n;
	using actuate_i18n.classes;
	

	/// <summary>
	///		Summary description for TableParameterDisplay1.
	/// </summary>
	public partial  class TableParameterDisplay1 : System.Web.UI.UserControl
	{

		private string m_tableParamName; 
		private FieldValue[][]  m_TableValues = null;
		private FieldValue[] m_InsertedRecord = null;
		private FieldDefinition[] m_recordDefinition = null;
		private string  m_tableParamDisplayMode = "insert";
		private int  m_tableEditedRowNumber  = 0;
		private int  m_rowToBeDeleted = 0;
		private ParameterDefinition m_parameterList = null;
		private string m_usedForActuateQuery = "false";




		
		

		private string m_executableName;
		private Hashtable   m_paramTable; // holds the name of the parameter and if it is hidden
		private bool m_dataChanged = false;


		protected void Page_Load(object sender, System.EventArgs e)
		{
			
			if ( IsSessionTimeOut() )
				return;

			GetRequiredInputParameters();			
			if ( ! IsPostBack && ( m_tableParamDisplayMode != "delete") )
			{
				CopyParameterList();
			}

			m_parameterList = ( ParameterDefinition) Session["EditedParamList"];

			GetDefaultValues();

			//Display the header of the Table
			DisplayHeader();

	
			if ( IsPostBack )
			{

				if ( m_tableParamDisplayMode == null || AcString.ToLower(m_tableParamDisplayMode) == "insert" )
				{
					DisplayInsertedRecord();
				}
			}
			

			DisplayTableValues( m_TableValues );
			RenderControl();
		
		}


		private void GetDefaultValues()
		{

			if ( ! IsPostBack )
			{
				if ( m_parameterList != null )
				{
					m_recordDefinition = m_parameterList.RecordDefinition;
					m_TableValues = m_parameterList.DefaultTableValues;
				}

				Session["RecordDefinition"] = m_recordDefinition;
				Session["DefaultTableValues"] = m_TableValues;
			}
			else
			{
				m_recordDefinition  = ( FieldDefinition[]) Session["RecordDefinition"];
				m_TableValues = ( FieldValue[][]) Session["DefaultTableValues"];

				if ( AcString.ToLower(m_tableParamDisplayMode) == "edit" )
				{
					UpdateTableValues();
				}
				
			}
		}

		private void DisplayHeader()
		{

			if ( m_recordDefinition == null )
				return;

			HtmlTableRow HeadingRow = new HtmlTableRow();

			HtmlTableCell ButtonCell = new HtmlTableCell("th");
			ButtonCell.Attributes["class"] = "tableParameterColumnHeader";


			AcButtonControl DeleteAllBtn = new AcButtonControl();
			DeleteAllBtn.Key = "parameters.tables.button.deleteall";
			DeleteAllBtn.AddSpaces = false;
			DeleteAllBtn.Click += new System.EventHandler( DeleteAllButton_Clicked );
			string ConfirmMsg = AcResourceManager.GetString( "parameters.tables.confirm.deleteall", Session );
			DeleteAllBtn.Attributes.Add ("onclick", "return confirm(\"" + ConfirmMsg + "\");");
			ButtonCell.Controls.Add( DeleteAllBtn );

			HeadingRow.Cells.Add( ButtonCell );

			m_paramTable = new Hashtable();

			for( int i=0; i< m_recordDefinition.Length; i++ )
			{
				HtmlTableCell HeadingCol = new HtmlTableCell("th");

				HeadingCol.Attributes["class"] = "tableParameterColumnHeader";
				HeadingCol.InnerHtml = "&nbsp;&nbsp;" + HttpUtility.HtmlEncode(m_recordDefinition[i].DisplayName) + "&nbsp;&nbsp;";
				HeadingCol.Visible = !m_recordDefinition[i].IsHidden;
				HeadingRow.Cells.Add( HeadingCol );
				m_paramTable[m_recordDefinition[i].Name] = m_recordDefinition[i].IsHidden ;
			}

			TableParamData.Rows.Add( HeadingRow );
		}


		private void DisplayTableValues( FieldValue[][] TableValues )
		{
			if (TableValues == null )
				return;

			// We assume that the Order of The parameters here is  the 
			// same as that in the ReordDefinition

			//  Which Row is the Edit BUtton For 
			int EditBtnIndex  = (TableParamData.Rows == null || TableParamData.Rows.Count == 0 ) ? 0: TableParamData.Rows.Count -1;

			for ( int j=0; j < TableValues.Length; j++ )
			{

				HtmlTableRow DataRow = new HtmlTableRow();

				HtmlTableCell ButtonCell = new HtmlTableCell();

				AcButtonControl EditButton = new AcButtonControl();
				EditButton.Key = "parameters.tables.button.edit";
				EditButton.AddSpaces = false;

				
				SetEditEventBtnAttributes( EditButton, EditBtnIndex );
				EditButton.Click +=  new EventHandler(EditButton_Clicked);
				EditBtnIndex ++ ;

				Literal space = new Literal();
				space.Text = "&nbsp;&nbsp;";

				AcButtonControl DeleteButton = new AcButtonControl();
				DeleteButton.Key = "BTN_DELETE";
				DeleteButton.AddSpaces = false;
				DeleteButton.CommandArgument = j.ToString();
				DeleteButton.Click +=  new System.EventHandler(DeleteButton_Clicked);

				ButtonCell.Controls.Add( EditButton );
				ButtonCell.Controls.Add( space );
				ButtonCell.Controls.Add( DeleteButton );

				DataRow.Cells.Add( ButtonCell );


				FieldValue[]  ActualData = TableValues[j];

				for ( int i=0; i < ActualData.Length; i++ )
				{
					HtmlTableCell DataCell = new HtmlTableCell();
					FieldValue fldVal = ActualData[i];
					
					DataCell.InnerText = fldVal.Value;
					DataRow.Cells.Add( DataCell );

					// Hide parameters which are not supposed to be displayed
					if ( m_paramTable != null )
					{
						DataCell.Visible =  !((bool) m_paramTable[fldVal.Name]);
					}
				}

				TableParamData.Rows.Add(DataRow );
			}
		}


		private void DisplayInsertedRecord()
		{

			m_InsertedRecord = ( FieldValue[] ) ( Session["InsertedTableRecord"] );

			if ( m_InsertedRecord == null )
				return;

			FieldValue[][]  InsertedRecord = new FieldValue[1][];
			InsertedRecord[0] = new FieldValue[1];
			InsertedRecord[0] = m_InsertedRecord;

			DisplayTableValues( InsertedRecord );

			//RepopulateTableValues();
			
		}


		/*
		private void RepopulateTableValues()
		{
			
			int NewTableSize = m_TableValues.Length + 1 ;
			FieldValue[][] NewTable = new FieldValue[ NewTableSize ][];

			for (int i = 0; i < NewTableSize - 1; i++ )
			{
				NewTable[i] = new FieldValue[ m_TableValues[i].Length ];
				NewTable[i] = m_TableValues[i];
			}

			NewTable[ NewTableSize - 1] = new FieldValue[ m_InsertedRecord.Length ];
			NewTable[ NewTableSize - 1] = m_InsertedRecord;


			GetReportParametersResponse ReportParametersRes = ( GetReportParametersResponse) Session["Parameters"];
			ParameterDefinition[] ParamList = ReportParametersRes.ParameterList;

			for ( int i=0; i < ParamList.Length; i++ )
			{
				if ( m_tableParamName == ParamList[i].Name )
				{
					ParamList[i].DefaultTableValues = NewTable;
					break;
				}
			}

			Session["InsertedTableRecord"] = null;
			Session["RecordInserted"] = null;
		}
		*/

		protected override void OnPreRender( System.EventArgs e )
		{
			
			if ( IsSessionTimeOut() )
			{
				GenerateWindowCloseScript();
				return;
			}
			string Args = GetInsertEditBtnArgs( "insert", 0 );
			AcInsertBtnTop.Attributes["onclick"] = "javascript:DisplayTableParameterEditor(" + Args + ");";
			AcInsertBtnBtm.Attributes["onclick"] = "javascript:DisplayTableParameterEditor(" + Args + ");";

			string ConfirmMsg = AcResourceManager.GetString( "parameters.tables.confirm.discardAll", Session );

			if ( IsPostBack )
			{
				m_dataChanged = (bool) ViewState["DataChanged"];
			}

			string CancelBtnArgs = null;

			// Give the Confirmation Message only when the "Edit" or the "Delete" button is clicked
			if ( m_dataChanged || AcString.ToLower(m_tableParamDisplayMode) == "delete" )
			{
				CancelBtnArgs =  "if ( confirm(\"" + ConfirmMsg + "\")) { javascript:CloseParameterDisplay(); }";
			}
			else
			{
				CancelBtnArgs =  " javascript:CloseParameterDisplay();";
			}
			AcCancelBtnTop.Attributes["onclick"] = CancelBtnArgs;
			AcCancelBtnBtm.Attributes["onclick"] = CancelBtnArgs;


			ResetSessionVariables( );

		}

		private void ResetSessionVariables()
		{

			//Session["InsertedTableRecord"] = null;
			//Session["RecordInserted"] = null;
			//Session["TableParamDisplayMode"] = null;
			//Session["TableParamRowNumber"] = null;

			// Add the INsrted row to the List so that it exists
			// across postbacks

			int NewTableLength = 0;
			NewTableLength += ( m_TableValues == null ) ? 0 : m_TableValues.Length ;
			NewTableLength += ( m_InsertedRecord == null ) ? 0 : 1 ;

			if ( NewTableLength > 0 )
			{
				FieldValue[][] NewTableValues = new FieldValue[ NewTableLength ][];

				// The first record should be the Inserted Record
				if ( m_InsertedRecord != null )
				{
					NewTableValues[0] = new FieldValue[ m_recordDefinition.Length ];
					NewTableValues[0] = JobUtil.CopyFieldValue(m_InsertedRecord);
				}


				// Now the Default Values or those already inserted 
				if ( m_TableValues != null )
				{
					for ( int i=0; i < m_TableValues.Length ; i++ )
					{
						int index = ( m_InsertedRecord == null ) ? i: i+1;
						NewTableValues[index] = new FieldValue[ m_recordDefinition.Length ];
						NewTableValues[index] = m_TableValues[i];
					}
				}


				ParameterDefinition TableParamDefinition  = new ParameterDefinition();
				TableParamDefinition = m_parameterList;
				TableParamDefinition.DefaultTableValues = NewTableValues;

				Session["EditedParamList"] = TableParamDefinition;
				Session["DefaultTableValues"] = NewTableValues;


				Session["InsertedTableRecord"] = null;
				Session["RecordInserted"] = null;
				Session["TableParamDisplayMode"] = null;
				Session["TableParamRowNumber"] = null;

				
			}
		}


		private void SetEditEventBtnAttributes( Button EditButton, int i )
		{
			string args = GetInsertEditBtnArgs( "edit", i);
			EditButton.Attributes["onclick"] = "javascript:DisplayTableParameterEditor(" + args + ");";
		}

		private void UpdateTableValues()
		{
			FieldValue[] UpdatedRecord  = ( FieldValue[] ) ( Session["InsertedTableRecord"] );

			if ( UpdatedRecord  == null || m_TableValues == null )
				return;

			m_TableValues[m_tableEditedRowNumber] = UpdatedRecord;
		}

		public void DeleteButton_Clicked( Object sender,System.EventArgs e )//CommandEventArgs 
		{
			try
			{
				Button DeleteButton = (Button)(sender);
				m_rowToBeDeleted =  Int32.Parse( DeleteButton.CommandArgument);
			}
			catch
			{
			}

			m_dataChanged = true;
			ViewState["DataChanged"] = m_dataChanged;

			DeleteTableValues();
			Server.Transfer(GetDeleteButtonArgs());

		}


		public void DeleteAllButton_Clicked( Object sender,System.EventArgs e )
		{
			DeleteAllTableValues();
			Server.Transfer(GetDeleteButtonArgs());
		}

		private void DeleteTableValues()
		{
			if ( m_TableValues == null )
				return;

			FieldValue[][] NewTableValues = new FieldValue[ m_TableValues.Length -1 ][];

			int j=0;
			for ( int i =0; i < m_TableValues.Length; i ++ )
			{
				if ( i == m_rowToBeDeleted )
					continue;

				NewTableValues[j] = new FieldValue[ m_recordDefinition.Length ];
				NewTableValues[j] = m_TableValues[i];

				j = j+1;
			}

			m_parameterList.DefaultTableValues = NewTableValues;
			Session["EditedParamList"] = m_parameterList;
		}

		private void DeleteAllTableValues()
		{
			m_parameterList.DefaultTableValues = null;
			Session["EditedParamList"]  = m_parameterList;
		}

		private void CopyParameterList()
		{
			ParameterDefinition[] ParamList = null;
			ParameterDefinition TableParamDefinition = null;

			if ( m_usedForActuateQuery == "false" )
			{
				GetReportParametersResponse ReportParametersResp = ( GetReportParametersResponse ) Session["Parameters"];

				if (ReportParametersResp == null )
					return;
				ParamList = ReportParametersResp.ParameterList;
			}
			else
			{
				Query query =  (Query)Session[AcParams.Name.queryObject.ToString()];
				ParamList = query.ParameterDefinitionList;
			}
			
			for ( int i=0; i < ParamList.Length ; i++ )
			{
				if ( ParamList[i].Name == m_tableParamName )
				{
					TableParamDefinition = JobUtil.CopyParameterDefinition( ParamList[i] );
				}
			}


			Session["EditedParamList"] = TableParamDefinition;

		}

		public void CancelButton_Clicked( Object sender, System.EventArgs e)
		{
		}

		public void OkButton_Clicked( Object sender, System.EventArgs e)
		{

			// Assign the new Edited Parameters to the Original  Parameter List
			ParameterDefinition[] ParamList = null;

			if ( AcString.ToLower(m_usedForActuateQuery) == "false" )
			{

				GetReportParametersResponse ReportParameterResp = ( GetReportParametersResponse ) Session["Parameters"];
				if ( ReportParameterResp != null )
				{
					ParamList = ReportParameterResp.ParameterList;
				}
			}
			else
			{
				Query query = (Query) Session[AcParams.Name.queryObject.ToString()];

				if ( query != null )
				{
					ParamList = query.ParameterDefinitionList;
				}
			}
            
			if ( ParamList != null )
			{
				for ( int i=0; i < ParamList.Length ; i++ )
				{
					if ( ParamList[i].Name == m_tableParamName )
					{
						ParamList[i] = (ParameterDefinition) Session["EditedParamList"] ;
					}
				}
			}

			// Close teh window
			GenerateWindowCloseScript();
		}

		private string GetInsertEditBtnArgs( string mode, int RowNumber )
		{
			AcUrlBuilder Url = new AcUrlBuilder("../newrequest/TableParameterEditor.aspx" );
			Url.AddKeyValue( AcParams.Name.TableParamName, m_tableParamName );
			Url.AddKeyValue( AcParams.Name.__executableName, m_executableName  );
			Url.AddKeyValue( AcParams.Name.TableParamDisplayMode, mode );
			Url.AddKeyValue( AcParams.Name.TableParamRowNo, RowNumber.ToString());
			Url.AddKeyValue( AcParams.Name.UsedForQuery, m_usedForActuateQuery );

			StringBuilder jsArgs = new StringBuilder();
			ActuUtil.AddJsArgument( jsArgs, Url.ToString() , true);

			return jsArgs.ToString();
		}


		private string GetDeleteButtonArgs()
		{
			AcUrlBuilder args = new AcUrlBuilder("../newrequest/TableParameterDisplay.aspx");
			args.AddKeyValue( AcParams.Name.TableParamName, m_tableParamName );
			args.AddKeyValue( AcParams.Name.__executableName, m_executableName );
			args.AddKeyValue( AcParams.Name.TableParamDisplayMode, "delete" );
			args.AddKeyValue( AcParams.Name.TableParamRowNo, m_rowToBeDeleted.ToString() );


			return args.ToString();
		}

		private void GetRequiredInputParameters()
		{
			m_tableParamDisplayMode = AcParams.Get( Page, AcParams.Name.TableParamDisplayMode, "insert"  );
			m_tableParamName = AcParams.Get( Page, AcParams.Name.TableParamName, AcParams.From.Url );
			m_executableName = AcParams.Get( Page, AcParams.Name.__executableName, AcParams.From.Url );
			m_usedForActuateQuery = AcParams.Get( Page, AcParams.Name.UsedForQuery, "false", AcParams.From.Url );
			m_usedForActuateQuery = AcString.ToLower(m_usedForActuateQuery);

			if ( m_tableParamDisplayMode == null )
			{
				m_tableParamDisplayMode = "insert";
			}
			if ( AcString.ToLower(m_tableParamDisplayMode) == "edit")
			{
				//string sEditedRowNumber  = AcParams.Get( Page, AcParams.Name.TableParamRowNo, "0", AcParams.From.Url );
				string sEditedRowNumber  = AcParams.Get( Page, AcParams.Name.TableParamRowNo, AcParams.From.Session );

				try
				{
					m_tableEditedRowNumber = Convert.ToInt32( sEditedRowNumber );
				}
				catch
				{
					m_tableEditedRowNumber = 0;
				}
			}

			if ( ! IsPostBack )
			{
				ViewState["DataChanged"] = m_dataChanged;
			}

		}

		private void RenderControl()
		{
		
			//  Table Param Heading

			AcKeyArgument[] args  = new AcKeyArgument[1];
			AcKeyArgument arg = new AcKeyArgument( null, m_tableParamName );
			args[0] = arg;
			AcTableParamHeading.Text = AcResourceManager.GetString("parameters.tables.header", args , Session );

			// Detrmine if the Delete All button is to be enabled or disabled
			HtmlTableRowCollection rows = TableParamData.Rows;
			if ( rows != null )
			{
				HtmlTableCell column = rows[0].Cells[0];
				AcButtonControl  DeleteAllBtn = (AcButtonControl) column.Controls[0];
				DeleteAllBtn.Enabled = ( (m_TableValues != null && m_TableValues.Length > 0) ||  m_InsertedRecord != null ) ;
			}

			AcInsertBtnTop.Click += new System.EventHandler( InsertButton_Clicked );
			AcInsertBtnBtm.Click += new System.EventHandler( InsertButton_Clicked );

		}

		protected void EditButton_Clicked( object sender, System.EventArgs e )
		{
			m_dataChanged = true;
			ViewState["DataChanged"] = m_dataChanged;
		}


		protected void InsertButton_Clicked( object sender, System.EventArgs e )
		{
			m_dataChanged = true;
			ViewState["DataChanged"] = m_dataChanged;
		}

		private bool IsSessionTimeOut()
		{
			return false;//  ( ((basePageTemplate)Page).SessionTimedOut );
		}

		private void GenerateWindowCloseScript()
		{
			StringBuilder Script = new StringBuilder();
			Script.Append( "<script language=\"javascript\">");
			Script.Append("window.close();");
			// Reload the Parent window which will take you to the Login page
			if ( IsSessionTimeOut() && IsPostBack )
			{
				Script.Append(" if ( (window.opener != null) && ( window.opener.closed == false ) && (window.opener.document != null) && ( window.opener.document.forms[0] != null) ) { ");
				Script.Append(" window.opener.location.replace(window.opener.location.href)};");
			}
			Script.Append("</script>");
			AcLtrlJScript.Text = Script.ToString();

		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		///		Required method for Designer support - do not modify
		///		the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}
		#endregion
	}
}
