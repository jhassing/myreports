<%@ Control Language="c#" Inherits="activeportal.usercontrols.newrequest.executereport" CodeFile="executereport.ascx.cs" %>
<%@ Register TagPrefix="actuate" TagName="SaveOutputFile" Src="SaveFile.ascx" %>
<%@ Register TagPrefix="actuate" TagName="parameters" Src="../common/parameters.ascx" %>
<%@ Register TagPrefix="actuate" TagName="Schedule" Src="../common/Schedule.ascx" %>
<%@ Register TagPrefix="acwc" Namespace="actuate.classes.webcontrols" Assembly="activeportal"%>

<table width="100%" cellspacing="0" cellpadding="0" border="0"  >
	<tr valign=top>
		<td>

			<acwc:actabstrip	id="AcReportTabs"
								TabSelectedCssClass="newCellActiveTab"
								RightTabDefaultCssClass="rightCellInactiveTab" 
								RightTabSelectedCssClass="rightCellActiveTab" 
								TabDefaultCssClass="newCellInactiveTab" 
								TabLinkCssClass="tabLink" 
								TabSelectedLinkCssClass="tabLinkSelected"
								TabDisabledLinkCssClass="lnkTabDisabled"
								runat="server"
								targetid="AcMultiPage">
	  			<acwc:Tab Key="TAB_SCHEDULE" ID="ScheduleTab" RunAt="server"/>
				<acwc:TabSeparator/>
								
				<acwc:TabSeparator />
				<acwc:Tab Key="TAB_PARAMETERS" ID="ParameterTab"/>
				<acwc:TabSeparator />
				
				<acwc:TabSeparator/>
				<acwc:Tab ID="OutputTab"/>
				<acwc:TabSeparator />
			</acwc:actabstrip>
		</td>
		<%-- THE HYPERLINKS TO SWITCH BETWEEN JOB TYPES & THE SUBMIT BUTTON --%>
		<td align="right">
		  
				<asp:LinkButton ID="SimpleRequestLink" Runat="server">
				</asp:LinkButton>
			
				<asp:Literal Text="&nbsp;" Runat="server">
				</asp:Literal>
			
				<asp:LinkButton ID="BackgroundJobLnk" Runat="server">
				</asp:LinkButton>
				
				<asp:Literal Text="&nbsp;" Runat="server">
				</asp:Literal>
			
				<asp:Button ID="SubmitBtnTop" OnClick="SubmitJob_Clicked" Runat="server">
				</asp:Button>
				
		
		</td>
	</tr>
	<tr>	
		<td colspan="2">
			<table  width="100%" class="panel">		  
				<tr valign=top>
					<td>
						<acwc:acmultipage id="AcMultipage" runat="server">
							<acwc:PageView>
								<actuate:Schedule id="Schedule" runat="server" />
							</acwc:PageView>
	
							<acwc:PageView>
								<actuate:Parameters id="ReportParameters"  runat="server" />
							</acwc:PageView>
	
							<acwc:PageView>
								<actuate:SaveOutputFile id="SaveOutputFile"  runat="server" />
							</acwc:PageView>
						</acwc:acmultipage>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan=2>
			<table width="100%">
				<tr>
					<td align="right">
     					<asp:Button ID="SubmitBtnBtm" OnClick="SubmitJob_Clicked" Runat="server">
						</asp:Button>
					</td>
				</tr>
			</table>
		</td>
	</tr>	
</table>
<input type="hidden" Id="ValidJob" RunAt="server"/>
