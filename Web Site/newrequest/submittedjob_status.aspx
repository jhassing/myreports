<%@ Register TagPrefix="localized" Namespace="actuate_i18n" Assembly="actuate_i18n" %>
<%@ Register TagPrefix="template" namespace="actuate.classes.webcontrols.masterpages" assembly="activeportal" %>
<%@ Register TagPrefix="actuate" TagName="submittedjob_status" Src="submittedjob_status.ascx" %>
<%@ Page language="c#" Inherits="DistributionPortal.newrequest.submittedjob_status" CodeFile="submittedjob_status.aspx.cs" %>
<template:contentcontainer Location="NewRequests" runat="server" id="Contentcontainer1">
	<template:content id="title" runat="server">
		<localized:AcLiteral id="AcLiteral1" runat="server" Key="MSGT_BROWSER">
			<ARGUMENT key="TBAR_ACTION_STATUS" />
		</localized:AcLiteral>
	</template:content>
	<template:content id="content" runat="server">
		<actuate:submittedjob_status id="Submittedjob_status1" JobDetailTarget="../requests/detail.aspx" DeleteJobTarget="../requests/do_deletejob.aspx"
			CancelJobTarget="../requests/do_drop.aspx" RunAt="server"></actuate:submittedjob_status>
	</template:content>
</template:contentcontainer>
