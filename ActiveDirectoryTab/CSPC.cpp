// CSPC.cpp : Implementation of CSPC
#include "stdafx.h"
#include "ActuateExt.h"

#include "SCPUIGeneral.h"
#include "PernoCostCenter.h"

#include "CSPC.h"
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
#include "util.h"
#include "adsprop.h"
#include "dsclient.h"   //CFSTR_DSOBJECTNAMES


/////////////////////////////////////////////////////////////////////////////
// CSPC

STDMETHODIMP CSPC::Initialize(LPCITEMIDLIST pIDFolder, LPDATAOBJECT pDataObject, HKEY hKeyID)
{
  if (pDataObject != NULL)
  {
    //
    // Store the IDataObject in a smart pointer.
    //
    m_spDataObj = pDataObject;
  }
  else
  {
    return E_INVALIDARG;
  }
  return S_OK;
}

//
// IShellPropSheetExt interface
//
STDMETHODIMP CSPC::AddPages(LPFNADDPROPSHEETPAGE pAddPageProc, LPARAM lParam)
{
  AFX_MANAGE_STATE(AfxGetStaticModuleState());

  //
  // Unpack the data pointer and create the property page.
  // Register clipboard format
  //
  CLIPFORMAT cfDsObjectNames = (CLIPFORMAT)RegisterClipboardFormat(CFSTR_DSOBJECTNAMES);
  ASSERT(cfDsObjectNames != 0);

  FORMATETC fmte = { cfDsObjectNames, NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL};
  STGMEDIUM objMedium;

  HRESULT hr = m_spDataObj->GetData(&fmte, &objMedium);
  if (FAILED(hr))
  {
    AfxMessageBox(_T("Failed to retrieve data from medium."));
    return hr;
  }

  LPDSOBJECTNAMES pDsObjectNames;
  pDsObjectNames = (LPDSOBJECTNAMES)objMedium.hGlobal;
  if (pDsObjectNames->cItems < 1)
  {
    return ERROR_INVALID_DATA;
  }

  PWSTR pszObjADsPath = (PWSTR)BYTE_OFFSET(pDsObjectNames, pDsObjectNames->aObjects[0].offsetName);
  PWSTR pszClass = (PWSTR)BYTE_OFFSET(pDsObjectNames, pDsObjectNames->aObjects[0].offsetClass);
  TRACE(_T("Object path: %s, class: %s\n"), pszObjADsPath, pszClass);

  //
  // Create/contact the notification object.
  //
  HWND hNotifyObj;

  hr = ADsPropCreateNotifyObj(m_spDataObj, pszObjADsPath, &hNotifyObj);
  
  if (FAILED(hr))
  {
    return hr;
  }

  //
  // Create and initialize the general page
  //
  m_pProppageUIGeneral = new CSCPUIGeneral(hNotifyObj);
  m_pProppageUIGeneral->Init(pszObjADsPath, pszClass);

  m_PernoCostCenter = new PernoCostCenter(hNotifyObj);
  m_PernoCostCenter->Init(pszObjADsPath, pszClass);

  //
  // Add the general page to the sheet
  //
  ((PROPSHEETPAGE*)&m_pProppageUIGeneral->m_psp)->lParam = (LPARAM)m_pProppageUIGeneral;
  if (!(*pAddPageProc)(CreatePropertySheetPage((PROPSHEETPAGE*)&m_pProppageUIGeneral->m_psp), (LPARAM)lParam))
  {
    return E_FAIL;
  }

  ((PROPSHEETPAGE*)&m_PernoCostCenter->m_psp)->lParam = (LPARAM)m_PernoCostCenter;
  if (!(*pAddPageProc)(CreatePropertySheetPage((PROPSHEETPAGE*)&m_PernoCostCenter->m_psp), (LPARAM)lParam))
  {
    return E_FAIL;
  }
 
  return S_OK;
}

STDMETHODIMP CSPC::ReplacePage(UINT uPageID, LPFNADDPROPSHEETPAGE lpfnReplaceWith, LPARAM lParam)
{
  //
  // This method is not used.  It should only be called if the pages are an extension for the Control Panel.
  //
  return E_FAIL;
}
