//+-------------------------------------------------------------------------
//
//  Microsoft Windows
//
//  Copyright (C) Microsoft Corporation, 1999 - 1999
//
//  File:       util.h
//
//--------------------------------------------------------------------------

#ifndef _UTIL_H
#define _UTIL_H

#define BYTE_OFFSET(base, offset) (((LPBYTE)base)+offset)

HRESULT SetStringAttribute(IADs* pIADs, LPCWSTR lpszAttr, LPCWSTR lpszValue);
BOOL IsAttributeWritable(PADS_ATTR_INFO m_pWritableAttrs, LPWSTR pAttribute);




/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////


#endif // _UTIL_H
