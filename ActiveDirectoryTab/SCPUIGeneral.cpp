// SCPUIGeneral.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "SCPUIGeneral.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSCPUIGeneral property page

IMPLEMENT_DYNCREATE(CSCPUIGeneral, CPropertyPage)

CSCPUIGeneral::CSCPUIGeneral() : CPropertyPage(CSCPUIGeneral::IDD)
{
	//{{AFX_DATA_INIT(CSCPUIGeneral)
	m_szHomeFolder = _T("");
	//}}AFX_DATA_INIT
}

CSCPUIGeneral::~CSCPUIGeneral()
{
}

void CSCPUIGeneral::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSCPUIGeneral)
	DDX_Control(pDX, IDC_CHECK1, m_chkUseActuate);
	DDX_Control(pDX, IDC_homeFolder, m_txtHomeFolder);
	DDX_Text(pDX, IDC_homeFolder, m_szHomeFolder);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSCPUIGeneral, CPropertyPage)
	//{{AFX_MSG_MAP(CSCPUIGeneral)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_CHECK1, OnCheck1)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(IDC_homeFolder, OnPropertyChange)
	ON_EN_CHANGE(IDC_CHECK1, OnPropertyChange)
	

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSCPUIGeneral message handlers

BOOL CSCPUIGeneral::OnApply() 
{
	  if (!m_bDirty)
  {
    //
    // Signal the change notification. Note that the notify-apply 
    // message must be sent even if the page is not dirty so that 
    // the notify ref-counting will properly decrement.
    //
    ::SendMessage(m_hNotifyObj, WM_ADSPROP_NOTIFY_APPLY, TRUE, 0);

    return TRUE;
  }
  ASSERT(m_spDsObj != NULL);
  //
  // Retrieve the data from the UI
  //
  GetUIData();
  //
  // Seup the Attribute Info array with the appropriate values
  //
  HRESULT hr = S_OK;
  ADS_ATTR_INFO attrInfo[2];
  PWSTR rgpwzAttrNames[] = {_T("actuateRDHomeFolder"), _T("actuateUser") };
  // 
  // Prepare the ADS Value array
  //
  ADSVALUE Value[2];

  //
  // Set the update values for HomeFolder property
  //
  attrInfo[0].dwADsType = ADSTYPE_CASE_IGNORE_STRING;
  attrInfo[0].pszAttrName = rgpwzAttrNames[0];
  
  Value[0].dwType = ADSTYPE_CASE_IGNORE_STRING;
  Value[0].CaseIgnoreString = (LPWSTR)(LPCWSTR)m_szHomeFolder;

  attrInfo[0].pADsValues = &Value[0];
  attrInfo[0].dwNumValues = 1;

  if( m_szHomeFolder.IsEmpty( ) )
	  attrInfo[0].dwControlCode = ADS_ATTR_CLEAR;
  else
	  attrInfo[0].dwControlCode = ADS_ATTR_UPDATE;

  
  //
  //
  attrInfo[1].dwADsType = ADSTYPE_BOOLEAN;
  attrInfo[1].pszAttrName = rgpwzAttrNames[1];
  
  Value[1].dwType = ADSTYPE_BOOLEAN;  
  Value[1].Boolean = m_bUseActuate;

  attrInfo[1].pADsValues = &Value[1];
  attrInfo[1].dwNumValues = 1;

  attrInfo[1].dwControlCode = ADS_ATTR_UPDATE;

  //
  // Set the new values in Active Directory
  //
  DWORD dwReturned = 0;
  hr = m_spDsObj->SetObjectAttributes(attrInfo, 2, &dwReturned);
  if (FAILED(hr))
  {
    AfxMessageBox(_T("Failed to set values for object."));
    return FALSE;
  }

  if (dwReturned < 1)
  {
    AfxMessageBox(_T("Failed to set values for object."));
    return FALSE;
  }

  //
  // Signal the change notification. Note that the notify-apply 
  // message must be sent even if the page is not dirty so that 
  // the notify ref-counting will properly decrement.
  //
  ::SendMessage(m_hNotifyObj, WM_ADSPROP_NOTIFY_APPLY, TRUE, 0);
  return TRUE;

	
	//return CPropertyPage::OnApply();
}

void CSCPUIGeneral::OnDestroy() 
{
	  CPropertyPage::OnDestroy();
  //
  // Tell the notification object to shut down
  //
  ::SendMessage(m_hNotifyObj, WM_ADSPROP_NOTIFY_EXIT, 0, 0);
  delete this;


	
}

BOOL CSCPUIGeneral::OnInitDialog() 
{
	
	BOOL bRes = CPropertyPage::OnInitDialog();

	//
	// Verify that this page is only coming up for users
	//
	ASSERT(m_szClass == _T("user"));
	
	//
	// Display data in UI
	//
	SetUIData();

	
	//CEdit *pEdit1 = (CEdit *)this->GetDlgItem(IDC_homeFolder);
	m_txtHomeFolder.SetReadOnly( !m_bWriteActuateRDHomeFolder );
	

	//CButton *pButton = (CButton *)this->GetDlgItem( IDC_CHECK1 );
	m_chkUseActuate.EnableWindow ( m_bWriteActuateRDHomeFolder );
	
	//
	// The page may have become dirty due to the SetWindowText() in SetUIData(),
	// so clear the dirty flag.
	//
	m_bDirty = FALSE;
  
	return bRes;

}

void CSCPUIGeneral::GetUIData()
{

	//
	// Retrieve the data from the controls
	//
	GetDlgItem(IDC_homeFolder)->GetWindowText(m_szHomeFolder);
	
	if (m_chkUseActuate.GetCheck() == BST_CHECKED)
	{
		m_bUseActuate = TRUE;
	}
	else
	{
		m_bUseActuate = FALSE;
	}

}


void CSCPUIGeneral::SetUIData()
{

	//
	// Prime the controls with the data from the object
	//
	LPWSTR ppszAttrNames[] = { _T("actuateRDHomeFolder"), _T("actuateUser")};
	PADS_ATTR_INFO pAttrInfo = NULL;
	DWORD dwReturned = 0;
	
	//
	// Retrieve the data
	//

	HRESULT hr = m_spDsObj->GetObjectAttributes(ppszAttrNames, 2, &pAttrInfo, &dwReturned);

	if (FAILED(hr))
	{
		AfxMessageBox(_T("Failed to retrieve data for this object."));
		return;
	}
  
	//
	// Put the data in the appropriate control
	//  
	for (DWORD idx = 0; idx < dwReturned; idx++)
	{
		ASSERT(pAttrInfo != NULL);
		if(_wcsicmp(pAttrInfo[idx].pszAttrName, L"actuateRDHomeFolder") == 0)
		{
            m_szHomeFolder = pAttrInfo[idx].pADsValues->CaseIgnoreString;
            GetDlgItem(IDC_homeFolder)->SetWindowText(m_szHomeFolder);
		}
		else if(_wcsicmp(pAttrInfo[idx].pszAttrName, L"actuateUser") == 0)
		{
		
			BOOL uu = (BOOL)pAttrInfo[idx].pADsValues->CaseIgnoreString;
			
			if (uu == TRUE)
			{
				m_chkUseActuate.SetCheck(BST_CHECKED);				
			}
			else
			{
				m_chkUseActuate.SetCheck(BST_UNCHECKED);
			}
			
		}
	}
  
	//
	// Free the memory allocated by GetObjectAttributes()
	//

	FreeADsMem(pAttrInfo);

	// Set the enabled stae of the text boxes
	SetControlStates();

}

void CSCPUIGeneral::OnPropertyChange()
{

	m_bDirty = TRUE;
	//
	// Enable the apply button
	//
	SetModified(TRUE);

}

HRESULT CSCPUIGeneral::Init(PWSTR pszPath, PWSTR pszClass)
{

	//
	// Store local versions of the path and class
	//
	m_szPath = pszPath;
	m_szClass = pszClass;
	// 
	// Get the property page initialization parameters from the notify window
	//
	ADSPROPINITPARAMS InitParams = {0};
	InitParams.dwSize = sizeof(ADSPROPINITPARAMS);
	
	if (!ADsPropGetInitInfo(m_hNotifyObj, &InitParams))
	{
		return E_FAIL;
	}

	if (FAILED(InitParams.hr))
	{
		return InitParams.hr;
	}
	//
	// Store local versions of the IDirectoryObject interface pointer
	// and the writable attributes
	//
	
	m_spDsObj = InitParams.pDsObj;
	m_pWritableAttrs = InitParams.pWritableAttrs;

	m_bWriteActuateRDHomeFolder = IsAttributeWritable( m_pWritableAttrs, L"actuateRDHomeFolder" );


	return S_OK;

}


void CSCPUIGeneral::OnCheck1() 
{
	OnPropertyChange();

	SetControlStates();
}



void CSCPUIGeneral::SetControlStates()
{
	
	if (m_chkUseActuate.GetCheck() == BST_CHECKED)
	{
		
		m_txtHomeFolder.EnableWindow(TRUE);
	}
	else
	{
		m_txtHomeFolder.EnableWindow(FALSE);
		m_txtHomeFolder.SetWindowText(_T(""));
	}
}
