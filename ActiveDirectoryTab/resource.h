//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ActuateExt.rc
//
#define IDS_PROJNAME                    100
#define IDR_CSPC                        102
#define IDD_SCP_GENERAL_PAGE            107
#define IDD_PERNO_COSTCENTER            108
#define IDC_CHECK1                      203
#define IDC_homeFolder                  204
#define IDC_homeFolder2                 205
#define IDC_CostCenter                  205
#define IDC_rdoNever                    206
#define IDC_PersonnelNumber             206
#define IDC_STATICC                     208
#define IDC_STATICA                     209
#define IDC_STATICB                     210
#define IDC_STATICD                     211
#define IDC_STATICFF                    212
#define IDC_Sap_AD                      213
#define IDC_STATICD2                    214
#define IDC_STATICB2                    215

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         214
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
