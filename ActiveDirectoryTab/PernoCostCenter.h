#pragma once

#include "resource.h"
#include <atlbase.h>
#include "afxwin.h"
#include "util.h"
#include "Label.h"


// PernoCostCenter dialog

class PernoCostCenter : public CPropertyPage
{
	DECLARE_DYNAMIC(PernoCostCenter)

public:
	PernoCostCenter();
	virtual ~PernoCostCenter();
	virtual HRESULT Init(PWSTR pszPath, PWSTR pszClass);
	virtual void GetUIData();
	virtual void SetUIData();
	afx_msg void OnPropertyChange();
	virtual BOOL OnApply();

	CString m_szCostCenter;
	CString m_szPersonnelNumber;
	CString m_szSapAD;

	PernoCostCenter(HWND hNotifyObj) : CPropertyPage(IDD_PERNO_COSTCENTER), m_hNotifyObj(hNotifyObj)
	{
		m_bDirty				= FALSE;
		m_szPersonnelNumber		= _T("");
		m_szCostCenter			= _T("");
		m_szSapAD				= _T("");

	}

// Dialog Data
	enum { IDD = IDD_PERNO_COSTCENTER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

private:
	BOOL m_bDirty;
	HWND m_hNotifyObj;
	PADS_ATTR_INFO m_pWritableAttrs;
	CComPtr<IDirectoryObject> m_spDsObj;
	CString m_szClass;
	CString m_szPath;

	BOOL m_bWritePerno;
	BOOL m_bWriteCostCenter;

public:
	afx_msg void OnEnChangeEdit1();
	
	CLabel m_lblPersonnelNumber;
	CLabel m_lblCostCenter;
	CLabel m_lblPersonnelNumberMax;
	CLabel m_lblCostCenterMax;

	CLabel m_lblGroupBox;

	CText m_txtPersonnelNumber;
	CText m_txtCostCenter;
	CText m_txtSapAD;
};
