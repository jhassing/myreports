// PernoCostCenter.cpp : implementation file
//

#include "stdafx.h"
#include "ActuateExt.h"	
#include "PernoCostCenter.h"
#include ".\pernocostcenter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// PernoCostCenter dialog

IMPLEMENT_DYNAMIC(PernoCostCenter, CPropertyPage)
PernoCostCenter::PernoCostCenter() : CPropertyPage(PernoCostCenter::IDD)
, m_szPersonnelNumber(_T(""))
, m_szCostCenter(_T(""))
, m_szSapAD(_T(""))
{
	//{{AFX_DATA_INIT(CSCPUIGeneral)
	m_szPersonnelNumber = _T("");
	m_szCostCenter		= _T("");
	m_szSapAD			= _T("");
	//}}AFX_DATA_INIT
}

PernoCostCenter::~PernoCostCenter()
{
}

void PernoCostCenter::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);

	DDX_Text(pDX, IDC_CostCenter, m_szCostCenter);
	DDV_MaxChars(pDX, m_szCostCenter, 6);

	DDX_Text(pDX, IDC_PersonnelNumber, m_szPersonnelNumber);
	DDV_MaxChars(pDX, m_szPersonnelNumber, 8);

	DDX_Text(pDX, IDC_Sap_AD, m_szSapAD);
	DDV_MaxChars(pDX, m_szSapAD, 13);

	//DDX_Control(pDX, IDC_STATICA, m_lblPersonnelNumber);
	//DDX_Control(pDX, IDC_STATICB, m_lblCostCenter);
	//DDX_Control(pDX, IDC_STATICC, m_lblPersonnelNumberMax);
	//DDX_Control(pDX, IDC_STATICD, m_lblCostCenterMax);
	//DDX_Control(pDX, IDC_STATICFF, m_lblGroupBox);
	DDX_Control(pDX, IDC_PersonnelNumber, m_txtPersonnelNumber);
	DDX_Control(pDX, IDC_CostCenter, m_txtCostCenter);
	DDX_Control(pDX, IDC_Sap_AD, m_txtSapAD);
	


}

BOOL PernoCostCenter::OnApply() 
{
	  if (!m_bDirty)
  {
    //
    // Signal the change notification. Note that the notify-apply 
    // message must be sent even if the page is not dirty so that 
    // the notify ref-counting will properly decrement.
    //
    ::SendMessage(m_hNotifyObj, WM_ADSPROP_NOTIFY_APPLY, TRUE, 0);

    return TRUE;
  }
  ASSERT(m_spDsObj != NULL);
  //
  // Retrieve the data from the UI
  //
  GetUIData();
  //
  // Seup the Attribute Info array with the appropriate values
  //
  HRESULT hr = S_OK;
  ADS_ATTR_INFO attrInfo[3];
  PWSTR rgpwzAttrNames[] = {_T("perno"), _T("CostCenter"), _T("SAP-AD-Sync-Hash") };
  // 
  // Prepare the ADS Value array
  //
  ADSVALUE Value[3];

  //
  // Set the update values for Personnel Number property
  //
  attrInfo[0].dwADsType = ADSTYPE_CASE_IGNORE_STRING;
  attrInfo[0].pszAttrName = rgpwzAttrNames[0];
  
  Value[0].dwType = ADSTYPE_CASE_IGNORE_STRING;
  Value[0].CaseIgnoreString = (LPWSTR)(LPCWSTR)m_szPersonnelNumber;

  attrInfo[0].pADsValues = &Value[0];
  attrInfo[0].dwNumValues = 1;

  if( m_szPersonnelNumber.IsEmpty( ) )
	  attrInfo[0].dwControlCode = ADS_ATTR_CLEAR;
  else
	  attrInfo[0].dwControlCode = ADS_ATTR_UPDATE;

  
  //
  // Set the update values for the Cost Center
  //
  attrInfo[1].dwADsType = ADSTYPE_CASE_IGNORE_STRING;
  attrInfo[1].pszAttrName = rgpwzAttrNames[1];
  
  Value[1].dwType = ADSTYPE_CASE_IGNORE_STRING;  
  Value[1].CaseIgnoreString = (LPWSTR)(LPCWSTR)m_szCostCenter;

  attrInfo[1].pADsValues = &Value[1];
  attrInfo[1].dwNumValues = 1;

  if( m_szCostCenter.IsEmpty( ) )
	  attrInfo[1].dwControlCode = ADS_ATTR_CLEAR;
  else
	  attrInfo[1].dwControlCode = ADS_ATTR_UPDATE;

  //
  // Set the update values for the AD Sync Hash
  //
  attrInfo[2].dwADsType = ADSTYPE_CASE_IGNORE_STRING;
  attrInfo[2].pszAttrName = rgpwzAttrNames[2];
  
  Value[2].dwType = ADSTYPE_CASE_IGNORE_STRING;  
  Value[2].CaseIgnoreString = (LPWSTR)(LPCWSTR)m_szSapAD;

  attrInfo[2].pADsValues = &Value[2];
  attrInfo[2].dwNumValues = 1;

  if( m_szSapAD.IsEmpty( ) )
	  attrInfo[2].dwControlCode = ADS_ATTR_CLEAR;
  else
	  attrInfo[2].dwControlCode = ADS_ATTR_UPDATE;

  

  //
  // Set the new values in Active Directory
  //
  DWORD dwReturned = 0;
  hr = m_spDsObj->SetObjectAttributes(attrInfo, 3, &dwReturned);
  if (FAILED(hr))
  {
    AfxMessageBox(_T("Failed to set values for object."));
    return FALSE;
  }

  if (dwReturned < 1)
  {
    AfxMessageBox(_T("Failed to set values for object."));
    return FALSE;
  }

  //
  // Signal the change notification. Note that the notify-apply 
  // message must be sent even if the page is not dirty so that 
  // the notify ref-counting will properly decrement.
  //
  ::SendMessage(m_hNotifyObj, WM_ADSPROP_NOTIFY_APPLY, TRUE, 0);
  return TRUE;

	
	//return CPropertyPage::OnApply();
}

void PernoCostCenter::OnDestroy() 
{
	  CPropertyPage::OnDestroy();
  //
  // Tell the notification object to shut down
  //
  ::SendMessage(m_hNotifyObj, WM_ADSPROP_NOTIFY_EXIT, 0, 0);
  delete this;


	
}


void PernoCostCenter::GetUIData()
{

	//
	// Retrieve the data from the controls
	//
	GetDlgItem(IDC_PersonnelNumber)->GetWindowText(m_szPersonnelNumber);
	GetDlgItem(IDC_CostCenter)->GetWindowText(m_szCostCenter);	
	GetDlgItem(IDC_Sap_AD)->GetWindowText(m_szSapAD);		
}



void PernoCostCenter::SetUIData()
{

	//
	// Prime the controls with the data from the object
	//
	LPWSTR ppszAttrNames[] = { _T("perno"), _T("CostCenter"), _T("SAP-AD-Sync-Hash")};
	PADS_ATTR_INFO pAttrInfo = NULL;
	DWORD dwReturned = 0;
	
	//
	// Retrieve the data
	//

	HRESULT hr = m_spDsObj->GetObjectAttributes(ppszAttrNames, 3, &pAttrInfo, &dwReturned);

	if (FAILED(hr))
	{
		AfxMessageBox(_T("Failed to retrieve data for this object."));
		return;
	}
  
	//
	// Put the data in the appropriate control
	//  
	for (DWORD idx = 0; idx < dwReturned; idx++)
	{
		ASSERT(pAttrInfo != NULL);
		if(_wcsicmp(pAttrInfo[idx].pszAttrName, L"perno") == 0)
		{
            m_szPersonnelNumber = pAttrInfo[idx].pADsValues->CaseIgnoreString;
            GetDlgItem(IDC_PersonnelNumber)->SetWindowText(m_szPersonnelNumber);
		}
		else if(_wcsicmp(pAttrInfo[idx].pszAttrName, L"CostCenter") == 0)
		{
			m_szCostCenter = pAttrInfo[idx].pADsValues->CaseIgnoreString;
            GetDlgItem(IDC_CostCenter)->SetWindowText(m_szCostCenter);			
		}
		else if(_wcsicmp(pAttrInfo[idx].pszAttrName, L"SAP-AD-Sync-Hash") == 0)
		{
			m_szSapAD = pAttrInfo[idx].pADsValues->CaseIgnoreString;
            GetDlgItem(IDC_Sap_AD)->SetWindowText(m_szSapAD);			
		}
		
	}
  
	//
	// Free the memory allocated by GetObjectAttributes()
	//

	FreeADsMem(pAttrInfo);

}

void PernoCostCenter::OnPropertyChange()
{

	m_bDirty = TRUE;
	//
	// Enable the apply button
	//
	SetModified(TRUE);

}



HRESULT PernoCostCenter::Init(PWSTR pszPath, PWSTR pszClass)
{

	//
	// Store local versions of the path and class
	//
	m_szPath = pszPath;
	m_szClass = pszClass;
	// 
	// Get the property page initialization parameters from the notify window
	//
	ADSPROPINITPARAMS InitParams = {0};
	InitParams.dwSize = sizeof(ADSPROPINITPARAMS);
	
	if (!ADsPropGetInitInfo(m_hNotifyObj, &InitParams))
	{
		return E_FAIL;
	}

	if (FAILED(InitParams.hr))
	{
		return InitParams.hr;
	}
	//
	// Store local versions of the IDirectoryObject interface pointer
	// and the writable attributes
	//
	
	m_spDsObj = InitParams.pDsObj;
	m_pWritableAttrs = InitParams.pWritableAttrs;

	m_bWritePerno = IsAttributeWritable( m_pWritableAttrs, L"perno" );
	m_bWriteCostCenter = IsAttributeWritable( m_pWritableAttrs, L"CostCenter" );

	return S_OK;
}






BOOL PernoCostCenter::OnInitDialog() 
{
	
	BOOL bRes = CPropertyPage::OnInitDialog();

	//
	// Verify that this page is only coming up for users
	//
	ASSERT(m_szClass == _T("user"));
	
	//
	// Display data in UI
	// John
	SetUIData();

	//m_lblPersonnelNumber.SetBkColor(RGB(251,250,251));
	//m_lblCostCenter.SetBkColor(RGB(251,250,251));
	//m_lblPersonnelNumberMax.SetBkColor(RGB(251,250,251));
	//m_lblCostCenterMax.SetBkColor(RGB(251,250,251));
	//m_lblGroupBox.SetBkColor(RGB(251,250,251));

	m_txtPersonnelNumber.SetReadOnly( !m_bWritePerno );
	m_txtCostCenter.SetReadOnly( !m_bWriteCostCenter );
	m_txtSapAD.SetReadOnly( !m_bWriteCostCenter );
	

	//m_txtCostCenter.SetBkColor(RGB(251,250,251));

	//
	// The page may have become dirty due to the SetWindowText() in SetUIData(),
	// so clear the dirty flag.
	//
	m_bDirty = FALSE;
  
	return bRes;

}



BEGIN_MESSAGE_MAP(PernoCostCenter, CPropertyPage)
		//{{AFX_MSG_MAP(CSCPUIGeneral)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(IDC_PersonnelNumber, OnPropertyChange)
	ON_EN_CHANGE(IDC_CostCenter, OnPropertyChange)
	ON_EN_CHANGE(IDC_Sap_AD, OnPropertyChange)

END_MESSAGE_MAP()


// PernoCostCenter message handlers


void PernoCostCenter::OnEnChangeEdit1()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CPropertyPage::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}
