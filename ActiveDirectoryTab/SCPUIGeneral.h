#if !defined(AFX_SCPUIGENERAL_H__3E88E2D1_91FB_4737_95B4_EE612BCA1E9D__INCLUDED_)
#define AFX_SCPUIGENERAL_H__3E88E2D1_91FB_4737_95B4_EE612BCA1E9D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SCPUIGeneral.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSCPUIGeneral dialog

#include "resource.h"
#include <atlbase.h>


class CSCPUIGeneral : public CPropertyPage
{
	DECLARE_DYNCREATE(CSCPUIGeneral)

// Construction
public:
	virtual void GetUIData();
	virtual void SetUIData();
	afx_msg void OnPropertyChange();
	virtual HRESULT Init(PWSTR pszPath, PWSTR pszClass);

	CSCPUIGeneral();
	~CSCPUIGeneral();

	CSCPUIGeneral(HWND hNotifyObj) : CPropertyPage(IDD_SCP_GENERAL_PAGE), m_hNotifyObj(hNotifyObj)
	{
		m_bDirty = FALSE;
		m_bUseActuate = FALSE;
		m_szHomeFolder = _T("");
	}


// Dialog Data
	//{{AFX_DATA(CSCPUIGeneral)
	enum { IDD = IDD_SCP_GENERAL_PAGE };
	CButton	m_chkUseActuate;
	CEdit	m_txtHomeFolder;
	CString m_szHomeFolder;
	BOOL m_bUseActuate;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSCPUIGeneral)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSCPUIGeneral)
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();
	afx_msg void OnCheck1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void SetControlStates();
	BOOL m_bDirty;
	HWND m_hNotifyObj;
	PADS_ATTR_INFO m_pWritableAttrs;
	CComPtr<IDirectoryObject> m_spDsObj;
	CString m_szClass;
	CString m_szPath;

	BOOL m_bWriteActuateRDHomeFolder;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCPUIGENERAL_H__3E88E2D1_91FB_4737_95B4_EE612BCA1E9D__INCLUDED_)
