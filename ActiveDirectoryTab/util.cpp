//+-------------------------------------------------------------------------
//
//  Microsoft Windows
//
//  Copyright (C) Microsoft Corporation, 1999 - 1999
//
//  File:       util.cpp
//
//--------------------------------------------------------------------------

#include "stdafx.h"
#include "util.h"

BOOL IsAttributeWritable(PADS_ATTR_INFO m_pWritableAttrs, LPWSTR pAttribute)
{
    DWORD i;
    BOOL bFound = FALSE;
    LPWSTR pCopyIn = _wcsupr(_wcsdup( pAttribute ));
    LPWSTR pCopyAttr;
	
    if( m_pWritableAttrs ) 
    {
        PADSVALUE pVal = m_pWritableAttrs->pADsValues;
        for( i = 0;(i < m_pWritableAttrs->dwNumValues) && !bFound;i++)
        {
            pCopyAttr = _wcsupr( _wcsdup(pVal[i].CaseIgnoreString));
            if( wcscmp( pCopyAttr, pCopyIn) ) free(pCopyAttr);
            else bFound = TRUE;
        }
        free(pCopyIn);
        if( bFound ) free(pCopyAttr);
    }
    return bFound;

}

HRESULT SetStringAttribute(IADs* pIADs, LPCWSTR lpszAttr, LPCWSTR lpszValue)
{
  ASSERT(pIADs != NULL);
  ASSERT(lpszAttr != NULL);

  //
  // Allocate the string
  //
  BSTR bstrAttr = ::SysAllocString(lpszAttr);

  //
  // Imbed the value in a VARIANT
  //
  HRESULT hr = S_OK;
  CComVariant varValue;
  varValue.vt = VT_BSTR;
  varValue.bstrVal = ::SysAllocString(lpszValue);

  //
  // Set the value in the temporary object
  //
  hr = pIADs->Put(bstrAttr, varValue);

  //
  // Clean up memory
  //
  ::SysFreeString(bstrAttr);
  return hr;
}
