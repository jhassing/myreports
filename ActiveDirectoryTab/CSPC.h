// CSPC.h : Declaration of the CSPC

#ifndef __CSPC_H_
#define __CSPC_H_

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "SCPUIGeneral.h"
#include "PernoCostCenter.h"

/////////////////////////////////////////////////////////////////////////////
// CSPC
class ATL_NO_VTABLE CSPC : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CSPC, &CLSID_CSPC>,
	public IShellExtInit,
	public IShellPropSheetExt,
	public ICSPC
{
public:
	CSPC()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CSPC)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CSPC)
	COM_INTERFACE_ENTRY(ICSPC)
	COM_INTERFACE_ENTRY(IShellExtInit)
	COM_INTERFACE_ENTRY(IShellPropSheetExt)
END_COM_MAP()

// ICSPC
public:
	  //
  // IShellExtInit interface
  //
  STDMETHOD(Initialize)(LPCITEMIDLIST pIDFolder, LPDATAOBJECT pDataObject, HKEY hKeyID);

  //
  // IShellPropSheetExt interface
  //
  STDMETHOD(AddPages)(LPFNADDPROPSHEETPAGE pAddPageProc, LPARAM lParam);
  STDMETHOD(ReplacePage)(UINT uPageID, LPFNADDPROPSHEETPAGE lpfnReplaceWith, LPARAM lParam);

  //Member variables
public:
	CSCPUIGeneral    *m_pProppageUIGeneral;
	PernoCostCenter  *m_PernoCostCenter;
	CComPtr<IDataObject> m_spDataObj;


};

#endif //__CSPC_H_
