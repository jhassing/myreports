// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#if !defined(AFX_STDAFX_H__7B402335_76BC_46FD_999B_6DDAB303F3AC__INCLUDED_)
#define AFX_STDAFX_H__7B402335_76BC_46FD_999B_6DDAB303F3AC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0400
#endif
#define _ATL_APARTMENT_THREADED

#include <afxwin.h>
#include <afxdisp.h>

#include "afxdlgs.h"
#include <activeds.h>
#include <adsprop.h>
#include <dsadmin.h>
#include "util.h"
#include "SCPUIGeneral.h"

#if ( _ATL_VER >= 0x0300 )
#define _ATL_NO_UUIDOF 
#endif


#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;
#include <atlcom.h>
#include <afxdlgs.h>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__7B402335_76BC_46FD_999B_6DDAB303F3AC__INCLUDED)
