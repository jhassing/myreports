using System;
using System.Web;
using System.Web.SessionState;
using System.Threading;
using System.Globalization;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Security.Principal;
using System.Resources;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace MyReports.Common
{
	/// <summary>
	/// Summary description for AppValues.
	/// </summary>
	public class AppValues
	{

		#region Properties (from Web.config)

		#region AppName
		public static string AppName
		{
			get
			{
				return "MyReports";
			}
		}
		#endregion
		
		#region FullyQualifiedDomain
		public static string FullyQualifiedDomain
		{
			get
			{
				string sValue = System.Convert.ToString(HttpContext.Current.Cache["FullyQualifiedDomainName"]);			

				if (sValue == null || sValue == "")
				{					
					sValue = Functions.ConfigKey("General/DomainSettings", "FullyQualifiedDomainName");
					
					HttpContext.Current.Cache.Insert("FullyQualifiedDomainName", sValue);
				}

				return sValue;
			}
		}
		
		#endregion	

		#region Max LDAP Search  results

		public static int MaxLDAPsearchResuts
		{
			get
			{
				string sValue = System.Convert.ToString(HttpContext.Current.Cache["MaxLDAPsearchResuts"]);

				if (sValue == null || sValue == "")
				{					
					sValue = Functions.ConfigKey("General/DomainSettings", "MaxResultes").ToString();
					
					HttpContext.Current.Cache.Insert("MaxLDAPsearchResuts", sValue);
				}

				return System.Convert.ToInt32( sValue );
			}
		}
		
		#endregion

		#region LdapPort

		public static int LdapPort
		{
			get
			{
				string sValue = System.Convert.ToString(HttpContext.Current.Cache["LdapPort"]);

				if (sValue == null || sValue == "")
				{					
					sValue = Functions.ConfigKey("General/DomainSettings", "LdapPort").ToString();
					
					HttpContext.Current.Cache.Insert("LdapPort", sValue);
				}

				return System.Convert.ToInt32( sValue );
			}
		}
		
		#endregion

		#region Ldap Base OU

		public static string LdapBaseOU
		{
			get
			{				
				string sValue = System.Convert.ToString(HttpContext.Current.Cache["LdapBaseOU"]);

				if (sValue == null || sValue == "")
				{					
					sValue = Functions.ConfigKey("General/DomainSettings", "BaseDN").ToString();
					
					HttpContext.Current.Cache.Insert("LdapBaseOU", sValue);
				}

				return sValue;
			}
		}

		#endregion

		#region Service Account User
		public static string ServiceAccountUser
		{
			get
			{
				string sValue = System.Convert.ToString(HttpContext.Current.Cache["ServiceAccountUser"]);

				if (sValue == null || sValue == "")
				{					
					sValue = Functions.ConfigKey("General/AccessControl", "ServiceAccountUser").ToString();
					
					HttpContext.Current.Cache.Insert("ServiceAccountUser", sValue);
				}

				return sValue;

			}
		}
		#endregion

		#region Service Account Password
		public static string ServiceAccountPassword
		{
			get
			{
				string sValue = System.Convert.ToString(HttpContext.Current.Cache["ServiceAccountPassword"]);

				if (sValue == null || sValue == "")
				{					
					sValue = Functions.ConfigKey("General/AccessControl", "ServiceAccountPassword").ToString();
					
					HttpContext.Current.Cache.Insert("ServiceAccountPassword", sValue);
				}

				return sValue;

			}
		}
		#endregion

		#region Report Server name
		public static string ReportServerName
		{
			get
			{				
				return Functions.ConfigKey("ReportDistribution/ReportServer", "ReportServerName");
			}
		}

		#endregion

		#region Report Server port
		public static string ReportServerPort
		{
			get
			{				
				return Functions.ConfigKey("ReportDistribution/ReportServer", "RemoteMachinePort");
			}
		}

		#endregion

		#region SMTP

		public static string SMTP
		{
			get
			{				
				string sValue = System.Convert.ToString(HttpContext.Current.Cache["SMTP"]);

				if (sValue == null || sValue == "")
				{					
					sValue = System.Configuration.ConfigurationSettings.AppSettings["SMTP"].ToString();

					HttpContext.Current.Cache.Insert("SMTP", sValue);
				}

				return sValue;
			}
		}

		#endregion

		#region New User Email Confirmation Address
		public static string NewUserEmailConfirmationAddress
		{
			get
			{
				return Functions.ConfigKey("ReportDistribution/EmailSettings", "NewUserEmailConfirmationAddress");
			}
		}
		#endregion

		#region DATABASE CONNECTION STRINGS

		#region Base DB Settings
		private static string getBaseDBSettings(string section)
		{
			
			//string sValue = System.Convert.ToString( HttpContext.Current.Cache["DatabaseBaseString"] );

			//if (sValue == null || sValue == "")
			//{				
			string conn = "packet size=4096;persist security info=True;data source=" + Functions.ConfigKey("General/DatabaseServer", "Name") + ";";

			string userID = "user id=" + Functions.ConfigKey(section + "/Database", "UserID") + ";";
			string password = "password=" + Functions.ConfigKey(section + "/Database", "Password") + ";";
			string databaseName = "initial catalog=" + Functions.ConfigKey(section + "/Database", "DatabaseName") + ";";
				
			string sValue = conn + userID + password + databaseName;

			//	HttpContext.Current.Cache["DatabaseBaseString"] = sValue;

	
			//}

			return sValue;
		}
		#endregion

		#region MyReports database connection string
		public static string MyReports_DB_ConnectionString
		{
			get
			{					
				string sValue = System.Convert.ToString( HttpContext.Current.Cache["MyReportsDBString"] );

				if (sValue == null || sValue == "")
				{
					sValue = getBaseDBSettings("ReportDistribution");
					HttpContext.Current.Cache["MyReportsDBString"] = sValue;
				}
				  	
				return sValue;
	
			}
		}

        public static Database MyReportsDB
        {
            get { return DatabaseFactory.CreateDatabase("MyReports"); }
        }

		#endregion

		#region UserManagement database connection string
		public static string UM_DB_ConnectionString
		{
			get
			{
				string sValue = System.Convert.ToString( HttpContext.Current.Cache["UserManagementDBString"] );

				if (sValue == null || sValue == "")
				{
					sValue = getBaseDBSettings("UserManagement");
					HttpContext.Current.Cache["UserManagementDBString"] = sValue;
				}
				  	
				return sValue;
	
			}
		}
		
		#endregion

		#region Passport database connection string
		public static string Passport_DB_ConnectionString
		{
			get
			{
				string sValue = System.Convert.ToString( HttpContext.Current.Cache["PassportDBString"] );

				if (sValue == null || sValue == "")
				{
					sValue = getBaseDBSettings("Passport");
					HttpContext.Current.Cache["PassportDBString"] = sValue;
				}
				  	
				return sValue;
	
			}
		}
		
		#endregion

		#region Budget Distribution database connection string
		public static string BD_DB_ConnectionString
		{
			get
			{
				string sValue = System.Convert.ToString( HttpContext.Current.Cache["BudgetDistributionDBString"] );

				if (sValue == null || sValue == "")
				{
					sValue = getBaseDBSettings("BudgetDistribution");
					HttpContext.Current.Cache["BudgetDistributionDBString"] = sValue;
				}
				  	
				return sValue;
	
			}
		}
		#endregion

        #endregion

        #region BudgetDistribution FileLocation Folder
        public static string BudgetDistribution_FileLocation_Folder
		{
			get
			{
				return Functions.AddTrailingSlash (Functions.ConfigKey("BudgetDistribution/FileLocation", "Folder") );
			}
		}
		#endregion

		#region BudgetDistribution TempFolder Location
		public static string BudgetDistribution_TempFolder_Location
		{
			get
			{
				return Functions.AddTrailingSlash( Functions.ConfigKey("BudgetDistribution/TempFolder", "Location") );
			}
		}
		#endregion

		#region New User OU Container
		public static string NewUser_OU_Container
		{
			get
			{				
				string sValue = Functions.ConfigKey("General/DomainSettings", "OU_NewUserContainer");

				if (sValue == null || sValue == "")
				{					
					sValue = System.Configuration.ConfigurationSettings.AppSettings["OU_NewUserContainer"].ToString();

					HttpContext.Current.Cache.Insert("OU_NewUserContainer", sValue);
				}

				return sValue;
			}
		}
		#endregion

		#region New Group OU Container
		public static string NewGroup_OU_Container
		{
			get
			{				
				string sValue = Functions.ConfigKey("General/DomainSettings", "OU_NewGroupContainer");

				if (sValue == null || sValue == "")
				{					
					sValue = System.Configuration.ConfigurationSettings.AppSettings["OU_NewGroupContainer"].ToString();

					HttpContext.Current.Cache.Insert("OU_NewGroupContainer", sValue);
				}

				return sValue;
			}
		}
		#endregion

		#region Groups Allowed To Use Site
		public static string GroupsAllowedToUseSite
		{
			get
			{				
				string sValue = Functions.ConfigKey("General/AccessControl", "GroupsAllowedToUseSite");

				if (sValue == null || sValue == "")
				{					
					sValue = System.Configuration.ConfigurationSettings.AppSettings["GroupsAllowedToUseSite"].ToString();

					HttpContext.Current.Cache.Insert("GroupsAllowedToUseSite", sValue);
				}

				return sValue;
			}
		}
		#endregion

		#region Report Distribution Group Permissions
		public static string FinancialGroups
		{
			get
			{
				return Functions.ConfigKey("ReportDistribution/GroupPermissions", "FinancialReports");	
			}
		}
		public static string PayrollSalaryGroups
		{
			get
			{
				return Functions.ConfigKey("ReportDistribution/GroupPermissions", "PayrollSalary");	
			}
		}
		public static string PayrollHourlyGroups
		{
			get
			{
				return Functions.ConfigKey("ReportDistribution/GroupPermissions", "PayrollHourly");	
			}
		}

		public static string BlueBookGroups
		{
			get
			{
				return Functions.ConfigKey("ReportDistribution/GroupPermissions", "BlueBook");	
			}
		}
		#endregion

		#endregion

		#region UserName
		public static string UserName
		{

			set
			{
				HttpContext.Current.Session.Add("userName", value);
			}
		
			get
			{
				return( System.Convert.ToString(HttpContext.Current.Session["userName"]) );	
			}
		}

		#endregion

        public static ArrayList UserGroupMembership
        {
            get
            {
                return (ArrayList)HttpContext.Current.Session["Groups"];
            }

            set
            {
                HttpContext.Current.Session["Groups"] = value;

            }
        }

        public static ArrayList UserRegionalGroupMembership
        {
            get
            {
                ArrayList ary = UserGroupMembership;
                ArrayList retAry = new ArrayList();

                foreach (string group in ary)
                {
                    // does this group end in "_grp"?
                    if (group.ToLower().EndsWith("_grp"))
                    {
                        // strip off the "_grp"
                        string grp = group.Replace("_grp", "").Trim().ToLower();

                        // is the length of the new group equal to 3 characters?
                        if (grp.Length == 3)
                        {
                            retAry.Add(grp);
                        }
                    }
                }

                return retAry;

            }    
        }

        #region IsInRole

        public static bool IsInRole(string sGroup)
        {
            try
            {
                for (int x = 0; x < AppValues.UserGroupMembership.Count; x++)
                {
                    if (AppValues.UserGroupMembership[x].ToString().ToLower() == sGroup.ToLower())
                    {
                        return (true);
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return (false);
        }

        #endregion

        #region User Distinguished Name
        public static string distinguishedUserName
		{

			set
			{
				HttpContext.Current.Session.Add("distinguishedUserName", value);
			}
		
			get
			{
				return( System.Convert.ToString(HttpContext.Current.Session["distinguishedUserName"]) );	
			}
		}

		#endregion

		#region User Password
		public static string Password
		{
			set
			{
				HttpContext.Current.Session.Add("userPassword", value);
			}
		
			get
			{
				return( System.Convert.ToString(HttpContext.Current.Session["userPassword"]) );	
			}
		}
		#endregion

		#region Current OU path

		public static string CurrentOU
		{
			set
			{
				//HttpContext.Current.Session.Add("CurrentOU", value);
				HttpContext.Current.Session["CurrentOU"] = value;
			}
		
			get
			{
				if ( HttpContext.Current.Session["CurrentOU"] == null )
					HttpContext.Current.Session["CurrentOU"] = AppValues.LdapBaseOU;

				return( System.Convert.ToString(HttpContext.Current.Session["CurrentOU"]) );	
			}
		}

		#endregion

		#region Actuate Server Name

		public static string ActuateServerName
		{
			get
			{
				try
				{
					string serverName = System.Configuration.ConfigurationSettings.AppSettings["SERVER_DEFAULT"];
					serverName = serverName.ToLower();
					serverName = serverName.Replace("http://", "");
					serverName = serverName.Replace(":8000", "" );
					serverName = serverName.Trim();

					return serverName;

				}
				catch
				{
					return "Error Locating Actuate Server";
				}
			}
		}

		#endregion

	}
}
