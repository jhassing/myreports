using System;
using System.Web;
using System.Web.SessionState;
using System.Threading;
using System.Globalization;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Security.Principal;
using System.Resources;
using System.Text;

using Compass.Security.Cryptography;
using Microsoft.ApplicationBlocks.ExceptionManagement;

using Compass.Security.ActiveDirectory;
using Novell.Directory.Ldap;

namespace MyReports.Common
{	
	public class Functions
	{
		#region Encryption
		public static string Encrypt(string sValue)
		{					
			return EncryptUtility.Encrypt(sValue, true).ToString();
		}

		public static string Decrypt(string sValue)
		{
			return EncryptUtility.Decrypt(sValue, true).ToString();
		}
		#endregion

		#region Conversions
		public static Byte[] ConvertToBytes(string sValue)
		{		
			return (Encoding.ASCII.GetBytes( Encrypt( sValue )));
		}

		public static string ConvertFromBytes(object bytes)
		{
			if (bytes.Equals(System.DBNull.Value))
				return "";

			return (Decrypt( Encoding.ASCII.GetString( (Byte[])bytes)));

		}
		#endregion

		#region Config Key Settings
		public static string ConfigKey(string sSection, string sKey)
		{
			try
			{
				string sSecKey = sSection + "/" + sKey;
				string sValue = System.Convert.ToString(HttpContext.Current.Cache[sSecKey]);

				if (sValue == null || sValue == "")
				{
					NameValueCollection SimpleSettings1 = (NameValueCollection)ConfigurationSettings.GetConfig(sSection);

					sValue = SimpleSettings1.Get(sKey).ToString();

					HttpContext.Current.Cache.Insert(sSecKey, sValue);
				}

				return(sValue);
			}
			catch (Exception ex)
			{
				NameValueCollection nv = new NameValueCollection();

				nv.Add("Section", sSection);
				nv.Add("Key", sKey);				

				ExceptionManager.Publish ( new Exception("Error getting config key values", ex), nv );

				throw (ex);
			}		
		}

		#endregion

		#region Path Functions
		public static string AddTrailingSlash(string sPath)
		{
			try
			{
				if (sPath.Substring(sPath.Length-1) != @"\")
					sPath += @"\";
			}
			catch(Exception ex)
			{
				NameValueCollection nv = new NameValueCollection();
				nv.Add("Path", sPath);
				ExceptionManager.Publish( ex);	
			}

			return(sPath);
		}
		#endregion

		#region GetAttributeValue

		public static string GetAttributeValue( LdapAttributeSet ldapAttribSet, string property )
		{
			try
			{
				LdapAttribute ldapAttrib = ldapAttribSet.getAttribute( property );

				if ( ldapAttrib == null )
					return "";

				return ldapAttribSet.getAttribute( property ).StringValue;
			}
			catch( Exception ex )
			{
				throw ex;
			}
		}

		public static string GetAttributeValue( LdapAttribute ldapAttrib )
		{
			try
			{
				if ( ldapAttrib == null )
					return "";

				return ldapAttrib.StringValue;
			}
			catch( Exception ex )
			{
				throw ex;
			}
		}


		#endregion

		#region GetAttributeValueArray

		public static string[] GetAttributeValueArray( LdapAttributeSet ldapAttribSet, string property )
		{

			try
			{
				LdapAttribute ldapAttrib = ldapAttribSet.getAttribute( property );

				if ( ldapAttrib == null )
					return null;

				return ldapAttribSet.getAttribute( property ).StringValueArray;
			}
			catch( Exception ex )
			{
				throw ex;
			}
		}


		#endregion

        #region Get Temp Directory

        public static string GetTempDirectoryForThisUser()
        {
            return System.IO.Path.GetTempPath() + AppValues.UserName + "\\";
        }

        #endregion
	}
}
