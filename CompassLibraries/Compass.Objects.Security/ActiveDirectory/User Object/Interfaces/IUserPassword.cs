

namespace Compass.Security.ActiveDirectory
{
    /// <summary>
    /// Defines all the function signatures for passwords
    /// </summary>
    public interface IUserPassword
    {

        LoginResult VerifyUserLogin(string username, string pwd, string searchBase, ref string distinguishedName);
        void SetNewPassword(string domainController, string distinguishedName, string sNewPassword);
        void SetNewPassword(string distinguishedName, string sNewPassword);
    }
}
