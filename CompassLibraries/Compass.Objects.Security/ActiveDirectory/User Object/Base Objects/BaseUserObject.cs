using System;
using System.DirectoryServices;	
using System.Collections;
using System.Collections.Specialized;
using System.Text;

using Novell.Directory.Ldap;
using Novell.Directory.Ldap.Utilclass;

using Compass.Security.ActiveDirectory;

namespace Compass.Security.ActiveDirectory
{

	#region LoginResult
 
	public enum LoginResult
	{
		LOGIN_OK = 0,
		LOGIN_USER_DOESNT_EXIST,
		LOGIN_USER_ACCOUNT_INACTIVE,
		LOGIN_USER_PASSSWORD_INCORRECT,
		LOGIN_USER_PASSWORD_EXPIRED
	}

	#endregion

	#region UserStatus

	internal enum UserStatus
	{
		Enable = 544,
		Disable = 546
	}

	
	#endregion

	/// <summary>
	/// Base class for user objects
	/// </summary>
	public abstract class BaseUserObject : ADBaseObject, IUserObject
	{
		#region ~const

		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="DomainUser">Domain user with read-write domain access.</param>
		/// <param name="DomainPassword">Password for the domain user.</param>
		/// <param name="ldapHost">Name of the domain to connecto to.</param>
		/// <param name="ldapPort">Port the LDAP server is on.</param>
		public BaseUserObject( LDAPConnection ldpConnection ) : base( ldpConnection ) 
		{
		}


		#endregion

		#region Does User Exist
		/// <summary>
		/// Find out if a user is a member of the domain.
		/// </summary>
		/// <param name="samAccountName">Domain user to search for.</param>
		/// <returns>True if the user is found, false otherwise.</returns>
		public bool DoesUserExist( string distinguishedName, string searchBase )
		{		
			return (base.GetLdapEntry_UsingSamAccountName( distinguishedName, searchBase ) !=null ? true : false);			 
		}
		#endregion

		#region Password Never Expires

		#region Does Password Expire?

		/// <summary>
		/// Method will evaluate if a users password expires
		/// </summary>
		/// <param name="distinguishedName">Distinguished name of the user to identify</param>
		/// <returns>True if the password does expire, false if not.</returns>
		public bool DoesUserPasswordNeverExpires( string distinguishedName )
		{
			try
			{
				return DoesUserPasswordNeverExpires( base.FindObjectsUserAccountControl( distinguishedName ) );
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		/// <summary>
		/// Method will evaluate if a users password expires
		/// </summary>
		/// <param name="userAccountControl">User account control property of the user to identify</param>
		/// <returns>True if the password does expire, false if not.</returns>
		public bool DoesUserPasswordNeverExpires( int userAccountControl )
		{
			try
			{
				return userAccountControl == ( userAccountControl | ADS_UF_DONT_EXPIRE_PASSWD );				
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		#endregion

		#region Set User Password to expire


		public void SetUserPwdNeverExpire( string distinguishedName, bool setPwdNeverExpire )
		{
			try
			{
				SetUserPwdNeverExpire( distinguishedName, base.FindObjectsUserAccountControl( distinguishedName ), setPwdNeverExpire );	
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private void SetUserPwdNeverExpire( string distinguishedName, int UserAccountControl, bool setPwdNeverExpire )
		{
			
			try
			{
				if (setPwdNeverExpire)
					UserAccountControl = UserAccountControl | ADS_UF_DONT_EXPIRE_PASSWD;
				else
					UserAccountControl = UserAccountControl & ~ADS_UF_DONT_EXPIRE_PASSWD;							

				LdapAttribute attribute = new LdapAttribute( "userAccountControl", UserAccountControl.ToString() );

				base._conn.Modify( distinguishedName, new LdapModification(LdapModification.REPLACE, attribute) );

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		
		#endregion		

		#endregion

		#region User Cannot change password

		#region Does Password Expire?

		/// <summary>
		/// Method will evaluate if a user can change his/her password
		/// </summary>
		/// <param name="userNetworkID">Network of the user to identify</param>
		/// <returns></returns>
		public bool GetUserCannotChangePassword( string distinguishedName )
		{
			try
			{
				return GetUserCannotChangePassword( GetLdapEntry( distinguishedName ) );				
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		
		private bool GetUserCannotChangePassword( LdapEntry ldapEntry )
		{
			try
			{
				if ( ldapEntry != null )
				{				
					int val = System.Convert.ToInt32( ldapEntry.getAttribute( "userAccountControl" ).StringValue );
			
					return val == ( val | ADS_UF_PASSWD_CANT_CHANGE );
				}

				return false;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		#endregion

		#region Set User Cannot change Password

		/// <summary>
		/// Method will evaluate if a user can change his/her password
		/// </summary>
		/// <param name="distinguishedName">Distinguished Name of the user to identify</param>
		/// <returns></returns>
		public bool SetUserCannotChangePassword( string distinguishedName )
		{
			try
			{
				return SetUserCannotChangePassword( GetLdapEntry( distinguishedName ) );				
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		private bool SetUserCannotChangePassword( LdapEntry ldapEntry )
		{
			try
			{
				if ( ldapEntry != null )
				{				
					int val = System.Convert.ToInt32( ldapEntry.getAttribute( "userAccountControl" ).StringValue );
			
					return val == ( val & ADS_UF_PASSWD_CANT_CHANGE );
				}

				return false;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		#endregion

		#endregion

		#region Account Active

		#region Is User Account Active?

		/// <summary>
		/// Checks to see if the user account is active
		/// </summary>
		/// <param name="distinguishedName">Distinguished Name of the user.</param>
		/// <returns>True = active, otherwise false</returns>
		public bool IsAccountActive( string distinguishedName )
		{
			try
			{
				return IsAccountActive( base.FindObjectsUserAccountControl( distinguishedName ) );
			}
			catch(Exception ex)
			{
				throw ex;
			}

		}
  

		/// <summary>
		/// Checks to see if the user account is active
		/// </summary>
		/// <param name="userAccountControl">User account control flag.</param>
		/// <returns>True = active, otherwise false</returns>
		public bool IsAccountActive( int userAccountControl )
		{
			try
			{
				return userAccountControl == ( userAccountControl & ~ ADS_UF_ACCOUNTDISABLE );
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		#endregion

		#region Set User Account Active

		/// <summary>
		/// Function will enable the specified user.
		/// </summary>
		/// <param name="userNetworkID">Domain user to enable.</param>
		/// <param name="bEnable">True if the user is to be enabled, false if the user is to be disabled</param>
		/// <example>EnableUserAccount("acadmin", true);</example>
		public void SetUserAccountDisabled( string distinguishedName, bool bDisable )
		{
			try
			{
				SetUserAccountDisabled( distinguishedName, base.FindObjectsUserAccountControl( distinguishedName ), bDisable );
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private void SetUserAccountDisabled( string distinguishedName, int UserAccountControl, bool bDisable )
		{
			
			try
			{
				if (bDisable)
					UserAccountControl = UserAccountControl | ADS_UF_ACCOUNTDISABLE;
				else
					UserAccountControl = UserAccountControl & ~ADS_UF_ACCOUNTDISABLE;							

				LdapAttribute attribute = new LdapAttribute( "userAccountControl", UserAccountControl.ToString() );

				base._conn.Modify( distinguishedName, new LdapModification(LdapModification.REPLACE, attribute) );

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		
		#endregion		

		#endregion

		#region User Must Change Password At Next Logon ( Password Expired )

		#region Is Password expired?

		/// <summary>
		/// Method will evaluate if a user can change his/her password
		/// </summary>
		/// <param name="userNetworkID">Network of the user to identify</param>
		/// <returns></returns>
		public bool UserPasswordExpired( string distinguishedName )
		{
			try
			{
				return UserPasswordExpired( GetLdapEntry( distinguishedName ) );				
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		
		private bool UserPasswordExpired( LdapEntry ldapEntry )
		{
			try
			{
				if ( ldapEntry != null )
				{				
					int val = System.Convert.ToInt32( ldapEntry.getAttribute( "userAccountControl" ).StringValue );
			
					return val == ( val & ADS_UF_PASSWORD_EXPIRED );
				}

				return false;
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		#endregion

		#region Set User Password Expired


		public void SetUserPasswordExpired( string distinguishedName, bool setPwdNeverExpire )
		{
			try
			{
				SetUserPasswordExpired( GetLdapEntry( distinguishedName ), setPwdNeverExpire );	
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}


		private void SetUserPasswordExpired( LdapEntry ldapEntry, bool setPwdNeverExpire )
		{
			
			try
			{
				int val = System.Convert.ToInt32( ldapEntry.getAttribute( "userAccountControl" ).StringValue );

				if (setPwdNeverExpire)
					val = val | ADS_UF_PASSWORD_EXPIRED;
				else
					val = val & ~ADS_UF_PASSWORD_EXPIRED;							

				LdapAttribute attribute = new LdapAttribute( "userAccountControl", val.ToString() );

				base._conn.Modify( ldapEntry.DN, new LdapModification(LdapModification.REPLACE, attribute) );

			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		
		#endregion		

		#endregion

		#region Add New User

		public string AddUser( string samAccountName, string searchBase,	string sOU_UserContainer )
		{
			return AddUser( samAccountName, "", null, searchBase, sOU_UserContainer, true, true, false, false );
		}

		/// <summary>
		/// Adds a new user to the domain
		/// </summary>
		/// <param name="samAccountName">Sam Account name of the new user.</param>
		/// <param name="password">New users password.</param>
		/// <param name="ht">Hashtable of properties to add to the new user.</param>
		/// <param name="sOU_UserContainer">Container to add the new user into.</param>
		/// <param name="enableUser">Enable the user.</param>
		/// <param name="forceUserResetPWDnextLogin">Force the new user to reset his password at next login</param>
		/// <param name="setPasswordNeverExpire">Set the new users password to never expire.</param>
		/// <param name="editUserIfExists">Edit the user if he/she already exists in the domain.</param>
		/// <returns>True if the user was created, otherwise false of the user already exists.</returns>
		public string AddUser(	string samAccountName, 
			string password, 
			Hashtable ht,
			string searchBase,
			string sOU_UserContainer, 
			bool enableUser, 
			bool forceUserResetPWDnextLogin, 
			bool setPasswordNeverExpire, 
			bool editUserIfExists)
		{

            string distinguishedName = "";

			try
			{	
	
				LdapEntry e =  base.GetLdapEntry_UsingSamAccountName( samAccountName, searchBase );

				if ( e == null )
				{
					// User does not exist

					LdapAttributeSet attributeSet = new LdapAttributeSet();
											
					attributeSet.Add(	new LdapAttribute( "objectclass",		"user" ) );                            				                                     
					attributeSet.Add(	new LdapAttribute( "samAccountName",	samAccountName ) );
					attributeSet.Add(	new LdapAttribute( "userPrincipalName",	samAccountName + "@" + this._conn.ldapHost ) );
                    
                    if (ht.ContainsKey("displayname"))
                        distinguishedName = "cn=" + ht["displayname"].ToString().Replace(",", @"\,") + "," + sOU_UserContainer;                        
                    else
                        distinguishedName = "cn=" + samAccountName + "," + sOU_UserContainer;
     
					LdapEntry newEntry = new LdapEntry( distinguishedName, attributeSet );
								  
					base._conn.Add( newEntry );

					// Set the user properties
					SetUserProperties( distinguishedName, ht );

					// Set the account enabled
					SetUserAccountDisabled ( distinguishedName, !enableUser );

					// Set the password never expire??
					SetUserPwdNeverExpire ( distinguishedName, setPasswordNeverExpire );
												 
					if ( password.Length > 0 )
					{
						// Set the users password
						SetNewPassword( distinguishedName, password );

					}

					return distinguishedName;

				}
				else
				{
					if ( editUserIfExists )
					{
						// edit the user properties, nothing with passwords
						SetUserProperties( e.DN, ht );
					}

					return e.DN;
				}
			}
			catch(Exception ex)
			{
				throw(ex);
			}
		}



		#endregion

		#region Get User Properties

		/// <summary>
		/// Function will evaluate the domain users container, and return the requested properties.
		/// </summary>
		/// <param name="distinguishedName">Distinguished Name</param>
		/// <param name="htProperties">Hashtable collection of attributes needing values.</param>
		/// <returns>Hashtable contianing the needed values for this Object container</returns>
		/// <example>ActiveDirectoryHelper adHelper = new ActiveDirectoryHelper("schiaj02", "Test,3456", "na.m3tgDev.corp");</example>
		/// <example>Hashtable htProp = new Hashtable();</example>
		/// <example>htProp.Add("actuateRDHomeFolder","");</example>
		/// <example>htProp.Add("sn","");</example>
		/// <example>adHelper.GetUserProperties("schiaj02",  ref htProp);</example>				
		public LdapAttributeSet GetUserProperties( string distinguishedName, ArrayList aryProps )
		{
			return base.GetObjectProperties ( distinguishedName, aryProps );
		}
		

		/// <summary>
		/// Function will locate a user, and return the specified property value
		/// </summary>
		/// <param name="distinguishedName">Distinguished Name</param>
		/// <param name="propertyName"></param>
		/// <returns></returns>
		public LdapAttribute GetUserProperties( string distinguishedName, string propertyName )
		{
			return base.GetObjectProperties ( distinguishedName, propertyName );			
		}

		#endregion

		#region Group Methods

		#region Add User To Group
		/// <summary>
		/// Functon will add a list of groups to a user container
		/// </summary>
		/// <param name="userNetworkID">String: User (samAccountName)</param>
		/// <param name="groups">Arraylist of groups to make this user a member of.</param>
		/// <example>AddUserToGroup("acadmin", aryListGroups);</example>
		public void AddUserToGroup( string userDN, ArrayList groups )
		{
			ADGroup oGroups = null;

			try
			{		
				if ( userDN.Length > 0 )
				{	   

					oGroups = new ADGroup( base._conn);

					foreach (string groupDN in groups)
					{

						try
						{
							oGroups.AddUsersToGroup( groupDN, userDN );  	
						}
						catch  (Exception ex)
						{
							continue;
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
 
			finally
			{
				oGroups = null;
			}
		}

		/// <summary>
		/// Functon will add a single group to a users container
		/// </summary>
		/// <param name="userNetworkID">String: User (samAccountName)</param>
		/// <param name="groupName">Group name to add to the users account.</param>
		/// <example>AddUserToGroup("acadmin", "MyGroup");</example>
		public void AddUserToGroup( string userDN, string groupDN )
		{
			ArrayList ary = new ArrayList();
			ary.Add( groupDN );

			AddUserToGroup( userDN, ary );
		}


		#endregion

		#region Remove User From Group
		/// <summary>
		/// Functon will remove a list of groups to a user container.
		/// </summary>
		/// <param name="userNetworkID">String: User (samAccountName)</param>
		/// <param name="groups">String array of groups to remove the user from.</param>
		/// <example>RemoveUserFromGroup("acadmin", "1000_grp|10000_grp");</example>
		public void RemoveUserFromGroup( string userDistinguishedName, ArrayList groups )
		{
			ADGroup oGroups = null;

			try
			{		
				oGroups = new ADGroup( base._conn );

				foreach ( string groupDN in groups )
				{
					oGroups.RemoveUserFromGroup( groupDN, userDistinguishedName );  	
				}
			}

			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				oGroups = null;
			}
		}

		/// <summary>
		/// Removes a single user from a group container
		/// </summary>
		/// <param name="userNetworkID"></param>
		/// <param name="group"></param>
		public void RemoveUserFromGroup(string userDistinguishedName, string groupDN )
		{
			ArrayList ary = new ArrayList();
			ary.Add( groupDN );

			RemoveUserFromGroup( userDistinguishedName, ary );
		}

		#endregion

		#region Get Users Group Membership

		/// <summary>
		/// Gets a listing of all the groups a user is a member of
		/// </summary>
		/// <param name="distinguishedName">Distinguished Name of the user.</param>
		/// <param name="parms">Arraylist of parameters from each group to return.</param>
		/// <returns>Arraylist of LdapAttributeSets containing return params of each group</returns>
		public ArrayList GetUsersMembership( string userDN, ArrayList parms )
		{
			try
			{
				return base.GetObjectMembership( userDN, parms, "memberOf" );
			}
			catch (Exception ex)
			{
				throw new Exception("Error obtaining user membership. " + ex.Message);
			}
		}



		/// <summary>
		/// Gets a listing of all the groups a user is a member of
		/// </summary>
		/// <param name="distinguishedName"></param>
		/// <returns>An Arraylist of string group names and sub group names.</returns>
		public ArrayList GetUsersMembership( string userDN, bool goDown2Levels )
		{
			try
			{

				ArrayList aryReturn		= new ArrayList();
				ArrayList parms			= new ArrayList();

				LdapAttribute ldapAttrib = base.GetObjectProperties( userDN, "memberOf" );

				if ( ldapAttrib != null )
				{
					string[] groups = ldapAttrib.StringValueArray;

					foreach( string groupDN in groups )
					{				

						parms.Add( groupDN );

						if ( goDown2Levels )
						{
							ArrayList subGroups1 = GetSubGroupLevels( groupDN );

							parms.InsertRange( 0, subGroups1 );

							foreach( string group1DN in subGroups1 )
							{
								ArrayList subGroups2 = GetSubGroupLevels( group1DN );
								parms.InsertRange( 0, subGroups2 );							

								foreach( string group2DN in subGroups2 )
								{
									ArrayList subGroups3 = GetSubGroupLevels( group2DN );
									parms.InsertRange( 0, subGroups3 );							
								}
							}
						}
					}
				}

				foreach( string groupDN in parms )
				{
					aryReturn.Add( this.getGroupNameFromDN( groupDN ));
				}

				return aryReturn;

			}
			catch (Exception ex)
			{
				throw new Exception("Error obtaining user membership. " + ex.Message);
			}
		}

		private string getGroupNameFromDN( string groupDN )
		{

			string[] tmp = groupDN.Split(',');

			if ( tmp.Length > 0 )
			{
				string[] tmp2 = tmp[0].Split('=');

				if ( tmp2.Length> 0 )
				{
					return tmp2[1].Trim();
				}
			}

			return "";
		}

		private ArrayList GetSubGroupLevels( string groupDN )
		{

			ArrayList ary = new ArrayList();

			LdapAttribute ldapAttrib = GetObjectProperties( groupDN, "memberOf" );

			if ( ldapAttrib != null )
			{
				string[] groups = ldapAttrib.StringValueArray;
			
				// Loop through each
				foreach ( string subGroupDN in groups )
				{						
					ary.Add( subGroupDN );
				}
			}

			return ary;
		}


		#endregion

		#endregion

		#region Set User Properties

		/// <summary>
		/// Function will update the User with the properties listed inside the hashtable
		/// </summary>
		/// <param name="distinguishedName">User object to locate in the domain</param>
		/// <param name="htProperties"></param>
		/// <returns></returns>
		public bool SetUserProperties( string distinguishedName, Hashtable htProperties ) 
		{
			return base.SetObjectProperties( distinguishedName, htProperties );
		}


		public bool SetUserProperties( string distinguishedName, string property, string newValue )
		{
			Hashtable htProperties = new Hashtable();
			htProperties.Add( property, newValue );

			return SetUserProperties( distinguishedName, htProperties );

		}

		#endregion

		#region Find Users Objects

		/// <summary>
		/// Returns an array of User Properties matching the filter description
		/// </summary>
		/// <param name="PropertiesToSearch">Hashtable of properties to search for</param>
		/// <param name="parms">Arraylist of parameters</param>
		/// <param name="ldapSearchBase">Search base for LDAP. ex. "dc=na,dc=company,dc=corp"</param>
		/// <returns>Array of object struct types (User Defined)</returns>
		public ArrayList FindUsers(Hashtable PropertiesToSearch, ArrayList parms, string objectClassAttributeName, string[] objectClasses, string ldapSearchBase, int searhScope, int maxResults)
		{
			return base.FindObjects( PropertiesToSearch, parms,  objectClassAttributeName, objectClasses, ldapSearchBase, searhScope, maxResults);  
		}


		#endregion

		#region Delete User

		/// <summary>
		/// Method will delete a user from the domain.
		/// </summary>
		/// <param name="distinguishedName">Distinguished Name of the user to delete.</param>
		public void DeleteUser( string distinguishedName )
		{
			try
			{																
				base.DeleteObject( distinguishedName );
			}
			catch(Exception ex)
			{
				throw(ex);
			}
		}

		#endregion

		#region IUserPassword Members

		public virtual LoginResult VerifyUserLogin( string username, string pwd, string searchBase, ref string distinguishedName )
		{
			return LoginResult.LOGIN_USER_PASSSWORD_INCORRECT;

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="samAccountName"></param>
		/// <param name="sNewPassword"></param>
		public virtual void SetNewPassword( string distinguishedName, string sNewPassword )
		{
			// TODO:  Add BaseUserObject.SetNewPassword implementation
		}

		public virtual void SetNewPassword( string domainController, string distinguishedName, string sNewPassword )
		{
			// TODO:  Add BaseUserObject.SetNewPassword implementation
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="userNetworkID"></param>
		/// <returns></returns>
		public virtual bool CanUserChangePwd( string distinguishedName )
		{
			return true;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="userNetworkID"></param>
		/// <param name="setUserCanChPwd"></param>
		public virtual void SetUserCanChangePassword( string distinguishedName, bool setUserCanChPwd )
		{

		}

		#endregion
	}
}
