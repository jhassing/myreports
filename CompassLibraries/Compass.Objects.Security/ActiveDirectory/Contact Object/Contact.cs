using System;
using System.Collections;
using Novell.Directory.Ldap;


namespace Compass.Security.ActiveDirectory
{
    /// <summary>
    /// Class will handle Adding, removing, editing and finding of all groups in LDAP
    /// </summary>
    public class Contact : ADBaseObject
    {
        #region ~const

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="DomainUser">Domain user with read-write domain access.</param>
        /// <param name="DomainPassword">Password for the domain user.</param>
        /// <param name="ldapHost">Name of the domain to connecto to.</param>
        /// <param name="ldapPort">Port the LDAP server is on.</param>
        public Contact(LDAPConnection ldpConnection)
            : base(ldpConnection)
        {

        }

        #endregion

        #region Create Contact

        /// <summary>
        /// Adds a new contact to the domain
        /// </summary>
        /// <param name="cn">Container name of the new contact.</param>
        /// <param name="ht">Hashtable of properties to add to the new contact.</param>
        /// <param name="container">Container to add the new contact into.</param>
        /// <param name="editContactIfExists">Edit the user if he/she already exists in the domain.</param>
        /// <returns>True if the contact was created, otherwise false of the contact already exists.</returns>
        public bool AddContact(string cn,
            Hashtable ht,
            string container,
            bool editContactIfExists)
        {
            try
            {

                string distinguishedName = "cn=" + cn + "," + container;

                // Contact does not exist

                LdapAttributeSet attributeSet = new LdapAttributeSet();

                attributeSet.Add(new LdapAttribute("objectclass", "contact"));

                LdapEntry newEntry = new LdapEntry(distinguishedName, attributeSet);

                base._conn.Add(newEntry);

                // Set the user properties
                base.SetObjectProperties(distinguishedName, ht);

                return true;

            }

            catch (Exception ex)
            {
                throw (ex);
            }
        }

        #endregion

        #region Delete Contact

        /// <summary>
        /// Method will delete a contact from the domain.
        /// </summary>
        /// <param name="distinguishedName">Distinguished Name of the contact to delete.</param>
        public void DeleteContact(string distinguishedName)
        {
            try
            {
                base.DeleteObject(distinguishedName);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// Deletes an ArrayList of groups.
        /// </summary>
        /// <param name="aryContacts">ArrayList of contacts to delete</param>
        public void DeleteContact(ArrayList aryContacts)
        {
            try
            {
                foreach (string contact in aryContacts)
                    DeleteContact(contact);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        #endregion


    }
}
