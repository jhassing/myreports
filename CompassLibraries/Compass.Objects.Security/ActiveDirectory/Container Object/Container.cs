using System;
using System.Collections;
using Novell.Directory.Ldap;


namespace Compass.Security.ActiveDirectory
{
    /// <summary>
    /// Summary description for Container.
    /// </summary>
    public class Container : ADBaseObject
    {
        #region ~const

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="DomainUser">Domain user with read-write domain access.</param>
        /// <param name="DomainPassword">Password for the domain user.</param>
        /// <param name="ldapHost">Name of the domain to connecto to.</param>
        /// <param name="ldapPort">Port the LDAP server is on.</param>
        public Container(LDAPConnection ldpConnection)
            : base(ldpConnection)
        {

        }

        #endregion

        #region Add New Container

        public void AddNewContainer(string containerName, string baseContainer)
        {

            try
            {
                // First, find if the container already exists
                if (!DoesContainerExist(containerName, new string[] { "organizationalUnit" }, baseContainer))
                {
                    LdapAttributeSet attributeSet = new LdapAttributeSet();

                    attributeSet.Add(new LdapAttribute("objectclass", "organizationalUnit"));
                    attributeSet.Add(new LdapAttribute("ou", containerName));
                    attributeSet.Add(new LdapAttribute("name", containerName));

                    string dn = "OU=" + containerName + "," + baseContainer;
                    LdapEntry newEntry = new LdapEntry(dn, attributeSet);

                    base._conn.Add(newEntry);
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region Find Containers

        /// <summary>
        /// Returns an array of User Properties matching the filter description
        /// </summary>
        /// <param name="filter">Sam Account Name of the group to find</param>
        /// <param name="parms">Arraylist of parameters</param>
        /// <param name="ldapSearchBase">Search base for LDAP. ex. "dc=na,dc=company,dc=corp"</param>/// 
        /// <returns>Array of object struct types (User Defined)</returns>
        public ArrayList FindContainerObjects(string filter, string[] objectClasses, ArrayList parms, string ldapSearchBase, int searhScope, int maxResults)
        {


            return base.FindObjects(new Hashtable(), parms, "objectCategory", objectClasses, ldapSearchBase, searhScope, 200);
        }

        #endregion

        #region Does Container Exists

        public bool DoesContainerExist(string containerName, string[] objectClasses, string ldapSearchBase)
        {
            try
            {
                Hashtable ht = new Hashtable();
                ht.Add("OU", "=*");

                ArrayList ary = new ArrayList();
                ary.Add("ou");

                ArrayList tt = base.FindObjects(ht, ary, "objectCategory", objectClasses, ldapSearchBase, 1, 6000);

                foreach (LdapAttributeSet attribSet in tt)
                {
                    if (attribSet.getAttribute("ou").StringValue.ToLower().Equals(containerName.ToLower()))
                        return true;

                }

                return false;

                return tt.Count == 0 ? false : true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Get Container Properties

        /// <summary>
        /// Function will evaluate the container, and return the requested properties.
        /// </summary>
        /// <param name="distinguishedName">Distinguished Name</param>
        /// <param name="htProperties">Hashtable collection of attributes needing values.</param>			
        public LdapAttributeSet GetContainerProperties(string distinguishedName, ArrayList aryProps)
        {
            return base.GetObjectProperties(distinguishedName, aryProps);
        }


        /// <summary>
        /// Function will locate a user, and return the specified property value
        /// </summary>
        /// <param name="distinguishedName">Distinguished Name</param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public LdapAttribute GetContainerProperties(string distinguishedName, string propertyName)
        {
            return base.GetObjectProperties(distinguishedName, propertyName);
        }

        #endregion

    }
}
