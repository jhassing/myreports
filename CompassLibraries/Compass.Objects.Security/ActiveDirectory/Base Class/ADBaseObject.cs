using System;
using System.Collections;
using System.DirectoryServices;
using System.Text;
using ActiveDs;
using Novell.Directory.Ldap;


namespace Compass.Security.ActiveDirectory
{


    /// <summary>
    /// Base class for Users and Groups into an LDAP server
    /// </summary>
    public abstract class ADBaseObject : IDisposable
    {
        #region CONSTANTS

        /// <summary>
        /// Change Password GUID in AD
        /// </summary>
        protected const string CHANGE_PASSWORD_GUID = "{AB721A53-1E2F-11D0-9819-00AA0040529B}";

        /// <summary>
        /// The ACE is of the standard ACCESS ALLOWED type, where the ObjectType and InheritedObjectType fields are NULL. 
        /// </summary>
        protected const int ADS_ACETYPE_ACCESS_ALLOWED_OBJECT = 0x5;

        /// <summary>
        /// The ACE is of the standard system-audit type, where the ObjectType and InheritedObjectType fields are NULL. 
        /// </summary>
        protected const int ADS_ACETYPE_ACCESS_DENIED_OBJECT = 0x6;

        /// <summary>
        /// The logon script is executed. This flag does not work for the ADSI LDAP provider on either read or write operations. For the ADSI WinNT provider, this flag is read-only data, and it cannot be set for user objects. 
        /// </summary>
        protected const int ADS_UF_SCRIPT = 0X0001;

        /// <summary>
        /// The user account is disabled. 
        /// </summary>
        protected const int ADS_UF_ACCOUNTDISABLE = 0X0002;

        /// <summary>
        /// The home directory is required. 
        /// </summary>
        protected const int ADS_UF_HOMEDIR_REQUIRED = 0X0008;

        /// <summary>
        /// The account is currently locked out. 
        /// </summary>
        protected const int ADS_UF_LOCKOUT = 0X0010;

        /// <summary>
        /// No password is required. 
        /// </summary>
        protected const int ADS_UF_PASSWD_NOTREQD = 0X0020;

        /// <summary>
        /// The user cannot change the password. This flag can be read, but not set directly. For more information and a code example that shows how to prevent a user from changing the password, see User Cannot Change Password. 
        /// </summary>
        protected const int ADS_UF_PASSWD_CANT_CHANGE = 0X0040;

        /// <summary>
        /// The user can send an encrypted password. 
        /// </summary>
        protected const int ADS_UF_ENCRYPTED_TEXT_PASSWORD_ALLOWED = 0X0080;

        /// <summary>
        /// This is an account for users whose primary account is in another domain. This account provides user access to this domain, but not to any domain that trusts this domain. Also known as a local user account. 
        /// </summary>
        protected const int ADS_UF_TEMP_DUPLICATE_ACCOUNT = 0X0100;

        /// <summary>
        /// This is a default account type that represents a typical user. 
        /// </summary>
        protected const int ADS_UF_NORMAL_ACCOUNT = 0X0200;

        /// <summary>
        /// This is a permit to trust account for a system domain that trusts other domains. 
        /// </summary>
        protected const int ADS_UF_INTERDOMAIN_TRUST_ACCOUNT = 0X0800;

        /// <summary>
        /// This is a computer account for a Microsoft Windows NT Workstation/Windows 2000 Professional or Windows NT Server/Windows 2000 Server that is a member of this domain. 
        /// </summary>
        protected const int ADS_UF_WORKSTATION_TRUST_ACCOUNT = 0X1000;

        /// <summary>
        /// This is a computer account for a system backup domain controller that is a member of this domain. 
        /// </summary>
        protected const int ADS_UF_SERVER_TRUST_ACCOUNT = 0X2000;

        /// <summary>
        /// When set, the password will not expire on this account. 
        /// </summary>
        protected const int ADS_UF_DONT_EXPIRE_PASSWD = 0X10000;

        /// <summary>
        /// This is an Majority Node Set (MNS) logon account. With MNS, you can configure a multi-node Windows cluster without using a common shared disk.
        /// </summary>
        protected const int ADS_UF_MNS_LOGON_ACCOUNT = 0X20000;

        /// <summary>
        /// When set, this flag will force the user to log on using a smart card. 
        /// </summary>
        protected const int ADS_UF_SMARTCARD_REQUIRED = 0X40000;

        /// <summary>
        /// When set, the service account (user or computer account), under which a service runs, is trusted for Kerberos delegation. Any such service can impersonate a client requesting the service. To enable a service for Kerberos delegation, set this flag on the userAccountControl property of the service account. 
        /// </summary>
        protected const int ADS_UF_TRUSTED_FOR_DELEGATION = 0X80000;

        /// <summary>
        /// When set, the security context of the user will not be delegated to a service even if the service account is set as trusted for Kerberos delegation. 
        /// </summary>
        protected const int ADS_UF_NOT_DELEGATED = 0X100000;

        /// <summary>
        /// Restrict this principal to use only Data Encryption Standard (DES) encryption types for keys.
        /// </summary>
        protected const int ADS_UF_USE_DES_KEY_ONLY = 0x200000;

        /// <summary>
        /// This account does not require Kerberos preauthentication for logon.
        /// </summary>
        protected const int ADS_UF_DONT_REQUIRE_PREAUTH = 0x400000;

        /// <summary>
        /// The user password has expired. This flag is created by the system using data from the password last set attribute and the domain policy. It is read-only and cannot be set. To manually set a user password as expired, use the NetUserSetInfo function with the USER_INFO_3 (usri3_password_expired member) or USER_INFO_4 (usri4_password_expired member) structure.
        /// </summary>
        protected const int ADS_UF_PASSWORD_EXPIRED = 0x800000;

        /// <summary>
        /// The account is enabled for delegation. This is a security-sensitive setting; accounts with this option enabled should be strictly controlled. This setting enables a service running under the account to assume a client identity and authenticate as that user to other remote servers on the network.
        /// </summary>
        protected const int ADS_UF_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION = 0x1000000;

        #endregion

        #region Private member variables



        protected LDAPConnection _conn;

        /// <summary>
        /// Directory Entry object for Directory Services
        /// </summary>
        [CLSCompliant(false)]
        //	protected DirectoryEntry	_deCont				= null;

        #endregion

        #region ~const
        public ADBaseObject(LDAPConnection conn)
        {
            this._conn = conn;

        }

        #endregion

        #region Protected Methods

        #region ConnectToDirectoryEntry


        /// <summary>
        /// Method will connect to a Directory Entry object, and return that object.
        /// </summary>
        /// <param name="distinguishedName"></param>
        /// <returns>Returns a directory object in AD.</returns>
        protected DirectoryEntry ConnectToDirectoryEntry(string distinguishedName)
        {
            DirectoryEntry deCont = null;

            try
            {
                deCont = new DirectoryEntry(LDAPpath(distinguishedName), this._conn.DomainUser, this._conn.DomainPassword, AuthenticationTypes.Secure);

                return (deCont);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //deCont = null;
            }

        }


        #endregion

        #region Get LDAP Path
        /// <summary>
        /// Method will build the LDAP connection string
        /// </summary>
        /// <param name="OU"></param>
        /// <returns>Returns LDAP connection string.</returns>	
        protected string LDAPpath(string OU)
        {
            return "LDAP://" + this._conn.DomainController + "/" + OU;
        }

        #endregion

        #region Get Long From Large Integer

        /// <summary>
        /// Method will convert a long from a Large Int.
        /// </summary>
        /// <param name="Li"></param>
        /// <returns></returns>
        [CLSCompliant(false)]
        protected long GetLongFromLargeInteger(IADsLargeInteger Li)
        {
            long retval = Li.HighPart;
            retval <<= 32;
            retval |= (uint)Li.LowPart;
            return retval;
        }


        #endregion

        #region Build LDAPS earch String
        /// <summary>
        /// Builds an LDAP search query string based off the Hashtable contents
        /// </summary>
        /// <param name="paramList"></param>
        /// <returns></returns>
        protected string BuildLDAPSearchString(Hashtable paramList)
        {
            StringBuilder search = new StringBuilder();

            search.Append("(&(|");

            // Loop through the hashtable build the LDAP search string
            foreach (DictionaryEntry myDE in paramList)
            {
                search.Append("(" + myDE.Key + myDE.Value + ")");
            }

            search.Append("))");

            return search.ToString();
        }


        #endregion

        #region Find Objects

        /// <summary>
        /// Returns an array of User Properties matching the filter description
        /// </summary>
        /// <param name="PropertiesToSearch">Hashtable of properties to search for.</param>
        /// <param name="parms">Arraylist of parameters to include in search.</param>
        /// <param name="objectClasses">String array of object class in AD to search for.</param>
        /// <param name="searchBase">Search base for LDAP. ex. "dc=na,dc=company,dc=corp"</param>
        /// <param name="searchScope">One Level, base sub.</param>
        /// <param name="maxResults">Max results returned</param>
        /// <returns>Array of structs containing matching data.</returns>
        protected ArrayList FindObjects(Hashtable PropertiesToSearch, ArrayList parms, string objectClassAttributeName, string[] objectClasses, string searchBase, int searchScope, int maxResults)
        {

            ArrayList retAry = new ArrayList();
            LdapSearchConstraints con = null;
            LdapSearchResults lsc = null;

            try
            {
                // Format the search string
                string searchFilter = "";
                string objectCategory = "";

                if (objectClasses.Length == 0)
                {
                    objectCategory = "(&(" + objectClassAttributeName + "=*)";
                }
                else if (objectClasses.Length == 1)
                {
                    objectCategory = "(&(" + objectClassAttributeName + "=" + objectClasses[0].ToString() + ")";
                }
                else
                {
                    objectCategory = "(|";
                    for (int x = 0; x < objectClasses.Length; x++)
                    {
                        objectCategory += "(" + objectClassAttributeName + "=" + objectClasses[x].ToString() + ")";
                    }

                    //objectClass += ")";
                }

                if (PropertiesToSearch.Count == 0)
                    searchFilter = objectCategory + ")";
                else
                    searchFilter = objectCategory + BuildLDAPSearchString(PropertiesToSearch) + ")"; ;

                String[] attrs = null;

                if (parms != null)
                    attrs = getStringParamsArray(parms);

                con = new LdapSearchConstraints();

                // Max Search Size
                con.MaxResults = maxResults;
                con.BatchSize = 20;
                con.TimeLimit = 0;

                lsc = this._conn.Search(searchBase,
                    searchScope,
                    searchFilter,
                    attrs,
                    false,
                    con);

                while (lsc.hasMore())
                {

                    LdapEntry nextEntry = null;

                    try
                    {
                        nextEntry = lsc.next();
                    }
                    catch (LdapException e)
                    {
                        // nothing to do?
                        continue;
                    }

                    retAry.Add(nextEntry.getAttributeSet());

                }

                return retAry;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con = null;
                lsc = null;
            }
        }


        /// <summary>
        /// Returns an array of User Properties matching the filter description
        /// </summary>
        /// <param name="searchFilter">ADSI search filter.</param>
        /// <param name="parms">Arraylist of parameters to include in search.</param>
        /// <param name="objectClasses">String array of object class in AD to search for.</param>
        /// <param name="searchBase">Search base for LDAP. ex. "dc=na,dc=company,dc=corp"</param>
        /// <param name="searchScope">One Level, base sub.</param>
        /// <param name="maxResults">Max results returned</param>
        /// <returns>Array of structs containing matching data.</returns>
        protected ArrayList FindObjects(string searchFilter, ArrayList parms, string objectClassAttributeName, string[] objectClasses, string searchBase, int searchScope, int maxResults)
        {

            ArrayList retAry = new ArrayList();
            LdapSearchConstraints con = null;
            LdapSearchResults lsc = null;
            string mainFilter;

            try
            {
                // Format the search string
                string objectCategory = "";

                if (objectClasses.Length == 0)
                {
                    objectCategory = "(&(" + objectClassAttributeName + "=*)";
                    mainFilter = objectCategory + searchFilter + ")";
                }
                else if (objectClasses.Length == 1)
                {
                    objectCategory = "(&(" + objectClassAttributeName + "=" + objectClasses[0].ToString() + ")";
                    mainFilter = objectCategory + searchFilter + ")";
                }
                else
                {
                    objectCategory = "(|";
                    for (int x = 0; x < objectClasses.Length; x++)
                    {
                        objectCategory += "(" + objectClassAttributeName + "=" + objectClasses[x].ToString() + ")";
                    }

                    mainFilter = objectCategory + searchFilter + ")";
                }


                String[] attrs = null;

                if (parms != null)
                    attrs = getStringParamsArray(parms);

                con = new LdapSearchConstraints();

                // Max Search Size
                con.MaxResults = maxResults;
                con.BatchSize = 100;
                con.setProperty("PageSize", 1000);


                lsc = this._conn.Search(searchBase,
                    searchScope,
                    mainFilter,
                    attrs,
                    false,
                    con);

                while (lsc.hasMore())
                {

                    LdapEntry nextEntry = null;

                    try
                    {
                        nextEntry = lsc.next();
                    }
                    catch (LdapException e)
                    {
                        // nothing to do?
                        continue;
                    }

                    retAry.Add(nextEntry.getAttributeSet());

                }

                return retAry;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con = null;
                lsc = null;
            }
        }


        #endregion

        #endregion

        #region Get ldap Entry

        /// <summary>
        /// Gets a particular LDAP connection object
        /// </summary>
        /// <param name="distinguishedName">Distinguished Name of the object to search for.</param>
        /// <returns></returns>		
        protected LdapEntry GetLdapEntry(string distinguishedName)
        {
            try
            {

                LdapEntry e = new LdapEntry(distinguishedName);

                return e;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Get LDAP entry using the SamAccountName of the object

        protected LdapEntry GetLdapEntry_UsingSamAccountName(string samAccountName, string searchBase)
        {
            LdapSearchResults lsc = null;
            LdapEntry nextEntry = null;

            try
            {
                // Format the search string
                string searchFilter = "(&(samAccountName=" + samAccountName + "))";

                lsc = this._conn.Search(searchBase,
                    LdapConnection.SCOPE_SUB,
                    searchFilter,
                    new string[] { "distinguishedName", "userAccountControl" },
                    false);

                while (lsc.hasMore())
                {

                    nextEntry = null;

                    try
                    {
                        nextEntry = lsc.next();

                        return nextEntry;
                    }
                    catch (LdapException e)
                    {
                        // nothing to do?
                        return null;
                    }
                }

                return null;

            }
            catch (Exception ex)
            {
                return null;

            }
            finally
            {
                lsc = null;
                nextEntry = null;
            }
        }


        #endregion

        #region Find Objects User Account Control, return INT.

        /// <summary>
        /// Returns a users account control flag in AD for bitwise compare.
        /// </summary>
        /// <param name="distinguishedName">Distinguished name of the object.</param>
        /// <returns></returns>
        public int FindObjectsUserAccountControl(string distinguishedName)
        {
            LdapEntry e = null;

            try
            {
                e = this._conn.Read(distinguishedName, new string[] { "userAccountControl" });
                return System.Convert.ToInt32(e.getAttribute("userAccountControl").StringValue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                e = null;
            }
        }


        #endregion

        #region Delete Object

        protected void DeleteObject(string distinguishedName)
        {
            try
            {
                this._conn.Delete(distinguishedName);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        #endregion

        #region Get Object Membership

        /// <summary>
        /// 
        /// </summary>
        /// <param name="distinguishedName"></param>
        /// <param name="parms"></param>
        /// <param name="attrib"></param>
        /// <returns></returns>
        public ArrayList GetObjectMembership(string distinguishedName, ArrayList parms, string attrib)
        {

            ArrayList ary = new ArrayList();

            try
            {
                LdapEntry lde = GetLdapEntry(distinguishedName);

                if (lde != null)
                {
                    LdapEntry ld = this._conn.Read(distinguishedName, new string[] { attrib });

                    LdapAttribute att = ld.getAttribute(attrib);

                    if (att != null)
                    {
                        foreach (string member in att.StringValueArray)
                        {
                            // Get a handle to the group
                            LdapEntry ldapGroup = this._conn.Read(member, getStringParamsArray(parms));

                            if (ldapGroup != null)
                            {
                                LdapAttributeSet attributeSet = ldapGroup.getAttributeSet();

                                ary.Add(attributeSet);
                            }
                        }
                    }
                }

                return ary;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Get Object Properties

        protected LdapAttributeSet GetObjectProperties(string distinguishedName, ArrayList parms)
        {

            LdapEntry ldapEntry;

            try
            {

                ldapEntry = this._conn.Read(distinguishedName, this.getStringParamsArray(parms));

                return ldapEntry.getAttributeSet();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                ldapEntry = null;
            }
        }


        protected LdapAttribute GetObjectProperties(string distinguishedName, string propertyName)
        {
            ArrayList ht = new ArrayList();
            ht.Add(propertyName);

            LdapAttributeSet attribSet = GetObjectProperties(distinguishedName, ht);

            return attribSet.getAttribute(propertyName);

        }


        #endregion

        #region Does User Belong To Group

        public bool DoesUserBelongToGroup(string groupDN, string userDN, string[] existingMembers)
        {

            foreach (string groupMembersDN in existingMembers)
            {
                if (groupMembersDN.ToLower().Equals(userDN.ToLower()))
                    return true;
            }

            return false;
        }


        #endregion

        #region Set Object Properties

        protected bool SetObjectProperties(string distinguishedName, Hashtable htProperties)
        {
            bool bRetVal = false;
            LdapEntry ldapEntry = null;

            try
            {

                if (htProperties != null)
                {
                    ldapEntry = GetLdapEntry(distinguishedName);

                    if (ldapEntry != null)
                    {
                        IDictionaryEnumerator myEnumerator = htProperties.GetEnumerator();
                        ArrayList modList = new ArrayList();

                        while (myEnumerator.MoveNext())
                        {
                            string sField = myEnumerator.Key.ToString();

                            if ((sField.ToLower() != "password") && (sField.ToLower() != "groups"))
                            {
                                string sValue = myEnumerator.Value.ToString().Trim();

                                if (sValue.Length == 0)
                                {
                                    //sValue = " ";
                                    LdapAttribute attribute = new LdapAttribute(sField);

                                    modList.Add(new LdapModification(LdapModification.DELETE, attribute));

                                }
                                else
                                {

                                    LdapAttribute attribute = new LdapAttribute(sField, sValue);

                                    modList.Add(new LdapModification(LdapModification.REPLACE, attribute));
                                }
                            }
                        }

                        LdapModification[] mods = new LdapModification[modList.Count];
                        mods = (LdapModification[])modList.ToArray(typeof(LdapModification));

                        this._conn.Modify(ldapEntry.DN, mods);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                ldapEntry = null;
            }

            return (bRetVal);
        }


        #endregion

        #region Redim array

        /// <summary>
        /// Redim an array in C#
        /// </summary>
        /// <param name="origArray"></param>
        /// <param name="desiredSize"></param>
        /// <returns></returns>
        protected Array Redim(Array origArray, Int32 desiredSize)
        {

            // Determine the types of each element
            Type t = origArray.GetType().GetElementType();

            // Construct a new array with the desired number of elements
            // Each element is the same type as was in the original array
            Array newArray = Array.CreateInstance(t, desiredSize);

            // Copy the element from the original array in to the new array
            Array.Copy(origArray, 0,
                newArray, 0, Math.Min(origArray.Length, desiredSize));

            // Return the new array
            return newArray;
        }


        #endregion


        #region Get Objects Distinguished Name

        /// <summary>
        /// Locate an LDAP object inside the domain based on the SamAccoutName attribute and return its Distingusihed Name.
        /// </summary>
        /// <param name="samAccountName">samAccountName of the object to locate.</param>
        /// <param name="searchBase">Base starting point in AD</param>
        /// <returns>Objects Distinguished Name or a blank string</returns>
        public string GetObjectsDistinguishedName(string samAccountName, string searchBase)
        {
            try
            {
                LdapEntry ldapEntry = GetLdapEntry_UsingSamAccountName(samAccountName, searchBase);
                return ldapEntry != null ? ldapEntry.DN : "";
            }
            catch (Exception ex)
            {
                throw ex;

            }
            finally
            {

            }

        }
        #endregion

        private string[] getStringParamsArray(ArrayList parms)
        {
            String[] attrs = new String[parms.Count];

            for (int x = 0; x < parms.Count; x++)
                attrs[x] = parms[x].ToString();

            return attrs;
        }

        #region IDisposable Members

        /// <summary>
        ///  Dispose method called when finished.
        /// </summary>
        public void Dispose()
        {
            this._conn.Disconnect();
        }

        #endregion
    }
}
