using System;
using System.Collections;
using Novell.Directory.Ldap;


namespace Compass.Security.ActiveDirectory
{
    /// <summary>
    /// Class will handle Adding, removing, editing and finding of all groups in LDAP
    /// </summary>
    public class ADGroup : ADBaseObject
    {
        #region ~const

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="DomainUser">Domain user with read-write domain access.</param>
        /// <param name="DomainPassword">Password for the domain user.</param>
        /// <param name="ldapHost">Name of the domain to connecto to.</param>
        /// <param name="ldapPort">Port the LDAP server is on.</param>
        public ADGroup(LDAPConnection ldpConnection)
            : base(ldpConnection)
        {

        }

        #endregion

        #region CreateGroup

        /// <summary>
        /// Method will create a new group in the domain
        /// </summary>
        /// <param name="samAccountName">samAccountName of the new group to create.</param>
        /// <param name="description">description of the new group to create.</param>
        /// <param name="container">OU location inside the domain where to create the new group.</param>
        /// <returns>True if worked, otherwise false.</returns>
        public bool CreateGroup(string samAccountName, string description, string container)
        {
            try
            {

                LdapAttributeSet attributeSet = new LdapAttributeSet();

                attributeSet.Add(new LdapAttribute("objectclass", "group"));
                attributeSet.Add(new LdapAttribute("samAccountName", samAccountName));

                if (description.Length > 0)
                    attributeSet.Add(new LdapAttribute("description", description));

                //long n = groupType + groupScope;

                // Define the bitwise operation for the Group type
                //attributeSet.Add(	new LdapAttribute( "groupType",			n.ToString() ) );  

                string dn = "cn=" + samAccountName + "," + container;
                LdapEntry newEntry = new LdapEntry(dn, attributeSet);

                base._conn.Add(newEntry);

                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        #endregion

        #region Modify Group

        /// <summary>
        /// Method will Modify a group description in the domain
        /// </summary>
        /// <param name="samAccountName">samAccountName of the group to modify.</param>
        /// <param name="description">description of the group to edit.</param>
        /// <returns>True if worked, otherwise false.</returns>
        public bool ModifyGroup(string distinguishedName, string description, int groupScope, long groupType)
        {
            try
            {

                ModifyGroup(distinguishedName, description);

                long n = groupType + groupScope;

                // Define the bitwise operation for the Group type
                LdapAttribute attribute1 = new LdapAttribute("groupType", n.ToString());

                base._conn.Modify(distinguishedName, new LdapModification(LdapModification.REPLACE, attribute1));


                // finally, return true
                return true;


            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// Method will modify a group description
        /// </summary>
        /// <param name="distinguishedName">Account to modify</param>
        /// <param name="description">New description</param>
        /// <returns></returns>
        public bool ModifyGroup(string distinguishedName, string description)
        {
            try
            {
                // Add a new value to the description attribute
                if (description.Length > 0)
                {
                    LdapAttribute attribute = new LdapAttribute("description", description);

                    base._conn.Modify(distinguishedName, new LdapModification(LdapModification.REPLACE, attribute));
                }

                return true;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        #endregion

        #region Delete Group

        /// <summary>
        /// Method will delete a group from the domain.
        /// </summary>
        /// <param name="distinguishedName">Distinguished Name of the group to delete.</param>
        public void DeleteGroup(string distinguishedName)
        {

            try
            {
                base.DeleteObject(distinguishedName);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// Deletes an ArrayList of groups.
        /// </summary>
        /// <param name="aryGroups">ArrayList of groups to delete</param>
        public void DeleteGroup(ArrayList aryGroups)
        {
            try
            {
                foreach (string group in aryGroups)
                    DeleteGroup(group);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        #endregion

        #region Find Group Objects

        /// <summary>
        /// Returns an array of User Properties matching the filter description
        /// </summary>
        /// <param name="filter">Sam Account Name of the group to find</param>
        /// <param name="parms">Arraylist of parameters</param>
        /// <param name="ldapSearchBase">Search base for LDAP. ex. "dc=na,dc=company,dc=corp"</param>/// 
        /// <returns>Array of object struct types (User Defined)</returns>
        public ArrayList FindGroups(string searchFilter, string ldapSearchBase, int searhScope, int maxResults)
        {
            return FindGroups(searchFilter, null, ldapSearchBase, searhScope, maxResults);
        }

        /// <summary>
        /// Returns an array of User Properties matching the filter description
        /// </summary>
        /// <param name="filter">Sam Account Name of the group to find</param>
        /// <param name="parms">Arraylist of parameters</param>
        /// <param name="ldapSearchBase">Search base for LDAP. ex. "dc=na,dc=company,dc=corp"</param>/// 
        /// <returns>Array of object struct types (User Defined)</returns>
        public ArrayList FindGroups(Hashtable filter, ArrayList parms, string ldapSearchBase, int searhScope, int maxResults)
        {
            return base.FindObjects(filter, parms, "objectCategory", new string[] { "group" }, ldapSearchBase, searhScope, maxResults);
        }

        public ArrayList FindGroups(string searchFilter, ArrayList parms, string ldapSearchBase, int searhScope, int maxResults)
        {
            // Create the hashtable for the samAccountName to search for
            //Hashtable ht = new Hashtable();
            //ht.Add( "samAccountName", "=" + searchFilter );

            return base.FindObjects(searchFilter, parms, "objectCategory", new string[] { "group" }, ldapSearchBase, searhScope, maxResults);
        }


        #endregion

        #region Get Group Membership
        /// <summary>
        /// Find all the members that this particular group has assosiated with it.
        /// </summary>
        /// <param name="distinguishedName">Domain User.</param>
        /// <returns>ContainerObject array containing the user membership groups.</returns>
        /// <example>UsersMembership("acadmin");</example>
        public ArrayList GetGroupMembership(string distinguishedName, ArrayList parms)
        {
            try
            {
                return base.GetObjectMembership(distinguishedName, parms, "member");
            }
            catch (Exception ex)
            {
                throw new Exception("Error obtaining group membership. " + ex.Message);
            }
        }

        #endregion

        #region Get Group Properties

        public LdapAttributeSet GetGroupProperties(string distinguishedName, ArrayList aryProps)
        {
            return base.GetObjectProperties(distinguishedName, aryProps);
        }


        /// <summary>
        /// Function will locate a user, and return the specified property value
        /// </summary>
        /// <param name="distinguishedName">Distinguished Name</param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public LdapAttribute GetGroupProperties(string distinguishedName, string propertyName)
        {
            return base.GetObjectProperties(distinguishedName, propertyName);
        }


        #endregion

        #region Set Group Properties

        /// <summary>
        /// Function will update the Group with the properties listed inside the hashtable
        /// </summary>
        /// <param name="distinguishedName">Group object to locate in the domain</param>
        /// <param name="htProperties"></param>
        /// <returns></returns>
        public bool SetGroupProperties(string distinguishedName, Hashtable htProperties)
        {
            return base.SetObjectProperties(distinguishedName, htProperties);
        }


        public bool SetGroupProperties(string distinguishedName, string property, string newValue)
        {
            Hashtable htProperties = new Hashtable();
            htProperties.Add(property, newValue);

            return SetGroupProperties(distinguishedName, htProperties);

        }

        #endregion

        #region AddUsersToGroup

        /// <summary>
        /// Adds a single user to a single group
        /// </summary>
        /// <param name="groupName">Groups samAccountName</param>
        /// <param name="user">User inside the domain to add to the group</param>
        public void AddUsersToGroup(string groupDN, string userDistinguishedName)
        {
            ArrayList ary = new ArrayList();
            ary.Add(userDistinguishedName);

            AddUsersToGroup(groupDN, ary);
        }


        /// <summary>
        /// Adds multiple users to a single group
        /// </summary>
        /// <param name="distinguishedName">Group samAccountName inwhich we will add the users too.</param>
        /// <param name="aryUserListing">Multiple user listing</param>
        public void AddUsersToGroup(string groupDN, ArrayList aryUserListing)
        {
            bool doUpdate = false;
            ArrayList modList = new ArrayList();

            try
            {
                // Get the existing users already in this group
                LdapAttribute ldapAttrib = GetGroupProperties(groupDN, "member");

                if (ldapAttrib == null)
                {
                    // no users have ever been added to this group
                    foreach (string userDN in aryUserListing)
                    {
                        LdapAttribute member = new LdapAttribute("member", userDN);
                        modList.Add(new LdapModification(LdapModification.ADD, member));

                        doUpdate = true;
                    }
                }
                else
                {
                    string[] existingMembers = ldapAttrib.StringValueArray;

                    foreach (string userDN in aryUserListing)
                    {
                        if (!DoesUserBelongToGroup(groupDN, userDN, existingMembers))
                        {
                            LdapAttribute member = new LdapAttribute("member", userDN);
                            modList.Add(new LdapModification(LdapModification.ADD, member));

                            doUpdate = true;
                        }
                    }
                }

                if (doUpdate)
                {
                    LdapModification[] mods = new LdapModification[modList.Count];

                    Type mtype = Type.GetType("Novell.Directory.LdapModification");
                    mods = (LdapModification[])modList.ToArray(typeof(LdapModification));

                    base._conn.Modify(groupDN, mods);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        #endregion

        #region RemoveUserFromGroup

        /// <summary>
        /// Removes a single user from a group
        /// </summary>
        /// <param name="groupDN">Groups DN</param>
        /// <param name="userDistinguishedName">User inside the domain to remove from the group</param>
        public void RemoveUserFromGroup(string groupDN, string userDistinguishedName)
        {
            ArrayList ary = new ArrayList();
            ary.Add(userDistinguishedName);

            RemoveUserFromGroup(groupDN, ary);
        }


        /// <summary>
        /// Removes multiple users to a single group
        /// </summary>
        /// <param name="groupDN">Group DN inwhich we will remove the users from.</param>
        /// <param name="aryUserListing">Multiple user listing</param>
        public void RemoveUserFromGroup(string groupDN, ArrayList aryUserListing)
        {
            try
            {
                bool doUpdate = false;
                ArrayList modList = new ArrayList();

                string[] existingMembers = GetGroupProperties(groupDN, "member").StringValueArray;

                foreach (string userDN in aryUserListing)
                {

                    if (DoesUserBelongToGroup(groupDN, userDN, existingMembers))
                    {
                        LdapAttribute member = new LdapAttribute("member", userDN);
                        modList.Add(new LdapModification(LdapModification.DELETE, member));

                        doUpdate = true;
                    }
                }

                if (doUpdate)
                {
                    LdapModification[] mods = new LdapModification[modList.Count];

                    Type mtype = Type.GetType("Novell.Directory.LdapModification");
                    mods = (LdapModification[])modList.ToArray(typeof(LdapModification));

                    base._conn.Modify(groupDN, mods);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }


        #endregion

        #region Rename Group

        public void RenameGroup(string distinguishedName, string newName, string container)
        {
            string _newName = newName;

            try
            {
                if (!newName.ToLower().StartsWith("cn="))
                {
                    _newName = "cn=" + _newName;
                }

                base._conn.Rename(distinguishedName, _newName, container, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RenameGroup(string distinguishedName, string newName)
        {
            string _container;

            try
            {
                // split the container out first
                int tt = distinguishedName.IndexOf(',');
                _container = distinguishedName.Substring(tt + 1, distinguishedName.Length - tt - 1);

                RenameGroup(distinguishedName, newName, _container);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
