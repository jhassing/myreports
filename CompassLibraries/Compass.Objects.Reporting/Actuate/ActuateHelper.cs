using System;
using System.Collections;
using System.IO;
using Server_Proxy;
using Server_Proxy.localhost;

namespace Compass.Reporting.Actuate
{
    public class ActuateHelper : IDisposable
    {
        protected ActuateAPI LProxy;

        #region  enum ActuateFileType

        public enum ActuateFileType
        {
            All = 0,
            Directory = 1,
            Roi = 2,
            Rox = 3,
            Pdf = 4
        }

        #endregion

        #region Login Actuate

        /// <summary>
        /// Logs into the actuate server
        /// </summary>
        /// <param name="sUserName">User name to log in with.</param>
        /// <param name="sPassword">Password to login with.</param>
        /// <param name="sUrl">Server to log into.</param>
        /// <example>base.LoginActuate("acadmin", "password", "Http://Reportserver:8000");</example>
        protected virtual void LoginActuate(string sUserName, string sPassword, string sUrl)//, out ActuateAPI proxy)
        {

            //ActuateAPI proxy;
            //	Create an instance of server proxy. Construct an empty header.
            LProxy = new ActuateAPI {Url = sUrl, HeaderValue = new Header()};

            //	Prepare Login reqest parameters
            Login lLoginReq = new Login {User = sUserName, Password = sPassword};

            //	Send Login reqest, handle SOAP exception if any
            LoginResponse lLoginRes = LProxy.login(lLoginReq);

            //	Store authentication id
            LProxy.HeaderValue.AuthId = lLoginRes.AuthId;

        }

        public void Logout()
        {
            LProxy.Dispose();
        }

        #endregion

        #region ActuateFileAttributes[] GetDirectoryContents

        protected GetFolderItemsResponse GetDirectoryContents(string sDir, ActuateFileType enumFileType, string sFilter)
        {
            return (GetDirectoryContents(sDir, enumFileType, sFilter, false));
        }

        protected GetFolderItemsResponse GetDirectoryContents(string sDir, ActuateFileType enumFileType)
        {
            return (GetDirectoryContents(sDir, enumFileType, "*", false));
        }

        protected GetFolderItemsResponse GetDirectoryContents(string sDir, ActuateFileType enumFileType, bool bLatestVersion)
        {
            return (GetDirectoryContents(sDir, enumFileType, "*", bLatestVersion));
        }

        protected GetFolderItemsResponse GetDirectoryContents(string sDir, string sFilter)
        {
            return (GetDirectoryContents(sDir, ActuateFileType.All, sFilter, false));
        }

        protected GetFolderItemsResponse GetDirectoryContents(string sDir, string sFilter, bool bLatestVersion)
        {
            return (GetDirectoryContents(sDir, ActuateFileType.All, sFilter, bLatestVersion));
        }

        protected GetFolderItemsResponse GetDirectoryContents(string sDir, bool bLatestVersion)
        {
            return (GetDirectoryContents(sDir, ActuateFileType.All, "*", bLatestVersion));
        }

        protected GetFolderItemsResponse GetDirectoryContents(string sDir)
        {
            return (GetDirectoryContents(sDir, ActuateFileType.All, "*", false));
        }

        protected GetFolderItemsResponse GetDirectoryContents(string sDir, ActuateFileType enumFileType,
                                                                string sFilter, bool bLatestVersion)
        {
            // Prepare GetFolderItems request
            GetFolderItems lReq = new GetFolderItems
                                       {
                                           ItemElementName = ItemChoiceType36.FolderName,
                                           Item = sDir,
                                           LatestVersionOnly = bLatestVersion,
                                           LatestVersionOnlySpecified = true,
                                           ResultDef = new string[7]
                                       };
            lReq.ResultDef[0] = "FileType";
            lReq.ResultDef[1] = "Description";
            lReq.ResultDef[2] = "Owner";
            lReq.ResultDef[3] = "TimeStamp";
            lReq.ResultDef[4] = "Version";
            lReq.ResultDef[5] = "PageCount";
            lReq.ResultDef[6] = "UserPermissions";

            //	Prepare the search critaria: set fetch size = 2. So server will
            // return 2 items a time in loop until all itemes are listed.
            lReq.Search = new FileSearch();
            FileCondition[] lSearchCondition = new FileCondition[2];

            lSearchCondition[0] = new FileCondition {Field = FileField.FileType};
            string sFileType = "*";

            switch (enumFileType)
            {
                case ActuateFileType.All:
                    sFileType = "*";
                    break;

                case ActuateFileType.Directory:
                    sFileType = "Directory";
                    break;

                case ActuateFileType.Roi:
                    sFileType = "ROI";
                    break;

                case ActuateFileType.Rox:
                    sFileType = "ROX";
                    break;

            }

            lSearchCondition[0].Match = sFileType;

            lSearchCondition[1] = new FileCondition {Field = FileField.Name, Match = FormatSearchFile(sFilter)};

            lReq.Search.Item = lSearchCondition;
            lReq.Search.FetchSize = 20000;
            lReq.Search.FetchSizeSpecified = true;
            lReq.Search.CountLimit = -1; // count all items
            lReq.Search.CountLimitSpecified = true;
            
            return LProxy.getFolderItems(lReq);

        }


        #endregion

        #region Rename File or Folder.

        /// <summary>
        /// Rename the file/folder in Actuate to a new name
        /// </summary>
        /// <param name="sOldFolderName">Full path to the original folder name.</param>
        /// <param name="sNewFolderName">Full path including the new folder name.</param>
        /// <example>RenameFileFolder("/(1) Accounting/cho - Mid Atlantic Region", "/(1) Accounting/cho - South Atlantic Region")</example>
        protected AdministrateResponse RenameFileFolder(string sOldFolderName, string sNewFolderName)
        {

            MoveFile moveFile = new MoveFile
                                    {
                                        Target = sNewFolderName,
                                        ItemElementName = ItemChoiceType23.WorkingFolderName,
                                        Item = sOldFolderName,
                                        Item1ElementName = Item1ChoiceType12.Name,
                                        Item1 = sOldFolderName,
                                        ReplaceExisting = false
                                    };

            AdminOperation[] adm = new AdminOperation[1];
                adm[0] = new AdminOperation {Item = moveFile};

            return LProxy.administrate(adm);
        }

        #endregion

        #region Delete File or Folder

        /// <summary>
        /// Delete the specified file/folder in Actuate.
        /// </summary>
        /// <param name="sFolderPath">Full path to the folder name.</param>
        /// <param name="sFileFolderToDelete">File/Folder name you wish to delete.</param>
        /// <example>DeleteFileFolder("/(1) Accounting/cho - Mid Atlantic Region", "/My Report.ROI")</example>
        protected AdministrateResponse DeleteFileFolder(string sFolderPath, string sFileFolderToDelete)
        {

            // Base folder to look in.
            DeleteFile deleteFile = new DeleteFile();
            FileSearch fsearch = new FileSearch();
            FileCondition fcond = new FileCondition
                                      {
                                          Field = FileField.Name,
                                          Match = FormatSearchFile(sFileFolderToDelete)
                                      };

            // File/Folder to delete				
            deleteFile.Recursive = true;
            deleteFile.RecursiveSpecified = true;

            deleteFile.IgnoreMissing = false;
            deleteFile.IgnoreMissingSpecified = true;

            fsearch.Item = fcond;

            deleteFile.Item = sFolderPath;
            deleteFile.ItemElementName = ItemChoiceType22.WorkingFolderName;
            deleteFile.Item1 = fsearch;
            deleteFile.Item1ElementName = Item1ChoiceType11.Search;

            AdminOperation[] adminOperation = new AdminOperation[1];
            AdminOperation aOperation = new AdminOperation {Item = deleteFile};

            adminOperation[0] = aOperation;

            return LProxy.administrate(adminOperation);
        }

        #endregion

        #region Does File Exist

        /// <summary>
        /// Looks into the specified directory, trys to see if a particual file does in fact exist
        /// </summary>
        /// <param name="sFolderPath">Folder location to search</param>
        /// <param name="sFileFolderToFind">File or folder to search for.</param>
        /// <param name="latestVersionOnly">Latest version only, or all versions.</param>
        /// <param name="searchRecursive">Search recursive or sub folders.</param>
        /// <returns>True or False based on the search results</returns>
        /// <example>DoesFileExist("(1) Accounting/", "My Report.roi", false, false)</example>
        protected SelectFilesResponse DoesFileExist(string sFolderPath, string sFileFolderToFind, bool latestVersionOnly, bool searchRecursive)
        {

            SelectFiles selectFiles = new SelectFiles
                                          {
                                              Item = sFolderPath,
                                              ItemElementName = ItemChoiceType35.WorkingFolderName,
                                              LatestVersionOnly = latestVersionOnly,
                                              LatestVersionOnlySpecified = true,
                                              Recursive = searchRecursive,
                                              RecursiveSpecified = true,
                                              ResultDef = new string[1]
                                          };

            // Folder to search in
            selectFiles.ResultDef[0] = "TimeStamp";

            //	Prepare search criteria
            FileSearch fileSearch = new FileSearch();
            FileCondition fileCondition = new FileCondition
                                              {
                                                  Field = FileField.Name,
                                                  Match = FormatSearchFile(sFileFolderToFind)
                                              };
            // File to search for
            fileSearch.Item = fileCondition;
            fileSearch.CountLimit = -1; // count all objects in query
            fileSearch.CountLimitSpecified = true;

            selectFiles.Item1 = fileSearch;
            selectFiles.Item1ElementName = Item1ChoiceType17.Search;

            return LProxy.selectFiles(selectFiles);

        }

        #endregion

        #region Create Folder

        protected AdministrateResponse CreateFolder(string sLocationToCreateFolder, string sFolderToCreate)
        {
            return CreateFolder(sLocationToCreateFolder, sFolderToCreate, "");
        }

        /// <summary>
        /// Create folder inside Actuate
        /// </summary>
        /// <param name="sLocationToCreateFolder">Full path to the location in which we will add the new folder.</param>
        /// <param name="sFolderToCreate">New folder name to be created.</param>
        /// <param name="description"></param>
        /// <example>CreateFolder("/(1) Accounting/cho - Mid Atlantic Region", "/My New Folder")</example>
        protected AdministrateResponse CreateFolder(string sLocationToCreateFolder, string sFolderToCreate, string description)
        {

            CreateFolder createFolder = new CreateFolder
                               {
                                   FolderName = sLocationToCreateFolder + sFolderToCreate,
                                   IgnoreDup = true,
                                   IgnoreDupSpecified = true,
                                   ItemElementName = ItemChoiceType21.WorkingFolderName,
                                   Item = "/",
                                   Description = description
                               };

            AdminOperation[]adm = new AdminOperation[1];
            adm[0] = new AdminOperation {Item = createFolder};

            return LProxy.administrate(adm);

        }

        #endregion

        #region Copy File/Folder

        protected AdministrateResponse CopyFileFolder(string sSourceFolder, string sSourceFileFolderToMove, string sTarget, bool replaceExistingReport)
        {
            AdminOperation[] adminOperation = new AdminOperation[1];
            AdminOperation adminMoveFile = new AdminOperation();
            CopyFile copyFile = new CopyFile();
            FileSearch fsearch = new FileSearch();
            FileCondition fcond = new FileCondition();

            copyFile.Target = sTarget;
            fcond.Field = FileField.Name;
            fcond.Match = FormatSearchFile(sSourceFileFolderToMove);
            fsearch.Item = fcond;
            copyFile.Item = sSourceFolder;
            copyFile.ReplaceExisting = replaceExistingReport;
            copyFile.ReplaceExistingSpecified = true;
            //moveFile.MaxVersions = 1;
            //moveFile.MaxVersionsSpecified = true;
            copyFile.LatestVersionOnly = false;
            copyFile.LatestVersionOnlySpecified = true;
            copyFile.ItemElementName = ItemChoiceType24.WorkingFolderName;
            copyFile.Item1 = fsearch;
            copyFile.Item1ElementName = Item1ChoiceType13.Search;

            // Create The Administration Operations for each step in Actuate		
            adminMoveFile.Item = copyFile;

            adminOperation[0] = adminMoveFile;

            return LProxy.administrate(adminOperation);
        }


        #endregion

        #region Move File/Folder

        protected AdministrateResponse MoveFileFolder(string sSourceFolder, string sSourceFileFolderToMove, string sTarget, bool replaceExistingReport)
        {
            AdminOperation[] adminOperation = new AdminOperation[1];
            AdminOperation adminMoveFile = new AdminOperation();
            MoveFile moveFile = new MoveFile();
            FileSearch fsearch = new FileSearch();
            FileCondition fcond = new FileCondition();

            moveFile.Target = sTarget;
            fcond.Field = FileField.Name;
            fcond.Match = FormatSearchFile(sSourceFileFolderToMove);
            fsearch.Item = fcond;
            moveFile.Item = sSourceFolder;
            moveFile.ReplaceExisting = replaceExistingReport;
            moveFile.ReplaceExistingSpecified = true;
            //moveFile.MaxVersions = 1;
            //moveFile.MaxVersionsSpecified = true;
            moveFile.LatestVersionOnly = false;
            moveFile.LatestVersionOnlySpecified = true;
            moveFile.ItemElementName = ItemChoiceType23.WorkingFolderName;
            moveFile.Item1 = fsearch;
            moveFile.Item1ElementName = Item1ChoiceType12.Search;

            // Create The Administration Operations for each step in Actuate		
            adminMoveFile.Item = moveFile;

            adminOperation[0] = adminMoveFile;

            return LProxy.administrate(adminOperation);
        }

        #endregion

        #region Archive File

        protected AdministrateResponse ArchiveFileFolder(int nMonthsToArchive, string path, string fileFolder, DateTime dateTimeArchiveExpires)
        {
            ArchiveRule aRule = new ArchiveRule
                                    {
                                        FileType = fileFolder.Substring(fileFolder.Length - 3, 3).ToUpper()
                                    };

            // if the archive expieration date is still greater then today, delete the file and exit the function
            if (dateTimeArchiveExpires <= DateTime.Now)
            {
                DeleteFileFolder(path, fileFolder);
                return null;
            }

            aRule.Item = dateTimeArchiveExpires;
            aRule.ArchiveOnExpiration = false;
            aRule.ArchiveOnExpirationSpecified = true;

            ArchiveRule[] archiveArray = new[] {aRule};

            UpdateFile upFile = new UpdateFile
                                    {
                                        LatestVersionOnly = true,
                                        LatestVersionOnlySpecified = true,
                                        Recursive = false,
                                        RecursiveSpecified = true,
                                        IgnoreMissing = true,
                                        IgnoreMissingSpecified = true
                                    };
            UpdateFileOperation upFileOp = new UpdateFileOperation
                                               {
                                                   Item = archiveArray,
                                                   ItemElementName = ItemChoiceType26.AddArchiveRules
                                               };
            upFile.Item1 = path + fileFolder;
            upFile.Item1ElementName = Item1ChoiceType14.Name;
            UpdateFileOperation[] arr = new[] {upFileOp};
            upFile.UpdateFileOperationGroup = arr;

            AdminOperation admOp = new AdminOperation {Item = upFile};

            AdminOperation[] lAdminRequest = new[] {admOp};

            return LProxy.administrate(lAdminRequest);
        }

        #endregion

        #region DirSearch

        /// <summary>
        /// Loop through an actuate directory, default settings. Return ALL files.
        /// </summary>
        /// <param name="sDir"></param>
        public void DirSearch(string sDir)
        {
            DirSearch(sDir, ActuateFileType.All);
        }

        public void DirSearch(string sDir, ActuateFileType actuateFileType)
        {
            if (sDir.Length > 1)
            {
                if (sDir.Substring(0, 2) == "//")
                    sDir = sDir.Substring(1, sDir.Length - 1);
            }

            GetFolderItemsResponse countAttribs = GetDirectoryContents(sDir, actuateFileType, true);

            if (countAttribs == null) return;

            for (int x = 0; x < countAttribs.TotalCount; x++)

                ProcessFileDirectory(sDir, countAttribs.ItemList[x]);
        }

        #endregion

        #region Virtual Process File Directory

        protected virtual void ProcessFileDirectory(string sDirectory, Server_Proxy.localhost.File oActuateFileAttributes)
        {
            // Dummy funciton

        }


        #endregion

        #region Set File/Folder permissions

        protected AdministrateResponse SetFolderPermissions(ArrayList aryRoleNames, string sFolderName, string sAccessRights, bool bOverrideExistingPermissions)
        {
            return SetFolderPermissions(aryRoleNames, sFolderName, sAccessRights,
                                  bOverrideExistingPermissions
                                      ? ItemChoiceType26.SetPermissions
                                      : ItemChoiceType26.GrantPermissions);
        }


        private AdministrateResponse SetFolderPermissions(ArrayList aryRoleNames, string sFolderName, string sAccessRights, ItemChoiceType26 operation)
        {

            #region Local variables

            if (aryRoleNames == null) return null;

            if (aryRoleNames.Count == 0) return null;

            UpdateFile updateFile = new UpdateFile();
            UpdateFileOperation[] list = new UpdateFileOperation[1];
            Permission[] perm = new Permission[aryRoleNames.Count];
            UpdateFileOperation updateFileOp = new UpdateFileOperation();
            AdminOperation updatePrevileges = new AdminOperation();
            AdminOperation[] adminOperations = new AdminOperation[1];

            #endregion

            updateFile.Item1 = sFolderName;
            updateFile.Item1ElementName = Item1ChoiceType14.Name;
            updateFile.IgnoreMissing = true;
            updateFile.LatestVersionOnly = false;
            updateFile.LatestVersionOnlySpecified = true;
            updateFile.IgnoreMissingSpecified = true;
            updateFile.Recursive = true;
            updateFile.RecursiveSpecified = true;

            int x = 0;

            foreach (string sRole in aryRoleNames)
            {
                Permission permission = new Permission
                {
                    Item = sRole,
                    ItemElementName = ItemChoiceType1.RoleName,
                    AccessRight = sAccessRights
                };

                perm[x] = permission;
                x++;
            }

           

            updateFileOp.ItemElementName = operation;
            updateFileOp.Item = perm;

            list[0] = updateFileOp;

            updateFile.UpdateFileOperationGroup = list;

            updatePrevileges.Item = updateFile;
            adminOperations[0] = updatePrevileges;

            return LProxy.administrate(adminOperations);
        }


        #endregion

        #region Format Search File

        /// <summary>
        /// Replace the illegal search characters for Actuate functionality
        /// </summary>
        /// <param name="sFile"></param>
        /// <returns>Formated string for Actuate usage</returns>
        protected string FormatSearchFile(string sFile)
        {

            // Coment out the "-", if it has not already been done
            int n1 = sFile.IndexOf("-");
            int n2 = sFile.IndexOf("\\-");

            if (n1 > 0 && n2 == -1)
                sFile = sFile.Replace("-", "\\-");

            // Now, comment out the "," if it has not already been done
            int m1 = sFile.IndexOf(",");
            int m2 = sFile.IndexOf("\\,");

            if (m1 > 0 && m2 == -1)
                sFile = sFile.Replace(",", "\\,");

            // Next, comment out the "#", if it has not already been done.
            int o1 = sFile.IndexOf("#");
            int o2 = sFile.IndexOf("\\#");

            if (o1 > 0 && o2 == -1)
                sFile = sFile.Replace("#", "\\#");

            return (sFile);
        }


        #endregion

        #region Get Contents


        /// <summary>
        /// Method will take an ROI and save it as a PDF
        /// </summary>
        /// <param name="file">Full path and file name inside actuate</param>
        /// <param name="saveLocation">Full path and file name on the local Hard Drive</param>
        protected GetContentResponse GetContents(string file, string saveLocation)
        {
            GetContent getContent = new GetContent();

            ObjectIdentifier oI = new ObjectIdentifier();
            ViewParameter vp = new ViewParameter();
            ComponentType ct = new ComponentType();

            oI.Name = file;
            oI.Version = 1;
            oI.VersionSpecified = true;

            vp.Format = "PDF";
            vp.ScalingFactor = 100;
            vp.ScalingFactorSpecified = true;
            vp.AcceptEncoding = "text/xml";

            ct.Id = "0";

            getContent.Component = ct;
            getContent.Object = oI;
            getContent.ViewParameter = vp;
            getContent.DownloadEmbedded = true;

            GetContentResponse getContentResponse = LProxy.getContent(getContent);

            Attachment a = getContentResponse.ContentRef;
            byte[] data = a.ContentData;

            MemoryStream memoryStream = new MemoryStream(data);

            using (FileStream fs = System.IO.File.Create(saveLocation))
                memoryStream.WriteTo(fs);

            memoryStream.Close();

            return getContentResponse;
        }


        #endregion

        protected void DownloadFile(string file, string saveLocation)
        {
            DownloadFile downloadFile = new DownloadFile
                                     {
                                         Item = file,
                                         ItemElementName = ItemChoiceType34.FileName,
                                         DownloadEmbedded = true
                                     };

            DownloadFileResponse lRes = LProxy.downloadFile(downloadFile);

            //	Save downloaded file
            MemoryStream memoryStream = new MemoryStream(((Attachment) lRes.Item).ContentData);
            FileStream fileStream = new FileStream(saveLocation, FileMode.Create);
            memoryStream.WriteTo(fileStream);
            fileStream.Close();
        }



        #region Get Counters

        protected GetAllCounterValuesResponse GetAllCounters()
        {
            return LProxy.getAllCounterValues(new GetAllCounterValues());
        }

        protected GetCounterValuesResponse GetCounters(long[] counterIdList)
        {
            GetCounterValues getCounterValues = new GetCounterValues {CounterIDList = counterIdList};

            return LProxy.getCounterValues(getCounterValues);
        }


        #endregion

        #region Get Report Parameters

        protected GetReportParametersResponse GetReportParameters(string reportName)
        {
            // Prepare and send GetReportParameters request
            GetReportParameters getReportParameters = new GetReportParameters
                                                          {
                                                              Item = reportName,
                                                              ItemElementName = ItemChoiceType39.ReportFileName
                                                          };

            return LProxy.getReportParameters(getReportParameters);

        }
        //
        #endregion

        #region Execute Report

        protected SubmitJobResponse ExecuteReport(string reportName, Hashtable htParms, string jobName, string headLine)
        {
            // Get the report parameters first
            GetReportParametersResponse getReportParametersResponse = GetReportParameters(reportName);

            // Prepare SubmitJob request
            SubmitJob submitJob = new SubmitJob
                                                         {
                                                             JobName = jobName,
                                                             Headline = headLine,
                                                             Priority = 1000,
                                                             Item = reportName,
                                                             ItemElementName = ItemChoiceType45.InputFileName,
                                                             Operation = SubmitJobOperation.RunReport
                                                         };

            if (getReportParametersResponse.ParameterList != null && getReportParametersResponse.ParameterList.Length > 0)
            {
                ParameterValue[] parameterValues = new ParameterValue[getReportParametersResponse.ParameterList.Length];
                for (int i = 0; i < getReportParametersResponse.ParameterList.Length; i++)
                {
                    parameterValues[i] = new ParameterValue
                                             {
                                                 Name = getReportParametersResponse.ParameterList[i].Name
                                             };

                    IDictionaryEnumerator myEnumerator = htParms.GetEnumerator();

                    while (myEnumerator.MoveNext())
                    {
                        if (getReportParametersResponse.ParameterList[i].Name == myEnumerator.Key.ToString())
                            parameterValues[i].Value = myEnumerator.Value.ToString();
                    }
                }

                submitJob.Item1 = parameterValues;
                submitJob.Item1ElementName = Item1ChoiceType21.ParameterValues;
            }

            return LProxy.submitJob(submitJob);


        }


        #endregion

        #region Upload File

        /// <summary>
        /// Overloaded method to upload a file into the actuate volume
        /// </summary>
        /// <param name="path">Path inside actuate to place the new file.</param>
        /// <param name="files">Array of full path and file names to be uploaded.</param>
        protected void UploadFile(string path, string[] files)
        {
            foreach (string file in files)
                UploadFile(path, file);
        }


        /// <summary>
        /// Overloaded method to upload a file into the actuate volume
        /// </summary>
        /// <param name="path">Path inside actuate to place the new file.</param>
        /// <param name="file">Full path and file name of the file you wish to upload.</param>
        protected UploadFileResponse UploadFile(string path, string file)
        {
            // Create extended proxy inctance
            ActuateAPIEx actuateApiEx = new ActuateAPIEx
                                            {
                                                Url = LProxy.Url,
                                                HeaderValue = new Header {AuthId = LProxy.HeaderValue.AuthId}
                                            };

            // Prepare UploadFile request
            UploadFile uploadFile = new UploadFile {NewFile = new NewFile()};

            char[] separators = {'/', '\\', ':'};
            string[] strList = file.Split(separators, 100);
            uploadFile.NewFile.Name = path + strList[strList.Length - 1];
            uploadFile.Content = new Attachment {ContentType = "binary", ContentId = "Attachment"};

            //	Open the file to be uploaded
            ActuateAPIEx.UploadStream = new FileStream(file, FileMode.Open);

            // Send request
            UploadFileResponse uploadFileResponse = actuateApiEx.uploadFile(uploadFile);

            ActuateAPIEx.UploadStream.Close();

            return uploadFileResponse;

        }

        #endregion

        #region De-structor

        ~ActuateHelper()
        {
            if (LProxy != null)
                LProxy.Dispose();

            LProxy = null;
        }

        #endregion

        #region Dispose

        public void Dispose()
        {
            if (LProxy != null)
                LProxy.Dispose();

            LProxy = null;
        }

        #endregion

    }	// Class

}	// Namespace
