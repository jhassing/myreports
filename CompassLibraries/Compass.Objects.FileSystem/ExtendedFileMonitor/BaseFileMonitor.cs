
namespace Compass.FileSystem.ExtendedFileMonitor
{
    /// <summary>
    /// Summary description for BaseFileMonitor.
    /// </summary>
    public class BaseFileMonitor : System.ComponentModel.Component
    {
        protected System.IO.FileSystemWatcher fswFileMonitor;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        public BaseFileMonitor(System.ComponentModel.IContainer container)
        {
            ///
            /// Required for Windows.Forms Class Composition Designer support
            ///
            container.Add(this);
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        public BaseFileMonitor()
        {
            ///
            /// Required for Windows.Forms Class Composition Designer support
            ///
            InitializeComponent();


        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }


        #region Component Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fswFileMonitor = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.fswFileMonitor)).BeginInit();
            // 
            // fswFileMonitor
            // 
            this.fswFileMonitor.EnableRaisingEvents = false;
            this.fswFileMonitor.NotifyFilter = System.IO.NotifyFilters.FileName;
            this.fswFileMonitor.Changed += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Changed);
            this.fswFileMonitor.Created += new System.IO.FileSystemEventHandler(this.fileSystemWatcher1_Created);
            this.fswFileMonitor.Renamed += new System.IO.RenamedEventHandler(fswFileMonitor_Renamed);
            ((System.ComponentModel.ISupportInitialize)(this.fswFileMonitor)).EndInit();

        }
        #endregion

        private void fileSystemWatcher1_Changed(object sender, System.IO.FileSystemEventArgs e)
        {

        }

        private void fileSystemWatcher1_Created(object sender, System.IO.FileSystemEventArgs e)
        {

        }

        protected void Pause()
        {
            System.Threading.Thread.Sleep(200);
        }

        protected void StopDirectoryMonitoring()
        {
            fswFileMonitor.EnableRaisingEvents = false;

            // Pause for a second
            Pause();
        }

        protected void StartDirectoryMonitoring()
        {
            fswFileMonitor.EnableRaisingEvents = true;

            // Pause for a second
            Pause();
        }

        private void fswFileMonitor_Renamed(object sender, System.IO.RenamedEventArgs e)
        {

        }
    }
}
