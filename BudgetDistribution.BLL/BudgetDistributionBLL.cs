using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

// Must be added to use the ZIP routines. Only available in the J# classes.
using java.util;
using java.util.zip;

// Compass Namespaces
using Compass.Data.MSSQLDataAccess;
using Compass.FileSystem.Compression;

namespace BudgetDistribution.BLL
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class BudgetDistributionBLL
	{
		private string _connectionString = "";

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="connectionString">Database connection string used for data access.</param>
		public BudgetDistributionBLL(string connectionString)
		{
			_connectionString = connectionString;
		}

		
		/// <summary>
		/// Empty contructor.
		/// </summary>
		public BudgetDistributionBLL() {}


		public void InsertFileDownloaded(string userName, string fileName, DateTime creationTime)
		{
			SqlConnection conn = null;
			SqlParameter[] sqlParms= null;

			try
			{
				conn = new SqlConnection(_connectionString);
		
				sqlParms = new SqlParameter[4];

				sqlParms[0] = new SqlParameter("@UserName", SqlDbType.VarChar, 50 ); 
				sqlParms[0].Value = userName;

				sqlParms[1] = new SqlParameter("@FileName", SqlDbType.VarChar, 150 ); 
				sqlParms[1].Value = fileName;

				sqlParms[2] = new SqlParameter("@CreationTime", SqlDbType.DateTime);
				sqlParms[2].Value = creationTime;

				sqlParms[3] = new SqlParameter("@DownloadedTime", SqlDbType.DateTime);
				sqlParms[3].Value = System.DateTime.Now;			

				SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_InsertDownloadedFile", sqlParms);					

			}
			catch(Exception ex)
			{			
				throw(ex);
			}	
			finally
			{
				if (conn != null)
				{
					conn.Close();
					conn.Dispose();
				}

				conn = null;
				sqlParms = null;
			}
		}

		
		public bool HasFileEverBeenDownloaded(string userName, string fileName, DateTime creationTime, out DateTime downloadedTime)
		{

			downloadedTime = System.DateTime.Now;

			SqlParameter[] arParms = null;
			SqlConnection conn = null;

			try
			{

				conn = new SqlConnection(_connectionString);
				arParms = new SqlParameter[5];
						
				arParms[0] = new SqlParameter("@UserName", SqlDbType.VarChar, 50 ); 
				arParms[0].Value = userName;

				arParms[1] = new SqlParameter("@FileName", SqlDbType.VarChar, 150 ); 
				arParms[1].Value = fileName;

				arParms[2] = new SqlParameter("@CreationTime", SqlDbType.DateTime);
				arParms[2].Value = creationTime.ToString();

				arParms[3] = new SqlParameter("@ReturnValue", SqlDbType.Bit );
				arParms[3].Direction = System.Data.ParameterDirection.Output;

				arParms[4] = new SqlParameter("@DownloadedTime", SqlDbType.DateTime );
				arParms[4].Direction = System.Data.ParameterDirection.Output;

				SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_GetDownloadedFileList", arParms);					
				
				if (arParms[4].Value != System.DBNull.Value)			
					downloadedTime = (DateTime)arParms[4].Value;

				return((bool)arParms[3].Value);

			}
			catch(Exception ex)
			{			
				throw(ex);
			}	
			finally
			{				
				if (conn != null)
					conn.Dispose();

				conn = null;
				arParms = null;
			}


		}

		
		public void ZipSelectedFiles(string path, string[] newFiles)
		{
			ZipUtils.UpdateZipFile(path, newFiles);
		}

		
		public string GetTempDirectoryForThisUser(string user, string baseTempFolder)
		{
			bool ready = false;
			string folder = "";
			int x = 1;

			string temp = user + "tmp";

			while(!ready)
			{
				folder = baseTempFolder + temp + x.ToString();

				if (Directory.Exists(folder))
					x++;
				else
					ready = true;
			}

			return(folder);
		}

		
		public string GetZipFileName()
		{
			return("Budget.zip");
		}

	}
}
