IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Get_ClientEmailSendFromListing]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Get_ClientEmailSendFromListing]
GO
/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_ClientEmailSendFromListing

Description:	Locates a client email addresss listing
  
Parameters:	

Returns:	Client email address listing		

Revisions:	03/14/2006 John Schiavarelli created
		06/19/2007 Jason Hassing modified

Example:	exec usp_Get_ClientEmailSendFromListing 'f'
------------------------------------------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_ClientEmailSendFromListing
(
	@as_FromEmailAddress 		as varchar(200)
)
AS

	SET NOCOUNT ON
	
	SELECT
		ClientEmailSendFromID,
		FromEmailAddress,
		EmailMessageID

	FROM         dbo.ClientEmailSendFrom

	WHERE   FromEmailAddress like '%' + @as_FromEmailAddress + '%'
GO

