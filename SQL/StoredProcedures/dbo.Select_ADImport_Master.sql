SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Select_ADImport_Master]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Select_ADImport_Master]
GO

CREATE     PROCEDURE dbo.Select_ADImport_Master
	(	@beenImported as int
	)

AS

  BEGIN

	SELECT     dbo.tblADImport_Master.*  FROM         dbo.tblADImport_Master WHERE     (BeenImported = @beenImported)

  END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

