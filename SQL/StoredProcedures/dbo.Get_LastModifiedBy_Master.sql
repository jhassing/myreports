SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Get_LastModifiedBy_Master]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Get_LastModifiedBy_Master]
GO

CREATE     PROCEDURE dbo.Get_LastModifiedBy_Master
	(	
		@FKADImport_MasterID uniqueidentifier,	
		@LastModifiedBy varchar(150) output,
		@ImportedDate datetime output
	)

AS

	SET NOCOUNT ON

	select @LastModifiedBy = LastModifiedBy from tblADImport_Master WHERE PKADImport_MasterID = @FKADImport_MasterID
	select @ImportedDate = ImportedDate from tblADImport_Master WHERE PKADImport_MasterID = @FKADImport_MasterID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

