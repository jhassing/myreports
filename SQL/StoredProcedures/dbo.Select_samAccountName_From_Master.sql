SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Select_samAccountName_From_Master]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Select_samAccountName_From_Master]
GO

CREATE     PROCEDURE dbo.Select_samAccountName_From_Master
	(	
		@PKADImport_MasterID uniqueidentifier,
		@samAccountName varchar(30) output
	)

AS
	set nocount on

	select @samAccountName = samAccountName from tblADImport_Master where PKADImport_MasterID=@PKADImport_MasterID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

