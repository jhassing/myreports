IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Update_EmailMessage]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Update_EmailMessage]
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Update_EmailMessage

Description:	Updates a client from email address
  
Parameters:	
	@ai_EmailMessageID	int  			Email Message ID
	@ai_Message		varchar(4000)		The email message body
	@ai_Subject		varchar(200)		The Email Subject

Returns:	Nothing

Revisions:	06/18/2007 Jason Hassing Created

Example:	exec usp_Update_EmailMessage 2,'Title of email message','This is the subject...','This is the message...','hassij01'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Update_EmailMessage
(
	@ai_EmailMessageID	int,
	@as_Title		varchar(50),
	@as_Subject		varchar(200),
	@as_Message		varchar(4000),
	@as_ModifiedBy	varchar(25)
)
AS

	UPDATE 
		EmailMessage
	SET	
		Title = @as_Title,
		Subject = @as_Subject,
		Message = @as_Message,		
		ModifiedBy = @as_ModifiedBy,
		ModifyDateTime = getDate()
		
	WHERE
		EmailMessageID = @ai_EmailMessageID
GO

