SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Insert_ADImport_Master]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Insert_ADImport_Master]
GO

CREATE     PROCEDURE dbo.Insert_ADImport_Master
	(
		@samAccountName as varchar(30),
		@password as varbinary(255),
		@RequestorsID as varchar(20),
		@requestorsFirstName as varchar(150),
		@requestorsLastName as varchar(150),
		@requestorsEmail as varchar(255),
		@requestorsPhoneNumber as varchar(30),
		@requestorsDepartment as varchar(150),
		@FKADImport_MasterID as uniqueidentifier out
	)
AS

  BEGIN

	INSERT INTO tblADImport_Master
	(
		samAccountName,
		[password],
		RequestorsID,
		RequestorsFirstName,
		RequestorsLastName,
		RequestorsEmail,
		RequestorsPhone,
		RequestorsDepartment
	)
	VALUES
	(   
		@samAccountName,
		@password,
		@RequestorsID,
		@RequestorsFirstName,
		@RequestorsLastName,
		@RequestorsEmail,
		@requestorsPhoneNumber,
		@RequestorsDepartment

	)
	
	set @FKADImport_MasterID = (SELECT top 1 PKADImport_MasterID from tblADImport_Master order by ImportedDate desc)

  END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

