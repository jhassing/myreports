if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_BurstProcessStatus_Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_BurstProcessStatus_Details]
GO

/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:
		dbo.usp_Insert_BurstProcessStatus_Details
Description:
		Inserts a new Bursted Report Record
Parameters:
		@ai_BurstProcessStatus	as int,
		@as_ReportLocation	as varchar(255)
Returns:
		Nothing
Revisions:
		10/23/2007 Jason Hassing Modified
		12/04/2007 Jason Hassing Modified, no duplicate records for Subsidy Invoices in any given month
Example:
		exec usp_Insert_BurstProcessStatus_Details '123456','/(3) Operations/018254 - Springs Preserve/arwkdet - Detail Aged Trial Balance by Operation ZCAR2201/'
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Insert_BurstProcessStatus_Details
	@ai_BurstProcessStatus		as int,
	@as_ReportLocation		as varchar(255)
AS

-- FOR DEBUGGING PURPOSSES
--DECLARE @ai_BurstProcessStatus as int
--DECLARE @as_ReportLocation as varchar(255)
--SET @ai_BurstProcessStatus = '998877'
--SET @as_ReportLocation = '/(3) Operations/999999 - Jason Hassing/Subsidy Invoice ZCAR2821.pdf'

IF (SELECT @as_ReportLocation) LIKE '%ZCAR2821%'
	BEGIN
	DECLARE @li_record_ID AS int
	SET @li_record_ID = (
		SELECT 
			BurstProcessStatusDetailID 
		FROM 
			BurstProcessStatus_Detail 
		WHERE	
			MONTH(getdate()) = MONTH(CreateDateTime)
		AND
			ReportLocation = @as_ReportLocation
	)

	IF (@li_record_ID) IS NOT NULL
		BEGIN
			--SELECT @li_record_ID
			UPDATE 
				BurstProcessStatus_Detail
			SET
				BurstProcessStatusID = @ai_BurstProcessStatus,
				CreateDateTime = getDate()			
			WHERE
				BurstProcessStatusDetailID = @li_record_ID
		END
	ELSE
		BEGIN
			INSERT INTO  dbo.BurstProcessStatus_Detail
				(
					BurstProcessStatusID,
					ReportLocation,
					CreateDateTime
				)
			VALUES
				(
					@ai_BurstProcessStatus,
					@as_ReportLocation,
					getDate()
				)
		END
	END
ELSE
BEGIN
	INSERT INTO  dbo.BurstProcessStatus_Detail
		(
			BurstProcessStatusID,
			ReportLocation,
			CreateDateTime
		)
	VALUES
		(
			@ai_BurstProcessStatus,
			@as_ReportLocation,
			getDate()
		)
END


GO

