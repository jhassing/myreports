IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Get_UnitsByDM]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Get_UnitsByDM]
GO
/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_UnitsByDM

Description:	Returns a list if Units in a District Manager Code
  
Parameters:	DM, varcarch(8)

Returns:	Unit code listing

Revisions:	09/10/2007 Jason Hassing created

Example:	exec usp_Get_UnitsByDM ''
------------------------------------------------------------------------------------------------------------------------------*/

CREATE      PROCEDURE dbo.usp_Get_UnitsByDM
(
	@as_DM	as varchar(8)
)
AS
	SELECT DISTINCT Unit
	FROM dbo.Zrops
	WHERE DM = @as_DM AND Unit <> ''
	ORDER BY Unit
GO
