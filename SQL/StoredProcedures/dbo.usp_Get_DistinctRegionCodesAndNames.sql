IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Get_DistinctRegionCodesAndNames]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Get_DistinctRegionCodesAndNames]
GO
/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_DistinctRegionCodesAndName

Description:	Returns a distinct recordset of region codes 
		from ZROPS and their names from Folder
  
Parameters:	none

Returns:	Region code listing with names

Revisions:	10/10/2007 Jason Hassing Created

Example:	exec usp_Get_DistinctRegionCodesAndName
------------------------------------------------------------------------------------------------------------------------------*/
CREATE      PROCEDURE dbo.usp_Get_DistinctRegionCodesAndNames
AS

SET NOCOUNT ON
	
SELECT DISTINCT 
	a.Region,
	b.FolderName

FROM 
	dbo.zrops a

INNER JOIN 
	dbo.Folder b

ON 
	a.Region = b.FolderCode

WHERE 
	a.Region NOT LIKE ''

ORDER BY 
	Region
GO
