SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Delete_Master_Details]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Delete_Master_Details]
GO

CREATE     PROCEDURE dbo.Delete_Master_Details
	(	@PKADImport_MasterID as uniqueidentifier
	)

AS

  BEGIN

	delete   FROM         dbo.tblADImport_Master WHERE     (PKADImport_MasterID = @PKADImport_MasterID)

  END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

