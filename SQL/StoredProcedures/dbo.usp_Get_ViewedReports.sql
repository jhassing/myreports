IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Get_ViewedReports]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Get_ViewedReports]
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Get_ViewedReports

Description:	Gets a listing of reports viewed between a given date range.
  
Parameters:	@ad_StartDate 	datetime
		@ad_EndDate	datetime
		@as
		@as_CostCenters

Returns:	Listing of viewed reports

Revisions:	1/20/2006 Monica Hancock  Created
		08/29/2007 Jason Hassing  Modified
		09/13/2007 Jason Hassing  Modified
		12/06/2007 Jason Hassing  Modified - track reports in the Archive Folders too

Example:	exec usp_Get_ViewedReports '1/01/2007', '01/01/2010','ZCAR2821','999999','1','1'
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
-- model params as if this is a sproc 
--DECLARE @ad_StartDate datetime SELECT @ad_StartDate = '2007.07.01'
--DECLARE @ad_EndDate datetime SELECT @ad_EndDate = '2007.09.12'
--DECLARE @as_ReportType varchar(8) SELECT @as_ReportType = 'ZCAR2201'
--DECLARE @as_CostCenters varchar(1000) SELECT @as_CostCenters = '21102|017781|009610|016740|098732' -- 9, 3 
--DECLARE @as_CostCenters varchar(1000) SELECT @as_CostCenters = '768787|675677|675467|986776|12323|14553|11234|343434|545454|039583' -- 15, 2
--DECLARE @as_CostCenters varchar(1000) SELECT @as_CostCenters = '768787|675677|675467|986776|12323|14553|11234|343434|545454|039583|656474|2545|012863|090944|434145' -- 22, 2
--DECLARE @as_CostCenters varchar(1000) SELECT @as_CostCenters = '768787|675677|675467|986776|12323|14553|11234|343434|545454|039583|656474|2545|012863|090944|434145|23435|123456|098765|17492|14923' -- 33, 3
--DECLARE @as_CostCenters varchar(1000) SELECT @as_CostCenters = '768787|675677|675467|986776|12323|14553|11234|343434|545454|039583|656474|2545|012863|090944|434145|23435|123456|098765|17492|14923|21102|017781|009610|016740|098732' -- 37, 5
--DECLARE @as_CostCenters varchar(1000) SELECT @as_CostCenters = '768787|675677|675467|986776|12323|14553|11234|343434|545454|039583|656474|2545|012863|090944|434145|23435|123456|098765|17492|14923|21102|017781|009610|016740|098732|494949|898989|34678|01928|495730|496340|597786|09629|230854|37300|34957|12342|069898' -- 56, 5
--DECLARE @ab_viewed bit SELECT @ab_viewed = '1'
--DECLARE @ab_notviewed bit SELECT @ab_notviewed = '1'

--DECLARE @as_PageNumber VARCHAR(10) SELECT @as_PageNumber='1'
--DECLARE @as_NumberOfRecordsPerPage VARCHAR(10) SELECT @as_NumberOfRecordsPerPage='10'
-- end params 

CREATE	PROCEDURE dbo.usp_Get_ViewedReports
	@ad_StartDate datetime,
	@ad_EndDate datetime,
	@as_ReportType varchar(8),
	@as_CostCenters varchar(4000),
	@ab_viewed bit,
	@ab_notviewed bit
AS

SET NOCOUNT ON


-- these variable are for looping thought the list of CCs*/
DECLARE @li_charpos	int
DECLARE @li_StartPos	int
DECLARE @ls_delimiter	char(1)
DECLARE @li_strpos	int
DECLARE @ls_CC		varchar (100)
DECLARE @valid		int
DECLARE @ls_CurCC	varchar(8)
DECLARE @ltbl_Unit TABLE (CC int, ProcessIndicator bit default 0)
-- end of CC looping variables

----------------------------------------------------------------------------------------------------------
-- CC PARAMS LOOP
-- This table is used to store the result set as the query loops through the list of CCs and is reduced by the paging table
DECLARE @ltbl_ret  TABLE
	(
		UserName varchar (50),
		ReportName varchar (150),
		ActuateFileTimeStamp datetime,
		BurstEntryTime datetime,
		ViewedDate datetime
	)

SET @ls_delimiter = '|'
SET @li_charpos = 1
SET @li_StartPos = 0
SET @ls_CC = ''
SET @valid=LEN(@as_CostCenters)
-- This next block puts the @as_CC param into a temp table for each entry separated by a | character
If RIGHT(@as_CostCenters, 1) <> '|'
	SET @as_CostCenters = @as_CostCenters + @ls_delimiter

WHILE @li_charpos <=Len (@as_CostCenters)
BEGIN
	SET @li_charpos = @li_charpos + 1
	IF SUBSTRING(@as_CostCenters, @li_charpos, 1) = @ls_delimiter
	BEGIN
		Set @ls_CC = SUBSTRING(@as_CostCenters, @li_startPos, @li_charpos - @li_StartPos)
		INSERT INTO @ltbl_Unit (CC) VALUES (@ls_CC)
		Set @li_StartPos = @li_charpos + 1
		Set @li_charpos = @li_StartPos 
	END
END
   
-- END CC LOOP

-- GET RECORD SET of records to narrow the select search to a more reasonable list than the full 8 million
DECLARE @li_BurstProcessStatus_Detail  TABLE
	(
		BurstProcessStatusDetailID int,
		BurstProcessStatusID int,
		ReportLocation varchar (255),
		LocationCode varchar (30),
		CreateDateTime datetime,
		CostCenter int,
		ReportType varchar (8)
	)
INSERT INTO @li_BurstProcessStatus_Detail
	(
		BurstProcessStatusDetailID,
		BurstProcessStatusID,
		ReportLocation,
		LocationCode,
		CreateDateTime,
		CostCenter,
		ReportType
	)
SELECT
	BurstProcessStatusDetailID,
	BurstProcessStatusID,
	ReportLocation,
	LocationCode,
	CreateDateTime,
	CostCenter = convert(int, Substring(ReportLocation,CHARINDEX('/', ReportLocation, 2)+1,6)),
	ReportType = Substring(ReportLocation,CHARINDEX('ZCAR',ReportLocation),8)
FROM 
	dbo.BurstProcessStatus_Detail
WHERE
	convert(datetime, convert(varchar(12), dbo.BurstProcessStatus_Detail.CreateDateTime), 101) 
	between 
	@ad_StartDate and @ad_EndDate AND
	dbo.BurstProcessStatus_Detail.ReportLocation LIKE '/(3%' AND 
	dbo.BurstProcessStatus_Detail.CreateDateTime IS NOT null AND
	dbo.BurstProcessStatus_Detail.ReportLocation LIKE '%' + @as_ReportType + '%' 



-- now filter the cost centers
DECLARE @ltbl_BurstProcessStatus_Detail  TABLE
	(
		BurstProcessStatusDetailID int,
		BurstProcessStatusID int,
		ReportLocation varchar (255),
		LocationCode varchar (30),
		CreateDateTime datetime,
		CostCenter int,
		ReportType varchar (8)
	)
INSERT INTO @ltbl_BurstProcessStatus_Detail
	(
		BurstProcessStatusDetailID,
		BurstProcessStatusID,
		ReportLocation,
		LocationCode,
		CreateDateTime,
		CostCenter,
		ReportType
	)
SELECT
 	BurstProcessStatusDetailID,
	BurstProcessStatusID,
	ReportLocation,
	LocationCode,
	CreateDateTime,
	CostCenter,
	ReportType
FROM 
	@li_BurstProcessStatus_Detail a
INNER JOIN 
	@ltbl_Unit b
ON 
	a.CostCenter = b.CC

DECLARE @ltbl_ReportViewedDate TABLE
	(
		ReportViewedDateID int,
		UserName varchar(50),
		ReportName varchar(250),
		CreationTime datetime,
		ViewedDate datetime
	)
INSERT INTO @ltbl_ReportViewedDate
	(
		ReportViewedDateID,
		UserName,
		ReportName,
		CreationTime,
		ViewedDate
	)
SELECT
	ReportViewedDateID,
	UserName,
 	RIGHT(ReportName, LEN(ReportName) - PATINDEX('%/(3)%', ReportName)),
	CreationTime,
	ViewedDate

FROM 
	ReportViewedDate
WHERE
	ReportCode = @as_ReportType
AND
	CreationTime BETWEEN @ad_StartDate and @ad_EndDate

--SELECT * FROM @ltbl_ReportViewDate

-- END GET RECORD SET 

------------------------------------------------------------------------------------------------------------------------
--  Start search for viewed reports for each CostCenter
--  Load records into a temporary table to be returned at the end of the sproc
------------------------------------------------------------------------------------------------------------------------

if (@ab_viewed = '1' AND @ab_notviewed = '0')
BEGIN
	INSERT INTO @ltbl_ret 
	(	
		UserName,
		ReportName,
		ActuateFileTimeStamp,
		BurstEntryTime,
		ViewedDate
	)
	SELECT
		a.UserName,
		b.ReportLocation as ReportName,
		a.CreationTime as ActuateFileTimeStamp,
		b.CreateDateTime as BurstEntryTime,
		a.ViewedDate
	FROM 
		@ltbl_BurstProcessStatus_Detail b
	INNER JOIN 
		@ltbl_ReportViewedDate a
	ON 
		b.ReportLocation LIKE '%' + a.ReportName 
		-- adding 4 hours to the createdatetime becuase the creationTime is recorded in the database exactly 4 hours ahead of the actual report creation time.
	AND 
		a.CreationTime < DATEADD(hour,5,b.CreateDateTime)
	AND
		a.CreationTime > b.CreateDateTime
	ORDER BY b.CreateDateTime DESC
END

if (@ab_viewed = '0' AND @ab_notviewed = '1')
BEGIN
	INSERT INTO @ltbl_ret 
	(	
		UserName,
		ReportName,
		ActuateFileTimeStamp,
		BurstEntryTime,
		ViewedDate
	)
	SELECT
		a.UserName,
		b.ReportLocation as ReportName,
		a.CreationTime as ActuateFileTimeStamp,
		b.CreateDateTime as BurstEntryTime,
		a.ViewedDate
	FROM 
		@ltbl_BurstProcessStatus_Detail b
	LEFT JOIN 
		@ltbl_ReportViewedDate a
	ON 
		b.ReportLocation LIKE '%' + a.ReportName
		-- adding 4 hours to the createdatetime becuase the creationTime is recorded in the database exactly 4 hours ahead of the actual report creation time.
	AND 
		a.CreationTime < DATEADD(hour,5,b.CreateDateTime)
	AND
		a.CreationTime > b.CreateDateTime
	WHERE a.ReportViewedDateID IS NULL
	ORDER BY b.CreateDateTime DESC
END

if (@ab_viewed = '1' AND @ab_notviewed = '1')
BEGIN
	INSERT INTO @ltbl_ret 
	(	
		UserName,
		ReportName,
		ActuateFileTimeStamp,
		BurstEntryTime,
		ViewedDate
	)
	SELECT
		a.UserName,
		b.ReportLocation as ReportName,
		a.CreationTime as ActuateFileTimeStamp,
		b.CreateDateTime as BurstEntryTime,
		a.ViewedDate
	FROM 
		@ltbl_ReportViewedDate a
	RIGHT OUTER JOIN 
		@ltbl_BurstProcessStatus_Detail b
	ON 
		b.ReportLocation LIKE '%' + a.ReportName
		-- adding 4 hours to the createdatetime because the creationTime is recorded in the database exactly 4 hours ahead of the actual report creation time.
	AND 
		a.CreationTime < DATEADD(hour,5,b.CreateDateTime)
	AND
		a.CreationTime > b.CreateDateTime
	ORDER BY b.CreateDateTime DESC
END

-- return all records in the temporary table
SELECT * FROM @ltbl_ret

/*-- MORE DEBUGGING CODE
SELECT * FROM @ltbl_BurstProcessStatus_Detail WHERE ReportLocation LIKE '%999999%'
DECLARE @ls_reportname as varchar(250) SET @ls_reportname = '/(3) Operations/999999 - Jason Hassing/Subsidy Invoice ZCAR2821.pdf'
SELECT * FROM @ltbl_ReportViewedDate WHERE ReportName LIKE '%' + @ls_reportname
DECLARE @ld_time datetime SET @ld_time = (SELECT CreationTime FROM ReportViewedDate WHERE ReportName LIKE '/(0%999999%') - (SELECT DATEADD(hour,5,CreateDateTime) FROM @ltbl_BurstProcessStatus_Detail WHERE ReportLocation LIKE '%999999%' AND CreateDateTime < '2007-12-01')
SELECT @ld_time
*/

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

