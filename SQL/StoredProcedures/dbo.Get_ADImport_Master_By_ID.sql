SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Get_ADImport_Master_By_ID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Get_ADImport_Master_By_ID]
GO

CREATE     PROCEDURE dbo.Get_ADImport_Master_By_ID
	(
		@PKADImport_MasterID as uniqueidentifier,
		@samAccountName varchar(30) output,
		@password varbinary(255) output,
		@RequestorsID varchar(20) output,
		@RequestorsFirstName varchar(150) output,
		@RequestorsLastName varchar(150) output,
		@RequestorsEmail varchar(255) output,
		@RequestorsPhoneNumber varchar(30) output,
		@RequestorsDepartment varchar(150) output

	)
AS

SELECT

    @samAccountName = samAccountName,
    @password = password,
    @RequestorsID = RequestorsID,
    @RequestorsFirstName = RequestorsFirstName,
    @RequestorsLastName = RequestorsLastName,
    @RequestorsEmail = RequestorsEmail,
    @RequestorsPhoneNumber = RequestorsPhone,
    @RequestorsDepartment  = RequestorsDepartment

 FROM dbo.tblADImport_Master WHERE  (PKADImport_MasterID = @PKADImport_MasterID)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

