IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Insert_EmailMessage]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Insert_EmailMessage]
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Insert_EmailMessage

Description:	Inserts a new email Message Body and Subject
  
Parameters:
	@as_Title	varchar(25)	Email Title
	@as_Subject	varchar(200)	Email Subject
	@as_Message	varchar(4000)	Email Message
	@as_CreatedBy	varchar(25)	Email Message Creator

Returns:	Nothing

Revisions:	06/14/2007 Jason Hassing

Example:	dbo.usp_Insert_EmailMessage "Title","Subject","Message...","hassij01"
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Insert_EmailMessage
(
	@as_Title	as	varchar(50),
	@as_Subject	as	varchar(200),
	@as_Message	as	varchar(4000),
	@as_CreatedBy	as	varchar(25)
)
AS
	INSERT INTO EmailMessage
	(
		Title,
		Subject,
		Message,
		CreatedBy,
		ModifiedBy,
		CreateDateTime,
		ModifyDateTime
	)	
	VALUES
	(	
		@as_Title,
		@as_Subject,
		@as_Message,
		@as_CreatedBy,
		@as_CreatedBy,
		getDate(),
		getDate()
	)
GO

