IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Get_EmailMessageByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Get_EmailMessageByPrimaryKey]
GO
/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_EmailMessageByPrimaryKey

Description:	Locates an email message
  
Parameters:	
	@ai_EmailMessageID	int  			Email Message ID

Returns:	An email message listing		

Revisions:	06/27/2007 Jason Hassing created

Example:	exec usp_Get_EmailMessageByPrimaryKey 1
------------------------------------------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_EmailMessageByPrimaryKey
(
	@ai_EmailMessageID 	int
)
AS

	SET NOCOUNT ON
	
	SELECT     EmailMessageID, Title, Subject, Message
	
	FROM         dbo.EmailMessage
	
	WHERE 
		EmailMessageID = @ai_EmailMessageID
GO

