IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Delete_EmailMessage]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Delete_EmailMessage]
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Delete_EmailMessage

Description:	Deletes an email Message from the table
  
Parameters:		
	@ai_EmailMessageID	int  			Email Message ID

Returns:	Nothing
Algorithm:	Deletes the recrod

Revisions:	06/18/2007 Jason Hassing Created

Example:	exec usp_Delete_EmailMessage 2
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Delete_EmailMessage
	@ai_EmailMessageID	as int
AS
	DELETE FROM EmailMessage

	WHERE EmailMessageID = @ai_EmailMessageID
GO

