IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[yea_GetUnitList] ') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[yea_GetUnitList] 
GO

/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	[dbo].[yea_GetUnitList] 

Description:	Returns a list of region codes

Returns:	Unit listing

Revisions:	9/24/08 Bob Schickman
-----------------------------------------------------------------------------------------------------------------------------*/

CREATE PROCEDURE [dbo].[yea_GetUnitList] AS
select replace(str(UnitNumber, 5), ' ', '0') Unit, max(isnull(DM,0)) DM, 
max(isnull(Region,0)) REG FROM tbl_UnitNumbers 
Left Outer JOIN zrops ON UnitNumber = Unit
Group by UnitNumber order by 1
GO