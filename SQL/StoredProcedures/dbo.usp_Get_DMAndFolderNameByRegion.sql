IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Get_DMAndFolderNameByRegion]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Get_DMAndFolderNameByRegion]
GO
/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_DMAndFolderNameByRegion

Description:	Returns a list if DMs and their Folder Names in a Region Code
  
Parameters:	Region, varcarch(8)

Returns:	DM code listing and coresponding folder names

Revisions:	09/10/2007 Jason Hassing created
		10/10/2007 Jason Hassing modyfied
			changed name and added folder name to return listing

Example:	exec usp_Get_DMAndFolderNameByRegion ''
------------------------------------------------------------------------------------------------------------------------------*/

-- test parameters
--DECLARE @as_Region as varchar(8)
--SELECT @as_Region = 'FET'


CREATE PROCEDURE dbo.usp_Get_DMAndFolderNameByRegion
(
	@as_Region	as varchar(8)
)
AS
	SELECT DISTINCT a.DM, b.FolderName
	FROM dbo.Zrops a
	INNER JOIN Folder b
	ON a.DM = b.FolderCode		
	WHERE Region = @as_Region AND DM <> ''
	ORDER BY DM

-- test select
--SELECT DISTINCT DM FROM zrops Where Region = @as_Region and DM <> ''
GO
