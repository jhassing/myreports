SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Updated_Master_Been_Imported]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Updated_Master_Been_Imported]
GO

CREATE     PROCEDURE dbo.Updated_Master_Been_Imported
	(	
		@PKADImport_MasterID uniqueidentifier,	
		@LastModifiedBy varchar(150),
		@BeenImported as int
	)

AS

	SET NOCOUNT ON

	UPDATE tblADImport_Master 

	SET 
		BeenImported = @BeenImported,
		ImportedDate = getdate(),
		LastModifiedBy = @LastModifiedBy
	
	WHERE
		PKADImport_MasterID = @PKADImport_MasterID 
	
	RETURN 0
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

