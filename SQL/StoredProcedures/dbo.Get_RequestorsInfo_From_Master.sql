SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Get_RequestorsInfo_From_Master]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Get_RequestorsInfo_From_Master]
GO

CREATE     PROCEDURE dbo.Get_RequestorsInfo_From_Master
	(
		@PKADImport_MasterID as uniqueidentifier,
		@RequestorsID varchar(20) output,
		@RequestorsEmail varchar(255) output,
		@RequestorsFirstName varchar(150) output,
		@RequestorsLastName varchar(150) output,
		@RequestedDate datetime output
	)
AS
	set nocount on

	SELECT @RequestorsEmail = RequestorsEmail  FROM tblADImport_Master WHERE (PKADImport_MasterID = @PKADImport_MasterID)
	SELECT @RequestorsID = RequestorsID FROM tblADImport_Master WHERE (PKADImport_MasterID=@PKADImport_MasterID)
	SELECT @RequestorsFirstName = RequestorsFirstName  FROM tblADImport_Master WHERE (PKADImport_MasterID = @PKADImport_MasterID)
	SELECT @RequestorsLastName = RequestorsLastName  FROM tblADImport_Master WHERE (PKADImport_MasterID = @PKADImport_MasterID)
	SELECT @RequestedDate = RequestedDate FROM tblADImport_Master WHERE (PKADImport_MasterID = @PKADImport_MasterID)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

