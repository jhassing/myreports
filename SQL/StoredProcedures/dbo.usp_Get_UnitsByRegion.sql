IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Get_UnitsByRegion]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Get_UnitsByRegion]
GO
/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_UnitsByRegion

Description:	Returns a list if Units in a region
  
Parameters:	Region, varcarch(8)

Returns:	Unit code listing

Revisions:	09/10/2007 Jason Hassing created

Example:	exec usp_Get_UnitsByRegion ''
------------------------------------------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_UnitsByRegion
(
	@as_Region	as varchar(8)
)
AS
	SELECT DISTINCT Unit
	FROM dbo.Zrops
	WHERE Region = @as_Region AND Unit <> ''
	ORDER BY Unit
GO
