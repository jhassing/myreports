IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Insert_ClientEmailSendFrom]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Insert_ClientEmailSendFrom]
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Insert_ClientEmailSendFrom

Description:	Inserts a new From email address
  
Parameters:	@as_FromEmailAddress		varchar(200)	New From email address	
		@as_CreatedBy			varchar	(25)	Network ID of the person doing the inserting

Returns:	Nothing

Revisions:	03/14/2006 John Schiavarelli Created
	07/24/2007 Jason Hassing

Example:	usp_Insert_ClientEmailSendFrom 'Charwells','hassij01','1'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Insert_ClientEmailSendFrom
(
	@as_FromEmailAddress		varchar(200),
	@as_CreatedBy			varchar	(25),
	@as_EmailMessageID		int	
)
AS
	INSERT INTO ClientEmailSendFrom
	(
		FromEmailAddress,
		EmailMessageID,
		CreatedBy,
		CreatedDateTime,
		ModifiedBy,
		ModifiedDateTime
	)	
	VALUES
	(
		@as_FromEmailAddress,
		@as_EmailMessageID,
		@as_CreatedBy,		
		getDate(),
		@as_CreatedBy,
		getDate()
	)
GO

