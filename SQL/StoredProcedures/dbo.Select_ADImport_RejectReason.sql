SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Select_ADImport_RejectReason]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Select_ADImport_RejectReason]
GO

CREATE     PROCEDURE dbo.Select_ADImport_RejectReason
	(	@FKADImport_MasterID as uniqueidentifier
	)

AS

  BEGIN

	set nocount on

	select Rejecter, RejectDate, RejectReason from  tblADImport_RejectReasons where FKADImport_MasterID = @FKADImport_MasterID order by RejectDate desc

	
  END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

