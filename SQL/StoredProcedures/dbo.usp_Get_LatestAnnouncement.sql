SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Get_LatestAnnouncement]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Get_LatestAnnouncement]
GO



/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_LatestAnnouncement

Description:	Returns the latest Announcement from SQL
  
Parameters:	

Returns:	Recordset	

Revisions:	07/12/2006 John Schiavarelli created

Example:	exec usp_Get_LatestAnnouncement
------------------------------------------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_LatestAnnouncement
AS

	SET NOCOUNT ON
	
	SELECT     TOP 1 id, MAX(itemdate) AS itemdate, title, description, CreatedBy, CreatedDateTime, ModifiedBy, ModifiedDateTime
FROM         dbo.Announcements
WHERE            itemdate <=GETDATE()
GROUP BY id, title, description, CreatedBy, CreatedDateTime, ModifiedBy, ModifiedDateTime
ORDER BY itemdate DESC


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[usp_Get_LatestAnnouncement]  TO [Role_HostedUser]
GO

