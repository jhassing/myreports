IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[yea_GetRegCodes]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[yea_GetRegCodes] 
GO

/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	yea_GetRegCodes

Description:	Returns a list of region codes
  
Parameters:	Region, varcarch(8)

Returns:	Region code listing

Revisions:	9/24/08 Bob Schickman
-----------------------------------------------------------------------------------------------------------------------------*/

CREATE PROCEDURE [dbo].[yea_GetRegCodes] AS
SELECT DISTINCT [Region] FROM [zrops] ORDER BY [Region]
GO