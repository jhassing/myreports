if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_ViewedReportDate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_ViewedReportDate]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Insert_ViewedReportDate

Description:	Gets a listing of ViewedReportDate records from the database based on UserName, ReportName and CreationDate
  
Parameters:	
		@as_UserName		as varchar(50),		User who viewed the report.
		@as_ReportName	as varchar(250),		Report that the user has viewed.
		@ad_CreationTime	as DateTime,		Date/Time the report was created.
		@ad_ViewedDate	as DateTime		Date/Time the report was viewed.
		@as_ReportCode		as varchar(20)		ReportCode

Returns:	Nothing

Revisions:	
		04/29/2004 John Schiavarelli Created
		02/08/2006 MJH Modified - added ReportCode to parameters
		12/18/2007 Jason Hassing - do not record additional viewed date records for reports already viewed per UserName and CreationDate


Example:	exec usp_Insert_ViewedReportDate 
			'hassij02',
			'/(0) Archives/(11) November/(3) Operations/999999 - Jason Hassing/Subsidy Invoice ZCAR2821.pdf',
			'2007-12-04 22:17:32.110',
			'2007-12-13 14:45:00.110',
			'ZCAR2821'
		
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE    PROCEDURE dbo.usp_Insert_ViewedReportDate
	@as_UserName			as varchar(50),
	@as_ReportName		as varchar(250),
	@ad_CreationTime		as DateTime,
	@ad_ViewedDate		as DateTime,
	@as_ReportCode		as varchar(20)

AS

	DECLARE	@lb_ReturnValue bit

	SET NOCOUNT ON
	SELECT 
		@lb_ReturnValue = count(0) 
	FROM 
		ReportViewedDate 
	WHERE 
		Username = @as_UserName 
	AND 
		ReportName LIKE '%' + RIGHT(@as_ReportName, LEN(@as_ReportName) - PATINDEX('%/(3)%', @as_ReportName) + 1)
	AND
		CreationTime = @ad_CreationTime
		
	IF @lb_ReturnValue = 0
		BEGIN

			INSERT INTO  ReportViewedDate
			(
				UserName,
				[ReportName],
				CreationTime,
				ViewedDate,
				ReportCode
			)
			VALUES
			(
				@as_UserName,
				@as_ReportName,
				@ad_CreationTime,
				@ad_ViewedDate,
				@as_ReportCode
			)
		END

/*	ELSE
		BEGIN
			UPDATE 
				ReportViewedDate
			SET 
				ViewedDate = @ad_ViewedDate,
				CreationTime = @ad_CreationTime,
				ReportCode = @as_ReportCode
	
			WHERE 
				Username = @as_UserName 
			AND 
				[ReportName] = @as_ReportName
		END
*/


GO