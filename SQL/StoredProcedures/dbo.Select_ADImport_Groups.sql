SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Select_ADImport_Groups]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Select_ADImport_Groups]
GO

CREATE     PROCEDURE dbo.Select_ADImport_Groups
	(	
		@FKADImport_MasterID as uniqueidentifier
	)

AS
  BEGIN
	SELECT   *  FROM  dbo.tblADImport_Groups  WHERE (FKADImport_MasterID = @FKADImport_MasterID)
  END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

