SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Update_Announcements]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Update_Announcements]
GO


/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Update_Announcements

Description:	Updates a client email address 
  
Parameters:

Returns:	Nothing

Revisions:	07/12/2006 John Schiavarelli Created

Example:	exec 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Update_Announcements
	@ai_id				as int,
	@ad_itemdate		as datetime,
	@as_description		as varchar(1000),
	@as_ModifiedBy 		as varchar(25)

AS
	UPDATE 
		Announcements
	SET	
		itemdate = @ad_itemdate,  
		description = @as_description, 
		ModifiedBy = @as_ModifiedBy,
		ModifiedDateTime = getDate()
	WHERE
		id = @ai_id

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[usp_Update_Announcements]  TO [Role_ReportDistributionUser]
GO

GRANT  EXECUTE  ON [dbo].[usp_Update_Announcements]  TO [Role_HostedUser]
GO

