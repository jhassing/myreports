IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Get_ClientEmailSendFromByPrimaryKey]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Get_ClientEmailSendFromByPrimaryKey]
GO
/*---------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Get_ClientEmailSendFromByPrimaryKey

Description:	Locates a client email addresss listing
  
Parameters:	

Returns:	Client email address listing		

Revisions:	03/14/2006 John Schiavarelli created
		06/19/2007 Jason Hassing modified

Example:	exec usp_Get_ClientEmailSendFromByPrimaryKey 22
----------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_ClientEmailSendFromByPrimaryKey
(
	@ai_ClientEmailSendFromID 	int
)
AS

	SET NOCOUNT ON
	
	SELECT
		ClientEmailSendFromID,
		FromEmailAddress,
		CreatedBy,
		CreatedDateTime,
		ModifiedBy,
		ModifiedDateTime,
		EmailMessageID
	
	FROM         dbo.ClientEmailSendFrom
	
	WHERE 
		ClientEmailSendFromID = @ai_ClientEmailSendFromID
GO

