SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Insert_ADImport_Detail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Insert_ADImport_Detail]
GO

CREATE     PROCEDURE dbo.Insert_ADImport_Detail
	(	@FKADImport_MasterID as uniqueidentifier, 
		@AttributeName as varbinary(250),
		@AttributeValue as varbinary(250)
	)

AS

  BEGIN

	INSERT INTO tblADImport_Detail
	(
		FKADImport_MasterID,
		AttributeName,
		AttributeValue
	)
	VALUES
	(   
		@FKADImport_MasterID,
		@AttributeName,
		@AttributeValue
	)
	
  END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

