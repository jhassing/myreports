IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Get_EmailMessageListing]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Get_EmailMessageListing]
GO
/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_EmailMessageListing

Description:	Locates email message(s)
  
Parameters:	Title

Returns:	All email messages with title that matches the parameter		

Revisions:	06/27/2007 Jason Hassing created

Example:	exec usp_Get_EmailMessageListing 'Title'
------------------------------------------------------------------------------------------------------------------------------*/

CREATE      PROCEDURE dbo.usp_Get_EmailMessageListing
(
	@as_Title 	varchar(50)
)
AS

	SET NOCOUNT ON
	
	SELECT	EmailMessageID, Title, Subject, Message
	
	FROM		dbo.EmailMessage
	
	WHERE 	Title like '%' + @as_Title + '%'
GO

