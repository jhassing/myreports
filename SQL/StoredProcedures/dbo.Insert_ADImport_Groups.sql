SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Insert_ADImport_Groups]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Insert_ADImport_Groups]
GO

CREATE     PROCEDURE dbo.Insert_ADImport_Groups
	(	@FKADImport_MasterID as uniqueidentifier, 
		@GroupName as varbinary(255)
	)

AS

  BEGIN

	INSERT INTO tblADImport_Groups
	(
		FKADImport_MasterID,
		GroupName
	)
	VALUES
	(   
		@FKADImport_MasterID,
		@GroupName
	)
	
  END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

