SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Insert_Announcements]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Insert_Announcements]
GO



/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Insert_Announcements

Description:	Inserts a new Announcement
  
Parameters:		@ad_itemdate		as datetime,
				@as_title			as varchar(500),
				@as_description		as varchar(1000),
				@as_CreatedBy 		as varchar(25)

Returns:	Nothing

Revisions:	07/11/2006 John Schiavarelli Created

Example:	exec usp_Insert_ClientEmailAddress 'JohnSchiavarelli@msn.com', 'Client Name', 'Client Contact', 1, 'CC1@msn.com', 'CC2@msn.com', 'CC3@Msn.com', 'John', 'LastName','cha', 'schiaj02'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Insert_Announcements
	@ad_itemdate		as datetime,
	@as_title			as varchar(500),
	@as_description		as varchar(1000),
	@as_CreatedBy 		as varchar(25)

AS
	INSERT INTO Announcements
	(
		itemdate,
		title,
		description,	
		CreatedBy,
		CreatedDateTime,
		ModifiedBy,
		ModifiedDateTime
	)	
	VALUES
	(
		@ad_itemdate,
		@as_title,
		@as_description,
		@as_CreatedBy,
		getDate(),
		@as_CreatedBy,
		getDate()
	)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GRANT  EXECUTE  ON [dbo].[usp_Insert_Announcements]  TO [Role_HostedUser]
GO

