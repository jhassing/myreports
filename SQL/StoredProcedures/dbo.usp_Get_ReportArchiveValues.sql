USE [MyReports]
GO
/****** Object:  StoredProcedure [dbo].[usp_Get_ReportArchiveValues]    Script Date: 10/25/2012 09:00:18 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



/*----------------------------------------------------------------------------------------------------------------------------------------------------------	
Stored Proc:	usp_Get_ReportArchiveValues

Description:  	Gets all the archive values for a particular report code.
		
						
Parameters:	@as_ReportCode  - Report code to get values for
		@ai_OverwriteCode - Overwrite Code  (Output)
		@ai_ArchiveLength - Archive length in days, weeks or months (Output)
		@ai_ReportTypeID - Report type, FInancial, Payroll, ect  (Output)
		@ai_ArchiveValueCode - Archive value Code  (Output)

Returns:	Output paramateres

Revisions:	03/22/2005 Created - John Schiavarelli

EXAMPLE:	
	DECLARE @ai_OverwriteCode 	INT
	DECLARE @ai_ArchiveLength 	INT
	DECLARE @ai_ReportTypeID 	INT
	DECLARE @ai_ArchiveValueCode 	INT
	
	EXEC dbo.usp_Get_ReportArchiveValues  'ZCHR0042B', @ai_OverwriteCode output, @ai_ArchiveLength output, @ai_ReportTypeID output, @ai_ArchiveValueCode output
	
	PRINT @ai_OverwriteCode
	PRINT @ai_ArchiveLength
	PRINT @ai_ReportTypeID
	PRINT @ai_ArchiveValueCode
		
----------------------------------------------------------------------------------------------------------------------------------------------------------*/

ALTER  PROCEDURE [dbo].[usp_Get_ReportArchiveValues]

	@as_ReportCode 		varchar(11),
	@ai_OverwriteCode 		int output,
	@ai_ArchiveLength		int output, 
	@ai_ReportTypeID		int output,
	@ai_ArchiveValueCode		int output

	AS
	
	Set NOCOUNT on
	
	SELECT  	@ai_OverwriteCode 		=	dbo.OverwriteValue.OverwriteValueID, 
			@ai_ArchiveLength 		= 	dbo.Report.ArchiveLength,
			@ai_ReportTypeID 		= 	dbo.ReportType.ReportTypeID, 
			@ai_ArchiveValueCode 		= 	dbo.ArchiveValue.ArchiveValueID
	
	FROM  
		       	dbo.Report WITH (NOLOCK) 
	LEFT OUTER JOIN
			dbo.ReportType ON dbo.Report.ReportTypeID = dbo.ReportType.ReportTypeID 
	LEFT OUTER JOIN
	               	dbo.OverwriteValue ON dbo.Report.OverwriteValueID = dbo.OverwriteValue.OverwriteValueID 
	LEFT OUTER JOIN
	                     	dbo.ArchiveValue ON dbo.Report.ArchiveValueID = dbo.ArchiveValue.ArchiveValueID 
		
	WHERE 	
			dbo.Report.ReportCode = @as_ReportCode
