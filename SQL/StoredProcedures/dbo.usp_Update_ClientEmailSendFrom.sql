IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Update_ClientEmailSendFrom]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Update_ClientEmailSendFrom]
GO
/*--------------------------------------------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	dbo.usp_Update_ClientEmailSendFrom

Description:	Updates a client from email address
  
Parameters:	@as_FromEmailAddress		varchar(200)	New From email address

Returns:	Nothing

Revisions:	03/14/2006 John Schiavarelli Created
		07/06/2007 Jason Hassing Modified

Example:	usp_Update_ClientEmailSendFrom '1','Rob Watkins','1','hassij01'
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

CREATE   PROCEDURE dbo.usp_Update_ClientEmailSendFrom
(
	@ai_ClientEmailSendFromID	int,
	@as_FromEmailAddress		varchar(200),
	@as_EmailMessageID		int,
	@as_ModifiedBy		varchar(25)

)
AS
	UPDATE 
		ClientEmailSendFrom
	SET	
		FromEmailAddress = @as_FromEmailAddress,
		EmailMessageID = @as_EmailMessageID,
		ModifiedBy = @as_ModifiedBy,
		ModifiedDateTime = getDate()
		
	WHERE
		ClientEmailSendFromID = @ai_ClientEmailSendFromID
GO

