SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Insert_ADImport_RejectReason]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Insert_ADImport_RejectReason]
GO

CREATE     PROCEDURE dbo.Insert_ADImport_RejectReason
	(	@FKADImport_MasterID as uniqueidentifier, 
		@Rejecter as varchar(100),
		@RejectReason  as text
	)

AS

  BEGIN

	set nocount on

	INSERT INTO tblADImport_RejectReasons
	(
		FKADImport_MasterID,
		Rejecter,
		RejectDate,
		RejectReason
	)
	VALUES
	(   
		@FKADImport_MasterID,
		@Rejecter,
		getDate(),
		@RejectReason	
	)

	exec Updated_Master_Been_Imported @FKADImport_MasterID, @Rejecter, 2
	
  END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

