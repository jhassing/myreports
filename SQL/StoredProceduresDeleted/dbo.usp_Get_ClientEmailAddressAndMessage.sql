/*-------------------------------------------------------------------------------------------------------------------------------
Stored Proc:	usp_Get_ClientEmailAddressAndMessage

Description:	Locates a client email addresss listing
  
Parameters:	

Returns:	Client email address listing		

Revisions:	06/20/2007 Jason Hassing created

Example:	exec usp_Get_ClientEmailAddressAndMessage '','','','','',''
------------------------------------------------------------------------------------------------------------------------------*/


CREATE      PROCEDURE dbo.usp_Get_ClientEmailAddressAndMessage
(
	@as_ClientEmailAddress 	as varchar(200),
	@as_ClientName		as varchar(200),
	@as_KeyWord			as varchar(200),
	@as_AccountantFN		as varchar(50),
	@as_AccountantLN		as varchar(100),
	@as_Region			as varchar(25)
)
AS

	SET NOCOUNT ON

	SELECT     	dbo.ClientEmailSendFrom.FromEmailAddress AS FromEmailAddress, 
			dbo.ClientEmailAddress.ClientEmailAddressID,
			dbo.ClientEmailAddress.ClientEmailAddress, 
			dbo.ClientEmailAddress.ClientName, 
		            	dbo.ClientEmailAddress.KeyWord, 
		            	dbo.ClientEmailAddress.ClientEmailSendFromID, 
		            	dbo.ClientEmailAddress.CC1, 
		            	dbo.ClientEmailAddress.CC2, 
	            		dbo.ClientEmailAddress.BCC1, 
			dbo.ClientEmailAddress.AccountantFN, 
			dbo.ClientEmailAddress.AccountantLN,
			dbo.ClientEmailAddress.Region, 
			dbo.ClientEmailAddress.CreatedBy, 
			dbo.ClientEmailAddress.CreatedDateTime, 
			dbo.ClientEmailAddress.ModifiedBy, 
			dbo.ClientEmailAddress.ModifiedDateTime,
			dbo.EmailMessage.Message,
			dbo.EmailMessage.Subject

	FROM         dbo.ClientEmailSendFrom 
		INNER JOIN	dbo.ClientEmailAddress 
		ON 		dbo.ClientEmailSendFrom.ClientEmailSendFromID = dbo.ClientEmailAddress.ClientEmailSendFromID
		INNER JOIN	dbo.EmailMessage
		ON		dbo.EmailMessage.EmailMessageID = dbo.ClientEmailSendFrom.EmailMessageID

 
	WHERE 
		( 
			( ClientEmailAddress like '%' + @as_ClientEmailAddress + '%') AND
			( ClientName like '%' + @as_ClientName + '%' ) AND
			( KeyWord like '%' + @as_KeyWord + '%' ) AND
			( AccountantFN like '%' + @as_AccountantFN + '%' ) AND
			( AccountantLN like '%' + @as_AccountantLN + '%' ) AND
			( Region like '%' + @as_Region + '%' )
		)
GO
