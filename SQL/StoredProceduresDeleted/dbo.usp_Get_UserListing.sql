SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[usp_Get_UserListing]') AND OBJECTPROPERTY(id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[usp_Get_UserListing]
GO

CREATE PROCEDURE [dbo].[usp_Get_UserListing]
	/*
	(
	@parameter1 int = 5,
	@parameter2 datatype OUTPUT
	)
	*/
AS

SELECT     UserConversionID, firstName, lastName, perNo, networkID, [exists]
FROM         dbo.UserConversion