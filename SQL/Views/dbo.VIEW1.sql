SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[VIEW1]
GO

CREATE VIEW dbo.VIEW1
AS
SELECT     AttributeName, AttributeValue
FROM         dbo.tblADImport_Detail
WHERE     (FKADImport_MasterID = '4ebedb4c-1d95-4ea6-9016-4647b186cf2e')

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

