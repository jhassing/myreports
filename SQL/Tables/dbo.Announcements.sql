CREATE TABLE [dbo].[Announcements] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[itemdate] [datetime] NULL ,
	[title] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[description] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreatedBy] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreatedDateTime] [datetime] NULL ,
	[ModifiedBy] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModifiedDateTime] [datetime] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Announcements] WITH NOCHECK ADD 
	CONSTRAINT [PK_Announcements] PRIMARY KEY  CLUSTERED 
	(
		[id]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

