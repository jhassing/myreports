CREATE TABLE [dbo].[UserConversion] (
	[UserConversionID] [int] IDENTITY (1, 1) NOT NULL ,
	[firstName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[lastName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[perNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[networkID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[exists] [int] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[UserConversion] WITH NOCHECK ADD 
	CONSTRAINT [PK_UserConversion] PRIMARY KEY  CLUSTERED 
	(
		[UserConversionID]
	)  ON [PRIMARY] 
GO
