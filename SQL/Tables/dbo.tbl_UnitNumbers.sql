USE [MyReports]
GO
/****** Object:  Table [dbo].[tbl_UnitNumbers]    Script Date: 09/23/2008 14:42:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_UnitNumbers](
	[UnitNumberID] [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UnitNumber] [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
