dbo.usp_Insert_EmailMessage 'Default',
'[FROM] Current period Invoice',
'Please find attached your current period subsidy bill and supporting financial statement(s).   

Please DO NOT reply to this e-mail.  If you have any questions, please contact the number provided at the bottom of your subsidy invoice, as this email address does not accept replies.



Thank you,

[FROM] Department',
'hassij01'


ALTER TABLE [dbo].[ClientEmailSendFrom] ADD CONSTRAINT [FK_ClientEmailSendFrom_EmailMessage] FOREIGN KEY 
	(
		[EmailMessageID]
	)
	REFERENCES [EmailMessage] 
	(	
		[EmailMessageID]
	)