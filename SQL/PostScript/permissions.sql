
-----------------------------------------------------
--Create login for role to read/update data
-----------------------------------------------------

if not exists (select * from dbo.sysusers where name = N'Role_HostedUser' and uid > 16399)
	EXEC sp_addrole N'Role_HostedUser'
GO



--Grant Execute on all stored procedure Objects
DECLARE lc_Access_Cursor CURSOR FOR
SELECT 'GRANT EXECUTE ON [' + USER_NAME(uid) + '].[' + name + '] TO ' + '[Role_HostedUser]'
	FROM sysobjects
	WHERE type = 'P'
		AND OBJECTPROPERTY(OBJECT_ID(QUOTENAME(USER_NAME(uid)) + '.' + QUOTENAME(name)), 'IsMSShipped') = 0
		AND name LIKE '%usp%' and USER_NAME(uid) = 'dbo' /*To grant EXECUTE permission on only procedures starting with Rep*/

DECLARE @ls_SQLStatement nvarchar(255)
OPEN lc_Access_Cursor

FETCH NEXT FROM lc_Access_Cursor INTO @ls_SQLStatement
WHILE (@@fetch_status <> -1)
BEGIN
	print @ls_SQLStatement

	EXEC sp_executesql @ls_SQLStatement
	FETCH NEXT FROM lc_Access_Cursor INTO @ls_SQLStatement
END

CLOSE lc_Access_Cursor
DEALLOCATE lc_Access_Cursor
GO


