USE [MyReports]
GO
/****** Object:  Table [dbo].[UserConversion]    Script Date: 12/05/2008 15:42:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserConversion](
	[UserConversionID] [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[firstName] [nchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[lastName] [nchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[perNo] [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[networkID] [nchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[exists] [int] NULL
) ON [PRIMARY]
